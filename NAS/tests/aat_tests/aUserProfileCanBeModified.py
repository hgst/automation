"""
title           :aUserProfileCanBeModified.py
description     :To verify if a created user profiled can be edited
author          :yang_ni
date            :2015/04/28
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time

class AUserProfileCanBeModified(TestClient):

    def run(self):
        try:
            global model
            model = self.get_model_number()

            self.log.info('Deleting all users ...')
            self.delete_all_users()
            time.sleep(3)
            self.log.info('Deleting all groups ...')
            self.delete_all_groups()

            self.log.info('Create a user named user1 ...')
            self.create_user(username='user1',password='pass',fullname='before before')

            if model != 'GLCR':
                self.log.info("Create two groups, user1 assigned to group1...")
                self.create_groups(group_name='group1', number_of_groups=1, memberusers='user1')
                self.create_groups(group_name='group2', number_of_groups=1)

                # Create user quota
                self.log.info("Setting user1 quota to 10MB...")
                self.create_user_quota(user_name='user1',size_of_quota=10,unit_size='MB')

                time.sleep(13)
                self.log.info("Get the groups which user1 belongs to")
                self.click_element('users_editGroupMember_link',timeout=15)
                group = self.get_text("//div[@id=\'m_group_div\']/ul/li[1]/div[2]")
                group1_chkbox = self.is_checkbox_selected('css=.LightningCheckbox,users_modGroup0_chkbox')
                self.click_element('users_modGroupSave_button', timeout=10)

                time.sleep(10)
                self.log.info("Get how much is the usage quota allotted to user1")
                self.click_element('users_editUserQuota_link',timeout=15)
                time.sleep(5)
                quota = self.element_find('users_v1Size_text').get_attribute('value')
                self.click_element('uesrs_modQuotaCancel_button', timeout=10)

            time.sleep(7)

            if model == 'GLCR':
                self.get_to_page('Home')
                time.sleep(7)
                self.get_to_page('Users')
                time.sleep(5)
                self.click_element('users_user_user1',timeout=15)
                time.sleep(8)

            '''
            Verifying if the user profiles are set successfully
            '''

            passwd = self._get_user_password(user='user1')
            firstname = self.element_find('users_editFirstName_text').get_attribute('value')
            lastname = self.element_find('users_editLastName_text').get_attribute('value')

            self.log.info('***Before start modifying profile, the data of user1 is as following: ***')

            if firstname == 'before':
                self.log.info('The first name is {0}'.format(firstname))
            else:
                self.log.error('Firstname is not set successfully.')

            if lastname == 'before':
                self.log.info('The last name is {0}'.format(lastname))
            else:
                self.log.error('Lastname is not set successfully.')

            if passwd == 'pass':
                self.log.info('The passwd is {0}'.format(passwd))
            else:
                self.log.error('Password is not set successfully.')

            if model != 'GLCR':
                if (group == 'group1') and (group1_chkbox == True):
                    self.log.info('User1 is assigned to {0}'.format(group))
                else:
                    self.log.error('Groups which user1 belongs to is not set successfully.')

                if quota == '10':
                    self.log.info('User1 is assigned {0} MB usage quota'.format(quota))
                else:
                    self.log.error('User1 usage quota is not set successfully.')

            '''
            Modify user profile
            '''
            self.modify_profile()

        except Exception as e:
            self.log.error('Failed with exception: {0}'.format(repr(e)))


    def modify_profile(self):

            '''
            Modify user profile
            '''

            self.log.info('***Starts to modify user1 profile...***')

            self.log.info('Modify first and last name, and also password of user1')
            self.update_user(user='user1',old_password='pass',new_password='pass2',fullname='after after')

            if model != 'GLCR':
                self.log.info('Assigning user1 to group2')
                self.assign_user_to_group(user_name='user1',group_name='group2')
                self.get_to_page('Home')
                time.sleep(8)

                # Create user quota
                self.log.info("Setting user quota to 100MB...")
                self.create_user_quota(user_name='user1',size_of_quota=100,unit_size='MB')
                time.sleep(15)

                self.log.info("Get the groups which user1 belongs to")
                self.get_to_page('Home')
                time.sleep(7)
                self.get_to_page('Users')
                time.sleep(5)
                self.click_element('users_user_user1',timeout=15)
                self.wait_until_element_is_visible('users_editGroupMember_link',timeout=15)
                self.click_element('users_editGroupMember_link',timeout=15)
                group = self.get_text("//div[@id=\'m_group_div\']/ul/li[2]/div[2]")
                group2_chkbox = self.is_checkbox_selected('css=.LightningCheckbox,users_modGroup1_chkbox')
                self.click_element('users_modGroupSave_button', timeout=10)
                time.sleep(10)

                self.log.info("Get how much is the usage quota allotted to user1")
                self.click_element('users_editUserQuota_link',timeout=15)
                time.sleep(5)
                quota = self.element_find('users_v1Size_text').get_attribute('value')
                self.log.info("Quota is {} MB now".format(quota))
                self.click_element('uesrs_modQuotaCancel_button', timeout=10)

            '''
            Verifying if the user profiles are changed successfully
            '''
            if model == 'GLCR':
                time.sleep(8)
                self.get_to_page('Home')
                time.sleep(7)
                self.get_to_page('Users')
                time.sleep(5)
                self.click_element('users_user_user1',timeout=15)
                time.sleep(8)

            passwd = self._get_user_password(user='user1')
            firstname = self.element_find('users_editFirstName_text').get_attribute('value')
            lastname = self.element_find('users_editLastName_text').get_attribute('value')

            self.log.info('***After modifying profile, the data of user1 is as following: ***')

            if firstname =='after':
                self.log.info('The first name is {0}'.format(firstname))
            else:
                self.log.error('Firstname is not changed successfully.')

            if lastname == 'after':
                self.log.info('The last name is {0}'.format(lastname))
            else:
                self.log.error('Lastname is not changed successfully.')

            if passwd == 'pass2':
                self.log.info('The passwd is {0}'.format(passwd))
            else:
                self.log.error('Password is not changed successfully.')

            if model != 'GLCR':
                if (group == 'group2') and (group2_chkbox == True):
                    self.log.info('User1 is assigned to {0}'.format(group))
                else:
                    self.log.error('Groups which user1 belongs to is not changed successfully.')

                if quota == '100':
                    self.log.info('User1 is assigned {0} MB usage quota'.format(quota))
                else:
                    self.log.error('User quota is not changed successfully.')

            '''
            #lastname, password, email doesn't work
            self.modify_user_account(user='user1',passwd='pass2',firstname='rick',lastname='lin',email='abc@abc.com')
            '''

    def tc_cleanup(self):
            self.log.info('Deleting all users ...')
            self.delete_all_users()
            self.log.info('Deleting all groups ...')
            self.delete_all_groups()


# This constructs the AUserProfileCanBeModified() class, which in turn constructs TestClient() which triggers the AUserProfileCanBeModified.run() function
AUserProfileCanBeModified()