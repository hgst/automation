""" Settings page-Network-Profile

    @Author: Lee_e
    
    Objective:Check Network Profile
    
    Automation: Full
    
    Supported Products:
        All    
    
    Test Status: 
              Local Lightning: Pass (FW: 1.05.30)
                    Lightning: Pass (FW: 2.00.40)
                    KC       : Pass (FW: 2.00.145)
              Jenkins Taiwan : Lightning -pass (FW: 2.00.174)
                             : Yosemite - pass (FW: 2.00.174)
                             : Kings Canyon - pass (FW:2.00.174)
                             : Sprite - pass (FW:2.00.174)
                             : Aurora - pass (FW: 2.00.171)
              Jenkins Irvine : Zion -pass (FW: 2.00.174)
                             : Glacier - pass (FW:2.00.174)
                             : Yellowstone -pass (FW:2.00.175)
     
""" 
from testCaseAPI.src.testclient import TestClient
import time

class NetworkProfile(TestClient):
    def run(self):
        self.check_network_profile()
        
    def check_network_profile(self):
        mac_address = ''.join(self.get_xml_tags(self.get_system_information(), 'mac_address'))
        ip_address = ''.join(self.get_xml_tags(self.get_network_configuration(), 'ip'))
        self.log.info('MAC Address from REST:{0} , IP from REST: {1}'.format(mac_address, ip_address))
        mac_address_ui, ip_address_ui = self.get_mac_and_ip_from_ui()
        if mac_address in mac_address_ui:
            self.log.info('PASS: MAC address shows correctly on Network Profile')
        else:
            self.log.error('FAIL: MAC address shows incorrectly on Network Profile')
        if ip_address in ip_address_ui:
            self.log.info('PASS: IP address shows correctly on Network Profile')
        else:
            self.log.error('FAIL: IP address shows incorrectly on Network Profile')

    def get_mac_and_ip_from_ui(self):
        self.get_to_page('Settings')
        time.sleep(2)
        self.click_wait_and_check(self.Elements.NETWORK_BUTTON, 'mac_div', timeout=30)
        mac_address_ui = self.get_text('mac_div')
        ip_address_ui = self.get_text('ipv4_address_div')
        self.log.info('MAC address UI: {0}, IP address UI:{1}'.format(mac_address_ui, ip_address_ui))
        return mac_address_ui, ip_address_ui

NetworkProfile()