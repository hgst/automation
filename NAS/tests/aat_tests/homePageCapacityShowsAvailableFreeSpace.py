'''
Create on May 5, 2015

@Author: lo_va

Objective: Verify Capacity shows available free space from WebUI.
Test ID: 117314
'''
import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E

class homePageCapacityShowsAvailableFreeSpace(TestClient):
    
    def run(self):
        
        try:
            self.log.info('Start to check Capacity free space in home page')
            self.get_to_page('Home')
            
            # Capacity verification
            if self.is_element_visible(E.HOME_DEVICE_CAPACITY_FREE_SPACE, 10):
                raid_capacity = self.get_text(E.HOME_DEVICE_CAPACITY_FREE_SPACE)
            elif self.is_element_visible('home_volcapity_info', 10):
                raid_capacity = self.get_text('home_volcapity_info')
            
            if raid_capacity:
                self.log.info('Raid capacity is %s' % (raid_capacity))
            if not raid_capacity:
                self.log.error('Checked raid capacity free space failed in home page!')
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Checked raid capacity free space failed in homp page!')
    
homePageCapacityShowsAvailableFreeSpace()