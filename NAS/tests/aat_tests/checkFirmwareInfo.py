"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify firmware info is available: 'name', 'version', 'description', 'package_build_time', 'last_upgrade_time'

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

firmware_info = ['name', 'version', 'description', 'package_build_time', 'last_upgrade_time']

class CheckFirmwareInfo(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            result = self.get_firmware_info()
            for item in firmware_info:
                value = self.get_xml_tag(result[1], item)
                self.log.info('{}: {}'.format(item, value))
        except Exception, ex:
            self.log.exception('Failed to complete firmware info test case \n' + str(ex))

CheckFirmwareInfo()