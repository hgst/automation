"""
title           :usersPageAdminProfileCanBeViewed.py
description     :To verify if admin user profile can be viewed
author          :yang_ni
date            :2015/07/29
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time

class AdminProfileCanBeViewed(TestClient):

    def run(self):

        try:
            self.modify_user_account(user='admin',fullname='Test 123')
            time.sleep(8)

            # Get admin profile value from DB
            fullname = self.get_xml_tag(self.get_user(username='admin')[1], tag='fullname')
            firstname = fullname.split(' ')[0]
            lastname = fullname.split(' ')[1]
            time.sleep(5)

            # Get profile value from UI
            self.get_to_page('Users')
            time.sleep(8)
            self.click_wait_and_check('users_user_admin', 'users_editUserName_text', timeout=30)
            time.sleep(5)
            ui_firstname = self.element_find('users_editFirstName_text').get_attribute('value')
            ui_lastname = self.element_find('users_editLastName_text').get_attribute('value')

            # Compare the value between DB and UI, if consistent then PASS
            self.log.info('*** Checking if Admin profile can be viewed: ***')
            if firstname == ui_firstname:
                self.log.info('The first name is {0}'.format(firstname))
            else:
                self.log.error('The first name cant be viewed, first name = {0}'.format(firstname))

            if lastname == ui_lastname:
                self.log.info('The last name is {0}'.format(lastname))
            else:
                self.log.error('The last name cant be viewed, last name = {0}'.format(lastname))

        except Exception as e:
            self.log.error('ERROR: Admin profile cant be viewed successfully: {}'.format(repr(e)))
        else:
            self.log.info('*** Admin profile can be viewed successfully ***')
        finally:
            self.log.info('*** Reset admin profile to default ***')
            self.modify_user_account(user='admin',passwd='',fullname='')

AdminProfileCanBeViewed()