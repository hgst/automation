""" Settings Page Utilities Device Maintenance (MA-90)

    @Author: vasquez_c
    Procedure @ silk:
                1) Open the webUI -> login as system admin
                2) Click on the Settings category
                3) Select Utilities -> Device Maintenance
                4) Click on the shutdown button
                5) Click on the reboot button
                6) Verify that shutdown and reboot are functional

    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.196)
"""             
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.webdriver.common.keys import Keys
from testCaseAPI.src.testclient import Elements as E
import time
import os
import re

testname = 'Settings Page Utilities Device Maintenance Test'
twoBayNAS = ['BWAZ', 'BBAZ', 'KC2A', 'BZVM']
fourBayNAS = ['LT4A', 'BNEZ', 'BWZE']

class utilitiesDeviceMaintenance(TestClient):
    
    def run(self):
        self.start_test(testname)

        try:
            time.sleep(3)
            self.perform_operation(operation='REBOOT')
            self.verify_reboot()
             
            self.perform_operation(operation='SHUTDOWN')
            self.verify_shutdown()      
        except Exception as e:
            self.fail_test(testname, repr(e))
        else:
            self.pass_test(testname)            
        self.log.info('######################## Settings Page - Utilities - Reboot/Shutdown TEST END ##############################')
    
    def tc_cleanup(self):
        self.turn_on_by_WOL()
                
    def perform_operation(self, operation):
        self.log.info('Performing "{}" operation'.format(operation))
        operation = operation.upper()
        
        self.delete_all_alerts()
        
        model_number = self.get_model_number()
        
        if operation == 'SHUTDOWN':
            self.click_button(E.SETTINGS_UTILITY_SHUTDOWN)
            time.sleep(2)
            self.click_button(locator='popup_apply_button')
            time.sleep(120)
            
            # Turn unit back on
            self.log.info('Turning unit on, please stand by ...')
            self.turn_on_by_WOL()
        else:
            # Execute reboot command
            self.click_button(E.SETTINGS_UTILITY_REBOOT)
            time.sleep(2)
            self.click_button(locator='popup_apply_button')
        
        if model_number in fourBayNAS:
            time.sleep(240)
        else:
            time.sleep(120)
        
        self.close_webUI()
        # wait for system to be ready
        count = 0
        while self.is_ready() is False:
            time.sleep(60)
            count += 1
            if count > 10:
                self.log.error('FAILED: Unit not ready after 10 minutes')
                break
        self.log.info('finished booting up')
        
    def verify_reboot(self):
        results =  self.get_alerts()
        alerts = self.get_xml_tags(xml_content=results[1], tag='title') 
     
        if 'System Rebooting' in alerts:
            self.log.info ('SUCCESS: Settings-Utilities-Device_Maintenance (Reboot) is functional')
        else:
            self.log.error ('FAILED: Settings-Utilities-Device_Maintenance (Reboot) is NOT functional')
            
               
    def verify_shutdown(self):
        self.get_to_page('Home')
        results =  self.get_alerts()
        alerts = self.get_xml_tags(xml_content=results[1], tag='title') 
     
        if 'System Shutting Down' in alerts:
            self.log.info ('SUCCESS: Settings-Utilities-Device_Maintenance (Shutdown) is functional')
        else:
            self.log.error ('FAILED: Settings-Utilities-Device_Maintenance (Shutdown) is NOT functional')        
        
    def turn_on_by_WOL(self):
        self.log.info('********** state = turn_on_by_WOL *********')

        self.send_magic_packet()
        time.sleep(3)
        start_time = time.time()
      
        while not self.is_ready():
            self.log.info('ping system')
            time.sleep(20)
            if time.time() - start_time > 380:
                raise Exception('Timed out for waiting to boot by WoL:{0} seconds '.format(time.time()-start_time))  
        if self.is_ready():
            self.log.info(' WoL spend time: {0} seconds'.format(time.time()-start_time))  
            self.log.info('PASS: Wake on Lan test Succeeded')          
utilitiesDeviceMaintenance()
        
        
            