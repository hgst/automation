"""
Create on Jan 29, 2016
@Author: lo_va
Objective:  Verify Device Manager module is loaded in firmware
            Verify devicemgr daemon is running
wiki URL: http://wiki.wdc.com/wiki/Load_Device_Manager_in_firmware
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadDeviceManager(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        devicemgr_daemon = ''
        devicemgr_files_list = ''
        devicemgr_ls_list = ''
        try:
            devicemgr_daemon = self.execute('/etc/init.d/devicemgrd status')[1]
            devicemgr_files_list = self.execute('find / -name *devicemgr*')[1]
            devicemgr_ls_list =  self.execute('ls -l /Data/devmgr/db')[1]
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['devicemgr is running']
        check_list2 = ['/etc/init.d/devicemgrd', '/var/lib/dpkg/info/devicemgr.md5sums', '/usr/local/devicemgr/devicemgr']
        check_list3 = ['wddevice.ini']
        if all(word in devicemgr_daemon for word in check_list1) and all(word in devicemgr_files_list for word in check_list2) \
                and all(word in devicemgr_ls_list for word in check_list3):
            self.log.info('Verify Device Manager is loaded in firmware PASSED!!')
        else:
            self.log.error('Verify Device Manager is loaded in firmware FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test!!')
            exit()

LoadDeviceManager(checkDevice=False)
