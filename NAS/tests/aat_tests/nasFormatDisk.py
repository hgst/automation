'''
Created on Jan, 8, 2015

@author: Carlos Vasquez
'''

import time
import re
import subprocess
import os
from testCaseAPI.src.testclient import TestClient

VOLUMES4BAY = ['Volume_1', 'Volume_2', 'Volume_3', 'Volume_4']
VOLUMES2BAY = ['Volume_1', 'Volume_2']
VOLUMES1BAY = ['Volume_1']
PUBLICFOLDERS=['Public', 'SmartWare','TimeMachineBackup']
SUBFOLDERS=['Shared\ Music', 'Shared\ Videos', 'Shared\ Pictures']
MOVINGPATH="/mnt/HD/HD_a2/Public/"
FILESIZE=10240

class nasFormatDisk(TestClient):   
    def run(self):       
        self._myCleanup()
        
        # Get number of volumes
        numberVolumes = len(self.get_xml_tags(self.get_volumes()[1], 'volume_id'))
        global folder_names
        generated_file = self.generate_test_file(FILESIZE)
        FILENAME = os.path.basename(generated_file)
        if numberVolumes == 4:
            folder_names = VOLUMES4BAY
        elif numberVolumes == 2:
            folder_names = VOLUMES2BAY
        else:
            folder_names = VOLUMES1BAY
        
        # configure raid (JBOD)
        self._configureJBOD()
        
        self.log.info('Setting up folders\n')
        self.nasFormatDisk_Initial()
        
        self.log.info('Copy files to the unit\n')
        self.nasFormatDisk_FileCopy(generated_file, FILENAME)
        
        self.log.info('Setting max web timeout')
        self.set_Max_UI_timeout()
        
        self.log.info('Formatting volumes\n')
        self.nasFormatDisk_Format(formatVolume='allVolumes')
        
        self.log.info('Check file existance\n')
        self.nasFormatDisk_CheckFile(FILENAME)
        
        # If a single drive then skip the rest of tests
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')
        
        if modelNumber in ('BWAZ', 'BBAZ', 'KC2A','BZVM', 'LT4A', 'BNEZ', 'BWZE'):
            self.log.info('Configuring folders for 2nd volume test\n')
            self.nasFormatDisk_Initial()
            
            self.log.info('Copying files to the unit for 2nd volume test\n')
            self.nasFormatDisk_FileCopy(generated_file, FILENAME)
            
            self.log.info('Formatting 2nd volume\n')
            self.nasFormatDisk_Format(formatVolume='Volume_2')
            
            self.nasFormatDisk_FileRead(FILENAME)
        
    def _myCleanup(self):
        # a quick restore will work faster here.
        self.delete_all_users("['admin','ftp','sshd']")
        self.execute('smbif -b user1')
        self.execute('smbif -b user2') 
        self.delete_all_shares() 
                             
    def _configureJBOD(self):   
        # configure raid (JBOD) if already JBOD skip configuration
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

        if modelNumber in ('BWAZ', 'BBAZ', 'KC2A','BZVM') and (self.get_raid_mode() <> 'JBOD'):  
            self.log.info('Configuring JBOD(2-Bay...')
            self.configure_jbod()   
        elif modelNumber in ('LT4A', 'BNEZ', 'BWZE') and (self.get_raid_mode() <> 'JBOD'):
            self.log.info('Configuring JBOD(4-Bay)...')
            self.configure_jbod()
        else: 
            self.log.info('No raid configuration needed')
            
    def set_Max_UI_timeout(self):
        self.log.info('Set max UI logout timeout')   
        time.sleep(1)
        self.get_to_page('Settings')    
        time.sleep(1)
        self.wait_until_element_is_visible('settings_general_link')
        self.click_element('settings_general_link')   
        self.click_element('settings_generalTimeout_select')
        time.sleep(1)
        self.drag_and_drop_by_offset('css=div.jspDrag', 0, 150)            
        self.click_element('settings_generalTimeoutLi26_select')


    def nasFormatDisk_Initial(self):
        time.sleep(2)
        self.get_to_page('Shares')
        for n in folder_names:
            time.sleep(2)
            self.wait_until_element_is_visible('shares_createShare_button')
            self.click_element('shares_createShare_button')
            time.sleep(1)
            if folder_names != VOLUMES1BAY:
                self.wait_until_element_is_visible('shares_volume_select')
                time.sleep(1)
                self.click_element('shares_volume_select')
                self.click_element('link='+n)
            self.input_text('shares_shareName_text',"test_"+n, False)
            self.input_text('shares_shareDesc_text',n, False)
            time.sleep(1)
            self.click_element('shares_createSave_button')
            time.sleep(4)
            
    def nasFormatDisk_FileCopy(self, generated_file, FILENAME):
        for x in PUBLICFOLDERS:
            status=self.write_files_to_smb(files=generated_file,share_name=x, delete_after_copy=False)
            
        for x in folder_names:
            status=self.write_files_to_smb(files=generated_file,share_name="test_"+x, delete_after_copy=False)

        for t in SUBFOLDERS:
            returnvalue = self.execute("cp " + MOVINGPATH + FILENAME + " " + MOVINGPATH+t)
                
    
    def nasFormatDisk_FileRead(self,FILENAME):
        for p in PUBLICFOLDERS:
            if self.get_file(p, FILENAME) == 0:
                self.log.info('Success: data still intact in {0} share'.format(p))
            else:
                self.log.error('Test Failed: file does not exist, something went wrong while formatting volume 2')

        for x in folder_names:
            if x == 'Volume_2':
                response=str(self.get_file('test_{0}'.format(x), FILENAME))
                if response != 0:
                    self.log.info('Success: Volume 2 has been erased')
                else:
                    self.log.error('Test Failed: file still exists, Volume_2 was not formatted')
            else:                               
                if self.get_file('test_{0}'.format(x), FILENAME) == 0:
                    self.log.info('Success: data still intact in {0} share'.format(x))
                else:
                    self.log.error('Test Failed: file has been deleted from {0} while formatting volume 2'.format(x))

        for t in SUBFOLDERS:                  
            value = self.execute('ls '+MOVINGPATH+t)
            if FILENAME in value[1]:
                self.log.info('Success: data still intact in {0} share'.format(t))
            else:
                self.log.error('Test Failed: file has been deleted from {0} while formatting volume 2'.format(t))
            
    def nasFormatDisk_Format(self, formatVolume='allVolumes'):
        time.sleep(2)
        self.get_to_page('Settings')    
        time.sleep(2)
        self.wait_until_element_is_visible('settings_utilities_link')
        self.click_element('settings_utilities_link')
        time.sleep(2)
        
        if formatVolume == 'Volume_2':
            self.click_element('settings_utilitiesFormatDisk_volume')
            self.click_element("xpath=(//a[contains(text(),'Volume_2')])[2]")
        
        self.wait_until_element_is_visible('settings_utilitiesFormatDisk_button')
        self.click_element('settings_utilitiesFormatDisk_button')      
        time.sleep(1)
    
        self.wait_until_element_is_visible('settings_utilitiesFormatDiskNext5_button')
        self.click_element('settings_utilitiesFormatDiskNext5_button')
        time.sleep(3)
        self.log.info('Formating volumes, please standby...')
        
        # Verify the Format operation shows a progress bar, a percentage of completion, and text
        # stating what volume is being formatted
        if self.is_element_visible('format_progressbar'):
            self.log.info('SUCCESS: progress bar is visible')
        else:
            self.log.error('FAILED: progress bar is not present')
            
        if self.is_element_visible('format_percent'):
            self.log.info('SUCCESS: percentage is visible')
        else:
            self.log.error('FAILED: percentage is not present')
            
        if self.is_element_visible('format_state'):
            self.log.info('SUCCESS: state is visible')
        else:
            self.log.error('FAILED: state is not present')
        
        time.sleep(10)
        # Verify the mke2fs process is running
        output = self.run_on_device('ps -ef |grep mke2fs')
        if 'mke2fs' in output:
            self.log.info('SUCCESS: the mke2fs process is running')
        else:
            self.log.error('FAILED: the mke2fs process is not running')
                    
        while True:
            result=self.is_element_visible('settings_utilitiesFormatDisk_button')
            if result==True:
                self.log.info('Format is complete')
                break
            
    def nasFormatDisk_CheckFile(self, FILENAME):
        for x in PUBLICFOLDERS:
            response=str(self.get_file(x, FILENAME))
            # Verify files were wiped after drive formatting 
            if response != "0":
                self.log.info('SUCCESS: {0} has been wiped clean of all data'.format(x))
            else:
                raise Exception('FAILED: volume was not formatted successfully')    
                
        for x in folder_names:
            response=str(self.get_file('test_{}'.format(x), FILENAME))
            if response != "0":
                self.log.info('SUCCESS: {0} has been wiped clean of all data'.format(x))        
            else:
                raise Exception('FAILED: volume was not formatted successfully')   
                  
        for x in SUBFOLDERS:
            response=str(self.get_file(x, FILENAME))
            if response != "0":
                self.log.info('SUCCESS: {0} has been wiped clean of all data'.format(x))
            else:
                raise Exception('FAILED: volume was not formatted successfully')   
        
        # Verify defaults shares are the only one available
        defaultShares = self.get_xml_tags(self.get_all_shares()[1],'share_name')
        if defaultShares.sort() == PUBLICFOLDERS.sort():
            self.log.info('SUCCESS: all default shares are the only ones available')      
        else:
            raise Exception('Test Failed: only the default shares should exist')
        
        # Verify defaults folders are the only one available
        defaultFolders = self.get_xml_tags(self.get_directory()[1],'name')
        if SUBFOLDERS.sort() == defaultFolders.sort():
            self.log.info('SUCCESS: all default folders are the only ones available')      
        else:
            raise Exception('Test Failed: only default Public folders should exist')

nasFormatDisk()