"""
Created on June 8th, 2015

@author: tran_jas

## @brief User shall able to create remote backup
#
#  @detail
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=33258&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=81&nTP=270424&etab=1

"""


from testCaseAPI.src.testclient import TestClient
import time
from testCaseAPI.src.testclient import Elements as Element

job_name = 'remote_test1'

class RemoteBackup(TestClient):

    def run(self):
        self.log.info('Delete Remote Backup Job: {0}'.format(job_name))
        self.delete_remote_backup_job(job_name)
        self.save_uut_ip()
        self.execute('rm /shares/Public/file*')
        self.create_random_file('/shares/Public/', filename='file0.jpg', blocksize=1024, count=1024)
        hash_public = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.log.info('hash_public md5sum: {0}'.format(hash_public))
        self.create_manual_backup()
        self.compare_file(folder_check=hash_public)

    def tc_cleanup(self):
        self.reset_uut_ip()
        self.log.info('Delete remote backup job')
        self.delete_remote_backup_job(job_name)

    def save_uut_ip(self):
        global org_uut_ip
        self.log.info('Save uut ip')
        org_uut_ip = self.uut[self.Fields.internal_ip_address]
        self.log.info("uut_ip: {}".format(org_uut_ip))

    def reset_uut_ip(self):
        global org_uut_ip
        self.log.info('Reset uut ip dictionary')
        self.uut[self.Fields.internal_ip_address] = org_uut_ip
        self.log.info("uut_ip: {}".format(self.uut[self.Fields.internal_ip_address]))

    def compare_file(self, folder_check=None):
        backup_server_ip = self.uut[self.Fields.ftp_server]
        self.log.info('Remote backup server: {}'.format(backup_server_ip))

        self.uut[self.Fields.internal_ip_address] = backup_server_ip
        self.log.info('Internal IP address is now {0}'.format(self.uut[self.Fields.internal_ip_address]))

        self.ssh_connect()
        self.log.info('Connecting via SSH to verify checksum on reference unit')
        sw_list = self.execute('ls /shares/SmartWare/')
        location = sw_list[1]
        self.log.info('location: {0}'.format(location))
        hash_smartware = self.md5_checksum('/shares/SmartWare/{0}/remote_test1/Public/'.format(location), 'file0.jpg')
        self.log.info("hash_smartware: {}".format(hash_smartware))
        if hash_smartware is not None:
            self.log.info('Create manual remote backup is successful')
        self.execute('pwd')
        self.execute('rm -rf /shares/SmartWare/W*')

        self.start_test('check file md5sum')
        if folder_check == hash_smartware:
            self.pass_test("Matched")
        else:
            self.fail_test("Mismatch")

    def create_manual_backup(self):
        self.start_test('create manual backup')
        try:
            self.get_to_page('Backups')
            self.click_wait_and_check('css=#nav_backups_link > div.menu_icon', Element.BACKUPS_REMOTE)
            self.click_wait_and_check(Element.BACKUPS_REMOTE, Element.BACKUPS_REMOTE_CREATE_JOB)
            self.click_wait_and_check(Element.BACKUPS_REMOTE_CREATE_JOB, Element.BACKUPS_REMOTE_BACKUPS_JOB_NAME)
            self.input_text_check(Element.BACKUPS_REMOTE_BACKUPS_JOB_NAME, job_name)
            self.input_text_check(Element.BACKUPS_REMOTE_BACKUPS_REMOTE_IP, self.uut[self.Fields.ftp_server])
            self.input_text_check(Element.BACKUPS_REMOTE_BACKUPS_REMOTE_PASSWORD, 'fituser')
            self.input_text_check(Element.BACKUPS_REMOTE_BACKUPS_SSH_PASSWORD, self.uut[self.Fields.default_ssh_password])
            self.click_wait_and_check(Element.BACKUPS_REMOTE_BACKUPS_SOURCE_FOLDER, '//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span', timeout=30)
            self.click_wait_and_check('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span', Element.BACKUPS_REMOTE_BACKUPS_SOURCE_FOLDER_OK)
            self.click_wait_and_check(Element.BACKUPS_REMOTE_BACKUPS_SOURCE_FOLDER_OK, Element.BACKUPS_REMOTE_BACKUPS_DESTINATION_FOLDER, timeout=30)
            self.click_wait_and_check(Element.BACKUPS_REMOTE_BACKUPS_DESTINATION_FOLDER, 'css = .LightningCheckbox #backups_rSelShare1_chkbox+span', timeout=20)
            self.click_wait_and_check('css = .LightningCheckbox #backups_rSelShare1_chkbox+span', Element.BACKUPS_REMOTE_BACKUPS_DESTINATION_FOLDER_OK)
            self.click_wait_and_check(Element.BACKUPS_REMOTE_BACKUPS_DESTINATION_FOLDER_OK, Element.BACKUPS_REMOTE_BACKUPS_CREATE_JOB_SAVE)
            self.click_wait_and_check(Element.BACKUPS_REMOTE_BACKUPS_CREATE_JOB_SAVE, Element.BACKUPS_REMOTE_BACKUPS_JOB_DELETE, timeout=120)
            self.pass_test('create manual backup successfully')
        except Exception as e:
            self.fail_test('Failed to create manual backup to {0}'.format(repr(e)))
        finally:
            self.close_webUI()

RemoteBackup()
