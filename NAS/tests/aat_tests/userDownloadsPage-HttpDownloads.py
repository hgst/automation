"""
Created on Mar 16th, 2015

@author: tran_jas

## @brief Regular user shall able to create HTTP download via web UI
#
#  @detail schedule http download job to start in 15 minutes, then click Start button
#
"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

httpFile = 'http://go.microsoft.com/fwlink/?LinkId=324638'
httpFileSum = '1eda16a52b3c855cb80e9de4b0a78eac'
httpFileName = 'EIE11_EN-US_MCM_WIN764.EXE'
my_http_user = 'jasont1eli'
http_password = 'fituser'
successful_text = 'Successful'
http_file_path = '/shares/Public/'
rm_exe = 'rm /shares/Public/*.EXE'


class HttpDownloads(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            self.log.info('httpDownloads started')
            self.create_user(my_http_user, password=http_password)
            time.sleep(5)
            self.http_downloader()
        except Exception, ex:
            raise Exception('Failed to HttpFtpDownloader \n' + str(ex))
        finally:
            self.cleaning()
            self.end()

    def http_downloader(self):

        # Call these methods to accept EULA as admin if needed
        self.access_webUI()
        self.close_webUI()

        self.uut[self.Fields.web_username] = my_http_user
        self.uut[self.Fields.web_password] = http_password

        self.access_webUI()

        if self.is_element_visible('nav_downloads_link'):
            self.log.info('Testing as regular user')
            self.click_element('nav_downloads_link')
            self.click_element('//li[@id=\'http_downloads\']/span')
        else:
            self.log.error('Fail to login as regular user')

        self.click_element('apps_httpdownloadsLoginMethodAnonymous_button')
        time.sleep(3)
        self.input_text('apps_httpdownloadsURL_text', httpFile, True)
        time.sleep(3)
        self.click_element('apps_httpdownloadsTest_button')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('apps_httpdownloadsTestClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successful_text:
            self.log.info('Valid URL was entered: {0}'.format(httpFile))
            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            time.sleep(5)
            self.click_element('home_treeOk_button')
            time.sleep(5)
            if self.is_element_visible('home_treeOk_button'):
                self.click_element('home_treeOk_button')

            # Disable NTP and set date time to 1:45AM
            self.log.info('Disable NTP and set date')
            self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                             time_zone_name='US/Pacific')
            self.execute('date -s 2015.01.05-01:45:00')

            # Schedule download at 2AM (15 minutes schedule)
            self.click_element('f_hour')
            self.click_element('link=2AM')
            self.click_element('f_min')
            self.click_element('link=00')
            self.click_element('apps_httpdownloadsCreate_button')
            time.sleep(3)

            # click Start button
            while self.is_element_visible('css=div.tip.tip_backup_wait > img'):
                self.log.info('Start http download job...')
                self.click_element('//tr[@id=\'row1\']/td[7]/div/a/img')
                time.sleep(10)

            self.log.info("1 minute wait for downloading...")
            time.sleep(60)

            loop_count = 0
            while (self.is_element_visible('css=div.tip.tip_backup_success > img') is False) and (loop_count < 30):
                self.log.info("Waiting for successful icon...")
                loop_count += 1
                time.sleep(60)
                self.click_element('apps_httpdownloads_link')
                time.sleep(10)

            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('http download successful image is displayed')

            time.sleep(30)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(http_file_path, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            self.log.info('Delete http job')
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.log.info('job to be deleted')

                # work around until ITR # 104595 is resolved
                # click Modify button, close error then continue
                if self.is_element_visible('apps_httpdownloadsModify_button'):
                    self.click_element('apps_httpdownloadsModify_button')
                    while self.is_element_visible('popup_ok_button'):
                        self.click_button('popup_ok_button')

                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsDel_button')

                while self.is_element_visible('popup_ok_button'):
                    self.log.info('Failed to select http job, close error message then try again')
                    self.click_button('popup_ok_button')

                    # work around until ITR # 104595 is resolved
                    # click Modify button, close error then continue
                    if self.is_element_visible('apps_httpdownloadsModify_button'):
                        self.click_element('apps_httpdownloadsModify_button')
                        while self.is_element_visible('popup_ok_button'):
                            self.click_button('popup_ok_button')

                    self.click_element('//tr[@id=\'row1\']/td[2]/div')
                    self.click_element('apps_httpdownloadsDel_button')
                    time.sleep(3)

                while self.is_element_visible('popup_apply_button'):
                    self.click_element('popup_apply_button')
                    time.sleep(3)

        else:
            self.log.error('Invalid URL was entered: {0}'.format(httpFile))

    def cleaning(self):
        self.log.info('Delete downloaded file')
        self.execute(rm_exe)
        self.log.info('Resetting user and password')
        self.uut[self.Fields.web_username] = 'admin'
        self.uut[self.Fields.web_password] = ''
        self.log.info('Delete user')
        self.delete_user(my_http_user)
        # Enable NTP and change time
        self.set_date_time_configuration(datetime='1421057768', ntpservice=True, ntpsrv0='time.windows.com',
                                         ntpsrv1='pool.ntp.org', ntpsrv_user='', time_zone_name='US/Pacific')
HttpDownloads()