""" Add Multiple Users - 2nd Set

    @Author: Lee_e
    
    Objective: check if can create 2 set of multiple user with different parameter
    
    Automation: Full
    
    Supported Products:
        All (exlude Glacier on FW-2.01)
    
    Test Status: 
              Local Lightning : Pass (FW: 2.00.215 & 2.00.216)
                    Lightning : Pass (FW: 2.10.105)
                    KC        : Pass (FW: 2.00.215 & 2.00.216)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.105 & 2.00.216)
                              : Yosemite     - PASS (FW: 2.10.105 & 2.00.216)
                              : Kings Canyon - PASS (FW: 2.10.105 & 2.00.216)
                              : Sprite       - PASS (FW: 2.00.216)
              Jenkins Irvine  : Zion         - PASS (FW: 2.10.105 & 2.00.216)
                              : Aurora       - PASS (FW: 2.10.105 & 2.00.216)
                              : Yellowstone  - PASS (FW: 2.10.105 & 2.00.216)
                              : Glacier      - PASS (FW: 2.00.216)
     
""" 
from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.ui_map import Page_Info
import time
import os

username_prefix = 'user'
useraccount_prefix = [1, 3]
usernumber_prefix = 2
user_set_quota = [5, 10]
user_set_password = ['welc0me', 'passw0rd']
picture_counts = 0

class AddMultipleUsers2ndSet(TestClient):
    def run(self):
        user_list = self.get_user_list()
        for i in range(0, len(useraccount_prefix)):
            username = username_prefix + str(useraccount_prefix[i])
            if username in user_list:
                delete_user_command = 'account -d -u {0}'.format(username)
                self.log.info('delete {0}'.format(username))
                self.run_on_device(delete_user_command)
        self.webUI_admin_login(self.uut[self.Fields.default_web_username])
        self.create_multiple_user_from_UI(username_prefix, useraccount_prefix, usernumber_prefix, user_set_password,
                                          user_set_quota)
        
    def tc_cleanup(self):
        self.log.info('delete user')
        self.delete_all_users(use_rest=False) 

    def create_multiple_user_from_UI(self, username_prefix, useraccount_prefix, usernumber_prefix, user_set_password,
                                     user_set_quota):
        self.log.info('*** Create Multiple Users from UI ***')
        for i in range(0, usernumber_prefix):
            try:
                self.click_wait_and_check(Page_Info.info['Users'].link, self.Elements.USERS_ADD_MULTIPLE_USERS_LINK, timeout=60)
                self.click_wait_and_check(self.Elements.USERS_ADD_MULTIPLE_USERS_LINK, self.Elements.USERS_CREATE_MULTIPLE_NEXT1, timeout=60)
                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT1, self.Elements.USERS_CREATE_MULTIPLE_NAME_PREFIX, timeout=60)
                self.log.info('*** Testing status: Add multiple User From UI - create %snd set ***' % (i+1))
                self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_NAME_PREFIX, text=username_prefix)
                self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_ACCOUNT_PREFIX, text=str(useraccount_prefix[i]))
                self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_NUMBER, text=str(usernumber_prefix))
                self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_PASSWORD, text=user_set_password[i])
                self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_PASSWORD_CONFIRM, text=user_set_password[i])
                
                if self.is_checkbox_selected(self.Elements.USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX):
                    self.unselect_checkbox(self.Elements.USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX)

                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT2, self.Elements.USERS_CREATE_MULTIPLE_NEXT3, timeout=60)
                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT3, self.Elements.USERS_CREATE_MULTIPLE_QUOTAS, timeout=60)
                self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_QUOTAS, text=str(user_set_quota[i]))
                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT4, self.Elements.USERS_CREATE_MULTIPLE_SAVE, timeout=60)
                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
                user_list = self.get_user_list()
                count = 0
                for j in range(0, usernumber_prefix):
                    username = username_prefix + str(j+1)
                    if username in user_list:
                        count += 1
                if count == usernumber_prefix:
                    self.log.info('PASS: create {0}nd set of users successfully'.format(i+1))
                else:
                    self.log.error('FAIL: create {0}nd set of users failed'.format(i+1))
            except Exception as e:
                self.take_screen_shot('create_multiple_user_from_UI')
                self.log.debug('Exception:{0} , create {1}nd set of user'.format(repr(e), i+1))

    def get_user_list(self):
        self.log.info('*** get user list ***')
        users = self.execute('account -i user')[1].split()
        
        return users

    def webUI_admin_login(self, username):
        """
        :param username: user name you'd like to login

        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("PASS: Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot()

AddMultipleUsers2ndSet()