'''
Create on May 29, 2015
@Author: vasquez_c
Objective: Test AFP protocol Read/Write Access for BVT project 
'''
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
import time
import os

username='nonadmin'
userpwd='password'

class afpReadWriteAccess(TestClient):

    def run(self):
        
        try:
            sharename = 'Public'
            time.sleep(5)
            afp_mount = self.mount_share(share_name=sharename, protocol=self.Share.afp)
            if afp_mount:
                created_files = self.create_file(filesize=51200, dest_dir_path=afp_mount)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file):
                        self.log.error('FAILED: afp Read/Write test')
                    else:
                        self.log.info('SUCCESS: afp Read/Write test')
        except Exception as ex:
            self.log.exception('afp Read/Write Access FAILED!! Exception: {}'.format(ex))

afpReadWriteAccess()