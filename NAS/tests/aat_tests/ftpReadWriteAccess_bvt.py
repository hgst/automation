""" FTP Read/Write Test - BVT (MA-336)

    @Author: lin_ri
    
    Objective: Verify ftp read/write access
    
    Automation: Full
    
    Supported Products:
        Lightning (No NFS service support)
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon
    
    Test Status: 
              Local Yellowstone: Pass (FW: 2.10.107)
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import global_libraries.wd_exceptions as exceptions
from ftplib import FTP
from ftplib import error_perm
import time
import os
import shutil
import timeit
import tempfile

picturecounts = 0

class FtpReadWriteTestBvt(TestClient):
    def run(self):
        self.log.info('############### FTP Read/Write Test - BVT (MA-336) TEST START ###############')
        try:
            self.test_anonymous_share(sharename='Public')
            # self.test_user_access(username='nonadmin', userpwd='password', sharename='nonadmin')
            # self.test_user_access(username='nonadmin', userpwd='password', sharename='Public')
        except Exception as e:
            self.log.error("FAILED: FTP Read/Write Test - BVT (MA-336) Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: FTP Read/Write Test - BVT (MA-336) Test PASS!!!")
        finally:
            self.log.info("========== (MA-336) Clean Up ==========")
            self.delete_all_shares()
            self.delete_all_users()
            self.delete_all_groups()
            self.log.info('############### FTP Read/Write Test - BVT (MA-336) TEST END ###############')

    def test_user_access(self, username='nonadmin', userpwd='password', sharename='nonadmin', retry=3):
        """
            Verify FTP read/write access for user account on specified share
        """
        try:
            self.delete_all_users(use_rest=False)
        except exceptions.InvalidDeviceState:
            self.log.info("User list is empty")
        self.delete_all_groups()
        self.delete_all_shares()
        self.log.info("******** TEST - Verify {} Share with {} User ********".format(username, username))
        self.get_to_page('Users')
        self.click_wait_and_check('users_createUser_link', 'users_addUserCancel1_button', visible=True)
        self.input_text('users_userName_text', username, do_login=False)
        self.input_text('users_newPW_password', userpwd, do_login=False)
        self.input_text('users_comfirmPW_password', userpwd, do_login=False)
        self.click_wait_and_check('users_addUserSave_button', visible=False)
        self.close_webUI()
        # Grant public access to (sharename) share for (username) user
        self.update_share_network_access(share_name=sharename, username=username, public_access=True)
        # Grant access to anonymous user
        # self.enable_ftpShare_access(share_name=username, access="Anonymous Read / Write")
        tmp_file = self.generate_test_file(102400)
        while retry >= 0:
            try:
                self.write_files_to_ftp(files=tmp_file, username=username, password=userpwd,
                                        share_name=sharename, delete_after_copy=False)
            except Exception as e:
                if retry == 0:
                    self.log.error('FTP Write ERROR: Reach maximum retries due to exception {}'.format(repr(e)))
                    raise
                self.log.info("FTP Write exception: {}, remaining {} retries...".format(repr(e), retry))
                retry -= 1
                time.sleep(1)
            else:
                break

        target, duration = self.read_files_from_ftp(username=username, password=userpwd, files=tmp_file,
                                                    share_name=sharename, delete_after_copy=False)
        self.ftp_hash_check(tmp_file, target)

    def test_anonymous_share(self, sharename='Public', retry=3):
        """
            Verify FTP read/write access for anonymous user on Public share
        """
        self.log.info("Delete all users")
        try:
            self.delete_all_users(use_rest=False)
        except exceptions.InvalidDeviceState:
            self.log.info("User list is empty")
        self.log.info("Delete all groups")
        self.delete_all_groups()
        self.log.info("Delete all shares")
        self.delete_all_shares()
        self.log.info("******** TEST - Verify {} Share with Anonymous User ********".format(sharename))
        self.enable_ftp_share_access(share_name=sharename, access="Anonymous Read / Write")
        tmp_file = self.generate_test_file(102400, prefix='TestBvt')
        # tmp_file = self.generate_test_files(file_count=5, kilobytes=10240)
        while retry >= 0:
            try:
                self.write_files_to_ftp(files=tmp_file, share_name=sharename, delete_after_copy=False)
            except Exception as e:
                if retry == 0:
                    self.log.error('FTP Write ERROR: Reach maximum retries due to exception {}'.format(repr(e)))
                    raise
                self.log.info("FTP Write exception: {}, remaining {} retries...".format(repr(e), retry))
                retry -= 1
                time.sleep(1)
            else:
                break
        # self.read_files_from_ftp(files=tmp_file, share_name=sharename, delete_after_copy=False)
        target, duration = self.read_files_from_ftp(files=tmp_file, share_name=sharename, delete_after_copy=False)
        self.ftp_hash_check(tmp_file, target)

    def ftp_hash_check(self, src, target):
        self.log.info("=========== START - FTP download file hash check ===========")
        if type(src) == str:
            src = [src]
        for s, d in zip(src, target):
            src_hash = self.checksum_files_on_workspace(dir_path=s, method='md5')
            dst_hash = self.checksum_files_on_workspace(dir_path=d, method='md5')
            if src_hash == dst_hash:
                self.log.info("PASS: {}".format(os.path.basename(d)))
            else:
                self.log.error("FAILED: {}".format(os.path.basename(d)))
        self.log.info("=========== END - FTP download file hash check ===========")

    def enable_ftp_service_by_cgi(self, state='ON'):
        """
        :param state: Turn ON or OFF the ftp service
        :return: 1 if success
        """
        access = {
            'ON': 1,
            'OFF': 0
        }
        if state not in access.keys():
            self.log.error("ERROR: Unable to recognize the state information({}), please use ON or OFF.".format(state))
        header = 'app_mgr.cgi?'
        cmd = 'FTP_Server_Enable&f_state={}'.format(access[state])
        result = self._cgi._send_cgi_cmd(cmd, header)
        cgi_result = self.get_xml_tags(result, 'result')
        if '1' in cgi_result:
            self.log.info("CGI: FTP service is set to {} successfully".format(state))
        else:
            self.log.error("CGI ERROR: Fail to set ftp service to {}".format(state))

    ########################################################################################
    #  - Add FTP functionality into AAT-framework                                          #
    #    * enable_ftp_service(), enable_ftp_share_access()                                 #
    #    * connect_ftp()                                                                   #
    #    * read_files_from_ftp(), write_files_to_ftp()                                     #
    ########################################################################################
    def enable_ftp_service(self, retry=3):
        """
            Enable FTP service
        """
        try:
            self.get_to_page('Settings')
            time.sleep(2)
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info('==> Configure ftp')
            self.wait_until_element_is_visible('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                               timeout=15)
            ftp_status_text = None
            while retry >= 0:
                try:
                    ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
                except Exception as e:
                    if retry == 0:
                        self.log.warning("WARN: Fail to enable ftp, switch from UI to cgi mode due to exception: {}".format(repr(e)))
                        self.enable_ftp_service_by_cgi(state='ON')
                        return
                    self.log.info("WARN: failed to get ftp info, due to exception: {}, remaining {} retry".format(retry, repr(e)))
                    retry -= 1
                else:
                    break
            if ftp_status_text == 'ON':
                self.log.info('FTP is enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.info('Turn ON FTP service')
                self.click_wait_and_check('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                          'popup_ok_button', visible=True)
                self.click_wait_and_check('popup_ok_button', visible=False)
                ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
        except Exception as e:
            self.log.exception('Failed: Unable to configure ftp!! Exception: {}'.format(e))
        else:
            if ftp_status_text == 'ON':
                self.log.info("FTP is enabled successfully. Status = {}".format(ftp_status_text))
            else:
                self.log.error("Unable to turn on FTP service, now is set to {}".format(ftp_status_text))
            self.close_webUI()

    def enable_ftp_share_access(self, share_name='Public', access="Anonymous Read / Write"):
        """
            Set access permission to the ftpShare for anonymous user

            @access: Public share access permission for anonymous
        """
        self.enable_ftp_service()
        try:
            self.log.info('Configure FTP share with {}'.format(access))
            self.get_to_page('Shares')
            time.sleep(2)
            # self.click_wait_and_check('nav_shares_link',
            #                           'shares_createShare_button', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.wait_until_element_is_visible('shares_share_{}'.format(share_name), timeout=30)
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            self.log.info('Switch the ftp access to ON')
            self.wait_until_element_is_visible('css=#shares_ftp_switch+span .checkbox_container', timeout=15)
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_wait_and_check('css=#shares_ftp_switch+span .checkbox_container', 'ftp_dev', visible=True)
            self.log.info('ftp service is set to ON')
            time.sleep(2)
            self.click_link_element('ftp_dev', access)
            if self.is_element_visible('shares_ftpSave_button', wait_time=30):
                self.click_wait_and_check('shares_ftpSave_button', visible=False)
            # Waiting for "Updating" to go away
            self.wait_until_element_is_clickable(locator='shares_share_{}'.format(share_name), timeout=90)
            time.sleep(10)  # Wait for applying done
        except Exception as e:
            self.log.error('FAILED: Unable to config {}, exception: {}'.format(share_name, repr(e)))
        else:
            self.log.info("Save {} permission to Anonymous on {} share.".format(access, share_name))
            self.close_webUI()

    def connect_ftp(self, ftp_ip=None, username=None, password=None, retry=3):
        """
            Connect ftp
        """
        ftp_ip = ftp_ip or self.uut[Fields.internal_ip_address]
        useAnonymous = False

        if not username:
            useAnonymous = True

        ftpconn = None
        while True:
            try:
                ftpconn = FTP(ftp_ip)
            except Exception as e:
                if retry == -1:  # No retry to verify the connection
                    raise
                elif retry == 0:
                    self.log.error("Reached maximum retries to make FTP connection.")
                    raise
                elif retry > 0:
                    self.log.info("Remaining {} retries: FTP connection failed, exception: {}, ".format(retry, repr(e)))
                    retry -= 1
                    time.sleep(1)
                    continue
            else:
                if ftpconn:
                    self.log.info("FTP connection to {} is established successfully.".format(ftp_ip))
                    break
                else:
                    retry -= 1

        # Anonymous login
        if useAnonymous:
            try:
                ftpconn.login()
            except Exception as e:
                self.log.info("Anonymous failed to login. exception: {}".format(repr(e)))
                raise
            else:
                self.log.info("Anonymous login is succeeded.")

        # account login
        else:
            try:
                ftpconn.login(username, password)
            except Exception as e:
                self.log.info("(user={}, pwd={}) account failed to login. "
                              "exception: {}".format(username, password, repr(e)))
                raise
            else:
                self.log.info("(user={}, pwd={}) account login succeeded.".format(username, password))

        return ftpconn

    def read_files_from_ftp(self, ftp_ip=None, username=None, password=None, retries=3,
                            files=None, share_name='Public', delete_after_copy=False):
        """
            Download

            1) Make sure you enable ftp
            2) Make sure you enable ftp share access

            share_name = 'Public' if anonymous
        """
        self.log.info("=========== START - FTP download temporary files for read access test ===========")
        try:
            conn = self.connect_ftp(ftp_ip, username, password, retries)
        except Exception as e:
            self.log.error("Unable to setup the ftp connection. Program exit due to exception: {}".format(repr(e)))
            exit(1)
        # Switch 1-level folder
        try:
            conn.cwd(share_name)
        except Exception as e:
            self.log.debug('Available Directories:')
            conn.dir()
            self.log.error("Unable to switch to the share folder: {}, exception: {}".format(share_name, repr(e)))
            raise
        else:
            self.log.info("Switch to {} folder".format(share_name))

        # Get available files under share_name folder
        serverfiles = conn.nlst()

        # Temporary files
        if not files:
            files = []
        if type(files) == str:
            files = [files]

        files = map(os.path.basename, files)  # get filename instead of abspath
        currentDir = os.getcwd()
        if '/tmp' not in currentDir:
            tempDir = tempfile.mkdtemp(dir=currentDir)
            os.chdir(tempDir)

        # Going to download files from ftp
        files_to_transfer = []
        total_filesize = 0
        self.log.info('STARTING TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        start = timeit.default_timer()

        for filename in files:
            try:
                if filename in serverfiles:
                    with open(filename, "wb") as f:
                        def callback(data):
                            f.write(data)
                        resp = conn.retrbinary("RETR "+filename, callback, blocksize=1048576)
                    if '226' in resp:  # File successfully transferred
                        self.log.info("Succeed to save download file to {}".format(os.path.abspath(filename)))
                        files_to_transfer.append(filename)
                        total_filesize += os.stat(filename).st_size
                    else:
                        self.log.error("Upload {} failed".format(os.path.basename(filename)))
                else:
                    self.log.info("{} file does not exist.".format(filename))
            except error_perm:
                self.log.debug('Available Directories:')
                conn.dir()
                raise
            except Exception as e:
                raise Exception("FAILED: ftpReadTest failed, can not download {}, "
                                "exception: {}".format(os.path.basename(filename), repr(e)))

        end = timeit.default_timer()
        duration = end - start
        if duration == 0:
            duration = 1

        conn.close()

        self.log.info('FINISHED TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        self.log.info('Transfer duration: {0} seconds'.format(int(duration)))
        self.log.info('Transferred data: {0} MiB'.format(int(total_filesize / 1048576)))
        self.log.info('Transfer speed: {0} MiB/s'.format(int(total_filesize / (duration * 1048576))))

        if delete_after_copy:
            # Remove temporary folder
            tempDir = os.getcwd()
            if '/tmp' in tempDir:
                self.log.info('Deleting temporary folder: {0}'.format(tempDir))
                shutil.rmtree(tempDir)
                os.chdir('..')

        self.log.info("=========== END - FTP download temporary files for read access test ===========")
        return (files_to_transfer, duration)

    def write_files_to_ftp(self, ftp_ip=None, username=None, password=None, retries=3,
                           files=None, share_name='Public', delete_after_copy=True):
        """
            Upload

            1) Make sure you enable ftp
            2) Make sure you enable ftp share access

            share_name = 'Public' if anonymous
        """
        self.log.info("=========== START - FTP upload temporary files for write access test ===========")
        try:
            conn = self.connect_ftp(ftp_ip, username, password, retries)
        except Exception as e:
            self.log.error("Unable to setup the ftp connection. Program exit due to exception: {}".format(repr(e)))
            exit(1)
        # Switch one-level folder
        try:
            conn.cwd(share_name)
        except Exception as e:
            self.log.debug('Available Directories: ')
            conn.dir()
            self.log.error("Unable to switch to the share folder: {}, exception: {}".format(share_name, repr(e)))
            raise
        else:
            self.log.info("Switch to {} folder".format(share_name))

        # Temporary files
        if not files:
            files = []
        if type(files) == str:
            files = [files]

        files = map(os.path.abspath, files)  # get abspath
        # Going to write files to ftp
        total_filesize = 0
        for file in files:
            absFilename = os.path.abspath(file)
            file_size = os.stat(absFilename).st_size
            total_filesize += file_size

        self.log.info('STARTING TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        start = timeit.default_timer()

        for filename in files:
            try:
                with open(filename, "rb") as f:
                    resp = conn.storbinary("STOR "+os.path.basename(filename), f, 1048576) # 1048576 = 1MB
                if '226' in resp: # File successfully transferred
                    self.log.info("{} is uploaded successfully".format(os.path.basename(filename)))
                else:
                    self.log.error("Upload {} failed".format(os.path.basename(filename)))
            except error_perm:
                self.log.debug('Available Directories: ')
                conn.dir()
                raise
            except Exception as e:
                raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. "
                                "exception: {}".format(os.path.basename(filename), repr(e)))

        end = timeit.default_timer()
        duration = end - start
        if duration == 0:
            duration = 1

        conn.close()

        self.log.info('FINISHED TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        self.log.info('Transfer duration: {0} seconds'.format(int(duration)))
        self.log.info('Transferred data: {0} MiB'.format(int(total_filesize / 1048576)))
        self.log.info('Transfer speed: {0} MiB/s'.format(int(total_filesize / (duration * 1048576))))

        if delete_after_copy:
            # Remove temporary folder
            tempDir = os.getcwd()
            if '/tmp' in tempDir:
                self.log.info('Deleting temporary folder: {0}'.format(tempDir))
                shutil.rmtree(tempDir)
                # Change directory to parent directory
                os.chdir('..')
        self.log.info("=========== END - FTP upload temporary files for write access test ===========")
        return (files, duration)

    ########################################################################################
    #   END OF AAT-Framework                                                               #
    ########################################################################################
    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

FtpReadWriteTestBvt()