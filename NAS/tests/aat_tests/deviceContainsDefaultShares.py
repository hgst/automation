"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify default shares are available on device

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

default_shares = ['Public', 'SmartWare', 'TimeMachineBackup']

class DefaultShares(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            shares = self.get_all_shares()
            all_shares = self.get_xml_tags(shares, 'share_name')

            if set(default_shares).issubset(set(all_shares)):
                for share in default_shares:
                    self.log.info("Default share: {}".format(share))
            else:
                self.log.error("Default shares not correct")
                for share in all_shares:
                    self.log.info("Share: {}".format(share))
        except Exception, ex:
            self.log.exception('Failed to complete default shares test case \n' + str(ex))


DefaultShares()