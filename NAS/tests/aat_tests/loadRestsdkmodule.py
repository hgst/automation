"""
Create on Dec 29, 2015
@Author: lo_va
Objective:  Verify Restsdk module is loaded in firmware
            Verify restsdk daemon is running
            KAM-243 (M2 feature)
wiki URL: http://wiki.wdc.com/wiki/Load_Restsdk_module_in_firmware
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadRestsdkmodule(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        restsdk_daemon = ''
        restsdk_files_list = ''
        restsdk_ls_list = ''
        try:
            restsdk_daemon = self.execute('/etc/init.d/restsdk-serverd status')[1]
            restsdk_files_list = self.execute('find / -name *restsdk*')[1]
            restsdk_ls_list = self.execute('ls -l /Data/restsdk/data/db')[1]
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['restsdk-server is running']
        check_list2 = ['/usr/local/restsdk', '/etc/init.d/restsdk-serverd', '/usr/local/restsdk/restsdk-server']
        check_list3 = ['index.db', 'index.db-shm', 'index.db-wal']
        if all(word in restsdk_daemon for word in check_list1) and all(word in restsdk_files_list for word in check_list2) \
                and all(word in restsdk_ls_list for word in check_list3):
            self.log.info('Verify Restsdk module is loaded in firmware PASSED!!')
        else:
            self.log.error('Verify Restsdk module is loaded in firmware FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test!!')
            exit()

LoadRestsdkmodule(checkDevice=False)
