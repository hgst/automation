""" Shares page - a share profile can be modified (MA-158)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=31793&execView=execDetails&tdetab=3&view=details&pltab=steps&pId=50&nTP=117415&etab=8
        1) Open WebUI and login as system admin
        2) Click on the Shares category
        3) Select a share
        4) Verify all default UI protocol switches are correct
           - FTP: OFF
           - NFS: OFF  (Lightning did not support NFS)
           - WebDAV: OFF
        5) Make changes to:
           Public, Enable Recycling Bin, Enable Media Serving, FTP Access, WebDAV Access, NFS Access
        6) Verify that a share profile can be modified
           Share profile is changed

    Automation: Full
    TestType: ART

    Note:
    Hardcode to check if Glacier FW is 2.00 or not.
    If Glacier FW is not 2.00.xxx, it is de-feature fw. So, skip unsupported recyclebin and webdav check.

    Support Product:
        Lightning  (Does not support NFS service)
        Glacier   (Does not have global NFS settings, Does not support WebDAV service)
        Glacier de-feature (Does not support recycle bin)
        YellowStone
        Yosemite
        Sprite
        Aurora
        Kings Canyon


    Test Status:
        Local Yosemite: N/A (FW: 2.00.201)
"""
from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.ui_map import Elements
from global_libraries.testdevice import Fields
from global_libraries.performance import Stopwatch
import global_libraries.wd_exceptions as exceptions
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import os

TCNAME = 'Shares page - a share profile can be modified (MA-158)'
shareName = 'ma158'

products_info = {
        'LT4A': 'Lightning',
        'BNEZ': 'Sprite',
        'BWZE': 'Yellowstone',
        'BWAZ': 'Yosemite',
        'BBAZ': 'Aurora',
        'KC2A': 'KingsCanyon',
        'BZVM': 'Zion',
        'GLCR': 'Glacier',
        'BWVZ': 'GrandTeton',
}

noNfsSupportModel = ('LT4A')
noWebDavModel = ('GLCR')

picturecounts = 0

class sharesPageShareProfileCanBeModified(TestClient):
    def run(self):
        self.log.info('######################## {} TEST START ##############################'.format(TCNAME))
        try:
            self.systemOnlyRestore()
            self.accept_eula()
            self.log.info("Delete all users")
            try:
                self.delete_all_users(use_rest=False)
            except exceptions.InvalidDeviceState:
                self.log.info("User list is empty")
            self.log.info("Delete all groups")
            self.delete_all_groups()
            self.log.info("Delete all shares")
            self.delete_all_shares()
            self.create_shares(number_of_shares=1, share_name=shareName, force_webui=True)
            self.log.info("====== VERIFY DEFAULT SWITCH ======")
            self.verify_default_switches(share_name=shareName)
            self.log.info("====== Enable media serving share ======")
            self.enable_share_media_serving(share_name=shareName)
            self.log.info("====== Enable ftp share access ======")
            self.enable_ftp_share_access(share_name=shareName, access='Anonymous Read / Write')
            self.log.info("====== Enable nfs share access ======")
            self.enable_nfs_share_access(share_name=shareName)
            self.log.info("====== Enable webdav share access ======")
            self.enable_webdav_share_access(share_name=shareName)
            self.log.info("====== Enable recycle bin ======")
            self.enable_share_recyclebin(share_name=shareName)
            self.log.info("====== Disable public access ======")
            self.toggle_public_switch(share_name=shareName)  # disable public switch
            self.log.info("====== VERIFY CHANGE SWITCH ======")
            self.verify_change_switches(share_name=shareName)
        except Exception as e:
            self.log.error("FAILED: {} Test Failed!!, exception: {}".format(TCNAME, repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: {} Test PASS!!!".format(TCNAME))
        finally:
            self.log.info("========== (MA-158) Clean Up ==========")
            self.delete_all_shares()
            self.log.info('######################## {} TEST END ##############################'.format(TCNAME))

    def verify_change_switches(self, share_name='Public'):
        self.log.info("Verify share profile can be modified")
        try:
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            switches = {
                'public': 'OFF',
                'recycle': 'ON',
                'media': 'ON',
                'ftp': 'ON',
                'webdav': 'ON',
                'nfs': 'ON'
            }
            product_model = self.get_model_number()
            for switch_name, status in switches.items():
                if switch_name == 'nfs':
                    if product_model in noNfsSupportModel:  # Lightning
                        self.log.info("{} model does not support NFS service, skip NFS test!".format(product_model))
                        continue
                if switch_name == 'webdav':  # Glacier
                    if product_model in noWebDavModel:
                        self.log.info("{} model does not support WebDAV service, skip WebDAV test!".format(product_model))
                        continue
                if switch_name == 'recycle':
                    if product_model in ('GLCR'):   # Glacier
                        device_fw = self.get_firmware_version_number()
                        if device_fw.rsplit('.', 1)[0] != '2.00':  # 2.00.116 --> 2.00
                            self.log.info("Glacier de-feature does not support Recycle Bin, FW={}".format(device_fw))
                            continue
                result = self.get_share_switch(name=switch_name, state=status)
                if result == 1:
                    raise Exception("FAILED: {} switch verification failed".format(switch_name))
        except Exception as e:
            self.takeScreenShot(prefix='verifyChangeSwitch')
            self.log.error('FAILED: Fail to verify the modified profile on {} share, '
                           'exception: {}'.format(share_name, repr(e)))

    def get_share_switch(self, name=None, state='ON'):
        """
            Verify the switch status
        :param name: Switch name
        :param state: ON or OFF
        :return: True when share switch matches the state being passed in
        """
        self.log.info("VERIFY: {} switch is {}".format(name, state))
        switch_status = None
        timer = Stopwatch(timer=300)
        timer.start()
        while True:
            try:
                switch_status = self.get_text('css=#shares_{}_switch+span .checkbox_container'.format(name))
            except Exception as e:
                if timer.is_timer_reached():
                    self.log.info("WARN: Reach the maximum 5 min to retry")
                    break
                self.log.info("{} switch fail to get the status due to exception: {}, "
                              "going to retry".format(repr(e), name))
                continue
            else:
                break
        if switch_status == state:
            self.log.info("PASS: {} switch is set to {}".format(name, switch_status))
            return 0
        else:
            self.log.warning("ERROR: {} switch is set to {}".format(name, switch_status))
            return 1

    def toggle_public_switch(self, share_name='Public'):
        """
            Turn off the public switch status
        """
        try:
            self.log.info('Configure Public access on {} share with OFF state'.format(share_name))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            self.log.info('Switch Public access to OFF')
            self.click_wait_and_check('css=#shares_public_switch+span .checkbox_container',
                                      'css=#shares_public_switch+span .checkbox_container .checkbox_off', visible=True)
            self.log.info("Check Public access state")
            public_status = self.get_text('css=#shares_public_switch+span .checkbox_container')
            self.log.info("Public access service is set to {}".format(public_status))
            time.sleep(2)
        except Exception as e:
            self.takeScreenShot(prefix='enablePublicAccess')
            self.log.error('FAILED: Unable to config public access on {}, exception: {}'.format(share_name, repr(e)))
        else:
            # self.close_webUI()
            self.reload_page()

    def enable_share_recyclebin(self, share_name='Public'):
        product_model = self.get_model_number()
        try:
            if product_model in noWebDavModel:
                device_fw = self.get_firmware_version_number()
                if device_fw.rsplit('.', 1)[0] != '2.00':  # 2.00.116 -> 2.00
                    self.log.info("Glacier de-feature does not support Recycle Bin, FW={}".format(device_fw))
                    return
        except Exception as e:
            self.log.error("ERROR: Fail to check Recycle bin for glacier de-feature, exception: {}".format(repr(e)))
        try:
            self.log.info('Configure Recycle Bin on {} share'.format(share_name))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            self.log.info('Switch Recycle Bin access to ON')
            self.wait_until_element_is_visible('css=#shares_recycle_switch+span .checkbox_container', timeout=15)
            recycle_status = self.get_text('css=#shares_recycle_switch+span .checkbox_container')
            if recycle_status == 'ON':
                self.log.info("Recycle Bin has been turned to ON, status = {}".format(recycle_status))
            else:
                self.click_wait_and_check('css=#shares_recycle_switch+span .checkbox_container',
                                          'css=#shares_recycle_switch+span .checkbox_container .checkbox_on', visible=True)
            recycle_status = self.get_text('css=#shares_recycle_switch+span .checkbox_container')
            self.log.info("Recycle Bin service is set to {}".format(recycle_status))
            time.sleep(2)
        except Exception as e:
            self.takeScreenShot(prefix='enableRecycleBinAccess')
            self.log.error('FAILED: Unable to config recycle bin on {}, exception: {}'.format(share_name, repr(e)))
        else:
            # self.close_webUI()
            self.reload_page()

    def enable_webdav_share_access(self, share_name='Public'):
        """
            Enable webdav access on share

            Glacier does not support WebDAV service
        """
        product_model = self.get_model_number()
        if product_model in noWebDavModel:
            self.log.info("{} model does not support WebDAV service, skip WebDAV enable share access!".format(product_model))
            return
        self.enable_webdav_service()
        try:
            self.log.info('Configure WebDAV on {} share'.format(share_name))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            self.log.info('{}: Switch the WebDAV access to ON'.format(share_name))
            self.wait_until_element_is_visible('css=#shares_webdav_switch+span .checkbox_container', timeout=15)
            webdav_status = self.get_text('css=#shares_webdav_switch+span .checkbox_container')
            if webdav_status == 'ON':
                self.log.info("{}: WebDAV has been turned to ON, status = {}".format(share_name, webdav_status))
            else:
                self.click_wait_and_check('css=#shares_webdav_switch+span .checkbox_container',
                                          'css=#shares_webdav_switch+span .checkbox_container .checkbox_on', visible=True)
            webdav_status = self.get_text('css=#shares_webdav_switch+span .checkbox_container')
            self.log.info("{}: WebDAV service is set to {}".format(share_name, webdav_status))
            time.sleep(2)
        except Exception as e:
            self.log.error('FAILED: Unable to config webdav on {}, exception: {}'.format(share_name, repr(e)))
            self.takeScreenShot(prefix='enableWebDavAccess')
        else:
            # self.close_webUI()
            self.reload_page()

    def enable_webdav_service(self):
        """
            Enable WebDAV service
        """
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('settings_network_link',
                                      'css=#settings_networkFTPAccess_switch+span .checkbox_container', visible=True)
            self.log.info('==> Configure WebDAV')
            self.wait_until_element_is_visible('css=#settings_networkWebdav_switch+span .checkbox_container',
                                               timeout=15)
            webdav_status = self.get_text('css=#settings_networkWebdav_switch+span .checkbox_container')
            if webdav_status == 'ON':
                self.log.info('WebDAV is enabled, status = {0}'.format(webdav_status))
            else:
                self.log.info('Turn ON WebDAV service')
                self.click_wait_and_check('css=#settings_networkWebdav_switch+span .checkbox_container',
                                          'css=#settings_networkWebdav_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
                if self.is_element_visible('popup_ok_button', wait_time=30):
                    self.click_wait_and_check('popup_ok_button', visible=False)
                webdav_status = self.get_text('css=#settings_networkWebdav_switch+span .checkbox_container')
        except Exception as e:
            self.takeScreenShot(prefix='enable_webdav_service')
            self.log.exception('Failed: Unable to configure WebDAV!! Exception: {}'.format(e))
        else:
            if webdav_status == 'ON':
                self.log.info("WebDAV is enabled successfully. Status = {}".format(webdav_status))
            else:
                self.log.error("Unable to turn on WebDAV service, now is set to {}".format(webdav_status))

    def enable_share_media_serving(self, share_name='Public'):
        """
            Enable Media Streaming first, then enable Media serving for the share
        """
        self.toggle_media_streaming(enable_streaming=True)
        self.enable_media_serving(share_name=share_name)
        self.close_webUI()

    def enable_media_serving(self, share_name):
        """
            Enable Media Serving

            @share_name: name of the share to enable media serving to
        """
        try:
            self.log.info('Configure Media Serving: {0}'.format(share_name))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            time.sleep(2)
            self.log.info('Switching media serving ON')
            media_serving_status = self.get_text('css=#shares_media_switch+span .checkbox_container')
            if media_serving_status == 'ON':
                self.log.info("media serving was ON, status = {}".format(media_serving_status))
            else:
                self.click_wait_and_check('css=#shares_media_switch+span .checkbox_container',
                                          'css=#shares_media_switch+span .checkbox_container .checkbox_on', visible=True)
            media_serving_status = self.get_text('css=#shares_media_switch+span .checkbox_container')
            self.log.info('media serving is set to {}'.format(media_serving_status))
            time.sleep(2)
        except Exception as e:
            self.takeScreenShot(prefix='enableMediaServing')
            self.log.error("FAILED: Media Serving was not enabled. exception: {}".format(repr(e)))

    def toggle_media_streaming(self, enable_streaming=True):
        """
            Enable/Disable Media Streaming
        """
        try:
            self.click_element(Elements.SETTINGS_MEDIA)
            media_status_text = self.get_text('css = #settings_mediaDLNA_switch+span .checkbox_container')
            if enable_streaming:
                if media_status_text == 'ON':
                    self.log.info('Media streaming is enabled, status = {0}'.format(media_status_text))
                else:
                    self.log.info('Turning ON Media streaming')
                    self.click_wait_and_check('css=#settings_mediaDLNA_switch+span .checkbox_container',
                                              'css=#settings_mediaDLNA_switch+span .checkbox_container .checkbox_on',
                                              visible=True)
                    self.click_wait_and_check('popup_ok_button', visible=False)
                    self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)
                    self.log.info('Media streaming was turned ON')
            else:
                if media_status_text == 'OFF':
                    self.log.info('Media streaming is disabled, status = {0}'.format(media_status_text))
                else:
                    self.log.info('Turning OFF Media streaming')
                    self.click_wait_and_check('css=#settings_mediaDLNA_switch+span .checkbox_container',
                                              'css=#settings_mediaDLNA_switch+span .checkbox_container .checkbox_off',
                                              visible=True)
                    self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)
                    self.log.info('Media streaming was turned OFF')
        except Exception as e:
            self.takeScreenShot(prefix='toggleMediaStreaming')
            self.log.error('FAILED: Could not enable Media Streaming. exception: {}'.format(repr(e)))

    def verify_default_switches(self, share_name='Public'):
        try:
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            time.sleep(2)
            self.log.info("****** Verify Default Share Access Settings *******")
            switches = (
                'ftp',
                'webdav',
                'nfs'
            )
            # Verify NFS, WebDAV and NFS default settings
            product_model = self.get_model_number()
            for switch_name in switches:
                if switch_name == 'nfs':
                    if product_model in noNfsSupportModel:  # Lightning
                        self.log.info("{} model does not support NFS service, skip NFS test!".format(product_model))
                        continue
                if switch_name == 'webdav':  # Glacier
                    if product_model in noWebDavModel:
                        self.log.info("{} model does not support WebDAV service, skip WebDAV test!".format(product_model))
                        continue
                # Default settings is disabled for v2.10.2xxx
                if not self._sel.is_enabled("css=#shares_{}_switch+span .checkbox_container".format(switch_name), timeout=10):
                    self.log.info("PASS: {} default setting is disabled due to global settings off".format(switch_name))
                else:
                    default_value = self.get_text("css=#shares_{}_switch+span .checkbox_container".format(switch_name))
                    if product_model == 'GLCR' and default_value == 'OFF':
                        self.log.info("PASS: {} default is {} due to global setting(off)".format(switch_name, default_value))
                    else:
                        self.log.error("FAILED: {} default setting is {} not disabled".format(switch_name, default_value))
                # Default settings is OFF for v2.00
                # default_value = self.get_text("css=#shares_{}_switch+span .checkbox_container".format(switch_name))
                # if default_value == value:
                #     self.log.info("PASS: {} default setting is set to {}".format(switch_name, default_value))
                # else:
                #     self.log.error("FAILED: {} default setting is set to {}".format(switch_name, default_value))
        except Exception as e:
            self.log.error("ERROR: Share Access default settings verification failed, exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            self.takeScreenShot(prefix='verifyDefault')
        else:
            self.log.info("PASS: share access pass default setting test")
        finally:
            # self.close_webUI()
            self.reload_page()

    def systemOnlyRestore(self, retry=3):
        restore_cmd = 'kill_running_process ; load_default 1 ; touch /var/www/system_boot.html'
        reboot_cmd = '/usr/sbin/do_reboot'
        self.log.info("Going to do the System Only restore process...")
        try:
            self.execute(command=restore_cmd)
            time.sleep(20)
            self.execute(command=reboot_cmd)
            self.log.info("Waiting 3 minutes to restore the system configuration...")
            time.sleep(180)  # 3min
            while retry >= 0:
                try:
                    self.set_ssh(enable=True)
                except Exception as e:
                    if retry == 0:
                        raise
                    self.log.info("WARN: Unable to set ssh due to exception: {}, "
                                  "remaining {} retry".format(repr(e), retry))
                    retry -= 1
                    time.sleep(10)
                    continue
                else:
                    break
            self.accept_eula(use_UI=False)
            time.sleep(10)
        except Exception as e:
            self.log.error("ERROR: System Only restore failed due to exception: {}".format(repr(e)))
        else:
            self.log.info("System Only restore completed")

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

    def enable_nfs_service(self):
        """
            Enable NFS
        """
        try:
            self.click_element(Elements.NETWORK_BUTTON)
            time.sleep(3)
            self.log.info('Enable NFS Service')
            nfs_status_text = self.get_text('css = #settings_networkNFS_switch+span .checkbox_container')
            if nfs_status_text == 'ON':
                self.log.info('NFS is enabled, status = {0}'.format(nfs_status_text))
            else:
                self.log.info('Turning ON NFS service')
                self.wait_until_element_is_clickable("css=#settings_networkNFS_switch+span .checkbox_container")
                self.click_wait_and_check('css=#settings_networkNFS_switch+span .checkbox_container',
                                          'css=#settings_networkNFS_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
                nfs_status_text = self.get_text('css = #settings_networkNFS_switch+span .checkbox_container')
                self.log.info("NFS service is set to {}".format(nfs_status_text))
            time.sleep(2)
        except Exception as e:
            self.log.exception('FAILED: Unable to enable nfs service!! Exception: {}'.format(repr(e)))

    def enable_nfs_share_access(self, share_name='Public', access='write'):
        """
            Configure nfsShare to the access value passed by the argument

            @share_name: name of the share to enable nfs access to
            @access: The access permission of nfs Share
        """
        productName = self.uut[Fields.product]

        if productName == 'Lightning':
            self.log.info("{} does not support NFS service, skip NFS enable".format(productName))
            return
        elif productName == 'Glacier':
            self.log.info('{} supports NFS service, but does not need to be set in the settings'.format(productName))
        else:
            self.enable_nfs_service()
        try:
            self.log.info('Configure NFS {0} with {1}'.format(share_name, access))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'css=#nav_shares.current', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_share_{}.LightningSubMenuOn'.format(share_name), visible=True)
            self.log.info('Switch the nfs to ON')
            nfs_status = self.get_text('css=#shares_nfs_switch+span .checkbox_container')
            if nfs_status == 'ON':
                self.log.info("nfs has been turned to ON, status = {}".format(nfs_status))
            else:
                self.click_wait_and_check('css=#shares_nfs_switch+span .checkbox_container',
                                          "css=#shares_nfs_switch+span .checkbox_container .checkbox_on", visible=True)
            time.sleep(2)
            self.click_wait_and_check('shares_editNFS_link',
                                      'css=#shares_write_switch+span .checkbox_container', visible=True)
            time.sleep(2)
            nfsWrite_status = self.get_text('css=#shares_write_switch+span .checkbox_container')
            if nfsWrite_status == 'ON':
                self.log.info("nfs Write access has been turned to ON, status = {}".format(nfsWrite_status))
            else:
                self.click_wait_and_check('css=#shares_write_switch+span .checkbox_container',
                                          'css=#shares_write_switch+span .checkbox_container .checkbox_on', visible=True)
            self.click_wait_and_check('shares_editNFSSave_button', visible=False, timeout=20)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)  # waiting for Updating done
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. exception: {}".format(repr(e)))
            raise

sharesPageShareProfileCanBeModified()