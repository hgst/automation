""" 
    Block FTP Access
    - Permanent Block
    - Temporary Block

    @Author: lin_ri
    wiki URL: http://wiki.wdc.com/wiki/FTP
    
    Automation: Full
    
    Test Status:
              Local Ligtning: Pass (FW: 1.05.30)
        TW Jenkins Lightning: Pass (FW: 1.05.30)

""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import xml.etree.ElementTree as ET
from ftplib import FTP
from ftplib import error_perm
import time
import os
import shutil
import subprocess

# To check if block ip setting is cleared or not in tc_cleanup()
cleanUpAction = False

picturecounts = 0

class blockFTPAccess(TestClient):
    
    def run(self):

        self.log.info('######################## Block FTP Access TESTS START ##############################')
        try:
            self.delete_all_shares()  # Clean all shares except Public, SmartWare and TimeMachineBackup
            self.configure_ftp()
            self.clear_ip_settings()
            # Permanent Block
            self.log.info("=== TEST 1: START Permanent Block Test ===")
            self.config_public_share(access="Anonymous Read / Write")
            time.sleep(3)
            self.test_anonymous_w_access(folder='Public')
            self.config_block_ip(access="Permanent")
            self.test_block_ip()
            self.clear_block_ip()
            self.test_anonymous_w_access(folder='Public')
            self.log.info("=== TEST 1: END Permanent Block Test ===")
            # Temporary Block
            self.log.info("=== TEST 2: START Temporary Block Test ===")
            self.configure_ftp()
            self.config_public_share(access="Anonymous Read / Write")
            time.sleep(3)
            self.test_anonymous_w_access(folder='Public')
            self.config_block_temp_ip(access="Temporary", delay="5 minutes")
            self.test_block_ip()
            self.log.info("Waiting for 5 minutes timeout to retry the access")
            time.sleep(310) # wait for 5 minutes
            self.log.info("Verify the access after 5 minutes")
            self.test_anonymous_w_access(folder='Public')
            self.config_block_temp_ip(access="Temporary", delay="1 Hour")
            self.test_block_ip()
            self.clear_block_ip()
            time.sleep(3)
            self.test_anonymous_w_access(folder='Public')
            self.close_webUI()
            self.log.info("=== TEST 2: END Temporary Block Test ===")
        except Exception as e:
            self.log.error("ERROR: Failed block ftp access test, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        self.log.info('######################## Block FTP Access TESTS END ##############################')

    def tc_cleanup(self):
        """
            Clean up action
        """
        global cleanUpAction
        self.log.info("******** Test Case Clean Up ********")
        if cleanUpAction:
            self.clear_block_ip()
        else:
            self.log.info("TC is clean!")

    def clear_ip_settings(self):
        """ 
            Clear block ip and maximum user back to 10
        """
        self.log.info("==> Clear block ip and maximum user back to 10")
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
        self.click_wait_and_check('network',
                                  'css=#network.LightningSubMenuOn', visible=True)
        time.sleep(3)
        self.log.info("==> Click configure")
        self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)',
                                  'FTPSet_f_maxuser', visible=True, timeout=5)
        # Set Maximum User to 10 (default)
        self.wait_until_element_is_visible('FTPSet_f_maxuser', 30)
        max_num = self.get_text('FTPSet_f_maxuser')
        if int(max_num) < 5:
            self.log.info("Set maximum user to 10")
            self.wait_and_click('css=#FTPSet_f_maxuser', timeout=5)
            self.drag_and_drop_by_offset('css=div.jspDrag', 0, yoffset=80)
            time.sleep(1)
            self.wait_and_click('link=10', timeout=5)
            time.sleep(1)
        # End of set maximum user
        self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button',
                                  'css=#settings_networkFTPAccessNext2_button', visible=True, timeout=5)
        self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button',
                                  'css=#settings_networkFTPAccessNext3_button', visible=True, timeout=5)
        self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button',
                                  'css=#settings_networkFTPAccessNext4_button', visible=True, timeout=5)
        while self.is_element_visible('css=a.del', 30):
            self.log.info("Clear block ip address")
            self.wait_and_click("css=a.del", timeout=5)
            time.sleep(1)
        time.sleep(3)
        self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False, timeout=30)
        self.log.info("SetUP clean action completes before kicking off the test")
        self.wait_for_updating_complete(interval=3)
        time.sleep(2)   # Waiting for updating
    
    def configure_ftp(self):
        """
            Configure FTP setting to ON
        """
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info('==> Configure ftp')
            self.wait_until_element_is_visible('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                               timeout=15)
            ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
            if ftp_status_text == 'ON':
                self.log.info('FTP is enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.info('Turn ON FTP service')
                self.click_wait_and_check('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                          'popup_ok_button', visible=True)
                self.click_wait_and_check('popup_ok_button', visible=False)
                ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
        except Exception as e:
            self.log.exception('Failed: Unable to configure ftp!! Exception: {}'.format(e))
            self.takeScreenShot(prefix='enable_ftp_service')
        else:
            if ftp_status_text == 'ON':
                self.log.info("FTP is enabled successfully. Status = {}".format(ftp_status_text))
            else:
                self.log.error("Unable to turn on FTP service, now is set to {}".format(ftp_status_text))
            # self.close_webUI()
            self.reload_page()

    def config_public_share(self, share_name='Public', access="Anonymous None"):
        """
            Set access permission to the public share for anonymous user
            
            @access: Public share access permission for anonymous
        """
        try:
            self.log.info('Configure FTP share with {}'.format(access))
            self.get_to_page('Shares')
            time.sleep(2)
            # self.click_wait_and_check('nav_shares_link',
            #                           'shares_createShare_button', visible=True)
            self.log.info('Click {}'.format(share_name))
            self.wait_until_element_is_visible('shares_share_{}'.format(share_name), timeout=30)
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            self.log.info('Switch the ftp access to ON')
            self.wait_until_element_is_visible('css=#shares_ftp_switch+span .checkbox_container', timeout=15)
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_wait_and_check('css=#shares_ftp_switch+span .checkbox_container', 'ftp_dev', visible=True)
            self.log.info('ftp service is set to ON')
            time.sleep(2)
            self.click_link_element('ftp_dev', access)
            if self.is_element_visible('shares_ftpSave_button', wait_time=30):
                self.click_wait_and_check('shares_ftpSave_button', visible=False)
            # Waiting for "Updating" to go away
            self.wait_until_element_is_clickable(locator='shares_share_{}'.format(share_name), timeout=90)
            time.sleep(10)  # Wait for applying done
        except Exception as e:
            self.log.error('FAILED: Unable to config {}, exception: {}'.format(share_name, repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            self.takeScreenShot(prefix='enableftpShareaccess')
        else:
            self.log.info("Save {} permission to Anonymous on {} share.".format(access, share_name))
            # self.close_webUI()
            self.reload_page()

    def test_anonymous_w_access(self, folder='Public'):
        """
            Verify the write access permission if disabled
            
            @folder: the folder name which anonymous user browses
        """
        self.log.info("Testing anonymous write access to Public folder...")
        retry = 3
        ftpconn = None
        while retry > 0:
            try:
                ftpconn, ex = self.ftpConnection(anonymous=True)
                if ex:
                    raise ex
            except Exception as e:
                retry -= 1
                self.log.info("Retrying {} time".format(3-retry))
                if retry == 0:
                    self.log.error("Failed 3 retries to establish FTP connection, exception: {}".format(repr(e)))
                    raise
                time.sleep(1)  # Wait 1 sec before next retry
                continue
            else:
                break
        try:
            self.ftpWriteTest(ftpconn, share_folder=folder, delete_after_upload=True)
        except (error_perm, OSError) as e:
            self.log.info("PASS: Anonymous can not write files via FTP, error: {}".format(repr(e)))
        except Exception as e:
            self.log.error(repr(e))

    def getstatusoutput(self, cmd):
        """
            @cmd: system command you'd like to execute
        """
        pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
        output = "".join(pipe.stdout.readlines())
        sts = pipe.returncode
        if sts is None:
            sts = 0
        return sts, output

    def get_local_ip(self):
        """
            Acquire local ip
        """
        cmdstring = 'ifconfig | grep 192.168'
        status, output = self.getstatusoutput(cmdstring)
        ips = output.strip().split()
        if ips[0].strip() == 'inet':
            ip = ips[1].strip()
        else:
            ip = None
        return ip

    def config_block_ip(self, local_ip="", access="Permanent"):
        """ 
            Setting block ip to local ip
            
            @local_ip: Specify one or use the local ip
            @access: The property of IP you want to block
        """
        global cleanUpAction
        if local_ip == "":
            local_ip = self.get_local_ip()
            if local_ip == None:
                self.log.error("FAILED: unable to get local ip. ip: {}".format(local_ip))
        self.log.info("==> Configure block ip: {}".format(local_ip))
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            time.sleep(3)
            self.log.info("==> Click configure")
            self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)',
                                      'css=#settings_networkFTPAccessNext1_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button',
                                      'css=#settings_networkFTPAccessNext2_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button',
                                      'css=#settings_networkFTPAccessNext3_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button',
                                      'css=#settings_networkFTPAccessNext4_button', visible=True, timeout=5)
            self.log.info("Setting block ip address")
            self.input_text('settings_networkFTPAccessBlockIP_text', local_ip, False)
            self.log.info("Select "+access)
            self.click_link_element('css=#FTPBlockIP_Set_Type > span._text', access, timeout=5)
            # self.wait_and_click("css=#FTPBlockIP_Set_Type > span._text", timeout=5)
            # self.wait_and_click("link="+access)
            self.click_button("settings_networkFTPAccessBlockIPSave_button")
            time.sleep(2)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False, timeout=30)
            self.log.info("Block IP is set to {} ".format(local_ip))
            self.wait_for_updating_complete(interval=3)
            time.sleep(2)  # Waiting for updating
            cleanUpAction = True
        except Exception as e:
            self.takeScreenShot(prefix='configBlockIp')
            self.log.error("ERROR: Failed to config block ip, exception: {}".format(repr(e)))

    def test_block_ip(self):
        """
            Verify the block ip access
        """
        local_ip = self.get_local_ip()
        ftp_ip = self.uut[Fields.internal_ip_address]
        self.log.info("==> Testing local ip: {}, to FTP: {}".format(local_ip, ftp_ip))
        try:
            ftpconn, ex = self.ftpConnection(anonymous=True)
            if ex:
                raise ex
        except EOFError as ex:
            self.log.info("PASS: [{}] Unable to connect to FTP [{}], exception: {}".format(local_ip, ftp_ip, repr(ex)))
        except Exception as ex:
            self.log.error("FAILED: Test Block IP failed, exception: {}".format(repr(e)))
    
    def clear_block_ip(self):
        """ 
            Clear block ip
        """
        global cleanUpAction
        local_ip = self.get_local_ip()
        self.log.info("==> Clear block ip: {}".format(local_ip))
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            time.sleep(3)
            self.log.info("==> Click configure")
            self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)',
                                      'css=#settings_networkFTPAccessNext1_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button',
                                      'css=#settings_networkFTPAccessNext2_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button',
                                      'css=#settings_networkFTPAccessNext3_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button',
                                      'css=#settings_networkFTPAccessNext4_button', visible=True, timeout=30)
            while self.is_element_visible('css=a.del', 30):
                self.log.info("Clear block ip address")
                self.wait_and_click("css=a.del", timeout=5)
                time.sleep(1)
            time.sleep(3)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False, timeout=30)
            self.log.info("Clear Block IP: {}".format(local_ip))
            self.wait_for_updating_complete(interval=3)
            time.sleep(2)  # Wait for updating
            cleanUpAction = False
        except Exception as e:
            self.log.error('ERROR: Failed to clear block IP')
            self.takeScreenShot(prefix='clearBlockIp')

    def config_block_temp_ip(self, local_ip="", access="Temporary", delay="5 minutes"):
        """ 
            Setting block ip to local ip
            
            @local_ip: Specify one or use the local ip
            @access: Set to Temporary
            @delay: how long this ip will be blocked
        """
        if local_ip == "":
            local_ip = self.get_local_ip()
        self.log.info("==> Configure block ip: {}".format(local_ip))
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            time.sleep(3)
            self.log.info("==> Click configure")
            self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)', 'css=#settings_networkFTPAccessNext1_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button', 'css=#settings_networkFTPAccessNext2_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button', 'css=#settings_networkFTPAccessNext3_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button', 'css=#settings_networkFTPAccessNext4_button', visible=True, timeout=5)
            self.log.info("Setting block ip address")
            self.input_text('settings_networkFTPAccessBlockIP_text', local_ip, False)
            self.log.info("Select "+access)
            self.click_link_element('css=#FTPBlockIP_Set_Type > span._text', access, timeout=5)
            # self.wait_and_click("css=#FTPBlockIP_Set_Type > span._text", timeout=5)
            # self.wait_and_click("link="+access, timeout=5)
            self.click_link_element('css=#FTPBlockIP_Set_releaseday > span._text', delay, timeout=5)
            # self.wait_and_click("css=#FTPBlockIP_Set_releaseday > span._text", timeout=5)
            # time.sleep(1)
            # self.wait_and_click("link="+delay, timeout=5)
            self.wait_and_click("settings_networkFTPAccessBlockIPSave_button", timeout=5)
            time.sleep(2)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False, timeout=30)
            self.log.info("Block IP is set to {} ".format(local_ip))
            self.wait_for_updating_complete(interval=3)
            time.sleep(2)  # Wait for updating
        except Exception as e:
            self.log.error("ERROR: Failed to config block temporary IP")
            self.takeScreenShot(prefix='configBlockTempIp')

    def wait_for_updating_complete(self, interval=3):
        """
            Updating dialog wait
        """
        count = 0
        while self.is_element_visible("//div[@class=\'updateStr\']", wait_time=5):
            if count == 10:
                self.log.error("ERROR: Reach the maximum wait for updating")
                self.takeScreenShot(prefix='updating')
                break
            self.log.info("Waiting for updating to finish...")
            time.sleep(interval)  # Waiting for Saving (Updating in LT4A takes longer time than other models)
            count += 1

    def ftpConnection(self, ftp_ip="", new_user_name='user1', new_user_pwd='password1', anonymous=True):
        """
            Create ftp connection and login by user account or anonymous
            Once login, it will return the ftp socket back and exception
            
            @ftp_ip: '192.168.1.91' or self.uut[Fileds.internal_ip_address]
            @anonymous: True (Anonymous login)
                        User login: new_user_name / new_user_pwd (Set to anonymous=False)
                        If you do not set anonymous to False, it always login as anonymous user
            
            Need to set anonymous=False to switch to user login mode
            @new_user_name: user account
            @new_user_pwd: user password               
            
            return (ftpconn, exception)
            
            Usage:
            ftpconn, ex = self.ftpConnection('192.168.1.91', new_user_name='user1', new_user_pwd='password1', anonymous=False)
            ftpconn, ex = self.ftpConnection('192.168.1.91', anonymous=True)
        """
        ex = None
        if ftp_ip == "":
            ftp_ip = self.uut[Fields.internal_ip_address]
            
        try:
            ftpconn = FTP(ftp_ip)
        except Exception as ex:
            self.log.info("Warning: Unable to connect to FTP: {}, exception: {}".format(ftp_ip, repr(ex)))
            ftpconn = None
            return (ftpconn, ex)

        try:
            if anonymous:
                resp = ftpconn.login()
            else:
                resp = ftpconn.login(new_user_name, new_user_pwd)
        except error_perm as ex:
            self.log.info("Warning: Unable to login ftp, exception: {}".format(repr(ex)))
            return (ftpconn, ex)
        except Exception as ex:
            self.log.info("FAILED: ftp login failed, exception: {}".format(repr(e)))
            return (ftpconn, ex)
                                
        if '230' in resp:
            if anonymous:
                self.log.info("Anonymous login succeed")
            else:
                self.log.info("{} login succeed".format(new_user_name))
        else:
            if anonymous:
                self.log.error("Anonymous login failed")
            else:
                self.log.error("{} login failed".format(new_user_name))
        
        return (ftpconn, ex)

    def ftpReadTest(self, ftpconn, filesize=10240, share_folder='Public', delete_after_download=False):
        """
            Pre-requiste:
                You need to have temporary file called 'file0.jpg' under the share_folder for test
                Normally, run the upload(write) test first to generate the temporary file there
                Then, execute the download(read) test
             
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_download: True (Delete the temporary file after downloading test)
        """
        resp = ftpconn.cwd(share_folder) # switch to 'Public' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))        
        
        # download file0.jpg as file_check.jpg
        tmpfile = 'file0.jpg' # temporary file on FTP server       
        download_file = "download_file0.jpg" # filename which is saved locally

        # if download_file exists, removes it
        if os.path.isfile(download_file):
            os.remove(download_file)

        # Check if tmpfile exists or not
        listfiles = ftpconn.nlst()
        if tmpfile in listfiles:
            self.log.info("{} exists, read test is ready to go".format(tmpfile))
        else:
            raise Exception("FAILED: {} does not exist for read test, STOP!".format(tmpfile))
    
        self.log.info("*** Going to downlad file for read access test ***")
        
        try:
            with open(download_file, "wb") as f:
                def callback(data):
                    f.write(data)
                resp = ftpconn.retrbinary("RETR "+tmpfile, callback, blocksize=1048576)
            if '226' in resp:
                self.log.info("{} is saved locally".format(download_file))
                self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("FAILED: {} download failed".format(download_file))
            
        except Exception as e:
            raise Exception("FAILED: ftpReadTest failed, can not download {}, exception: {}".format(tmpfile, repr(e)))
        ftpconn.close()
        
        if delete_after_download:
            os.remove(download_file)
            self.log.info("Cleaning temporary files: {}".format(os.path.abspath(download_file)))
        
        self.log.info("*** Read access test END ***")                 
    
    def ftpWriteTest(self, ftpconn, filesize=10240, share_folder='Public', delete_after_upload=True):
        """
            Generate temporary file called file0.jpg for uploading test
            
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_upload: True (Delete the temporary file after uploading test)
        """
        resp = ftpconn.cwd(share_folder) # switch to 'Public' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))   
        
        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        tmpfile = ''.join(tmp_file)
        self.log.info("Generate temporary file: {}".format(tmpfile))
                    
        self.log.info("** Going to upload temporary file for write access test **")
        # upload file0.jpg
        try:
            with open(tmpfile, "rb") as f:
                resp = ftpconn.storbinary("STOR "+os.path.basename(tmpfile), f, 1048576) # 1048576 = 1MB
            if '226' in resp: # File successfully transferred
                self.log.info("{} is uploaded successfully".format(os.path.basename(tmpfile)))
                self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("Upload {} failed".format(os.path.basename(tmpfile)))
        except error_perm as e:
            self.get_ftp_anonymous_access(sharename=share_folder)
            raise e
        except Exception as e:
            raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. exception: {}".format(os.path.basename(tmpfile), repr(e)))
        
        ftpconn.close()
        
        #os.chdir('..') # switch to parent directory instead of staying in temporary folder
        if delete_after_upload:
            shutil.rmtree(os.path.dirname(tmpfile))
            self.log.info("Cleaning temporary files: {}".format(tmpfile))
        
        self.log.info("*** Write access test END ***")

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            # visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, timeout=5, retry=3):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception as e:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def get_ftp_anonymous_access(self, sharename='Public'):
        readlist = None
        writelist = None
        xml_contents = self.execute('cat /etc/NAS_CFG/ftp.xml')[1]
        root = ET.fromstring(xml_contents)
        for item in root.findall('.//item'):
            share = item.find('name').text
            if share == sharename:
                readlist = item.find('read_list').text
                writelist = item.find('write_list').text
                self.log.debug("READ list: {}".format(readlist))
                self.log.debug("WRITE list: {}".format(writelist))
                break
        if writelist and 'ftp' in writelist:
            self.log.info("{} share has [anonymous r/w] access".format(sharename))
        elif readlist and 'ftp' in readlist:
            self.log.info("{} share has [anonymous read only] access".format(sharename))
        else:
            self.log.info("{} share has [anonymous none] access".format(sharename))

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

blockFTPAccess()
