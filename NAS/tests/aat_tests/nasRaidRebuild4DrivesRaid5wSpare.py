'''
Create on Nov 17, 2014

@Author: lo_va

Objective: To validate RAID5 with spare configuration
           rebuild scenarios for 4 bay Consumer NAS.
wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Rebuild
'''

import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
from global_libraries import CommonTestLibrary as ctl
from global_libraries import wd_exceptions
sharename = 'RebuildR5wSpare'
generated_file_size = 10240
timeout = 240

class nasRaidRebuild4DrivesRaid5wSpare(TestClient):

    def run(self):
        
        # Check Drives account
        if self.get_drive_count() != 4:
            raise wd_exceptions.InvalidDeviceState('Device is not with 4 drives, cannot test RAID5 w/ spare rebuild Test case!!!')
        # Set Web Acces Timeout value to 30 mins
        self.set_web_access_timeout()
        
        try:
            # Create RAID5 with Spare if UUT is not in RAID5 with Spare
            raid_mode = self.get_raid_mode()
            raid_status = self.check_storage_page()
            bool, check_verify = self.verifyMountVolume(volumeid=[1])
            if raid_mode == 'RAID5' and raid_status == 'Healthy' and 'Spare' in check_verify:
                self.log.info('Raid type already in RAID5 w/ spare and status is Healthy')
            else:
                self.log.info('Start to create RAID5 with spare.')
                self.configure_raid(raidtype='RAID5', spare=True, volume_size=500, force_rebuild=True)
                bool, check_verify = self.verifyMountVolume(volumeid=[1])
                if not bool:
                    raise Exception('Verified mount failed')
            self.delete_all_shares()

            # Create share and validate access via smb protocol
            self.log.info('Create %s folder and validate access via smb protocol.' % sharename)
            self.create_shares(share_name=sharename, force_webui=True)
            self.wait_until_element_is_not_visible(E.UPDATING_STRING)
            self.read_write_access_check(sharename=sharename, prot='SAMBA')
        except Exception, ex:
            self.log.exception('Configure RAID5 w/ spare was not successful. Please check RAID settings' + str(ex))

        # Clean up and Plug out the disk 1
        self.log.info('Plug out Disk 1 !!')
        self.clean_up_drive(1)
        self.remove_drives(1)

        # Time to wait rebuilding function starting. Optional
        self.wait_rebuild_start()

        # Check RAID status was in degraded and validate access
        self.log.info('Check RAID5 status was in rebuilding and validate access.')
        raid_status1 = self.raid_status(1)
        if 'recovering' in raid_status1:
            try:
                self.log.info('Raid status: Rebuilding.')
                self.check_home_page('rebuilding')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
                #self.read_write_access_check(sharename=sharename, prot='NFS')
            except Exception, ex:
                self.log.exception('Failed to access rebuilding Degraded_RAID5 w/ spare\n' + str(ex))
        else:
            self.log.error('RAID5 not in rebuilding mode!\n')

        # Wait until rebuild is finished and validate RAID access.
        self.log.info('Wait until rebuild is finished.')
        self.wait_rebuild_finished()

        self.log.info('Start to check raid status.')
        raid_status = self.check_storage_page()
        if raid_status == 'Healthy' or raid_status == 'Verifying RAID parity':
            try:
                self.log.info('Raid status: Healthy')
                self.check_home_page('healthy')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
                self.log.info('****** NAS Rebuild for RAID5 w/ Hot Spare test PASS!! GOOD JOB!! ******')
            except Exception, ex:
                self.log.exception('Failed to access Healthy_RAID5\n' + str(ex))
                self.log.info('****** NAS Rebuild for RAID5 w/ Hot Spare test FAILED!! ******')
        else:
            self.log.info('Raid status = %s' % raid_status)
            self.log.critical('****** NAS Rebuild for RAID5 w/ Hot Spare test FAILED!! ******')

        self.log.info('Plug in Disk 1 !!')
        self.insert_drives(1)
        
    def tc_cleanup(self):
        self.checked_disk_exist()
        raid_status1 = self.raid_status(1)
        if 'degraded' in raid_status1:
            self.configure_raid(raidtype='RAID0', volume_size=100)

    def checked_disk_exist(self):
        drive_mapping = self.get_drive_mappings()
        for x in range(1, self.uut[self.Fields.number_of_drives]+1):
            if x not in drive_mapping:
                self.log.info('Drive {} is disconnected, try to insert it!'.format(x))
                self.insert_drives(x)
        if self.get_drive_count() == self.uut[self.Fields.number_of_drives]:
            self.log.info('Clean_up for insert drives succeed!')
        else:
            self.log.warning('Clean_up for insert drives failed!')
            
    def clean_up_drive(self, drive_number):
        drive_mapping = self.get_drive_mappings()
        clean_up_cmd = 'echo -e "o\ny\nw\ny\n" | gdisk /dev/{}'.format(drive_mapping.get(drive_number))
        self.run_on_device(clean_up_cmd)

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED!!')
        else:
            self.log.error('Hash test FAILED!!')

    def read_write_access_check(self, sharename, prot):

        if prot == 'SAMBA':
            generated_file = self.generate_test_file(generated_file_size)
            time.sleep(10)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            # Wait for transfer finished
            time.sleep(10)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName='file0.jpg')
            self.read_files_from_smb(files='file0.jpg', share_name=sharename, delete_after_copy=True)
            time.sleep(10)
            
        elif prot == 'NFS':
            nfs_mount = self.mount_share(share_name=sharename, protocol=self.Share.nfs)
            if nfs_mount:
                created_files = self.create_file(filesize=generated_file_size, dest_share_name=sharename)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=sharename):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
        else:
            self.log.info('Protocol {} is not supported'.format(protocol))

    def raid_status(self, raidnum):
        check_raid_status = 'mdadm --detail /dev/md%s | grep "State :"' % raidnum
        raid_status = self.run_on_device(check_raid_status)
        time.sleep(1) # Time to wait status return value
        while not raid_status:
            raid_status = self.run_on_device(check_raid_status)
            time.sleep(1)
        self.log.info('Raid_status = %s' % raid_status)
        raid_status1 = str(raid_status.split(': ')[1])

        return raid_status1

    def wait_rebuild_start(self):
        # Wait for recovering started
        self.log.info('Waiting up to 30 seconds for the RAID status become rebuild')
        timer = Stopwatch(timer=30)
        timer.start()

        while True:
            raid_status1 = self.raid_status(1)
            if 'recovering' in raid_status1:
                self.log.info('RAID status took {} seconds became rebuild'.format(timer.get_elapsed_time()))
                break
            else:
                if timer.is_timer_reached():
                    self.log.warning('RAID status never became rebuilding')
                    break
                else:
                    time.sleep(1)
                
    def wait_rebuild_finished(self):
        # Wait for recovering finished              
        status_mins = 600
        while status_mins >= 5:
            raid_status1 = self.raid_status(1)
            if 'recovering' in raid_status1:
                self.get_to_page('Storage')
                self.wait_until_element_is_visible('css = .list_icon_bar_text')
                raidstatus = self.get_text('css = .list_icon_bar_text')
                status = str(raidstatus.split(', ')[0])
                status_mins = int(raidstatus.split(', ')[-1].rstrip(' mins'))
                self.log.info('Still need wait for %d minutes' % status_mins)
                self.close_webUI()
                time.sleep(360)
            else:
                status_mins = 4
    
    def check_storage_page(self):
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        while not raid_status:
            raid_status = self.get_text('raid_healthy')
            time.sleep(1)
        self.close_webUI()
        return raid_status
    
    def check_home_page(self, raidstate):
        try:
            self.log.info('Start to check raid status in home page')
            self.get_to_page('Home')
            if raidstate == 'rebuilding':
                dignostics_state = self.get_text('diagnostics_state')
                if 'Caution' in dignostics_state:
                    self.click_element('smart_info')
                    self.drag_and_drop_by_offset('css = div.jspDrag', 0, 80)
                    diagnosticsRaidStatus = self.get_text('home_diagnosticsRaidStatus_value')
                    if 'Rebuilding' in diagnosticsRaidStatus:
                        self.log.info('Raid status is Rebuilding!')
                else:
                    self.log.error('Raid status is not in Rebuilding')

            if raidstate == 'healthy':
                dignostics_state = self.get_text('diagnostics_state')
                if dignostics_state == 'Healthy':
                    self.log.info('Raid status is Healthy!')
                else:
                    self.log.error('Raid status is not in Healthy')

            if self.is_element_visible('home_diagnosticsClose1_button', 10):
                self.click_element('home_diagnosticsClose1_button')
            
            # Capacity verification
            if self.is_element_visible(E.HOME_DEVICE_CAPACITY_FREE_SPACE, 10):
                raid_capacity = self.get_text(E.HOME_DEVICE_CAPACITY_FREE_SPACE)
            elif self.is_element_visible('home_volcapity_info', 10):
                raid_capacity = self.get_text('home_volcapity_info')
            
            if raid_capacity:
                self.log.info('Raid capacity is %s' % (raid_capacity))
            if not raid_capacity:
                self.log.error('Checked raid capacity failed in home page!')
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Checked raid status failed in homp page!')

    def verifyMountVolume(self, volumeid):
        vol = ''
        self.get_to_page('Storage')
        if self.is_element_visible('css = div.flexigrid', 2):
            vol = self.get_text('css = div.flexigrid')
        for x in volumeid:    
            if 'Volume_%s' % x in vol:
                self.log.info('Volume_%s Mount Successed!' % x)
                bool = True
            else:
                self.log.warning('Volume_%s not Mount!' % x)
                bool = False
        self.close_webUI()
        
        return bool, vol
    
nasRaidRebuild4DrivesRaid5wSpare()