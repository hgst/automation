"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify we can update admin user's password
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

username = 'amyngo'
password = 'elijah'

class AdminPassword(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            # self.skip_cleanup = True
            self.create_user(username=username, is_admin=True)
            result = self.get_user(username)

            is_password1 = self.get_xml_tag(result[1], 'is_password')
            self.log.info("Before set password: {}".format(is_password1))
            self.update_user(user=username, new_password=password)
            result = self.get_user(username)
            is_password2 = self.get_xml_tag(result[1], 'is_password')

            if is_password1 == 'false' and is_password2 == 'true':
                self.log.info("Update admin password successfully")
            else:
                self.log.error("Failed to update admin password")

            self.update_user(user='admin', new_password=password)
            result1 = self.get_user(username)

            self.update_user(user='admin', old_password=password, new_password='')
            result2 = self.get_user(username)

            if result1[0] == 0 and result2[0] == 0:
                self.log.info("Update 'admin' password successfully")
            else:
                self.log.error("Failed to update 'admin' password")

        except Exception, ex:
            self.log.exception('Failed to complete update admin password test case \n' + str(ex))
        finally:
            self.log.info("Delete user {}".format(username))
            self.delete_user(username)

AdminPassword()