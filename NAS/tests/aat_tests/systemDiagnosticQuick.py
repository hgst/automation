'''
Created on Feb 10 2015

@author: Carlos Vasquez

Objective:  To verify that the System Diagnostic(Quick Test) is functional
'''

import os
from testCaseAPI.src.testclient import TestClient

class systemDiagnostic(TestClient):   
    def run(self):
        numberDrives = self.uut[self.Fields.number_of_drives]
        self.click_element(self.Elements.SETTINGS_UTILITY_QUICKTEST)
        self.systemDiagnostic_CheckResult('Quick Test', numberDrives)
              
    def systemDiagnostic_CheckResult(self, diagnosticType, numberDrives):
        self.log.info('Checking System Diagnostic({0}) results'.format(diagnosticType))        
        self.wait_until_element_is_visible('settings_utilitiesDiskTestClose_button',600)
        text_result=self.get_text('DIV_SMART_RES')
      
        reportedDrives = text_result.count('Disk')
        # numberDrives should be equal to number of lines with the word 'Disk' returned in new_result
        if numberDrives == reportedDrives:
            self.log.info('SUCCESS: System Diagnostic({0}) completed successfully'.format(diagnosticType))
        else:
            self.log.error('FAILED: System Diagnostic({0}) did not complete successfully'.format(diagnosticType))      
        self.click_element('settings_utilitiesDiskTestClose_button')     
systemDiagnostic()