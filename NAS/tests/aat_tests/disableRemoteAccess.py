"""Created on Feb 10, 2015

@Author: lee_e

PASS/FAIL criteria: As long as each step below is successful, this test is considered passed

save below config before run wd2go.sh or do quickrestore:
1) Adds an entry to /etc/hosts
2) Modifies system.conf
3) Deletes /tmp/dynamicconfig.ini
4) Deletes the device users from /usr/local/nas/orion/orion.db
5) Modifies /usr/local/config/dynamicconfig_config.ini
6) Modifies /var/www/rest-api/config/dynamicconfig.ini
7) Executes /usr/local/sbin/genHostsConfig.sh, which modifies /etc/hosts and /etc/dhcp3/dhclient.conf
8) Copies /etc/hosts to /usr/local/config
9) Modifies /usr/local/onboarding/onbrd.ini



"""

import time
import os
from testCaseAPI.src.testclient import TestClient


waiting_string = '/ #'

class WDNtpError(Exception):
    pass

class DisableRemoteAccess(TestClient):
    def run(self):
        self.save_hosts_file()
        self.enable_remote_access()
        time.sleep(10)
        self.disable_remote_access_test()
       
    def tc_cleanup(self):
        self.reset_hosts_file()
        self.run_on_device('kill_running_process ; load_default 1 ; touch /var/www/system_boot.html')
        self.serial_reboot(max_boot_time=self.uut[self.Fields.reboot_time])
        self.check_init_config()
        self.set_ssh(enable=True)
        # Workaround Solution
        try:
            self.execute('echo test1')
        except Exception as ex:
            self.log.warning('Sleep 20 secs because except: {}'.format(ex))
            # Wait system ready.(Some models still need to wait 20 secs to let httpd service ready)
            time.sleep(20)
            try:
                self.execute('echo test2')
            except Exception as ex:
                self.log.warning('Sleep 10 secs because except: {}'.format(ex))
                time.sleep(10)

    def disable_remote_access_test(self):
        self.log.info('******** Test Start *******')
        self.run_wd2go_script()
        self.update_hosts_file()
        self.config_cloud_device(delete=False)
        self.check_cloud_access_status()
        self.check_communication_status(check_status='running')
        self.update_cloud_access(switch=False)
        self.check_communication_status(check_status='not running')
        self.update_cloud_access(switch=True)
        self.check_communication_status(check_status='running')
        self.log.info('******* Test End ********')
    
    def update_cloud_access(self, switch):
        self.log.info('****** Update Cloud Access to {0} *********'.format(switch))
        self.get_to_page('Settings')
        time.sleep(2)
        while self.is_element_visible(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK, wait_time=15) != switch:
            try:
                self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH, self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK, timeout=10)
                self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK, self.Elements.UPDATING_STRING, visible=False, timeout=60)
            except Exception as e:
                self.take_screen_shot('Update_cloud_access_to_'+str(switch))
                raise WDNtpError('Exception as {0}'.format(repr(e)))
    
    def config_cloud_device(self, delete, timeout=60):
        self.log.info('****** Config Cloud Device *********')
        self.get_to_page('Cloud Access')
        self.wait_until_element_is_visible(self.Elements.CLOUD_ADMIN_ICON, timeout=timeout)
        if delete:
            self.click_wait_and_check(self.Elements.CLOUD_ADMIN_ICON, 'cloud_delCode0_link', timeout=timeout)
            self.click_wait_and_check('cloud_delCode0_link', 'popup_apply_button', timeout=timeout)
            self.click_wait_and_check('popup_apply_button', self.Elements.UPDATING_STRING, visible=False, timeout=timeout)
        else:
            self.click_wait_and_check(self.Elements.CLOUD_ADMIN_ICON, self.Elements.CLOUD_ADMIN_GET_CODE, timeout=timeout)
            self.click_wait_and_check(self.Elements.CLOUD_ADMIN_GET_CODE, self.Elements.UPDATING_STRING, visible=False, timeout=timeout)
            self.wait_until_element_is_visible(self.Elements.CLOUD_ADMIN_GET_CODE_DIAG_TITLE, timeout=timeout)
            title = self.get_text(self.Elements.CLOUD_ADMIN_GET_CODE_DIAG_TITLE)
            if title == 'Add Cloud Access':
                self.wait_until_element_is_clickable(self.Elements.CLOUD_ADMIN_GET_CODE_OK, timeout=timeout)
                self.click_wait_and_check(self.Elements.CLOUD_ADMIN_GET_CODE_OK, self.Elements.CLOUD_ADMIN_ICON, timeout=timeout)
            else:
                self.log.error('{0}: Failed to create device user'.format(title))
            # sleep for waiting add complete
            time.sleep(5)
    
    def check_cloud_access_status(self):
        self.log.info('****** Check Cloud Access Status *********')
        self.get_to_page('Cloud Access')
        #correct status: 'Connected (Relay connection established)' or 'Connected -relay'
        max_boot_time = 60
        start_time = time.time()
        status = self.get_text('cloud_status_value')
        while status.find('Connected') != 0:
            status = self.get_text('cloud_status_value')
            self.log.info('Current Cloud Access Status is '+status)
            time.sleep(15)
            if time.time() - start_time >= max_boot_time*1.5:
                self.log.error('FAIL: Cloud Access Status is '+status)
                raise WDNtpError('Timed out waiting to update Cloud Access Status: '+str(time.time() - start_time))

        if status.find('Connected') == 0:
            self.log.info('PASS: Cloud Access Status is '+status)

    def run_wd2go_script(self):
        self.log.info('****** Run wd2go script *********')
        command = '/usr/local/modules/localsbin/wd2go.sh -0'
        output = self.run_on_device(command)
        self.log.debug('output of command - {0} : {1}'.format(command, output))

    def check_communication_status(self, check_status):
        self.log.info('****** Check NAS Communication Status *********')
        command = '/usr/local/orion/communicationmanager/communicationmanagerd status'
        max_boot_time = 20
        # which means retry 6 times
        output = self.run_on_device(command)
        self.log.info('console output: '+output)
        version, status = output.split('\n')
        start_time = time.time()
        while status != check_status:
            # add try to prevent that execute command fail
            try:
                output = self.run_on_device(command)
                self.log.info('console output: '+output)
                version, status = output.split('\n')
            except Exception as e:
                self.log.debug('output does not get status: {0}'.format(repr(e)))

            if time.time() - start_time > max_boot_time*1.5:
                self.log.debug('Debug: process is now ' + str(status))
                raise WDNtpError('Timed out for waiting by use command: '+str(command))

        if check_status == status:
            self.log.info('PASS: process is now '+check_status)
    
    def save_hosts_file(self):
        self.execute('cp /etc/hosts /etc/hosts.ntpconfiguration')

    def reset_hosts_file(self):
        self.log.info('**** Reset Hosts file ****')
        self.execute('cp /etc/hosts.ntpconfiguration /etc/hosts')
        self.execute('rm /etc/hosts.ntpconfiguration')

    def save_system_conf_file(self):
        self.execute('cp /etc/system.conf /etc/system_original.conf')

    def reset_system_conf_file(self):
        self.log.info('**** Reset system config file ****')
        self.execute('cp /etc/system_original.conf /etc/system.conf')
        self.execute('rm /etc/system_original.conf')
    
    def save_dynamicconfig_file(self):
        if os._exists('/tmp/dynamicconfig.ini'):
            self.execute('cp /tmp/dynamicconfig.ini /tmp/dynamicconfig_org.ini')

    def reset_dynamicconfig_file(self):
        self.log.info('**** Reset dynamicconfig file ****')
        if os._exists('/tmp/dynamicconfig.ini'):
            self.execute('cp /tmp/dynamicconfig_org.ini /tmp/dynamicconfig.ini')
            self.execute('rm /tmp/dynamicconfig_org.ini')

    def update_hosts_file(self):
        self.log.info('****** Update Hosts file *********')
        #self.execute('echo "127.0.0.1 localhost.localdomain localhost" > /etc/hosts')
        remove_secondary_loopback = 'cat /etc/hosts | grep -v 127.0.1.1 > /etc/hosts.temp'
        self.execute(remove_secondary_loopback)
        self.execute('mv /etc/hosts.temp /etc/hosts')

    def serial_reboot(self, max_boot_time=600):
        # Open serial connection
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))

        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write('/usr/sbin/do_reboot')
        self.log.info('do command: /usr/sbin/do_reboot')

        # Loop until the pings stop
        start = time.time()
        while self.is_ready():
            time.sleep(0.5)
            try:
                if time.time() - start >=  max_boot_time:
                    raise Exception('Device failed to power off within {0} seconds'.format(max_boot_time))
            except Exception as e:
                self.log.exception('Fail to power down within {0} seconds by UI'.format(repr(e)))
        time.sleep(5)

        start_time = time.time()
        while not self.is_ready():
            self.log.debug('Web is not ready')
            try:
                if time.time() - start_time >= max_boot_time:
                    raise Exception('Timed out for waiting {0} seconds to boot'.format(max_boot_time))
            except Exception as e:
                self.log.exception('Fail to boot up within {0} seconds by UI'.format(repr(e)))

        self.log.info('Web is ready')

    def check_init_config(self):
        response = self.get_firmware_version().status_code
        result = self.get_firmware_version()
        self.log.info('Response:{0}, firmware_version:{1}'.format(response, result))

DisableRemoteAccess()