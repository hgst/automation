""" User Apps Page [MA-119]

    @Author: Vasquez_c
    
    Objective: Apps page is shown
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 2.00.229)
              
              Jenkins Irvine : Glacier -pass (FW:2.10.120)
""" 

import sys
import time
import os
import requests
import inspect
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import TestClient

availableApps = {
    'aMule': 'css=.LightningCheckbox,f_apps_install_0',
    'IceCast': 'css=.LightningCheckbox,f_apps_install_1',
    'Dropbox':'css=.LightningCheckbox,f_apps_install_11'
}

username = 'user1'
user_password = '12345'

class userAppsPage(TestClient):
    
    def run(self):
        self.tc_cleanup()
        self.get_to_page('Apps')
        time.sleep(3) # wait for app server to load the app list
        self.wait_and_click('apps_installed_button')
        self.verifyAppWizard()
        self.updateAvailableAppsLocator()
        self.addAppUI(appName='aMule')
        self.add_app(appName='aMule')
        self.setup_user()   
        self.user_redirect_verification()
        self.tc_cleanup()
        
    def tc_cleanup(self):
        self.delete_all_users(use_rest=False)
        self.delete_app(appName='aMule')
    
    def get_user_list(self):
        command = 'account -i user'
        output = self.run_on_device(command)
        
        return output 
       
    def setup_user(self):
        #Verify if user exists, if not create it
        user_list = self.get_user_list()
        if username in user_list:
            self.modify_user_account(user=username, passwd=user_password)
        else:
            add_user_command = 'account -a -u {0} -p {1}'.format(username, user_password)
            self.run_on_device(add_user_command) 
            
    def user_redirect_verification(self):
        self.uut[self.Fields.web_username] = username
        self.uut[self.Fields.web_password] = user_password
        self.get_to_page('Non Admin Home')
        
        self.click_wait_and_check('nav_apps_link', 'css=div.header_1 > span._text')
        if self.get_text('css=div.header_1 > span._text') == 'Apps':
            self.log.info('PASS: user was redirected to the Apps page')
        else:
            self.log.error('FAIL: user was not redirected to the Apps page')

            
    def addAppUI(self, appName='IceCast'):
        """
            Add IceCast App
        :param appName: App Name
        """
        global availableApps
        if appName not in availableApps.keys():
            raise Exception("ERROR: Unable to find the {} app in current available app list".format(appName))
        old_top = 0.0
        cur_top = 0.0
        while True:
            try:
                self.select_checkbox(availableApps[appName], timeout=5)
            except Exception:  # ElementNotVisibleException
                self.log.debug("Going to scroll the scrollbar to find the {} app".format(appName))
                scroll = self.element_find('css=.jspDrag')
                scrolltext = scroll.get_attribute('style')
                self.log.info("DBG - STYLE: {}".format(scrolltext))
                if 'top' not in scrolltext:
                    scrollcount = 50.0
                else:
                    cur_top = float(scrolltext.split('top:')[-1].split('px;')[0].strip())
                    if cur_top > old_top:    # Lightning: 124px, Yellowstone: 109px
                        scrollcount = 50.0
                    else:
                        self.log.info("Reach the bottom of the scrollbar, current top: {} px".format(cur_top))
                        raise Exception("ERROR: Can not find the app {} and hit the bottom of the scroll bar".format(appName))
                try:
                    self.drag_and_drop_by_offset('css=.jspDrag', 0, scrollcount)  # max = 124px
                except Exception:
                    pass
                finally:
                    old_top = cur_top
            else:
                break
        count = 3
        while count > 0:
            try:
                if not self.is_checkbox_selected(availableApps[appName]):
                    self.select_checkbox(availableApps[appName])
                    self.log.info("Select {} app to install".format(appName))
                else:
                    self.log.info("*** {} is selected ***".format(appName))
            except Exception as e:
                self.log.error("ERROR: Unable to select {} app, exception: {}".format(appName, repr(e)))
            time.sleep(1)
            if self.is_element_visible('Apps_next_button_3'):
                self.wait_and_click('Apps_next_button_3')
                self.log.info("Click Install to continue the APP installation")
                break
            else:
                self.log.info("RETRY: Unable to click Install button, going to re-check the checkbox")
                count -= 1
                time.sleep(1)
                continue
        time.sleep(1)
        appcmd = 'ps aux | grep -v grep | grep download.wdc.com'
        count = 0
        while True:
            if count >= 10:
                self.log.info("Reach the maximum 10 minutes wait for App installation")
                break
            try:
                appResult = self.run_on_device(appcmd)
            except Exception as e:
                self.log.info("Retry: Unable to check app status, exception: {}".format(repr(e)))
                time.sleep(60)
                count += 1
                self.log.info("{} minute wait".format(count))
                continue
            else:
                if appResult:
                    self.log.info("{} app is still installing...".format(appName))
                    self.log.debug("== {} ==".format(appResult))
                    time.sleep(60)
                    count += 1
                    self.log.info("{} minute wait".format(count))
                    continue
                else:
                    self.log.info("{} app installation completed".format(appName))
                    break
        if self.is_element_visible('popup_ok_button'):
            self.log.info("Click OK to confirm the APP installation")
            self.click_wait_and_check('popup_ok_button', visible=False)
        self.close_webUI()
        time.sleep(2)


    def updateAvailableAppsLocator(self):
        """
            Update available apps locator
        """
        global availableApps
        appdict = {}
        count = 3
        while count > 0:
            try:
                elementlist = self._sel.driver._element_find("css=.LightningCheckbox", False, True)
                for element in elementlist:
                    applist = element.find_elements(by="xpath", value=".//*")
                    if applist:
                        for app in applist:
                            id = app.get_attribute('id')
                            name = app.get_attribute('value')
                            if name in availableApps.keys():
                                appdict[name] = id
            except Exception as e:
                self.log.error("ERROR: Unable to locate the element, exception: {}".format(repr(e)))
                count -= 1
                self.log.info("Going to re-locate elements, remaining {} retry".format(count))
                continue
            else:
                num_of_apps = int(len(appdict.keys()))
                if num_of_apps == 0:
                    count -= 1
                    self.log.info("Going to reload the app wizard, remaining {} retry".format(count))
                    self.wait_and_click('Apps_Cancel1_button', timeout=5)
                    self.wait_and_click('apps_installed_button', timeout=5)
                    continue
                self.log.info("Found {} apps available".format(num_of_apps))
                for k, v in appdict.items():
                    availableApps[k] = 'css=.LightningCheckbox,' + v
                break
            
    def verifyAppWizard(self):
        """
            Verify App Wizard window is launched!
        """
        time.sleep(3)
        while self.is_element_visible('apps_next_button_7', wait_time=5): # App term of service
            self.log.info("Going to accept APP term of service!")
            self.click_element('apps_next_button_7')
        self.wait_until_element_is_visible('AppsDiag_title')
        if self.is_element_visible('AppsDiag_title'):
            self.log.info("PASS: [Add an App] wizard window is launched!")
        else:
            self.log.error("ERROR: [Add an App] wizard window is NOT FOUND!!!")
userAppsPage()