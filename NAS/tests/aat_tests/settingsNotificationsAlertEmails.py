""" Settings Page - Notifications - Alert Emails (MA-93)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?execView=execDetails&pId=50&etab=8&nTP=117538&view=details&nEx=20018&pltab=steps)
                1) Open webUI to login as system admin
                2) Go to Settings Page
                3) Select Notifications -> Alert emails
                4) Enable Alert emails
                5) Click configure
                6) Select Critical Only, Critical and Warning or All
                7) Verify that the email address recevies alert messages
                8) Verify multiple email addresses can be added
                9) Verify that email addresses can be removed
                Alert emails are sent to the specified email address
                
    
    Automation: Partial
    
    Manual:
        1) Verify alert mails are sent by either critical, critical and warning or All
        2) Verify multiple emails can receive the alert messages
        
    
    Note: Your Alert email configuration will be clear at the end of the test
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.133)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.webdriver.common.keys import Keys
import time
import os

# Max: 5
testAddrList = ('fituser@mailinator.com', 'fituser1@mailinator.com', 'fituser2@mailinator.com', 'fituser3@mailinator.com', 'fituser4@mailinator.com')

picturecounts = 0

class alertEmails(TestClient):
    
    def run(self):
        self.log.info('######################## Settings Notifications Alert Emails TEST START ##############################')
        try:
            self.config_email_notification(testAddress='fituser@mailinator.com', level='a')
            self.execute('alert_test -R')
            self.log.info("Generating alerts...")
            self.trigger_alerts(alertcode=('0001', '1003', '2003'))  # Generate Critical, Warning and Informational alerts
            time.sleep(5) # Waiting for alert mails being sent
            self.addRemove_email_settings()
        except Exception as e:
            self.log.error("FAILED: Settings Notifications Alert Emails Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.execute('alert_test -R')
            self.log.info("Conditional PASS: Settings Notifications Alert Emails Test Pass, You have to check the alert emails in your mailbox!!")
            
        self.log.info('######################## Settings Notifications Alert Emails TEST END ##############################')
    
    def addRemove_email_settings(self):
        """
            Add/Remove Email setting test
        """
        global testAddrList
        if len(testAddrList) != 5:
            self.log.error("ERROR: You do not provide correct number of mail boxes for test...")
            return
        try:
            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link'):
                self.log.info("Delete one email address")
                try:
                    self.wait_and_click('settings_notificationsDelMail1_link')
                except Exception:  # Handle StaleElementException
                    pass
                time.sleep(2)
            self.log.info("All email settings are deleted!")

            # Add 5 mailboxes
            for index, testMail in enumerate(testAddrList, 1):
                self.click_wait_and_check('settings_notificationsAddMail_button',
                                          'settings_notificationsMail_text', visible=True)
                self.input_text_check('settings_notificationsMail_text', testMail, do_login=False)
                time.sleep(2)
                self.click_wait_and_check('settings_notificationsSaveMail_button', visible=False)
                self.log.info("Add new email {}: {}".format(index, testMail))

            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link'):
                self.log.info("Delete one email address")
                try:
                    self.wait_and_click('settings_notificationsDelMail1_link', timeout=15)
                except Exception:  # Handle StaleElementException
                    pass
                time.sleep(2)
            self.log.info("All email settings are cleared!")
            self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
        except Exception as e:
            self.takeScreenShot(prefix='addremoveEmailSettings')
            self.log.error("FAILED: Add/remove email settings failed due to exception: {}".format(repr(e)))

    def config_email_notification(self, testAddress="fituser@mailinator.com", level='a'):
        """
            Enable and Test email alert
            
            @testAddress: email address you'd like to add in the notification
                          if you set to fituser@mailinator.com, it will set 5 emails from fituser1 to fituser5
            @severity: 'c': critical only
                       'w': critical and warning
                       'a': All                          
        """
        try:
            self.log.info("==> Configure Alert emails notification")
            self.get_to_page('Settings')
            self.wait_and_click('settings_notifications_link')

            self.log.info("Turn on Alert emails notification")
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            if email_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsAlert_switch+span .checkbox_container',
                                          'css=#settings_notificationsAlert_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            self.log.info("Alert emails notification is enabled to {}".format(email_status))
            time.sleep(2)
            self.log.info("Click Alert emails configure")
            self.click_wait_and_check('settings_notificationsAlert_link',
                                      'settings_notificationsMailSave_button', visible=True)

            # Select Severity to Critical Only, Critical and Warning, All
            self.config_sev_notification(severity=level)

            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link'):
                self.log.info("Delete one email address")
                self.wait_and_click('settings_notificationsDelMail1_link')
                time.sleep(2)
            self.log.info("All email settings are deleted!")

            # Add One valid email
            self.wait_until_element_is_visible('settings_notificationsAddMail_button')
            self.click_wait_and_check('settings_notificationsAddMail_button',
                                      'settings_notificationsMail_text', visible=True)
            if self.is_element_visible('settings_notificationsMail_text'):
                self.log.info("Add valid email: {}".format(testAddress))
                self.input_text_check('settings_notificationsMail_text', testAddress, do_login=False)
                time.sleep(2)
                self.click_wait_and_check('settings_notificationsSaveMail_button', visible=False)
        except Exception as e:
            self.takeScreenShot(prefix='configEmail')
            self.log.error("FAILED: Config email settings failed, exception: {}".format(repr(e)))

    def config_sev_notification(self, severity='c'):
        """
            Configure notification Level
            
            @severity: 'c': critical only
                       'w': critical and warning
                       'a': All
        """
        self.wait_until_element_is_visible("//div[@id='settings_notificationsDisplay_slider']/a")
        # Get Current Level value
        element = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
        percent_text = element.get_attribute('style')
        percent = int(percent_text.split()[1][:-2])
        self.log.info("Current Notification Level: {}".format(percent))
        time.sleep(2)
        
        if severity == 'c':
            self.log.info("Setting Notification Level to Critical Only(0%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 50%
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 0%
            else:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 0%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 0:
                self.log.info("PASS: Notification Display has been set to Critical ONLY successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set Critical ONLY...")
            return
        elif severity == 'w':
            self.log.info("Setting Notification Level to Critical and Warning(50%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 50%
            elif percent < 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 50:
                self.log.info("PASS: Notification Display has been set to Critical and Warning successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set Critical and Warning...")
            return
        elif severity == 'a':
            self.log.info("Setting Notification Level to All(100%)...")
            if percent <= 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 100:
                self.log.info("PASS: Notification Display has been set to ALL successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set ALL...")
            return       
        
    def trigger_alerts(self, alertcode=None):
        """
            Trigger Critical Alerts which does not require parameters
            
            @alertcode: '0001' the alert code you want to trigger
            
            If the alert needs arguments, the command requires '-p PARM1,PARM2' after -a
            Sample alert codes:
            Critical
                0001        System Over Temperature
                0224        Replace Drive with Red Light
                0029        Fan Not Working
            Warning
                1002        Network Link Down
                1003        Firmware Update Failed
            Information
                1020        System Shutting down                 
                2003        Temperature Normal
                
        """
        if alertcode == None:
            alertcode = ('0001')
        
        # Generate alerts
        for code in alertcode:
            self.execute('alert_test -a {} -f'.format(code))
        
        # Retrieve current alerts
        resp = self.execute('alert_test -g')
        alerts = resp[1].split('---->')
        for a in alerts[1:]:
            alertnum = a.split('\n')[2]
            alertmsg = a.split('\n')[6]
            self.log.info('{}, {} is generated.'.format(alertnum, alertmsg)) # print the alertMSG
        self.log.info("Verify: Please check your mailbox if you receive alert emails")

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

alertEmails()
        
        
            