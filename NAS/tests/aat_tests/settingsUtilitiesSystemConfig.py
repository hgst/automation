""" Settings Page Utilities System Configuration (MA-89)

    @Author: vasquez_c , yang_ni
    Procedure @ silk:
                1) Open the webUI -> login as system admin
                2) Click on the Settings category
                3) Select Utilities -> System Configuration
                4) Click on save config file
                5) Click on import file
                6) Verify that the system config file can be saved
                7) Verify that a config files can be imported

    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.xxx)
"""             
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as E
import requests,os
import time

fourBayNAS = ['LT4A', 'BNEZ', 'BWZE']

class utilitiesDeviceMaintenance(TestClient):
    
    def run(self):

        try:
            global modelNumber
            xml_response = self.get_system_information()
            modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

            # Enable media streaming
            self.toggle_media_streaming(enable_streaming=True)

            # Save configuration file
            self.log.info('Saving configuration file...')
            self.download_config_file('backup.config')
            config_file = os.path.join(os.getcwd(),'backup.config')

            # Disable media streaming
            self.log.info('Disabling media streaming...')
            self.toggle_media_streaming(enable_streaming=False)

            # Restore to previous configuration
            self.log.info('Restoring saved configuration...')
            self.get_to_page('Settings')
            self.click_wait_and_check('settings_utilities_link', 'settings_utilitiesConfig_button', visible=True)
            self.log.info("Click upload config")
            self.press_key('settings_utilitiesImport_button',config_file)
            self.log.info("System going to reboot...")
            time.sleep(70)

            # wait for system to be ready
            count = 0
            while not self.is_ready():
                self.log.info("System is not ready yet, wait for a minute...")
                time.sleep(60)
                count += 1
                if count > 10:
                    self.log.error('FAILED: Unit not ready after 10 minutes')
                    break
            self.close_webUI()

            # Streaming should be enabled
            self.import_file_verification()

        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
        else:
            self.log.info('*** System Configuration test is performed successfully ***')
        finally:
            self.log.info('*** Remove the config file ***')
            os.remove(config_file)
            
    def import_file_verification(self):
        self.get_to_page('Settings')
        self.click_element(E.SETTINGS_MEDIA)
        self.log.info('Confirming media server is ON')
        media_setting_text = self.get_text('css = #settings_mediaDLNA_switch+span .checkbox_container')
       
        if media_setting_text == 'ON':
            self.log.info('Media Server Setting is enabled (Test PASSED), status = {0}'.format(media_setting_text))
        else:
            self.log.error('Media Server Setting is disabled (Test FAILED), status = {0}'.format(media_setting_text)) 
                      
utilitiesDeviceMaintenance()