""" Settings Page-General-Mac Backups

    @Author: Lee_e
    
    Objective: verify Time Machine can be switched and UI shows correctly
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.00.216)
                    Lightning : PASS (FW: 2.10.109)
                    KC        : PASS (FW: 2.00.215)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.112 & 2.00.216)
                              : Yosemite     - PASS (FW: 2.10.113& 2.00.216)
                              : Kings Canyon - PASS (FW: 2.10.113 & 2.00.216)
                              : Sprite       - PASS (FW: 2.10.109 & 2.00.216)
                              : Zion         - PASS (FW: 2.10.107 & 2.00.216)
                              : Aurora       - PASS (FW: 2.10.113 & 2.00.221)
                              : Yellowstone  - PASS (FW: 2.10.113 & 2.00.216)
                              : Glacier      - PASS (FW: 2.10.127 & 2.00.221)
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os
import requests

default_time_machine = 1
share_folder_name = 'SmartWare'

class SettingsPageGeneralMacBackups(TestClient):
    def run(self):
        ini_time_machine_status = self.get_time_machine_info_cgi()
        if int(ini_time_machine_status) == 0:
            self.log.info('The value of Time machine should be {0}, but now is {1}'.format(default_time_machine, ini_time_machine_status))
            self.config_time_machine_cgi(default_time_machine)
        self.time_machine_test(ini_time_machine_status, share_folder_name)

    def time_machine_test(self, ori_time_machine_status, foldername):
        self.log.info('*** Start Time Machine Toggle Testing ***')
        self.log.debug('ori_time_machine_status: {0}'.format(ori_time_machine_status))
        self.get_to_page('Settings')
        self.time_machine_config_from_UI(current_value='ON')
        self.time_machine_config_from_UI(current_value='OFF', foldername=foldername)
        self.close_webUI()
        
    def tc_cleanup(self):
        result, enable = self.config_time_machine_cgi(default_time_machine)
        status = self.get_xml_tags(result, 'status')
        if 'ok' in status:
            self.log.info('time machine setting to {0} successfully'.format(enable))
        else:
            self.log.info('time machine setting to {0} failed: {1} '.format(enable,status))
    
    def time_machine_config_from_UI(self, current_value, foldername=None):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.start_test('*** Toggle Time Machine to {0} Test ***'.format(value_oppsite_dict[current_value]))

        for attempts_remaining in range(3, -1, -1):
            if attempts_remaining > 0:
                try:
                    if current_value == 'ON':
                        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SWITCH,
                                                  self.Elements.SETTINGS_GENERAL_TIME_MACHINE_LINK, visible=False, timeout=10)
                    else:
                        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SWITCH, self.Elements.SETTINGS_GENERAL_TIME_MACHINE_LINK, timeout=10)
                        self.time_machine_configure_from_UI(share_folder=foldername)
                    self.check_attribute(locator='settings_generalTM_switch', expected_value=str(value_dict[current_value]), attribute_type='value')
                    self.pass_test('*** Toggle Time Machine to {0} Test ***'.format(value_oppsite_dict[current_value]))
                    if current_value == 'OFF':
                        self.check_time_machine_configure_from_UI(share_folder=foldername)
                    break

                except Exception as ex:
                    self.log.info('Exception: {0}'.format(repr(ex)))
                    time.sleep(2)
            else:
                self.fail_test('*** Toggle Time Machine to {0} Test ***'.format(value_oppsite_dict[current_value]))
                self.take_screen_shot()
        
    def time_machine_configure_from_UI(self, share_folder):
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_LINK, self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SHARE_SELECT, timeout=60)
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SHARE_SELECT, 'link='+str(share_folder), timeout=20)
        self.click_wait_and_check('link='+str(share_folder), self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SAVE, timeout=10)
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)

    def check_time_machine_configure_from_UI(self, share_folder):
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_LINK, self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SHARE_SELECT, timeout=60)
        self.start_test('Time machine config check')
        value = self.get_text(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SHARE_SELECT)
        self.log.debug('List of share folder name on UI:{0}'.format(value))
        if value == share_folder:
            self.pass_test('Time machine config check: save correct share folder-{0}'.format(value))
        else:
            self.fail_test('Time machine config check: save to incorrect share folder-{0}'.format(value))
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_DIAG_CANCEL, self.Elements.SETTINGS_GENERAL_TIME_MACHINE_DIAG, visible=False, timeout=60)
            
    def get_time_machine_status_from_UI(self):
        self.log.info('*** Get Time Machine Status From UI')
        value = self.get_text(self.Elements.SETTINGS_GENERAL_TIME_MACHINE_SWITCH)
        
        return value
    
    def config_time_machine_cgi(self, enable):
        head_url = 'time_machine.cgi'
        time_machine_cgi = 'cgi_tm_set&enable='+str(enable)
        result = self.send_cgi(time_machine_cgi, head_url)

        return result, enable
    
    def get_time_machine_info_cgi(self):
        self.log.info('*** Get Time Machine Information ***')
        time_machine_cgi = 'cgi_tm_get_info'
        head_url = 'time_machine.cgi'
        result = self.send_cgi(time_machine_cgi, head_url)
        time_machine_value = ''.join(self.get_xml_tags(result, tag='tm_enable'))
        self.log.info('time machine enable: {0}'.format(time_machine_value))

        return time_machine_value
        
    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

SettingsPageGeneralMacBackups()