"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify user can access web UI

"""


import time

from sequoiaAPI.src.sequoia import Sequoia
from seleniumAPI.src.seq_ui_map import Elements as ET

remove_eula = 'rm -f /etc/.eula_accepted'


class AccessWebUi(Sequoia):
    # Create tests as function in this class

    def run(self):

        try:
            status = self.is_eula_accepted()
            if status:
                self.log.info('Reset EULA acceptance')
                self.execute(remove_eula)
            self.seq_accept_eula(accept_eula=True)
            time.sleep(3)
            if self.is_element_visible(ET.HOME_LINK):
                self.log.info('User access web UI successfully')
            else:
                self.log.error('User failed access web UI')
        except Exception, ex:
            self.log.exception('Failed to complete User Can Access Web UI test case \n' + str(ex))

AccessWebUi()
