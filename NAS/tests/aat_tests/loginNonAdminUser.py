"""
title           :loginNonAdminUser.py
description     :To verify if a newly created non-admin user can login web UI
author          :yang_ni
date            :2015/03/11
notes           :
"""

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import time


class loginNonAdminUser(TestClient):

    def run(self):
            if self.get_model_number() in ('GLCR',):
                self.log.info("=== Glacier de-feature does not support user login, skip the test ===")
                return

            testname = 'HTTP Login as non admin user'
            self.start_test(testname)

            # Keep the default username/password of uut Fields
            Original_username =  self.uut[Fields.web_username]
            Original_password =  self.uut[Fields.web_password]

            # Delete all the users first
            self.log.info('Deleting all users ...')
            self.delete_all_users()

            time.sleep(3)

            # Create a non admin user
            self.log.info('Creating a non admin user ...')
            non_admin_user = self.create_user(username='user1', password='password', is_admin=False)

            if non_admin_user == 0:
                self.log.info('user1 was created successfully')
            else:
                self.log.error('user1 was not created successfully')

            try:
                # Set uut Fields username/password to user1/password
                self.uut[Fields.web_username] = 'user1'
                self.uut[Fields.web_password] = 'password'

                self.access_webUI()
                time.sleep(8)
                self.wait_until_element_is_clickable('logout_toolbar')
                self.click_button('logout_toolbar')
                time.sleep(6)

                # Check if the username shown in Web GUI is 'user1'
                login_name = str(self.get_text('login_name')).split(',')[0]

                if login_name == 'user1':
                    self.pass_test(testname, 'Successfully login with non admin user')
                else:
                    self.fail_test(testname, 'Login account is not user1')

            except Exception as e:
                    self.fail_test(testname, 'Failed to login with non admin user: {}'.format(e))

            finally:
                # Reset back the web username and password
                self.uut[Fields.web_username] = Original_username
                self.uut[Fields.web_password] = Original_password

# This constructs the loginNonAdminUser() class, which in turn constructs TestClient() which triggers the loginNonAdminUser.run() function
loginNonAdminUser()