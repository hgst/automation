"""
Created on Nov 16, 2014
@Author: O.K.
@ Updated by tran_jas 3/3/2015
    @ Brief:
    # Check Latest FW on Server
    # Enable SSH
    # Check Default Flag Value
    # Set Update Flag
    # Reset EULA & Spoof FW
    # Login, Accept EULA & Check Prompt
    # Install from Online Server
    # Update UUT back to testing firmware version
"""


import os
import time
import urllib2
import requests
import xml.etree.ElementTree as ET
from decimal import *
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields

# old_nas = ['LT4A', 'KC2A', 'BZVM']
local_drive_folder = "/tmp/mfu"


class MandatoryFirmwareUpdateAuto(TestClient):

    previous_released_fw_build = '00.00.001'
    current_firmware = ''
    device_ip = ''
    product = ''

    def run(self):
        try:

            self.preparation()
            self.log.info('Begin Mandatory FW Update - Online Server')
            self.click_element(self.Elements.SETTINGS_UTILITY_RESTORE)
            if self.is_element_visible('popup_apply_button'):
                self.click_element('popup_apply_button')
            self.log.info('5 minutes wait: factory restore in progress...')
            time.sleep(300)
            self.close_webUI()

            self.wait_for_factory_restore_to_finish()
            self.set_ssh(enable=True)
            self.get_the_latest_released_fw_build()
            self.get_the_latest_released_fw_dwnld_link()
            self.get_the_latest_released_fw_rels_note()
            self.download_the_latest_released_build()
            self.chk_mndtory_fw_updt_flg()
            self.set_mndtory_fw_updt_flg()
            self.clear_eula()
            self.spoof_fw_version()
            self.validate_eula_acceptance()
            self.accept_eula_thru_ui(use_UI=True)
            result = self.validate_mfu_pop_up_menu()
            self.log.info('result[0] and result[1]: {0} and {1}'.format(result[0], result[1]))
            if result[0] == 0 and result[1] == 0:
                self.update_and_install_auto()
            self.log.info('End Mandatory FW Update - Online Server')

        except Exception, ex:
            self.log.exception('Failed to Run Mandatory Firmware Update Test Case - Test could not run.\n' + str(ex))
        finally:
            self.cleaning()
            time.sleep(120)
            self.end()

    def preparation(self):
        global sleeping_time    # this is a sleeping time after a firmware update + Rebooting
        global model_number
        global update_server
        global actual_fw_ver
        global sleeping_time

        actual_fw_ver = None
        update_server = None
        model_number = None
        sleeping_time = 480

        global current_firmware
        global device_ip
        global product

        current_firmware = self.uut[self.Fields.actual_fw_version]
        device_ip = self.uut[self.Fields.internal_ip_address]
        product = self.uut[self.Fields.product]

        try:
            actual_fw_ver = self.uut[Fields.actual_fw_version]
            self.log.info('actual_fw_ver: {0}'.format(actual_fw_ver))
            system_info = self.get_system_information()
            self.log.info('system_info: {0}'.format(system_info))

            if system_info[0] == 0:
                model_number = self.get_xml_tag(xml_content=system_info[1].content, tag='model_number')
                self.log.info('model number: {0}'.format(model_number))

                """
                if model_number in old_nas:
                    self.log.info('Problem with LT4A, KC2A, BZVM - use 1.05.21 for now')
                    self.previous_released_fw_build = '1.05.21'
                """

                update_server = 'http://support.wdc.com/nas/list.asp?devtype='+model_number+'&devfw=' + \
                                self.previous_released_fw_build + '&devlang=eng'
                self.log.info('Update server URL: {0}'.format(update_server))

                self.log.info('Successfully obtained the model number and was able to build the server query URL')
                if model_number == 'LT4A' or model_number == 'BWZE':
                    sleeping_time = 480
                else:
                    sleeping_time = 320
        except Exception, ex:
            self.log.error('Could not obtain the model number of the device and '
                           'build the server query URL for the latest F/W' + str(ex))
            raise

    def get_the_latest_released_fw_build(self):

        global latest_release_fw_build
        latest_release_fw_build = None

        try:
            f = urllib2.urlopen(url=update_server)
            fcontent = f.read()
            latest_release_fw_build = self.get_xml_tag(xml_content=fcontent, tag='Version')
            if latest_release_fw_build is not None and latest_release_fw_build != '':
                self.log.info('STEP1: Successfully obtained the latest F/W build NUMBER \"{0}\" '
                              'from WDC website - PASSED'.format(latest_release_fw_build))
                if actual_fw_ver is not None and actual_fw_ver != '':
                    act_fw_float = Decimal(actual_fw_ver.replace('.', ''))
                    query_fw_float = Decimal(latest_release_fw_build.replace('.', ''))
                    difference_between_fw_ver = act_fw_float - query_fw_float
                    if difference_between_fw_ver > 3:
                        self.log.warning('CAUTION: The Build from the query (support.wdc.com) is older than three '
                                         'versions\n'
                                         'Actual FW on UUT = \"{0}\" vs. the query FW = '
                                         '\"{1}\"'.format(actual_fw_ver, latest_release_fw_build ))
                pass
            else:
                self.log.error('STEP1: Could NOT obtain the latest F/W build NUMBER from WDC website - FAILED')
        except Exception, ex:
            self.log.error('STEP1: Failed to get the Firmware Build Number\n' + str(ex))
            raise

    def get_the_latest_released_fw_dwnld_link(self):
        global latest_release_fw_download_link
        latest_release_fw_download_link = None
        try:
            f = urllib2.urlopen(url=update_server)
            fcontent = f.read()
            latest_release_fw_download_link = self.get_xml_tag(xml_content=fcontent, tag='Image')

            if latest_release_fw_download_link is not None and latest_release_fw_download_link != '':
                self.log.info('firmware download link: {0}'.format(latest_release_fw_download_link))
                self.log.info('STEP1: Successfully obtained the latest F/W build '
                              'Download Link from WDC website - PASSED')
            else:
                self.log.error('STEP1: Could NOT obtain the latest F/W build link from WDC website - FAILED')
        except Exception, ex:
            self.log.error('STEP1: Failed to get the Firmware Download Link\n' + str(ex))
            raise

    def get_the_latest_released_fw_rels_note(self):
        catch_word = 'Firmware Release'
        global latest_release_fw_release_note
        latest_release_fw_release_note = None

        try:
            f = urllib2.urlopen(url=update_server)
            fcontent = f.read()
            latest_release_fw_release_note = self.get_xml_tag(xml_content=fcontent, tag='ReleaseNotes')
            if latest_release_fw_release_note != '' and latest_release_fw_release_note is not None:
                release_note_content = self.get_page_contents(url=latest_release_fw_release_note)
                if release_note_content[0] == 0:
                    self.log.info('STEP2: Successfully validated the Release Note link - PASSED')
                    for i in release_note_content[2]:
                        if catch_word in i:
                            indeks = i.find(catch_word)
                            page = str(i).lower()
                            page = page[indeks: indeks+25]

                            self.log.warning("STEP2: Found '{0}' on Release Note Page - PASSED".format(page))

                            break
                else:
                    self.log.error('STEP2: Could not get the content of the Release Note page URL - FAILED\n')

            else:
                self.log.warning('STEP2: Release Note link Does NOT exist on the WDC support server at this time\n')
        except Exception, ex:
            self.log.error('STEP2: Failed to get the Firmware Release Note Link - FAILED\n' + str(ex))
            pass

    def download_the_latest_released_build(self):
        global build_filename
        build_filename = None
        try:
            dir_exist = os.path.isdir(local_drive_folder)
            if not dir_exist:
                os.mkdir(local_drive_folder)
            tuple_url = urllib2.urlparse.urlparse(url=latest_release_fw_download_link, scheme="/")
            build_filename = tuple_url[2]
            build_filename = build_filename.split("/")
            binary_file = urllib2.urlopen(url= latest_release_fw_download_link)
            output = open(local_drive_folder+os.sep+build_filename[2], 'wb')
            output.write(binary_file.read())
            output.close()
            time.sleep(10)
            if os.path.exists('/tmp/mfu/'+build_filename[2]):
                self.log.info('STEP3: Successfully downloaded the latest F/W build \"{0}\" from WDC website and stored '
                              'in \"{1}\" - PASSED'.format(build_filename[2], local_drive_folder))
                pass
            else:
                self.log.critical('STEP3: Could NOT download the latest F/W build \"{0}\" '
                                  'from WDC website - FAILED'.format(build_filename[2]))
        except Exception, ex:
            self.log.error('STEP3: Failed to DOWNLOAD the latest build from WDC website\n' + str(ex))
            raise

    def chk_mndtory_fw_updt_flg(self):
        try:
            self.execute('chk_update_firmware -r')
            result_chk = self.execute('cat /tmp/flash_update_firmware')

            if result_chk[1] == '0':
                self.log.info('STEP4: Check the Mandatory FW Update Flag Value: Successfully received ZERO as '
                              'returned value - PASSED')
                pass
            else:
                self.log.warning('STEP4: Check the Mandatory FW Update Flag Value: Did not return ZERO as '
                                 'expected - FAILED')
                pass
        except Exception, ex:
            self.log.warning('STEP4: Failed to CHECK the Mandatory FW Update Flag\n' + str(ex))
            pass

    def set_mndtory_fw_updt_flg(self):
        try:
            self.execute('chk_update_firmware -w 1')
            time.sleep(5)
            self.execute('chk_update_firmware -r')
            time.sleep(4)
            result_set = self.execute('cat /tmp/flash_update_firmware')
            if result_set[1] == '1':
                self.log.info('STEP5: Set the Mandatory FW Update Flag Value: Successfully received ONE as returned '
                              'value - PASSED')
                pass
            else:
                self.log.warning('STEP5: Set the Mandatory FW Update Flag Value: Did not return value ONE as '
                                 'expected - FAILED')
                pass
        except Exception, ex:
            self.log.critical('STEP5: Failed to SET the Mandatory FW Update Flag \n' + str(ex))
            pass

    def clear_eula(self):
        try:
            self.execute('xmldbc -s eula 0')
            time.sleep(3)
            self.execute('rm /etc/.eula_accepted')
            time.sleep(2)
            self.log.info('STEP6-1: Successfully CLEARED the EULA acceptance - PASSED')
        except Exception, ex:
            self.log.critical('STEP6-1: Failed to CLEAR the EULA Acceptance - FAILED\n' + str(ex))
            pass

    def spoof_fw_version(self):
        try:
            result_spoof = self.execute('auto_fw -s %s' % self.previous_released_fw_build)
            if result_spoof[1] == self.previous_released_fw_build:
                self.log.info('STEP6-2: Successfully spoofed the F/W version to \"{0}\" value - '
                              'PASSED'.format(self.previous_released_fw_build))
                pass
            else:
                self.log.warning('STEP6-2: Could NOT spoof the F/W version to \"{0}\" value - '
                                 'FAILED'.format(self.previous_released_fw_build))
                pass
        except Exception, ex:
            self.log.warning('****STEP6-2****Unable to spoof the F/W version**** \n' + str(ex))
            pass

    def validate_eula_acceptance(self):
        try:
            result_eula = self.get_eula_status()
            error_message = self.get_xml_tag(xml_content=result_eula[1].content, tag='error_message')
            if result_eula[0] == 0 and error_message == 'EULA not accepted':
                self.log.info('STEP7-1: Successfully received the EULA status from REST call - Response: \"{0}\"  '
                              '- PASSED'.format(error_message))
                pass
            else:
                self.log.warning('STEP7-1: Failed to receive the EULA status Thru REST call - Response: \"{0}\"  '
                                 '- FAILED'.format(result_eula[1].content))
        except Exception, ex:
            self.log.critical('STEP7-1: Failed to VALIDATE the EULA acceptance status through REST call \n' + str(ex))
            pass

    def accept_eula_thru_ui(self, use_UI):
        global MFU_menu
        MFU_menu = None

        try:
            time.sleep(1)
            result = self.accept_eula(use_UI=True)
            if result == 0:
                self.log.error('STEP7-2: Accepted EULA page through web UI and Main Page is now available but not '
                               'Mandatory F/W Update - PASSED')
            if result == 2:
                self.log.info('STEP7-2: Successfully accepted EULA page through web UI and Mandatory F/W update Menu '
                              'is now available - PASSED')
                MFU_menu = True
            if result == 1 or result is None:
                self.log.error('STEP7-2: Failed to accept EULA page through web UI - FAILED')
        except Exception, ex:
            self.log.exception('STEP7-2: Failed to get accept EULA REST CALL return \n') + str(ex)

    def validate_mfu_pop_up_menu(self):
        curr_match = None
        late_match = None
        try:
            if MFU_menu:
                global popup_current_fw

                popup_current_fw = self.get_text(locator='id_eula_current')
                popup_latest_fw = self.get_text('id_e_latest')
                if popup_current_fw == self.previous_released_fw_build:
                    self.log.info('STEP7-3: The current F/W \"{0}\" matches the spoofed version which was \"{1}\" - '
                                  'PASSED \n'.format(popup_current_fw, self.previous_released_fw_build))
                    curr_match = 0
                else:
                    self.log.error('STEP7-3: The current F/W \"{0}\" DOES NOT match the spoofed version which was '
                                   '\"{1}\" - FAILED\n'.format(popup_current_fw, self.previous_released_fw_build))
                    curr_match = 1
                if popup_latest_fw == latest_release_fw_build:
                    self.log.info('STEP7-4: The latest FW \"{0}\" matches the obtained version from WDC support Website'
                                  ' which was \"{1}\" - PASSED\n'.format(popup_latest_fw, latest_release_fw_build))
                    late_match = 0
                else:
                    self.log.error('STEP7-4: The latest FW \"{0}\" DOES NOT match the obtained version from WDC Website'
                                   ' which was \"{1}\"- FAILED\n'.format(popup_latest_fw, latest_release_fw_build))
                    late_match = 1
            else:
                self.log.critical('STEP7-3: The POP-UP menu for Mandatory Firmware Update did not show up after EULA '
                                  'acceptance" - FAILED\nPlease Make sure you have the latest Firmware on the Unit\n '
                                  'and restore the device to factory defaults and try again\n')
                late_match = curr_match = 1
        except:
            self.log.error('STEP 7-3 and 7-4: Could not read current/latest values from the Mandatory Pop-Up menu" '
                           '- FAILED')
            late_match = curr_match = 1
        return curr_match, late_match

    def update_and_install_auto(self):
        global latest_release_fw_build
        global new_update_FW_ver_auto
        new_update_FW_ver_auto = None

        try:
            self.log.info('STEP8: Starting the updating process of latest F/W by fetching it from Internet and then '
                          'rebooting the system\n Please WAIT.....')
            self.click_element('eula_fwUpdateInstall_button')
            time.sleep(10)
            if self.is_element_visible('eula_fwUpdateInstall_button'):
                self.log.info('eula_fwUpdateInstall_button visible, click again')
                self.click_element('eula_fwUpdateInstall_button')

            self.log.info('5 minutes wait: firmware update in progress...')
            time.sleep(300)
            success_on_update_and_install = self.wait_for_factory_restore_to_finish()
            self.log.info('success_on_update_and_install: {0}'.format(success_on_update_and_install))

            self.close_webUI()

            # give another minute as test fail due to 'Connection refused' for Yellowstone ??
            # time.sleep(60)

            try:
                xml_content = self.get_system_state()
                status = self.get_xml_tag(xml_content, 'status')
                self.log.info('Current system status: {0}'.format(status))
            except Exception, ex:
                self.log.exception('Fail to get system state\n' + str(ex))

            current_uut_firmware = self.get_firmware_version_number()
            self.log.info('current_uut_firmware and latest_release_fw_build: {0} and '
                          '{1}'.format(current_uut_firmware, latest_release_fw_build))
            if current_uut_firmware == latest_release_fw_build:
                self.log.info('STEP8: Correct firmware version is updated: {0}'.format(current_uut_firmware))
            else:
                self.log.error('STEP8: Incorrect firmware version is updated: {0}'.format(current_uut_firmware))

        except Exception, ex:
            self.close_webUI()
            self.log.exception('STEP8: Could not validate the F/W version after update and rebooting the system '
                               '- FAILED \n' + str(ex))
            pass

    """
    def firmware_update_nexus(self):
        global current_firmware
        global device_ip
        global product

        self.log.info('current_firmware: {0}'.format(current_firmware))
        self.log.info('device_ip: {0}'.format(device_ip))
        self.log.info('product: {0}'.format(product))

        a, b, c = current_firmware.split(".")
        firmware_version = a + '_' + b
        self.log.info('firmware_version: {0}'.format(firmware_version))

        group = self.uut[self.Fields.group]

        firmware_url = 'http://repo.wdc.com/service/local/repositories/projects/content/' + product + '-' + \
                       firmware_version + '/' + group + '/' + current_firmware + '/' + group + '_' \
                       + current_firmware + '.bin'
        self.log.info('firmware_url: {0}'.format(firmware_url))

        self.log.info('Updating firmware {0}'.format(current_firmware))
        self.firmware_update(firmware_url)

        self.log.info('5 minutes wait: firmware update in progress...')
        time.sleep(300)
        self.wait_for_factory_restore_to_finish()

        self.log.info('Verify correct firmware version is installed')
        firmware_number = self.get_firmware_version_number()
        if firmware_number == current_firmware:
            self.log.info('Correct firmware: {0}'.format(firmware_number))
        else:
            self.log.error('Incorrect firmware: {0}'.format(firmware_number))

    def firmware_update_nexus_requests(self):

        global current_firmware
        global device_ip
        global product

        self.log.info('current_firmware: {0}'.format(current_firmware))
        self.log.info('device_ip: {0}'.format(device_ip))
        self.log.info('product: {0}'.format(product))

        # wait until system ready
        while self.is_ready() is False:
            time.sleep(30)

        a, b, c = current_firmware.split(".")
        firmware_version = a + '_' + b
        self.log.info('firmware_version: {0}'.format(firmware_version))

        group = self.uut[self.Fields.group]

        firmware_url = 'http://repo.wdc.com/service/local/repositories/projects/content/' + product + '-' + \
                       firmware_version + '/' + group + '/' + current_firmware + '/' + group + '_' \
                       + current_firmware + '.bin'
        self.log.info('firmware_url: {0}'.format(firmware_url))

        self.log.info('Updating firmware {0}'.format(current_firmware))
        self.firmware_update(firmware_url)

        # update firmware for test unit
        update_command = 'http://' + device_ip + \
                         '/api/2.6/rest/firmware_update?rest_method=PUT&auth_username=admin&auth_password=&image=' \
                         + firmware_url
        self.log.info('update_command: {0}'.format(update_command))
        self.log.info('updating firmware...')
        try:
            r = requests.put(update_command)
        except requests.exceptions.ConnectionError as e:
            self.log.info(e)
            pass

        self.log.info('updating firmware REST call return value: {0}'.format(r))
        self.log.info('r result: {0}'.format(r.content))

        # wait 5 minutes before checking system status
        # looking for 'ready' status, looping otherwise
        self.log.info('waiting 5 minutes for firmware update...')
        time.sleep(300)
        system_state_command = 'http://' + device_ip + '/api/2.6/rest/system_state'
        count = 0
        still_waiting = True

        while still_waiting:
            try:
                r = requests.get(system_state_command)
            except requests.exceptions.ConnectionError as e:
                self.log.info(e)
                pass

            self.log.info('system state return value: {0}'.format(r))
            self.log.info('r result: {0}'.format(r.content))

            my_et = ET.fromstring(r.content)

            try:
                element = my_et.iter('status').next()
                text = None
                if element.text:
                    text = element.text.strip()
                my_status = text
            except StopIteration, ex:
                self.log.info(ex)
                pass

            self.log.info('device status: {0}'.format(my_status))

            # exit loop when system is ready
            if my_status == 'ready':
                still_waiting = False

            # keep waiting unless it passes 10 minutes
            time.sleep(60)
            count += 1
            self.log.info('Waiting... loop count: {0}'.format(count))
            if count > 10:
                self.log.info('FAILED: Unit not ready after 10 minutes')
                break

        # Verify correct firmware is on test unit
        version_command = 'http://' + device_ip + '/api/2.6/rest/version'
        self.log.info('versionCommand: {0}'.format(version_command))

        try:
            r = requests.get(version_command)
        except requests.exceptions.ConnectionError as e:
            self.log.info(e)
            pass

        self.log.info('versionCommand return value: {0}'.format(r))
        self.log.info('r result: {0}'.format(r.content))

        my_et = ET.fromstring(r.content)

        uut_current_firmware = ''

        try:
            element = my_et.iter('firmware').next()
            text = None
            if element.text:
                text = element.text.strip()
            uut_current_firmware = text
        except StopIteration, ex:
            self.log.info(ex)
            pass

        self.log.info('uut_current_firmware: {0}'.format(uut_current_firmware))

        if uut_current_firmware == current_firmware:
            self.log.info('correct firmware on test unit: {0}'.format(uut_current_firmware))
            return True
        else:
            self.log.error('incorrect firmware on test unit: {0}'.format(uut_current_firmware))
            return False
    """


    def download_firmware_from_nexus(self, fimware_link):

        try:
            dir_exist = os.path.isdir(local_drive_folder)
            if not dir_exist:
                os.mkdir(local_drive_folder)

            tuple_url = fimware_link.split('/')
            file_name_tuple = 11
            firmware_file = tuple_url[file_name_tuple]

            binary_file = urllib2.urlopen(url=fimware_link)
            output = open(local_drive_folder + os.sep + firmware_file, 'wb')
            output.write(binary_file.read())
            output.close()
            if os.path.exists(local_drive_folder + os.sep + firmware_file):
                self.log.info('Successfully downloaded firmware \"{0}\" from WDC website and stored '
                              'in \"{1}\" - PASSED'.format(firmware_file, local_drive_folder))
                return firmware_file
            else:
                self.log.critical('Failed to download firmware \"{0}\" - FAILED'.format(firmware_file))
        except Exception, ex:
            self.log.error('Failed to download firmware from Nexus\n' + str(ex))
            raise

    def get_nexus_link(self, current_firmware):
        # uut_current_firmware = self.uut[self.Fields.actual_fw_version]
        uut_current_firmware = current_firmware
        uut_product = self.uut[self.Fields.product]

        self.log.info('current_firmware: {0}'.format(uut_current_firmware))
        self.log.info('product: {0}'.format(uut_product))

        a, b, c = uut_current_firmware.split(".")
        firmware_version = a + '_' + b
        self.log.info('firmware_version: {0}'.format(firmware_version))

        group = self.uut[self.Fields.group]

        uut_product = uut_product.replace(' ', '')
        self.log.info('remove space for KC and Grand Teton: {0}'.format(product))

        # if uut_product == 'Kings Canyon':
        #     uut_product = 'KingsCanyon'

        firmware_link = 'http://repo.wdc.com/service/local/repositories/projects/content/' + uut_product + '-' + \
                        firmware_version + '/' + group + '/' + uut_current_firmware + '/' + group + '_' \
                        + uut_current_firmware + '.bin'
        self.log.info('firmware_url: {0}'.format(firmware_link))

        return firmware_link

    def update_firmware_from_file(self, file_path, firmware_file):
        try:
            file_path = file_path
            self.log.info("file_path: {0}".format(file_path))
            time.sleep(5)
            self.scp_file_to_device(files=file_path, remote_path='/shares/Public')
            result = self.firmware_update_by_file(firmware_file)
            time.sleep(5)
            if result == 0:
                self.log.info('Updating firmware by file \"{0}\" through REST call...'.format(firmware_file))
            else:
                self.log.error('Failed to update firmware by file \"{0}\" through REST call...'.format(firmware_file))
        except Exception, ex:
            self.log.error('Failed to update firmware by file - through REST call\n' + str(ex))
            pass

        self.log.info('5 minutes wait: firmware update in progress...')
        time.sleep(300)
        success_on_update_and_install = self.wait_for_factory_restore_to_finish()
        self.log.info('success_on_update_and_install: {0}'.format(success_on_update_and_install))

        current_uut_firmware = self.get_firmware_version_number()
        return current_uut_firmware

    def cleaning(self):
        global current_firmware
        try:

            dir_exist = os.path.isdir(local_drive_folder)
            if dir_exist:
                filelist = [f for f in os.listdir(local_drive_folder) if f.endswith(".bin")]
                for f in filelist:
                    os.remove(local_drive_folder+os.sep+f)
                self.log.info('Successfully cleaned up the downloaded f/w image from the local system')

            current_firmware_on_uut = self.get_firmware_version()
            self.log.info('current_firmware_on_uut: {}'.format(current_firmware_on_uut))
            if current_firmware_on_uut == current_firmware:
                self.log.info('Unit already has testing firmware version')
            else:
                loop_count = 0
                successfully_firmware_update = False
                while loop_count < 3:
                    # update system back to testing firmware
                    fimware_link = self.get_nexus_link(current_firmware)
                    firmware_file = self.download_firmware_from_nexus(fimware_link)
                    file_path = local_drive_folder + os.sep + firmware_file
                    current_uut_firmware = self.update_firmware_from_file(file_path, firmware_file)

                    self.log.info('current_uut_firmware and current_firmware: {0} and '
                                  '{1}'.format(current_uut_firmware, current_firmware))
                    if current_uut_firmware == current_firmware:
                        self.log.info('Correct firmware version is updated: {0}'.format(current_uut_firmware))
                        loop_count = 3
                        successfully_firmware_update = True
                    else:
                        self.log.warning('Incorrect firmware version is updated: {0}'.format(current_uut_firmware))
                        loop_count += 1
                if successfully_firmware_update:
                    self.log.info('successfully_firmware_update: {0}'.format(successfully_firmware_update))
                else:
                    self.log.error('successfully_firmware_update: {0}'.format(successfully_firmware_update))
        except Exception, ex:
            self.log.warning('Failed to clean up the f/w files from local system' + str(ex))

        try:
            self.run_on_device('rm /shares/Public/*.bin')
            self.log.info('Successfully cleaned up the copied f/w image from CacheVolume location on DUT')
        except Exception, ex:
            self.log.warning('Failed to clean up the f/w files from CacheVolume location on DUT' + str(ex))

MandatoryFirmwareUpdateAuto()