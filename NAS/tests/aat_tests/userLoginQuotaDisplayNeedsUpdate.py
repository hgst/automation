""" User Login - Quota Display(Needs update) (MA-209)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=20018&execView=execDetails&view=details&tdetab=0&pltab=steps&fiTpId=13015&pId=50&nTP=117873&etab=8
    1) Set a Quota for the user you're logging in as
    2) This Quota will be the maximum space the user should have access to
    3) The Capacity meter on the User's login should show the correct size as set by the Quota

    Notes:
      The value you set in the quota page needs to be re-calculated.
      (Use 1024 to convert in Bytes then use 1000 converted back to MB/GB/TB)
      For example:
         Set: 100 MB
         UI: (100 * 1024 * 1024)/(1000*1000) = 104.8576 MB, So UI will show 104 MB

    Automation: Full

    Not supported product:
        Glacier (does not support user Login)

    Support Product:
        Lightning
        YellowStone
        Yosemite
        Sprite
        Aurora
        Kings Canyon
        Grand Teton

    Defect: (Resolved by deferred)
      SKY-2641 (The capacity size display in homepage is different from user quota)

    Test Status:
        Local Lightning: N/A (FW: 2.10.147)
"""
from testCaseAPI.src.testclient import TestClient
import global_libraries.wd_exceptions as exceptions
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import os


class userLoginQuotaDisplayNeedsUpdate(TestClient):
    def run(self):
        if self.get_model_number() in ('GLCR',):
            self.log.info("=== Glacier de-feature does not support user login, skip the test ===")
            return
        self.log.info('############### (MA-209) User Login - Quota Display(Needs update) TESTS START ###############')
        new_user_name = 'ma209'
        new_user_pwd = 'ma209'
        amount = 100
        unit = 'MB'
        try:
            self.delete_all_groups()
            self.delete_all_shares()
            try:
                self.delete_all_users(use_rest=False)
            except exceptions.InvalidDeviceState:
                self.log.info("User list is empty")
            self.create_new_user(username=new_user_name, password=new_user_pwd)
            self.set_user_quota(user_name=new_user_name, amount=amount, unit=unit)
            self.verify_user_quota(user_name=new_user_name, user_pwd=new_user_pwd, amount=amount, unit=unit)
        except Exception as e:
            self.log.error("Test Failed: (MA-209) Quota Display(Needs update) test, exception: {}".format(repr(e)))
        finally:
            self.log.info("====== Clean Up section ======")
            try:
                self.delete_all_users(use_rest=False)
            except exceptions.InvalidDeviceState:
                self.log.info("User list is empty")
            self.log.info('############### (MA-209) User Login - Quota Display(Needs update) TESTS END ###############')

    def create_new_user(self, username, password=None):
        """
            create_user() in framework uses RESTful API could cause some issue in quota set
            Need to use Web for user creation
        :param username: New user name  (only allow lower case)
        :param password: New user password
        """
        try:
            self.get_to_page('Users')
            self.click_wait_and_check('nav_users_link', 'users_createUser_link', visible=True)
            self.click_wait_and_check('users_createUser_link', 'users_userName_text', visible=True)
            self.input_text_check('users_userName_text', username.lower(), do_login=False)
            if password:
                self.input_text_check('users_newPW_password', password, do_login=False)
                self.input_text_check('users_comfirmPW_password', password, do_login=False)
            self.click_wait_and_check('users_addUserSave_button', visible=False)
            time.sleep(5)  # Wait for updating
        except Exception as e:
            self.log.error("ERROR: Fail to create new user: {}, exception: {}".format(username, repr(e)))
        else:
            self.log.info("New user: {} is created successfully!!".format(username))
        finally:
            self.reload_page()

    def verify_user_quota(self, user_name, user_pwd, amount, unit):
        """
        :param user_name: non-admin user name
        :param user_pwd: non-admin user password
        :param amount: integer
        :param unit: 'GB' or 'MB' or 'TB'
        :return:
        """
        unit_table = {
            'MB': 2,   # 1024 * 1024
            'GB': 3,
            'TB': 4
        }
        try:
            self.webUI_user_login(username=user_name, userpwd=user_pwd)
            time.sleep(10)  # Wait for page load
            cap_value = self.get_text('myhome_b4_capacity_info')
            unit_value = self.get_text('myhome_b4_capacity_size')
            self.log.info("UI Cap: {}, unit: {}".format(cap_value, unit_value))
            total_amount = self.calculate_quota_size(amount=amount, unit=unit)
            self.log.info("Correct amount: {}".format(total_amount))
            if cap_value <= str(total_amount) and unit_value == unit:
                self.log.info("PASS: User quota matches")
            else:
                self.log.error("ERROR: User quota set to {}{} "
                               "which does not match {}{}".format(cap_value, unit_value, total_amount, unit))
                self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            time.sleep(5)
        except Exception as e:
            self.log.error("ERROR: Verify User quota failed, exception: {}".format(repr(e)))
            self.take_screen_shot(prefix='userQuotaFailed')
        finally:
            self.close_webUI()

    def calculate_quota_size(self, amount, unit):
        unit_table = {
            'MB': 2,   # 1024 * 1024
            'GB': 3,
            'TB': 4
        }
        volumes = self.get_raid_volumes()
        if not volumes:
            self.log.error("ERROR: No available RAID volumes")
            return
        amount = int(volumes) * int(amount)
        adjusted_amount = int((amount * pow(1024, unit_table[unit]))/(pow(1000, unit_table[unit])))
        return adjusted_amount

    def set_user_quota(self, user_name, amount, unit='GB'):
        """
        :param amount: The size you'd like to set
        :param unit: Could be either 'GB', 'TB', 'MB'
        """
        valid_units = ('GB', 'TB', 'MB')
        try:
            if unit not in valid_units:
                self.log.error("ERROR: Input the wrong unit size {}".format(unit))
                return
            if type(amount) != int:
                self.log.error("ERROR: Input the wrong amount type {}, should be integer".format(type(amount)))
                return
            self.log.info("=== Setting User Quota ===")
            self.get_to_page('Users')
            self.click_wait_and_check('users_user_{}'.format(user_name),
                                      'css=#users_editPW_switch+span .checkbox_container', visible=True)
            self.click_wait_and_check('users_editUserQuota_link', 'uesrs_modQuotaSave_button', visible=True)
            volumes = self.get_raid_volumes()
            self.log.info("Detect {} raid volumes".format(volumes))
            for i in range(1, volumes+1):
                self.input_text_check('users_v{}Size_text'.format(i), str(amount), do_login=False)
                self.log.info("Set Volume_{} size: {}".format(i, amount))
                self.click_link_element('quota_unit_main_{}'.format(i), unit)
            self.click_wait_and_check('uesrs_modQuotaSave_button', visible=False)
            time.sleep(5)
        except Exception as e:
            self.log.exception("ERROR: Fail to set the quota for user {}, exception: {}".format(user_name, repr(e)))
        finally:
            self.log.info("Logout admin from UI")
            self.click_wait_and_check('id_logout', 'home_logout_link', visible=True)
            self.click_wait_and_check('home_logout_link', visible=False)
            # self.close_webUI()

    def get_raid_volumes(self):
        """
            Filter out my passport, only counts Volume_X
        :return: number of raid volumes
        """
        volume_list = self.get_xml_tags(self.get_volumes()[1], 'base_path')
        count = 0
        for v in volume_list:
            if 'Volume_' in v:
                self.log.debug("Available: {}".format(v))
                count += 1
        return count

    def webUI_user_login(self, username, userpwd):
        """
        :param username: user name you'd like to login
        :param userpwd: user password for user name account
        """
        try:
            self.log.info("Going to login as non-admin user: {}".format(username))
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.input_text_check('login_pw_password', userpwd, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("PASS: Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.error("ERROR: Fail to login by user {}, exception: {}".format(username, repr(e)))

userLoginQuotaDisplayNeedsUpdate()
