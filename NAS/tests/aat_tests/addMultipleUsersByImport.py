"""
title           :addMultipleUsersByImport.py
description     :To verify if multiple users can be created by importing the user list file
author          :yang_ni
date            :2015/05/19
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
import os
from testCaseAPI.src.testclient import Elements as E

class AddUserByImport(TestClient):

    def run(self):
        if self.get_model_number() in ('GLCR',):
           self.log.info("=== Glacier de-feature does not support creating multiple users, skip the test ===")
           return

        testname = 'Add Multiple users - Import Users'
        self.start_test(testname)

        try:
            self.delete_all_users()

            '''
            Prepare the User List file
            '''
            self.log.info("***Writing the user information into the user list***")
            with open('import_3users.txt','a') as f:
                f.write('user0001/testtest/////0:0:0:0\n')
                f.write('user0002/testtest/////0:0:0:0\n')
                f.write('user0003/testtest/////0:0:0:0\n')
            self.log.info("***Finish writing the user list, close the file***")
            user_list = os.path.join(os.getcwd(),'import_3users.txt')

            '''
            Import the User List which contains 3 users
            '''
            self.access_webUI()
            time.sleep(5)
            self.get_to_page('Users')
            time.sleep(5)
            self.click_element(E.USERS_BUTTON)
            self.click_element(E.USERS_ADD_MULTIPLE,timeout=15)
            self.click_element(E.USERS_CREATE_IMPORT_USERS,timeout=15)
            self.click_element(E.USERS_CREATE_MULTIPLE_NEXT1,timeout=15)

            self.log.info("Upload User Lists ( 3 users)")
            self.press_key('users_impUsersFile_text', user_list)
            time.sleep(8)
            self.click_element("users_impNext1_button",timeout=20)
            time.sleep(15)
            self.click_element("users_impSave_button",timeout=20)
            time.sleep(15)

            self.log.info("*** Verify if all the 3 users are created ***")
            self.element_should_be_visible('users_user_user0001')
            self.element_should_be_visible('users_user_user0002')
            self.element_should_be_visible('users_user_user0003')
            self.log.info("*** All the 3 users are created ***")
            time.sleep(10)
            self.pass_test(testname, 'User list is successfully imported')

        except Exception as e:
            self.fail_test(testname, "Test FAILED: exception: {}".format(repr(e)))

        finally:
            self.delete_all_users()
            self.log.info('*** Remove the user list file ***')
            os.remove(user_list)

# This constructs the AddUserByImport() class, which in turn constructs TestClient() which triggers the AddUserByImport.run() function
AddUserByImport()