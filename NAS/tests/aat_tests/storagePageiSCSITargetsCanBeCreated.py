""" Storage-Page-iSCSI Targets Can Be Created

    @Author: Lee_e

    Objective: check if iSCSI Targets Can Be Created

    Automation: Full

    Supported Products:
        All

    Test Status:
              Local Lightning : Pass (FW: 2.10.159)
                    Lightning : Pass (FW: 2.10.164 & 2.10.182)
                    KC        : Pass (FW: 2.10.159)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.164 & 2.10.182)
                              : Yosemite     - PASS (FW: 2.10.164 & 2.10.180)
                              : Kings Canyon - PASS (FW: 2.10.165 & 2.10.186)
                              : Sprite       - PASS (FW: 2.10.164 & 2.10.182)
                              : Zion         -  non-support
                              : Aurora       - PASS (FW: 2.10.164 & 2.10.180)
                              : Yellowstone  - PASS (FW: 2.10.157 & 2.10.161 & 2.10.182)
                              : Glacier      - non-support
                              : Grand Teton  - non-support

"""
from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.ui_map import Page_Info
import time
import requests

default_web_access_timeout_value = 5
alias = 'iscsi-test'
iscsi_size = '1'
capacity = 'GB'

class StoragePageiSCSITargetsCanBeCreated(TestClient):
    def run(self):
        model = self.get_model_number()
        non_support = ['GLCR', 'BZVM', 'BWVZ']
        if model in non_support:
            self.log.info('Model is {0} in not support iSCSI list{1}'.format(model, non_support))
            return
        self.delete_iscsi_targe(alias=alias)
        self.webUI_admin_login(username=self.uut[self.Fields.default_web_username])
        self.created_iscsi_targets(alias=alias, iscsi_size=iscsi_size, capacity=capacity)
        self.check_iscsi_targets(iscsi_target_name=alias)

    def tc_cleanup(self):
        self.delete_iscsi_targe(alias=alias)
        self.iscsi_config(enable='disable')

    def created_iscsi_targets(self, alias=None, iscsi_size=None, capacity='TB'):
        self.start_test('created iSCSI targets: {0}'.format(alias))
        self.click_wait_and_check(Page_Info.info['Storage'].link, self.Elements.STORAGE_ISCSI)
        self.click_wait_and_check(self.Elements.STORAGE_ISCSI, self.Elements.STORAGE_ISCSI_SWITCH, timeout=10)
        self.click_wait_and_check(self.Elements.STORAGE_ISCSI_SWITCH, 'css=#storage_iscsi_switch+span .checkbox_container .checkbox_on')
        self.click_wait_and_check(self.Elements.STORAGE_ISCSI_TARGET_CREATE_BUTTON, self.Elements.STORAGE_ISCSI_TARGET_ALIAS_TEXT)
        self.input_text_check(self.Elements.STORAGE_ISCSI_TARGET_ALIAS_TEXT, alias)
        self.input_text_check(self.Elements.STORAGE_ISCSI_TARGET_SIZE_TEXT, iscsi_size)
        self.log.info('capacity: ' + str(self.get_text(self.Elements.STORAGE_ISCSI_TARGET_CAPACITY_LIST)))
        if capacity not in self.get_text(self.Elements.STORAGE_ISCSI_TARGET_CAPACITY_LIST):
            self.click_wait_and_check(self.Elements.STORAGE_ISCSI_TARGET_CAPACITY_LIST, 'link='+capacity)
            self.click_wait_and_check('link='+capacity, self.Elements.STORAGE_ISCSI_TARGET_NEXT1)
        self.click_wait_and_check(self.Elements.STORAGE_ISCSI_TARGET_NEXT1, self.Elements.STORAGE_ISCSI_TARGET_NEXT2)

        try:
            self.click_wait_and_check(self.Elements.STORAGE_ISCSI_TARGET_NEXT2, self.Elements.UPDATING_STRING, visible=False, timeout=60)
            self.pass_test('created iSCSI targets: {0}'.format(alias))
        except Exception as e:
            self.take_screen_shot('create_iSCSI_targets')
            self.fail_test('created iSCSI targets:{0} with exception {1}'.format(alias, repr(e)))

    def check_iscsi_targets(self, iscsi_target_name=None):
        self.start_test('check iSCSI targets:{0}'.format(iscsi_target_name))
        try:
            value = self.get_text('storage_iscsiTargetName1_value')
            if iscsi_target_name in value:
                self.pass_test('check iSCSI target:{0} successfully'.format(iscsi_target_name))
            else:
                self.fail_test('Fail to check iSCSI target:{0}'.format(iscsi_target_name))
        except Exception as e:
            self.take_screen_shot('check_iSCSI_targets')
            self.fail_test('check iSCSI target: {0} with exception: {1}'.format(iscsi_target_name, repr(e)))

    def webUI_admin_login(self, username):
        """
        :param username: user name you'd like to login
        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot('login')

    def delete_iscsi_targe(self, alias):
        self.log.info('*** Delete iSCSI Target ***')
        head = 'iscsi_mgr.cgi'
        cmd = 'cgi_del_iscsi&del_file=1&alias='+alias
        self.send_cgi(cmd=cmd, url2_cgi_head=head)

    def iscsi_config(self, enable='enable'):
        self.log.info('*** iSCSI Config enable: {0} ***'.format(enable))
        head = 'iscsi_mgr.cgi'
        cmd = 'cgi_'+enable
        self.send_cgi(cmd=cmd, url2_cgi_head=head)

    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

StoragePageiSCSITargetsCanBeCreated()