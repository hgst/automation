'''
@Author: lo_va
wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Modes_Creation
'''
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import time
import random

class nasRaidModesCreation2DrivesRaid1wSpanning(TestClient):

    def run(self):

        drives_number = self.get_drive_count()
        if drives_number < 2:    # Check Drives counts
            raise wd_exceptions.InvalidDeviceState('Device was less than 2 drives, cannot test Creation Test!!!')
        file_prefix = 'rwfile{}'.format(random.randint(1,100))
        generate_size = 102400
        
        # Cleanup Drives first
        for i in range(2, drives_number):
            self.log.info('Clean up drive {}'.format(i))
            self.clean_up_drive(i+1)
        
        # Remove Drives
        for i in range(2, drives_number):
            self.log.info('Remove drive {}'.format(i))
            self.remove_drives(i+1)
        
        try:
            self.configure_raid(raidtype='RAID1', drive_bays=2, spanning=True, force_rebuild=True)
            generated_file = self.generate_test_file(generate_size, prefix=file_prefix)
        except Exception, ex:      
            self.log.exception('Failed to Configured RAID1 with 2 Drive, Exception: {}'.format(repr(ex)))
        
        # Insert Drives Back
        for i in range(2, drives_number):
            self.log.info('Insert drive {}'.format(i))
            self.insert_drives(i+1)
        
        # Wait for system protocol service ready
        time.sleep(10)
        
        # Read/Write Access test for RAID1 Volume
        sharename = 'Public'
        try:    
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(files=file_prefix+'0.jpg', share_name=sharename, delete_after_copy=True)
            self.log.info('Read/Write Access for RAID1 with 2 Drive, test PASS!!')
        except Exception, ex:      
            self.log.exception('Read/Write Access for RAID1 with 2 Drive, test Failed!!, Exception: {}'.format(repr(ex)))
            
        # Read/Write Access test for Spanning Volume
        sharenamespan = 'SpanningVolFolder'
        self.create_shares(number_of_shares=1, share_name=sharenamespan, volume_name='Volume_2')
        try:    
            self.write_files_to_smb(files=generated_file, share_name=sharenamespan, delete_after_copy=False)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharenamespan+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(files=file_prefix+'0.jpg', share_name=sharenamespan, delete_after_copy=True)
            self.log.info('Read/Write Access for RAID1 w/ Spanning volume with 2 Drive, test PASS!!')
        except Exception, ex:      
            self.log.exception('Read/Write Access for RAID1 w/ Spanning volume with 2 Drive, test Failed!!, Exception: {}'.format(repr(ex)))
            
    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('***** Hash check SUCCEEDED!! *****')
        else:
            self.log.error('***** Hash check FAILED!! *****')
            
    def clean_up_drive(self, drive_number):
        drive_mapping = self.get_drive_mappings()
        clean_up_cmd = 'echo -e "o\ny\nw\ny\n" | gdisk /dev/{}'.format(drive_mapping.get(drive_number))
        self.run_on_device(clean_up_cmd)

# This constructs the Tests() class, which in turn constructs TestClient() which triggers the Tests.run() function
nasRaidModesCreation2DrivesRaid1wSpanning()
