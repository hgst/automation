'''
Created on Apr 27th, 2015

@author: tsui_b

Objective: Verify Device Diag shows diagnostics overlay
'''
import time

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

timeout = 60

class homePageDeviceDiagShowsHealthStatus(TestClient):

    def run(self):
        self.log.info('### Verifying Device Diagnostics shows diagnostics overlay on home page ###')

        self.get_to_page('Home')
        self.log.info('Verify Device Diag by clicking diagnostics arrow')
        self.click_element(E.HOME_DEVICE_DIAGNOSTICS_ARROW)
        self.current_frame_contains('Diagnostics')
        # Check if Diagnostics Overlay exist
        self.element_should_be_visible(E.HOME_DEVICE_DIAGNOSTICS_DIAG_OVERLAY)
        self.click_close_element(E.HOME_DEVICE_DIAGNOSTICS_DIAG_CLOSE, E.HOME_DEVICE_DIAGNOSTICS_DIAG)
        self.close_webUI()

    def click_close_element(self, close_element, invisible_element, attempts=2, attempt_delay=2):
        for attempts_remaining in range(attempts, -1, -1):
            if attempts_remaining > 0:
                try:
                    self.log.debug('Clicking element: "{0}" to close: "{1}"'.format(close_element.name, invisible_element.name))
                    self.click_element(close_element)
                    self.wait_until_element_is_not_visible(invisible_element, timeout)
                    self.log.debug('Element: "{}" is closed.'.format(invisible_element.name))
                    break
                except:
                    self.log.info('Element: "{0}" did not click. Wait {1} seconds and retry. {2} attempts remaining.' \
                                  .format(close_element.name, attempt_delay, attempts_remaining))
                    time.sleep(attempt_delay)
            else:
                raise Exception('Failed to close element: "{}"'.format(invisible_element.name))

homePageDeviceDiagShowsHealthStatus()