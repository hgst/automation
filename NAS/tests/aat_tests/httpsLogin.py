'''
Created on Sepember 23, 2014
@Author: O.K.
'''

from testCaseAPI.src.testclient import TestClient

class MyClass(TestClient):
    def run(self):
        testname = 'HTTPS Login'
        self.start_test(testname)
        self.access_webUI(use_ssl=True)
        mac_add_text = self.get_text(self.Elements.NETWORK_MAC_ADDRESS)
        if (mac_add_text != '') and (mac_add_text is not None):
            self.pass_test(testname, 'MAC ADDRESS = {0}'.format(mac_add_text))
        else:
            self.fail_test(testname, 'Failed to read MAC Address')

MyClass()