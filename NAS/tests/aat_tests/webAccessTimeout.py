"""
## @author: tran_jas
## @Brief Verify unit will logout after set timeout value
#  @details Verify unit will logout after 5 minutes (default)
#   Set timeout value to 9 minutes, verify unit will not logout before 9 minutes
#      and will logout after 9 minutes
#   Verify min value is 5 and max value is 30 by try to select 4 and 31
#     then select 30 and 5
"""
import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch

class WebAccessTimeout(TestClient):
    """ Objectives:

        1) Confirm that the default timeout is 5 minutes (check_default_value)
        2) Confirm that the options in the drop down list contain every expected value and that the values point to
           the correct link (check_timeout_values)
        3) Confirm that the web UI times out when expected when set to 5 minutes
        4) Confirm that the web UI times out when expected when set to 12 minutes (chosen to make sure it has to scroll
           to get to the value)




    """
    def run(self):
        self.check_default_value()
        self.check_timeout_values()
        self.wait_for_timeout(5)
        self.wait_for_timeout(12)

    def tc_cleanup(self):
        # Reset the timeout to 5 minutes
        self.set_web_access_timeout("5")

    def check_default_value(self):
        timeout_element = self.Elements.SETTINGS_WEB_UI_TIMEOUT

        self.start_test('Check Default Timeout (should be "5 minutes")')

        self.get_to_page(timeout_element.page)
        default_value = self.get_text(self.Elements.SETTINGS_WEB_UI_TIMEOUT, check_is_ready=False)
        if default_value != '5 minutes':
            self.fail_test(error='Default value is incorrect. It is: "{}"'.format(default_value))
        else:
            self.pass_test(result='Default value is correct')

    def check_timeout_values(self):
        self.start_test('Confirm all timeout values are present and valid with no extras')
        text_values = self.get_dropdown_list_options(self.Elements.SETTINGS_WEB_UI_TIMEOUT_OPTION)
        html_values = self.get_dropdown_list_options(self.Elements.SETTINGS_WEB_UI_TIMEOUT_OPTION, 'outerHTML')

        # Format is min, max, increment
        timeout_range = self.uut[self.Fields.web_ui_timeout]

        min_timeout = timeout_range[0]
        max_timeout = timeout_range[1]
        increment = timeout_range[2]

        missing_values = 0
        invalid_values = 0
        for index in xrange(min_timeout, max_timeout + 1, increment):
            # Get the text and outer_html for this rel value
            rel_value = '{}'.format(index)
            text = text_values.get(rel_value, None)
            html = html_values.get(rel_value, None)

            if text is None:
                missing_values += 1
                self.log.error('Timeout value of {} minutes is missing from drop-down list'.format(rel_value))
            else:
                # Remove it from the dictionary
                del text_values[rel_value]

                if html is None:
                    invalid_values += 1
                    self.log.error('Timeout value of {} has no HTML code'.format(text))
                else:
                    # See if the set_idle value is correct
                    set_idle = html.split('set_idle(&quot;')[1].split('&')[0]
                    minutes = text.split()[0]

                    if set_idle == minutes:
                        self.log.info('{} correctly matches set_idle value of {}'.format(text, set_idle))
                    else:
                        invalid_values += 1
                        self.log.error('Invalid set_idle value for {}. HTML code is: {}'.format(text,
                                                                                                html))
                    # Remove it from the dictionaries (to find any unexpected values)
                    del html_values[rel_value]

        extra_values = len(text_values)
        if extra_values > 0:
            for value in text_values:
                self.log.error('The timeout value of {} is not expected.'.format(value))

        if extra_values + missing_values + invalid_values > 0:
            # Test failed
            self.fail_test('Test failed. {} extra, {} missing, and {} invalid items in the list'.format(extra_values,
                                                                                                        missing_values,
                                                                                                        invalid_values))
        else:
            self.pass_test('All items in the drop down list are accurate with no unexpected items found.')

    def wait_for_timeout(self, minutes):
        # Sets the web UI timeout to the specified number of minutes, and then waits for the web UI to timeout
        self.log.info("Setting time timeout value to {} minutes".format(minutes))
        self.set_web_access_timeout(minutes, force_webui=True)

        seconds = minutes * 60

        # Adjust this for how much variance is allowed. This is a percentage.
        variance = .10

        max_timeout = int(seconds * (1 + variance))
        min_timeout = int(seconds * (1 - variance))

        self.start_test("Waiting {} minutes for the web UI to timeout ({} to {} seconds)".format(minutes,
                                                                                                 min_timeout,
                                                                                                 max_timeout))

        sw = Stopwatch()
        sw.start()

        last_status = 0
        # Number of seconds to wait before outputting another status
        update_interval = 5
        for i in xrange(0, max_timeout + 1):
            elapsed = sw.get_elapsed_time()

            if self.is_element_visible('login_login_button', wait_time=0):
                # Found the login page, so the UI has timed out. See if enough time has passed
                if elapsed >= min_timeout:
                    self.pass_test("Unit logged out in {} seconds".format(elapsed))
                    return
                else:
                    self.fail_test("Unit logged out too soon ({} seconds)".format(elapsed))
                    return

            # Use an integer for the log messages
            elapsed = int(elapsed)
            if int(elapsed / update_interval) != last_status:
                last_status = elapsed / update_interval
                self.log.info('{} seconds elapsed ({} to {} allowed)'.format(elapsed,
                                                                             min_timeout,
                                                                             max_timeout))

            time.sleep(.5)

        self.fail_test('Unit did not log out after {} seconds'.format(max_timeout))

WebAccessTimeout()