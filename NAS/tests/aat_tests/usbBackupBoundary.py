
from testCaseAPI.src.testclient import TestClient
from global_libraries.constants import USB_Backup_Mode
from seleniumAPI.src.ui_map import Elements as E

import time
import requests

folder_name = '125GB set'
folder_loc = '125GB\ set'

class UsbBackupBoundary(TestClient):

    def run(self):
        global usb_name, usb_loc
        usb_name, usb_loc = self.get_usb_name()

        self.tc_cleanup()

        self.copy_large_dataset_test(source_path=usb_name, dest_path='/Public')
        self.job_name_conflict_test(usb_name=usb_name)
        self.max_number_backup_job_test(usb_name=usb_name)
        self.max_number_source_paths_test(usb_loc=usb_loc, usb_name=usb_name)

    def tc_cleanup(self):
        global usb_name, usb_loc
        for i in range(0, 25):
            job = 'a_%s' % i
            self.usb_backup_usage(job_name=job, usage='jobdel')

        test_folders = usb_loc+'/test*'
        self.execute('rm -rf %s' % test_folders)
        self.execute('rm -rf /shares/Public/a1/', timeout=600)
        self.usb_backup_usage(job_name='a1', usage='jobdel')

    def copy_large_dataset_test(self, source_path='', dest_path=''):
        job = 'a1'
        set_125gb = '/{0}/{1}'.format(source_path, folder_name)
        self.log.info('set_125gb path: %s' %set_125gb)
        self.start_test('copy_large_dataset_test')
        self.usb_backup_usage(source_path=set_125gb, dest_path=dest_path, job_name=job, mode='1', device_type=1, direction=1, usage='jobadd')
        self.web_access_timeout_config(timeout_value=30)
        count = 12
        wait_time = 600
        self.click_element(E.BACKUPS_USB)

        if self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div', retry=5, retry_delay=5) == 'Backup Ready':
            self.log.info('Click for start backup')
            self.click_wait_and_check('backups_USBBackupsGoJob1_button', E.UPDATING_STRING, retry=5, timeout=10)
            self.wait_until_element_is_not_visible(E.UPDATING_STRING, time_out=180)

        while count > 0:
            if self.is_element_visible(E.LOGIN_USERNAME, wait_time=1):
                self.click_element(E.BACKUPS_USB)
            try:
                if self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div', retry=5, retry_delay=5) == 'Backup Completed':
                    self.log.info('USB Backup Completed')
                    break
                else:
                    self.log.info('Job current status: {0}'.format(self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div', retry=5, retry_delay=5)))
            except Exception:
                if self.is_element_visible(E.LOGIN_USERNAME, wait_time=3):
                    self.log.info('Web time out, re-login')
                    self.click_element(E.BACKUPS_USB)
            self.log.info('Need Wait more {0} minutes for backup complete.'.format(count * wait_time/60))
            time.sleep(wait_time)
            count -= 1

        self.log.info('**Start checksum**')
        src_path = '/shares/' + source_path + '/' + folder_loc
        backup_path = '/shares' + dest_path + '/' + job + '/' + source_path + '/' + folder_loc
        result = self.generate_checksum_files_from_src_to_dest_directory(src_path, backup_path)

        if not result:
            self.pass_test('copy_large_dataset_test')
        else:
            self.fail_test('copy_large_dataset_test')
        self.close_webUI()
        
    def max_number_backup_job_test(self, usb_name):
        for i in range(0, 25):
            job = 'a_%s' % i
            self.usb_backup_usage(source_path='/Public', dest_path='/'+usb_name, job_name=job,
                                  mode=USB_Backup_Mode.COPY, direction=2, usage='jobadd')
            time.sleep(1)
        self.start_test('max_number_backup_job_test')
        self.click_element(E.BACKUPS_USB)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_JOB.locator, 'css=div.LightningStatusPanelIcon.LightningStatusPanelIconCritical')
        text = self.get_text('css=#error_dialog_message_list > li')
        self.log.debug('Error Message: {0}'.format(text))
        self.click_wait_and_check('popup_ok_button')
        if text == 'The maximum number of backup jobs has been reached.':
            self.pass_test('max_number_backup_job_test')
        else:
            self.fail_test('max_number_backup_job_test')
        self.close_webUI()
        
        for i in range(0, 25):
            job = 'a_%s' % i
            self.usb_backup_usage(job_name=job, usage='jobdel')

    def max_number_source_paths_test(self, usb_loc='', usb_name=''):
        for i in range(1, 27):
            test_folder = usb_loc+'/test%s' % i
            self.execute('mkdir "{0}"'.format(test_folder))

        self.click_element(E.BACKUPS_USB)
        self.click_element(E.BACKUPS_CREATE_USB_JOB)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_DIRECTION_LINK, 'link=USB to NAS')
        self.click_wait_and_check('link=USB to NAS', 'link=NAS to USB', visible=False)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SOURCE_FOLDER_BUTTON, 'link='+usb_name)
        self.click_wait_and_check('link='+usb_name, 'home_treeCancel_button')
        for i in range(1, 27):

            self.log.info('link=test%s is visible: %s' % (i, self.is_element_visible('link=test%s' % i, wait_time=5)))

            self.scroll_bar(offset=43, retry=3, locator='link=test%s' % i)

            if i == 26:
                self.start_test('max_number_source_paths_test')
                self.select_folder_by_xpath('test%s' % i, 'link=test%s' % i, -65, 5)
                text = self.get_text('css=#error_dialog_message_list > li')
                self.log.debug('Error Message: {0}'.format(text))
                self.click_wait_and_check('popup_ok_button')
                if text == 'The maximum number of folders and/or files has been reached.':
                    self.pass_test('max_number_source_paths_test')
                else:
                    self.fail_test('max_number_source_paths_test')
                self.click_wait_and_check('home_treeCancel_button', E.BACKUPS_CREATE_USB_DIAG_CANCEL)
                self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_CANCEL)
            else:
                self.select_folder_by_xpath('test%s' % i, 'link=test%s' % i, -65, 5)
        self.close_webUI()
        
        test_folders = usb_loc+'/test*'
        self.execute('rm -rf %s' % test_folders)

    def job_name_conflict_test(self, usb_name=''):
        job = 'a1'
        self.start_test('job_name_conflict_test')
        self.click_element(E.BACKUPS_USB)
        self.click_element(E.BACKUPS_CREATE_USB_JOB)
        self.input_text_check(E.BACKUPS_CREATE_USB_DIAG_JOB_NAME, job)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_DIRECTION_LINK, 'link=USB to NAS')
        self.click_wait_and_check('link=NAS to USB', 'link=USB to NAS', visible=False)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SOURCE_FOLDER_BUTTON, 'home_treeCancel_button', timeout=10)
        self.click_wait_and_check('css=li.directory.collapsed > label.LightningCheckbox > span', E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK, timeout=10)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_DESTINATION_FOLDER_BUTTON, 'link='+usb_name, timeout=10)
        self.click_wait_and_check('css=li.directory.collapsed > label.LightningCheckbox > span', E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK)
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_CREATE, 'css=div.LightningStatusPanelIcon.LightningStatusPanelIconCritical')
        text = self.get_text('css=#error_dialog_message_list > li')
        self.log.debug('Error Message: {0}'.format(text))
        self.click_wait_and_check('popup_ok_button')
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_CANCEL)
        if text == 'The Job name entered already exists. Enter a different name.':
            self.pass_test('job_name_conflict_test')
        else:
            self.fail_test('job_name_conflict_test')
        self.close_webUI()
        self.usb_backup_usage(job_name=job, usage='jobdel')

    def get_usb_name(self):
        data = ''.join(self.get_usb_mapping_info())
        usb_name = data.split(':')[0]
        data_loc = data.split(':')[1]
        self.log.info('Get USB Mapping Info:{0}, USB NAME:{1}'.format(data, usb_name))
        return ''.join(usb_name), ''.join(data_loc)

    def get_scrollbar_position(self):
        """
            Get the current scroll bar position
        :return: the current top px
        """
        scroll = self.element_find('css=.jspDrag')
        scroll_text = scroll.get_attribute('style')
        self.log.info("STYLE: {}".format(scroll_text))
        if 'top' not in scroll_text:
            cur_top = 0.0
        else:
            cur_top = float(scroll_text.split('top:')[-1].split('px;')[0].strip())
        return cur_top

    def scroll_bar(self, offset=43, retry=3, locator=''):
        y_offset = offset
        prev_state = self.get_scrollbar_position()
        while not self.is_element_visible(locator, wait_time=5) and retry >= 0:
            self.log.info('Scroll bar for search %s' %locator)
            self.log.info('y_offset: %s' % y_offset)
            state = self.get_scrollbar_position()
            self.log.info('Current bar state: %s' % state)
            try:
                self.drag_and_drop_by_offset('css=div.jspDrag', 0, y_offset)
            except Exception:
                pass
            finally:
                next_state = self.get_scrollbar_position()
                self.log.info('Scroll bar new state: %s' % next_state)
                if state == next_state:
                    self.log.info("Did not scroll bar, retry again")
                    y_offset = y_offset
                    if state == prev_state:
                        self.log.info("Scroll bar reaches the top or bottom")
                        if state == 0:
                            self.log.info("Scroll bar reaches the top ")
                            y_offset = offset
                        else:
                            self.log.info("Scroll bar reaches the bottom")
                            y_offset = -offset
                    retry -= 1
                else:
                    retry = 3
                prev_state = state
                self.log.info('Still has retry times: %s' % retry)

    def select_folder_by_xpath(self, rel, locator, xoffset, yoffset, retry=3):
        checkbox_selected_result = self.is_checkbox_selected("//input[@type='checkbox' and @name='folder_name' and @rel='{0}']".format(rel))
        self.log.info('status of checkbox on {0} is {1}'.format(rel, checkbox_selected_result))
        while retry >= 0:
            self.log.debug('Clicking at x:{0}, y:{1} coordinates of element:"{2}"\nand check the value of "{3}" is "{4}"'
                           .format(xoffset, yoffset, locator, rel, self.is_checkbox_selected("//input[@type='checkbox' and @name='folder_name' and @rel='{0}']".format(rel))))
            try:
                self.click_element_at_coordinates(locator, xoffset, yoffset, 1)
            except Exception as e:
                if not self.is_element_visible(locator):
                    self.drag_and_drop_by_offset('css=div.jspDrag', 0, -10)
                    self.log.info('Element too high, hard to click, scroll up')
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not selected, remaining {1} retries...'.format(rel, retry))                

            if self.is_checkbox_selected("//input[@type='checkbox' and @name='folder_name' and @rel='{0}']".format(rel)) or self.is_element_visible('css=#error_dialog_message_list > li'):
                break
            else:
                self.drag_and_drop_by_offset('css=div.jspDrag', 0, 5)
                self.log.info('Element too low, hard to click, scroll down')
                retry -= 1
                continue

    def web_access_timeout_config(self, timeout_value):
        self.log.info('*** Web Access Timeout set to {0} ***'.format(timeout_value))
        cgi_idle = 'cgi_idle&f_idle='+str(timeout_value)
        head_url = 'system_mgr.cgi'
        self.send_cgi(cgi_idle, head_url)

    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

    def generate_checksum_files_from_src_to_dest_directory(self, src_path, backup_path, timeout =7200):
        md5_file = 'file.md5'
        myCommand = 'find {0}/ -type f -print0 | xargs -0 busybox md5sum >> {1}/{2}'.format(src_path, backup_path, md5_file)

        self.log.info('do command: {}'.format(myCommand))
        self.run_on_device(myCommand, timeout)

        self.log.info('Compare two directory')
        compare_fail_command = 'cd {0}/ ; busybox md5sum -c {1} | grep FAILED'.format(backup_path, md5_file)
        self.log.info('do command: {}'.format(compare_fail_command))
        compare_fail = self.run_on_device(compare_fail_command, timeout)
        self.log.info('FAIL: {0}'.format(compare_fail))

        return compare_fail

UsbBackupBoundary()
