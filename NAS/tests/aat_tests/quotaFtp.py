'''
Create on June 11, 2015

@Author: lo_va

Test ID: 117749, 270602
'''
import time
import random
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
generated_file_size = 2048000
quota_user_name = 'quota_ftpuser{}'.format(random.randint(1,100))
sharename = 'Public'

class quotaFtp(TestClient):
    
    def run(self):
        
        self.delete_all_users()
        self.create_user(username=quota_user_name)
        self.enable_ftp_service()
        self.enable_ftp_share_access(share_name=sharename)
        # Create user quota
        try:
            self.create_user_quota(user_name=quota_user_name, size_of_quota=1000, unit_size='MB')
        except Exception as ex:
            self.log.exception('Setup user quota failed, test Failed.')
        self.close_webUI()
        generated_file = self.generate_test_file(generated_file_size)
        
        # Try to upload a file size larger than user quota. Upload should be Failed.
        Upload_finished = False
        try:
            self.write_files_to_ftp(username=quota_user_name, files=generated_file, share_name=sharename)
            Upload_finished = True
        except Exception as ex:
            self.log.info('Cannot upload file, TEST PASS!!! Msg: {}'.format(repr(ex)))
        if Upload_finished == True:
            self.log.error('Can uploaded file size more than user quota size! TEST FAILED!!!')
        
        # Remove user quota size
        self.create_user_quota(user_name=quota_user_name, size_of_quota='', unit_size='MB')
        
        # Try to upload a file size without user quota set. Upload should be Successful.
        try:
            self.write_files_to_ftp(username=quota_user_name, files=generated_file, share_name=sharename)
        except Exception:
            self.log.exception('Cannot upload file, TEST FAILED!!!')
        
quotaFtp()