'''
Created on Feb 3 2015

@author: Carlos Vasquez

Objective:  To verify that the NAS is able to connect at all supported Ethernet rates
'''

from testCaseAPI.src.testclient import TestClient

AGREGATIONTYPE = ['Active Backup', 'OFF']
# The only devices supported are: Sprite, Aurora, YellowStone, Yosemite, Lightning
SUPPORT_AGREGATIONTYPE_MODELS = ['BWAZ', 'BBAZ', 'LT4A', 'BNEZ', 'BWZE']
NON_SUPPORT_AGREGATIONTYPE_MODELS = ['KC2A', 'BZVM', 'BWVZ', 'BVBZ']
SPRITEAURORA = ['BBAZ', 'BNEZ']
NON_SUPPORT_MODEL = ['GLCR']

class connectRates(TestClient):   
    def run(self):
        global modelNumber
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')
        returnedVersion = self.get_firmware_version_number()
        firmwareVersion = returnedVersion[:4]
        self.get_to_page('Settings')
        self.click_element(self.Elements.NETWORK_BUTTON)

        if modelNumber in NON_SUPPORT_MODEL:
            self.log.info('This device ({0}) does not support connectRates testing'.format(modelNumber))

        elif ((modelNumber in  SUPPORT_AGREGATIONTYPE_MODELS) and not ((firmwareVersion == '1.06') and (modelNumber in SPRITEAURORA))):
            self.verify_aggregation_type(aggregation_type=AGREGATIONTYPE)

        else:
            self.log.info('This device ({0}) does not support Link Aggregation'.format(modelNumber))
            self.link_speed(link_speed='100')
            self.verify_link_speed(speed='100 Mbps')
            self.link_speed(link_speed='1000')
            self.verify_link_speed(speed='1 Gbps')

    def verify_aggregation_type(self, aggregation_type=None):
        for link in aggregation_type:
                self.execute('dmesg -c')
                self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_LINK_AGGREGATION_SELECT, 'link={0}'.format(link))
                self.click_wait_and_check('link={0}'.format(link), self.Elements.SETTINGS_NETWORK_LINK_AGGREGATION_SAVE)
                self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_LINK_AGGREGATION_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
                self.click_wait_and_check('popup_ok_button')
                self.get_to_page('Settings')
                self.click_element(self.Elements.NETWORK_BUTTON)

                self.log.info('****************Link Agregation - {0} **************'.format(link))
                self.link_speed(link_speed='Auto')
                self.verify_link_speed(link, '1 Gbps')
                self.link_speed(link_speed='100')
                self.verify_link_speed(link, '100 Mbps')
                self.link_speed(link_speed='1000')
                self.verify_link_speed(link, '1 Gbps')
             
    def link_speed(self, link_speed):
        self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_LINK_SPEED_SELECT, 'link={0}'.format(link_speed))
        self.click_wait_and_check('link={0}'.format(link_speed), self.Elements.SETTINGS_NETWORK_LINK_SPEED_SAVE)
        self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_LINK_SPEED_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
    
    def verify_link_speed(self, link=None, speed=None):
        dataTransmission = 'full duplex'
        returnedDmesg = self.execute('dmesg | grep -i egiga0:')
        lastEntry = returnedDmesg[1].split('/n')[-1]
        
        link_speed = dataTransmission + ', ' + speed
        
        if (speed == '1 Gbps') and (modelNumber in SPRITEAURORA):
            speed = '1000 Mbps'
             
        if speed.lower() in lastEntry.lower():
            self.log.info('SUCCESS: the speed is back to {0}'.format(link_speed))
        else:
            self.log.error('Failed: the speed is not as expected({0}/{1})'.format(link, speed))
           
connectRates()