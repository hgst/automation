__author__ = 'erica.lee@wdc.com'

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadWdappmgr(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        wdappmgr_daemon = 'wdappmgr is running'
        output = ''
        try:
            self.start_test('Load wdappmgr in firmware test')
            status, output = self.execute('/etc/init.d/wdappmgrd status')
            self.log.info('output: {0}'.format(output))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        if wdappmgr_daemon in output:
            self.pass_test()
        else:
            self.fail_test()

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit()

LoadWdappmgr(checkDevice=False)
