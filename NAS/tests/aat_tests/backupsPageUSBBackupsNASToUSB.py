'''
Created on May 19th, 2015

@author: tsui_b

Objective: Verify USB Backups - NAS to USB 

Automation: Full
    
Supported Products: All
    
Test Status:
            Local 
                    Lightning: Pass (FW: 2.00.221)
            Jenkins 
                    Lightning: Pass (FW: 2.00.221)
                    Kings Canyon: Pass (FW: 2.00.221)
                    Glacier: Pass (FW: 2.00.221)
                    Zion: Pass (FW: 2.00.221)
                    Yosemite: Pass (FW: 2.00.221)
                    Yellowstone: Pass (FW: 2.00.221)
                    Aurora: Pass (FW: 2.00.221)
                    Sprite: Pass (FW: 2.00.221)
'''
import random
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

class backupsPageUSBBackupsNASToUSB(TestClient):
    def run(self):
        usb_share_name = 'AAT_USB_Share'
        nas_share_name = 'AAT_NAS_Share'
        file_name = 'dummyfile'
        backup_job_name = 'AAT_USB_Backup'
        volume = 'Volume_1'  # Need to select vol before shares since FW 2.10.123

        self.log.info('### Verify USB Backups: NAS to USB ###')
        self.log.info('Checking if there are any USB device exist.')
        result = self.get_usb_info()
        if result[0] != 0:
            self.log.error('Get USB info from Rest API failed! Return value: {}'.format(result[0]))
            return
        else:
            usb_device_name = self.get_xml_tags(result[1], 'share')
            if not usb_device_name:
                self.log.warning('There are no USB devices, unable to test USB Backups: NAS to USB.')
                return
        
        usb_device = random.choice(usb_device_name)
        self.log.info('Select USB device:{} as destination share folder.'.format(usb_device))
        try:
            self.delete_all_shares()
            self.delete_all_backups_jobs()
            self.delete_usb_shares(usb_device, usb_share_name)
            self.create_shares(share_name=nas_share_name, description=nas_share_name)
            md5_src_file = self.create_file_in_src_share(nas_share_name, file_name)
            self.create_usb_shares(usb_device, usb_share_name)
            self.create_backup_job_from_ui(backup_job_name, nas_share_name, usb_device, usb_share_name, volume)
            self.log.info('Comparing md5 checksum between src_file and dst_file.')
            md5_dst_file = self.verify_file_in_dst_share(backup_job_name, nas_share_name, usb_device, usb_share_name, file_name)
            if md5_src_file != md5_dst_file:
                self.log.error('Data compare error! MD5_SRC_File:{0}, MD5_DST_FILE:{1}'.format(md5_src_file, md5_dst_file))
        except Exception as e:
            self.log.error("Failed to verify USB Backups: NAS to USB! Exception: {0}".format(repr(e)))
        finally:
            self.delete_usb_shares(usb_device, usb_share_name)

    def tc_cleanup(self):
        self.delete_all_shares()
        self.delete_all_backups_jobs()
    
    def delete_all_backups_jobs(self):
        self.log.info('Check how many backups jobs are created before.')
        result = self.execute('cat /var/www/xml/usb_backup.xml')[1]
        if 'No such file or directory' in result:
            self.log.info('There are no created backups jobs.')
            return
        
        created_backups = self.get_xml_tags(result, 'task_name')
        if not created_backups:
            self.log.info('There are no created backups jobs.')
        else:
            self.log.info('There are {0} created backups jobs, delete them.'.format(len(created_backups)))
            for backups in created_backups:
                self.log.info('Deleting backups job: {0}'.format(backups))
                result = self.execute('usb_backup -a {0} -c jobdel'.format(backups))
                if result[0] != 0:
                    raise Exception('Delete backups job failed! Exception: {0}'.format(result[1]))

            self.log.info('All backups jobs are deleted.')
    
    def delete_usb_shares(self, usb_name, share_name):
        self.log.info('Deleting share folder:{0} in USB device:{1} if exist.'.format(share_name, usb_name))
        cmd = 'ls /shares/{0}/{1}'.format(usb_name, share_name)
        result = self.execute(cmd)
        if 'No such file or directory' in result[1]:
            self.log.info('Share folder {0} has not been created before.'.format(share_name))
        else:
            cmd = 'rm -R /shares/{0}/{1}'.format(usb_name, share_name)
            result = self.execute(cmd)
            # Suppose not to get any response if success
            if result[1]:
                raise Exception('Delete share folder:{0} in USB device:{1} failed! Exception:{2}'.format(share_name, usb_name, result[1]))
                
    def create_usb_shares(self, usb_name, share_name):
        self.log.info('Creating share folder:{0} in USB device:{1}.'.format(share_name, usb_name))
        cmd = 'mkdir /shares/{0}/{1}'.format(usb_name, share_name)
        result = self.execute(cmd)
        # Suppose not to get any response if success
        if result[1]:
            raise Exception('Create share folder:{0} in USB device:{1} failed! Exception:{2}'.format(share_name, usb_name, result[1]))
                
    def create_file_in_src_share(self, share_name, file_name):
        self.log.info('Creating a 1MB dummy file in source share folder.')
        cmd = 'dd if=/dev/urandom of=/shares/{0}/{1} bs=1024 count=1'.format(share_name, file_name)
        result = self.execute(cmd)
        if '1+0 records in\n1+0 records out\n1024 bytes (1.0KB) copied' not in result[1]:
            raise Exception('Failed to create file in source share folder!')
 
        md5_src_file = self.md5_checksum('/shares/{0}/'.format(share_name), file_name)
        return md5_src_file

    def create_backup_job_from_ui(self, job_name, src_folder, usb_device, dst_folder, volume):
        self.log.info('Creating USB Backups job from Web UI.')
        self.get_to_page('Backups')
        self.click_element(E.BACKUPS_USB)
        self.click_element(E.BACKUPS_CREATE_USB_JOB)
        self.input_text_check(E.BACKUPS_CREATE_USB_DIAG_JOB_NAME, job_name)
        self.click_link_element(E.BACKUPS_CREATE_USB_DIAG_DIRECTION_LINK, 'NAS to USB')
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SOURCE_FOLDER_BUTTON, E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG)
        # Use click_coordinates to click checkbox cause we can't find its locator.
        # Case might be failed when the checkbox position is changed on the web UI.
        
        # Spec change again in FW 2.10.140..no need to select volume in this version
        # self.wait_until_element_is_visible('link={}'.format(volume), 60)
        # self.click_wait_and_check('link={}'.format(volume), 'link={}'.format(src_folder))
        self.wait_until_element_is_visible('link={}'.format(src_folder), 60)
        self.click_coordinates_and_check_attribute('link={}'.format(src_folder), -105, 0, 
                                                   E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK)        
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_DESTINATION_FOLDER_BUTTON, E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG)
        self.wait_until_element_is_visible('link={}'.format(usb_device), 60)
        self.click_wait_and_check('link={}'.format(usb_device), 'link={}'.format(dst_folder))
        self.click_coordinates_and_check_attribute('link={}'.format(dst_folder), -105, 0, 
                                                   E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK)

        self.click_link_element(E.BACKUPS_CREATE_USB_DIAG_BACKUP_TYPE_LINK, 'Synchronize')
        self.click_wait_and_check(E.BACKUPS_CREATE_USB_DIAG_CREATE, E.UPDATING_STRING, visible='None', timeout=60)
        self.wait_until_element_is_visible(E.BACKUPS_USB_JOB_LIST)
        self.current_frame_contains(job_name)
        self.current_frame_contains('Backup Completed')
        self.close_webUI()
    
    def verify_file_in_dst_share(self, job_name, nas_share, usb_device, usb_share, file_name):
        self.log.info('Verifying {0} is in destination folder:{1}.'.format(file_name, usb_share))
        cmd = 'ls /shares/{0}/{1}/{2}/{3}'.format(usb_device, usb_share, job_name, nas_share)
        result = self.execute(cmd)
        if file_name not in result[1]:
            raise Exception('File:"{0}" did not in destination folder:"{1}" after executing USB Backup!'.format(file_name, nas_share))

        md5_dst_file = self.md5_checksum('/shares/{0}/{1}/{2}/{3}/'.format(usb_device, usb_share, job_name, nas_share), file_name)
        return md5_dst_file

    def click_coordinates_and_check_attribute(self, locator, xoffset, yoffset, check_locator, attribute, expected_value, retry=3):
        while retry >= 0:
            self.log.debug('Clicking at x:{0}, y:{1} coordinates of element:"{2}"\nand check the "{3}" attribute of locator "{4}" is "{5}"'.
                           format(xoffset, yoffset, locator, attribute, check_locator.name, expected_value))
            try:
                self.click_element_at_coordinates(locator, xoffset, yoffset)
                self.check_attribute(check_locator, expected_value, attribute)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

backupsPageUSBBackupsNASToUSB()