#
# Objective: To validate that the device is capable of supporting a large number
# of users/shares/groups
# wiki URL: http://wiki.wdc.com/wiki/Max Users Shares
#
from testCaseAPI.src.testclient import TestClient

maxNumberOfShares = 128
maxNumberOfGroups = 64


class MaxUsersGroups(TestClient):
    def run(self):
        # create 511 users
        for i in range(1, 512):
            user_name = 'user{0}'.format(i)
            self.create_user(username=user_name, fullname='user user')

        # Create max number of groups (64)
        self.create_groups(group_name='group', number_of_groups=64)

        # Create 128 shares.
        self.create_shares(share_name='share', number_of_shares=128)
        response = self.get_all_shares()
        total_shares = len(self.get_xml_tags(response[1], 'share_name'))

        self.access_webUI()

        # Validate max number of groups
        last_group = self.find_element_in_sublist('group64', 'group')
        testname = 'Maximun Number of Groups (64)'
        self.start_test(testname)
        if last_group:
            self.pass_test(testname, 'group64 was the last group')
        else:
            self.fail_test(testname, 'group64 was not the last group')

        # Validate max number of shares
        testname = 'Maximum Number of Shares (128)'
        self.start_test(testname)
        print 'The total number of shares: {0}'.format(total_shares)
        if total_shares > maxNumberOfShares:
            self.pass_test(testname, '{} total shares'.format(total_shares))
        else:
            self.fail_test(testname,
                           '{} shares is not equal to the expected {} shares'.format(total_shares, maxNumberOfShares))

        # Change share1, share60, and share128 to private
        self.update_share('share1', public_access=False)
        self.update_share('share60', public_access=False)
        self.update_share('share128', public_access=False)

        # Change group1's quota to 100MB for each volume and allow it read/write access to share1
        self.set_quota_all_volumes(name='group1', amount=100)
        self.assign_group_to_share(group_name='group1', share_number=1, access_type='rw')

        # Change group30's quota to 300MB for each volume and allow it read/write access to share60
        self.set_quota_all_volumes(name='group30', amount=300)
        self.assign_group_to_share('group30', share_number=60)

        # Change group64's quota to 500MB for each volume and allow it read/write access to share128
        self.set_quota_all_volumes(name='group64', amount=500)
        self.assign_group_to_share('group64', share_number=128)

        # Assign  user1 to group1
        self.assign_user_to_group(user_name='user1', group_name='group1')
        self.create_share_access(share_name='share1', username='user1', access='RW')

        # Assign  user300 to group30
        self.assign_user_to_group(user_name='user300', group_name='group30')
        self.create_share_access(share_name='share60', username='user300', access='RW')

        # Assign  user511 to group64
        self.assign_user_to_group(user_name='user511', group_name='group64')
        self.create_share_access(share_name='share128', username='user511', access='RW')

        # verify user1 has RW access to share1
        user1_access = self.get_share_access(share_name='share1', username='user1')
        testname = 'User1 has RW access to share1'
        self.start_test(testname)
        if (self.get_xml_tag(xml_content=user1_access[1], tag='access')) == 'RW':
            self.pass_test(testname, 'User1 has RW access to share1')
        else:
            self.fail_test(testname, 'User1 does not have RW access to share1')

        # verify user300 has RW access to share60
        user300_access = self.get_share_access(share_name='share60', username='user300')
        testname = 'user300 has RW access to share60'
        self.start_test(testname)
        if (self.get_xml_tag(xml_content=user300_access[1], tag='access')) == 'RW':
            self.pass_test(testname, 'User300 has RW access to share60')
        else:
            self.fail_test(testname, 'User1 does not have RW access to share60')

        # verify user511 has RW access to share128
        user511_access = self.get_share_access(share_name='share128', username='user511')
        testname = 'user511 has RW access to share128'
        if (self.get_xml_tag(xml_content=user511_access[1], tag='access')) == 'RW':
            self.pass_test(testname, 'User511 has RW access to share128')
        else:
            self.fail_test(testname, 'User511 does not have RW access to share128')


MaxUsersGroups()
