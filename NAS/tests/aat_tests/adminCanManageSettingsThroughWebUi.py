"""
Created on July 27th, 2015

@author: tran_jas

## @brief create manage safepoint through web UI

"""

from sequoiaAPI.src.sequoia import Sequoia
from seleniumAPI.src.seq_ui_map import Elements as ET


class Settings(Sequoia):

    def run(self):

        try:
            self.seq_accept_eula()
            self.click_element(ET.SETTINGS_LINK)

            self.click_element(ET.DEVICE_LINK)
            if not self.is_element_visible(ET.DEVICE_CONTAINER):
                self.log.error('DEVICE_CONTAINER is not displayed')

            self.click_element(ET.NETWORK_LINK)
            if not self.is_element_visible(ET.NETWORK_CONTAINER):
                self.log.error('NETWORK_CONTAINER is not displayed')

            self.click_element(ET.MEDIA_LINK)
            if not self.is_element_visible(ET.MEDIA_CONTAINER):
                self.log.error('MEDIA_CONTAINER is not displayed')

            self.click_element(ET.DIAGNOSTICS_LINK)
            if not self.is_element_visible(ET.DIAGNOSTICS_CONTAINER):
                self.log.error('DIAGNOSTICS_CONTAINER is not displayed')

            self.click_element(ET.NOTIFICATIONS_LINK)
            if not self.is_element_visible(ET.NOTIFICATIONS_CONTAINER):
                self.log.error('NOTIFICATIONS_CONTAINER is not displayed')

            self.click_element(ET.UPDATE_LINK)
            if not self.is_element_visible(ET.UPDATE_CONTAINER):
                self.log.error('UPDATE_CONTAINER is not displayed')

        except Exception, ex:
            self.log.exception('Failed to complete Admin Can Manage Settings Through WebUI test case \n' + str(ex))

Settings()
