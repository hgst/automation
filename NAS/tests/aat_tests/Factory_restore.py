'''
Created on Sepember 16, 2014
@Author: O.K.
'''
import time 
import os
import ftplib
import posixpath
import tarfile
from xml.dom import minidom 
from global_libraries import CommonTestLibrary as CTL
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries.wd_dictionary import compare
#-------------------------------------------------------------------- import xml
#-------------------------------------------------------------- import xmltodict
#-------------------------------------------------------------------- import sys
import shutil


#===================================================================================================================
# THIS TEST REQUIRES USB STORAGE - PLEASE LEAVE THE USB STORAGE ATTACHED TO THE DEVICE AT ALL TIME DURING THIS TEST
#===================================================================================================================

# instantiate TESTClient class
test = TestClient()
logger1 = CTL.getLogger('AAT.Factory_Restore')

# New variables defined in this module 
new_admin_password = 'branded1'

new_folder = 'frtc_folder'
new_share = 'FRTC_share'
new_user = 'frtc_user'
new_user_psw = 'fituser99'
temp_local_folder = '/tmp/frtc'
FTP_server_URL = '64.58.146.206'
music_source_path = FTP_server_URL+'/Public/media/music/'  #'/mnt/hgfs/AAT_framework/media/music'
music_target_path = '/shares/Public/Shared\ Music/'
pix_source_path = FTP_server_URL+'/Public/media/pictures/' #'/mnt/hgfs/AAT_framework/media/pictures'    
pix_target_path = '/shares/Public/Shared\ Pictures/'
vid_source_path = FTP_server_URL+'/Public/media/videos/' #'/mnt/hgfs/AAT_framework/media/videos'
vid_target_path = '/shares/Public/Shared\ Videos/'
Twonky_username = ''
Twonky_password = ''

######################### ######################### ######################### ######################### ######################### 
########################################  Please do not delete anything above this line #########################################
######################### ######################### ######################### ######################### #########################



class FactoryRestore(TestClient):
    
    def run(self):
        try:            
            
            '''
            #01) update admin password with a new one (FAILING)
            self.update_admin_passw()  
            
            #02) Create a new user
            self.create_new_user()  
              
            #03) Create a new share    
            self.create_new_share()
            
            #04) Update share full access for the new user    
            self.update_newuser_full_access_to_newshare()
            
            #05) Update the new private share Read access for the admin (FAILING)    
            self.update_admin_readaccess_to_newuser()   
                     
            #06) Enable Recycle Bin for all the shares, private and public ones    
            self.Enable_Recycle_Bin_for_all_shares()  
             
            #07) Set Network Mode to Static IP, different from previous DHCP and verify the web UI access
            self.set_network_to_static()    
            
            #08) Create a Public Share folder through Web UI 
            self.create_a_new_folder_under_public_thru_web() 
            
            #09) Generate new files and write them to the new private share thru samba and read them back 
            self.copy_generated_contents_to_new_folder_under_public()        
            
            #10) Detect the USB share and name and give a new full access to the recently created user in step 2 (media serving must be off and its default is ON)
            self.detect_usb_and_full_access_to_new_user()
            
            #11) copy 10 files of 5MB each to the USB share under "frtc_folder" directory, so after factory restore the code will check and read the files back to see if USB hasn't been formatted!
            self.copy_generated_contents_to_usb_share()
            
            #12) Change the Oplocks state for the USB Share (default is on) 
            self.turn_off_Oplocks_for_usb_share()   
                
            #13) Copy new photos/videos/music to Shared Pictures/Shared Music and Shared Videos folders from FTP server    
            self.copy_ftp_media_files_to_UUT_public_subfolders()   
             
            #14) Get a DAC code (Cloud Access) for the "frtc_user"    
            self.generate_a_DAC_code_for_new_user()  
              
            #15) Open the Twonky page and modify the configurations    
            self.twonky_modifications()  
            
            #16) Save configuration file and extract it to /tmp/frtc/backup_<suffic>/backup folder
            self.save_configuration_file(target_dir = temp_local_folder, new_suffix='new')
            '''
            #17) Read the contents and save a dictionary of target tag name
            global model_number
            sys_info = test.get_system_information()
            model_number = test.get_xml_tag(xml_content=sys_info[1], tag='model_number')
            self.compare_parent_tags(1, 2, parent_tag='download_mgr')
            
            
            #===================================================================
                        # net_mngr_dict = self.read_xml_file(xml_file_path=None, tag_name = 'network_mgr')
            #----------------- if net_mngr_dict != None and net_mngr_dict != '':
                #------------------------------------------- print net_mngr_dict
            # sw_ver2 = self.read_xml_file(xml_file_path=None, tag_name = 'sw_ver_2')
            #----------------------------- if sw_ver2 != None and sw_ver2 != '':
                #------------------------------------------------- print sw_ver2
            # hw_ver = self.read_xml_file(xml_file_path=None, tag_name = 'hw_ver')
            #------------------------------- if hw_ver != None and hw_ver != '':
                #-------------------------------------------------- print hw_ver
            #===================================================================
            #===================================================================
            # omid = 'crazy'
            # new_value = 'not_crazy'
            # setattr(self, omid, new_value)            
            # print self.crazy
            #===================================================================
            #===============================================================
            # eula
            # sw_ver_1
            # dsk_mgr
            # backup_mgr
            # app_mgr
            # download_mgr
            # cloud
            # recycle_bin
            #===============================================================
            #18)
            
        except Exception, ex:
            raise Exception('Failed to Run Factory Restore Test Case - Test could not run.\n' + str(ex))
        finally:
            #test.end() #end is also calling my test case tc_cleanup() function. no need to call it again here.
            print 'finally executed'
 
    def update_admin_passw(self):                
        #1)##############################################################################################################################################
        # Update the "Admin" user with a new password "new_admin_password"    
        try:
            #Reset the Admin password first (Admin must have NO password set at this point)
            test.reset_admin_password()
            time.sleep(3)
            # Update Admin password
            admin_pwd_updt = test.set_admin_password(new_admin_password)
            # Get the response of the SHARE creation (Zero means success)
            if admin_pwd_updt == 0:
                logger1.info('**********Admin user has been successfully updated with new password*****PASSED*****\nResponse: {0}'.format(admin_pwd_updt))
                newpas = test.uut[Fields.web_password] = new_admin_password
                logger1.info('**********The new password for admin is \"{0}\"*****PASSED*****'.format(newpas))
                pass
            else:
                logger1.error('**********Unable to update Admin password*****FAILED*****')
                pass
        except Exception, ex:  
            print '**********Unable to update Admin password*****FAILED*****\nException: {0}'.format(str(ex))       
            logger1.error('Failed to update Admin password\n' + str(ex))   
            pass 
        #1)##############################################################################################################################################
    def create_new_user(self):
        #2)##############################################################################################################################################       
        # Creating a USER in the beginning of Factory Restore Test (to be able to validate its existance after execution)
        try:
            test.local_login(username=test.uut[Fields.web_username], password=test.uut[Fields.web_password])
            # create a USER for future verification
            cu = test.create_user(username = new_user, password = new_user_psw, fullname='factory_restore_test_user', is_admin=True)
            if cu == 0:
                logger1.info('**********\"{0}\" USER has been successfully created******PASSED****'.format(new_user))
                pass
            else:
                logger1.error('**********Failed to create a USER******FAILED****\n Response: \"{0}\"'.format(cu))
                pass
        except Exception, ex: 
            print '**********Failed to create a USER*****FAILED*****\n Exception: \"{0}\"'.format(str(ex))       
            logger1.error('Failed to Create a USER\n' + str(ex))
            pass
        #2)##############################################################################################################################################       
    def create_new_share(self):            
        #3)##############################################################################################################################################       
        # Creating a new SHARE in the beginning of Factory Restore Test (to be able to validate its existance after execution)
        try:        
            # create a SHARE for future verification 
            cs = test.create_shares(share_name = new_share, description='factory_restore_test_share', public_access=False, media_serving='any')
            # Get the response of the SHARE creation    
            if cs == 0:
                logger1.info('**********\"{0}\" SHARE has been successfully created*****PASSED******\nResponse: \"{1}\"'.format(new_share,cs))
                pass
            else:
                logger1.error( '**********Failed to create a SHARE*****FAILED******\nResponse: {0}'.format(cs))
                pass
        except Exception, ex: 
            print '**********Failed to create a SHARE******FAILED*****\nException: {0}'.format(str(ex))       
            logger1.error('Failed to Create a SHARE\n' + str(ex))
            pass
        #3)#############################################################################################################################################       
    def update_newuser_full_access_to_newshare(self):
        #4)##############################################################################################################################################       
        # Updating the new user access to the Public share (Grant full access to the "FRTC_user" USER.
        try:
            # Grant USER full access to the Private share
            grant_user = test.create_share_access(share_name = new_share, username = new_user, access='RW') 
            # Get the response of the update access         
            if grant_user == 0:
                logger1.info('**********\"{0}\" has been successfully granted full access to the Private Share \"{1}\"*****PASSED*****\nResponse: \"{2}\"'.format(new_user,new_share,grant_user))
                pass
            else:
                grant_user_retry = test.update_share_access(share_name = new_share, username = new_user, access='RW')
                if grant_user_retry == 0:
                    print '\n**********Share has already been existed**********\n'
                    logger1.info('**********\"{0}\" has been successfully granted full access to the Private Share \"{1}\"*****PASSED*****'.format(new_user,new_share))
                else:
                    logger1.error('**********Failed to Update \"{0}\" FUll access to the Private Share \"{1}\"**********'.format(new_user,new_share))
                pass
        except Exception, ex:       
            logger1.error('**********Failed to Update \"{0}\" FUll access to the Private Share \"{1}\"**********'.format(new_user,new_share) + str(ex))
            pass  
        #4)##############################################################################################################################################       
    def update_admin_readaccess_to_newuser(self):   
        #5)##############################################################################################################################################
        # Updating the read only access to the recently created share "FRTC_share" for the ADMIN 
        try:
            # Grant ADMIN read only access to the Private share
            grant_admin = test.create_share_access(share_name = new_share, username='admin', access='RO') 
            # Get the response of the update access 
            if grant_admin == 0:
                logger1.info('**********\"Admin\" has been successfully granted Read access to the Private Share \"{0}\"*****PASSED*****\nResponse: {1}'.format(new_share,grant_admin))
                pass
            else:
                grant_admin_retry = test.update_share_access(share_name = new_share, username='admin', access='RO')
                if grant_admin_retry == 0:
                    print '\n**********Share has already been existed**********\n'
                    logger1.info('**********\"Admin\" has been successfully granted Read access to the Private Share \"{0}\"*****PASSED*****\nResponse: {0}'.format(new_share))
                else:
                    logger1.error('**********Failed to update "Admin"\'s access to private Share \"{0}\"*****FAILED*****\n**********Response: \"{1}\"**********'.format(new_share,grant_admin_retry))
                pass    
        except Exception, ex:                 
            logger1.error('Failed to update \"Admin\"\'s Read access to the Private Share \"{0}\"'.format(new_share) + str(ex))
            pass
        #5)##############################################################################################################################################
    def Enable_Recycle_Bin_for_all_shares(self):    
        #6)##############################################################################################################################################    
        # Enabling Recycle Bin for all the current Shares
        try:
            #Enable Recycle Bin for private share "FRTC_share"
            FRTC_share_recyc_text = None
            count = 3 
            test.access_webUI(use_ssl=False)
            time.sleep(6)
            test.get_to_page('Shares')
            time.sleep(5)
            state = test.is_element_visible('shares_share_FRTC_share')
            if not state:
                test.click_element('shares_shareDown_link')        
            test.click_element('shares_share_FRTC_share')
            time.sleep(5)
            FRTC_share_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container') 
            if FRTC_share_recyc_text == 'ON':
                logger1.info('**********The Recycle Bin for {1} is already enabled*****PASSED*****\nStatus=\"{0}\"'.format(FRTC_share_recyc_text,new_share))    
                pass
            while (FRTC_share_recyc_text == None) or (FRTC_share_recyc_text == 'OFF') and count >= 0:            
                try:
                    test.click_element('css = #shares_recycle_switch+span .checkbox_container')
                    time.sleep(6)
                    test.reload_page() 
                    time.sleep(4)
                    test.get_to_page('Shares')
                    time.sleep(4)
                    test.click_element('shares_share_FRTC_share')
                    time.sleep(4)
                    FRTC_share_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container')
                    if FRTC_share_recyc_text == 'ON':
                        logger1.info('**********The Recycle Bin for "FRTC_share" has successfully enabled*****PASSED*****\nStatus=\"{0}\"'.format(FRTC_share_recyc_text))                                
                    count = count - 1
                except:
                    logger1.error('**********The Recycle Bin for "FRTC_share" Could not be enabled*****FAILED*****')                    
                    count = -1  
            test.close_webUI()             
        except Exception, ex:
            logger1.error('Could not get or change Recycle Bin state \n' + str(ex))
            test.close_webUI()                
    
        #Enable Recycle Bin for public share "Public"
        try:  
            test.access_webUI(use_ssl=False) 
            public_recyc_text = None
            count = 3        
            test.reload_page()
            time.sleep(6)
            test.get_to_page('Shares')
            time.sleep(5)
            test.click_element('shares_share_Public')
            time.sleep(5)
            public_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container') 
            if public_recyc_text == 'ON':
                logger1.info('**********The Recycle Bin for "Public" is already enabled*****PASSED*****\nStatus=\"{0}\"'.format(public_recyc_text))    
                pass
            while (public_recyc_text == None) or (public_recyc_text == 'OFF') and count >= 0:            
                try:
                    test.click_element('css = #shares_recycle_switch+span .checkbox_container')
                    time.sleep(6)
                    test.reload_page() 
                    time.sleep(4)
                    test.get_to_page('Shares')
                    time.sleep(4)
                    test.click_element('shares_share_Public')
                    time.sleep(4)
                    public_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container')
                    if public_recyc_text == 'ON':
                        logger1.info('**********The Recycle Bin for "Public" has successfully enabled******PASSED****\nStatus={0}'.format(public_recyc_text))            
                    count = count - 1
                except:
                    logger1.error('**********The Recycle Bin for "Public" Could not be enabled*****FAILED*****')  
                    count = -1
        except Exception, ex:
            logger1.error('Could not get or change Recycle Bin state for "SmartWare" share"\n' + str(ex))  
            
        #Enable Recycle Bin for "SmartWare" share 
        try:
            test.local_login(username=test.uut[Fields.web_username], password=test.uut[Fields.web_password])
            smartware_recyc_text = None
            count = 3        
            test.reload_page()
            time.sleep(6)
            test.get_to_page('Shares')
            time.sleep(4)
            state = test.is_element_visible('shares_share_SmartWare')
            if not state:
                test.click_element('shares_shareDown_link')
            test.click_element('shares_share_SmartWare')
            time.sleep(4)
            smartware_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container') 
            if smartware_recyc_text == 'ON':
                logger1.info('**********The Recycle Bin for "SmartWare" is already enabled*****PASSED*****\nStatus={0}'.format(smartware_recyc_text))     
                pass
            while (smartware_recyc_text == None) or (smartware_recyc_text == 'OFF') and count >= 0:            
                try:
                    test.click_element('css = #shares_recycle_switch+span .checkbox_container')
                    time.sleep(6)
                    test.reload_page() 
                    time.sleep(5)
                    test.get_to_page('Shares')
                    time.sleep(5)
                    test.click_element('shares_share_SmartWare')
                    time.sleep(5)
                    smartware_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container')
                    if smartware_recyc_text == 'ON':
                        logger1.info('**********The Recycle Bin for "SmartWare" has successfully enabled*****PASSED*****\nStatus={0}'.format(smartware_recyc_text))            
                    count = count - 1
                except:
                    logger1.error('**********The Recycle Bin for "SmartWare" Could not be enabled*****FAILED*****')  
                    count = -1
            test.close_webUI()
        except Exception, ex:        
            logger1.error('Could not get or change Recycle Bin state for "SmartWare" share"\n' + str(ex))
            test.close_webUI()
                
        #Enable Recycle Bin for "TimeMachineBackup" share 
        try:
            test.close_webUI()
            TMB_recyc_text = None
            count = 3 
            test.local_login(username=test.uut[Fields.web_username], password=test.uut[Fields.web_password])
            test.access_webUI(use_ssl=False)
            time.sleep(6)
            test.get_to_page('Shares')
            time.sleep(4)
            state = test.is_element_visible('shares_share_TimeMachineBackup')
            if not state:
                test.click_element('shares_shareDown_link')
            test.click_element('shares_share_TimeMachineBackup')
            time.sleep(4)
            TMB_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container') 
            if TMB_recyc_text == 'ON':
                logger1.info('**********The Recycle Bin for "TimeMachineBackup" is already enabled*****PASSED*****\nStatus={0}'.format(TMB_recyc_text))
                test.close_webUI()    
                pass
            while (TMB_recyc_text == None) or (TMB_recyc_text == 'OFF') and count >= 0:            
                try:
                    test.click_element('css = #shares_recycle_switch+span .checkbox_container')
                    time.sleep(6)
                    test.reload_page() 
                    time.sleep(5)
                    test.get_to_page('Shares')
                    time.sleep(5)
                    test.click_element('shares_share_TimeMachineBackup')
                    time.sleep(5)
                    TMB_recyc_text = test.get_text('css = #shares_recycle_switch+span .checkbox_container')
                    if TMB_recyc_text == 'ON':
                        logger1.info('**********The Recycle Bin for "TimeMachineBackup" has successfully enabled*****PASSED*****\nStatus={0}'.format(TMB_recyc_text))
                        test.close_webUI()
                        pass            
                    count = count - 1
                except:
                    logger1.error('**********The Recycle Bin for "TimeMachineBackup" Could not be enabled*****FAILED*****')  
                    count = -1
                    test.close_webUI()
                    pass
        except Exception, ex:
            logger1.error('Could not get or change Recycle Bin state for "TimeMachineBackup" share \n' + str(ex))
            test.close_webUI()
            pass               
        #6)##############################################################################################################################################   
    def set_network_to_static(self):       
        #7)##############################################################################################################################################
        # Set the Network mode to Static IP and verify the web UI access
        self.get_new_static_IP()   
        try:        
            test.access_webUI(use_ssl=False)            
            test.get_to_page('Settings')
            test.click_element('settings_network_link')
            time.sleep(3)
            test.click_element('settings_networkLAN0IPv4Static_button')
            time.sleep(2)
            test.click_element('settings_networkIPv4Next1_button')
            time.sleep(2)
            test.input_text(locator='settings_networkIP_text', text = ip_new, do_login=False)
            time.sleep(2)
            test.click_element('settings_networkIPv4Next2_butotn')
            time.sleep(2)
            test.click_element('settings_networkIPv4Next3_button')
            time.sleep(2)
            test.click_element('settings_networkIPv4Next4_button')
            time.sleep(2)
            test.click_element('popup_apply_button')
            time.sleep(30)        
            test.uut[Fields.internal_ip_address] = ip_new
            test.close_webUI()
            try:       
                test.access_webUI(use_ssl=False)   
                test.get_to_page('Settings')
                time.sleep(1)
                test.click_element('settings_network_link')
                time.sleep(3)
                mac_add_text = test.get_text('mac_div')
                if  (mac_add_text != '') and (mac_add_text != None):
                    logger1.info('**********Successfully accessed web UI after switching from DHCP to Static IP Configuration******PASSED****\nMAC ADDRESS = \"{0}\"'.format(mac_add_text))
                    test.close_webUI()
                    pass
                else:
                    logger1.error('**********Failed to access web UI after Static IP modification******FAILED****\n**********DUT MAC ADDRESS = \"{0}\"**********'.format(mac_add_text))
                    test.close_webUI()
            except Exception, ex:
                logger1.error('**********Could not access the Web UI after changing the IP address to static**********\n' + str(ex)) 
                test.close_webUI()
        except Exception, ex:
            logger1.error('**********Could not set static IP for the network mode**********\n' + str(ex)) 
            test.close_webUI() 
            pass          
        #7)##############################################################################################################################################        
    def create_a_new_folder_under_public_thru_web(self):     
        #8)##############################################################################################################################################        
        # Create a Public Share folder through Web UI 
        try:        
            test.access_webUI(use_ssl=False)                            
            test.get_to_page('Apps')
            time.sleep(5)
            test.click_element('apps_webfileviewer_link')
            time.sleep(5) 
             
            test.select_frame('mainFrame')
            time.sleep(3)
            # Row 1 is the Public folder in this frame but there is a possibility if a new SHARE or a new USER creation distract this order and then PUBLIC will move to second of third ROW !! 
            test.double_click_element('css = #row1')
            time.sleep(3)
            test.click_element('css = #Apps_webfileviewerCreate_link')
            time.sleep(2)
            
            test.unselect_frame()
            time.sleep(1)        
            test.input_text(locator='css = #Apps_webfileviewerCreate_text', text=new_folder, do_login=False) # css = #Apps_webfileviewerCreate_text
            time.sleep(1)        
            test.click_button('Apps_webfileviewerCreateSave_button')
            logger1.info('**********Successfully created new folder under Public share******PASSED****\n The Created folder = \"{0}\"'.format(new_folder))
            pass                
        except Exception, ex:
            logger1.error('**********Failed to create a new folder under Public Share**********\n' + str(ex)) 
            pass       
        #8)############################################################################################################################################## 
    def copy_generated_contents_to_new_folder_under_public(self):   
        #9)##############################################################################################################################################
        # Copy some contents to a public folder under "Public" Share, with the NEW USER Credential
        file_no = 10
        try:
            dir_exist = os.path.isdir(temp_local_folder)
            if not dir_exist:
                os.mkdir(temp_local_folder)
            second_dir_exist = os.path.isdir(temp_local_folder+os.sep+'gen_files')
            if not second_dir_exist:
                os.mkdir(temp_local_folder+os.sep+'gen_files')        
            generated_files = test.generate_test_files(kilobytes=10240, file_count = file_no, extension='jpg')        
            self.copy_files_to_uut(file_list=generated_files, target_path='/shares/Public/'+new_folder+os.sep)        
            time.sleep(20)        
            read = self.copy_files_from_uut(file_list=generated_files, source_path='/shares/Public/'+new_folder+os.sep, local_target_path=temp_local_folder+os.sep+'gen_files'+os.sep,delete_after_copy=True)
            if read:           
                logger1.info('**********Successfully generated, written and read \"{0}\" files to/from \"{1}\" share ******PASSED****'.format(file_no,'Public'))                      
                pass
            else:
                logger1.error('**********Failed to generate, write or read files from {0} share ******FAILED****'.format('Public'))            
                pass 
        except Exception, ex:
            logger1.error('Failed to generate, write or read the files thru Samba' + str(ex))           
            pass           
        #9)##############################################################################################################################################        
    def detect_usb_and_full_access_to_new_user(self):
        #10)#############################################################################################################################################
        # Detect the USB share and name and give a new full access to the recently created user in step 2 , also Media Serving for the USB share will be disabled here
        update_to_private = None
        global USB_share_name; USB_share_name = []
        try:
            usb_found=False
            current_shares = test.get_share(share_name='')     
            tags_desc = test.get_xml_tags(xml_content=current_shares[1], tag = 'description')
            for n in range(0, len(tags_desc)):    
                if str(tags_desc[n]).startswith('USB'):
                    usb_found = True
                    tags_all_names = test.get_xml_tags(xml_content=current_shares[1], tag='share_name')
                    USB_share_name.append(tags_all_names[n])
                    break
                else:
                    usb_found = False
            if usb_found:
                logger1.info('********USB is attached to the Unit********\n********The name of the USB share is = \"{0}\"********'.format(USB_share_name[0]))
                try:
                    try:                    
                        update_to_private = test.update_share(share_name=USB_share_name[0], description='USB Device Share', public_access=False, media_serving='none')
                        if update_to_private == 0:
                            logger1.info('**********\"{0}\" share has successfully changed to Private share - Also Media Serving is now disabled**********\nResponse: {1}'.format(USB_share_name[0],update_to_private))
                            pass
                        else:
                            logger1.error('**********Unable to Update the share \"{0}\" from Public to a Private share with Media Serving disabled**********\nResponse: {1}'.format(USB_share_name[0],update_to_private))
                            pass
                    except Exception, ex:
                        logger1.error('Failed to change Public share to private share for \"{0}\"' + str(ex)).format(USB_share_name[0])
                        pass
                    if update_to_private == 0: 
                        try: 
                            # TODO: An Extra step over manual procedure. I had to grant "Admin" the READ/WRITE privilege to the USB Share, otherwise later on, the copying files to the USB Share will fail.
                            test.create_share_access(share_name = USB_share_name[0], username = 'admin', access='RW')                                       
                            grant_user = test.create_share_access(share_name = USB_share_name[0], username = new_user, access='RW')                         
                            if grant_user == 0:
                                logger1.info('**********\"{0}\" has been successfully granted full access to the \"{1}\" Share**********\nResponse: {2}'.format(new_user,USB_share_name[0],grant_user))
                                pass
                            else:
                                grant_user = test.update_share_access(share_name = USB_share_name[0], username = new_user, access='RW')
                                test.update_share_access(share_name = USB_share_name[0], username = 'admin', access='RW')
                                if grant_user  == 0:
                                    logger1.info('**********\"{0}\" has been successfully granted full access to the \"{1}\" Share**********\nResponse: {2}'.format(new_user, USB_share_name[0], grant_user))                                
                                    pass
                                else:
                                    logger1.error('**********Failed to update \"{0}\" access to \"{1}\" Share**********\nResponse: {2}'.format(new_user, USB_share_name[0], grant_user))
                                    pass
                        except:
                            grant_user = test.update_share_access(share_name = USB_share_name[0], username = new_user, access='RW')
                            #Get the response of the update access (tag of Status)                        
                            if grant_user  == 0:
                                logger1.info('**********\"{0}\" has been successfully granted full access to the \"{1}\" Share**********\nResponse: {2}'.format(new_user, USB_share_name[0], grant_user))
                                pass
                            else:
                                logger1.error('**********Failed to update \"{0}\" access to \"{1}\" Share**********Here is the Rest Call Response:\nResponse: {2}'.format(new_user, USB_share_name[0], grant_user))
                                pass
                            pass   
                except Exception, ex:
                    logger1.error( '**********Failed to update \"{0}\" access to \"{1}\" Share**********\n'.format(new_user, USB_share_name[0] + str(ex)))
                    pass                
            else:
                logger1.error('********No USB is attached to the Unit****FAILED****')
                pass
        except Exception, ex:
            logger1.error('Failed to detect any USB drive connected to the Unit' + str(ex))  
            pass        
        #10)#############################################################################################################################################      
    def copy_generated_contents_to_usb_share(self):
        #11)#############################################################################################################################################

        if USB_share_name != None and USB_share_name != '':
            file_no = 10
            try:
                dir_exist = os.path.isdir(temp_local_folder)
                if not dir_exist:
                    os.mkdir(temp_local_folder)
                second_dir_exist = os.path.isdir(temp_local_folder+os.sep+'gen_files_usb')
                if not second_dir_exist:
                    os.mkdir(temp_local_folder+os.sep+'gen_files_usb')
                response = test.create_directory(share_name=USB_share_name[0], dir_path=new_folder)
                response_id = test.get_xml_tag(xml_content=response[1].content , tag='error_id')
                if response[0] == 0 or response_id == '21':            
                    generated_files = test.generate_test_files(kilobytes=5012, file_count = file_no, extension='jpg')        
                    self.copy_files_to_uut(file_list=generated_files, target_path='/shares/'+USB_share_name[0]+os.sep+new_folder+os.sep)        
                    time.sleep(20)        
                    read = self.copy_files_from_uut(file_list=generated_files, source_path='/shares/'+USB_share_name[0]+os.sep+new_folder+os.sep, local_target_path=temp_local_folder+os.sep+'gen_files_usb'+os.sep,delete_after_copy=True)
                    if read:           
                        logger1.info('**********Successfully generated, written and read \"{0}\" files to/from \"{1}\" share******PASSED****'.format(file_no,USB_share_name[0]))                      
                        pass
                    else:
                        logger1.error('**********Failed to generate, write or read files from {0} share ******FAILED****'.format(USB_share_name[0]))            
                        pass        
            except Exception, ex:
                logger1.error('Failed to generate, write or read the files thru Samba to the USB share' + str(ex)) 
        #11)#############################################################################################################################################
    def turn_off_Oplocks_for_usb_share(self):    
        #12)#############################################################################################################################################
        # Turning off Oplocks for USB share 
        try:
            count = 3
            Oplocks_text = None        
            status_changed = None        
            test.close_webUI()
            if USB_share_name != None and USB_share_name != '':
                usb_share_element = str('shares_share_'+USB_share_name[0]) # Example should look like : shares_share_Patriot_Memory-1
                test.local_login(username=test.uut[Fields.web_username], password=test.uut[Fields.web_password])    
                test.access_webUI(use_ssl=False)
                time.sleep(3)        
                test.get_to_page('Shares')
                time.sleep(4)
                test.click_element(usb_share_element)
                time.sleep(3)
                Oplocks_text = test.get_text('css = #shares_oplocks_switch+span .checkbox_container') 
                if Oplocks_text == 'ON':
                    logger1.info('**********The Oplocks status for \"{1}\" is enable**********\nStatus={0}'.format(Oplocks_text, USB_share_name[0]))
                    try:
                        test.click_element('css = #shares_oplocks_switch+span .checkbox_container')
                        time.sleep(7)
                        test.reload_page() 
                        time.sleep(4)
                        test.get_to_page('Shares')
                        test.click_element(usb_share_element)
                        time.sleep(5)
                        Oplocks_text = test.get_text('css = #shares_oplocks_switch+span .checkbox_container')
                        if Oplocks_text == 'OFF':
                            logger1.info('**********The Oplocks for \"{1}\" has successfully disabled*****PASSED*****\nStatus={0}'.format(Oplocks_text,USB_share_name[0]))
                            status_changed = True                    
                            pass
                    except Exception, ex:
                        logger1.error('**********The Oplocks for \"{1}\" Share Could not be disabled*****FAILED*****'.format(USB_share_name[0])) 
                        status_changed = False
                        pass
                while (status_changed != True) and ((Oplocks_text == None) or (Oplocks_text == 'OFF') and count >= 0):            
                    try:
                        test.click_element('css = #shares_oplocks_switch+span .checkbox_container')
                        time.sleep(6)
                        test.reload_page() 
                        time.sleep(4)
                        test.get_to_page('Shares')
                        test.click_element(usb_share_element)
                        time.sleep(6)
                        Oplocks_text = test.get_text('css = #shares_oplocks_switch+span .checkbox_container')
                        if Oplocks_text == 'ON':
                            logger1.info('**********The Oplocks for \"{1}\" has successfully enabled*****PASSED*****\nStatus={0}'.format(Oplocks_text,USB_share_name[0]))
                            test.close_webUI()
                            pass            
                        count = count - 1
                    except:
                        logger1.error('**********The Oplocks for \"{1}\" Share Could not be enabled*****FAILED*****'.format(USB_share_name[0])) 
                        count = -1
                        test.close_webUI()
                        pass
        except Exception, ex:
            logger1.error('**********Could not get or change Oplocks state for \"{0}\" Share**********\n'.format(USB_share_name[0]) + str(ex))
            test.close_webUI()
            pass 
        #12)#############################################################################################################################################
    def copy_ftp_media_files_to_UUT_public_subfolders(self):   
        #13)#############################################################################################################################################
        # # @brief Copy new photos/videos/music to Shared Pictures/Shared Music and Shared Videos folders under PUBLIC share
        # # @brief Trying to validate the ftp server up and Copy the files from there to the Local OS server and then to the UUT share 
        #  @param sample of FTP File: ftp://64.58.146.206/Public/media/music/Wishbone Ash - Here To Hear - 01 - Cosmic Jazz.mp3
        #  @param Sample of SMB path on UUT: smb://192.168.1.101/Public/Shared Music/Wishbone Ash - Here To Hear - 01 - Cosmic Jazz.mp3
        #  @return Copy the Music files   
        music_files = []
        tmp_local_path_music_files  = []
        filenames = None         
        try:        
            dir_exist = os.path.isdir(temp_local_folder)
            if not dir_exist:
                os.mkdir(temp_local_folder)
            ftp = ftplib.FTP(FTP_server_URL)
            ftp.login()
            ftp.cwd('Public')
            ftp.cwd('media')  
            ftp.cwd('music')        
            if str(ftp.welcome).startswith('220'):
                print ftp.getwelcome()
                music_files.append(ftp.nlst())
                music_files[0].remove('.')
                music_files[0].remove('..')
                for filenames in music_files[0]:
                    tmp_mus = posixpath.os.path.join(temp_local_folder,filenames) 
                    tmp_local_path_music_files.append(tmp_mus)
                    with open(tmp_mus,'wb') as closure:
                        def callback(data):
                            closure.write(data)
                        ftp.retrbinary('RETR %s' %filenames , callback)            
                time.sleep(8)
                ftp.close()  
                self.Copy_files_from_local_to_the_device(source_path_file_list=tmp_local_path_music_files, target_path=music_target_path, delete_after_from_local=False)
                logger1.info('\n**********Successfully Copied 10 Music files from FTP server to the UUT location \"{0}\"**********\n'.format(music_target_path))                      
            else:
                logger1.error('**********Could not find or connect to the FTP server \"{0}\"**********'.format(FTP_server_URL))
                ftp.close()              
        except Exception, ex:
            logger1.error('**********Could not Copy the Music Media files from \"{0}\" to \"{1}\" location**********'.format(music_source_path,music_target_path) + str(ex))
            ftp.close()
               
        # Copy the Picture files 
        pic_files = []
        tmp_local_path_pic_files  = []
        filenames = None       
        try:
            dir_exist = os.path.isdir(temp_local_folder)
            if not dir_exist:
                os.mkdir(temp_local_folder)
            ftp = ftplib.FTP(FTP_server_URL)
            ftp.login()
            ftp.cwd('Public')
            ftp.cwd('media')  
            ftp.cwd('pictures')        
            if str(ftp.welcome).startswith('220'):
                print ftp.getwelcome()
                pic_files.append(ftp.nlst())
                pic_files[0].remove('.')
                pic_files[0].remove('..')
                pic_files[0].remove('.wdmc')
                for filenames in pic_files[0]:
                    tmp_pix = posixpath.os.path.join(temp_local_folder,filenames) 
                    tmp_local_path_pic_files.append(tmp_pix)
                    with open(tmp_pix,'wb') as closure:
                        def callback(data):
                            closure.write(data)
                        ftp.retrbinary('RETR %s' %filenames , callback)
                time.sleep(8)
                ftp.close()  
                self.Copy_files_from_local_to_the_device(source_path_file_list=tmp_local_path_pic_files, target_path=pix_target_path, delete_after_from_local=False)
                logger1.info('\n**********Successfully Copied 10 Picture files from FTP server to the UUT location \"{0}\"**********\n'.format(pix_target_path))               
            else:
                logger1.error('Could not find or connect to the FTP server \"{0}\"'.format(FTP_server_URL))
                ftp.close()               
        except Exception, ex:
            logger1.error('Could not Copy the Picture Media files from \"{0}\" to \"{1}\" location'.format(pix_source_path,pix_target_path) + str(ex))
            ftp.close()
            
        # Copy the Video files 
        vid_files = []
        tmp_local_path_vid_files  = []
        filenames = None     
        try:
            dir_exist = os.path.isdir(temp_local_folder)
            if not dir_exist:
                os.mkdir(temp_local_folder)
            ftp = ftplib.FTP(FTP_server_URL)
            ftp.login()
            ftp.cwd('Public')
            ftp.cwd('media')  
            ftp.cwd('videos')        
            if str(ftp.welcome).startswith('220'):
                print ftp.getwelcome()
                vid_files.append(ftp.nlst())
                vid_files[0].remove('.')
                vid_files[0].remove('..')
                vid_files[0].remove('.wdmc')
                for filenames in vid_files[0]:
                    tmp_vid = posixpath.os.path.join(temp_local_folder,filenames) 
                    tmp_local_path_vid_files.append(tmp_vid)
                    with open(tmp_vid,'wb') as closure:
                        def callback(data):
                            closure.write(data)
                        ftp.retrbinary('RETR %s' %filenames , callback)   
                time.sleep(8)
                ftp.close()  
                self.Copy_files_from_local_to_the_device(source_path_file_list=tmp_local_path_vid_files, target_path=vid_target_path, delete_after_from_local=False)
                logger1.info('\n**********Successfully Copied 10 Video files from FTP server to the UUT location \"{0}\"**********\n'.format(vid_target_path))          
            else:
                logger1.error('Could not find or connect to the FTP server \"{0}\"'.format(FTP_server_URL))
                ftp.close()               
        except Exception, ex:
            logger1.error('Could not Copy the Video Media files from \"{0}\" to \"{1}\" location'.format(vid_source_path,vid_target_path) + str(ex))
            ftp.close()    
        #13)############################################################################################################################################# 
    def generate_a_DAC_code_for_new_user(self):   
        #14)#############################################################################################################################################
        # Get a DAC code (Cloud Access) for the "frtc_user" 
        try:
            test.close_webUI()
            test.local_login(username=test.uut[Fields.web_username], password=test.uut[Fields.web_password])
            test.access_webUI(use_ssl=False)
            test.get_to_page('Cloud Access')
            time.sleep(3)
            test.click_button('cloud_account_frtc_user')
            time.sleep(3)
            test.click_button('cloud_account_frtc_user')
            time.sleep(2)
            test.click_element('cloud_account_frtc_user')
            time.sleep(5)
            test.click_button('cloud_getCode_button')
            time.sleep(5)
            test.click_button('cloud_getCodeOK_button')
            time.sleep(25)
            if test.is_element_visible('cloud_statusInfo_value'):            
                cloud_conn_type = test.get_text('cloud_statusInfo_value')        
                if cloud_conn_type == 'Port forwarding connection established.' or cloud_conn_type == 'Relay connection established.':
                    logger1.info('**********Cloud Connection type is: \"{0}\"**********'.format(cloud_conn_type))
                    logger1.info('**********The DAC Code for user \"{0}\" has been successfully created*****PASSED*****\n**********Status={1}**********'.format(new_user,cloud_conn_type))
                else:
                    logger1.info('**********Cloud Connection type is: \"{0}\"**********'.format(cloud_conn_type))
                    logger1.warning('**********Could not validate the Connection Status for the created DAC Code for user \"{0}\"*****FAILED*****\nStatus={1}'.format(new_user,cloud_conn_type))
                test.close_webUI()        
        except Exception, ex:
            test.close_webUI() 
            logger1.warning('**********Could not create the DAC Code for user \"{0}\"**********'.format(new_user) + str(ex))
            pass
        #14)#############################################################################################################################################  
    def twonky_modifications(self):    
        #15)#############################################################################################################################################
        # Open the Twonky page and modify the configurations 
        try: 
            test.close_webUI()
            result = test.get_media_server_configuration()
            if result[0] == 0:
                media_state = test.get_xml_tag(xml_content=result[1].content, tag='enable_media_server')
                media_state = str(media_state).lower()
                if not media_state == 'true':
                    test.set_media_server_configuration(enable_media_server=True)           
                # Enable Twonky Logging 
                self.Twonky_set_logging()                   
                # Enable the aggregation 
                self.Twonky_set_aggregation()
                # Change aggregation mode to Aggregate 
                self.Twonky_set_aggregation_mode(Mode='1')            
                # Disable Twonky restart on NIC Changes            
                self.Twonky_set_restart_on_NIC_changes()            
                # Change the rescan interval time to 5 minutes 
                self.Twonky_set_rescan_interval(mins='5')            
                # Change the Server Name 
                self.Twonky_set_server_name(newname='Branded')
                # set a password for above username
                self.Twonky_set_password(pswrd='fituser', username='')
                # set a username for Twonky page 
                self.Twonky_set_username(user='fitlab')            
            else:
                logger1.error('**********Failed to retrieve Media Serving Status**********')       
        except Exception, ex:
            logger1.error('Could not access Twonly page or enable media serving for the DUT' + str(ex))
            pass
        #15)#############################################################################################################################################
    def save_configuration_file(self, target_dir, new_suffix):
        #16)#############################################################################################################################################
        #@ Brief gets the config file of the UUT and saves it to the local system extracted
        # @ parameter target_dir : should be just the local path where you want to save the files
        # @ parameter new_suffix : is the extra extension to the folder name that if happens to be two or more saved configurations then you can identify them
        # Save configuration file and extract it to /tmp/frtc/backup_<suffic>/backup folder  
        global new_local_path_dir
        global xml_file_path
        global sys_info
        global model_number
        # this test is self tc_cleanup , however it keeps the configuration files in the /tmp/frtc directory as reference.
        try:    
            #model_number = test.get_model_number() #O.K. For some reason when the whole code is run and reaches at this point, getting the model number will FAIL !!! 
            # 2014-12-22 12:36:57 ***DEBUG*** Executing command: cat /usr/local/twonky/resources/devicedescription-custom-settings.txt | grep MODELNUMBER on 192.168.1.212 - Called from AAT.SSHClient:__call__:518
            sys_info = test.get_system_information()
            model_number = test.get_xml_tag(xml_content=sys_info[1], tag='model_number')
            dir_exist = os.path.isdir(target_dir)
            if not dir_exist:
                os.mkdir(target_dir)        
            target_dir = target_dir+os.sep
            result = test.get_system_configuration(attach_file=False)
            if result[0] == 0:
                print 'successfully received configuration from UUT\n'
                source = test.get_xml_tag(xml_content=result[1].content, tag='path_to_config')  
                self.copy_files_from_uut(file_list=None, source_path=source, local_target_path=target_dir, delete_after_copy=False)
                new_local_path_zip = target_dir + 'backup.tgz'
                new_local_path_dir = target_dir + model_number+'_'+'backup'+ '_' + new_suffix
                tar = tarfile.open(new_local_path_zip)
                tar.extractall(new_local_path_dir)
                tar.close()
                if os.path.isdir(new_local_path_dir):
                    os.remove(new_local_path_zip)
                    #test.run_on_device('rm /CacheVolume/backup.tgz')
                    xml_file_path = new_local_path_dir+os.sep+'backup'+os.sep+'config.xml'
                    print 'successfully extracted the configuration file to the directory \"{0}\" in Local OS'.format(new_local_path_dir)
                else:
                    print 'Could not extract the configuration file'
                #tar.close()
        except Exception, ex:
            logger1.error('*******Failed to save the configuration files in \"{0}\"*******'.format(new_local_path_dir) + str(ex))
            pass
        #16)#############################################################################################################################################
    def read_xml_file(self, xml_file_path, tag_name):
        #17)#############################################################################################################################################
        #Read the contents and save a dictionary of target tag name
        d = {}        
        #global system_mgr_dict; system_mgr_dict = {}
        if xml_file_path == 1:  
            # TODO: must remove the hardcoded path     
            xml_file_path = temp_local_folder + os.sep +model_number+'_'+'backup'+ '_' + 'new'+os.sep+'backup'+os.sep+'config1.xml'
        if xml_file_path == 2:  
            xml_file_path = temp_local_folder + os.sep +model_number+'_'+'backup'+ '_' + 'new'+os.sep+'backup'+os.sep+'config2.xml'
        try:
            if os.path.isfile(xml_file_path):
                data = minidom.parse(xml_file_path) 
                d = self.xmltodict(data)
                dict_of_parent_tag = self.get_target_element_dict(full_config_xml_content_in_dict_format=d, target_element_tagname=tag_name)
                return dict_of_parent_tag
        except Exception, ex:
            logger1.error('*******Failed to read the XML file content*******' + str(ex))
            pass
        #17)#############################################################################################################################################


    #===========================================================================
    # Twonky section for set/get and some of the commands used in this test case
    #===========================================================================
    def set_rpc (self, command, value, user='',pwd=''):
        if Twonky_username != '':
            user = Twonky_username
        if Twonky_password != '':
            pwd = Twonky_password        
        hostname = 'http'
        if user != '':
            hostname += '://{0}:{1}@{2}'.format(user,pwd,test.uut[Fields.internal_ip_address])
        elif user == '':        
            hostname += '://{0}'.format(test.uut[Fields.internal_ip_address])        
        url = hostname+':9000'+'/rpc/set_option?'+command+'='+value    
        try:
            return test.get_page_contents(url)                
        except Exception, ex:
            logger1.error('Could not SET the Twonky Configuration for \"{0}\" command '.format(command) + str(ex))
    def get_rpc (self, command, user='',pwd=''):
        if Twonky_username != '':
            user = Twonky_username
        if Twonky_password != '':
            pwd = Twonky_password  
        hostname = 'http'
        if user != '':
            hostname += '://{0}:{1}@{2}'.format(user,pwd,test.uut[Fields.internal_ip_address])
        elif user == '':        
            hostname += '://{0}'.format(test.uut[Fields.internal_ip_address])  
        url = hostname+':9000'+'/rpc/get_option?'+command
        try:
            return test.get_page_contents(url)               
        except Exception, ex:
            logger1.error('Could not GET the Twonky Configuration for \"{0}\" command '.format(command) + str(ex))
    def Twonky_set_logging(self, enable=True):
        setvalue = None
        if enable:
            setvalue = '4095'
        elif not enable:
            setvalue = '0' 
        try:
            # get logging status
            response_logging = self.get_rpc('v')           
            if response_logging[0] == 0:
                if not response_logging[1] == setvalue:
                    result = self.set_rpc(command='v', value=setvalue)
                    if result[1] == setvalue:
                        logger1.info('**********Successfully enabled the Logging for Twonky **********')
                    else:
                        logger1.error('**********Failed to \"SET\" the Twonky logging status**********') 
                elif response_logging[1] == setvalue:
                    logger1.info('**********Logging is already enabled for Twonky **********')
            else: 
                logger1.error('**********Failed to \"GET\" the Twonky logging status**********')         
        except Exception, ex:
            logger1.error('Could not access Twonly page for Logging Enable/Disable' + str(ex))        
    def Twonky_set_aggregation(self, enable=True):
        setvalue = None
        if enable:
            setvalue = '1'
        elif not enable:
            setvalue = '0' 
        try:
            response_aggregation = self.get_rpc('aggregation')           
            if response_aggregation[0] == 0:
                if not response_aggregation[1] == setvalue:
                    result = self.set_rpc(command='aggregation', value=setvalue)
                    if result[1] == setvalue:
                        logger1.info('**********Successfully enabled the Aggregation for Twonky **********')
                    else:
                        logger1.error('**********Failed to \"SET\" the Twonky aggregation to enable**********')
                elif response_aggregation[1] == setvalue:
                    logger1.info('**********Twonky aggregation is already enabled for Twonky **********')
            else: 
                logger1.error('**********Failed to \"GET\" the Twonky aggregation status**********')  
        except Exception, ex:
            logger1.error('Could not access Twonly page for Aggregation Enable/Disable' + str(ex))                 
    def Twonky_set_aggregation_mode(self, Mode='1'):
        if Mode =='0':
            mode_text = 'Ignore'
        if Mode == '1':
            mode_text = 'Aggregate'
        if Mode == 2:
            mode_text = 'AutoCopy'
        try:    
            response_aggregation_mode = self.get_rpc('aggmode')           
            if response_aggregation_mode[0] == 0:
                if not response_aggregation_mode[1] == Mode:
                    result = self.set_rpc(command='aggmode', value=Mode)
                    if result[1] == Mode:
                        logger1.info('**********Successfully Changed the Aggregation mode to \"{0}\" for Twonky **********'.format(mode_text))
                    else:
                        logger1.error('**********Failed to \"SET\" the Twonky aggregation Mode to \"{0}\"**********'.format(mode_text))
                elif response_aggregation_mode[1] == Mode:
                    logger1.info('**********Twonky aggregation MODE is already set to \"{0}\" for Twonky **********'.format(mode_text))
            else: 
                logger1.error('**********Failed to \"GET\" the Twonky aggregation Mode status**********') 
        except Exception, ex:
            logger1.error('Could not access Twonly page for getting or setting Aggregation Mode' + str(ex)) 
    def Twonky_set_restart_on_NIC_changes(self, enable=False):
        setvalue = None
        if enable:
            setvalue = '1'
        elif not enable:
            setvalue = '0' 
        try:
            response_NIC_restart = self.get_rpc('nicrestart')           
            if response_NIC_restart[0] == 0:
                if not response_NIC_restart[1] == setvalue:
                    result = self.set_rpc(command='nicrestart', value=setvalue)
                    if result[1] == setvalue:
                        logger1.info('**********Successfully disabled \"Restart on NIC Changes\" for Twonky **********')
                    else:
                        logger1.error('**********Failed to \"SET\" and disable the Restart on NIC Changes for Twonky  **********')
                elif response_NIC_restart[1] == setvalue:
                    logger1.info('**********Restart on NIC Changes is already Disabled for Twonky **********')                                  
            else: 
                logger1.error('**********Failed to \"GET\" the Restart on NIC Changes for Twonky**********')         
        except Exception,ex:
            logger1.error('Could not access Twonly page for changing the Restart on NIC changes' + str(ex)) 
    def Twonky_set_rescan_interval(self, mins ='5'):
        try:
            response_rescan_interv = self.get_rpc('scantime')           
            if response_rescan_interv[0] == 0:
                if not response_rescan_interv[1] == mins:
                    result = self.set_rpc(command='scantime', value=mins)
                    if result[1] == mins:
                        logger1.info('**********Successfully set the Rescan interval time to \"{0}\" minutes for Twonky **********'.format(mins))
                    else:
                        logger1.error('**********Failed to \"SET\" the Rescan interval time to \"{0}\" minutes for Twonky  **********'.format(mins))
                elif response_rescan_interv[1] == mins:
                    logger1.info('**********Rescan interval time is already set to \"{0}\" minutes for Twonky **********'.format(mins))                                  
            else: 
                logger1.error('**********Failed to \"GET\" the Rescan interval time for Twonky**********')                 
        except Exception,ex:
            logger1.error('Could not access Twonly page for changing Rescan Interval time' + str(ex)) 
    def Twonky_set_server_name(self, newname='Branded'):
        try:
            response_server_name= self.get_rpc('friendlyname')           
            if response_server_name[0] == 0:
                if not response_server_name[1] == newname:
                    result = self.set_rpc(command='friendlyname', value=newname)
                    if result[1] == newname:
                        logger1.info('**********Successfully set the Server Name to \"{0}\" for Twonky **********'.format(newname))
                    else:
                        logger1.error('**********Failed to \"SET\" the Server Name to \"{0}\" for Twonky **********'.format(newname))
                elif response_server_name[1] == newname:
                    logger1.info('**********Server Name is already set to \"{0}\" for Twonky **********'.format(newname))                                  
            else: 
                logger1.error('**********Failed to \"GET\" the Server Name for Twonky**********')    
        except Exception, ex:
            logger1.error('Could not access Twonly page for changing Server Name' + str(ex)) 
    def Twonky_set_username(self, user='fitlab'):
        try:
            response_user = self.get_rpc(command='accessuser')                   
            if response_user[0] == 0:
                if not response_user[1] == user:
                    result = self.set_rpc(command='accessuser', value=user)
                    if result[1] == user:
                        logger1.info('**********Successfully set the User Name to \"{0}\" for Twonky **********'.format(user))
                    else:
                        logger1.error('**********Failed to \"SET\" the User Name to \"{0}\" for Twonky  **********'.format(user))
                elif response_user[1] == user:
                    logger1.info('**********User Name is already set to \"{0}\" for Twonky **********'.format(user))                                  
            else: 
                logger1.error('**********Failed to \"GET\" the User Name for Twonky**********')         
        except Exception, ex:
            logger1.error('Could not access Twonly page for changing User Name' + str(ex)) 
    def Twonky_set_password(self, pswrd='fituser',username=''):
        try:
            response_pass = self.get_rpc(command='accesspwd',user=username) 
            if response_pass[0] == 0:
                if not response_pass[1] == pswrd:
                    result = self.set_rpc(command='accesspwd', value=pswrd, user=username)
                    if result[1] == pswrd:
                        logger1.info('**********Successfully set the Password to \"{0}\" for Twonky **********'.format(pswrd))
                    else:
                        logger1.error('**********Failed to \"SET\" the Password to \"{0}\" for Twonky **********'.format(pswrd))
                elif response_pass[1] == pswrd:
                    logger1.info('**********Password is already set to \"{0}\" for Twonky **********'.format(pswrd))                                  
            else: 
                logger1.error('**********Failed to \"GET\" the Password for Twonky**********')        
        except Exception, ex:
            logger1.error('Could not access Twonly page for changing Password' + str(ex)) 

    
    #===========================================================================
    # necessary functions for coopying To/From UUT, using generated files or a target file from local or a list of files
    #===========================================================================
    def Copy_files_from_local_to_the_device(self, source_path_file_list, target_path, delete_after_from_local=True):
        n = None
        filelist = None
        try:
            for n in source_path_file_list:        
                print 'FROM %s' %n 
                print 'TO %s' %target_path            
                test.scp_file_to_device(files=n, remote_path=target_path, recursive=False, preserve_times=False)                
                time.sleep(5)       
            time.sleep(5)
            if delete_after_from_local:
                filelist = [ f for f in os.listdir(temp_local_folder) if f.endswith(".mp3") ]
                for f in filelist:
                    os.remove(temp_local_folder+os.sep+f) 
        except Exception, ex:
            logger1.error('**********Could Not Copy File(s) Through SCP connection' + str(ex))
    
    def copy_files_to_uut (self, file_list, target_path):
        # @ BRIEF This function is an extra step toward copying the generated files to the UUT by SCP protocol 
        # @ Arguments, file_list has to be a list of source files with full path
        # @ Arguments, target_path can be any linux path to the location on UUT, such as /share/Public/Shared\ Music or any other location.
        abs_file_list = []
        try:
            for fn in file_list:
                full_abs_path = os.path.abspath(fn)
                abs_file_list.append(full_abs_path)            
            for m in abs_file_list:
                test.scp_file_to_device(files=m, remote_path=target_path, recursive=False, preserve_times=False)  #'/shares/Public/frtc_folder/'
        except Exception, ex:
            logger1.error('**********Could Not Copy File(s) to the UUT Through SCP connection' + str(ex))
      
    def copy_files_from_uut(self, file_list, source_path, local_target_path, delete_after_copy):
        abs_file_list = []
        try:  
            if file_list == [] or file_list == None:
                abs_file_list.append(source_path)                
            else:
                for fn in file_list:
                    full_abs_path = os.path.join(source_path,fn)
                    abs_file_list.append(full_abs_path)        
            for m in abs_file_list:
                test.scp_from_device(path=m, is_dir=False, timeout=30, verbose=False, local_path=local_target_path, retry=False)
            if delete_after_copy:
                filelist = [ f for f in os.listdir(local_target_path) if f.endswith(".jpg") ]
                for f in filelist:
                    os.remove(local_target_path+f)  
            return True # (path='/shares/Public/frtc_folder/', is_dir=False, timeout=90, verbose=False, local_path='/tmp/frtc/gen_files/', retry=False)  
        except Exception, ex:
            logger1.error('**********Could Not Copy File(s) From UUT Through SCP connection' + str(ex))
            return False
 
 
    #===========================================================================
    # Deal with XML and Configuration file read and get the target content by tag name
    #===========================================================================
    
    def compare_parent_tags(self, dict1_path, dict2_path, parent_tag):
        try:
            dict_1 = self.read_xml_file(xml_file_path=dict1_path, tag_name = parent_tag)
            if dict_1 != None and dict_1 != '':
                print dict_1 
            dict_2 = self.read_xml_file(xml_file_path=dict2_path, tag_name = parent_tag)
            if dict_2 != None and dict_2 != '':
                print dict_2  
            compared_dict_0 = compare(dict_1, dict_2)   
            print compared_dict_0
            #Covert the dictionary of changes to LIST, RES is the returned list value and LEN is the number of TAG changes 
            res0, len0 = self.get_the_list_of_changes_in_compared_dict_result(compared_dict_0)
            if len0 > 0:
                for i in res0:
                    # Going through each changes in the list above and seeking one level deep for the actual change  
                    first_dict_change_1 = self.go_one_layer_down_to_dict(my_dict=dict_1, parent_tag=i)
                    second_dict_change_1 = self.go_one_layer_down_to_dict(my_dict=dict_2, parent_tag=i)
                    compared_dict_1 = compare(first_dict_change_1, second_dict_change_1)
                    res1, len1 = self.get_the_list_of_changes_in_compared_dict_result(compared_dict_1)
                    if len1 > 0:
                        print 'The following TAGS under Parent TAG of \"{0}\" have been changed ='.format(i) 
                        print res1
        except Exception, ex:
            logger1.error('Failed to compare changes in given two dictionaries'+ str(ex))
    
    def get_the_list_of_changes_in_compared_dict_result (self, compared_result_dict):
        global new_list_of_changed_parent_tags; new_list_of_changed_parent_tags = []
        try:
            for m, n in compared_result_dict.iteritems() :
                if m == 'changed':
                    for e in n:
                        new_list_of_changed_parent_tags.append(e)
            lenght = len(new_list_of_changed_parent_tags)
            return new_list_of_changed_parent_tags, lenght
        except Exception, ex:
            logger1.error('Failed to get the list of changes in the compared results of two Dictionaries'+ str(ex))
           
    def get_target_element_dict (self, full_config_xml_content_in_dict_format, target_element_tagname):
        dictionary = {}        
        try:
            for parent_key, parent_value in full_config_xml_content_in_dict_format.iteritems():
                if parent_key == '#document':                    
                    for child_key, child_value in parent_value.iteritems():
                        if child_key == 'config':
                            for sub_child_key, sub_child_value in child_value.iteritems():
                                if sub_child_key == target_element_tagname:
                                    if not isinstance(sub_child_value , dict):
                                        dictionary.update({sub_child_key : sub_child_value})
                                        return dictionary                                    
                                    else:
                                        for target_sub_child_key, target_sub_child_value in sub_child_value.iteritems():
                                            dictionary.update({target_sub_child_key : target_sub_child_value})
                                        return dictionary            
        except Exception, ex:
            logger1.error('Failed to get into the target element in the XML file content'+ str(ex))
      
    def go_one_layer_down_to_dict (self, my_dict, parent_tag):
        layered_dict = {}
        try:
            for p_key, p_val in my_dict.iteritems():
                if p_key == parent_tag:
                    for ch_key, ch_val in p_val.iteritems():
                        layered_dict.update({ch_key : ch_val})
                    return layered_dict            
        except Exception, ex:
            logger1.error('Failed to get into the target element into the given Dictionary'+ str(ex)) 
        
    def build_dict(self, dictionary, key, item):
        """Append item to dictionary at key.  Only create a list if there is more than one item for the given key.
        dictionary[key]=item if key doesn't exist.
        dictionary[key].append(item) if key exists."""
        if key in dictionary.keys():
            if not isinstance(dictionary[key], list):
                lst=[]
                lst.append(dictionary[key])
                lst.append(item)
                dictionary[key]=lst
            else:
                dictionary[key].append(item)
        else:
            dictionary.setdefault(key, item)

    def node_attributes(self, node):
        """Return an attribute dictionary """
        if node.hasAttributes():
            return dict([(str(attr), str(node.attributes[attr].value)) for attr in node.attributes.keys()])
        else:
            return None

    def attr_str(self,node):
        return "%s-attrs" % str(node.nodeName)

    def hasAttributes(self, node):
        if node.nodeType == node.ELEMENT_NODE:
            if node.hasAttributes():
                return True
        return False

    def with_attributes(self, node, values):
        if self.hasAttributes(node):
            if isinstance(values, dict):
                self.build_dict(values, '#attributes', self.node_attributes(node))
                return { str(node.nodeName): values }
            elif isinstance(values, str):
                return { str(node.nodeName): values,
                         self.attr_str(node): self.node_attributes(node)}
        else:
            return { str(node.nodeName): values }

    def xmltodict(self, node):
        """Given an xml dom node tree,
        return a python dictionary corresponding to the tree structure of the XML.
        This parser does not make lists unless they are needed.  For example:
    
        '<list><item>1</item><item>2</item></list>' becomes:
        { 'list' : { 'item' : ['1', '2'] } }
        BUT
        '<list><item>1</item></list>' would be:
        { 'list' : { 'item' : '1' } }
    
        This is a shortcut for a particular problem and probably not a good long-term design.
        """
        if not node.hasChildNodes():
            if node.nodeType == node.TEXT_NODE:
                if node.data.strip() != '':
                    return str(node.data.strip())
                else:
                    return None
            else:
                return self.with_attributes(node, None)
        else:
            #recursively create the list of child nodes
            childlist=[self.xmltodict(child) for child in node.childNodes if (self.xmltodict(child) != None and child.nodeType != child.COMMENT_NODE)]
            if len(childlist)==1:
                return self.with_attributes(node, childlist[0])
            else:
                #if False not in [isinstance(child, dict) for child in childlist]:
                new_dict={}
                for child in childlist:
                    if isinstance(child, dict):
                        for k in child:
                            self.build_dict(new_dict, k, child[k])
                    elif isinstance(child, str):
                        self.build_dict(new_dict, '#text', child)
                    else:
                        print "ERROR"
                return self.with_attributes(node, new_dict)

    def load(self, fname):
        return self.xmltodict(minidom.parse(fname))

    def list_to_str(self, node, lst=None, level=0):
        if lst==None:
            lst=[]
        if not isinstance(node, dict) and not isinstance(node, list):
            lst.append(' "%s"' % node)
        elif isinstance(node, dict):
            for key in node.keys():
                lst.append("\n%s(%s" % (level, key)) #lst.append("\n%s(%s" % (spaces(level), key))
                self.list_to_str(node[key], lst, level+2)
                lst.append(")")
        elif isinstance(node, list):
            lst.append(" [")
            for item in node:
                self.list_to_str(item, lst, level)
            lst.append("]")
        return lst
    
    def get_new_static_IP(self):
        ip_split = []
        global ip_new; ip_new = ''
        try:
            ip_split = (test.uut[Fields.internal_ip_address]).split('.',3)
            ip_int = int(ip_split[3])
            ip_new_sub = ip_int + 100
            ip_new = ip_split[0]+'.'+ip_split[1]+'.'+ip_split[2]+'.'+str(ip_new_sub)
            #'192.168.1.102' 
            return ip_new         
        except Exception, ex:
            logger1.error('Failed to assign an static IP address for the UUT' + str(ex))
            return 1

    def tc_cleanup(self):
        try:        
            # /shares/Public/'+new_folder+os.sep
            dir_exist = os.path.isdir(temp_local_folder)
            if dir_exist:
                filelist = [ f for f in os.listdir(temp_local_folder+os.sep) ]
                for f in filelist:
                    if os.path.isfile(temp_local_folder+os.sep+f):
                        os.remove(temp_local_folder+os.sep+f) 
                for f in filelist:
                    if os.path.isdir(temp_local_folder+os.sep+f):
                        if not str(f).startswith(model_number):
                            #os.removedirs(temp_local_folder+os.sep+f) 
                            shutil.rmtree(path=temp_local_folder+os.sep+f, ignore_errors=True)
                # TODO: We want to keep the configuration folder and files in here, so removing it at this point doesnt make sense, until I figure out a way to post them to silk or so.
                #-------------------------- if os.path.isdir(temp_local_folder):
                    #-------------------------- os.removedirs(temp_local_folder)
            # TODO: add the clean up for the configuration file and directory      
            logger1.info('*******Successfully cleaned up the Files in local system Directory \"{0}\"*******'.format(temp_local_folder+os.sep))        
        except Exception, ex:
            logger1.warning('*******Failed to clean up the Copied files from local system Location at \"{0}\"*******'.format(temp_local_folder) + str(ex))
            pass
        try:
            #---------- test.run_on_device('rm /shares/Public/%s/*' %new_folder)
            #-------- test.run_on_device('rm -rf /shares/Public/%s' %new_folder)
            #--------------------- test.run_on_device('rm %s*' %pix_target_path)
            #--------------------- test.run_on_device('rm %s*' %vid_target_path)
            #------------------- test.run_on_device('rm %s*' %music_target_path)
            test.run_on_device('rm /CacheVolume/backup.tgz')
            USB = dir().count(USB_share_name)
            if USB == 1:            
            #if USB_share_name != [] and USB_share_name[0] != None and USB_share_name[0]!= '':
                usb_target_cleanup_path = '/shares/'+USB_share_name[0]+os.sep+new_folder
                test.run_on_device('rm -rf %s' %usb_target_cleanup_path)
            logger1.info('*******Successfully cleaned up the copied files from all locations on DUT*******') 
        except Exception, ex:
            logger1.warning('*******Failed to clean up the copied files from all locations on DUT*******' + str(ex))
            pass      
    
    def verify_the_deep_level_of_tags(self, list_of_changes):
        global two_layer_tag_list; two_layer_tag_list = ['test', 'setting', 'iSNS', 'list', 'mail_event','ipv6', 'tunnel_broker', 'trap']
        global three_layer_tag_list; three_layer_tag_list = ['sharedfolders','user.log','stime', 'wd_crontab','app_get_info',
                                                             'recycle_bin_clear','chk_wfs_download','random_check','user_expire_chk',
                                                             'fw_available','item id="1"','item id="2"','item id="3"','item id="4"',
                                                             'item id="5"','item id="6"','item id="7"']
        try:            
            for j in list_of_changes:
                if j in  two_layer_tag_list:
                    return 2
                elif j in three_layer_tag_list:
                    return 3
                else:
                    return 1
        except Exception, ex:
            logger1.error('Failed to detect the layers of given tag in the list of changes'+ str(ex))
             
FactoryRestore()




