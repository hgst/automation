"""
Created on June 16th, 2015

@author: vasquez_c

## @brief User shall able to FTP download via web UI as account
#
#  @detail 
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=32375&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=50&nTP=117472&etab=8

"""

import time
import os

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as E


ampm = '12'
military = '24'


ntpserver = 'time1.google.com'

class settingsPageGeneralLanguageAndClock(TestClient):
    
    def run(self):
        self.log.info('Settings Page General Language and Clock test started')
        self.log.info('Saving configuration file...')
        self.download_config_file('backup.config')
        self.clock_setting()
        self.language_setting('Italiano')
        self.language_setting('English', verified=False)
        self.time_zone_setting()
        self.primary_server_config()
         
    def primary_server_config(self):
        self.click_wait_and_check(E.SETTINGS_GENERAL_NTP_SERVICE, 'css=#settings_generalNTP_switch+span .checkbox_container .checkbox_on', timeout=10)
        self.click_wait_and_check(E.SETTINGS_GENERAL_PRIMARY_SERVER_LINK, E.SETTINGS_GENERAL_NTP_SERVER_SAVE_BUTTON)
        # Now check if needs to create new ntp service in dialog box
        if self.is_element_visible(E.SETTINGS_GENERAL_NTP_SERVER_ADD_BUTTON):
            self.click_element(E.SETTINGS_GENERAL_NTP_SERVER_ADD_BUTTON)

        self.input_text_check(E.SETTINGS_GENERAL_NTP_SERVER_TEXT, ntpserver)
        self.click_wait_and_check(E.SETTINGS_GENERAL_NTP_SERVER_SAVE_BUTTON, E.SETTINGS_GENERAL_PRIMARY_SERVER_LINK, timeout=30)
        time.sleep(5)
        returned_text = self.get_text(E.SETTINGS_GENERAL_NTP_SERVER_VALUE)
        
        if returned_text == ntpserver:
            self.log.info('Test Passed. NTP server was added successfully')
        else:
            self.log.error('Test Failed. NTP server was not added successfully')   
            
    def language_setting(self, language, verified=True):
        original_lang = self.check_language()
        self.log.info('Original Language is: {0}'.format(original_lang))
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', E.GENERAL_BUTTON, timeout=30)
        self.click_wait_and_check(E.GENERAL_BUTTON, E.SETTINGS_GENERAL_LANGUAGE)
        self.click_wait_and_check(E.SETTINGS_GENERAL_LANGUAGE, 'link={}'.format(language))
        self.click_wait_and_check('link={}'.format(language), E.SETTINGS_GENERAL_LANGUAGE_SAVE)
        self.click_wait_and_check(E.SETTINGS_GENERAL_LANGUAGE_SAVE, 'css=div.menu_icon', timeout=30)
        if verified:
            changed_lang = self.check_language()
            self.verify_language(changed_lang)
        self.close_webUI()
        
    def clock_setting(self):
        self.switch_time_format(time_format=ampm)
        time.sleep(3)
        actual_time = self.get_time()
        self.verify_time_format(actual_time, ampm)
        self.switch_time_format(time_format=military)
        actual_time = self.get_time()
        time.sleep(3)
        self.verify_time_format(actual_time, military)
        
    def time_zone_setting(self):
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', E.GENERAL_BUTTON)
        self.click_wait_and_check(E.GENERAL_BUTTON, E.SETTINGS_GENERAL_TIME_ZONE)
        self.change_time_zone('(GMT-10:00) Hawaii')
        time.sleep(3)
        self.verify_time_zone('Hawaii')
          
    def change_time_zone(self, time_zone):
        self.click_wait_and_check(E.SETTINGS_GENERAL_TIME_ZONE, E.SETTINGS_GENERAL_TIMEZONE_LIST)
        self.click_wait_and_check('link={}'.format(time_zone), E.SETTINGS_GENERAL_TIMEZONE_SAVE)
        self.click_wait_and_check(E.SETTINGS_GENERAL_TIMEZONE_SAVE, visible=False)
         
    def get_time(self):   
        time_format = self.get_text(E.SETTINGS_GENERAL_DATE_TIME)
        self.log.info('Time: {0}'.format(time_format))
        return time_format
    
    def switch_time_format(self, time_format):
        self.click_wait_and_check(E.GENERAL_BUTTON, E.SETTINGS_GENERAL_TIME_FORMAT)
        self.click_wait_and_check(E.SETTINGS_GENERAL_TIME_FORMAT, 'link={}'.format(time_format))
        self.click_wait_and_check('link={}'.format(time_format), E.UPDATING_STRING, visible=False)
        
    def verify_time_format(self, actual_time, expected_time_format): 
        if ('AM' in actual_time) or ('PM' in actual_time):
            time_format = ampm
        else:
            time_format = military
            
        if time_format == expected_time_format:
            self.log.info('Test PASSED: set time format and expected time format matched')
        else:
            self.log.error('Test FAILED: set time format and expected time format did not match')
    
        ret_val = (self.execute('cat /etc/language.conf')[1]).split()
        return ret_val[1]
    
    def verify_time_zone(self, changed_time_zone): 
        time_zone = (self.execute('cat /etc/timezone'))
        
        if changed_time_zone in time_zone[1]:
            self.log.info('Test PASSED: current time zone and expected time zone matched')
        else:
            self.log.error('Test FAILED: current time zone and expected time zone did not match')
                          
    def check_language(self):
        ret_val = (self.execute('cat /etc/language.conf')[1]).split()
        return ret_val[1]
    
    def verify_language(self, changed_lang):
        if 'it_IT' == changed_lang:
            self.log.info('Language was changed to Italian, test PASSED')
        else:
            self.log.error('Language did not changed, test FAILED')
            
    def tc_cleanup(self):
               
        # Restore to previous configuration
        self.config_restore()

    def config_restore(self):
        self.log.info('*** Restoring saved configuration... ***')
        config_file = os.path.join(os.getcwd(), 'backup.config')

        # Restore to previous configuration
        self.log.info('Restoring saved configuration...')
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', E.SETTINGS_UTILITY)
        self.click_wait_and_check('settings_utilities_link', 'settings_utilitiesConfig_button')
        self.log.info("Click upload config")
        self.press_key('settings_utilitiesImport_button', config_file)
        self.log.info("System going to reboot...")

        #self.upload_config_file('backup.config')

        # Loop until the pings stop
        start = time.time()
        while self.is_ready():
            time.sleep(0.5)
            try:
                if time.time() - start >= self.uut[self.Fields.reboot_time]:
                    raise Exception('Device failed to power off within {0} seconds'
                                    .format(self.uut[self.Fields.reboot_time]))
            except Exception as e:
                self.log.exception('Fail to power down within {0} seconds by UI'.format(repr(e)))
        time.sleep(5)

        start_time = time.time()
        while not self.is_ready():
            self.log.debug('Web is not ready')
            try:
                if time.time() - start_time > self.uut[self.Fields.reboot_time]:
                    raise Exception('Timed out for waiting {0} seconds to boot'
                                    .format(self.uut[self.Fields.reboot_time]))
            except Exception as e:
                self.log.exception('Fail to boot up within {0} seconds by UI'.format(repr(e)))
        self.close_webUI()
        self.ssh_connect()
        os.remove(config_file)

settingsPageGeneralLanguageAndClock()