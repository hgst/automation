"""
Created on May 27th, 2015

@author: tran_jas

## @brief User shall able to FTP download via web UI as regular user
#
#  @detail
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=31797&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=50&nTP=117563&etab=8

"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

ftp_file_path = '/shares/Public/'
ftp_user = 'ftpuser'
my_ftp_user = 'jtran'
ftp_password = 'fituser'
ftp_filename = 'AFP-74BD.mp4'
file_md5_sum = 'b776a446fa43d7b735854de315cbb840'
rm_mp4 = 'rm /shares/Public/*.mp4'
ftp_file = "FTP_file-01"
ftp_port = '21'


class FtpDownloadAccount(TestClient):
    # Create tests as function in this class

    def run(self):
        try:
            self.log.info('User page - FTP download as anonymous started')

            # ensure accept EULA as admin - in case EULA hasn't been accepted
            self.access_webUI()
            self.close_webUI()

            self.create_user(my_ftp_user, password=ftp_password)
            self.uut[self.Fields.web_username] = my_ftp_user
            self.uut[self.Fields.web_password] = ftp_password
            self.ftp_downloader_account()

        except Exception, ex:
            self.log.exception('Failed to complete FTP download \n' + str(ex))
        finally:
            self.log.info('Delete downloaded file')
            self.execute(rm_mp4)

            time.sleep(3)
            self.click_element('apps_ftpdownloadsDelJob1_button')
            while self.is_element_visible('popup_apply_button') is False:
                time.sleep(5)
                self.click_element('apps_ftpdownloadsDelJob1_button')

            self.click_element('popup_apply_button')

            self.delete_user(my_ftp_user)
            self.uut[self.Fields.web_username] = 'admin'
            self.uut[self.Fields.web_password] = ''
            self.close_webUI()

    def ftp_downloader_account(self):

        ftp_server_ip = self.uut[self.Fields.ftp_server]
        self.log.info('ftp_server_ip: {}'.format(ftp_server_ip))

        self.access_webUI()

        if self.is_element_visible('nav_downloads_link'):
            self.log.info('Testing as regular user')
            self.click_element('nav_downloads_link')
            self.click_element('//li[@id=\'ftp_downloads\']/span')
        else:
            self.get_to_page('Apps')
            self.click_element('apps_ftpdownloads_link')

        # Select to create FTP job with authentication
        self.click_element('apps_ftpdownloadsCreate_button')
        self.click_element('apps_ftpdownlaodsAccount_button')

        # Create FTP job
        self.log.info("Creating FTP job...")
        self.input_text('apps_ftpdownloadsTaskName_text', ftp_file, True)
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.input_text('apps_ftpdownloadsHostPort_text', ftp_port, True)
        self.input_text('apps_ftpdownloadsUsername_text', ftp_user, True)
        self.input_text('apps_ftpdownloadsPassword_text', ftp_password, True)

        self.click_element('apps_ftpdownloadsSourcepath_button', 120)
        time.sleep(5)
        self.click_element('link=ftpshare')
        time.sleep(1)
        # self.click_element('//div[@id=\'folder_selector\']/ul/li[3]/ul/li[2]/label/span', 120)
        # self.click_element("//li[3]/ul/li[2]/label/span", 120)
        self.click_element_at_coordinates("link=AFP-74BD.mp4", -100, 0)

        self.click_element('home_treeOk_button')

        self.click_element('apps_ftpdownloadsDestpath_button', 120)
        time.sleep(5)
        # self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li/label/span')
        # self.click_element('//div[@id=\'folder_selector\']/ul/li/label/span', 120)
        self.click_element_at_coordinates('link=Public', -70, 0)
        time.sleep(5)
        self.click_element('home_treeOk_button')

        self.click_element('apps_ftpdownloadsCreate3_button')
        self.click_element('apps_ftpdownloadsGoJob1_button')
        time.sleep(10)
        # refresh page
        self.click_element('//li[@id=\'ftp_downloads\']/span')
        my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
        self.log.info('Current ftp downloading status: {0}'.format(my_text))
        max_wait = 0
        while my_text != "Download Completed" and max_wait < 15:
            time.sleep(60)
            max_wait += 1
            self.log.info('Waiting for ftp download, loop count: {0}'.format(max_wait))
            self.click_element('//li[@id=\'ftp_downloads\']/span')
            my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
            self.log.info('Current ftp downloading status: {0}'.format(my_text))

        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path, ftp_filename)
        if file_md5_sum == ftpdownloadedfilesum:
            self.log.info('FTP file download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP file download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

FtpDownloadAccount()