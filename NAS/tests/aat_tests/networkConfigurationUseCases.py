'''
Created on Feb 10 2015

@author: Carlos Vasquez

Objective:  To verify that all network settings are configurable and operate correctly

Note: There is no need to run this test frequently. It should, however, be run in its entirely early in the test
      proccess and again as the firmware nears release.
      
'''

import os
import time
import string
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import TestClient

fourBayNAS = ['LT4A', 'BNEZ', 'BWZE']

NETMASKS = ['255.255.0.0','255.255.128.0','255.255.192.0','255.255.224.0','255.255.240.0','255.255.248.0',
            '255.255.252.0','255.255.254.0',  '255.255.255.0']

class networkConfiguration(TestClient):   
    def run(self):  
        global newIP
         
        originalIP = self.uut[Fields.internal_ip_address]
        lastOctet = originalIP.split('.')[-1]
        newOctet = int(lastOctet) + 100
        newIP = string.replace(str(originalIP),str(lastOctet), str(newOctet))  
         
                     
        for netmask in NETMASKS:
            self.uut[Fields.internal_ip_address] = originalIP
            ifnameReturned = ''.join(self.get_xml_tags(self.get_network_configuration()[1],'ifname'))
            self.execute('setNetworkStatic.sh ifname={0} {1} {2}'.format(ifnameReturned, newIP, netmask), timeout=-1)
             
            self.uut[Fields.internal_ip_address] = newIP          
            self.validateWebUI()
            self.click_element(self.Elements.SETTINGS_NETWORK_NETWORK_DHCP)
            self.click_button('settings_networkIPv4Next1_button')
            self.click_element(locator='settings_networkDNS1_radio')
            self.click_wait_and_check(locator='settings_networkIPv4Next2_butotn')
            self.click_wait_and_check(locator='settings_networkIPv4Next3_button')
            self.click_wait_and_check(locator='settings_networkIPv4Next4_button')
            time.sleep(30)
            self.close_webUI()       
        self.uut[Fields.internal_ip_address] = originalIP
    def validateWebUI(self):    
        self.log.info('Validating WebUI can be reached using new IP Address.')
        self.access_webUI()
    
        if self.element_find(self.Elements.LOGIN_WD_LOGO):
            self.log.info('Success: Able to reach WebUI using new IP Address.')
        else:
            self.log.error('Test Failed: Unable to reach WebUI using new IP Address')
        self.close_webUI()
        
    def tc_cleanup(self):
        self.log.info('Running tc_clean...')
        self.reset_network_config_dhcp()

networkConfiguration()