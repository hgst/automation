""" Full - Alert Message (MA-30)

    @Author: lin_ri
    http://wiki.wdc.com/wiki/Alert_Messages
    
    Automation: Partial
    
    Manual:
    
    Supported Models: 'Big 5' and 'Lightning' 
    Not supported Models: Avatar, Sequoia, 3G NAS/Zermatt and 4NC/FFxNC 
    
    1) Check-in the cellphone# and email you'd like to trigger for alerts
    2) Verify if you do receive the Test SMS and Test email
    Test Script will loop over alert template to generate all alerts but clean 5 alerts every round
    3) Verify the critical alerts are displayed on LCD
    4) Verify only one warning or info alert will be displayed on the LCD
    5) Verify each alert must be emailed with alert code, the message and the firmware version in the email message
    6) Verify each alert must be sent out via SMS. SMS message is only sent with a text message(no alert code or f/w version)
    LCD Bootup Alert
    7) Reboot the unit and verify if the initial boot up message must be displayed
    8) The default after boot-up messages must be displayed on the LCD
    Disable LCD
    9) Go to settings->General-Energy Saver-> LCD OFF, 
   10) Trigger some alerts. Verify they are not shown on the LCD    
    
    Test Status:
              Local Ligtning:

   Removed alerts:
   1) 1020
   2) 1122
   3) 1030 (Not used by Alpha, how about WD?)
   4) 1004 (duplicate as 0001, so remove)
   5) 1021
   6) 2221 (Remove this for RED drive only)

   Obsolete notes:
   Big5
   - Generated code(1403) with PRI=1 does NOT match PRI=2 on UI...
   - Generated code(1402) with PRI=1 does NOT match PRI=2 on UI...

   KC/Zion
   - Generated code(1036) with PRI=1 does NOT match PRI=0 on UI...
     1036 --> 0036
   - Generated code(2002) with PRI=2 does NOT match PRI=1 on UI...
     2002 --> 1011

  Note:
   SKY-2887(Can not login Web UI after batch trigger email/SMS alerts)
    1. With email/SMS alert enabled, Web UI will no longer respond when multiple alerts are generated (alert_test)
    2. The root cause is the DNS server setting in NAS.
    If we have incorrect DNS server being set in NAS,
    the web UI starts to behave abnormal when multiple alerts are generated.
""" 

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.webdriver.common.keys import Keys
import time
import os

alertTable = {
# Critical
#    code       Title
    '0001': {'MSG': 'System Over Temperature',    'PRI': 0 },
    '0002': {'MSG': 'System Under Temperature',   'PRI': 0 },
    '0003': {'MSG': 'Drive SMART Failure',        'PRI': 0 , 'PARM': '2'},  # The drive %1 self-check failed
    '0004': {'MSG': 'Volume Failure',             'PRI': 0 },  # The data volume %1 on the drive is not accessible
    '0005': {'MSG': 'Pending Thermal Shutdown',   'PRI': 0 },
    '0006': {'MSG': 'Thermal Shutdown Immediate', 'PRI': 0 },
    '0029': {'MSG': 'Fan Not Working',            'PRI': 0 },
    '0031': {'MSG': 'On UPS Power',               'PRI': 0 , 'PARM': '26'},  # %1% power left.
    '0201': {'MSG': 'Drive Failed',               'PRI': 0 , 'PARM': '1'},  # Drive failed in Bay %1
    '0207': {'MSG': 'Drive About to Fail',        'PRI': 0 , 'PARM': '1'},  # Drive %1 is about to fail
    '0208': {'MSG': 'Volume Degraded',            'PRI': 0 , 'PARM': 'vol2'},  # Volume %1 degraded
    '0212': {'MSG': 'Volume Rebuild Failed',      'PRI': 0 , 'PARM': 'vol3'},  # The volume %1 rebuild has failed
    '0213': {'MSG': 'Volume Expansion Failed',    'PRI': 0 , 'PARM': 'vol4'},  # The volume %1 Expansion failed
    '0224': {'MSG': 'Replace Drive with Red LED', 'PRI': 0 },
    '0042': {'MSG': '50% UPS Power Left',         'PRI': 0 },
    '0043': {'MSG': '15% UPS Power Left',         'PRI': 0 },
    '0044': {'MSG': 'UPS Out of Power',           'PRI': 0 },
    '0036': {'MSG': 'File System Check Failed',   'PRI': 0 },

# Warning
#    code       Title
    '1001': {'MSG': 'Volume Usage is Above 95%',           'PRI': 1 , 'PARM': 'vol2'}, # Your volume %1 usage is close to full capacity
    '1002': {'MSG': 'Network Link Down',                   'PRI': 1 , 'PARM': '1'},  # network link %1 is down
    '1003': {'MSG': 'Firmware Update Failed',              'PRI': 1 },
    '1004': {'MSG': 'High system temperature',             'PRI': 1 },  # Remove - dup as 0001 & Alpha not using
    '1021': {'MSG': 'Restore Config Failed',               'PRI': 1 },  # Remove as this should be UI process warning. Not alert
    '1022': {'MSG': 'Power Supply Failure',                'PRI': 1 , 'PARM': '1'},  # Power supply %1 failed
    '1030': {'MSG': 'Storage Below Threshold',             'PRI': 1 },  # Remove, not used by Alpha
    '1037': {'MSG': 'System Not Ready',                    'PRI': 1 },
    '1120': {'MSG': 'Unsupported USB Device',              'PRI': 1 , 'PARM': 'STEC STECABC S00001'},  # (vendor: %1, model: %2, serial number: %3)
    '1121': {'MSG': 'Unsupported File System',             'PRI': 1 },
    '1122': {'MSG': 'Unable to Create Share',              'PRI': 1 , 'PARM': 'STEC STECABC S00001 jffs2 FakeDrive fakeshare'},  # (vendor %1, model: %2, serial number: %3, file system: %4, label: %5, share name: %6)  # Remove as this should be UI process warning. Not alert
    '1123': {'MSG': 'Unsafe Device Removal',               'PRI': 1 , 'PARM': 'STEC STECABC S00001'},  # (vendor: %1, model: %2, serial number: %3)
    '1124': {'MSG': 'Unable to Mount USB Device',          'PRI': 1 , 'PARM': 'STEC STECABC S00001'},  # (vendor: %1, model: %2, serial number: %3)
    '1127': {'MSG': 'Cannot Backup Files from USB Device', 'PRI': 1 },
    '1130': {'MSG': 'USB Backup Queued',                   'PRI': 1 },
    '1400': {'MSG': 'Remote Backup Error',                 'PRI': 1 , 'PARM': 'fakeremotejob'},  # An error occurred for the remote backup job named %1
    '1401': {'MSG': 'Remote Backup Restore Error',         'PRI': 1 , 'PARM': 'fakeremotejob'},  # An error occurred for the remote backup job named %1
    '1011': {'MSG': 'New Firmware Available',              'PRI': 1 },
    '1028': {'MSG': 'Reboot Required',                     'PRI': 1 },
    '1200': {'MSG': 'Unsupported Drive',                   'PRI': 1 , 'PARM': '1'},  # Unsupported drive in Bay %1 # should be Warning not Informational
    '1501': {'MSG': 'App Update Available',                'PRI': 1 , 'PARM': 'fakeapp'},  # %1 app has update available
    '1045': {'MSG': 'File System Error Corrected',         'PRI': 1 },
    '1035': {'MSG': 'Product Improvement Program',         'PRI': 1 },
    '1502': {'MSG': 'Virus Found',                         'PRI': 1 },
    
# Informational
#    code       Title
    '2003': {'MSG': 'Temperature Normal',           'PRI': 2 },
    '2004': {'MSG': 'Firmware Update Successful',   'PRI': 2 , 'PARM': '10.0.000'},  # The firmware was successfully updated to %1
    '2005': {'MSG': 'Factory Restore Succeeded',    'PRI': 2 },
    '2024': {'MSG': 'Downloading Firmware Update',  'PRI': 2 },
    '2026': {'MSG': 'Installing Firmware Update',   'PRI': 2 },
    '2032': {'MSG': 'System is in standby mode',    'PRI': 2 },
    '2033': {'MSG': 'System Rebooting',             'PRI': 2 },
    '2038': {'MSG': 'Storage Quota Almost Full',    'PRI': 2 , 'PARM': 'test1'},  # Storage quota for [%1] is almost full
    '2039': {'MSG': 'Storage Quota Full',           'PRI': 2 , 'PARM': 'test1'},  # Storage quota for [%1] has reached the limit
    '2040': {'MSG': 'Unsupported UPS device'  ,     'PRI': 2 },
    '2041': {'MSG': 'Ethernet Port %1 Connected at 10/100', 'PRI': 2 , 'PARM': '1'},  # Ethernet Port %1 connected at 10/100
    '2121': {'MSG': 'Locked USB device',            'PRI': 2 , 'PARM': 'STEC STECABC S00001'},  # A locked USB device has been detected (vendor: %1, model: %2, serial number: %3)
    '2126': {'MSG': 'Files Copied from USB device', 'PRI': 2 , 'PARM': 'USB-A USB-B'},  # Files copied from <%1> to <%2>
    '2128': {'MSG': 'Copying Files',                'PRI': 2 },
    '2129': {'MSG': 'Files Moved from USB device',  'PRI': 2 , 'PARM': 'USB-A USB-C'},  # Files moved from <%1> to <%2>
    '2131': {'MSG': 'Moving Files',                 'PRI': 2 },
    '2214': {'MSG': 'Drive Inserted',               'PRI': 2 , 'PARM': '1'},  # A new drive has been inserted into Bay %1
    '2215': {'MSG': 'Drive Removed',                'PRI': 2 , 'PARM': '1'},  # Drive Removed from Bay %1
    '2217': {'MSG': 'No Drives Installed',          'PRI': 2 },
    '2218': {'MSG': 'Hot Spare Drive Added',        'PRI': 2 },
    '2219': {'MSG': 'RAID Migration Completed',     'PRI': 2 },
    '2220': {'MSG': 'RAID Rebuild Completed',       'PRI': 2 },
    '2221': {'MSG': 'Non-WD Red NAS drive inserted','PRI': 2 },  # Removed
    '2222': {'MSG': 'Volume Formatted',             'PRI': 2 , 'PARM': 'vol2'},  # Volume %1 is formatted
    '2223': {'MSG': 'Expanding Volume Progress',    'PRI': 2 , 'PARM': 'vol4 66'},  # Volume %1 expanding progress: %2%
    '2300': {'MSG': 'Media Scan Stopped',           'PRI': 2 },
    '2209': {'MSG': 'Volume Migration Progress',    'PRI': 2 , 'PARM': 'vol3 88'},  # Volume %1 migration progress: %2 %
    '2210': {'MSG': 'Rebuilding Volume',            'PRI': 2 , 'PARM': 'vol4 10'},  # Rebuild volume %1 time in progress: %2 minutes
    '2216': {'MSG': 'RAID Roaming Enabled',         'PRI': 2 },
    '2020': {'MSG': 'System Shutting Down',         'PRI': 2 },
    '2503': {'MSG': 'Virus Scan Completed',         'PRI': 2 },
    '2504': {'MSG': 'Scheduled Virus Scan Started', 'PRI': 2 },
    '2505': {'MSG': 'Scheduled Virus Scan Stopped', 'PRI': 2 },
    '1020': {'MSG': 'System Shutting Down',         'PRI': 2 },
    '0209': {'MSG': 'Volume Migration Progress',    'PRI': 2 , 'PARM': 'vol2 95'},  # Volume %1 Migration Progress - %2 %
    '0210': {'MSG': 'Rebuilding Volume',            'PRI': 2 , 'PARM': 'vol3 6'},   # Rebuild volume %1 time in progress: %2 minutes
    '0216': {'MSG': 'RAID Roaming Enabled',         'PRI': 2 },
    '2402': {'MSG': 'Remote Backup Success',        'PRI': 2 , 'PARM': 'fakeremotejob sda4 fakeshare'},  # (was 1402) Your remote backup job name: %1, successfully completed on the device: %2, to the share: %3
    '2403': {'MSG': 'Remote Backup Restore Success', 'PRI': 2 , 'PARM': 'fakerestorejob sda4 fakeshare'},  # (was be 1403) Your remote restore job name: %1, successfully completed on the device: %2, to the share: %3
### REMOVE 1020
}

KcZionTable = {
# Critical
#    code       Title
    '0001': {'MSG': 'System Over Temperature',    'PRI': 0 },
    '0002': {'MSG': 'System Under Temperature',   'PRI': 0 },
    '0003': {'MSG': 'Drive SMART Failure',        'PRI': 0 , 'PARM': '2'},  # The drive %1 self-check failed
    '0004': {'MSG': 'Volume Failure',             'PRI': 0 },  # The data volume %1 on the drive is not accessible
    '0005': {'MSG': 'Pending Thermal Shutdown',   'PRI': 0 },
    '0006': {'MSG': 'Thermal Shutdown Immediate', 'PRI': 0 },
    '0029': {'MSG': 'Fan Not Working',            'PRI': 0 },
    '0031': {'MSG': 'On UPS Power',               'PRI': 0 , 'PARM': '26'},  # %1% power left.
    '0201': {'MSG': 'Drive Failed',               'PRI': 0 , 'PARM': '1'},  # Drive failed in Bay %1
    '0207': {'MSG': 'Drive About to Fail',        'PRI': 0 , 'PARM': '1'},  # Drive %1 is about to fail
    '0208': {'MSG': 'Volume Degraded',            'PRI': 0 , 'PARM': 'vol2'},  # Volume %1 degraded
    '0212': {'MSG': 'Volume Rebuild Failed',      'PRI': 0 , 'PARM': 'vol3'},  # The volume %1 rebuild has failed,
    '0213': {'MSG': 'Volume Expansion Failed',    'PRI': 0 , 'PARM': 'vol4'},  # The volume %1 Expansion failed,
    '1036': {'MSG': 'File System Check Failed',   'PRI': 0},  # KC/Zion Only, Now is 0036 (PRI=0)
# Warning
#    code       Title
    '1001': {'MSG': 'Volume Usage is Above 95%',           'PRI': 1 , 'PARM': 'vol2'}, # Your volume %1 usage is close to full capacity
    '1002': {'MSG': 'Network Link Down',                   'PRI': 1 , 'PARM': '1'},  # network link %1 is down
    '1003': {'MSG': 'Firmware Update Failed',              'PRI': 1 },
    '1004': {'MSG': 'High system temperature',             'PRI': 1 },  # Remove - dup as 0001 & Alpha not using
    '1021': {'MSG': 'Restore Config Failed',               'PRI': 1 },  # Remove as this should be UI process warning. Not alert,
    '1030': {'MSG': 'Storage Below Threshold',             'PRI': 1 },  # Remove, not used by Alpha
    '1037': {'MSG': 'System Not Ready',                    'PRI': 1 },
    '1120': {'MSG': 'Unsupported USB Device',              'PRI': 1 , 'PARM': 'STEC STECABC S00001'},  # (vendor: %1, model: %2, serial number: %3)
    '1121': {'MSG': 'Unsupported File System',             'PRI': 1 },
    '1122': {'MSG': 'Unable to Create Share',              'PRI': 1 , 'PARM': 'STEC STECABC S00001 jffs2 FakeDrive fakeshare'},  # (vendor %1, model: %2, serial number: %3, file system: %4, label: %5, share name: %6)  # Remove as this should be UI process warning. Not alert
    '1123': {'MSG': 'Unsafe Device Removal',               'PRI': 1 , 'PARM': 'STEC STECABC S00001'},  # (vendor: %1, model: %2, serial number: %3)
    '1124': {'MSG': 'Unable to Mount USB Device',          'PRI': 1 , 'PARM': 'STEC STECABC S00001'},  # (vendor: %1, model: %2, serial number: %3)
    '1127': {'MSG': 'Cannot Backup Files from USB Device', 'PRI': 1 },
    '1130': {'MSG': 'USB Backup Queued',                   'PRI': 1 },
    '1400': {'MSG': 'Remote Backup Error',                 'PRI': 1 , 'PARM': 'fakeremotejob'},  # An error occurred for the remote backup job named %1
    '1401': {'MSG': 'Remote Backup Restore Error',         'PRI': 1 , 'PARM': 'fakeremotejob'},  # An error occurred for the remote backup job named %1
    '1028': {'MSG': 'Reboot Required',                     'PRI': 1 },
    '1200': {'MSG': 'Unsupported Drive',                   'PRI': 1 , 'PARM': '1'},  # Unsupported drive in Bay %1 # should be Warning not Informational,
    '2002': {'MSG': 'New Firmware Available', 'PRI': 1},   # Now is 1011, so PRI=1
    '0022': {'MSG': 'Power Supply Failure',       'PRI': 1 , 'PARM': '1'},  # (previously 1022) Power supply %1 failed
# Informational
#    code       Title
    '2402': {'MSG': 'Remote Backup Success',        'PRI': 2 , 'PARM': 'fakeremotejob sda4 fakeshare'},  # (Previously 1402) Your remote backup job name: %1, successfully completed on the device: %2, to the share: %3,
    '2403': {'MSG': 'Remote Backup Restore Success','PRI': 2 , 'PARM': 'fakerestorejob sda4 fakeshare'},  # (Previously 1403) Your remote restore job name: %1, successfully completed on the device: %2, to the share: %3,
    '2003': {'MSG': 'Temperature Normal',           'PRI': 2 },
    '2004': {'MSG': 'Firmware Update Successful',   'PRI': 2 , 'PARM': '10.0.000'},  # The firmware was successfully updated to %1
    '2005': {'MSG': 'Factory Restore Succeeded',    'PRI': 2 },
    '2024': {'MSG': 'Downloading Firmware Update',  'PRI': 2 },
    '2026': {'MSG': 'Installing Firmware Update',   'PRI': 2 },
    '2032': {'MSG': 'System is in standby mode',    'PRI': 2 },
    '2040': {'MSG': 'Unsupported UPS device'  ,     'PRI': 2 },
    '2121': {'MSG': 'Locked USB device',            'PRI': 2 , 'PARM': 'STEC STECABC S00001'},  # A locked USB device has been detected (vendor: %1, model: %2, serial number: %3)
    '2126': {'MSG': 'Files Copied from USB device', 'PRI': 2 , 'PARM': 'USB-A USB-B'},  # Files copied from <%1> to <%2>
    '2128': {'MSG': 'Copying Files',                'PRI': 2 },
    '2129': {'MSG': 'Files Moved from USB device',  'PRI': 2 , 'PARM': 'USB-A USB-C'},  # Files moved from <%1> to <%2>
    '2131': {'MSG': 'Moving Files',                 'PRI': 2 },
    '2214': {'MSG': 'Drive Inserted',               'PRI': 2 , 'PARM': '1'},  # A new drive has been inserted into Bay %1
    '2217': {'MSG': 'No Drives Installed',          'PRI': 2 },
    '2218': {'MSG': 'Hot Spare Drive Added',        'PRI': 2 },
    '2219': {'MSG': 'RAID Migration Completed',     'PRI': 2 },
    '2220': {'MSG': 'RAID Rebuild Completed',       'PRI': 2 },
    '2221': {'MSG': 'Non-WD Red NAS drive inserted','PRI': 2 },  # Removed
    '2222': {'MSG': 'Volume Formatted',             'PRI': 2 , 'PARM': 'vol2'},  # Volume %1 is formatted
    '2223': {'MSG': 'Expanding Volume Progress',    'PRI': 2 , 'PARM': 'vol4 66'},  # Volume %1 expanding progress: %2%
    '2300': {'MSG': 'Media Scan Stopped',           'PRI': 2 },
    '0209': {'MSG': 'Volume Migration Progress',    'PRI': 2 , 'PARM': 'vol2 95'},  # Volume %1 Migration Progress - %2 %
    '0210': {'MSG': 'Rebuilding Volume',            'PRI': 2 , 'PARM': 'vol3 6'},   # Rebuild volume %1 time in progress: %2 minutes
    '0216': {'MSG': 'RAID Roaming Enabled',         'PRI': 2 },
    '0215': {'MSG': 'Drive Removed',                'PRI': 2 , 'PARM': '1'},  # (Previously 2215) Drive Removed from Bay %1,
    # '1020': {'MSG': 'System Shutting Down',         'PRI': 2 },
    '1033': {'MSG': 'System Rebooting',             'PRI': 2 },   # (Previously 2033)
    '1038': {'MSG': 'Storage Quota Almost Full',    'PRI': 2 , 'PARM': 'test1'},  # (Previously 2038) Storage quota for [%1] is almost full
    '1039': {'MSG': 'Storage Quota Full',           'PRI': 2 , 'PARM': 'test1'},  # (Previously 2039) Storage quota for [%1] has reached the limit,
}

# KC/Zion has their own table
diffTableModels = ('KC2A', 'BZVM')

# LCD models
# LT4A: Lightning, BWZE: Yellowstone, BNEZ: Sprite,
lcd_models = ('LT4A', 'BWZE', 'BNEZ')

invalidAddrList = ['usr.usr.usr', 'd@d$3d', 'usr@ddd$.ccc']

picture_counts = 0

class alertMsg(TestClient):
    def run(self):
        self.log.info('############### (MA-30) Full - Alert Messages TESTS START ###############')
        try:
            self.disable_email_settings()
            self.disable_sms_settings()
            # self.log.info("=== Configure email settings ===")
            # self.config_email_notification(testAddress="fituser@mailinator.com", invalidAddressList=invalidAddrList)
            # self.log.info("VERIFY: Please verify if test mailbox receive test email message")
            # self.log.info("=== Configure SMS settings ===")
            # self.set_sms_notification(vendor='kotsms', username='WDTW', userpwd='WDTWSEVX', mobile=886912345678,
            #                           msg="MSG from WD MyCloud EX4 NAS!!!")
            # self.log.info("VERIFY: Please verify if test mobile receive test SMS message")
            # # Generate all alerts from alert template(xlsx)
            # # Verify 5 alerts and clear before the next 5 new alerts
            self.make_alerts()
            self.log.info("VERIFY: Please verify if you receive SMS & email alerts in your mailbox and cellphone")

            # # SMS alert - username (verify if it accepts username in email format like 'Fituser@mailinator.com')
            # self.log.info("=== Verify if Fituser@mailinator.com can be accepted as username in SMS setting === ")
            # self.set_sms_notification(vendor='kotsms', username='Fituser@mailinator.com', userpwd='WDTWSEV',
            #                           mobile=886912345678, msg="WD MyCloud EX2 NAS!!!")
            # self.log.info("INFO: Due to wrong username, you will not receive the Test SMS")
            # # Disable LCD and trigger some alerts to verify Not on LCD
            # self.log.info("=== Disable LCD and trigger some alerts to verify Not on LCD ===")
            # self.disable_lcd()
            self.execute('alert_test -R')
            self.log.info("VERIFY: Going to generate 3 alerts, please check the LCD if you can see the alerts")
            self.trigger_alerts(num=3)
            time.sleep(5)
        except Exception as e:
            self.log.error("Test Failed: (MA-30) Alert Messages test, exception: {}".format(repr(e)))
        finally:
            self.log.info("====== Clean Up section ======")
            self.execute('alert_test -R')
            self.close_webUI()
            # # self.enable_lcd()
            # self.delete_sms_settings()
            # self.delete_email_settings()
            self.set_web_access_timeout(timeout='30')   # Set timeout back to default value (30)
            self.log.info('############### (MA-30) Full - Alert Messages TESTS END ###############')

    def make_alerts(self):
        """
            Generate all alerts from alert template(xlsx)
            Verify 5 alerts and clear before the next 5 new alerts
        :return:
        """
        global alertTable, diffTableModels
        model_name = self.get_model_number()
        alert_table = alertTable
        self.log.info("=== Generating all alerts({}) from alert template on {} ===".format(len(alert_table), model_name))
        self.log.info("Clean all existing alerts before generating new alerts...")
        self.execute('alert_test -R')
        # UI = 1: Check alerts from UI
        # UI = 0: Check alerts from SSH command
        UI = 1
        try:
            altgen = self.alert_generate(alertTemplate=alert_table)
            alertList = []
            records = 0   # Remember how many alerts are generated
            for i, x in enumerate(altgen):
                records += 1
                self.log.info("Code-{}: {}".format(i+1, x))
                alertList.append(x)
                if (i+1) % 5 != 0:
                    continue
                # when generate 5 alerts
                time.sleep(5)  # Add delay to wait for alert being reflected on UI
                if UI == 1:
                    self.ui_clear_alert(alertList, alert_table)
                else:
                    alertDict = self.alert_get()
                    self.cli_alert_chk(alertDict, alertList)
                    alertDict.clear()  # clear generated alert code history
                    self.execute('alert_test -R')  # clean 5 generated alerts
                alertList[:] = []
                time.sleep(5)  # wait for alert notification sent

            # Remaining alerts
            remAlerts = len(alertList)
            self.log.info("Remaining {} alerts".format(remAlerts))
            if alertList:
                if UI == 1:
                    self.ui_clear_alert(alertList, alert_table)
                else:
                    alertDict = self.alert_get()
                    self.cli_alert_chk(alertDict, alertList)
                    alertDict.clear()  # clear generated alert code history
                    self.execute('alert_test -R')  # clean remaining generated alerts
                alertList[:] = []
            self.log.info("Total # of generated alerts: {}".format(records))
            self.log.info("=== End of {} alerts generation ===".format(len(alert_table)))
        except Exception as e:
            self.log.error("ERROR: Alert generation failed, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='makeAlerts')
        else:
            if self._sel._is_browser():
                self.close_webUI()

    def enable_lcd(self):
        """
            Turn On LCD
        """
        self.log.info("==> Turn On LCD")
        self.get_to_page('Settings')
        self.wait_and_click('settings_general_link')
        time.sleep(2)
        self.log.info("Scroll the window to the bottom")
        self.execute_javascript('window.scrollTo(0, document.body.scrollHeight);')
        time.sleep(2)
        sms_status = self.get_text('css=#settings_generalLcd_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_element('css=#settings_generalLcd_switch+span .checkbox_container')
        sms_status = self.get_text('css=#settings_generalLcd_switch+span .checkbox_container')            
        self.log.info("SMS notification is turned to {}".format(sms_status))
        time.sleep(2)
    
    def trigger_alerts(self, num=3):
        """
            Trigger # of alerts 
            
            @num: (int) # of alerts you'd like to trigger
        """
        global alertTable
        # global alertTable, diffTableModels
        # model_name = self.get_model_number()
        # if model_name in diffTableModels:
        #     alert_table = KcZionTable
        # else:
        #     alert_table = alertTable
        alert_table = alertTable
        altgen = self.alert_generate(alertTemplate=alert_table)
        for i, x in enumerate(altgen, 1):
            self.log.info("Generating {} alert, code: {}".format(i, x))
            if i == num:
                altgen.close()

    def disable_lcd(self, switch=False):
        """
            Turn off LCD
        """
        global lcd_models
        model_name = self.get_model_number()
        if model_name not in lcd_models:
            self.log.info("{} does not have LCD, skip LCD test".format(model_name))
            return
        try:
            if switch:
                switch = 'OFF'
            else:
                switch = 'ON'
            self.log.info("==> Turn {} LCD".format(switch))
            self.get_to_page('Settings')
            self.wait_and_click('settings_general_link')
            time.sleep(2)
            self.log.info("Scroll the window to the bottom")
            self.execute_javascript('window.scrollTo(0, document.body.scrollHeight);')
            time.sleep(2)
            sms_status = self.get_text('css=#settings_generalLcd_switch+span .checkbox_container')
            retry = 3
            while retry > 0:
                if sms_status != switch:
                    self.click_element('css=#settings_generalLcd_switch+span .checkbox_container')
                    sms_status = self.get_text('css=#settings_generalLcd_switch+span .checkbox_container')
                    retry -= 1
                else:
                    break
            self.log.info("SMS notification is turned to {}".format(sms_status))
            time.sleep(2)
        except Exception as e:
            self.takeScreenShot(prefix='disableLcd')
            self.log.exception("ERROR: Fail to disable LCD, exception: {}".format(repr(e)))

    def cli_alert_chk(self, alert5Dict=None, genAlertList=None):
        
        if alert5Dict is None or genAlertList is None:
            self.log.error("ERROR: One of arguments is None")
            return 0
                
        for k in genAlertList:
            if k in alert5Dict.keys():
                self.log.info("PASS: Generated code({}) is found via CLI interface...".format(k))
                alert5Dict.pop(k)
            else:
                self.log.error("ERROR: Generated code({}) can NOT be found via CLI interface...".format(k))
    
    def alert_get(self):
        """
            Use command ./alert_test -g to parse all generated alerts into dictionary
        
            return parsed alert dictionary
        """
        alertDict = {}  # {'0212': {'MSG': 'Alert MSG', 'PRI': 0, 'DESC':'alert desc'}}
        resp = self.execute('alert_test -g')
        alerts = resp[1].split('---->')
        for a in alerts[1:]:
            alertCode = a.split('\n')[2].rsplit('=')[1].strip()  # alertCode = 210
            # alertId = a.split('\n')[3].rsplit('=')[1].strip()    # alertID = 35
            alertPri = int(a.split('\n')[5].rsplit('=')[1].strip())   # priority = 2
            alertMsg = a.split('\n')[6].rsplit('[')[1].rsplit(']')[0] # alertMSG:[Rebuilding Volume]
            alertDesc = a.split('\n')[7].rsplit('alertDesc:[')[1].rsplit(']', 1)[0]  # alertDesc:[Rebuilding volume %1 time in progress:  minutes]
            if len(alertCode) == 1:
                alertCode = '0' * 3 + alertCode
            elif len(alertCode) == 2:
                alertCode = '0' * 2 + alertCode
            elif len(alertCode) == 3:
                alertCode = '0' * 1 + alertCode
            alertDict[alertCode] = { 'MSG': alertMsg, 'PRI': alertPri, 'DESC': alertDesc}
            # self.log.info('{} - Generated Code: {}, MSG: {}, PRI: {}'.format(i, alertCode, alertMsg, alertPri)) # print the alertMSG
        return alertDict        

    def get_scrollbar_position(self):
        """
            Get the current scroll bar position
        :return: the current top px
        """
        scroll = self.element_find('css=.jspDrag')
        scroll_text = scroll.get_attribute('style')
        cur_top = 0.0
        self.log.info("STYLE: {}".format(scroll_text))
        if 'top' not in scroll_text:
            cur_top = 0.0
        else:
            cur_top = float(scroll_text.split('top:')[-1].split('px;')[0].strip())
        return cur_top

    def ui_clear_alert(self, alert5List=None, alertTable=None):
        """
            Check if generated alert code is on UI page
            Clear alerts after check
            
            @alertCode: Pass the generated alert code for verification
        """
        if alert5List == None:
            self.log.error("ui_clear_alert() requires 1 argument")
            return 0
        self.set_web_access_timeout(timeout='60')   # default is 30
        self.get_to_page('Home')
        # self.wait_and_click('css=#notification_toolbar')
        time.sleep(5)
        try:
            # self.click_wait_and_check('css=#notification_toolbar', 'css=#home_alertView_button', visible=True)
            self.click_wait_and_check('id_alert', 'css=#home_alertView_button', visible=True)
        except Exception as e:
            self.log.error("ERROR: No alerts found, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='noalerts')
            self.log.info("===== Check httpd process status & CPU usage START ======")
            print '='*80
            cmd = 'ps aux | grep -v grep | grep httpd'
            output = self.run_on_device(cmd)
            print output
            print '='*80
            cmd2 = 'mpstat -P ALL'
            output = self.run_on_device(cmd2)
            print output
            print '='*80
            self.log.info("===== Check httpd process status & CPU usage END ======")

        # Check if alerts exist
        if self.is_element_visible('css=#home_alertView_button'):
            self.click_wait_and_check('css=#home_alertView_button',
                                      'css=#home_alertViewClose_button', visible=True)
            # code = self.get_text("//div[@class=\'alert_tb\']/table/tbody/tr[3]/td[3]") # Get the first alert code string
            numOfAlerts = len(alert5List)
            icon_sev = {
                'a_icon_c': 0,  # critical icon (PRI == 0)
                'a_icon_w': 1,  # warning icon (PRI == 1)
                'a_icon_i': 2,  # information icon (PRI == 2)
            }
            sev = 'a_icon_c'  # Default value set to critical (PRI == 0)
            row_elem = self.element_find("//div[@id=\'id_alert_all\']")
            rows = len(row_elem.find_elements_by_xpath("//div[@id=\'id_alert_all\']/div"))
            if rows != numOfAlerts:
                self.log.error("ERROR: Generated {} alerts but UI only has {} alerts".format(numOfAlerts, rows))
            else:
                self.log.info("Detect {} alerts on UI, same as generated alerts".format(rows))
            code_dict = {}
            count = 1
            retry = 5
            while count <= rows:
                time.sleep(1)
                while retry >= 0:
                    if count % 2 == 0:
                        if self.is_element_visible('css=div.jspDrag', wait_time=5):
                            cur_top = self.get_scrollbar_position()
                            scroll = 3
                            while cur_top >= 0.0 and scroll >= 0:
                                try:
                                    self.drag_and_drop_by_offset('css=.jspDrag', 0, 80)
                                    self.log.info("Scrolling the bar")
                                except Exception:
                                    pass
                                finally:
                                    if scroll == 0:
                                        self.log.info("Scroll bar reaches the bottom")
                                        break
                                    new_top = self.get_scrollbar_position()
                                    if new_top == cur_top:
                                        scroll -= 1
                                        self.log.info("Did not scroll bar, retry again")
                                    else:
                                        break
                    try:
                        icon = self.element_find("//div[@id=\'id_alert_all\']/div[{}]/table/tbody/tr[1]/td[2]/div".format(count))
                        sev = icon.get_attribute('class')
                        code = self.get_text("//div[@id=\'id_alert_all\']/div[{}]/table/tbody/tr[3]/td[3]".format(count))  # Get the first alert code string
                    except Exception as e:
                        if retry == 0:
                            self.log.error('ERROR: Reach the maximum retry to get UI alert code text')
                            raise
                        self.log.debug("Going to retry, exception: {}".format(repr(e)))
                        retry -= 1
                        continue
                    else:
                        # self.log.info('SEV: {}'.format(sev))
                        code = code.split(':')[1]
                        break
                self.log.info("UI ALERT CODE: {}, SEV: {}".format(code, sev))
                if len(code) == 1:
                    code = '0' * 3 + code
                elif len(code) == 2:
                    code = '0' * 2 + code
                elif len(code) == 3:
                    code = '0' * 1 + code
                # codeList.append(code)
                code_dict[code] = icon_sev[sev]
                count += 1
            # Check alerts
            self.log.info("====== Verify UI alerts ======")
            for k in alert5List:
                if k in code_dict.keys():
                    if alertTable[k]['PRI'] == code_dict[k]:
                        self.log.info("PASS: Generated code({}) is on UI page...".format(k))
                        # codeList.remove(k)
                        code_dict.pop(k)
                    else:
                        self.log.error("ERROR: Generated code({}) with PRI={} does NOT match PRI={} on UI...".format(
                            k, alertTable[k]['PRI'], code_dict[k]))
                else:
                    self.log.error("ERROR: Generated code({}) can not be found on UI".format(k))
                    # self.takeScreenShot(prefix=k)
            time.sleep(1)
            self.log.info("Click Dismiss All button to clear alert messages")
            self.click_wait_and_check('css=#home_alertDelAll_button', visible=False)
        else:  # no alerts on UI, might be removed
            self.log.error("No alerts are found, take screenshot")
            self.takeScreenShot(prefix='noAlertsFound')
        self.execute('alert_test -R')
        # self.close_webUI()
        self.reload_page()
        # self.web_admin_logout()
        time.sleep(5)

    def web_admin_logout(self):
        """
            Logout Web UI without close the browser
        """
        if self._sel._is_browser():
            self.get_to_page('Home')
            self.click_wait_and_check('id_logout', 'home_logout_link', visible=True)
            self.click_wait_and_check('home_logout_link', visible=False)
            self.log.info("LogOut: Succeed to log out admin from WebUI")
        else:
            self.log.info("No browser is available to logout")

    def alert_generate(self, alertTemplate=None):
        """
            Generate all alerts in alertTemplate dict
            
            self.execute('alert_test -R') # Remove all alerts
            self.execute('alert_test -a {} -f'.format('0212')) # Critical, priority: 0
            self.execute('alert_test -a {} -f'.format('1002')) # Warning, priority: 1
            self.execute('alert_test -a {} -f'.format('1020')) # Informational, priority: 2
            self.execute('alert_test -a {} {} -f'.format('2126', '-p A2,B33'))  # Generate alerts with parameters            
        """
        if not alertTemplate:
            raise Exception("ERROR: Please pass correct alert table information")
        for code in alertTemplate:
            # self.log.info("Generate alert code: {}".format(code))
            if 'PARM' in alertTemplate[code].keys():
                self.execute('alert_test -a {} -p {} -f'.format(code, ','.join(alertTemplate[code]['PARM'].split())))
            else:
                self.execute('alert_test -a {} -f'.format(code))
            yield code

    def config_sev_notification(self, severity='c'):
        """
            Configure Alert notification Level

            @severity: 'c': critical only
                       'w': critical and warning
                       'a': All
        """
        self.wait_until_element_is_visible("//div[@id='settings_notificationsAlert_slider']/a")
        # Get Current Level value
        element = self.element_find("//div[@id='settings_notificationsAlert_slider']/a")
        percent_text = element.get_attribute('style')
        percent = int(percent_text.split()[1][:-2])
        self.log.info("Current Notification Level: {}".format(percent))
        time.sleep(2)

        if severity == 'c':
            self.log.info("Setting Notification Level to Critical Only(0%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_LEFT) # 50%
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_LEFT) # 0%
            else:
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_LEFT) # 0%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsAlert_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 0:
                self.log.info("PASS: Alert notification has been set to Critical ONLY successfully...")
            else:
                self.log.error("FAIL: Alert notification Display failed to set Critical ONLY...")
            return
        elif severity == 'w':
            self.log.info("Setting Notification Level to Critical and Warning(50%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_LEFT) # 50%
            elif percent < 50:
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_RIGHT) # 50%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsAlert_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 50:
                self.log.info("PASS: Alert notification has been set to Critical and Warning successfully...")
            else:
                self.log.error("FAIL: Alert notification failed to set Critical and Warning...")
            return
        elif severity == 'a':
            self.log.info("Setting Notification Level to All(100%)...")
            if percent <= 50:
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_RIGHT) # 50%
                self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_RIGHT) # 100%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsAlert_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 100:
                self.log.info("PASS: Alert notification has been set to ALL successfully...")
            else:
                self.log.error("FAIL: Alert notification failed to set ALL...")
            return

    def config_email_notification(self, testAddress="fituser@mailinator.com", invalidAddressList=None):
        """
            Enable and Test email alert
            
            @testAddress: email address you'd like to add in the notification
                          if you set to fituser@mailinator.com, it will set 5 emails from fituser1 to fituser5
            @invalidAddressList: An invalid email pattern list for test
        """
        if not invalidAddressList:
            invalidAddressList = ['usr.usr.usr', 'd@d$3d', 'usr@ddd$.ccc']
        try:
            self.log.info("==> Configure Alert emails notification")
            self.get_to_page('Settings')
            self.click_wait_and_check('settings_notifications_link',
                                      "//div[@id='settings_notificationsDisplay_slider']/a", visible=True)
            # Enable Alert Email
            self.log.info("====== Turn on Alert emails notification ======")
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            if email_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsAlert_switch+span .checkbox_container',
                                          'css=#settings_notificationsAlert_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            self.log.info("Alert emails notification is switched to {}".format(email_status))
            time.sleep(2)
            self.log.info("====== Click Alert emails configure ======")
            self.click_wait_and_check('settings_notificationsAlert_link',
                                      "//div[@id='settings_notificationsAlert_slider']/a", visible=True)

            # Test Alert slider
            self.log.info("====== Slide the alert slider to Warning&Critical, Critical and All ======")
            self.config_sev_notification(severity='w')
            self.config_sev_notification(severity='c')
            self.config_sev_notification(severity='a')
            time.sleep(2)
            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link', wait_time=5):
                self.log.info("Delete one email address")
                self.wait_and_click('settings_notificationsDelMail1_link')
                time.sleep(2)
            self.log.info("All email settings are deleted!")

            # Test invalid email and invalid characters
            self.log.info("====== Test invalid mail address ======")
            if self.is_element_visible('settings_notificationsAddMail_button'):
                self.click_wait_and_check('settings_notificationsAddMail_button',
                                          'settings_notificationsSaveMail_button', visible=True)
                for invalidAddress in invalidAddressList:
                    self.log.info("Send email pattern: {}".format(invalidAddress))
                    self.input_text_check('settings_notificationsMail_text', invalidAddress, do_login=False)
                    self.wait_and_click('settings_notificationsSaveMail_button')
                    if self.is_element_visible('popup_ok_button'):
                        self.log.info('Email check dialog: ' + self.get_text('error_dialog_message'))
                        self.click_wait_and_check('popup_ok_button', visible=False)
                    else:
                        if self.is_element_visible('settings_notificationsDelMail1_link'):
                            self.log.error("ERROR: Invalid email test pattern [{}] is accepted.".format(invalidAddress))
                            # self.takeScreenShot(prefix='InvalidEmailPattern')
            # Add valid email
            self.log.info("====== Add valid mail address ======")
            if self.is_element_visible('settings_notificationsMail_text'):
                if testAddress == 'fituser@mailinator.com':
                    count = 1
                    while count <= 5:
                        testAddress = 'fituser{}@mailinator.com'.format(count)
                        self.log.info("Add valid email: {}".format(testAddress))
                        self.input_text_check('settings_notificationsMail_text', testAddress, do_login=False)
                        time.sleep(2)
                        self.click_wait_and_check('settings_notificationsSaveMail_button', visible=False)
                        time.sleep(2)
                        if count != 5:
                            self.wait_and_click('settings_notificationsAddMail_button')
                        count += 1
                else:
                    self.log.info("Add valid email: {}".format(testAddress))
                    self.input_text_check('settings_notificationsMail_text', testAddress, do_login=False)
                    time.sleep(2)
                    self.click_wait_and_check('settings_notificationsSaveMail_button', visible=False)
            # Send Test Email
            self.log.info("====== Click Send Test Email ======")
            self.wait_and_click('settings_notificationsTestMail_button')
            time.sleep(2)
            self.log.info("VERIFY: PLEASE CHECK YOUR TEST EMAILBOX FOR TEST MAIL")
            self.log.info("Click OK to finish Alert emails configuration")
            self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
            time.sleep(2)
        except Exception as e:
            self.log.error("ERROR: Unable to configure email alert notification due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='configEmail')
    
    def set_sms_notification(self, vendor='kotsms',username='WDTW', userpwd='WDTWSEV',
                             mobile=886912345678, msg="MSG from WD MyCloud EX4 NAS!!!"):
        """
            Configure SMS notification

            @username: kotsms's account name
            @userpwd: kotsms's account password
            @mobile: Your cellphone filled in kotsms
            @msg: Message you'd like to transmit
        """
        try:
            self.log.info("==> Configure SMS notification")
            self.get_to_page('Settings')
            time.sleep(2)
            self.click_wait_and_check('settings_notifications_link',
                                      "//div[@id='settings_notificationsDisplay_slider']/a", visible=True)
            self.log.info("Slide the notification display bar to All")
            time.sleep(1)
            self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT)  # 50%
            self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT)  # 100%
            time.sleep(2)
            self.log.info("====== Turn on SMS notification ======")
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                          'css=#settings_notificationsSms_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            self.log.info("SMS notification is switched to {}".format(sms_status))
            time.sleep(2)
            self.log.info("====== Click SMS configure ======")
            self.click_wait_and_check('settings_notificationsSms_link',
                                      'smsDiag_title', visible=True)
            if self.is_element_visible('settings_notificationsSmsDel_button', wait_time=10):
                self.log.info("Clean existing SMS configuration")
                self.click_wait_and_check("settings_notificationsSmsDel_button", visible=False)
                time.sleep(2)
            time.sleep(2)
            self.input_text_check('settings_notificationsSmsName_text', vendor, False)
            SMS_URL = "http://202.39.48.216/kotsmsapi-1.php?username={}&password={}&dstaddr={}&smbody={}".format(username, userpwd, mobile, msg)
            self.log.info("SMS URL: {}".format(SMS_URL))
            time.sleep(2)
            self.input_text_check('settings_notificationsSmsUrl_text', SMS_URL, False)
            self.click_wait_and_check('settings_notificationsSmsNext1_button',
                                      'settings_notificationsSmsSave_button', visible=True)
            time.sleep(2)
            self.log.info("====== Configure SMS link settings ======")
            self.click_link_element('id_alias0', linkvalue='Username')
            self.click_link_element('id_alias1', linkvalue='Password')
            self.click_link_element('id_alias2', linkvalue='Phone number')
            self.click_link_element('id_alias3', linkvalue='Message content')
            self.click_wait_and_check('settings_notificationsSmsSave_button', visible=False)
            time.sleep(2)
            self.log.info("Click Test SMS button to send out SMS test message")
            self.wait_and_click('settings_notificationsSmsTest_button')
            time.sleep(2)
            # self.wait_until_element_is_visible('popup_ok_button')
            count = 1
            while count <= 3:
                if self.is_element_visible('popup_ok_button'):
                    break
                else:
                    time.sleep(1)
                    self.log.info("{}: Re-click Test SMS button".format(count))
                    self.click_element('settings_notificationsSmsTest_button')
                    count += 1
            sms_test = self.get_text('css=div#error_dialog_message')
            self.log.info("{}".format(sms_test))
            self.click_wait_and_check('popup_ok_button', visible=False)
            self.click_wait_and_check('settings_notificationsSmsSetSave_button', visible=False)
        except Exception as e:
            self.log.error("ERROR: Unable to configure SMS settings due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='configSMS')
        finally:
            self.close_webUI()

    def disable_sms_settings(self):
        """
            Disable SMS configuration
        """
        try:
            self.log.info("==> Disable SMS notification")
            self.get_to_page('Settings')
            self.click_wait_and_check('settings_notifications_link',
                                      'css=#settings_notificationsSms_switch+span .checkbox_container', visible=True)
            time.sleep(2)
            self.log.info("Turn off SMS notification")
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'OFF':
                self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                          'css=#settings_notificationsSms_switch+span .checkbox_container .checkbox_off',
                                          visible=True)
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            self.log.info("SMS notification is switched to {}".format(sms_status))
            time.sleep(2)
        except Exception as e:
            self.log.error("ERROR: Unable to disable SMS due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='disableSMS')

    def disable_email_settings(self):
        """
            Delete email settings
        """
        try:
            self.log.info("==> Disable Email settings ")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            # self.wait_and_click('settings_notifications_link')
            self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
            time.sleep(2)
            self.log.info("Turn off Alert emails notification")
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            if email_status != 'OFF':
                self.click_wait_and_check('css=#settings_notificationsAlert_switch+span .checkbox_container',
                                          'css=#settings_notificationsAlert_switch+span .checkbox_container .checkbox_off',
                                          visible=True)
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            self.log.info("Alert emails notification is switched to {}".format(email_status))
            time.sleep(2)
        except Exception as e:
            self.log.error("ERROR: Unable to disable email alert due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='disableEmail')

    def delete_sms_settings(self):
        """
            Delete SMS configuration
        """
        try:
            self.log.info("==> Clear SMS notification")
            self.get_to_page('Settings')
            self.click_wait_and_check('settings_notifications_link',
                                      'css=#settings_notificationsSms_switch+span .checkbox_container', visible=True)
            time.sleep(2)
            self.log.info("Turn on SMS notification")
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                          'css=#settings_notificationsSms_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            self.log.info("SMS notification is switched to {}".format(sms_status))
            time.sleep(2)
            self.log.info("====== Click SMS configure ======")
            # self.wait_and_click('css=#settings_notificationsSms_link > span._text')
            self.wait_and_click('settings_notificationsSms_link')
            if self.is_element_visible('settings_notificationsSmsDel_button', wait_time=10):
                self.log.info("Clean existing SMS configuration")
                self.click_wait_and_check("settings_notificationsSmsDel_button", visible=False)
                time.sleep(3)
                self.log.info("Close SMS Setting dialog window")
                self.click_wait_and_check('css=#settings_notificationsSmsCancel1_button', visible=False)
                self.log.info("Succeed to clear SMS notification")
                self.log.info("SMS notification will be turned to OFF due to no configuration...")
            time.sleep(2)
        except Exception as e:
            self.log.error("ERROR: Unable to clear SMS settings due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='clearSMS')
    
    def delete_email_settings(self):
        """
            Delete email settings
        """
        try:
            self.log.info("==> Clear Email settings ")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            # self.wait_and_click('settings_notifications_link')
            self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
            time.sleep(2)
            self.log.info("Turn on Alert emails notification")
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            if email_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsAlert_switch+span .checkbox_container',
                                          'css=#settings_notificationsAlert_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            self.log.info("Alert emails notification is switched to {}".format(email_status))
            time.sleep(2)
            self.log.info("====== Click Alert emails configure ======")
            # self.click_wait_and_check('css=#settings_notificationsAlert_link > span._text',
            #                           'settings_notificationsMailSave_button', visible=True)
            self.wait_until_element_is_visible('settings_notificationsAlert_link', timeout=15)
            self.click_wait_and_check('settings_notificationsAlert_link',
                                      'settings_notificationsMailSave_button', visible=True)
            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link', wait_time=10):
                self.wait_and_click('settings_notificationsDelMail1_link')
                time.sleep(2)
            self.log.info("All email settings are deleted!")
            self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
            time.sleep(2)
        except Exception as e:
            self.log.error("ERROR: Unable to clear email alert settings due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='clearEmail')

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1

alertMsg()