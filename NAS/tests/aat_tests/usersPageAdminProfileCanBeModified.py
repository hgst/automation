"""
title           :usersPageAdminProfileCanBeModified.py
description     :To verify if admin user profile can be edited
author          :yang_ni
date            :2015/06/09
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time

class AdminProfileCanBeModified(TestClient):

    def run(self):

        try:
            self.get_to_page('Users')
            time.sleep(8)
            self.click_button('users_user_admin')
            time.sleep(5)

            passwd = self._get_user_password(user='admin')
            firstname = self.element_find('users_editFirstName_text').get_attribute('value')
            lastname = self.element_find('users_editLastName_text').get_attribute('value')

            self.log.info('***Before start modifying profile, the profile of admin is as following: ***')
            self.log.info('The first name is {0}'.format(firstname))
            self.log.info('The last name is {0}'.format(lastname))
            self.log.info('The passwd is {0}'.format(passwd))

            self.modify_admin_profile()
            self.verify_admin_profile()

        except Exception as e:
            self.log.error('ERROR: Admin profile is not modified successfully: {}'.format(repr(e)))
        else:
            self.log.info('*** Admin profile is modified successfully ***')
        finally:
            self.log.info('*** Reset admin profile to default ***')
            self.modify_user_account(user='admin',passwd='',fullname='')

    def modify_admin_profile(self):
        self.log.info("====== Modify Admin Profile ======")
        try:
            # Edit admin firstname
            self.input_text_check(locator='users_editFirstName_text',text='firstname')
            self.click_element('users_editFirstNameSave_button',timeout=10)
            time.sleep(5)

            # Edit admin lastname
            self.input_text_check(locator='users_editLastName_text',text='lastname')
            self.click_element('users_editLastNameSave_button',timeout=10)
            time.sleep(5)

            # Edit admin password
            self.wait_until_element_is_visible('css=#users_editPW_switch+span .checkbox_container')
            admin_passwd_status = self.get_text('css=#users_editPW_switch+span .checkbox_container')

            if admin_passwd_status == 'OFF':
                self.log.info('Admin password is turned OFF. Enable the admin password')
                self.click_element('css=#users_editPW_switch+span .checkbox_container')
                time.sleep(5)
                self.input_password('users_editPW_password','aaaaa')
                time.sleep(3)
                self.input_password('users_editComfirmPW_password','aaaaa')
                time.sleep(3)
                self.click_element('users_editPWSave_button',timeout=10)
                time.sleep(7)

        except Exception as e:
            self.log.error('[modify_admin_profile] : Unable to modify admin profile, exception: {}'.format(repr(e)))
        else:
            self.log.info("====== [modify_admin_profile] : Successfully change admin profile ======")

    def verify_admin_profile(self):
        self.log.info("====== Verify Admin Profile ======")
        try:
            firstname = self.element_find('users_editFirstName_text').get_attribute('value')
            lastname = self.element_find('users_editLastName_text').get_attribute('value')

            self.log.info('***After modifying profile, the profile of admin is as following: ***')

            if firstname =='firstname':
                self.log.info('The first name is {0}'.format(firstname))
            else:
                self.log.error('Firstname is not changed successfully.')

            if lastname == 'lastname':
                self.log.info('The last name is {0}'.format(lastname))
            else:
                self.log.error('Lastname is not changed successfully.')

            # Check if password is correct modified
            self.click_wait_and_check('users_editPW_link', 'users_oldPW_password')
            self.input_password('users_oldPW_password','aaaaa')
            time.sleep(3)
            self.input_password('users_editPW_password','bbbbb')
            time.sleep(3)
            self.input_password('users_editComfirmPW_password','bbbbb')
            time.sleep(3)
            self.click_element('users_editPWSave_button',timeout=10)
            time.sleep(7)
            self.element_should_not_be_visible('users_oldPW_password')
            time.sleep(5)
            self.close_webUI()
            time.sleep(5)

            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', 'admin', do_login=False)
            self.input_text_check('login_pw_password', 'bbbbb', do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()

        except Exception as e:
            self.log.error('[verify_admin_profile] : Unable to verify admin profile, exception: {}'.format(repr(e)))
        else:
            self.log.info("*** Password is changed successfully ***")
            self.log.info("====== [verify_admin_profile] : Successfully verify admin profile is changed correctly ======")

AdminProfileCanBeModified()