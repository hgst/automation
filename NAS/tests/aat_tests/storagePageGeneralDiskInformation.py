'''
Create on May 6, 2015

@Author: lo_va

Objective: Verify that the following items are presented in the WebUI:
           Enclosure temperature (Diagnostics from the Home page)
           Fan speed (Diagnostics from the Home page)
           Device uptime (Settings -> Utilities -> Device Maintenance)
           Drive status (Diagnostics from the Home page)
           RAID status (Diagnostics from the Home page)
Test ID: 196174
'''
import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E

class storagePageGeneralDiskInformation(TestClient):
    
    def run(self):
        
        raid_HDD_status = self.get_RAID_drives_status()
        drives_mapping_list = self.get_xml_tags(raid_HDD_status, 'location')
        self.get_to_page('Home')
        self.click_wait_and_check(E.HOME_DEVICE_DIAGNOSTICS_ARROW, E.HOME_DEVICE_DIAGNOSTICS_DIAG)
        diagnostics_diag_text = self.get_text(E.HOME_DEVICE_DIAGNOSTICS_DIAG)
        
        # Check diagnostics entries is visible
        times = 0
        while 'RAID Status' not in diagnostics_diag_text and times < 10:
            self.log.info('Drop down menu to check all entries')
            self.drag_and_drop_by_offset('css=div.jspDrag', 0, 30)
            diagnostics_diag_text += self.get_text(E.HOME_DEVICE_DIAGNOSTICS_DIAG)
            times += 1
        
        check_list = ['System Temperature','Fan Speed', 'Drive Status', 'RAID Status']
        for item in drives_mapping_list:
            check_list += ['Drive{0} Temperature'.format(item)]
        
        for item in check_list:
            self.start_test(item)
            if item in diagnostics_diag_text:
                self.pass_test(item, 'Item {0} is in the list'.format(item))
            else:
                self.fail_test(item, 'Item {0} is not in the list'.format(item))
                
        # Check diagnostics entries value is visible
        for item in check_list:
            times = 0
            value_item = self.item_title_to_value(item)
            self.start_test(value_item.split('diagnostics')[1])

            if self.is_element_visible(value_item, 1):
                self.pass_test(value_item.split('diagnostics')[1], 'Value {0} is exist'.format(value_item.split('diagnostics')[1]))
            else:
                while times < 3:
                    self.log.info('Drop up menu to check component')
                    self.drag_and_drop_by_offset('css=div.jspDrag', 0, -30)
                    times += 1
                    if self.is_element_visible(value_item, 1):
                        self.pass_test(value_item.split('diagnostics')[1], 'Value {0} is exist'.format(value_item.split('diagnostics')[1]))
                        break

                while 3 <= times < 6:
                    self.log.info('Drop down menu to check component')
                    self.drag_and_drop_by_offset('css=div.jspDrag', 0, 30)
                    times += 1
                    if self.is_element_visible(value_item, 1):
                        self.pass_test(value_item.split('diagnostics')[1], 'Value {0} is exist'.format(value_item.split('diagnostics')[1]))
                        break
                if times > 10:
                    self.fail_test(value_item.split('diagnostics')[1], 'Value {0} is not exist'.format(value_item.split('diagnostics')[1]))
        self.click_wait_and_check(E.HOME_DEVICE_DIAGNOSTICS_DIAG_CLOSE, visible=False)
                    
        # Check Device uptime
        self.get_to_page('Settings')
        item = 'Device Uptime'
        self.start_test(item)
        self.click_element(E.SETTINGS_UTILITY)
        self.wait_until_element_is_visible('mainbody')
        mainbody = self.get_text('mainbody')
        
        if item in mainbody:
            self.pass_test(item, '{0} is exist'.format(item))
        else:
            self.fail_test(item, '{0} is not exist'.format(item))

        self.start_test(item+'_value')
        if self.is_element_visible('settings_utilitiesUptime_value', 10):
            self.pass_test(item+'_value', '{0} is exist'.format(item+'_value'))
        else:
            self.fail_test(item+'_value', '{0} is not exist'.format(item+'_value'))
            
    def item_title_to_value(self, item):
        if item == 'System Temperature':
            retval = 'home_diagnosticsSysTemper_value'
        elif item == 'Fan Speed':
            retval = 'home_diagnosticsFanTemper_value'
        elif item == 'Drive Status':
            retval = 'home_diagnosticsDriveStatus_value'
        elif item == 'RAID Status':
            retval = 'home_diagnosticsRaidStatus_value'
        elif item == 'Drive1 Temperature':
            retval = 'home_diagnosticsDrive1Temper_value'
        elif item == 'Drive2 Temperature':
            retval = 'home_diagnosticsDrive2Temper_value'
        elif item == 'Drive3 Temperature':
            retval = 'home_diagnosticsDrive3Temper_value'
        elif item == 'Drive4 Temperature':
            retval = 'home_diagnosticsDrive4Temper_value'
        return retval
    
storagePageGeneralDiskInformation()