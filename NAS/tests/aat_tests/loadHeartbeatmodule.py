"""
Create on Dec 29, 2015
@Author: lo_va
Objective:  Verify Heartbeat module is loaded in firmware
            Verify heartbeat
            Refer KAM-247 (M2 feature) for detail requirement and implementations
wiki URL: http://wiki.wdc.com/wiki/Load_Heartbeat_module_in_firmware
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadHeartbeatmodule(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        heartbeat_daemon = ''
        heartbeat_files_list = ''
        try:
            heartbeat_daemon = self.execute('/etc/init.d/heartbeatd status')[1]
            heartbeat_files_list = self.execute('find / -name *heartbeat*')[1]
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['heartbeat is running']
        check_list2 = ['/usr/local/heartbeat/heartbeat', '/etc/init.d/heartbeatd']
        if all(word in heartbeat_daemon for word in check_list1) \
                and all(word in heartbeat_files_list for word in check_list2):
            self.log.info('Load Heartbeat module in firmware test PASSED!!')
        else:
            self.log.error('Load Heartbeat module in firmware test FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit()

LoadHeartbeatmodule(checkDevice=False)
