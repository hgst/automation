'''
Created on Nov 23, 2015

@author: tsui_b

@brief: The test runner is a simple passthrough to execute test cases/suites
        in subprocess as if it is run directly, and it will also:
        1. Re-print the logs from stdout, and save them in runner.log with additional info
        2. Zip all the log files
        3. Send the zip log file to Amazon S3 (not implement yet)
        4. Save device info, test results, and log file link to Elastic Search server.
'''

import subprocess
import sys
import os
import logging
import requests
import xml.etree.ElementTree
import tarfile

from datetime import datetime
from inspect import stack
from elasticsearch import Elasticsearch

class Runner(object):

    def __init__(self):
        # These values are written in hardcode temporarily,
        # we will get them from Jenkins afterwards
        self.runner_log_name = 'runner.log'
        self.elastic_host = '10.136.139.28'
        self.elastic_port = '9200'
        self.test_result_xml = 'output.xml'
        self.device_file = 'test_device.txt'
        self.log_file_list = [self.runner_log_name, 'AAT_debug.log']

        # Try to get silk temp folder too see if test runner is running on silk server
        self.silk_folder = os.environ.get('sctm_test_results_dir')
        if self.silk_folder:
            self.runner_log_name = os.path.join(self.silk_folder, self.runner_log_name)
            self.test_result_xml = os.path.join(self.silk_folder, self.test_result_xml)

        # Elasticsearch.trace has no logging handler by default
        logging.getLogger('elasticsearch.trace').addHandler(logging.NullHandler())
        self.log = self.create_logger('Runner')
        # get local device info
        if os.path.isfile(self.device_file):
            self.local_device_info = eval(open(self.device_file).read())
        else:
            self.local_device_info = {}

        # get device info from silk, jenkins or local config
        self.jenkins_job_name = self.get_device_info('sctm_execdef_name', 'JOB_NAME', 'job_name')
        self.product_name = self.get_device_info('sctm_product', 'DEVICE_NAME', 'platform_description')
        if self.silk_folder:
            # the firmware version is divided into two variables on silk
            self.firmware_version = '{}.{}'.format(self.get_device_info('sctm_version','',''), self.get_device_info('sctm_build','',''))
        else:
            self.firmware_version = self.get_device_info('', 'DEVICE_FIRMWARE', 'firmware')
        self.device_ip = self.get_device_info('', 'DEVICE_IP', 'internalIPAddress')
        self.device_mac = self.get_device_info('', 'DEVICE_MAC', 'macAddress')

    def run(self, cmd):
        start_time = datetime.now().replace(microsecond=0)
        self.log.info('*** Start testing ***')
        self.log.info('Jenkins Job Description: {}'.format(self.jenkins_job_name))
        self.log.info('Test Start Time: {}'.format(start_time))
        self.log.info('Test Case Name: {}'.format(sys.argv[1]))
        self.log.info('Product: {}'.format(self.product_name))
        self.log.info('Firmware Version: {}'.format(self.firmware_version))
        self.log.info('Device IP Address: {}'.format(self.device_ip))
        self.log.info('Device MAC Address: {}'.format(self.device_mac))
        # Execute test case in subprocess
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        # Poll process for new output until finished
        self.log.info('Re-print the outputs from test case:')
        tc_log = self.create_logger(sys.argv[1])
        while True:
            nextline = process.stdout.readline()
            if nextline == '' and process.poll() != None:
                break
            tc_log.info(nextline.strip())
        exitCode = process.returncode
        if exitCode != 0:
            self.log.error('Execute command:[{0}] failed! Error code:[{1}]'.format(cmd, exitCode))
        else:
            self.log.info('*** Test Complete ***')

        end_time = datetime.now().replace(microsecond=0)
        test_duration = end_time - start_time
        self.log.info('Test Duration: {}'.format(test_duration))
        # Tar log files
        self.log.info('Zipping log files')
        # Runner log name example: Glacier_2.11.120_httplogin_2015-12-07_12:00:00
        test_case_name = sys.argv[1]
        log_file_name = '{0}_{1}_{2}_{3}.tar.gz'.format(self.product_name, self.firmware_version,
                                                        test_case_name, start_time.strftime('%Y%m%d%H%M%S'))
        if self.silk_folder:
            tar = tarfile.open(os.path.join(self.silk_folder, log_file_name), 'w:gz')
        else:
            tar = tarfile.open(log_file_name, 'w:gz')

        for filename in self.log_file_list:
            if self.silk_folder:
                filename = os.path.join(self.silk_folder, filename)
            if os.path.isfile(filename):
                tar.add(filename)
            else:
                self.log.warning('Log file: {} does not exist and cannot be zipped!'.format(filename))
        tar.close()

        # Upload log file to S3 and get the links
        self.log.info('Uploading zip log files to S3')
        s3_link = self.export_testcase_output(log_file_name)

        # Get test result from the xml file generated by test case
        self.log.info('Parsing test result from {}'.format(self.test_result_xml))
        error_count = self.get_xml_tags(self.test_result_xml, 'ErrorCount')[0]
        warning_count = self.get_xml_tags(self.test_result_xml, 'WarningCount')[0]
        was_success = self.get_xml_tags(self.test_result_xml, 'WasSuccess')[0]
        if was_success == 'True':
            test_result = 'Passed'
        else:
            test_result = 'Failed'

        # Save test result to elastic search
        es = Elasticsearch([{'host': self.elastic_host, 'port': self.elastic_port}])
        test_info = {
            'Jenkins Job Description': self.jenkins_job_name,
            'Test Case Name': sys.argv[1],
            'Device Name': self.product_name,
            'Firmware Version': self.firmware_version,
            'Device IP Address': self.device_ip,
            'Device MAC Address': self.device_mac,
            'Test Start Time': start_time,
            'Test Duration': '{}'.format(test_duration),
            'Errors': error_count,
            'Warnings': warning_count,
            'Test Result': test_result,
            'Test Logs': s3_link,
        }
        
        result = es.index(index='runner', doc_type='AAT', body=test_info)
        if result['created']:
            self.log.info('Save test result to Elastic Search Successfully')
            self.log.info('Log Index:{}'.format(result['_index']))
            self.log.info('Log Type:{}'.format(result['_type']))
            self.log.info('Log ID:{}'.format(result['_id']))
        else:
            self.log.error('Failed to save result to Elastic Search!')

    def get_device_info(self, silk_key, jenkins_key, local_key):
        # Try to get device info in this order: silk->jenkins->local config, until it is found
        if os.environ.get(silk_key):
            return os.environ[silk_key]
        elif os.environ.get(jenkins_key):
            return os.environ[jenkins_key]
        else:
            return self.local_device_info.get(local_key)

    def create_logger(self, logname=None):
        if logname is None or logname == '':
            # Figure out what script called this function
            if len(stack()) > 2:
                # Called from some function
                caller_frame = stack()[2]
            else:
                # Called directly
                caller_frame = stack()[1]

            callingscript = caller_frame[0].f_globals.get('__file__', None)
            callingscript = os.path.basename(callingscript)
            if '.' in callingscript:
                callingscript = callingscript.split('.')[0]
            logname = callingscript

        # set up logging to file
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                            datefmt='%m-%d %H:%M',
                            filename=self.runner_log_name,
                            filemode='w')
        # set a format which is simpler for console use
        formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        console.setFormatter(formatter)
        logger = logging.getLogger(logname)
        logger.addHandler(console)
        return logger

    def get_xml_tags(self, xml_file, tag):
        tag_content = []
        if not os.path.isfile(xml_file):
            self.log.warning('Cannot find specified xml file: {}!'.format(xml_file))
            tag_content.append('Unknown')
            return tag_content

        tree = xml.etree.ElementTree.parse(xml_file)
        if tag:
            tag = str(tag).strip().strip('"\'')
        for element in tree.iter(tag):
            text = None
            if element.text:
                text = element.text.strip()
            if not text:
                # we have an element that has children
                text = xml.etree.ElementTree.tostring(element)
            tag_content.append(text)
        return tag_content

    def export_testcase_output(self, filename):
        # Dummy function, it only return a fake link for now.
        # Will upload file to Amazon S3 and return the file link afterward.
        result = 's3://wdtestautomation/project/result/{}'.format(filename)
        return result

if __name__ == '__main__':
    try:
        sys.argv[1]
    except IndexError:
        raise ValueError('Please key-in the test case name after runner.py')
    else:
        myClass = Runner()
        myClass.run('python {}.py'.format(sys.argv[1]))