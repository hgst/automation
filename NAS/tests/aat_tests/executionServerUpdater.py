""" This module will scan the execution server for missing or outdated packages needed for the AAT

"""
from global_libraries import CommonTestLibrary

log = CommonTestLibrary.getLogger('AAT.{}'.format(CommonTestLibrary.get_silk_test_name()))

# Update this dictionary with packages and expected versions. Only include the version number, not the whole version
# returned from yum's list option (such as 0.8.1-13.fc20).
yum_expected = {'fuse-afp': '0.8.1-13',
                'afpfs-ng': '0.8.1-13'}

# First, get a list of all packages installed through Yum as a list of lines
log.info('Getting list of installed packages from Yum')
yum_installed = CommonTestLibrary.run_command_locally('yum list installed')[1]

# Go through each package to see if it's installed and up-to-date

for package in yum_expected:
    package_location = yum_installed.find(package)
    expected_version = yum_expected[package]

    if package_location > 0:
        package_info = yum_installed[package_location:].splitlines()[0]
        installed_version = package_info.split()[1]

        if expected_version in installed_version:
            log.info('Package {} is installed and the expected version of {}'.format(package, expected_version))
        else:
            log.info('Updating {} from {} to {}'.format(package, installed_version, expected_version))
            # First, remove the old version
            result, output = CommonTestLibrary.run_command_locally('sudo yum -y remove {}'.format(package))
            if result != 0:
                raise Exception('Failed to remove {} - Output: {}'.format(package, output))

            log.info('Removed old version')

            # Install the new version
            res, output = CommonTestLibrary.run_command_locally('sudo yum -y install {}-{}*'.format(package,
                                                                                                    expected_version))
            if res != 0:
                raise Exception('Failed to install {}-{} Output: {}'.format(package, expected_version, output))

            log.info('Successfully installed {}-{}'.format(package, expected_version))
    else:
        log.info('Package {} is not installed'.format(package))
        # Install the new version
        res, output = CommonTestLibrary.run_command_locally('sudo yum -y install {}-{}*'.format(package,
                                                                                                expected_version))
        if res != 0:
            raise Exception('Failed to install {}-{} Output: {}'.format(package, expected_version, output))

        log.info('Successfully installed {}-{}'.format(package, expected_version))

