"""
Created on July 27th, 2015

@author: tran_jas

## @brief create share through web UI

"""

import time

from sequoiaAPI.src.sequoia import Sequoia
from seleniumAPI.src.seq_ui_map import Elements as ET

share_name = 'testshare'


class CreateShare(Sequoia):

    def run(self):

        try:
            self.seq_accept_eula()
            self.click_element(ET.SHARES_LINK)
            self.click_element(ET.CREATE_SHARE_BUTTON)
            self.input_text(ET.SHARE_NAME_TEXTBOX, share_name)
            self.click_element(ET.SAVE_SHARE_BUTTON)
            time.sleep(3)
            if self.is_element_visible(ET.TEST_SHARE):
                self.log.info('admin can create share through web UI successfully')
            else:
                self.log.error('Fail to create share through web UI')
        except Exception, ex:
            self.log.exception('Failed to complete Admin Can Create Shares Through WebUI test case \n' + str(ex))
        finally:
            self.delete_share(share_name)

CreateShare()
