'''
Create on Aug 2, 2015
@Author: lo_va
Objective: Test Firmware Self Upgrade for BVT project.
'''
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.product_info import Attributes
from global_libraries.testdevice import Fields
from global_libraries.performance import Stopwatch
import time
REPO_LOCATION = 'http://repo.wdc.com'
REPO_FILE_BASE = '{0}/content/repositories/projects/'.format(REPO_LOCATION)


class firmwareSelfUpgradeTest(TestClient):
    
    def run(self):
        self.delete_all_alerts()
        curr_fw = self.get_firmware_version_number()
        product_group = self.uut[self.Fields.group]
        product_info = self.uut[self.Fields.product]
        product_folder_name = curr_fw.split('.')[0] + '_' + curr_fw.split('.')[1]

        # Specific for AstoriaDev Case
        if 'AstoriaDev' in curr_fw:
            AstoriaDev_curr_fw = curr_fw.lstrip('AstoriaDev-')
            product_folder_name = 'AstoriaDev'
            self.log.info('Upload link: {0}{1}-{2}/{3}/{4}/{5}_{6}-AstoriaDev.bin'.format(REPO_FILE_BASE,
                                                                           product_info.replace(' ',''),
                                                                           product_folder_name,
                                                                           product_group,
                                                                           AstoriaDev_curr_fw,
                                                                           product_group,
                                                                           AstoriaDev_curr_fw))
            self.firmware_update('{0}{1}-{2}/{3}/{4}/{5}_{6}-AstoriaDev.bin'.format(REPO_FILE_BASE,
                                                                     product_info.replace(' ',''),
                                                                     product_folder_name,
                                                                     product_group,
                                                                     AstoriaDev_curr_fw,
                                                                     product_group,
                                                                     AstoriaDev_curr_fw))
        else:
            self.log.info('Upload link: {0}{1}-{2}/{3}/{4}/{5}_{6}.bin'.format(REPO_FILE_BASE,
                                                                               product_info.replace(' ',''),
                                                                               product_folder_name,
                                                                               product_group,
                                                                               curr_fw,
                                                                               product_group,
                                                                               curr_fw))
            self.firmware_update('{0}{1}-{2}/{3}/{4}/{5}_{6}.bin'.format(REPO_FILE_BASE,
                                                                         product_info.replace(' ',''),
                                                                         product_folder_name,
                                                                         product_group,
                                                                         curr_fw,
                                                                         product_group,
                                                                         curr_fw))
        self.wait_firmware_updating()
        self.wait_system_back()
        alerts_info = self.get_alerts_cgi()
        alerts_msg = self.get_xml_tags(alerts_info, 'msg')
        if alerts_msg:
            if 'Firmware Update Successful' not in alerts_msg:
                self.log.error('Firmware self upgrade test failed')
            else:
                self.log.info('Current alert list: {}'.format(alerts_msg))
                self.log.info('Firmware self upgrade test PASS!!')
        else:
            self.log.error('Firmware self upgrade test failed!!')
        time.sleep(5)
        
    def wait_firmware_updating(self):
        fwupload_check = 'ps aux | grep -v grep | grep upload_firmware'
        fwpercent_check = 'xmldbc -i -g /runtime/firmware_percentage'
        model = self.get_model_number()

        timer = Stopwatch(timer=80)
        timer.start()
        while True:
            try:
                fwupload_check_result = self.run_on_device(fwupload_check)
                fwpercent_check_result = self.run_on_device(fwpercent_check)
            except Exception as e:
                self.log.warning('Checked flash status with except, try again, Exception: {}'.format(repr(e)))
                continue
            if fwupload_check_result:
                self.log.info('Firmware Flash process: {}'.format(fwupload_check_result))
            if 'upload_firmware' in fwupload_check_result:
                self.log.info('Start to upload firmware')
                break
            if fwpercent_check_result:
                self.log.info('Firmware Flashing already beginning {}%'.format(fwpercent_check_result))
                break
            else:
                if timer.is_timer_reached():
                    raise Exception('Firmware is not uploading!!')
                else:
                    if model != 'GLCR':
                        time.sleep(1)
                    else:
                        time.sleep(0.3)
        #time.sleep(10)
        
        timer = Stopwatch(timer=600)
        timer.start()
        while True:
            try:
                fwpercent_check_result = self.run_on_device(fwpercent_check)
            except Exception as e:
                self.log.warning('Checked flash status with except, try again, Exception: {}'.format(repr(e)))
                continue
            if fwpercent_check_result:
                self.log.info('Firmware Flashing {}%'.format(fwpercent_check_result))
            if str(fwpercent_check_result) == '100':
                self.log.info('Firmware Flashing completed.')
                break
            else:
                if timer.is_timer_reached():
                    raise Exception('Firmware flahsing timeout!')
                else:
                    if model != 'GLCR':
                        time.sleep(2)
                    else:
                        time.sleep(0.6)

    def wait_system_back(self, sleep_time=10, max_boot_time=None):
        time.sleep(sleep_time)
        # Wait for the shutdown to finish
        if not max_boot_time:
            max_boot_time = self.uut[Fields.reboot_time]
        start = time.time()
        self.log.info('Device is powering off for reboot')
        while self.is_ready():
            time.sleep(1)
            if time.time() - start >= max_boot_time:
                self.log.warning('Device failed to power off within {} seconds, using power cycle'.format(max_boot_time))
                self.power_cycle()
                continue

        # Now, wait for the system to come back up.
        self.log.info('Waiting for device to power back up')
        start_time = time.time()
        while not self.is_ready():
            time.sleep(5)
            self.log.debug('Web is not ready')
            if time.time() - start_time > (max_boot_time+180):
                raise Exception('Timed out waiting to boot within {} seconds'.format(max_boot_time+180))
        self.log.info('Web is ready')
        
firmwareSelfUpgradeTest()