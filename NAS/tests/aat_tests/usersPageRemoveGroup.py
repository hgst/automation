""" Users page-Remove Group

    @Author: Lee_e
    
    Objective: check if group can be deleted from UI
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lghtning : Pass (FW: 2.00.205)
                    KC       : Pass (FW: 2.00.205)
              Jenkins Taiwan : Lightning    - PASS (FW: 2.00.205)
                             : Yosemite     - PASS (FW: 2.00.208)
                             : Kings Canyon - PASS (FW: 2.00.208)
                             : Sprite       - PASS (FW: 2.00.215)
                             : Yellowstone  - PASS (FW: 2.00.206)
                             : Glacier      - PASS (FW: 2.00.208)
              Jenkins Irvine : Zion         - PASS (FW: 2.00.215)
                             : Aurora       - PASS (FW: 2.00.205)
                             : Glacier      - PASS (FW: 2.00.200)
                             : Yellowstone  - PASS (FW: 2.00.215)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

groupname = 'a1'

class RemoveGroup(TestClient):
    def run(self):
        self.delete_all_groups()
        self.creat_group(group_name=groupname)
        self.remove_group_from_UI(groupname)
        self.check_group(groupname)
     
    def tc_cleanup(self):
        self.log.info('delete group')
        self.execute("account -d -g '{0}'".format('a1'))    
    
    def creat_group(self,numberOfgroups=1, group_name=None,memberusers=None):
        self.log.info('*** Testing status: Create Group & assing Quota ***')
        self.create_groups(group_name, numberOfgroups, memberusers)

    def remove_group_from_UI(self,group_name=None):
        self.log.info('*** Testing status: Remove Group from UI ***')
        try:
            self.get_to_page('Users')
            self.wait_until_element_is_visible(self.Elements.GROUPS_BUTTON, timeout=5)
            self.click_close_element(self.Elements.GROUPS_BUTTON,'users_group_'+str(group_name))
            if self.is_element_visible('users_group_'+str(group_name)):
                self.click_close_element('users_group_'+str(group_name),'users_removeGroup_button')
                self.click_close_element('users_removeGroup_button', 'popup_apply_button')
                self.click_close_element('popup_apply_button', self.Elements.UPDATING_STRING)
                self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            else:
                self.log.error('No {0} group exist'.format(group_name))  
        except Exception as e:
            self.log.error('Exception error:{0}'.format(repr(e)))         
          
    def check_group(self,group_name):
        self.log.info('*** check group list ***')
        command = 'account -i group'
        group_list = self.run_on_device(command)
        self.log.debug('command : {0}'.format(command))
        self.log.debug('group_list: {0}'.format(group_list))
        if group_name in group_list.split('\n'):
            self.log.error('FAIL: delete group-{0} from UI is fail'.format(group_name))
        else:
            self.log.info('PASS: delete group-{0} from UI is successfully'.format(group_name))
        
    def click_close_element(self, close_element, visible_element, attempts=3, attempt_delay=2):
        for attempts_remaining in range(attempts, -1, -1):
            if attempts_remaining > 0:
                try:
                    self.log.debug('Clicking element: "{0}" to close: "{1}"'.format(close_element, visible_element))
                    self.click_element(close_element)
                    self.wait_until_element_is_visible(visible_element, timeout=60)
                    self.log.debug('Element: "{}" is closed.'.format(visible_element))
                    break
                except:
                    self.log.info('Element: "{0}" did not click. Wait {1} seconds and retry. {2} attempts remaining.' \
                                  .format(close_element, attempt_delay, attempts_remaining))
                    time.sleep(attempt_delay)
            else:
                raise Exception('Failed to popup element: "{}"'.format(visible_element))
        
RemoveGroup()