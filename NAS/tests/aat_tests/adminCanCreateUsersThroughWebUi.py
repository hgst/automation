"""
Created on July 27th, 2015

@author: tran_jas

## @brief create user through web UI

"""

from sequoiaAPI.src.sequoia import Sequoia
from seleniumAPI.src.seq_ui_map import Elements as ET

user_name = 'testuser'


class CreateUser(Sequoia):

    def run(self):

        try:
            self.seq_accept_eula()
            self.click_element(ET.USERS_LINK)
            self.click_element(ET.CREATE_USER_BUTTON)
            self.input_text(ET.FIRSTNAME_TEXTBOX, user_name)
            self.click_element(ET.SAVE_USER_BUTTON)

            if self.is_element_visible(ET.TEST_USER):
                self.log.info('admin can create user through web UI successfully')
            else:
                self.log.error('Fail to create user through web UI')
        except Exception, ex:
            self.log.exception('Failed to complete Admin Can Create Users Through WebUI test case \n' + str(ex))
        finally:
            self.delete_user(user_name)
            self.delete_share(user_name)

CreateUser()
