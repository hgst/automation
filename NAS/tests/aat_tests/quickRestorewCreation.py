"""
Created on July 3, 2015

@author: tran_jas, lo_va

## @brief Restore test device to factory default
#  delete all created users, shares, reset all settings
#
#  Using serial connection because test unit might be in bad state
#   where ssh, network are not working
#  Adding REST call to check progress of RAID configuration
#   as 4-Bay NAS seems to display command prompt even RAID config is in progress
"""

import time
import os
import xml.etree.ElementTree as ET
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries import wd_localfilesystem as wdlfs

kill_process = 'kill_running_process'
load_default = 'load_default 1'
echo_quick = 'echo "quick" > /usr/local/config/system_restore'
remove_restore_file = 'rm /usr/local/config/system_restore'
recreate_2bay = 'diskmgr -v1 -m4 -s 200 -f3 -ksdasdb --kill_running_process --load_module -n'
recreate_4bay = 'diskmgr -v1 -m3 -s 200 -f3 -ksdasdbsdcsdd --kill_running_process --load_module -n'
delete_1 = 'rm -rf /mnt/HD/HD_?2/*'
delete_2 = 'rm -rf /mnt/HD_?4/.systemfile/P2'
delete_3 = 'rm -rf /mnt/HD_?4/.systemfile/.smb*'
delete_4 = 'rm -rf /mnt/HD_?4/.systemfile/ftp.*'
reboot_command = '/usr/sbin/do_reboot'
reset_network_bond0 = 'setNetworkDhcp.sh ifname=bond0'
reset_network_egiga0 = 'setNetworkDhcp.sh ifname=egiga0'
get_vols_total_size = 'cat /var/www/xml/sysinfo.xml | grep total_size'
get_vols_state = 'cat /var/www/xml/sysinfo.xml | grep state'

# prompt
waiting_string = '/ #'
waitingStringGlacier = 'WDMyCloud / #'


class QuickRestore(TestClient):
    def run(self):
        self.skip_cleanup = True
        
        self.open_serial_connection()
        
        # Check volume status is normal or not if yes, re-create RAID
        if not self.checked_volumes_total_size():
            self.log.info('Volumes_total_size is zero, re-create RAID')
            self.do_quick_restore(recreation=True)
        elif not self.checked_raid_status():
            self.log.info('RAID status is not healthy, re-create RAID')
            self.do_quick_restore(recreation=True)
        else:
            self.do_quick_restore()

        # Workaround Solution    
        try:
            count = 0
            self.accept_eula()
            while True and count < 6:
                # Remove mounted shares
                self._unmount_all
                # Delete all mnt directories
                test_base = self.get_test_client_directory()
                wdlfs.delete_file(os.path.join(test_base, 'mnt'))
                wdlfs.delete_path(os.path.join(test_base, 'mnt'), delete_contents=True)
                # Enable SSH
                self.set_ssh(enable=True)
                # Disconnect all logged-in users
                self.disconnect_user_sessions()
                # Confirm that the test is running on the correct product
                self._check_product_name()
                # Reset the admin password
                self.reset_admin_password(ignore_errors=True)
                cleanup = self._cleanup()
                result = self.port_test()
                if result > 0:
                    raise Exception
                count += 1
                time.sleep(3)
        except Exception as ex:
            self.log.warning('Sleep 50 secs because except: {}'.format(ex))
            # Wait system ready.(Some models still need to wait 20 secs to let httpd service ready)
            time.sleep(50)

        # Checked restore succeed or not, if not, try again
        if not self.checked_restore_result():
            self.log.warning('Restore not succeed, try again!')
            if not self.checked_volumes_total_size():
                self.log.info('Volumes_total_size is zero, re-create RAID')
                self.do_quick_restore(recreation=True)
            else:
                self.do_quick_restore()
        else:
            self.log.info('Restore unit successful!')

    def do_quick_restore(self, recreation=False):
        if not recreation:
            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(kill_process)
            self.log.info('kill_process: {0}'.format(kill_process))

            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(load_default)
            self.log.info('load_default: {0}'.format(load_default))

            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(delete_1)
            self.log.info('delete_1: {0}'.format(delete_1))

            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(delete_2)
            self.log.info('delete_2: {0}'.format(delete_2))

            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(delete_3)
            self.log.info('delete_3: {0}'.format(delete_3))

            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(delete_4)
            self.log.info('delete_4: {0}'.format(delete_4))

            self.do_reboot()
        
        if recreation:
            try:
                self._cleanup_reconnect_drives()
                drives_number = self.get_drive_count()
                get_model = self.get_model_number()
            except Exception, e:
                self.log.warning('Get drives_number and model got exception, try to reset network. '
                                 'Exception: {}'.format(e))
                self.serial_wait_for_string(waiting_string, 30)
                self.serial_write(reset_network_bond0)
                self.log.info('reset_network_bond0: {0}'.format(reset_network_bond0))
                
                self.serial_wait_for_string(waiting_string, 30)
                self.serial_write(reset_network_egiga0)
                self.log.info('reset_network_egiga0: {0}'.format(reset_network_egiga0))
                
                try:
                    self._cleanup_reconnect_drives()
                    drives_number = self.get_drive_count()
                    get_model = self.get_model_number()
                except Exception, e:
                    self.log.warning('Get drives_number and model still got exception, try to reboot unit. '
                                     'Exception: {}'.format(e))
                    self.do_reboot()
                    self.open_serial_connection()
                    self._cleanup_reconnect_drives()
                    drives_number = self.get_drive_count()
                    get_model = self.get_model_number() 

            if get_model == 'GLCR':
                self.log.warning('Model is Glacier, cannot re-create volume, do quick restore without re-creation')
                return self.do_quick_restore()
            else:
                self.serial_wait_for_string(waiting_string, 30)
                self.serial_write(echo_quick)
                self.log.info('echo_quick: {0}'.format(echo_quick))
            
                self.serial_wait_for_string(waiting_string, 30)
                self.serial_write(load_default)
                self.log.info('load_default: {0}'.format(load_default))
                
                if drives_number == 2:
                    self.serial_wait_for_string(waiting_string, 30)
                    self.serial_write(recreate_2bay)
                    self.log.info('recreate_2bay: {0}'.format(recreate_2bay))
                    self.serial_wait_for_string(waiting_string, 1800)
                if drives_number == 4:
                    self.serial_wait_for_string(waiting_string, 30)
                    self.serial_write(recreate_4bay)
                    self.log.info('recreate_4bay: {0}'.format(recreate_4bay))
                    self.serial_wait_for_string(waiting_string, 1800)
            
                self.serial_wait_for_string(waiting_string, 30)
                self.serial_write(remove_restore_file)
                self.log.info('remove_restore_file: {0}'.format(remove_restore_file))

                self.do_reboot()

    def open_serial_connection(self):
        # Open serial connection
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))
        
    def checked_volumes_total_size(self):
        # Checked volumes total size
        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write(get_vols_total_size)
        self.log.info('get_vols_total_size: {0}'.format(get_vols_total_size))
        self.serial_wait_for_string('total_size', 20)
        get_size = self.serial_read()
        total_size = filter(str.isdigit, get_size)
        self.log.info('Volumes total size is {0}'.format(total_size))
        if int(total_size) > 0:
            return True
        else:
            return False
        
    def checked_raid_status(self):
        # Checked the RAID status is in degraded or not.
        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write(get_vols_state)
        self.log.info('get_vols_state: {0}'.format(get_vols_state))
        self.serial_wait_for_string('state', 20)
        get_state = self.serial_read()
        self.log.info('RAID status is : {0}'.format(get_state))
        if 'degraded' in str(get_state):
            return False
        else:
            return True

    def checked_restore_result(self):
        # Checked unit has been reset to default
        result = True
        
        # Checked User list
        userlist = self.get_xml_tags(self.get_all_users(), 'username')
        self.log.info('Users list : {0}'.format(userlist))
        userlist.remove('admin')
        if len(userlist) > 0:
            result = False
        
        # Checked Shares
        sharelist = self.get_xml_tags(self.get_all_shares(), 'share_name')
        defaultshare = ['Public', 'SmartWare', 'TimeMachineBackup']
        sharelist = list(set(sharelist) - set(defaultshare))
        self.log.info('Shares list : {0}'.format(sharelist))
        usblist = self.get_xml_tags(self.get_usb_info(), 'share')
        self.log.info('USB port list : {0}'.format(usblist))
        if (len(sharelist) - len(usblist)) > 0:
            result = False
        return result

    def do_reboot(self):
        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write(reboot_command)
        self.log.info('Rebooting: {0}'.format(reboot_command))
            
        # Wait for the shutdown to finish
        max_boot_time = self.uut[Fields.reboot_time]
        start = time.time()
        self.log.info('Device is powering off for reboot')
        while self.is_ready():
            time.sleep(1)
            if time.time() - start >= max_boot_time:
                self.log.warning('Device failed to power off within {} seconds, using power cycle'.format(max_boot_time))
                self.power_cycle()

        # Now, wait for the system to come back up.
        self.log.info('Waiting for device to power back up')
        start_time = time.time()
        while not self.is_ready():
            time.sleep(5)
            self.log.debug('Web is not ready')
            if time.time() - start_time > (max_boot_time+180):
                raise Exception('Timed out waiting to boot within {} seconds'.format(max_boot_time+180))
        self.log.info('Web is ready')
        

# instantiate class without running through the basic checks (in case the unit is stuck)
QuickRestore(checkDevice=False)
