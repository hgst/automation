from testCaseAPI.src.testclient import TestClient
import time

class InternalBackup(TestClient):
    # Create tests as function in this class
    

    def run(self):
        file_size = 10240
        
        try:          
            self.delete_all_shares()  
            self.execute('rm /shares/Public/file*')
            self.execute('rm /shares/SmartWare/file*')
            self.execute('rm -rf /shares/SmartWare/test_1*')
            self.execute('rm /shares/TimeMachineBackup/file*')
            file = self.create_random_file('/shares/Public/', filename='file0.jpg', blocksize=1024, count=file_size)
            file1 = self.create_random_file('/shares/Public/', filename='file1.jpg', blocksize=1024, count=file_size)
            file2 = self.create_random_file('/shares/SmartWare', filename='file3.jpg', blocksize=1024, count=1024)
            file5 = self.create_random_file('/shares/Public/', filename='file5.jpg', blocksize=1024, count=1024)
            self.log.info('Finished creating files')
            self.copy_backup(file_size=file_size)
            self.copy_backup_recurring(file_size=file_size)
            self.cleanup()
            self.incremental_backup(file_size=file_size)
            self.incremental_backup_recurring(file_size=file_size)
            self.modify_backup_settings()
            self.backup_during_standby()                
        except Exception, ex:    
            self.log.error('Failed to backup files internally' + str(ex))             
            
    def copy_backup(self, file_size):
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.log.info(hash1)
        self.get_to_page('Backups')
        #self.wait_until_element_is_clickable('backups_internal_link')
        self.log.info('Going to internal backup page')
        self.click_element('backups_internal_link')
        #self.wait_until_element_is_clickable('backups_InternalBackupsCreate_button')
        self.log.info('Creating backup')
        self.click_element('backups_InternalBackupsCreate_button')
           
        #self.wait_until_element_is_clickable('backups_InternalBackupsSourcepath_button')
        self.log.info('Selecting source path')
        self.click_element('backups_InternalBackupsSourcepath_button')
        self.log.info('Waiting for paths to load')
        time.sleep(30)
        #self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[1]/label/span')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span') 
        self.log.info('Clicking path')      
        time.sleep(10)
        self.click_element('home_treeOk_button')
            
        #self.wait_until_element_is_clickable('backups_InternalBackupsDestpath_button')
        self.log.info('Selecting destination path')
        self.click_element('backups_InternalBackupsDestpath_button')
        time.sleep(15)
        #self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.click_element('home_treeOk_button')
        self.log.info('Trying name with symbols. Expecting fail')   
        self.input_text('backups_InternalBackupsTaskName_text', 'test_1-1!@#!', '')
        self.click_element('backups_InternalBackupsCreate2_button')
        self.click_element('popup_ok_button')
        self.log.info('Entering proper name for backup')
        self.input_text('backups_InternalBackupsTaskName_text', 'test_1-1', '')
        self.click_element('backups_InternalBackupsCreate2_button')
        time.sleep(60)
        self.click_element('backups_internal_link')
        #self.wait_until_element_is_clickable('backups_InternalBackupsGoJob1_button')
        self.execute('cp /shares/SmartWare/test_1-1/Public/file0.jpg /shares/TimeMachineBackup')
        time.sleep(5)
        md = self.md5_checksum('/shares/TimeMachineBackup/', 'file0.jpg')
        hash3 = md
        #self.read_files_from_smb(files='file0.jpg', share_name='TimeMachineBackup')
        self.log.info(hash1)
        self.log.info(hash3)

        if hash1 == hash3:
            self.log.info('First Checksum is successful')
        else:
            self.log.error('Checksum was not successful. File corrupted')
        hash2 = self.md5_checksum('/shares/Public/', 'file1.jpg')
        self.log.info(hash2)
#         self.click_element('backups_internal_link')
#         time.sleep(5)
#         #self.wait_until_element_is_clickable('backups_InternalBackupsGoJob1_button')
#         time.sleep(15)
#         self.click_element('backups_InternalBackupsGoJob1_button')
#         self.click_element('backups_InternalBackupsGoJob1_button')
#         self.click_element('backups_InternalBackupsGoJob1_button')
                   
        time.sleep(60)
        
        self.click_element('backups_internal_link')
        self.execute('cp /shares/SmartWare/test_1-1/Public/file1.jpg /shares/TimeMachineBackup')
        time.sleep(5)
        md = self.md5_checksum('/shares/TimeMachineBackup/', 'file1.jpg')
        hash4 = md
        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4) 
        if hash2 == hash4:
            self.log.info('Second Checksum is successful')
        else:
            self.log.error('Checksum was not successful. File corrupted')       
         
    def copy_backup_recurring(self, file_size):
        
        hash2 = self.md5_checksum('/shares/Public/', 'file1.jpg')
        self.log.info(hash2)
        self.log.info('Starting recurring copy backup - daily')
        self.execute('rm /shares/TimeMachineBackup/file*')
        time.sleep(3)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(3)
        self.click_element('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        time.sleep(3)
        self.click_element('id_sch_hour')
        time.sleep(3)
        self.click_element('link=2:00')
        time.sleep(3)
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='', time_zone_name='US/Pacific')
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.click_element('backups_internal_link')
        time.sleep(120)
        
        self.execute('cp /shares/SmartWare/test_1-1/Public/file1.jpg /shares/TimeMachineBackup')
        self.click_element('backups_internal_link')
        md = self.md5_checksum('/shares/TimeMachineBackup/', 'file1.jpg')
        self.click_element('backups_internal_link')
        hash4 = md
        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4)
        if hash2 == hash4:
            self.log.info('Recurring Daily Backup Checksum is successful')
        else:  
            self.log.error('Checksum was not successful. File corrupted') 
        
        self.execute('rm -rf /shares/SmartWare/test_1-1')
        self.execute('rm /shares/TimeMachineBackup/file*')    
        #self.write_files_to_smb(files=file[1], share_name='Public')
        self.log.info('Starting recurring copy backup - weekly')
        self.execute('rm /shares/TimeMachineBackup/file*')
        time.sleep(3)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(3)
        #self.click_element('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.double_click_element('backups_InternalBackupsWeekly_button')
        time.sleep(3)
        self.double_click_element('backups_InternalBackupsWeekly_button')
        time.sleep(3)
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.execute('date -s 2015.01.05-01:57:00')
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.execute('cp /shares/SmartWare/test_1-1/Public/file1.jpg /shares/TimeMachineBackup')
        self.click_element('backups_internal_link')
        md = self.md5_checksum('/shares/TimeMachineBackup/', 'file1.jpg')
        self.click_element('backups_internal_link')
        #md = self.execute('busybox md5sum /shares/SmartWare/test_1-1/Public/file1.jpg')
        hash4 = md
        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4)
        if hash2 == hash4:
            self.log.info('Recurring Weekly Backup Checksum is successful')
        else:  
            self.log.error('Checksum was not successful. File corrupted')     
          
        self.execute('rm -rf /shares/SmartWare/test_1-1')
        self.execute('rm /shares/TimeMachineBackup/file*')    
        self.log.info('Starting recurring copy backup - monthly')
        self.execute('date -s 2015.01.01-02:30:00')
        self.execute('rm /shares/TimeMachineBackup/file*')
        time.sleep(3)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
            
        visible = self.is_element_visible('id_sch_day')
        self.log.info(visible)
        while visible == False:
            self.click_element('backups_InternalBackupsMonthly _button')
            time.sleep(5)
            visible = self.is_element_visible('id_sch_day')
            self.log.info(visible)
            time.sleep(5)
#         self.click_element('backups_internalBackupsMonthly _button')
#         time.sleep(5)
#         self.click_element('backups_internalBackupsMonthly _button')
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.execute('date -s 2015.01.01-01:57:00')
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.execute('cp /shares/SmartWare/test_1-1/Public/file1.jpg /shares/TimeMachineBackup')
        self.click_element('backups_internal_link')
        hash4 = self.md5_checksum('/shares/TimeMachineBackup/', 'file1.jpg')

        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4)
        if hash2 == hash4:
            self.log.info('Recurring Monthly Backup Checksum is successful')
        else:  
            self.log.info('Checksum was not successful. File corrupted')
            
        self.execute('rm -rf /shares/SmartWare/test_1-1')
        self.execute('rm /shares/TimeMachineBackup/file*')
        #self.click_element('backups_internal_link') 
        time.sleep(10)
        visible = self.is_element_visible('popup_apply_button')
        self.log.info(visible)
        while visible == False:
            self.click_element('backups_InternalBackupsDelJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            self.log.info(visible)
            time.sleep(5)
        self.click_element('popup_apply_button')
        time.sleep(5)        
        
    def incremental_backup(self, file_size):
        self.log.info('Starting incremental backup')
        #self.click_element('backups_InternalBackupsDelJob1_button')
        #self.click_element('popup_apply_button')
        #self.execute('rm /shares/Public/file*')
        self.execute('rm -rf /shares/SmartWare/test_1-1')
        self.execute('rm -rf /shares/SmartWare/test_1-2')
        self.execute('rm /shares/TimeMachineBackup/file*')
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.log.info(hash1)
        self.get_to_page('Backups')
        self.wait_until_element_is_clickable('backups_internal_link')
        self.log.info('Getting to internal backup page')
        time.sleep(3)
        self.click_element('backups_internal_link')
        self.log.info('Clicking internal backup create button')
        time.sleep(3)
        self.click_element('backups_InternalBackupsCreate_button')          
        #self.wait_until_element_is_clickable('backups_InternalBackupsSourcepath_button')
        self.log.info('Entering source path')
        
        self.click_element('backups_InternalBackupsSourcepath_button')
        time.sleep(30)
        #self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[1]/label/span')
        self.log.info('Choosing public folder')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span')           
        self.double_click_element('home_treeOk_button')
        #self.wait_until_element_is_clickable('backups_InternalBackupsDestpath_button')
        self.log.info('Entering destination path')
        self.click_element('backups_InternalBackupsDestpath_button')
        time.sleep(30)
        #self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.log.info('choosing SmartWare folder')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.double_click_element('home_treeOk_button')
        self.log.info('Entering backup name')
        self.input_text('backups_InternalBackupsTaskName_text', 'test_1-2', '')
        self.log.info('Changing type to incremental')
        self.click_element('id=f_type')
        self.click_element('link=Incremental')
        self.log.info('Changing date and time')
        self.execute('date -s 2015.01.01-01:59:30')
        self.log.info('Finish creating backup')
        self.click_element('backups_InternalBackupsCreate2_button')
        time.sleep(60)
        self.click_element('backups_internal_link')
        list = self.execute('ls /shares/SmartWare/test_1-2/')
        location = list[1]
        hash3 = self.md5_checksum('/shares/SmartWare/test_1-2/'+location+'/Public/', 'file0.jpg')
        self.click_element('backups_internal_link')
        #self.read_files_from_smb(files='file0.jpg', share_name='TimeMachineBackup')
        self.log.info(hash1)
        self.log.info(hash3)

        if hash1 == hash3:
            self.log.info('First Checksum is successful')
        else:
            self.log.error('Checksum was not successful. File corrupted')
        self.execute('rm -rf /shares/SmartWare/test*')
        self.click_element('backups_internal_link')
        hash2 = self.md5_checksum('/shares/Public/', 'file1.jpg')
        self.log.info(hash2)
        time.sleep(5)
        self.log.info('Modifying backup job')
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(5)
        self.log.info('Clicking recurring backup switch')
        self.click_element('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        time.sleep(3)
        self.log.info('Setting date and time')
        self.click_element('id_sch_hour')
        time.sleep(3)
        self.click_element('link=2:00')
        time.sleep(3)
        self.log.info('Saving Backup')
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='', time_zone_name='US/Pacific')
        self.execute('date -s 2015.01.01-01:57:00')
        time.sleep(120)
        self.click_element('backups_internal_link')
        time.sleep(120)
        list = self.execute('ls /shares/SmartWare/test_1-2/')
        location = list[1]
        self.click_element('backups_internal_link')
        hash4 = self.md5_checksum('/shares/SmartWare/test_1-2/'+location+'/Public/', 'file1.jpg')
        self.click_element('backups_internal_link')
        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.log.info(hash2)
        self.log.info(hash4)
        if hash2 == hash4:
            self.log.info('Incremental Backup Checksum is successful')
        else:  
            self.log.error('Checksum was not successful. File corrupted')  
          
    def incremental_backup_recurring(self, file_size):
        #self.get_to_page('Backups')
        self.log.info('Starting incremental recurring backup - daily')
        hash2 = self.md5_checksum('/shares/Public/', 'file1.jpg')
        self.log.info(hash2)
        self.execute('rm /shares/TimeMachineBackup/file*')
        
        self.wait_until_element_is_clickable('backups_internal_link')
        self.click_element('backups_internal_link')
        time.sleep(3)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(3)
        self.click_element('id_sch_hour')
        time.sleep(3)
        self.click_element('link=2:00')
        time.sleep(3)
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='', time_zone_name='US/Pacific')
        self.execute('date -s 2015.01.01-01:57:00')
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.click_element('backups_internal_link')
        time.sleep(120)
        list = self.execute('ls /shares/SmartWare/test_1-2/')
        location = list[1]
        hash4 = self.md5_checksum('/shares/SmartWare/test_1-2/'+location+'/Public/', 'file1.jpg')
        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4)
        if hash2 == hash4:
            self.log.info('Recurring Daily Backup Checksum is successful')
        else:  
            self.log.error('Checksum was not successful. File corrupted')  

        self.execute('rm -rf /shares/SmartWare/test_1-2')
        self.execute('rm /shares/TimeMachineBackup/file*')    
        #self.write_files_to_smb(files=file[1], share_name='Public')
        self.log.info('Starting incremental recurring copy backup - weekly')
        self.execute('rm /shares/TimeMachineBackup/file*')
        time.sleep(3)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(5)
        #self.click_element('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.double_click_element('backups_InternalBackupsWeekly_button')
        time.sleep(3)
        self.double_click_element('backups_InternalBackupsWeekly_button')
        time.sleep(3)
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.execute('date -s 2015.01.05-01:57:00')
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.click_element('backups_internal_link')
        time.sleep(120)
        list = self.execute('ls /shares/SmartWare/test_1-2/')
        location = list[1]
        #self.execute('cp /shares/SmartWare/test_1-2/'+location+'/Public/file1.jpg /shares/TimeMachineBackup')
        hash4 = self.md5_checksum('/shares/SmartWare/test_1-2/'+location+'/Public/', 'file1.jpg')

        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4) 
        if hash2 == hash4:
            self.log.info('Recurring Weekly Backup Checksum is successful')
        else:  
            self.log.error('Checksum was not successful. File corrupted')     
          
        self.execute('rm -rf /shares/SmartWare/test_1-2')
        self.execute('rm /shares/TimeMachineBackup/file*')    
        self.log.info('Starting incremental recurring copy backup - monthly')
        self.execute('date -s 2015.01.01-02:30:00')
        time.sleep(3)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(10)
        visible = self.is_element_visible('id_sch_day')
        self.log.info(visible)
        while visible == False:
            self.click_element('backups_InternalBackupsMonthly _button')
            time.sleep(5)
            visible = self.is_element_visible('id_sch_day')
            self.log.info(visible)
            time.sleep(5)
           
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.execute('date -s 2015.01.01-01:57:00')
        self.click_element('backups_internal_link')
        time.sleep(120)
        self.click_element('backups_internal_link')
        time.sleep(120)
        list = self.execute('ls /shares/SmartWare/test_1-2/')
        location = list[1]
        #self.execute('cp /shares/SmartWare/test_1-2/'+location+'/Public/file1.jpg /shares/TimeMachineBackup')
        hash4 = self.md5_checksum('/shares/SmartWare/test_1-2/'+location+'/Public/', 'file1.jpg')

        #self.read_files_from_smb(files='file1.jpg', share_name='TimeMachineBackup')
        self.click_element('backups_internal_link')
        self.log.info(hash2)
        self.log.info(hash4) 
        if hash2 == hash4:
            self.log.info('Recurring Monthly Backup Checksum is successful')
        else:  
            self.log.info('Checksum was not successful. File corrupted')
        self.execute('rm /shares/SmartWare/file*') 
        self.execute('rm -rf /shares/SmartWare/test*') 
        self.execute('rm /shares/TimeMachineBackup/file1.jpg') 
   
    def cleanup(self):        
        '''#self.execute('rm /shares/Public/file*')
        self.execute('rm -rf /shares/SmartWare/test_1-1')
        self.execute('rm /shares/TimeMachineBackup/file*')
        self.click_element('backups_InternalBackupsDelJob1_button')
        self.click_element('popup_apply_button')
        self.set_date_time_configuration(datetime='1421057768', ntpservice=True, ntpsrv0='time.windows.com', ntpsrv1='pool.ntp.org', ntpsrv_user='', time_zone_name='US/Pacific')
        '''  
        
    def modify_backup_settings(self):
        self.log.info('Starting modify backup settings')
        self.get_to_page('Backups')
        self.execute('rm /shares/SmartWare/file3.jpg')
        self.execute('rm /shares/TimeMachineBackup/file3.jpg')
        self.create_random_file('/shares/SmartWare/', filename='file4.jpg', blocksize=1024, count=1024)
        hash5 = self.md5_checksum('/shares/SmartWare/', 'file4.jpg')
        
        #self.wait_until_element_is_clickable('backups_internal_link')
        self.click_element('backups_internal_link')
        #self.double_click_element('backups_InternalBackupsModifyJob1_button')
        time.sleep(10)
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(3)
#         visible = self.is_element_visible('backups_InternalBackupsSourcepath_button')
#         if visible == False:
#             self.click_element('backups_InternalBackupsModifyJob1_button')
        #self.wait_until_element_is_clickable('backups_InternalBackupsSourcepath_button')
        time.sleep(3)
        self.click_element('backups_InternalBackupsSourcepath_button')
        time.sleep(15)
        self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/label/span')           
        self.double_click_element('home_treeOk_button')
        self.wait_until_element_is_clickable('backups_InternalBackupsDestpath_button')
        self.click_element('backups_InternalBackupsDestpath_button')
        time.sleep(15)
        self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[3]/label/span')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[3]/label/span')
        self.double_click_element('home_treeOk_button')
        self.input_text('backups_InternalBackupsTaskName_text', 'test_1-3', '')
        self.click_element('id=f_type')
        self.click_element('link=Copy')
        self.click_element('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.click_element('backups_internal_link')
        self.click_element('backups_InternalBackupsGoJob1_button')
        #time.sleep(10)
        #self.click_element('backups_InternalBackupsGoJob1_button')
        #self.execute('cp /shares/Public/test_1-3/SmartWare/file3.jpg /shares/TimeMachineBackup')
        time.sleep(5)
        hash6 = self.md5_checksum('/shares/TimeMachineBackup/test_1-3/SmartWare/', 'file4.jpg')
        self.log.info(hash5)
        self.log.info(hash6) 
        if hash5 == hash6:
            self.log.info('Modify Backup Checksum is successful')            
        
        self.execute('rm /shares/TimeMachineBackup/test*')
        #self.execute('rm -rf /shares/Public/test*')
            
    def backup_during_standby(self):
        self.log.info('Starting backup during standby')
        hash7 = self.md5_checksum('/shares/SmartWare/', 'file4.jpg')
        #self.get_to_page('Backups')
        self.wait_until_element_is_clickable('backups_internal_link')
        self.click_element('backups_internal_link')
        visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.log.info(visible)
        while visible == False:
            self.double_click_element('backups_InternalBackupsModifyJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
            self.log.info(visible)
            time.sleep(5)
        time.sleep(3)
        self.click_element('css = #backups_InternalBackupsRecurring_switch+span .checkbox_container')
        self.click_element('id_sch_hour')
        self.click_element('link=2:00')
        self.click_element('backups_InternalBackupsSave1_button')
        time.sleep(5)
        self.execute('rm -rf /shares/TimeMachineBackup/test*')
        self.execute('date -s 2015.01.01-01:44:00')
        self.log.info('Waiting for standby')
        time.sleep(600)
        self.log.info('Elapsed 10 minutes')
        time.sleep(480)
        self.log.info('15 minutes passed for wait time. Another 2 minutes for the backup')
        hash8 = self.md5_checksum('/shares/TimeMachineBackup/test_1-3/SmartWare/', 'file4.jpg')
        self.log.info(hash7)
        self.log.info(hash8) 
        if hash7 == hash8:
            self.log.info('Standby Backup Checksum is successful')
        else:  
            self.log.error('Checksum was not successful. File corrupted')
            
        self.get_to_page('Backups')
        self.wait_until_element_is_clickable('backups_internal_link')
        self.click_element('backups_internal_link') 
        time.sleep(5)
        visible = self.is_element_visible('popup_apply_button')
        self.log.info(visible)
        while visible == False:
            self.click_element('backups_InternalBackupsDelJob1_button')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            self.log.info(visible)
        self.click_element('popup_apply_button')
        self.execute('rm -rf /shares/TimeMachineBackup/test*')
   
       
            
InternalBackup()       
