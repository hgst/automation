"""
Created on July 27th, 2015

@author: tran_jas

## @brief create share through web UI

"""

from sequoiaAPI.src.sequoia import Sequoia
from seleniumAPI.src.seq_ui_map import Elements as ET


class CloudAccess(Sequoia):

    def run(self):

        try:
            self.seq_accept_eula()
            self.click_element(ET.CLOUD_ACCESS_LINK)
            self.click_element(ET.REMOTE_ACCESS_ADMIN)

            if self.is_element_visible(ET.CREATE_DEVICE_USER_ACCESS_BUTTON):
                self.log.info('admin can manage cloud access through web UI successfully')
            else:
                self.log.error('Fail to manage cloud access through web UI')
        except Exception, ex:
            self.log.exception('Failed to complete Admin Can Manage Cloud Access Through WebUI test case \n' + str(ex))

CloudAccess()
