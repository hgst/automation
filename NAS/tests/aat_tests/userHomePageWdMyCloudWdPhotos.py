""" User Home Page - WD My Cloud and WD Photos (MA-109)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=31793&execView=execDetails&tdetab=3&view=details&pltab=steps&pId=50&nTP=117556&etab=8
        1) Open WebUI and login as a user
        2) Click on the WD My Cloud and WD Photos link
        4) Verify that the user is redirected to the wdc mobile apps page
           User is taken to products feature page
           http://www.wdc.com/global/products/features/?id=mobileapps&language=en

    Automation: Full

    Not supported product:
        N/A

    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon

    ITR: 106330 , FW: 2.10.131
    Failed: Yellowstone, Yosemite, Sprite, Aurora, Glacier
    Pass: Lightning, KingsCanyon, Zion

    (SKY-4878) Client App section for non-admin user removed.
    - FW 2.10.266 will remove this link, so no more support after 2.10.266.

    Test Status:
        Local Yosemite: N/A (FW: 2.00.201)
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
from global_libraries.CommonTestLibrary import get_silk_results_dir
import time
import requests
import os


userName = 'test2'
userPass = 'test2'

picturecounts = 0

class userHomePageWdMyCloudWdPhotos(TestClient):
    def run(self):
        FW_NUM = self.get_firmware_version_number()
        if FW_NUM.rsplit('.', 1)[0] != '2.00':
            self.log.info("=== This feature is only available in 2.00 and removed after 2.10.xxx ===")
            self.log.info("=== DUT FW({}) is not 2.00.xxx, skip the test ===".format(FW_NUM))
            return
        if self.get_model_number() in ('GLCR',):
            self.log.info("=== Glacier de-feature does not support user login, skip the test ===")
            return
        self.log.info('######################## User Home Page - WD My Cloud and WD Photos (MA-109) TEST START ##############################')
        try:
            self.delete_all_groups()
            self.delete_all_shares()
            self.delete_all_users()
            self.create_user(username=userName, password=userPass)
            self.webUI_user_login(username=userName, userpwd=userPass)
            self.verifyMobileLink()
        except Exception as e:
            self.log.error("FAILED: User Home Page - WD My Cloud and WD Photos (MA-109) Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: User Home Page - WD My Cloud and WD Photos (MA-109) Test PASS!!!")
        finally:
            self.log.info("========== (MA-109) Clean Up ==========")
            self.delete_user(user=userName)
            self.log.info('######################## User Home Page - WD My Cloud and WD Photos (MA-109) TEST END ##############################')

    def verifyMobileLink(self):
        try:
            self.wait_until_element_is_visible('css=#wd_2go_photos_text_url > span._text', timeout=15)
            while len(self._sel.driver._current_browser().window_handles) == 1:
                self.click_element('css=#wd_2go_photos_text_url > span._text', timeout=5)
            time.sleep(5) # Waiting for windows loading
            nas_window = self._sel.driver._current_browser().current_window_handle
            all_window = self._sel.driver._current_browser().window_handles
            all_window.remove(nas_window)
            pop_window = all_window[0]
            self.log.info("NAS Handle: {}".format(nas_window))
            self.log.info("Pop Handle: {}".format(pop_window))
            self._sel.driver._current_browser().switch_to_window(pop_window)
            # Click WD My Cloud and WD Photos link
            # self._sel.driver.select_window('title=Product Features')
            # Verify if the link is correct
            title_text = self._sel.driver.get_title().encode('ascii')
            self.log.info("Title: {}".format(title_text))
        except Exception as e:
            self.log.error("ERROR: Failed to verify mobile link due to {}".format(repr(e)))
            raise
        else:
            if title_text == 'Product Features':
                self.log.info('PASS: Product Features(WD Mobile APP) page is launched successfully.')
            else:
                self.takeScreenShot(prefix="ProductFeatures")
                self.log.error('FAILED: Error to open Product Features(WD Mobile APP) page.')

    def webUI_user_login(self, username, userpwd):
        """
        :param username: user name you'd like to login
        :param userpwd: user password for user name account
        """
        self.access_webUI(do_login=False)
        self.input_text_check('login_userName_text', username, do_login=False)
        self.input_text_check('login_pw_password', userpwd, do_login=False)
        self.click_wait_and_check('login_login_button', visible=False)
        self._sel.check_for_popups()
        self.log.info("PASS: Succeed to login as [{}] user".format(username))

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
            Note:
            from global_libraries.CommonTestLibrary import get_silk_results_dir
        """
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

userHomePageWdMyCloudWdPhotos()
