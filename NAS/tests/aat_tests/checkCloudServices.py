"""
Create on Feb 15, 2016
@Author: yang_ni
Objective:  Check cloud services and IP address by Rest command by verifying "cloudConnected" and "localIpAddress" fields
wiki URL:   http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=33214&execView=execDetails&view=details&pltab=steps&pId=86&nTP=317547
"""

import requests
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields

class CheckConnectionStatus(TestClient):

    def run(self):

        try:
            self.yocto_init()
            self.yocto_check()

            wddevice1 = self.execute('cat /Data/devmgr/db/wddevice.ini | grep deviceid')[1]
            wddevice2 = self.execute('cat /Data/devmgr/db/wddevice.ini | grep accesstoken')[1]
            deviceid = wddevice1.split('deviceid = ')[1]
            accesstoken = wddevice2.split('accesstoken = ')[1]
            url = 'http://qa1-device.remotewd1.com/device/v1/device/{0}?&access_token={1}'.format(deviceid, accesstoken)
            status_code,res = self.get(url)
            result = res.get('data')

            cloud_connected_value = ''
            network_value = ''
            if status_code == 200:
                for k,v in result.items():
                    if k == 'cloudConnected':
                        cloud_connected_value = str(v)
                    if k == 'network':
                        network_value = v

                ip_address = str(network_value['localIpAddress'])

                if cloud_connected_value == 'True':
                    self.log.info('*** PASS: cloudConnected: {}'.format(cloud_connected_value))
                else:
                    self.log.error('*** FAIL: cloudConnected: {}'.format(cloud_connected_value))
                
                if ip_address == self.uut[Fields.internal_ip_address]:
                    self.log.info('*** PASS: NAS IP Address: {}'.format(ip_address))
                else:
                    self.log.error('*** FAIL: NAS IP Address: {}'.format(ip_address))

            else:
                self.log.error('*** ERROR: Response code is not 200, response code:{0} ***'.format(status_code))

        except Exception as ex:
            self.log.error('*** ERROR: Check cloud services is not successful: {} ***'.format(repr(ex)))
        else:
            self.log.info('*** Check cloud services is performed successfully ***')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit()

    def get(self, url):
        response = requests.get(url)
        return response.status_code,response.json()

CheckConnectionStatus(checkDevice=False)
