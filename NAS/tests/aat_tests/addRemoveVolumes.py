"""
Created on Sept 25, 2014

"""

from testCaseAPI.src.testclient import TestClient
    
volume_name = 'Volume_'
volumes = {'Volume_1', 'Volume_2', 'Volume_3', 'Volume_4'}
public_share_name = ['PublicShare_1', 'PublicShare_2', 'PublicShare_3','PublicShare_4']
private_share_name_A = ['PrivateShareA_1', 'PrivateShareA_2', 'PrivateShareA_3','PrivateShareA_4']
private_share_name_B = ['PrivateShareB_1', 'PrivateShareB_2', 'PrivateShareB_3','PrivateShareB_4']
share_desc = 'share volume'

class MyClass(TestClient):
    
    def run(self):      
        try:
           self.setup()
           #self._create_shares(public_share_name, public=True)
           #self._create_shares(private_share_name_A, public=False)
           #self._create_shares(private_share_name_B, public=False)
           #self.validation()
        except:
            self.log.exception('An exception has occurred in addRemoveVolumes test')

    def setup(self):
        # Clean up shares by removing shares
        self.delete_all_shares()
        
        # If already JBOD (4 Volumes) then skip configure_jbod
        xml_content = self.get_volumes()
        print xml_content[1].content
        print len(self.get_xml_tags(xml_content[1].content, 'base_path'))
        #if len(self.get_xml_tags(xml_content[1].content, 'base_path')) != 4:
            #self.configure_jbod()
           
    def _create_shares(self, share_name = [], public=True):
        try:            
            # Create shares named PublicShare_1-4/PrivateShareA_1-4/PrivateShareB_1-4 on Volumes_1-4 respectively           
            for i in range (1,5):
                self.log.info('Creating share: {0}'.format(share_name[i-1]))
                self.create_shares(share_name=share_name[i-1], description=share_desc, public_access=public)      
        except Exception,ex:
            self.log.exception('Failed to create share\n' + str(ex))
    
    def public_share_accessible_no_authentication(self):
        try:            
            
            for share in public_share_name:
                xml_content = self.read_files_from_smb(share_name=share)
                print xml_content
        except Exception,ex:
            self.log.exception('Failed to access public shares\n' + str(ex))
                   
MyClass()
