'''
Create on May 18, 2015
@Author: lo_va
Objective: Test SMB protocol Read/Write Access for BVT project 
'''
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
import time
import random
generate_size = 102400
file_prefix = 'rwfile{}'.format(random.randint(1,100))

class smbReadWriteAccess(TestClient):

    def run(self):
        
        try:
            sharename = 'Public'
            self.log.info('Start to test SMB protocol Read/Write Access.')
            generated_file = self.generate_test_file(generate_size, prefix=file_prefix)
            time.sleep(9)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            # Wait for transfer finished
            time.sleep(3)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(files=file_prefix+'0.jpg', share_name=sharename, delete_after_copy=True)
            time.sleep(3)
            self.log.info('SMB protocol Read/Write Access test PASS!!')
        except Exception as ex:
            self.log.exception('SMB Read/Write Access FAILED!! Exception: {}'.format(ex))

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('***** Hash check SUCCEEDED!! *****')
        else:
            self.log.error('***** Hash check FAILED!! *****')
    
smbReadWriteAccess()