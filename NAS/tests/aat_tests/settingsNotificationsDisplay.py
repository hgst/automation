""" Settings Page - Notifications - Display(MA-95)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?pId=50&nTP=117540&view=details&pltab=steps)
                1) Open webUI to login as system admin
                2) Go to Settings Page
                3) Select Notifications -> Notification Display
                4) Set notification level
                   a) Critical Only
                   b) Critical and Warning
                   c) All
                5) Verify display notifications are saved
    
    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.133)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.webdriver.common.keys import Keys
import time
import os

class notifyDisplay(TestClient):
    
    def run(self):
        self.log.info('######################## Settings Page - Notifications - Display TEST START ##############################')
        try:
            self.get_to_page('Settings')
            self.log.info("==> Go to Notification Level")
            self.wait_and_click('settings_notifications_link')
            self.config_sev_notification(severity='c')
            self.config_sev_notification(severity='w')
            self.config_sev_notification(severity='a')
        except Exception as e:
            self.log.error("FAILED: Settings Page - Notifications - Display Test Failed!!, exception: {}".format(repr(e)))
        else:
            self.log.info("PASS: Settings Page - Notifications - Display Test PASS!!")
            
        self.log.info('######################## Settings Page - Notifications - Display TEST END ##############################')

    def config_sev_notification(self, severity='c'):
        """
            Configure notification Level
            
            @severity: 'c': critical only
                       'w': critical and warning
                       'a': All
        """        
        self.wait_until_element_is_visible("//div[@id='settings_notificationsDisplay_slider']/a")
        # Get Current Level value
        element = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
        percent_text = element.get_attribute('style')
        percent = int(percent_text.split()[1][:-2])
        self.log.info("Current Notification Level: {}".format(percent))
        time.sleep(2)
        
        if severity == 'c':
            self.log.info("Setting Notification Level to Critical Only(0%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 50%
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 0%
            else:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 0%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 0:
                self.log.info("PASS: Notification Display has been set to Critical ONLY successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set Critical ONLY...")
            return
        elif severity == 'w':
            self.log.info("Setting Notification Level to Critical and Warning(50%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 50%
            elif percent < 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 50:
                self.log.info("PASS: Notification Display has been set to Critical and Warning successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set Critical and Warning...")
            return
        elif severity == 'a':
            self.log.info("Setting Notification Level to All(100%)...")
            if percent <= 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 100:
                self.log.info("PASS: Notification Display has been set to ALL successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set ALL...")
            return

notifyDisplay()
        
        
            