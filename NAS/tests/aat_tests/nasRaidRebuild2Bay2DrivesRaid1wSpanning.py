'''
Create on Mar 17, 2015

@Author: Vasquez_c

Objective: To validate RAID1 with Spanning configuration
            rebuild scenarios for 2 bay Consumer NAS.
wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Rebuild
'''

import time
import sys
import re
import os
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.tc_shares import Share
from testCaseAPI.src.testclient import Elements as E

clean_up_disk1 = 'dd if=/dev/zero of=/dev/sda count=10000'
sharename = 'RebuildR1'
block_size = 1024
generated_file_size = 1024000
timeout = 240

class nasRaidRebuild2DrivesRAID1wSpanning(TestClient):

    def run(self):
        
        try: 
                 
            # Create RAID1
            self.log.info('Start to create RAID1 with Spanning. This may take a while, please stand by ...')
            self.configure_raid(raidtype='RAID1', spanning=True, number_of_drives=2)
            self.verifyMountVolume(volumeid=1)
            self.delete_all_shares()
            self.disable_auto_rebuild()
                
            # Create share and validate access via smb protocol
            self.log.info('Create %s share and validate access via smb protocol.' % sharename)
            self.create_shares(share_name=sharename, force_webui=True)
                 
            self.read_write_access_check(sharename=sharename, prot='SAMBA')
              
        except Exception, ex:
            self.log.exception('Configure RAID1 with spanning was not successful. Please check RAID settings')
            self.log.info('Failed to Configure RAID1 with spanning\n' + str(ex))    
               
        self.read_write_access_check(sharename=sharename, prot='NFS')
 
        # Clean up and Plug out the disk 1
        self.log.info('Plug out Disk 1 !!')
        self.run_on_device(clean_up_disk1)
        self.remove_drives(1)
        self.wait_system_busy()

        # Check First RAID1 status was in degraded and validate access
        self.log.info('Check RAID1 status was in degraded and validate access.')
        raid_status1 = self.raid_status(1)
        if 'degraded' in raid_status1:
            try:
                self.log.info('Raid status: Degraded.')
                self.check_home_page('degraded')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
                self.read_write_access_check(sharename=sharename, prot='NFS')
            except Exception, ex:
                self.log.exception('Failed to access First Degraded_RAID1\n' + str(ex))
        else:
            self.log.error('Failed to Plug out Disk 1\n')
                     
        # Plug in Disk 1
        self.log.info('Plug in Disk 1 !')
        self.insert_drives(1)
        self.wait_system_busy()

        self.manual_start_rebuild()
        
        # Wait until rebuild is finished and validate RAID access.
        self.wait_rebuild_finished(1)
        self.log.info('Check RAID1 status.')
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        while not raid_status:
            raid_status = self.get_text('raid_healthy')
            time.sleep(1)
        if raid_status == 'Healthy':
            try:
                self.log.info('Raid status is healthy.')
                self.check_home_page('healthy')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
                self.log.info('****** NAS Rebuild for RAID1 with Spanning test SUCCEEDED!! ******')
            except Exception, ex:
                self.log.exception('Failed to access Healthy_RAID1\n' + str(ex))
                self.log.info('****** NAS Rebuild for RAID1 test FAILED!! ******\n')
        else:
            self.log.info('Raid status = %s' % raid_status)
            self.log.critical('****** NAS Rebuild for RAID1 test FAILED!! ******\n')
        
    def hash_check(self, localFilePath, uutFilePath, fileName):       
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName) 
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED')
        else:
            self.log.error('Hash test FAILED')
        
    def read_write_access_check(self, sharename, prot):

        if prot == 'SAMBA':
            generated_file = self.generate_test_file(generated_file_size)
            time.sleep(10)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            # Wait for transfer finished
            time.sleep(10)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName='file0.jpg')
            self.read_files_from_smb(files='file0.jpg', share_name=sharename, delete_after_copy=True)
            time.sleep(120)
            
        elif prot == 'NFS' and self.uut[Fields.product] != 'Lightning':
            nfs_mount = self.mount_share(share_name=sharename, protocol=self.Share.nfs)
            created_files = self.create_file(filesize=generated_file_size, dest_share_name=sharename)
            for next_file in created_files[1]:
                if not self.is_file_good(next_file, dest_share_name=sharename):
                    self.log.error('FAILED: nfs read test')
                else:
                    self.log.info('SUCCESS: nfs read test')
        else:
            self.log.info('Protocol {} is not supported'.format(prot))
          
    def wait_system_busy(self):
        # Wait for system busy is ready
        self.log.info('Wait System Busy is ready.')
        time.sleep(30)
        waitloop = 0
        while waitloop < 50:
            drives_status = self.get_RAID_drives_status()
            drives_status1 = self.get_xml_tags(drives_status, 'status')
            self.log.info('Raid Drives Status = %s' % drives_status1)
            if drives_status1[0] not in {'drive_raid_ready', 'incorrect_drive_order', 'drive_raid_already_formatted', 'rebuilding'}:
                waitloop += 1
                self.log.info('Raid Drives Status = %s, Loop : %s' % (drives_status1,waitloop))
                time.sleep(5)
            else:
                waitloop = 50
                self.log.info('Raid Drives Status is Ready.')

    def raid_status(self, raidnum):

        check_raid_status = 'mdadm --detail /dev/md%s | grep "State :"' % raidnum

        raid_status = self.run_on_device(check_raid_status)
        time.sleep(1) # Time to wait status return value
        while not raid_status:
            raid_status = self.run_on_device(check_raid_status)
            time.sleep(1)
        self.log.info('Raid_status = %s' % raid_status)
        raid_status1 = str(raid_status.split(': ')[1])

        return raid_status1
    
    def wait_rebuild_start(self, raidnum):

        # Wait for recovering started
        waitloop = 0
        while waitloop < 30:
            raid_status1 = self.raid_status(raidnum)
            if not 'recovering' in raid_status1:
                waitloop += 1
                time.sleep(1)
                self.log.info('Wait rebuild starting. Loop %s' % waitloop)
            else:
                waitloop = 30

    def wait_rebuild_finished(self, raidnum):

        # Wait for recovering finished
        waitloop = 0
        while waitloop < 60:
            raid_status1 = self.raid_status(raidnum)
            if 'recovering' in raid_status1:
                waitloop += 1
                self.log.info('Wait rebuilding finished. Loop %s' % waitloop)
                time.sleep(60)
            else:
                waitloop = 60
                time.sleep(60)

    def enable_rebuild(self):

        try:
            self.get_to_page('Storage')
            if self.is_element_visible('storage_raidFormatFinish9_button'):
                self.log.info('Close Storage completed page.')
                self.click_element('storage_raidFormatFinish9_button')
            check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
            time.sleep(2)
            check_status_attr = check_status.get_attribute('style')
            if 'block' in check_status_attr:
                self.log.info('Enable Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .toggle_off')
                check_status_attr = check_status.get_attribute('style')
                while 'block' in check_status_attr:
                    self.click_element('css = #storage_raidAutoRebuild_switch+span .toggle_off')
                    check_status_attr = check_status.get_attribute('style')
                    time.sleep(2)
                time.sleep(2)
            if 'none' in check_status_attr:
                self.log.info('Auto-Rebuild already enabled')
            self.close_webUI()

        except Exception, ex:
                self.log.exception('Failed to enable auto-rebuild\n' + str(ex))
    
    def verify_raid_config(self):
        self.get_to_page('Storage')
        returnedVolume = self.get_text("//tr[@id='row1']/td/div")
        if returnedVolume == 'Volume_1':
            self.log.info('SUCCESS: RAID section shows  the correct volume: {}'.format(returnedVolume))
        else:
            self.log.error('FAILED: RAID section shows incorrect volume: {}'.format(returnedVolume))
        
        returnedRaid = self.get_text("//tr[@id='row1']/td[2]/div")
        if returnedRaid == 'RAID 1':
            self.log.info('SUCCESS: RAID section shows  the correct drive configuration: {}'.format(returnedRaid))
        else:
            self.log.error('FAILED: RAID section shows incorrect drive configuration: {}'.format(returnedRaid)) 
    
              
    def disable_auto_rebuild(self):
        try:
            self.log.info('Disabling auto rebuild ...')
            self.get_to_page('Storage')
            if self.is_element_visible('storage_raidFormatFinish9_button'):
                self.log.info('Close Storage completed page.')
                self.click_element('storage_raidFormatFinish9_button')
            check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
            time.sleep(2)
            check_status_attr = check_status.get_attribute('style')
            if 'none' in check_status_attr:
                self.log.info('Disable Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .toggle_off')
                check_status_attr = check_status.get_attribute('style')
                while 'none' in check_status_attr:
                    self.click_element('css = #storage_raidAutoRebuild_switch+span .toggle_off')
                    check_status_attr = check_status.get_attribute('style')
                    time.sleep(2)
                time.sleep(2)
            if 'block' in check_status_attr:
                self.log.info('Auto-Rebuild has already been disabled')
            
        except Exception, ex:
                self.log.exception('Failed to disable auto-rebuild\n' + str(ex))
    
    def manual_start_rebuild(self):
        
        try:
            self.log.info('Manual start raid rebuild.')
            self.get_to_page('Storage')
            time.sleep(2)
            self.click_element('storage_raidManuallyRebuild_button')
            time.sleep(2)
            self.click_element('storage_raidManuallyRebuildNext1_button')
            time.sleep(2)
            self.wait_until_element_is_clickable('storage_raidManuallyRebuildNext2_button', 180)
            self.click_element('storage_raidManuallyRebuildNext2_button')
            self.wait_until_element_is_not_visible(E.UPDATING_STRING)
            self.close_webUI()
        
        except Exception, ex:
            self.log.exception('Failed to manual start rebuilding!')
    
    def check_home_page(self, raidstate):    
        try:
            self.log.info('Start to check raid status in home page')
            self.get_to_page('Home')
            if raidstate == 'degraded':
                dignostics_state = self.get_text('diagnostics_state')
                if 'Caution' in dignostics_state:
                    self.click_element('smart_info')
                    self.drag_and_drop_by_offset('css = div.jspDrag', 0, 80)
                    diagnosticsRaidStatus = self.get_text('home_diagnosticsRaidStatus_value')
                    if 'Degraded' in diagnosticsRaidStatus:
                        self.log.info('Raid status is Degraded!')
                else:
                    self.log.error('Raid status is not in Degraded')         
                    
            if raidstate == 'healthy':
                dignostics_state = self.get_text('diagnostics_state')
                if dignostics_state == 'Healthy':
                    self.log.info('Raid status is Healthy!')
                else:
                    self.log.error('Raid status is not in Healthy')
                    
            # Capacity verification
            if self.is_element_visible('home_b4_capacity_info'):
                raid_capacity = self.get_text('home_volcapity_info')
            elif self.is_element_visible('home_b4_free'):
                raid_capacity = self.get_text('home_b4_free')

            if raid_capacity:
                self.log.info('Raid capacity is {}'.format(raid_capacity))
            if not raid_capacity:
                self.log.error('Checked raid capacity failed in home page!')
            self.close_webUI()
            
        except Exception, ex:
            self.log.exception('Checked raid status failed in home page!')
            
    def verifyMountVolume(self, volumeid):
        vol = ''
        try:
            self.get_to_page('Storage')
            if self.is_element_visible('css = #vol_list > tbody:nth-child(1) > tr:nth-child(%s) > td:nth-child(1) > div:nth-child(1)' % 1):
                vol = self.get_text('css = #vol_list > tbody:nth-child(1) > tr:nth-child(%s) > td:nth-child(1) > div:nth-child(1)' % 1)
               
            if 'Volume_1' in vol:
                self.log.info('Volume: {} Mount Succeeded!!'.format(vol))
            else:
                self.log.error('Volume: {} Mount Failed!!'.format(vol))
        except Exception, ex:
            self.log.exception('Volume did not mount!!')
 
nasRaidRebuild2DrivesRAID1wSpanning()