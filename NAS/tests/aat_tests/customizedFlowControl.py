""" FTP Customized Flow Control 

    @Author: lin_ri
    
    Objective: To monitor the ftp upload speed with customized flow limit
    wiki URL: http://wiki.wdc.com/wiki/FTP
    
    Automation: Full
    
    Supported Products:
        Lightning
        Sprite
    
    Test Status: 
              Local Ligtning: Pass (FW: 1.05.30)
     Irvine Jenkins Ligtning: Pass (FW: 1.06.30)
           TW Jenkins Sprite: Pass (FW: 1.06.112)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import time
import os
import timeit
import shutil

FTP_URL = "192.168.1.91"
limit_speed = 200  # default: 200 KB/s

picturecounts = 0

class flowControl(TestClient):

    def run(self):
        try:
            global FTP_URL
            self.log.info('######################## FTP customized flow control TESTS START##############################')
            FTP_URL = self.uut[Fields.internal_ip_address]
            self.log.info("DUT FTP IP: {}".format(FTP_URL))
            self.delete_all_shares()  # Clean all shares except Public, SmartWare and TimeMachineBackup
            self.access_webUI()
            time.sleep(1)           
            self.configure_ftp()
            self.config_public_share()
            self.config_ftp_bandwidth(20)  # speed * 10, limit = 20*10 KB/s
            self.close_webUI()
            time.sleep(2)
            self.upload_test()   # Create 10MB temporary file to upload       
        except Exception as e:
            self.log.error("Test Failed: FTP customized Flow control test, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        finally:
            self.clear_ftp_bandwidth()
            self.log.info('######################## FTP customized flow control TESTS END ##############################')

    def clear_ftp_bandwidth(self):
        """ 
            Clear restricted bandwidth settings
        """
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info("==> Click configure")
            self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)',
                                      'css=#settings_networkFTPAccessFlowControlUnlimited_button', visible=True)
            self.log.info("Select unlimited bandwidth")
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            self.click_wait_and_check('settings_networkFTPAccessFlowControlUnlimited_button',
                                      'css=#settings_networkFTPAccessFlowControlUnlimited_button.left_button._text.sel',
                                      visible=True)
            time.sleep(2)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button', 'css=#settings_networkFTPAccessNext2_button', visible=True)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button', 'css=#settings_networkFTPAccessNext3_button', visible=True)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button', 'css=#settings_networkFTPAccessNext4_button', visible=True)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(3)  # Waiting for updating
        except Exception as e:
            self.log.error("ERROR: Failed to clear ftp bandwidth due to exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='clearftp')
        else:
            self.log.info("Clear the restricted bandwidth setting!")

    def configure_ftp(self):
        """
            Configure FTP setting to ON
        """
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info('==> Configure ftp')
            ftp_status_text = self.get_text('css = #settings_networkFTPAccess_switch+span .checkbox_container')
            if ftp_status_text == 'ON':
                self.log.info('FTP is enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.info('Turn ON FTP service')
                self.click_wait_and_check('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                          'popup_ok_button', visible=True)
                self.click_wait_and_check('popup_ok_button', visible=False)
                time.sleep(3)  # wait for updating
                ftp_status_text = self.get_text('css = #settings_networkFTPAccess_switch+span .checkbox_container')
                if ftp_status_text == 'ON':
                    self.log.info("FTP is enabled successfully. Status = {}".format(ftp_status_text))
            time.sleep(2)
        except Exception as e:
            self.log.exception('FAILED: Unable to configure ftp!! Exception: {}'.format(repr(e)))
        else:
            self.close_webUI()

    def config_public_share(self, access="Anonymous Read / Write"):
        """
            Set access permission to the public share for anonymous user

            @access: Public share access permission for anonymous
        """
        try:
            self.log.info('==> Configure FTP share with {}'.format(access))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link', 'shares_share_Public', visible=True)
            self.log.info("Click Public folder")
            self.click_wait_and_check('shares_share_Public', 'css=#shares_public_switch+span .checkbox_container', visible=True)
            time.sleep(1)
            self.log.info('Switch the ftp to ON')
            self.wait_until_element_is_visible('css=#shares_ftp_switch+span .checkbox_container', 30)
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_wait_and_check('css=#shares_ftp_switch+span .checkbox_container', 'ftp_dev', visible=True)
            self.log.info('ftp service is enabled to ON')
            self.click_link_element('ftp_dev', access)
            time.sleep(1)
            self.click_wait_and_check('shares_ftpSave_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(3)  # Waiting for Saving
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. Exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='confpublicshare')
        else:
            self.log.info("PASS: Save {} permission to Anonymous.".format(access))
    
    def config_ftp_bandwidth(self, speed=20):
        """ 
            speed is in X*10KB/s, default: 20 * 10KB/s == 200KB/s
        """
        global limit_speed
        limit_speed = speed * 10 # Unit KB/s
        try:
            self.get_to_page('Settings')
            time.sleep(2)
            self.click_wait_and_check('nav_settings_link',
                                      'settings_network_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info("==> Click configure")
            self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)',
                                      'css=#settings_networkFTPAccessFlowControlCustomize_button', visible=True)
            time.sleep(1)
            self.log.info("Select customized and set the bandwidth")
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            self.click_wait_and_check('settings_networkFTPAccessFlowControlCustomize_button',
                                      'css=#settings_networkFTPAccessFlowControlCustomize_button.right_button._text.sel',
                                      visible=True)
            time.sleep(1)
            self.input_text_check('settings_networkFTPAccessFlowK_text', str(speed), False)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button',
                                      'css=#settings_networkFTPAccessNext2_button', visible=True)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button',
                                      'css=#settings_networkFTPAccessNext3_button', visible=True)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button',
                                      'css=#settings_networkFTPAccessNext4_button', visible=True)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(3)  # Waiting for updating
        except Exception as e:
            self.log.error("FAILED: Unable to set ftp bandwidth to {} KB/s. Exception: {}".format(speed, repr(e)))
            self.takeScreenShot(prefix='configbandwidth')
        else:
            self.log.info("PASS: Restricted bandwidth is set to {} KB/s".format(limit_speed))

    def upload_test(self, filesize=10240):
        """ 
            upload file test, create 10MB(filesize unit: KB)
        """      
        global limit_speed
        self.log.info("** Going to start ftp upload test **")
        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        tmpfile = ''.join(tmp_file)
        self.log.info("Generate temporary file: {}".format(tmpfile))
        time.sleep(2)
        try:
            avg_speed = self.upload(tmpfile, filesize)
            restrict_speed = limit_speed + limit_speed / 2  # tolerance rate         
            if avg_speed > restrict_speed:
                raise Exception("Test Failed: FTP upload speed({} KB/s) is over limit (speed = {})".format(avg_speed, restrict_speed))
            else:
                self.log.info("Test Pass: FTP upload speed test pass! (speed = {} KB/s)".format(avg_speed))        
            os.chdir('..') # switch to the parent directory of the temporary file folder
        except Exception as e:
            self.log.error("Test Failed: FTP uploading test failed. Exception: {}".format(repr(e)))
        finally:
            shutil.rmtree(os.path.dirname(tmpfile))
    
    def upload(self, tmpfile, filesize=10240):
        """  
            Monitor upload speed by transferring 10MB file
            Return the average of ftp upload speed 
        """
        global FTP_URL
        retry = 3
        ftps = None
        try:
            from ftplib import FTP
            self.log.info("Connecting to FTP Server: {}".format(FTP_URL))
            while retry > 0:
                try:
                    ftps = FTP(FTP_URL)
                except Exception as e:
                    retry = retry - 1
                    self.log.info("Retrying {} times".format(3-retry))
                    if retry == 0:
                        self.log.error("Failed 3 retries to establish FTP connection to {}, exception: {}".format(FTP_URL, repr(e)))
                    time.sleep(1)
                else:
                    break
            try:
                ftps.login()
            except Exception as e:
                self.log.error("Anonymous failed to login. exception: {}".format(repr(e)))
            ftps.cwd('Public')
            try:
                f = open(tmpfile, "rb")
            except Exception as e:
                self.log.error("Unable to open temporary file: {} for uploading, exception: {}".format(tmpfile, repr(e)))
            up_speed = []
            block_start_time = [0]
            block_end_time = [0]            
            def callback(block):
                block_end_time[0] = timeit.default_timer()
                elapsed_time = block_end_time[0] - block_start_time[0]
                if elapsed_time == 0:
                    elapsed_time = 1
                sizeRead = len(block)
                block_speed = sizeRead / (1024 * elapsed_time)
                if up_speed != []: # skip the first burst rate
                    self.log.info('Block speed: {} KB/s, blocksize={}'.format(block_speed, sizeRead))
                up_speed.append(block_speed)
                block_start_time[0] = timeit.default_timer()
            block_start_time[0] = timeit.default_timer()
            try:
                ftps.storbinary("STOR "+os.path.basename(tmpfile), f, 1048576, callback) # 1048576 = 1MB
            except Exception as e:
                self.log.info("storbinary() error, exception: {}".format(e))
            f.close()
            ftps.close()
            # avg_speed = float(sum(up_speed[1:])) / (len(up_speed)-1)
            avg_speed = float(sum(up_speed[2:])) / (len(up_speed)-2)
            self.log.info('The average of upload speed: %f KB/s' %(avg_speed))
            return avg_speed
        except Exception as e:
            self.log.error("Test Failed: Unable to upload FTP file successfully. Exception: {}".format(repr(e)))

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, retry=3, timeout=5):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

flowControl()
