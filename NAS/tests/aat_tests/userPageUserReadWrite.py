"""
title           :userPageUserReadWrite.py
description     :To verify if a user can be authorized 'Read/Write' access to a given folder
author          :yang_ni
date            :2015/04/07
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
from smb.smb_structs import OperationFailure

class userPageUserReadWrite(TestClient):

    def run(self):

        try:
            #Delete all the users, groups and shares
            self.setup()

            # Create a user
            self.log.info('Creating a user ...')
            self.create_user(username='user1')

            # Create a share.
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=1)

            # Change share to private, share must be set to private instead of public. Toggle the Public switch off
            self.log.info('Toggling the share from private to public ...')
            self.update_share_network_access(share_name='share', public_access=False)

            #Assigns user1 to share with the read/write access level
            self.log.info('Assigning user1 with read/write access to share ...')
            self.assign_user_to_share(user_name='user1', share_name='share', access_type='rw')


            #Check from Web UI if 'Read/Write' access of share is given to user1
            self.log.info('Checking if user1 has read/write access to share1 ...')

            '''
            time.sleep(20)
            self.get_to_page('Users')
            time.sleep(5)
            self.click_button('users_user_user1')
            text = self.get_text("//div[@id=\'user_sharelist\']/ul/li[4]/div[4]")

            if text == 'Read / Write':
                self.pass_test(testname, 'Successfully authorizes read/write access to user1')
            else:
                self.fail_test(testname, 'Failed to authorizes read/write access to user1')
            '''

            test_file = self.generate_test_file(kilobytes=1024)

            #Check if user1 has write access to the share folder, fail if no.
            try:
                self.write_files_to_smb(files=test_file, username='user1', delete_after_copy=False, share_name='share')
            except OperationFailure:
                self.log.error('Failed to write file to share folder, wrong behavior')
            else:
                self.log.info('Success to write file to share folder, correct behavior')

            #Check if user1 has read access to the share folder, fail if no.
            try:
                self.read_files_from_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.error('Failed to read file from share folder, wrong behavior')
            else:
                self.log.info('Success to read file to share folder, correct behavior')
                self.log.info('Successfully authorizes read/write access to user1')
        except Exception as e:
            self.log.error('Failed to authorizes read/write access to a user1, error: {}'.format(e))

# # @Clean up all the users/shares/groups before test starts
    def setup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()


# This constructs the userPageUserReadWrite() class, which in turn constructs TestClient() which triggers the userPageUserReadWrite.run() function
userPageUserReadWrite()