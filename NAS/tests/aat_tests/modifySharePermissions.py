"""
Created on Jan 5, 2015
Author: Carlos Vasquez
"""

from testCaseAPI.src.testclient import TestClient
from smb.smb_structs import OperationFailure
import time

private_share_name_B = ['PrivateShareB_1', 'PrivateShareB_2', 'PrivateShareB_3','PrivateShareB_4']

share_desc = 'share volume'

class modifySharePermission(TestClient):
    
    def run(self):      
        try:
           self._myCleanup()
           
           # configure raid (JBOD)
           self._configureJBOD()
           
           # Create or modify share folders
           self._create_shares(private_share_name_B, public=False)
                                                  
           # Create or modify users
           self._update_user_shares()         
  
           # Modify permissions
           self._modifySharePermissions('PrivateShareB_2')
        except:
            self.log.exception('An exception has occurred modifying share permissions')
    
    def _myCleanup(self): 
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        self.execute('smbif -b user1')
        
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()
        
        self.log.info('Deleting all groups ...')
        for group in range (1,65):
            group_name = 'group{0}'.format(group)
            self.delete_group(group_name) 
            
         
    def _configureJBOD(self):   
        # configure raid (JBOD) if already JBOD skip configuration
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

        if modelNumber in ('BWAZ', 'BBAZ') and (self.get_raid_mode() <> 'JBOD'):  
            self.log.info('Configuring JBOD(2-Bay...')
            self.configure_jbod()   
        elif modelNumber in ('LT4A', 'BNEZ', 'BWZE') and (self.get_raid_mode() <> 'JBOD'):
            self.log.info('Configuring JBOD(4-Bay)...')
            self.configure_jbod()
        else: 
            self.log.info('No raid configuration needed')
             
    def _create_shares(self, share_name = [], public=True):
        self.log.info('######################## Create Shares ########################')
        try:                      
            for i in range (1,5):
                self.log.info('Creating share: {0}'.format(share_name[i-1]))
                self.create_shares(share_name=share_name[i-1], description=share_desc, public_access=True)      
        except Exception,ex:
            self.log.info('Failed to create share\n' + str(ex))
       
    def _assign_share_to_user(self, user_name, share_name, access_type='rw'):
        """
        Assigns the given share to the given user name with the given access level

        The user number corresponds to the user name, e.g. user1 is share number 1,
        user34 is 34.

        Note: We assume that users were created using this framework (user0...user511)
        share must be set to private instead of public. Toggle the Public switch off

        Acceptable access_level:
        rw - Read/Write access
        r  - Read access only
        d  - No access
        """
        self.get_to_page('Users')
        access_text = access_type.lower()
        #share_number = user_name.replace("user", "")
        share_number = share_name.replace(str(share_name),"")
        share_link = 'users_rw4_link'
        user_link = 'users_user_user2'
        time.sleep(2)
        #self.find_element_in_sublist(user_name, 'user')
        self.click_element(user_link)
        time.sleep(2)
        self.click_element(share_link)
        self.wait_until_element_is_not_visible('css=div.updateStr')
    
            
    def _update_user_shares(self):
        try:   
            self.log.info('######################## Update User Shares ########################')
            # Create user1 to be used to access shares
            createdUser1 = self.create_user(username='user1', fullname='User One', password='fituser', is_admin=False)
            if createdUser1 == 0:
                self.log.info('user1 was created successfully')
            else:
                self.log.error('user1 was not created successfully')
                
            # Create user2 to be used to access shares
            createdUser2 = self.create_user(username='user2', fullname='User One', password='fituser', is_admin=False)
            if createdUser2 == 0:
                self.log.info('user2 was created successfully')
            else:
                self.log.error('user2 was not created successfully') 
                      
            # For user2 deny access for PrivateShareB_2
            self.create_share_access(share_name='PrivateShareB_2', username='user2', access='D')                
        except Exception,ex:
            logger1.info('Failed to update share\n' + str(ex))      
            
    def _modifySharePermissions(self, shareName):
        
        self.log.info('######################## Modify Permissions ########################')
        
        
        self.update_share(share_name='user2', public_access=False)
        self._assign_share_to_user(user_name='user2', share_name='PrivateShareB_2', access_type='rw')
        
        self.clear_cache()
 
        generated_file = self.generate_test_file(kilobytes=1)
        try:
            self.write_files_to_smb(username='user2', password='fituser', files=generated_file, share_name=shareName, delete_after_copy=True)
        except OperationFailure:
            self.log.error('Failed to write file to {0}'.format(shareName))
            
        
        # Check the generated file exists in share 
        try:
            exists = self.list_files_on_smb(username='user2', password='fituser', share_name=shareName)
            if 'file0.jpg' in exists:
                self.log.info('Modify Permissions(write file) test for {0}: SUCCESS'.format(shareName))
            else:
                self.log.error('Modify Permissions(write file) test for {0}: FAILED'.format(shareName))
        except OperationFailure:
            self.log.error('Failed to list files from {0}'.format(shareName))
          
        # Delete file from share
        try:
            self.delete_file_from_smb(filename='file0.jpg', username='user2', password='fituser', share_name=shareName)
        except OperationFailure:
            self.log.error('Failed to delete file from {0}'.format(shareName))
        
        # Check the deleted file does not exist in share 
        try:
            deleted_ = self.list_files_on_smb(username='user2', password='fituser', share_name=shareName)
            if 'file0.jpg' not in deleted_:
                self.log.info('Modify Permissions(delete file) test for {0}: SUCCESS'.format(shareName))
            else:
                self.log.error('Modify Permissions(delete file) test for {0}: FAILED'.format(shareName)) 
        except OperationFailure:
            self.log.error('Failed to list files from {0}'.format(shareName))
           
modifySharePermission() 