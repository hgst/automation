"""
Created on May 27th, 2015

@author: tran_jas

## @brief User shall able to FTP download via web UI as regular user
#
#  @detail 
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=31797&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=50&nTP=117562&etab=8

"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

ftp_file_path_20 = '/shares/Public/FTPTest/'
my_ftp_user = 'jtran'
ftp_password = 'fituser'
ftp_filename_2 = 'diaryofacamper_raw.mp4'
file_md5_sum_2 = '185236549de96b693b6cd7b2afb25427'
rm_ftp_test = 'rm -r /shares/Public/FTPTest'
ftp_folder = "FTP_folder-345"


class FtpDownload(TestClient):

    def run(self):
        try:
            self.log.info('User page - FTP download as anonymous')

            # ensure accept EULA as admin - in case EULA hasn't been accepted
            self.access_webUI()
            self.close_webUI()

            self.create_user(my_ftp_user, password=ftp_password)
            self.uut[self.Fields.web_username] = my_ftp_user
            self.uut[self.Fields.web_password] = ftp_password
            self.ftp_downloader_anonymous()
        except Exception, ex:
            self.log.exception('Failed to complete FTP download \n' + str(ex))
        finally:
            self.log.info('Delete downloaded folder')
            self.execute(rm_ftp_test)

            self.log.info('Delete FTP job')
            self.click_element('ftp_downloads')
            self.click_element('apps_ftpdownloadsDelJob1_button')
            while self.is_element_visible('popup_apply_button') is False:
                time.sleep(5)
                self.click_element('apps_ftpdownloadsDelJob1_button')

            self.click_element('popup_apply_button')

            self.log.info('Reset to admin')
            self.delete_user(my_ftp_user)
            self.uut[self.Fields.web_username] = 'admin'
            self.uut[self.Fields.web_password] = ''
            self.close_webUI()
            self.end()

    def ftp_downloader_anonymous(self):

        ftp_server_ip = self.uut[self.Fields.ftp_server]
        self.log.info('ftp_server_ip: {}'.format(ftp_server_ip))

        self.access_webUI()

        if self.is_element_visible('nav_downloads_link'):
            self.log.info('Testing as regular user')
            self.click_element('nav_downloads_link')
            time.sleep(3)
            self.click_element('ftp_downloads')
            time.sleep(3)
        else:
            self.get_to_page('Apps')
            self.click_element('apps_ftpdownloads_link')

        self.click_element('apps_ftpdownloadsCreate_button')

        self.log.info('FTP download a folder')
        self.input_text('apps_ftpdownloadsTaskName_text', ftp_folder, True)
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.click_element('apps_ftpdownloadsSourcepath_button', 120)
        time.sleep(5)
        self.click_element('link=Public', 120)
        time.sleep(5)
        # self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/ul/li[2]/label/span', 120)
        # self.click_element("//li[2]/ul/li[2]/label/span", 120)
        self.click_element_at_coordinates('link=FTPTest', -70, 0)
        time.sleep(5)

        self.click_element('home_treeOk_button')

        self.click_element('apps_ftpdownloadsDestpath_button', 120)
        time.sleep(5)
        # self.click_element('//div[@id=\'folder_selector\']/ul/li/label/span', 120)
        # self.click_element("//li/label/span", 120)
        self.click_element_at_coordinates('link=Public', -70, 0)

        self.click_element('home_treeOk_button')
        self.click_element('apps_ftpdownloadsCreate3_button')

        time.sleep(30)
        my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
        self.log.info('Current ftp downloading status: {0}'.format(my_text))
        max_wait = 0
        while my_text != "Download Completed" and max_wait < 15:
            time.sleep(60)
            max_wait += 1
            self.log.info('Waiting for ftp download, loop count: {0}'.format(max_wait))
            my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
            self.log.info('Current ftp downloading status: {0}'.format(my_text))

        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path_20, ftp_filename_2)
        if file_md5_sum_2 == ftpdownloadedfilesum:
            self.log.info('FTP folder download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP folder download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

FtpDownload()