""" NAS RAID Downgrade - RAID 1 (2 drive) (MA-243)

    @Author: lin_ri
    Procedure @ http://wiki.wdc.com/wiki/NAS_RAID_Downgrade
    
    Automation: 
    
    Not supported product:
        Glacier (Does not have Storage page)
    
    Support Product:
        Lightning (No NFS service support)
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon
    
                   
    Test Status:
        Local Yosemite: Pass (FW: 2.00.201)
""" 

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as Elem
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
from xml.etree import ElementTree as ET
import time
import os
import sys
import shutil
import inspect

products_info = {
        'LT4A': 'Lightning',      
        'BNEZ': 'Sprite',
        'BWZE': 'Yellowstone',
        'BWAZ': 'Yosemite',
        'BBAZ': 'Aurora',
        'KC2A': 'KingsCanyon',
        'BZVM': 'Zion',
        'GLCR': 'Glacier',
        'BWVZ': 'GrandTeton'
}


# Lightning does not support NFS
# Glacier 
noNfsSupportModel = ('LT4A', 'GLCR')
newShareName = 'R1toJBOD'
tempFileSize = 10 # 10KB
# JBOD on Drive 1: available disk 2, vice versa
availDrive = {1:2, 2:1}

class nasRaidDowngrade2DrviesRaid1(TestClient):
    
    def run(self):
        self.log.info('######################## NAS RAID Downgrade - RAID 1 (2 drive) (MA-243) TEST START ##############################')
        if self.uut[self.Fields.product] in ('Glacier', ):
            self.log.info("Glacier does not support RAID, skip the test")
            return
        remDriveLst = []
        try:
            for jbodDrive in (1, 2):
                self.log.info("========== START - RAID 1 downgrade to JBOD (on Drive {}) ==========".format(jbodDrive))
                self._unmount_all()
                self.nfs_folders_cleanup()
                drivesLabel = self.getdriveslist()
                self.cleanRaidConfig(diskLabels=drivesLabel)
                self.cleanDisks(diskLabels=drivesLabel)
                remDriveLst = self.populate2SameDrives()
                # Configure RAID 1
                diskLabel = self.getdriveslist()
                self.config_raid1(diskLabel)
                self.closeRaidCompletePage()
                self.verifyRAIDVolumeSoup(volumeId=1)  # Replace verifyRAIDVolume()
                self.delete_all_shares()
                # Switch off Auto-Rebuild
                self.disableAutoRebuild()
                # Create share
                self.log.info("********** Create {} share **********".format(newShareName))
                self.create_shares(share_name=newShareName, force_webui=True)
                self.update_share_network_access(share_name=newShareName, public_access=True)
                # Verify Samba protocol
                self.log.info("********** Verify Samba protocol on {} share **********".format(newShareName))
                self.read_write_access_check(shareName=newShareName, prot='SAMBA', testFileSize=tempFileSize)  # 10240KB -> 10KB
                # Verify NFS protocol
                self.log.info("********** Verify NFS protocol on {} share **********".format(newShareName))
                productModel = self.get_model_number()
                if productModel in noNfsSupportModel:
                    self.log.info('{} model does not support NFS service, skip NFS test!'.format(productModel))
                else:
                    self.read_write_access_check(shareName=newShareName, prot='NFS', testFileSize=tempFileSize) # 10KB
                    # pass
                # Change RAID mode (R1 to JBOD on drive 1)
                self.r1Down2JbodCmd(diskLabel=diskLabel, drive=jbodDrive)
                self.closeRaidCompletePage()
                self.verifyRAIDVolume(volumeId=1, raid=11) # 11: JBOD
                self.verifyAvailableDisk(diskLabel=diskLabel, drive=availDrive[jbodDrive]) # available disk 2 due to JBOD drive 1
                # Check Home Page
                self.uiCheckSize()
                # Check Shares Page
                self.uiCheckShares(shareName=newShareName)
                # Check Storage Page
                self.verifyRAIDHealth()
                self.verifyDiskProfile()
                self.verifyDiskStatus()
                # Verify Samba protocol
                self.log.info("Verify Samba protocol on {} share".format(newShareName))
                self.read_write_access_check(shareName=newShareName, prot='SAMBA', testFileSize=tempFileSize) # 10KB
                # Verify NFS protocol
                productModel = self.get_model_number()
                if productModel in noNfsSupportModel:
                    self.log.info('{} model does not support NFS service, skip NFS test!'.format(productModel))
                else:
                    self.read_write_access_check(shareName=newShareName, prot='NFS', testFileSize=tempFileSize)  # 10KB
                    # pass
                self.log.info("========== END - RAID 1 downgrade to JBOD (on Drive {}) ==========".format(jbodDrive))
                self.insertDrives(remDriveLst)
        except Exception as e:
            self.log.error("FAILED: NAS RAID Downgrade - RAID 1 (2 drive) (MA-243) Test Failed!!, exception: {}".format(repr(e)))
        else:
            self.log.info("PASS: NAS RAID Downgrade - RAID 1 (2 drive) (MA-243) Test PASS!!!")
        
        finally:
            self.log.info("========== (MA-243) Clean Up ==========")
            self.insertDrives(remDriveLst)
            self.log.info('######################## NAS RAID Downgrade - RAID 1 (2 drive) (MA-243) TEST END ##############################')

    def nfs_folders_cleanup(self):
        from global_libraries import CommonTestLibrary as ctl
        from glob import glob
        if os.path.exists('mnt'):
            os.chdir('mnt')
            nfs_tmps = glob('wdaat*')
            for folder in nfs_tmps:
                self.log.info("Remove nfs temp folder: {}".format(folder))
                resp = ctl.run_command_locally('sudo rm -rf {}'.format(folder))
                self.log.info("Result: {}".format(resp))
        else:
            self.log.info("No mnt folder is found!")

    def tc_cleanup(self):
        """
            Clean up action to make sure all drives are back
        """
        self.resumeDrivesBack()

    def resumeDrivesBack(self):
        """
            Check supported bays and available bays to power on the remaining slots
        """
        numOfBays = self.uut[Fields.number_of_drives]
        fulldriveset = set(map(str, range(1, numOfBays+1)))
        drive_list = self.get_xml_tags(self.get_RAID_drives_status()[1].content, 'location')
        curdriveset = set(drive_list)
        remdriveset = fulldriveset - curdriveset
        for drive in remdriveset:
            self.log.info("Insert drive {} back".format(drive))
            remCmd = 'sata_pwr_ctl -n {} -p1'.format(drive)
            self.execute(remCmd)
        if remdriveset:
            self.wait_until_HDD_busy_is_ready()

    def uiCheckShares(self, shareName):
        """
        :param shareName: Share Name you'd like to verify if exists
        """
        self.get_to_page('Shares')
        time.sleep(2)
        try:
            self.wait_until_element_is_visible('shares_share_{}'.format(shareName), timeout=5)
        except Exception as e:
            self.log.error("ERROR: Unable to find the [{}] share, exception: {}".format(shareName, repr(e)))
            raise
        self.click_element('shares_share_{}'.format(shareName))
        self.log.info("PASS: Click share: {}".format(shareName))

    def verifyDiskProfile(self):
        """
            Verify the Disk Profile
        """
        self.get_to_page('Storage')
        try:
            self.click_wait_and_check('nav_storage_link', 'storage_raid_link', visible=True)
            self.click_wait_and_check('css=#disk_mgmt', 'css=#disk_mgmt.LightningSubMenuOn', visible=True)
            time.sleep(2)
            self.wait_until_element_is_visible("//div[@class=\'field_top\']/table/tbody/tr/td[1]/span", timeout=15)
            healthTitleText = self.get_text("//div[@class=\'field_top\']/table/tbody/tr/td[1]/span")
            self.wait_until_element_is_visible("//div[@id=\'disk_mgmt_state\']/span", timeout=15)
            diskHealthText = self.get_text("//div[@id=\'disk_mgmt_state\']/span")
            if 'Healthy' in diskHealthText:
                self.log.info("PASS: {} = {}".format(healthTitleText, diskHealthText))
            else:
                self.log.error("ERROR: {} = {}".format(healthTitleText, diskHealthText))
        except Exception as e:
            self.log.error("ERROR: Disk profile information is not properly displayed! Exception: {}".format(repr(e)))

    def verifyDiskStatus(self):
        # Glacier does not have Storage page
        self.get_to_page('Storage')
        time.sleep(2)
        self.click_element(Elem.STORAGE_DISK_STATUS)
        time.sleep(2)
        # How many physical drives are installed now
        drive_list = self.get_xml_tags(self.get_RAID_drives_status()[1].content, 'location')
        numDrives = len(drive_list)
        try:
            for i in range(1, numDrives+1):
                diskNumLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[2]/div".format(i)
                self.wait_until_element_is_visible(diskNumLocator, timeout=5)
                diskNum = self.get_text(diskNumLocator)
                diskSizeLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[3]/div".format(i)
                self.wait_until_element_is_visible(diskSizeLocator, timeout=5)
                diskSize = self.get_text(diskSizeLocator)
                diskStatusLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[5]/div".format(i)
                self.wait_until_element_is_visible(diskStatusLocator, timeout=5)
                diskStatus = self.get_text(diskStatusLocator)
                if 'Good' in diskStatus:
                    self.log.info("PASS: {}, Size = {}, Status = {}".format(diskNum, diskSize, diskStatus))
                else:
                    self.log.error("ERROR: {}, Size = {}, Status = {}".format(diskNum, diskSize, diskStatus))
                time.sleep(2)
        except ElementNotReady as e:
            self.log.error("ERROR: {}".format(repr(e)))

    def verifyRAIDHealth(self):
        """
            Verify the RAID Health
        """
        self.get_to_page('Storage')
        time.sleep(1)
        try:
            self.wait_until_element_is_visible("//div[@id=\'div_raid_desc\']/table/tbody/tr/td[1]/span", timeout=5)
            healthTitleText = self.get_text("//div[@id=\'div_raid_desc\']/table/tbody/tr/td[1]/span")
            self.wait_until_element_is_visible('raid_healthy', timeout=5)
            raidHealthText = self.get_text('raid_healthy')
            self.wait_until_element_is_visible('raid_healthy_desc', timeout=5)
            raidHealthDescText = self.get_text('raid_healthy_desc')
            self.log.info("{}: {}, {}".format(healthTitleText, raidHealthText, raidHealthDescText))
            if 'Healthy' in raidHealthText:
                self.log.info("{}: {}".format(sys._getframe().f_code.co_name,"RAID status is HEALTHY!!"))
            else:
                self.log.error("{}: {}".format(sys._getframe().f_code.co_name, "RAID status is {}".format(raidHealthText)))
        except Exception as e:
            self.log.error("ERROR: RAID Health information is not properly displayed! Exception: {}".format(repr(e)))

    def uiCheckSize(self, volumeId=1):
        """
            Check the Volume_X size (Assume no extra data has been copied)
        :param volumeId: Volume_1 or Volume_2
        """
        self.get_to_page('Home')
        time.sleep(1)
        HomeCapLocator = 'home_b4_free'  # Lightning, KingsCanyon, Glacier, Zion
        if self.is_element_visible(HomeCapLocator, wait_time=5):
            cap_text = self.get_text(HomeCapLocator)
        else:
            HomeCapLocator = 'home_b4_capacity_info' # Aurora, Sprite, Yosemite, Yellowstone
            HomeUnitLocator = 'home_b4_capacity_size'
            cap_text = self.get_text(HomeCapLocator) + '\n' + self.get_text(HomeUnitLocator)

        cap_text = ''.join(cap_text.split())
        self.log.info("Home page - Capacity: {}".format(cap_text))
        self.get_to_page('Storage')
        time.sleep(1)
        volSizeLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[3]/div".format(volumeId)
        self.wait_until_element_is_visible(volSizeLocator, timeout=5)
        volSizeText = ''.join(self.get_text(volSizeLocator).split()) # 1.96TB
        self.log.info("Storage page - Capacity: {}".format(volSizeText))
        if volSizeText == cap_text:
            self.log.info("PASS: Same Capacity.")
        else:
            self.log.error("FAILED: Incorrect capacity.")

    def verifyAvailableDisk(self, diskLabel, drive=2):
        """
        :param drive: which drive is available
        :return:
        """
        if not diskLabel:
            diskLabel = {1: 'sda', 2:'sdb'}
        availdiskcmd = 'ls -l /dev/{}* | wc -l'.format(diskLabel[drive])
        resp = int(self.run_on_device(availdiskcmd).strip())
        self.log.info("# of partition on {}: {}".format(diskLabel[drive], resp-1))
        if resp == 1: # no partition
            self.log.info("PASS: DISK {} is available for new volume creation!".format(drive))
        else:
            self.log.error("FAILED: DISK {} has {} partitions".format(drive, resp-1))

    def r1Down2JbodUi(self, drive=1):
        """
            UI to migrate R1 to JBOD
            
            Downgrade RAID 1 to JBOD on either drive 1 or drive 2 
            @drive: 1  (on sda)
                    2  (on sdb)

            Obsoleted for now
        """
        self.get_to_page('Storage')
        time.sleep(2)
        self.log.info("Click [Change RAID Mode] button")
        self.click_element('storage_raidChangeRAIDMode_button')
        time.sleep(1)
        self.wait_until_element_is_visible('popup_apply_button', timeout=5)
        self.click_element('popup_apply_button')
        time.sleep(1)
        downText = self.get_text('DIV_CHECKBOX_R12R5_HTML')
        self.log.info("DBG: {}".format(downText))
        self.click_element('css=.LightningCheckbox,storage_raidFormatType10_chkbox')
        time.sleep(2) # wait for the Next button visible
        self.wait_until_element_is_visible('storage_raidFormatNext1_button', timeout=5)
        self.click_element('storage_raidFormatNext1_button')
        # Choose which volume at this page if you have more than 1 volume
        self.log.info("Choose the primary drive: Drive {}".format(drive))
        if drive == 1:
            self.click_element('css=.LightningCheckbox,f_r12std_primaary_hd')
        else:
            self.click_element('css=.LightningCheckbox,f_r12std_primaary_hd')

    def getdriveslist(self):
        """
            Yellowstone and Sprite are {1: 'sdc', 2:'sdd', 3:'sda', 4:'sdb'}
            Others are {1:'sda', 2:'sdb', 3:'sdc', 4:'sdd'}
        :return: {diskbay number: drive letter} mapping
        """
        drives = {}
        xml_contents = self.execute('cat /var/www/xml/sysinfo.xml')[1]
        root = ET.fromstring(xml_contents)
        for diskid in root.findall('.//disk'):
            driveId = diskid.attrib['id']
            driveLetter = diskid.find('name').text
            connected = diskid.find('connected').text
            if connected == '0':
                self.log.info("Disk(Bay)-{} is empty without disk connected!".format(driveId))
                continue
            else:
                self.log.info("Disk(Bay)-{}:{}".format(driveId, driveLetter))
                drives[int(driveId)] = driveLetter
        print drives
        return drives

    def r1Down2JbodCmd(self, diskLabel, drive=1):
        """
            Command line to migrate R1 to JBOD
            
            Downgrade RAID 1 to JBOD on either drive 1 or drive 2 
            @drive: 1  (on sda or sdc)
                    2  (on sdb or sdd)
        """
        if not diskLabel:
            diskLabel = {1: 'sda', 2:'sdb'}
        downCmd = 'ps aux | grep -v grep | grep diskmgr'
        self.log.info('*'*70)
        self.log.info("*********** Going to downgrade RAID 1 to JBOD on drive {} ***********".format(diskLabel[drive]))
        self.log.info('*'*70)
        if drive == 1:
            migCmd = "diskmgr -v1 -m1 -k{}{} --kill_running_process --load_module --raid1_to_std".format(diskLabel[1], diskLabel[2])
        else:
            migCmd = "diskmgr -v1 -m1 -k{}{} --kill_running_process --load_module --raid1_to_std".format(diskLabel[2], diskLabel[1])
        self.log.info("Downgrade Command: {}".format(migCmd))
        self.execute(migCmd)
        # Wait for downgrade completion if it is still going
        count = 0
        while True:
            count += 1
            if count > 10:
                self.log.info("Reach the maximum 5 minutes wait for downgrade(R1 to JBOD) completion")
                break
            try:
                downChkResult = self.run_on_device(downCmd)
            except Exception as e:
                self.log.info("Retry: Unable to check downgrade status, exception: {}".format(repr(e)))
                time.sleep(30)
                continue
            if downChkResult:
                self.log.info("RAID 1 to JBOD is still under migration...")
                self.log.debug("== {} ==".format(downChkResult))
                time.sleep(30)
                continue
            else:
                break
        time.sleep(10) # Wait for system update

    def config_raid1(self, diskLabel):
        """
            Configure RAID 1 with disk 1 and disk 2
        """
        supportedRAID = {'11': 'JBOD', '12': 'Spanning',
                         '0': 'R0', '1': 'R1',
                         '5': 'R5', '10': 'R10', '99': 'N/A'}
        if not diskLabel:
            self.log.info("** WARN: Set empty drives letters to default sda and sdb **")
            diskLabel = {1: 'sda', 2: 'sdb'}
        createR1Cmd = 'diskmgr -v1 -m4 -f3 -k{} --kill_running_process --load_module -n'.format(''.join(diskLabel.values()))
        stsCmd = 'xmldbc -S /var/run/xmldb_sock_raid_init -g /percent'
        # Create RAID 1
        self.log.info("*** Configure RAID 1 ***")
        self.log.info("CMD: {}".format(createR1Cmd))
        raidCmdStatus = self.run_on_device(createR1Cmd)
        if 'Terminated' in raidCmdStatus:
            self.log.info("Configure RAID 1 command failed and terminated, re-try again...")
            self.log.debug("DBG: {}".format(raidCmdStatus))
            self.execute(createR1Cmd)
        # Wait for volume format completion if it is still going
        count = 0
        while True:
            count += 1
            if count > 10:
                self.log.info("Reach the maximum 5 minutes wait for volume format completion")
                break
            try:
                stsChkResult = self.run_on_device(stsCmd)
            except Exception as e:
                self.log.info("Retry: Unable to check format status, exception: {}".format(repr(e)))
                time.sleep(30)
                continue
            if not stsChkResult or stsChkResult.strip() == '100':
                self.log.info("Configuration is done!")
                # time.sleep(10) # wait for sata_reload_application completion
                break
            else:
                self.log.info("RAID volume configuration is still ongoing with percentage={}...".format(stsChkResult))
                time.sleep(30)
                continue
        # Verify if RAID level is R1
        result = self.get_RAID_drives_status()[1]
        raidLevel = self.get_xml_tags(xml_content=result.content, tag='raid_mode')[0]
        if '<raid_mode />' in raidLevel:
            raidLevel = '99'
        if supportedRAID[raidLevel] == 'R1':
            self.log.info("PASS: R1 is created successfully.")
        else:
            self.log.error("FAILED: Fail to create R1, current RAID level: {}".format(supportedRAID[raidLevel]))
    
    def closeRaidCompletePage(self):
        """
            Clean temporary files to prevent the storage RAID completion page pop-up
        """
        cleanCmd1 = "rm -f /var/www/xml/dm_read_state.xml"
        cleanCmd2 = "rm -f /var/www/xml/dm_write_state.xml"
        cleanCmd3 = "rm -f /var/www/xml/cgi_fmt_gui_log.xml"
        cleanCmd4 = "rm -f /tmp/hdd_format_done"
        cleanCmd5 = "rm -f /tmp/cgi_raid_rebuild_*"
        cleanCmds = [cleanCmd1, cleanCmd2, cleanCmd3, cleanCmd4, cleanCmd5]
        
        self.log.info("Clean RAID configuration files to prevent the pop-up page")
        for i, cmd in enumerate(cleanCmds, 1):
            self.log.debug("CMD-{}: {}".format(i, cmd))
            self.execute(cmd)

    def verifyRAIDVolumeSoup(self, volumeId=1, raid=1):
        """
            Verify RAID volumes & RAID level

            return volume_X size
        """
        supportedRAID = {'11': 'JBOD', '12': 'Spanning',
                         '0': 'R0', '1': 'R1',
                         '5': 'R5', '10': 'R10'}

        self.get_to_page('Storage')
        time.sleep(2)
        result = self.get_volumes()[1]
        volumesList = self.get_xml_tags(xml_content=result.content, tag='base_path')
        # Remove non Volume_X shares
        for i in volumesList:
            if 'Volume' not in i:
                volumesList.remove(i)
        vols = len(volumesList)
        if not vols:
            vols = 1
        self.log.info("VERIFY RAID: {} VOLUME".format(supportedRAID[str(raid)]))
        from BeautifulSoup import BeautifulSoup
        try:
            page_contents = BeautifulSoup(self._sel.driver.get_source())
            mountVolumes = {}
            for i in range(1, vols+1):
                tr_row = page_contents.find("tr", {"id": "row{0}".format(i)})
                divs = tr_row.findAll("div")
                row_list = []
                for div in divs:
                    row_list.append(div.contents[0])
                mountVolumes[row_list[0]] = row_list[1]
            for k, v in mountVolumes.items():
                self.log.info("{} : {}".format(k, v))
        except Exception as e:
            self.log.error("ERROR: RAID volume information is not properly displayed! Exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            self.take_screen_shot(prefix='verifyRAIDVolume')
            raise

        # Get RAID level
        result = self.get_RAID_drives_status()[1]
        raidLst = self.get_xml_tags(xml_content=result.content, tag='raid_mode')
        while '<raid_mode />' in raidLst:
            raidLst.remove('<raid_mode />')
        raidLevel = raidLst[0] or '0'

        if supportedRAID[str(raid)] != supportedRAID[raidLevel]:
            self.log.error("ERROR: Current RAID: {}, not {}".format(supportedRAID[raidLevel], supportedRAID[str(raid)]))
        else:
            self.log.info("PASS: RAID level = {}".format(supportedRAID[raidLevel]))

        volStr = 'Volume_{}'.format(volumeId)
        if volStr in mountVolumes.keys():
            self.log.info("{} is mounted with {}".format(volStr, mountVolumes[volStr]))
        else:
            self.log.error("ERROR: Can not find {} RAID volume".format(volStr))

    def verifyRAIDVolume(self, volumeId=1, raid=1):
        """
            Verify RAID volumes & RAID level

            return volume_X size
        """
        supportedRAID = {'11': 'JBOD', '12': 'Spanning', 
                         '0': 'R0', '1': 'R1', 
                         '5': 'R5', '10': 'R10'}        
        
        self.get_to_page('Storage')
        time.sleep(2)
        result = self.get_volumes()[1]
        volumesList = self.get_xml_tags(xml_content=result.content, tag='base_path')
        # Remove non Volume_X shares
        for i in volumesList:
            if 'Volume' not in i:
                volumesList.remove(i)
        vols = len(volumesList)
        if not vols:
            vols = 1
        self.log.info("VERIFY RAID: {} VOLUME".format(supportedRAID[str(raid)]))
        count = 3
        while count > 0:
            try:
                mountVolumes = {}
                for i in range(1, vols+1):
                    volNumLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[1]/div".format(i)
                    self.wait_until_element_is_visible(volNumLocator, timeout=5)
                    volNumText = self.get_text(volNumLocator)
                    volRaidLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[2]/div".format(i)
                    self.wait_until_element_is_visible(volRaidLocator, timeout=5)
                    volRaidText = self.get_text(volRaidLocator)
                    self.log.info("{}: {}".format(volNumText, volRaidText))
                    if not volNumText or not volRaidText:
                        raise StaleElementReferenceException('WARN: got empty value, raise exception to retry!!!')
                    mountVolumes[volNumText] = volRaidText
            except StaleElementReferenceException as e:
                self.log.info("WARN: {} retry, due to exception: {}".format(4-count, repr(e)))
                self.close_webUI()
                self.get_to_page('Storage')
                time.sleep(2)
                count -= 1
                if count == 0:
                    self.log.error("ERROR: StaleElementReferenceException reach 3 retries!!")
                    self.takeScreenShot(logname='verifyRVStale')
                    break
                continue
            except Exception as e:
                self.log.error("ERROR: RAID volume information is not properly displayed! Exception: {}".format(repr(e)))
            else:
                break
        
        # Get RAID level
        result = self.get_RAID_drives_status()[1]
        raidLst = self.get_xml_tags(xml_content=result.content, tag='raid_mode')
        while '<raid_mode />' in raidLst:
            raidLst.remove('<raid_mode />')
        raidLevel = raidLst[0] or '0'
        
        if supportedRAID[str(raid)] != supportedRAID[raidLevel]:
            self.log.error("ERROR: Current RAID: {}, not {}".format(supportedRAID[raidLevel], supportedRAID[str(raid)]))
        else:
            self.log.info("PASS: RAID level = {}".format(supportedRAID[raidLevel]))

        volStr = 'Volume_{}'.format(volumeId)
        if volStr in mountVolumes.keys():
            self.log.info("{} is mounted with {}".format(volStr, mountVolumes[volStr]))
        else:
            self.log.error("ERROR: Can not find {} RAID volume".format(volStr))

    def disableAutoRebuild(self):
        """
            Switch off auto-rebuild
        """
        try:
            self.log.info('Disable auto rebuild ...')
            self.get_to_page('Storage')
            time.sleep(2)
            if self.is_element_visible('storage_raidFormatFinish9_button', wait_time=5):
                self.log.info('Close Storage completion page.')
                self.click_element('storage_raidFormatFinish9_button')
            self.log.info('==> Configure Auto-Rebuild')
            self.wait_until_element_is_visible('css = #storage_raidAutoRebuild_switch+span .checkbox_container', 30)
            autoRebuild_status_text = self.get_text('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
            if autoRebuild_status_text == 'OFF':
                self.log.info('Auto-Rebuild is disabled, status = {0}'.format(autoRebuild_status_text))
            else:
                self.log.info('Turn OFF Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                time.sleep(3)
                self.wait_until_element_is_visible('css = #storage_raidAutoRebuild_switch+span .checkbox_container', timeout=5)
                autoRebuild_status_text = self.get_text('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                if autoRebuild_status_text == 'OFF':
                    self.log.info("Auto-Rebuild is disabled successfully. Status = {}".format(autoRebuild_status_text))
            time.sleep(2)
        except Exception as e:
            self.log.error("Failed to disable Auto-Rebuild, exception: {}".format(repr(e)))
            self.takeScreenShot(logname='diableAutoRebuild')

    def cleanRaidConfig(self, diskLabels):
        """
            Clean RAID configuration
        """
        if not diskLabels:
            self.log.error("ERROR: Unavailable drive letter list!!")
            raise Exception("ERROR: Unavailable drive letter list!!")

        cleanCmd = 'diskmgr -D'
        cleanCmd += ''.join(diskLabels.values())
        cleanCmd += ' --kill_running_process'
        self.log.info("Going to Clean RAID configuration: {}".format(cleanCmd))
        self.execute(cleanCmd)
        time.sleep(3)
        self.log.info("Clean RAID configuration on drives: {}".format(','.join(map(str, diskLabels.keys()))))
     
    def populate2SameDrives(self):
        """
            Remove drives either disk 1 nor disk 2 to have only 2 disks installed
            Verify disk 1 and disk 2 are the same size
            Clean disk 1 and disk 2
            
            return removed disk list to be added when clean up
        """
        numOfBays = self.uut[Fields.number_of_drives]
        drive_list = self.get_xml_tags(self.get_RAID_drives_status()[1].content, 'location')
        numOfDrives = len(drive_list)
        productName = products_info[self.get_model_number()]
        remDriveLst = []
        
        self.log.info("Product: {}, Supported Bays: {}, Current installed drives: {}".format(productName, numOfBays, numOfDrives))

        if numOfDrives < 2: 
            self.log.error("Number of drives now: {}".format(numOfDrives))
            raise Exception("Not enough drives({}) available to continue the test...".format(numOfDrives))
        
        elif numOfDrives > 2:
            remDriveLst = range(3, numOfDrives+1)
            self.log.info("Going to remove Drives: {}".format(','.join([str(x) for x in remDriveLst])))
            self.removeDrives(remDriveLst)
        
        elif numOfDrives == 2:
            if drive_list != ['1', '2']:
                raise Exception("ERROR: Drives are not installed in Bay1 and Bay2. DriveList = {}, {}".format(*drive_list))
            
        # Verify Drive Capacity
        result = self.get_RAID_drives_status()[1]
        driveSizeList = self.get_xml_tags(result, 'drive_size')
        sameSizeChk = all(x == driveSizeList[0] for x in driveSizeList)
        if sameSizeChk:
            self.log.info("PASS: Drives are the same size!")
        else:
            raise Exception("ERROR: Installed drives are not the same size. Please check the drives to re-run the test!")

        return remDriveLst

    def removeDrives(self, driveNumLst):
        """
            Remove other drives like disk3 and disk 4 even more if exist 
        """
        if driveNumLst == []:
            return
        for drive in driveNumLst:
            self.log.info("Remove drive {}".format(drive))
            remCmd = 'sata_pwr_ctl -n {} -p0'.format(drive)
            self.execute(remCmd)
        self.wait_until_HDD_busy_is_ready()
    
    def insertDrives(self, driveNumLst):
        """
            insert other drives like disk3 and disk 4 back
            :param driveNumLst: a list of removed disk number
        """
        if driveNumLst == []:
            return
        for drive in tuple(driveNumLst):
            self.log.info("Insert drive {} back".format(drive))
            remCmd = 'sata_pwr_ctl -n {} -p1'.format(drive)
            self.execute(remCmd)
            driveNumLst.remove(drive) # List is mutable, it's ok not to return the driveNumlst
        self.wait_until_HDD_busy_is_ready()
        return driveNumLst
    
    def cleanDisks(self, diskLabels):
        """
            Clean all available Disks

            TODO: If random disk population, need to handle shuffle sdx disks
            (ex: disk 4 w/o disk 3 installed will be sdc)
        """
        if not diskLabels:
            self.log.error("ERROR: Unavailable drive letter list!!!")
            raise Exception("ERROR: Unavailable drive letter list!!")

        self.log.info("Going to clean available disks...")
        for n in diskLabels.values():
            cleanCmd = "echo $'\n\nx\nz\ny\ny\n' | gdisk /dev/{}".format(n)
            self.log.debug("clean cmd: {}".format(repr(cleanCmd)))
            self.log.info("Clean {}".format(n))
            self.execute(cleanCmd)
            time.sleep(1)
        gdiskchkcmd = 'ps aux | grep -v grep | grep gdisk'
        count = 0
        while True:
            count += 1
            if count > 10:
                self.log.info("Reach the maximum 5 minutes wait for disk clean completion")
                break
            try:
                gdiskResult = self.run_on_device(gdiskchkcmd)
            except Exception as e:
                self.log.info("Retry: Unable to check gdisk status, exception: {}".format(repr(e)))
                time.sleep(30)
                continue
            if gdiskResult:
                self.log.info("gdisk is still ongoing...")
                self.log.debug("== {} ==".format(gdiskResult))
                time.sleep(30)
                continue
            else:
                break

    def config_nfs(self):
        """
            Configure NFS
        """
        try:
            self.get_to_page('Settings')
            self.click_element('settings_network_link')
            time.sleep(3)
            self.log.info('==> Configure nfs')
            self.wait_until_element_is_visible('css = #settings_networkNFS_switch+span .checkbox_container', timeout=5)
            nfs_status_text = self.get_text('css = #settings_networkNFS_switch+span .checkbox_container')
            if nfs_status_text == 'ON':
                self.log.info('NFS is enabled, status = {0}'.format(nfs_status_text))
            else:
                self.log.info('Turn ON NFS service')
                self.wait_until_element_is_clickable("css = #settings_networkNFS_switch+span .checkbox_container")
                self.click_element('css = #settings_networkNFS_switch+span .checkbox_container')
                time.sleep(2)
                nfs_status_text = self.get_text('css = #settings_networkNFS_switch+span .checkbox_container')
                if nfs_status_text == 'ON':
                    self.log.info('NFS is enabled.')
            time.sleep(2)
        except Exception as e:
            self.log.exception('FAILED: Unable to configure nfs!! Exception: {}'.format(repr(e)))     

    def config_nfsShare_access(self, sharename, writeAccess='ON'):
        """
            Configure nfs share to the access value passed by the argument
            
            @access: The access permission of nfs share
        """
        try:              
            self.log.info('==> Configure NFS share with {}'.format(writeAccess))
            self.get_to_page('Shares')
            time.sleep(2)
            self.log.info('Click Share')
            self.click_wait_and_check('shares_share_{}'.format(sharename), 'css=#shares_public_switch+span .checkbox_container', visible=True)
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            self.log.info('Switch the nfs to ON')
            self.wait_until_element_is_visible('css=#shares_nfs_switch+span .checkbox_container', timeout=15)
            nfs_status = self.get_text('css=#shares_nfs_switch+span .checkbox_container')
            if nfs_status == 'ON':
                self.log.info("nfs has been turned to ON, status = {}".format(nfs_status))
            else:
                self.click_element('css=#shares_nfs_switch+span .checkbox_container')
                self.log.info('nfs service is enabled to ON')
            time.sleep(2)
            self.click_element('shares_editNFS_link')
            time.sleep(2)
            # Set Host source IP to '*'
            hostSrc = self.get_text('shares_nfsHost_text')
            if hostSrc != '*':
                self.input_text('shares_nfsHost_text', '*', False)
            time.sleep(1)
            # Set Write access
            nfsWrite_status = self.get_text('css=#shares_write_switch+span .checkbox_container')
            if writeAccess == 'ON':
                if nfsWrite_status == 'ON':
                    self.log.info("nfs Write access has been turned to ON, status = {}".format(nfsWrite_status))
                else:
                    self.click_element('css=#shares_write_switch+span .checkbox_container')
                    self.log.info('nfs write access is enabled to ON')
            else: # set write access to OFF
                if nfsWrite_status == 'ON':
                    self.click_element('css=#shares_write_switch+span .checkbox_container')
                    self.log.info('nfs write access is disabled to OFF')
                else:
                    self.log.info("nfs Write access has been turned to OFF, status = {}".format(nfsWrite_status))
            self.click_element('shares_editNFSSave_button')
            time.sleep(5) # Waiting for Saving  to complete
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. exception: {}".format(repr(e)))      
               
    def read_write_access_check(self, shareName, prot, testFileSize):
        """
            Verify NFS/SAMBA protocol access
        """
        tmp_file = self.generate_test_file(kilobytes=testFileSize)
        time.sleep(10)
        if prot == 'SAMBA':
            self.log.info("---------- Start SAMBA write ----------")
            # delete_after_copy=True: only delete local generated file
            self.write_files_to_smb(files=tmp_file, share_name=shareName, delete_after_copy=False)
            self.log.info("---------- Complete SAMBA write ----------")
            # Wait for transfer finished
            time.sleep(10)
            self.samba_hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+shareName+'/', fileName='file0.jpg')
            self.log.info("---------- Start SAMBA read ----------")
            # delete_after_copy=True: delete local saved and remote files
            self.read_files_from_smb(files='file0.jpg', share_name=shareName, delete_after_copy=True)
            self.log.info("---------- Complete SAMBA read ----------")
            time.sleep(10)
            
        elif prot == 'NFS':
            # mount_share() contains
            # 1) config_nfs() and
            # 2) config_nfsShare_access(newShareName)
            nfs_mount = self.mount_share(share_name=shareName, protocol=self.Share.nfs)
            self.log.info("---------- Start NFS write ----------")
            try:
                # self.copy_file(source_file=tmp_file, dest_file='toNfsFile.bmp', destination_path=nfs_mount, user='admin')
                self.copyfile(src_file=tmp_file, dst_file='toNfsFile.bmp', dst_path=nfs_mount)
            except Exception as e:
                self.log.error("NFS write failed, exception: {}".format(repr(e)))
            self.log.info("---------- Complete NFS write ----------")
            time.sleep(10)
            # Hash check for two NFS files
            self.nfs_hash_check(srcfile=tmp_file, dstfile=os.path.join(nfs_mount, 'toNfsFile.bmp'))
            curdir = os.getcwd()
            self.log.info("---------- Start NFS read ----------")
            # self.copy_file(source_file=nfs_mount+'toNfsFile.bmp', dest_file='fromNfsFile.bmp', destination_path=curdir, user='admin')
            self.copyfile(src_file=os.path.join(nfs_mount, 'toNfsFile.bmp'), dst_file='fromNfsFile.bmp', dst_path=curdir)
            self.log.info("---------- Complete NFS read ----------")
            self.log.info("Unmount NFS share: {}".format(nfs_mount))
            self.unmount_share(share_name=shareName, protocol=self.Share.nfs)
            time.sleep(10)
        else:
            self.log.info('Protocol {} is not supported'.format(prot))

    def copyfile(self, src_file, dst_file, dst_path):
        src_path = os.path.abspath(src_file)
        dst_path = os.path.join(dst_path, dst_file)
        try:
            shutil.copyfile(src_path, dst_path)
        except Exception as e:
            self.log.error("FAILED: Unable to copy file from {} to {}, exception: {}".format(src_path, dst_path, repr(e)))
            raise
        self.log.info("PASS: Succeed to copy {} to {}".format(src_file, dst_path))

    def nfs_hash_check(self, srcfile, dstfile):
        hashsrc = self.checksum_files_on_workspace(dir_path=srcfile, method='md5')
        hasdst = self.checksum_files_on_workspace(dir_path=dstfile, method='md5')
        if hashsrc == hasdst:
            self.log.info('PASS: NFS MD5 hash check test')
        else:
            self.log.error('FAILED: NFS MD5 hash check test')

    def samba_hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName) 
        if hashLocal == hashUUT:
            self.log.info('PASS: Samba MD5 hash check test')
        else:
            self.log.error('FAILED: Samba MD5 hash check test')

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            # visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, timeout=5, retry=3):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def takeScreenShot(self, logname):
        """
            Take Screen Shot
        """
        filename = os.path.basename(inspect.stack()[0][1])
        filename = os.path.splitext(filename)[0] + '_error_{}.png'.format(logname)
        self._sel.capture_browser_screenshot(filename)

nasRaidDowngrade2DrviesRaid1()