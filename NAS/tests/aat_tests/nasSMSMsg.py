""" NAS SMS Messages (MA-23)

    @Author: lin_ri
    wiki URL: http://wiki.wdc.com/wiki/NAS_SMSMessages
    
    Automation: Partial
                1) Need to input the mobile phone number you'd like to receive SMS messages
                2) Verify 2 SMS messages in your mobile phone if you do receive them 
    
    Manual:
            I. Uses kotsms which WD TW registered (www.kotsms.com.tw) 
            
               1) No need to register the account 
               
               Assume your cell phone number is 0912345678
               
               API: http://202.39.48.216/kotsmsapi-1.php?username=WDTW&password=WDTWSEV&dstaddr=8860912345678&smbody=MSG test fromNAS
               
               2) You have to verify if you get two SMS messages from 0961-238-382(Manual)
                  a) Test SMS
                     This is a SMS test.
                     Your WDMyCloudEx4
                  b) Alert Event SMS
                     Restore Config Failed
                     Your WDMyCloudEx4
               
            II. Use clickatell, Need to use self.set_clickatell_sms_notification()
              
            Register an account manually and pass them to set_sms_notification parameters
            1) Go to www.clickatell.com
            2) Select "Developers' Central" and "International"
            3) Fill all of your information including email and mobile#
            4) Check your email and click the authentiation link to complete the registration
            5) Go to https://central.clickatell.com/api/manage
               Login with your username/password and ClientID
            6) You can see the values you have to pass to the set_sms_notification()
            Ex: 
               self.set_clickatell_sms_notification(vendor='Clickatell', username='TestUser', userpwd='TestPassword', apiId=3148203, mobile=886123456789, msg="Hello World")
            7) You have to verify if you get two SMS messages
               a) Test SMS
               b) Configuration failed message
    
    Note: Your SMS configuration will be deleted at the end of the test
                   
    Status: Local Lightning FW 2.00.133: Pass
""" 

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import subprocess
import time
from selenium.webdriver.common.keys import Keys
import os

picture_counts = 0

class nasSMSMsg(TestClient):
    
    def run(self):
        self.log.info('######################## NAS SMS Messages TESTS START ##############################')
        try:
            self.test_valid_ip()
            #self.set_sms_notification(username='TestUser', userpwd='TestPassword', mobile=886123456789, msg="Hello World")
            # Set userpwd to WDTWSEVX not WDTWSEV to save SMS money
            self.set_sms_notification(vendor='kotsms', username='WDTW', userpwd='WDTWSEVX', mobile='886912345678', msg="MSG from WD MyCloud EX4 NAS!!!")
            #self.set_clickatell_sms_notification(username='TestUser', userpwd='TestPassword', apiId=3148203, mobile=886912345678, msg="Hello World")
            #self.set_clickatell_sms_notification(username="rickhau", userpwd="IbcKRSFbECdTHf", apiId=3521620, mobile=886912345678, msg="WDMYCLOUDEX4-NAS SMS MSG")
            self.trigger_alerts(alertcode='1020')  # Generate 'System Shutting down' alert
            self.close_webUI()
            self.log.info("Please check your mobile if you get two SMS messages: 1) Test SMS 2) Alert SMS(code: 1020)")
        except Exception as e:
            self.log.error("FAILED: NAS SMS Messages Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("Conditional PASS: NAS SMS Messages Test Pass, You have to check your mobile phone for 2 SMS msgs!!")
            self.delete_sms_notification()
        self.log.info('######################## END NAS SMS Messages TESTS ##############################')

    def test_valid_ip(self):
        """
            Check if IP is valid
        """
        local_ip = self.get_local_ip()
        if local_ip is not None:
            self.log.info("PASS: Valid local IP - {}".format(local_ip))
        else:
            self.log.error("FAILED: Invalid local IP - {}".format(local_ip))

    def set_sms_notification(self, vendor='kotsms', username='WDTW', userpwd='WDTWSEV',
                             mobile='886912345678', msg="MSG from WD MyCloud EX4 NAS!!!"):
        """
            Configure SMS notification
            
            @username: kotsms's account name
            @userpwd: kotsms's account password
            @mobile: Your cellphone filled in Clickatell
            @msg: Message you'd like to transmit 
        """
        try:
            self.log.info("==> Configure SMS notification")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            # self.wait_and_click('settings_notifications_link')
            self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
            self.log.info("Slide the notification display bar to All")
            time.sleep(1)
            self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
            self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
            time.sleep(2)
            self.log.info("====== Turn on SMS notification ======")
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                          'settings_notificationsSms_link', visible=True)
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'ON':
                raise Exception("ERROR: Fail to enable SMS switch button")
            self.log.info("SMS notification is enabled to {}".format(sms_status))
            time.sleep(2)
            self.log.info("====== Click SMS configure ======")
            self.click_wait_and_check('settings_notificationsSms_link',
                                      'smsDiag_title', visible=True)
            # self.wait_and_click('css=#settings_notificationsSms_link > span._text', timeout=15)
            if self.is_element_visible('settings_notificationsSmsDel_button', 10):
                self.log.info("Clean existing SMS configuration")
                self.click_element("settings_notificationsSmsDel_button")
                time.sleep(3)
            time.sleep(2)
            self.log.info("Input SMS Provider Name: {}".format(vendor))
            self.input_text_check('settings_notificationsSmsName_text', vendor, False)
            SMS_URL = "http://202.39.48.216/kotsmsapi-1.php?username={}&password={}&dstaddr={}&smbody={}".format(username, userpwd, mobile, msg)
            self.log.info("SMS URL: {}".format(SMS_URL))
            time.sleep(2)
            self.input_text_check('settings_notificationsSmsUrl_text', SMS_URL, False)
            self.log.info("SMS setting is configured successfully...")
            self.click_wait_and_check('settings_notificationsSmsNext1_button',
                                      'settings_notificationsSmsBack2_button', visible=True)
            time.sleep(2)
            self.log.info("====== Configure SMS link settings ======")
            self.click_link_element('id_alias0', 'Username')
            self.click_link_element('id_alias1', 'Password')
            self.click_link_element('id_alias2', 'Phone number')
            self.click_link_element('id_alias3', 'Message content')
            self.click_wait_and_check('settings_notificationsSmsSave_button', visible=False)
            time.sleep(2)
            self.log.info("Click Test SMS button to send out SMS test message")
            self.click_element('settings_notificationsSmsTest_button')
            time.sleep(2)
            # self.wait_until_element_is_visible('popup_ok_button')
            count = 1
            while count <= 3:
                if self.is_element_visible('popup_ok_button', 30):
                    break
                else:
                    time.sleep(1)
                    self.log.info("{}: Re-click Test SMS button".format(count))
                    self.click_element('settings_notificationsSmsTest_button')
                    count += 1
            sms_test = self.get_text('css=div#error_dialog_message')
            self.log.info("{}".format(sms_test))
            self.click_wait_and_check('popup_ok_button', visible=False)
            time.sleep(1)
            self.click_wait_and_check('settings_notificationsSmsSetSave_button', visible=False)
        except Exception as e:
            self.log.error("ERROR: Fail to set SMS notification message, exception: {}".format(repr(e)))
        
    def set_clickatell_sms_notification(self, vendor='Clickatell', username='ricklin', userpwd='SDCHgCJDYORefR',
                                        apiId=3520459, mobile=886912345678, msg="MSG from WD NAS"):
        """
            Configure SMS notification
            
            @username: Clickatell's account name (ex: Fituser)
            @userpwd: Clickatell's account password (ex: Fituser102013)
            @apiId: Clickatell's API ID (3442171)
            @mobile: Your cellphone filled in Clickatell
            @msg: Message you'd like to transmit (It seems that you can not send out the msg unless you purchase)
        """
        self.log.info("==> Configure SMS notification")
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
        self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
        self.log.info("Slide the notification display bar to All")
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
        time.sleep(1)
        self.log.info("Turn on SMS notification")
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_element('css=#settings_notificationsSms_switch+span .checkbox_container')
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')            
        self.log.info("SMS notification is enabled to {}".format(sms_status))
        time.sleep(2)
        self.log.info("Click SMS configure")
        # self.click_wait_and_check('css=#settings_notificationsSms_link > span._text', 'settings_notificationsSmsCancel1_button', visible=True)
        self.click_wait_and_check('settings_notificationsSms_link',
                                  'settings_notificationsSmsCancel1_button', visible=True)
        if self.is_element_visible('settings_notificationsSmsDel_button', 30):
            self.log.info("Clean existing SMS configuration")
            self.click_element("settings_notificationsSmsDel_button")
            time.sleep(2)
        self.log.info("Input SMS Provider Name: {}".format(vendor))
        self.input_text_check('settings_notificationsSmsName_text', vendor, False)
        SMS_URL = "http://api.clickatell.com/http/sendmsg?user={}&password={}&api_id={}&to={}&text={}".format(username, userpwd, apiId, mobile, msg)
        self.log.info("SMS URL: {}".format(SMS_URL))
        self.input_text_check('settings_notificationsSmsUrl_text', SMS_URL, False)
        self.click_wait_and_check('settings_notificationsSmsNext1_button',
                                  'settings_notificationsSmsSave_button', visible=True)
        time.sleep(1)
        self.log.info("Configure SMS settings")
        self.click_link_element('id_alias0', 'Username')
        self.click_link_element('id_alias1', 'Password')
        self.click_link_element('id_alias2', 'Phone number')
        self.click_link_element('id_alias3', 'Message content')
        self.click_link_element('id_alias4', 'Message content')
        # self.click_element('id_alias0')
        # self.click_element('link=Username')
        # self.click_element('id_alias1')
        # self.click_element('link=Password')
        # self.click_element('id_alias2')
        # self.click_element('link=Other')
        # self.click_element('id_alias3')
        # self.click_element('link=Phone number')
        # self.click_element('id_alias4')
        # self.click_element('link=Message content')
        self.click_element('settings_notificationsSmsSave_button')
        time.sleep(2)
        self.log.info("Click Test SMS button to send out SMS test message")
        self.click_element('settings_notificationsSmsTest_button')
        time.sleep(2)
        self.wait_until_element_is_visible('popup_ok_button', 30)
        sms_test = self.get_text('css=div#error_dialog_message')
        self.log.info("{}".format(sms_test))
        self.click_element('popup_ok_button')
        time.sleep(1)
        self.click_element('settings_notificationsSmsSetSave_button')

    def delete_sms_notification(self):
        """
            Delete SMS configuration
        """
        self.log.info("==> Clear SMS notification")
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
        # self.wait_and_click('settings_notifications_link')
        self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
        time.sleep(2)
        self.log.info("Turn on SMS notification")
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_element('css=#settings_notificationsSms_switch+span .checkbox_container')
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')            
        self.log.info("SMS notification is enabled to {}".format(sms_status))
        time.sleep(2)
        self.log.info("Click SMS configure")
        # self.click_element('css=#settings_notificationsSms_link > span._text')
        self.click_element('settings_notificationsSms_link')
        if self.is_element_visible('settings_notificationsSmsDel_button', 30):
            self.log.info("Clean existing SMS configuration")
            self.click_wait_and_check("settings_notificationsSmsDel_button", visible=False)
            time.sleep(3)
            self.log.info("Close SMS Setting dialog window")
            self.click_wait_and_check('css=#settings_notificationsSmsCancel1_button', visible=False)
            self.log.info("Succeed to clear SMS notification")
            self.log.info("SMS notification will be turned to OFF due to no configuration...")
        time.sleep(2)

    def trigger_alerts(self, alertcode='0001'):
        """
            Trigger Critical Alerts which does not require parameters
            
            @alertcode: '0001' the alert code you want to trigger
            
            If the alert needs arguments, the command requires '-p PARM1,PARM2' after -a
            Sample alert codes:
            Critical
                0001        System Over Temperature
                0224        Replace Drive with Red Light
                0029        Fan Not Working
                1020        System Shutting down                
            Warning
                1002        Network Link Down
            Information
                2004        Firmware Update successfully
        """
        if alertcode == '':
            alertcode = '0001'
        self.execute('alert_test -R')
        self.execute('alert_test -a {} -f'.format(alertcode))
        resp = self.execute('alert_test -g')
        alerts = resp[1].split('---->')
        for a in alerts[1:]:
            alertnum = a.split('\n')[2]
            alertmsg = a.split('\n')[6]
            self.log.info('{}, {} is generated.'.format(alertnum, alertmsg)) # print the alertMSG
        self.log.info("Verify: Please check your cell phone if you receive alert SMS")

    def getstatusoutput(self, cmd):
        """
            Execute the command
            @cmd: command you want to run
        """
        pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, universal_newlines=True)
        output = "".join(pipe.stdout.readlines())
        sts = pipe.returncode
        if sts is None:
            sts = 0
        return sts, output

    def get_local_ip(self):
        """
            Acquire local ip
        """
        cmdstring = 'ifconfig | grep 192.168'
        status, output = self.getstatusoutput(cmdstring)
        ips = output.strip().split()
        if ips[0].strip() == 'inet':
            ip = ips[1].strip()
        else:
            ip = None
        return ip

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, timeout=5, retry=3):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def input_text_check(self, locator, text, do_login=True, check_browser=True, check_page=True, retry=3):
        """
            This method is mainly to verify if the text field is filled with correct data
        """
        while retry >= 0:
            try:
                self.input_text(locator, text, do_login, check_browser, check_page)
                textbox = self.get_value(locator)
            except Exception:
                if retry == 0:
                    raise Exception("Failed to input text with maximum {} retries".format(retry))
                self.log.info("Input text did not succeed, remaining {} retry".format(retry))
                retry -= 1
                time.sleep(1)
                continue
            else:
                if textbox != text:
                    self.log.info("Input value is not consistent, remaining {} retries".format(retry))
                    retry -= 1
                    time.sleep(1)
                    continue
                break

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1

nasSMSMsg()