'''
Create on Dec 1, 2014

@Author: lo_va

Objective: To verify that volume encryption works as expected.
Wiki URL: http://wiki.wdc.com/wiki/Volume_Encryption
'''
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
from global_libraries import CommonTestLibrary as ctl
from global_libraries import wd_exceptions
import time

password = 'password'
password1 = 'fituser99'
password2 = 'fituser98'
password3 = 'fituser97'
password4 = 'fituser96'
filesize = 10240

class volumeEncryption(TestClient):

    def run(self):

        self.set_web_access_timeout()
        if self.get_drive_count() != 4:
            raise wd_exceptions.InvalidDeviceState('Device is not with 4 drives, cannot test Volume Encryption Test case!!!')
        
        # Test Encrypting Drives
        self.createEncrypDrives(Raidtype='4JBOD')
        self.verifyCreation(Drives=4)
        self.verifyNoneShares()
        self.mountVolumes(volumes=4)
        self.createEncrypShare(volumeid=[1,2,3,4])
        self.accessEncrypShares(volumeid=[1,2,3,4])
        self.createEncrypDrives(Raidtype='3JBOD')
        self.verifyCreation(Drives=3)
        self.mountVolumes(volumes=3)
        self.createEncrypShare(volumeid=[1,2,3,4])
        self.accessEncrypShares(volumeid=[1,2,3,4])

        # Test Enable Auto-mount
        self.enableAutoMount(volumeid=[1], volumes=3)
        time.sleep(5)
        self.reboot(500)
        time.sleep(5)
        self.verifyMountVolume(volumeid=[1], mount=True)
        self.verifyMountVolume(volumeid=[2,4], mount=False)
        self.accessEncrypShares(volumeid=[1])

        # Test Disable Auto-mount
        self.enableAutoMount(volumeid=[2,4], volumes=3)
        self.disableAutoMount(volumeid=[1], volumes=3)
        time.sleep(5)
        self.reboot(500)
        time.sleep(5)
        self.verifyMountVolume(volumeid=[2,4], mount=True)
        self.verifyMountVolume(volumeid=[1], mount=False)
        self.accessEncrypShares(volumeid=[2,4])
        
        # Test Encryption in mixed RAID mode
        self.createEncrypDrives(Raidtype='Raid5Mix')
        self.verifyCreation(Drives=1)
        self.verifyMountVolume(volumeid=[2], mount=True)
        self.verifyMountVolume(volumeid=[1], mount=False)
        self.mountVolumes(volumes=1)
        self.createEncrypShare(volumeid=[1,2])
        self.accessEncrypShares(volumeid=[1,2])
    
    def tc_cleanup(self):
        self.set_web_access_timeout('5')
        # Configure RAID back to RAID1
        raid_type = self.get_raid_mode()
        if raid_type == 'JBOD' or raid_type == 'RAID5':
            self.configure_raid(raidtype='RAID1')
        
    def createEncrypDrives(self, Raidtype):
        try:
            drives_mapping = self.get_drive_mappings()
            drive_name = ctl.get_drive_names(drives_mapping)
            if Raidtype == '3JBOD':
                self.log.info('Start to Create 4 JBOD w/ 3 Encryption Volumes!')
                self.run_on_device('kill_running_process; diskmgr -D{}'.format(drive_name))
                self.run_on_device('diskmgr -v1 -s30 -70 -P %s -m1 -f3 -k%s -v2 -s30 -70 -P %s -m1 -f3 -k%s -v3 -s30 -m1 -f3 -k%s \
                -v4 -s30 -70 -P %s -m1 -f3 -k%s --kill_running_process --load_module -n' % (password1,drives_mapping.get(1),password2,drives_mapping.get(2),\
                                                                                              drives_mapping.get(3),password4,drives_mapping.get(4)))
                self.log.info('Create 4 JBOD w/ 3 Encryption Volumes finished!')

            if Raidtype == '4JBOD':
                self.log.info('Start to Create 4 JBOD w/ 4 Encryption Volumes!')
                self.run_on_device('kill_running_process; diskmgr -D{}'.format(drive_name))
                self.run_on_device('diskmgr -v1 -s30 -70 -P %s -m1 -f3 -k%s -v2 -s30 -70 -P %s -m1 -f3 -k%s -v3 -s30 -70 -P %s -m1 -f3 -k%s \
                -v4 -s30 -70 -P %s -m1 -f3 -k%s --kill_running_process --load_module -n' % (password1,drives_mapping.get(1),password2,drives_mapping.get(2),\
                                                                                             password3,drives_mapping.get(3),password4,drives_mapping.get(4)))
                self.log.info('Create 4 JBOD w/ 4 Encryption Volumes finished!')

            if Raidtype == 'Raid5Mix':
                self.log.info('Start to Create Raid5 w/ Encryption Volume and Span w/o Encryption Volume!')
                self.run_on_device('kill_running_process; diskmgr -D{}'.format(drive_name))
                self.run_on_device('diskmgr -v1 -s30 -70 -P %s -m5 -f3 -k%s --assume-clean 0 -v2 -3 -s30 -m2 -f3 -k%s \
                --kill_running_process --load_module -n' % (password1,drive_name,drive_name))
                self.log.info('Create Raid5 w/ Encryption Volume and Span w/o Encryption Volume finished!')
        except Exception, ex:
            self.log.exception('Failed to create Encryption Volumes!!')

    def verifyCreation(self, Drives):
        self.log.info('Start to verify Encryption creation result!')
        self.get_to_page('Storage')
        if self.is_element_visible('storage_volumeEncryption_link', 15):
            self.click_element('storage_volumeEncryption_link')
            try:
                self.wait_until_element_is_visible('VE_List')
            except:
                self.log.info('Sometimes Click button will be hanged, try 1 more time.')
                self.click_element('storage_volumeEncryption_link')
                self.wait_until_element_is_visible('VE_List')
            self.wait_until_element_is_visible('VE_List')
            row = self.get_text('VE_List')
            if not row:
                vol = self.get_text('VE_List')

            if Drives == 1:
                if 'Volume_1' in row:
                    self.log.info('Volume_1 Encryption creation successful!!')
                else:
                    self.log.error('Volume_1 Encryption creation failed!!')

            if Drives == 3:
                if 'Volume_1' and 'Volume_2' and 'Volume_4' in row:
                    self.log.info('Volumes Encryption creation successful!!')
                else:
                    self.log.error('Volumes Encryption creation failed!!')
                    
            if Drives == 4:
                if 'Volume_1' and 'Volume_2' and 'Volume_3' and 'Volume_4' in row:
                    self.log.info('Volumes Encryption creation successful!!')
                else:
                    self.log.error('Volumes Encryption creation failed!!')
        else:
            self.log.error('Volume Encryption page not exist!!')
        #self.close_webUI()

    def verifyNoneShares(self):
        try:
            self.get_to_page('Shares')
            s1 = self.is_element_visible('shares_share_Public', 2)
            s2 = self.is_element_visible('shares_share_SmartWare', 2)
            s3 = self.is_element_visible('shares_share_TimeMachineBackup', 2)
            if s1 or s2 or s3:
                self.log.error('Failed!! Shares is in the list!!')
            else:
                self.log.info('PASS!! Shares is not in the list.')
            #self.close_webUI()
        except Exception, ex:
            self.log.exception('Error to check Shares List!')

    def mountVolumes(self, volumes):
        try:
            self.log.info('Start to mount Encryption Volumes')
            self.get_to_page('Storage')
            self.click_element('storage_volumeEncryption_link')
            try:
                self.wait_until_element_is_visible('VE_List')
            except:
                self.log.info('Sometimes Click button will be hanged, try 1 more time.')
                self.click_element('storage_volumeEncryption_link')
                self.wait_until_element_is_visible('VE_List')

            if volumes >= 1:
                self.click_element('//tr[@id=\'row1\']/td[4]/div')
                self.input_text('f_ve_mount_1st_pwd', password1, False)
                self.click_wait_and_check('ve_apply_button_1')
                self.wait_icon_loading()

            if volumes >= 3:
                self.click_element('//tr[@id=\'row2\']/td[4]/div')
                self.input_text('f_ve_mount_1st_pwd', password2, False)
                self.click_wait_and_check('ve_apply_button_1')
                self.wait_icon_loading()

            if volumes == 3:
                self.click_element('//tr[@id=\'row3\']/td[4]/div')
                self.input_text('f_ve_mount_1st_pwd', password4, False)
                self.click_wait_and_check('ve_apply_button_1')
                self.wait_icon_loading()

            if volumes == 4:
                self.click_element('//tr[@id=\'row3\']/td[4]/div')
                self.input_text('f_ve_mount_1st_pwd', password3, False)
                self.click_wait_and_check('ve_apply_button_1')
                self.wait_icon_loading()

                self.click_element('//tr[@id=\'row4\']/td[4]/div')
                self.input_text('f_ve_mount_1st_pwd', password4, False)
                self.click_wait_and_check('ve_apply_button_1')
                self.wait_icon_loading()
            #self.close_webUI()
        except Exception, ex:
            self.log.exception('Mount Encryption Volumes failed!')

    def wait_icon_loading(self):
        self.wait_until_element_is_visible('icon_loading')
        timer = Stopwatch(timer=100)
        timer.start()
        while True:
            VE_wait = self.element_find('VE_Diag_Wait')
            VE_wait_attr = VE_wait.get_attribute('style')
            if 'none' in VE_wait_attr:
                self.log.info('Loading finished')
                time.sleep(1)
                break
            else:
                if timer.is_timer_reached():
                    raise Exception.ActionTimeout('Loading Timeout')
                else:
                    time.sleep(2)

    def verifyMountVolume(self, volumeid, mount=True):
        if mount == True:
            try:
                self.get_to_page('Storage')
                self.click_element(E.STORAGE_RAID)
                if self.is_element_visible('css = div.flexigrid', 2):
                    vol = self.get_text('css = div.flexigrid')
                    if not vol:
                        vol = self.get_text('css = div.flexigrid')
                for x in volumeid:    
                    if 'Volume_%s' % x in vol:
                        self.log.info('Volume_%s Mount Successed!!' % x)
                    else:
                        self.log.error('Volume_%s Mount Failed!!' % x)
            except Exception, ex:
                self.log.exception('Volumes Mount not normally!!')
            self.close_webUI()
                
        if mount == False:
            vol = ''
            try:
                self.get_to_page('Storage')
                self.click_element(E.STORAGE_RAID)
                if self.is_element_visible('css = div.flexigrid', 2):
                    vol = self.get_text('css = div.flexigrid')
                    if not vol:
                        vol = self.get_text('css = div.flexigrid')
                for x in volumeid:
                    if 'Volume_%s' % x in vol:
                        self.log.error('Failed!! Volume_%s is Mounted!!' % x)
                    else:
                        self.log.info('PASS!! Volume_%s is not Mounted!!' % x)
            except Exception, ex:
                self.log.exception('Volumes Mount not normally!!')
            self.close_webUI()

    def createEncrypShare(self, volumeid):
        try:
            self.log.info('Start to create Encryption Shares')
            self.get_to_page('Shares')

            for x in volumeid:
                time.sleep(3)
                self.click_button(E.SHARES_CREATE_SHARE)
                self.click_element('shares_volume_select')
                if not self.is_element_visible('css = div.sRight.wd_select_r_down'):
                    self.log.info('Sometimes Click button will be hanged, try 1 more time.')
                    self.click_element('shares_volume_select')
                self.click_element('link=Volume_%s' % x)
                self.input_text(E.SHARES_ADD_NAME, 'Volume%s' % x, False)
                self.input_text(E.SHARES_ADD_DESCRIPTION, 'testvolume%s' % x, False)
                self.click_element(E.SHARES_ADD_SAVE)
                self.wait_until_element_is_not_visible(E.UPDATING_STRING)
                #self.update_share_network_access(share_name='Volume%s' % x, public_access=True)
                get_share_list = self.get_all_shares()
                share_list = self.get_xml_tags(get_share_list, tag='share_name')
                if 'Volume%s' % x in share_list:
                    self.log.info('Volume%s created' % x)
                else:
                    raise Exception('Volume%s create failed' % x)
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Create Encryption Shares failed!')

    def accessEncrypShares(self, volumeid):
        self.log.info('Start to verify Encryption Shares access')
        for x in volumeid:
            try:
                self.log.info('Access Volume%s' % x)
                self.write_files_to_smb(files=self.generate_test_file(filesize), share_name='Volume%s' % x)
                time.sleep(8)
            except Exception, ex:
                self.log.exception('Access Encryption Shares volume%d failed!' % x)

    def decryptingEncryp(self):
        pass

    def enableAutoMount(self, volumeid, volumes):
        try:
            self.log.info('Start to Enable Auto_mount function')
            self.get_to_page('Storage')
            self.click_element('storage_volumeEncryption_link')
            try:
                self.wait_until_element_is_visible('VE_List')
            except:
                self.log.info('Sometimes Click button will be hanged, try 1 more time.')
                self.click_element('storage_volumeEncryption_link')
                self.wait_until_element_is_visible('VE_List')
            for x in volumeid:
                if x is not 4:
                    time.sleep(1)
                    self.click_element('css = #row%s > td > div' % x)
                    self.click_element('VE_Modify_Button')
                    self.input_text('f_modify_ve_pwd', eval(password+str(x)), False)
                    self.click_element('ve_apply_button_3')
                    if self.element_find('f_modify_ve_auto_mount').is_selected():
                        self.click_element('ve_apply_button_4')
                    else:
                        self.click_element('css = .LightningCheckbox #f_modify_ve_auto_mount+span')
                        self.click_element('ve_apply_button_4')
                        time.sleep(10)
                        while self.is_element_visible('icon_loading'):
                            time.sleep(5)

                if x == 4:
                    if volumes == 3:
                        self.click_element('css = #row3 > td > div')
                    if volumes == 4:
                        self.click_element('css = #row4 > td > div')
                    self.click_element('VE_Modify_Button')
                    self.input_text('f_modify_ve_pwd', password4, False)
                    self.click_element('ve_apply_button_3')
                    if self.element_find('f_modify_ve_auto_mount').is_selected():
                        self.click_element('ve_apply_button_4')
                    else:
                        self.click_element('css = .LightningCheckbox #f_modify_ve_auto_mount+span')
                        self.click_element('ve_apply_button_4')
                        time.sleep(10)
                        while self.is_element_visible('icon_loading'):
                            time.sleep(5)
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Failed to enabled Auto_mount Function!')

    def disableAutoMount(self, volumeid, volumes):
        try:
            self.log.info('Start to Disable Auto_mount function')
            self.get_to_page('Storage')
            self.click_element('storage_volumeEncryption_link')
            try:
                self.wait_until_element_is_visible('VE_List')
            except:
                self.log.info('Sometimes Click button will be hanged, try 1 more time.')
                self.click_element('storage_volumeEncryption_link')
                self.wait_until_element_is_visible('VE_List')
            for x in volumeid:
                if x is not 4:
                    self.click_element('css = #row%s > td > div' % x)
                    self.click_element('VE_Modify_Button')
                    self.input_text('f_modify_ve_pwd', eval(password+str(x)), False)
                    self.click_element('ve_apply_button_3')
                    if self.element_find('f_modify_ve_auto_mount').is_selected():
                        self.click_element('css = .LightningCheckbox #f_modify_ve_auto_mount+span')
                        self.click_element('ve_apply_button_4')
                    else:
                        self.click_element('ve_apply_button_4')

                if x == 4:
                    if volumes == 3:
                        self.click_element('css = #row3 > td > div')
                    if volumes == 4:
                        self.click_element('css = #row4 > td > div')
                    self.click_element('VE_Modify_Button')
                    self.input_text('f_modify_ve_pwd', password4, False)
                    self.click_element('ve_apply_button_3')
                    if self.element_find('f_modify_ve_auto_mount').is_selected():
                        self.click_element('css = .LightningCheckbox #f_modify_ve_auto_mount+span')
                        self.click_element('ve_apply_button_4')
                    else:
                        self.click_element('ve_apply_button_4')
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Failed to disabled Auto_mount Function!')

# This constructs the volumeEncryption() class, which in turn constructs TestClient() which triggers the volumeEncryption.run() function
volumeEncryption()