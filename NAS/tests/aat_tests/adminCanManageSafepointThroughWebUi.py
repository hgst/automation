"""
Created on July 27th, 2015

@author: tran_jas

## @brief create manage safepoint through web UI

"""

from sequoiaAPI.src.sequoia import Sequoia
from seleniumAPI.src.seq_ui_map import Elements as ET


class Safepoint(Sequoia):

    def run(self):

        try:
            self.seq_accept_eula()
            self.click_element(ET.SAFEPOINT_LINK)
            self.click_element(ET.ADD_SAFEPOINT_BUTTON)
            self.click_element(ET.CANCEL_CREATE_SAFEPOINT_BUTTON)
            self.click_element(ET.RESTORE_SAFEPOINT_BUTTON)
            self.click_element(ET.CANCEL_CREATE_SAFEPOINT_BUTTON)

        except Exception, ex:
            self.log.exception('Failed to complete Admin Can Manage Safepoint Through WebUI test case \n' + str(ex))

Safepoint()
