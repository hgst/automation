'''
Created on Feb 10 2015

@author: Carlos Vasquez

Objective:  To verify that the System Restore is functional
'''

import os
import time
from testCaseAPI.src.testclient import TestClient

fourBayNAS = ['LT4A', 'BNEZ', 'BWZE']
SYSTEMRESTORE = 'Factory Restore Succeeded'

class systemRestore(TestClient):   
    def run(self):
        self.delete_all_alerts()
        numberDrives = self.uut[self.Fields.number_of_drives]
        self.click_element(self.Elements.SETTINGS_UTILITY_RESTORE)
        self.systemDiagnostic_CheckResult('RESTORE', numberDrives)
        
    def systemDiagnostic_CheckResult(self, diagnosticType, numberDrives):
        self.log.info('System Restore Test')
        
        global modelNumber
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')
        
        self.wait_until_element_is_visible('popup_apply_button', 5)
        self.click_button('popup_apply_button') 
        
        self.wait_until_element_is_not_visible('WDLabelBodyDialogue')
            
        if modelNumber in fourBayNAS:
            time.sleep(240)
        else:
            time.sleep(120)
        
        # wait for system to be ready
        count = 0
        while self.is_ready() == False:
            time.sleep(60)
            count += 1
            if count > 10:
                self.log.error('FAILED: Unit not ready after 10 minutes')
                break
        self.accept_eula(False)
        
        restoreAlert = self.get_xml_tags(self.get_alerts()[1], 'title')
        
        if SYSTEMRESTORE in restoreAlert:
            self.log.info('SUCCESS: System Restore is functional')
        else:
            self.log.error('FAILED: System Restore is NOT functional')
        
systemRestore()