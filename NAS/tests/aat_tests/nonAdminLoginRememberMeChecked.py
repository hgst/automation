"""
title           :nonAdminLoginRememberMeChecked.py
description     :To verify if the non system admin credentials are kept after 'remember me' is checked on login page, after logout the system.
author          :yang_ni
date            :2015/04/20
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
from testCaseAPI.src.testclient import Elements
from global_libraries.testdevice import Fields

class nonAdminLoginRememberMeChecked(TestClient):

    def run(self):
            if self.get_model_number() in ('GLCR',):
                self.log.info("=== Glacier de-feature does not support user login, skip the test ===")
                return

            testname = 'My cloud other (user) login - Remember me checked'
            self.start_test(testname)

            #Delete all users, create a non-admin user and change uut credentials
            self.setup()

            try:
                self.access_webUI(do_login=False)
                time.sleep(5)

                self.input_text(Elements.LOGIN_USERNAME, 'user1')
                self.input_password(Elements.LOGIN_PASSWORD, 'password')

                self.select_checkbox(Elements.LOGIN_REMEMBER_ME)
                self.checkbox_should_be_selected(Elements.LOGIN_REMEMBER_ME)

                self.click_button(Elements.LOGIN_BUTTON)
                time.sleep(8)
                self.wait_until_element_is_clickable('logout_toolbar',15)
                self.click_button('logout_toolbar')
                self.wait_until_element_is_visible(Elements.TOOLBAR_LOGOUT_LOGOUT,15)
                self.click_element(Elements.TOOLBAR_LOGOUT_LOGOUT)
                time.sleep(15)

                username = self.element_find('login_userName_text').get_attribute('value')
                self.log.info("The username after logout is : {}".format(username))

                chkbox = self.is_checkbox_selected(Elements.LOGIN_REMEMBER_ME)
                self.log.info("Remember Me checkbox is checked? : {}".format(chkbox))

                if (username == 'user1') and (chkbox == True):
                    self.pass_test(testname, 'User1 successfully login with Remember Me Checked')
                else:
                    self.fail_test(testname, 'Name of user1 or remember me checkbox status are not kept after logging out')

            except Exception as e:
                    self.fail_test(testname, 'Non admin failed to login with Remember Me Checked: {}'.format(e))

            finally:
                #Reset back the web username and password
                self.uut[Fields.web_username] = 'admin'
                self.uut[Fields.web_password] = ''


    # # @Delete all users, create a non-admin user and change uut credentials
    def setup(self):

            #Delete all the users first
            self.log.info('Deleting all users ...')
            self.delete_all_users()

            time.sleep(3)

            #Create a non admin user
            self.log.info('Creating a non admin user ...')
            non_admin_user = self.create_user(username='user1', password='password', is_admin=False)

            if non_admin_user == 0:
                self.log.info('user1 was created successfully')
            else:
                self.log.error('user1 was not created successfully')

            #Set uut Fields username/password to user1/password
            self.uut[Fields.web_username] = 'user1'
            self.uut[Fields.web_password] = 'password'

# This constructs the nonAdminLoginRememberMeChecked() class, which in turn constructs TestClient() which triggers the nonAdminLoginRememberMeChecked.run() function
nonAdminLoginRememberMeChecked()