"""
Create on Jan 24, 2016
@Author: lo_va
Objective:  Verify Config Manager module is loaded in firmware
            Verify confmgr daemon is running
            Refer KAM-257 (M2 feature) for detail requirement and implementations
wiki URL: http://wiki.wdc.com/wiki/Load_Conf_Manager_in_firmware
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadConfManager(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        confmanager_daemon = ''
        confmanager_files_list = ''
        try:
            confmanager_daemon = self.execute('/etc/init.d/configmgrd status')[1]
            confmanager_files_list = self.execute('find / -name *configmgr*')[1]
            self.log.debug('Conf Manager Daemon: {}'.format(confmanager_daemon))
            self.log.debug('Conf Manager find files list: {}'.format(confmanager_files_list))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['configmgr is running']
        check_list2 = ['/etc/init.d/configmgrd', '/usr/local/configmgr/configmgr']
        if all(word in confmanager_daemon for word in check_list1) \
                and all(word in confmanager_files_list for word in check_list2):
            self.log.info('Load Conf Manager in firmware test PASSED!!')
        else:
            self.log.error('Load Conf Manager in firmware test FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

LoadConfManager(checkDevice=False)
