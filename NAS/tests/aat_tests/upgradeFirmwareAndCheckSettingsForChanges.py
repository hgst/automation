"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify SSH works on test device

"""

import urllib
import os
import time
import xml.etree.ElementTree as ET

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

REPO_LOCATION = 'http://repo.wdc.com'
SEARCH_BASE_URL = '{0}/service/local/lucene/search?repositoryID=projects'.format(REPO_LOCATION)
ARTIFACT_BASE_URL = '{0}/service/local/repositories/projects/content/'.format(REPO_LOCATION)

fake_fw_version = '00.00.00-000'
set_version_command = 'echo ' + fake_fw_version + ' > /etc/version'
get_version_command = 'cat /etc/version'

seq_group = 'Sequoia-04_04'
artifact = 'sq'
classifier = None
build_url = None
current_fw_version = ''

class UpgradeFirmware(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            firmware_url = ''
            latest_version = ''

            current_fw_version = self._rest.get_firmware_version_number()
            self.log.info('current_fw_version: {}'.format(current_fw_version))
            # seq_fw_format = Ctl.convert_to_seq_firmware_format(current_fw_version)
            # self.log.info('seq_fw_format: {}'.format(seq_fw_format))

            major_version, minor_version, revision, build = current_fw_version.replace('-', '.').split('.')
            self.log.info('major_version: {0}, minor_version: {1}, revision: {2}, build: {3}'.format(major_version, minor_version, revision, build))

            version = current_fw_version.replace('.', '')
            self.log.info('version: {}'.format(version))

            #group = seq_group
            seq_foldername = 'Sequoia-'

            if int(revision) == 0:
                sec_group = '_'.join([major_version, minor_version])
            else:
                sec_group = '_'.join([major_version, minor_version, revision])

            group = seq_foldername + sec_group

            downloader_seq = NexusDownloaderSequoia(group, artifact, version, classifier, build_url)
            print 'downloader: {0}'.format(downloader_seq)
            # write out the build properties
            results = downloader_seq.write_build_properties()

            if results[0] and results[1]:
                firmware_url = results[0]
                latest_version = results[1]
                print 'build url: {0}'.format(firmware_url)
                print 'firmware version: {0}'.format(latest_version)

            users = self.get_all_users()
            old_users = self.get_xml_tags(users, 'username')
            self.log.info("old_users: {}".format(old_users))
            shares = self.get_all_shares()
            old_shares = self.get_xml_tags(shares, 'share_name')
            self.log.info("old_shares: {}".format(old_shares))

            self.execute(set_version_command)
            fw_version = self.execute(get_version_command)
            self.log.info('fw_version: {}'.format(fw_version[1]))

            update_result = self.firmware_update(image=firmware_url)

            if update_result == 0:
                self.log.info('Update successfully')
            else:
                self.log.info('Failed to update')

            time.sleep(60)
            self.wait_for_firmware_update_to_start()
            self.wait_for_firmware_update_to_complete()
            self.wait_for_factory_restore_to_finish()

            users = self.get_all_users()
            new_users = self.get_xml_tags(users, 'username')
            self.log.info("new_users: {}".format(new_users))
            shares = self.get_all_shares()
            new_shares = self.get_xml_tags(shares, 'share_name')
            self.log.info("new_shares: {}".format(new_shares))

            if set(new_shares).issubset(set(old_shares)):
                for share in new_shares:
                    self.log.info("Share: {}".format(share))
            else:
                self.log.error("Default shares not correct")
                for share in new_shares:
                    self.log.info("Share: {}".format(share))

            if set(new_users).issubset(set(old_users)):
                for user in new_users:
                    self.log.info("User: {}".format(user))
            else:
                self.log.error("Default shares not correct")
                for user in new_users:
                    self.log.info("User: {}".format(user))

            # result = self.execute('ifconfig')
            # if result:
            #     self.log.info("SSH to device successfully, result : {}".format(result))
            # else:
            #     self.log.error("Failed to SSH to device")
        except Exception, ex:
            self.log.exception('Failed to complete firmware upgrade test case \n' + str(ex))
        finally:
            self.log.info("Reset fw version".format(current_fw_version))
            set_seq_version_command = 'echo ' + current_fw_version + ' > /etc/version'
            self.execute(set_seq_version_command)

class NexusDownloaderSequoia(object):
    def __init__(self, group, artifact, version, classifier, build_url=None):
        '''Using the given parameters, determine the latest (and/or desired) build properties.
            Provides methods to query nexus and download artifacts.'''
        self.group = group
        self.artifact = artifact
        self.version = version
        self.classifier = classifier
        self.latest = None
        self.build_url = build_url

        print "version inside init: {}".format(version)

        if not self.build_url or build_url == 'NOT_SET' or '$' in build_url:
            # if there are unknown params, use a lucene search to find them
            if not classifier or '*' in group:
                self.group, self.artifact, self.version, self.classifier = self.get_lucene_result(group, artifact,
                                                                                                  version)

            # put together the url that will return the actual file
            self.build_url, self.promote = self.get_artifact_url()
            print 'self.build_url: {}'.format(self.build_url)
            print 'self.promote: {}'.format(self.promote)

    def get_lucene_result(self, group, artifact=None, version='LATEST'):
        '''Uses Nexus api lucene search to find the newest build, or a particular build specified by version
        ***NOTE*** This assumes that classifiers in Nexus are YYYYMMDD dates
        See lucene documentation:
        https://repository.sonatype.org/nexus-indexer-lucene-plugin/default/docs/path__lucene_search.html'''

        repo_query = '&g={0}&a={1}'.format(group, artifact)
        url = SEARCH_BASE_URL + repo_query

        print '*INFO* [LUCENE QUERY] {0}'.format(url)

        data = urllib.urlopen(url).read()
        try:
            tree = ET.XML(data)
        except ET.ParseError as e:
            print data
            raise e

        results = []
        # count = 0
        found_desired_version = False

        print "version inside lucene: {}".format(version)

        for searchHit in ET.ElementPath.findall(tree, './/data//artifact'):
            result_group = searchHit.find('groupId').text
            result_artifact = searchHit.find('artifactId').text
            result_version = searchHit.find('version').text

            if result_version == version:
                found_desired_version = True

            result_classifier = None
            # artifacts can have other files associated (like a .pom), so we need to find the .deb
            for artifact_link in ET.ElementPath.findall(searchHit, './/artifactHits//artifactLink'):
                if artifact_link.find('extension').text == 'deb':
                    # ignore non-numerical build classifiers
                    try:
                        result_classifier = int(artifact_link.find('classifier').text)
                    except:
                        continue

            print '*DEBUG* Found result: {0}  {1}  {2}  {3}'.format(result_group, result_artifact, result_version,
                                                                    result_classifier)
            if result_group and result_artifact and result_version and result_classifier:
                results.append((result_group, result_artifact, result_version, result_classifier))

        print results

        # set latest build
        self.latest = results[0][2]
        print 'latest build: {0}'.format(self.latest)
        print 'found_desired_version: {0}'.format(found_desired_version)
        print 'version: {0}'.format(version)

        if results and version is None:
            return results[0]
        elif results and found_desired_version:
            # we have to get a specific version
            return [result for result in results if result[2] == version][0]
        else:
            raise Exception("Requested artifact could not be found")

    def get_artifact_url(self):
        repo_query = "{0}/{1}/{2}/{1}-{2}-{3}.deb".format(self.group, self.artifact, self.version, self.classifier)
        repo_query = ARTIFACT_BASE_URL + repo_query
        print '*INFO* [ARTIFACT REQUEST] {0}'.format(repo_query)

        promote_query = "{0}/{1}/{2}/{1}-{2}-promoted.txt".format(self.group, self.artifact, self.version)
        promote_query = ARTIFACT_BASE_URL + promote_query
        print '*INFO* [PROMOTE REQUEST] {0}'.format(promote_query)

        return repo_query, promote_query

    def write_build_properties(self):
        '''Writes the properties.txt and build_url.txt files containing the build details'''

        s_firmware_version, s_firmware_build = self.convert_seq_firmware_format(self.version)
        firmware_version = s_firmware_version + '.' + s_firmware_build

        with open('properties.txt', 'w') as properties:
            properties.write('FIRMWARE_BUILD_URL={0}\n'.format(self.build_url))
            properties.write('FIRMWARE_BUILD_NAME={0}/{1}/{2}\n'.format(self.group, self.artifact, self.version))
            properties.write('FIRMWARE_BUILD_NUMBER={0}\n'.format(firmware_version))
            properties.write('TYPE={0}\n'.format(os.environ.get('TYPE')))
            properties.write('PACKAGE_URL={0}\n'.format(self.build_url))
            properties.write('FIRMWARE_FILE_URL={0}\n'.format(self.build_url))
            os.environ['PACKAGE_URL'] = self.build_url
            os.environ['SILK_VERSION'] = s_firmware_version
            os.environ['SILK_BUILD'] = s_firmware_build
            properties.write('SILK_VERSION={0}\n'.format(s_firmware_version))
            properties.write('SILK_BUILD={0}\n'.format(s_firmware_build))

        with open('build_url.txt', 'w') as build_file:
            build_file.write('PACKAGE_URL={0}\n'.format(self.build_url))
            if self.version:
                build_file.write('BUILD_NUMBER={0}\n'.format(self.version))

            build_file.write('LATEST={0}\n'.format(self.latest))

        return self.build_url, self.latest, self.promote

    def convert_seq_firmware_format(self, firmware):
            a, build = firmware.split('-')
            print a, build
            version = firmware[2:4]
            version1 = firmware[1:2]
            print version, version1
            s_firmware_version = version1 + '.' + version
            s_firmware_build = build
            return s_firmware_version, s_firmware_build

UpgradeFirmware()