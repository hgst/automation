""" Settings Page Media DNLA info Display(MA-85)

    @Author: vasquez_c
    Procedure @ silk:
                1) Open the webUI -> login as system admin
                2) Click on the Settings category
                3) Select Media ====> Confirm that the Default DLNA media server setting is set to OFF
                4) Switch ON DLNA Media Streaming ====> Verify that media is being served 
                5) Click rescan under DLNA media server ====> MUST be able to refresh/rescan the DLNA database. 
                                                              Refresh/Rescan button should be greyed out while a 
                                                              refesh/rescan is in progress. MUST show how many Videos,
                                                              Photos, Music items it has found while rescanning.
                6) Click rebuild under DLNA media server====> MUST be able to refresh/rescan the DLNA database. 
                                                              Refresh/Rescan button should be greyed out while a 
                                                              refesh/rescan is in progress. MUST show how many 
                                                              Videos, Photos, Music items it has found while rescanning.
                7) Click on view media players link     ====> Verify Generic Media Receivers and WDTV Products on network are displayed
                8) Verify that DLNA media server settings 
                   are functioning                      ====> DLNA page settings are functional
                9) Toggle media streaming to OFF        ====> DLNA stats are no longer displayed. Device is not discoverable via DLNA

    Automation: Partial
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.XXX)
"""             
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.webdriver.common.keys import Keys
from testCaseAPI.src.testclient import Elements as E
import time
import os
import re
SHARENAME = 'mediaShare'
testname = 'Settings Page Media DNLA info Display Test'

class mediaDLNADisplay(TestClient):
    
    def run(self):
        self.start_test(testname)
        self.get_to_page('Settings')
        
        # Eject usb drive, if present, as it may contain media files.
        if self.find_usb_drive()[1] <> '':
            usb_info = self.get_usb_info()
            usb_handle = self.get_xml_tag(usb_info[1], 'handle')
            self.eject_usb_drive(usb_handle)
            self.log.info('USB drive was ejected')
               
        self.media_set_off_verification()
        self.toggle_media_streaming(enable_streaming=True)
        self.populate_media_server()
        self.verify_media_files(operation='RESCAN')
        self.verify_media_files(operation='REBUILD')
        self.toggle_media_streaming(enable_streaming=False)
        self.pass_test(testname)
        self.log.info('######################## Settings Page - Media DLNA - Display TEST END ##############################')
    
          
    def populate_media_server(self):
        self.delete_all_shares()
        self.create_shares(share_name=SHARENAME, media_serving='any', force_webui=True)
        # Refresh page
        self.get_to_page('Home')
        self.enable_media_serving(share_name=SHARENAME)

        self.generate_media_files(media_info=('jpg', 1024, 768),
                                  total_files=3,
                                  destination='',
                                  share_name=SHARENAME,
                                  #files_per_folder=3, 
                                  media_variance=10,
                                  random_data=True)
        self.create_file(filesize=1024, file_count=4, dest_share_name=SHARENAME, extension='mp3')
        self.create_file(filesize=1024, file_count=2, dest_share_name=SHARENAME, extension='mpeg')

    def media_set_off_verification(self):
        self.click_element(E.SETTINGS_MEDIA)
        self.log.info('Confirming media server setting default (OFF)')
        media_setting_text = self.get_text('css = #settings_mediaDLNA_switch+span .checkbox_container')
       
        if media_setting_text == 'OFF':
            self.log.info('Media Server Setting is disabled (Test PASSED), status = {0}'.format(media_setting_text))
        else:
            self.log.error('Media Server Setting is enabled (Test FAILED), status = {0}'.format(media_setting_text))
            
    def verify_media_files(self, operation):
        self.log.info('Verifying media files')
        operation = operation.upper()
        if operation == 'RESCAN':
            self.click_button(E.RESCAN_MEDIA_BUTTON)
            time.sleep(5)
            # wait for database to finish updating
            while self.wait_until_element_is_clickable(E.RESCAN_MEDIA_BUTTON,300) is False:
                time.sleep(1)
           
        else:
            self.click_button(E.REBUILD_MEDIA_BUTTON)
            time.sleep(5)
            # wait for database to finish updating
            while self.wait_until_element_is_clickable(E.REBUILD_MEDIA_BUTTON,300) is False:
                time.sleep(1)
                       
        media_files = re.findall(r'\d+', self.get_text(locator='DIV_RPC_Media'))
        if ((media_files[0] == '4') and (media_files[1] == '20') and (media_files[2] == '2')):
            self.log.info('PASS: {} Number of Videos, Photos and Music file matched'.format(operation))
        else:
            self.log.error('FAIL: {} Number of Videos, Photos and Music file did not matched'.format(operation))
mediaDLNADisplay()
        
        
            