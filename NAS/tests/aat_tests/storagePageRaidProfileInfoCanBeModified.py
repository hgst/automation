"""
## @author: tran_jas
## @Brief Test unit's RAID profile info can be changed
#  @details
#   Configure test unit as RAID1 or RAID0
#   SMB copy/read to verify RAID is configured properly
#   Verify correct RAID mode text is displayed
"""


import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as Element


class RaidProfile(TestClient):

    def run(self):
        try:
            self.log.info('RAID profile can be modified test case')
            current_raid_mode = self.get_raid_mode()
            self.log.info('current_raid_mode: {}'.format(current_raid_mode))
            self.access_webUI()
            time.sleep(5)
            if current_raid_mode == 'RAID1':
                self.configure_raid0()
                set_raid_mode = 'RAID 0'
            else:
                self.configure_raid1()
                set_raid_mode = 'RAID 1'

            generated_file = self.generate_test_file(kilobytes=10240)
            self.write_files_to_smb(files=generated_file)
            self.read_files_from_smb(files='file0.jpg')
            self.close_webUI()

            self.click_element(Element.STORAGE_RAID)
            new_raid_mode = self.get_text("//tr[@id='row1']/td[2]/div")
            self.log.info('New RAID mode: {}'.format(new_raid_mode))

            if new_raid_mode == set_raid_mode:
                self.log.info('RAID profile change passed')
            else:
                self.log.error('RAID profile change failed')
        except Exception, ex:
            self.log.error('Converting RAID mode was not successful. Check RAID profile settings\n' + str(ex))

RaidProfile()
