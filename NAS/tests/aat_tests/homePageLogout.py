"""
title           :homePageLogout.py
description     :To verify if system admin can logout from the dashboard
author          :yang_ni
date            :2015/05/09
notes           :
"""

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
import time


class homePageLogout(TestClient):

    def run(self):

            try:
                testname = 'Home page - Logout - user can log out from the home page'
                self.start_test(testname)

                self.access_webUI()
                time.sleep(8)
                self.click_element(E.TOOLBAR_LOGOUT)
                time.sleep(5)
                self.wait_until_element_is_visible(E.TOOLBAR_LOGOUT_LOGOUT,15)
                self.click_element(E.TOOLBAR_LOGOUT_LOGOUT)
                time.sleep(10)

                # Go back to login page
                self.wait_until_element_is_visible(E.LOGIN_BUTTON,15)
                self.pass_test(testname, 'System admin successfully logs out')

            except Exception as e:
                self.fail_test(testname, 'System admin fails to log out: {}'.format(e))

# This constructs the homePageLogout() class, which in turn constructs TestClient() which triggers the homePageLogout.run() function
homePageLogout()