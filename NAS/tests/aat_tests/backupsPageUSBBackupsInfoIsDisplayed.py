'''
Created on May 13th, 2015

@author: tsui_b

Objective: Verify USB Backups job list is not displayed when there are no created jobs

Automation: Full
    
Supported Products: All
    
Test Status: 
            Local 
                    Lightning: Pass (FW: 2.00.215)
            Jenkins 
                    Lightning: Pass (FW: 2.00.216)
                    Kings Canyon: Pass (FW: 2.00.216)
                    Glacier: Pass (FW: 2.00.216)
                    Zion: Pass (FW: 2.00.216)
                    Yosemite: Pass (FW: 2.00.216)
                    Yellowstone: Pass (FW: 2.00.216)
                    Aurora: Pass (FW: 2.00.216)
                    Sprite: Pass (FW: 2.00.216)  
'''
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

class backupsPageUSBBackupsInfoIsDisplayed(TestClient):

    def run(self):
        self.log.info('### Verify USB Backups job list is not displayed when there are no created jobs ###')        
        try:
            self.delete_all_backups_jobs()
            self.check_empty_job_list()
        except Exception as e:
            self.log.error("Failed to verify USB Backups job list! Exception: {0}".format(repr(e)))

    def tc_cleanup(self):
        self.delete_all_backups_jobs()

    def delete_all_backups_jobs(self):
        self.log.info('Check how many backups jobs are created before.')
        result = self.execute('cat /var/www/xml/usb_backup.xml')[1]
        if 'No such file or directory' in result:
            self.log.info('There are no created backups jobs.')
            return
        
        created_backups = self.get_xml_tags(result, 'task_name')
        if not created_backups:
            self.log.info('There are no created backups jobs.')
        else:
            self.log.info('There are {0} created backups jobs, delete them.'.format(len(created_backups)))
            for backups in created_backups:
                self.log.info('Deleting backups job: {0}'.format(backups))
                result = self.execute('usb_backup -a {0} -c jobdel'.format(backups))
                if result[0] != 0:
                    raise Exception('Delete backups job failed! Exception: {0}'.format(result[1]))

            self.log.info('All backups jobs are deleted.')

    def check_empty_job_list(self):
        self.log.info('Verifying USB Backups job list is not displayed when there are no created jobs.')
        self.get_to_page('Backups')
        self.click_element(E.BACKUPS_USB)
        self.element_should_not_be_visible(E.BACKUPS_USB_JOB_LIST)
        
backupsPageUSBBackupsInfoIsDisplayed()