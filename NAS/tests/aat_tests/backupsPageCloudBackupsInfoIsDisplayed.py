""" Backups Page-Cloud Backups info is displayed

    @Author: Lee_e
    
    Objective: verify information about Cloud backups is shown correctly
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.10.140)
                    Lightning : PASS (FW: 2.10.146)
                    KC        : PASS (FW: 2.10.146)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.150 )
                              : Yosemite     - PASS (FW: 2.10.150 )
                              : Kings Canyon - PASS (FW: 2.10.150 )
                              : Sprite       - PASS (FW: 2.10.150 )
                              : Zion         - PASS (FW: 2.10.150 )
                              : Aurora       - PASS (FW: 2.10.150 )
                              : Yellowstone  - PASS (FW: 2.10.150 )
                              : Glacier      -  De-feature
     
""" 
from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.ui_map import Page_Info

title_note = 'Cloud Backups'
description = 'A cloud backup allows you to create remote backups that are accessible over the internet, and through various mobile devices.'
    

class BackupsPageCloudBackupsInfoIsDisplayed(TestClient):
    def run(self):
        self.backupsPageCloudBackupsInfoIsDisplayed_test()

    def backupsPageCloudBackupsInfoIsDisplayed_test(self):
        try:
            self.webUI_admin_login(username=self.uut[self.Fields.default_web_username])
            self.click_wait_and_check(Page_Info.info['Backups'].link, self.Elements.BACKUPS_CLOUD)
            self.click_wait_and_check(self.Elements.BACKUPS_CLOUD, 'css=div.h1_content.header_2 > span._text', timeout=10)
            title_ui = self.get_text('css=div.h1_content.header_2 > span._text')
            self.log.debug('title shows on UI: {0}'.format(title_ui))
            if title_note in title_ui:
                self.log.info('PASS: title of Cloud Backup shown correctly')
            else:
                self.log.error('FAIL: title of Cloud Backup shown incorrectly : {0}'.format(title_ui))

            self.wait_until_element_is_visible('css=div.tdfield_padding > span._text')
            description_ui = self.get_text('css=div.tdfield_padding > span._text')
            self.log.debug('description shows on UI: {0}'.format(description_ui))

            if description_ui == description:
                self.log.info('PASS: Description of Cloud Backup shown correctly')
            else:
                self.log.error('FAIL: Description of Cloud Backup title shown incorrectly : {0}'.format(description_ui))

        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot()

    def webUI_admin_login(self, username):
        """
        :param username: user name you'd like to login

        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("PASS: Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot()
                
BackupsPageCloudBackupsInfoIsDisplayed()