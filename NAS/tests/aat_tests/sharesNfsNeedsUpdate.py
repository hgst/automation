""" Shares - NFS (NEEDS UPDATE) - (MA-202)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?pId=50&view=details&nTP=117743&pltab=steps
        1) Enable NFS
        2) Test NFS Access (able to copy file using NFS)
        3) Disable NFS
        4) Test NFS Disabled

    Automation: Full

    Not supported product:
        Lightning

    Support Product:
        YellowStone
        Glacier   (Does not have global NFS settings)
        Yosemite
        Sprite
        Aurora
        Kings Canyon


    Test Status:
        Local Yosemite: Pass (FW: 2.10.116)
"""
from testCaseAPI.src.testclient import TestClient
import global_libraries.wd_exceptions as exceptions
from global_libraries.testdevice import Fields
import os
import time
import shutil

TCNAME = "Shares - NFS (NEEDS UPDATE) - (MA-202)"
newShareName = 'nfstest'
testFileSize = 100
picturecounts = 0

noNfsSupportModel = ('LT4A')


class sharesNfsNeedsUpdate(TestClient):
    def run(self):
        self.log.info('######################## {} TEST START ##############################'.format(TCNAME))
        nfs_mount = None
        try:
            resp = self.verify_nfs_model()
            if resp == 1:
                return
            self.log.info("Delete all groups")
            self.delete_all_groups()
            self.log.info("Delete all users")
            try:
                self.delete_all_users(use_rest=False)
            except exceptions.InvalidDeviceState:
                self.log.info("User list is empty")
            self.log.info("Delete all shares")
            self.delete_all_shares()
            self.log.info("Umount and clean all existing mnt folders before test")
            self._unmount_all()
            self.nfs_folders_cleanup()
            self.create_shares(share_name=newShareName, force_webui=True)
            self.close_webUI()  # Need to close the UI to refresh the new share being loaded
            # self.update_share_network_access(share_name=newShareName, public_access=True)
            tmp_file = self.generate_test_file(kilobytes=testFileSize)
            self.log.info("======== Verify NFS access ========")
            # mount nfs folder
            nfs_mount = self.mount_share(share_name=newShareName, protocol=self.Share.nfs)
            # Test NFS read/write access
            self.nfs_read_write_access(nfs_mount=nfs_mount, tmp_file=tmp_file)
            # Turn off NFS and test nfs write access
            self.log.info("======== Going to turn off NFS ========")
            self.disable_nfs_share_access(share_name=newShareName)
            self.test_nfs_write_access(nfs_mount=nfs_mount, tmp_file=tmp_file)
            # Unmount and remount the NFS share to verify the NFS r/w access
            self.log.info("Unmount NFS share: {}".format(nfs_mount))
            self.unmount_share(share_name=newShareName, protocol=self.Share.nfs)
            # Delete mount folder ./mnt/wdaatX
            if os.path.exists(nfs_mount):
                self.log.inf("INFO: Going to clean nfs mount @ {}".format(nfs_mount))
                shutil.rmtree(nfs_mount)
            self.log.info("======== Re-enable and verify NFS access again ========")
            nfs_mount = self.mount_share(share_name=newShareName, protocol=self.Share.nfs)
            self.nfs_read_write_access(nfs_mount=nfs_mount, tmp_file=tmp_file)
        except Exception as e:
            self.log.error("FAILED: {} Test Failed!!, exception: {}".format(TCNAME, repr(e)))
        else:
            self.log.info("PASS: {} Test PASS!!!".format(TCNAME))
        finally:
            self.log.info("========== (MA-202) Clean Up ==========")
            if nfs_mount:
                self.unmount_share(share_name=newShareName, protocol=self.Share.nfs)
            self.delete_all_shares()
            self.log.info('######################## {} TEST END ##############################'.format(TCNAME))

    def umount_nfs_share(self, nfs_mount_dir):
        from global_libraries import CommonTestLibrary as ctl
        try:
            ctl.run_command_locally('sudo umount -f {}'.format(nfs_mount_dir))
        except Exception as e:
            self.log.error("ERROR: fail to umount nfs share: {}, exception: {}".format(nfs_mount_dir, repr(e)))

    def nfs_folders_cleanup(self):
        from global_libraries import CommonTestLibrary as ctl
        from glob import glob
        if os.path.exists('mnt'):
            os.chdir('mnt')
            nfs_tmps = glob('wdaat*')
            for folder in nfs_tmps:
                self.log.info("Remove nfs temp folder: {}".format(folder))
                resp = ctl.run_command_locally('sudo rm -rf {}'.format(folder))
                self.log.info("Result: {}".format(resp))
        else:
            self.log.info("No mnt folder is found!")

    def verify_nfs_model(self):
        product_model = self.get_model_number()
        if product_model == 'LT4A':
            self.log.info("Lightning does not support NFS service, end of the test!!!")
            return 1
        else:
            return 0

    def test_nfs_write_access(self, nfs_mount=None, tmp_file=None):
        try:
            # self.copy_file(source_file=tmp_file, dest_file='toNfsFile.bmp', destination_path=nfs_mount, user='admin')
            self.copy_files(source_file=tmp_file, dest_file='toNfsFile.bmp', destination_path=nfs_mount)
        except Exception as e:
            self.log.info("PASS: NFS write failed due to NFS service set to OFF, exception: {}".format(repr(e)))

    def nfs_read_write_access(self, nfs_mount=None, tmp_file=None):
        if (not nfs_mount) and (not tmp_file):
            raise Exception("ERROR: either nfs_mount or tmp_file is None")
        self.log.info("---------- Start NFS write ----------")
        try:
            # self.copy_file(source_file=tmp_file, dest_file='toNfsFile.bmp', destination_path=nfs_mount, user='admin')
            self.copy_files(source_file=tmp_file, dest_file='toNfsFile.bmp', destination_path=nfs_mount)
        except Exception as e:
            self.log.error("NFS write failed, exception: {}".format(repr(e)))
        self.log.info("---------- Complete NFS write ----------")
        self.log.info("---------- Start NFS read ----------")
        curdir = os.getcwd()
        # self.copy_file(source_file=nfs_mount+'toNfsFile.bmp', dest_file='fromNfsFile.bmp', destination_path=curdir, user='admin')
        try:
            self.copy_files(source_file=nfs_mount+'toNfsFile.bmp', dest_file='fromNfsFile.bmp', destination_path=curdir)
        except Exception as e:
            self.log.error("NFS read failed, exception: {}".format(repr(e)))
        self.log.info("---------- Complete NFS read ----------")
        self.nfs_hash_check(srcfile=tmp_file, dstfile='fromNfsFile.bmp')
        # self.log.info("Unmount NFS share: {}".format(nfs_mount))
        # self.unmount_share(share_name=newShareName, protocol=self.Share.nfs)

    def copy_files(self, source_file, dest_file, destination_path):
        import os
        dst_file = os.path.join(destination_path, dest_file)
        self.log.info("Going to copy [{}] to [{}]".format(source_file, dst_file))
        shutil.copyfile(source_file, dst_file)

    def nfs_hash_check(self, srcfile, dstfile):
        hashsrc = self.checksum_files_on_workspace(dir_path=srcfile, method='md5')
        hasdst = self.checksum_files_on_workspace(dir_path=dstfile, method='md5')
        if hashsrc == hasdst:
            self.log.info('PASS: NFS MD5 hash check test')
        else:
            self.log.error('FAILED: NFS MD5 hash check test')

    def disable_nfs_share_access(self, share_name):
        """
            Configure nfsShare to the access value passed by the argument

            @share_name: name of the share to enable nfs access to
            @access: The access permission of nfs Share
        """
        try:
            self.log.info('Configure NFS {0}'.format(share_name))
            self.get_to_page('Shares')
            time.sleep(2)
            self.log.info('Click on {}'.format(share_name))
            self.click_element('shares_share_{}'.format(share_name))
            time.sleep(2)
            self.log.info('Switch the nfs to OFF')
            nfs_status = self.get_text('css=#shares_nfs_switch+span .checkbox_container')
            if nfs_status == 'ON':
                self.click_element('css=#shares_nfs_switch+span .checkbox_container')
            else:
                self.log.info("nfs has been turned to OFF, status = {}".format(nfs_status))
            self.log.info('nfs service is set to OFF')
            time.sleep(5)  # Waiting for Saving  to complete
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. exception: {}".format(repr(e)))

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

sharesNfsNeedsUpdate()