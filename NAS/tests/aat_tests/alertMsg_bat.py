""" BAT - Alert Message (MA-29)

    @Author: lin_ri
    On silk:
    1) Turn on Alert emails
    2) Turn on SMS alert
    3) Trigger Critical Alert
    4) Trigger Warning Alert
    5) Trigger Info alert 
    
    Automation: Partial
    
    Manual:
        Verify if you receive alert emails and SMS messages
    
    Test Status:
              Local Ligtning: Pass (FW: 2.00.154)
              
""" 

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.webdriver.common.keys import Keys
import time


class alertMsgBat(TestClient):
    def run(self):
        try:
            self.log.info('######################## BAT - Alert Messages TESTS START ##############################')            
            self.config_email_notification(testAddress="fituser@mailinator.com", invalidAddressList=['usr.usr.usr', 'd@d$3d'])
            self.set_sms_notification(vendor='kotsms', username='WDTW', userpwd='WDTWSEVX', mobile=886912345678, msg="MSG from WD MyCloud EX4 NAS!!!")
            self.log.info("Clean all existing alerts...")
            self.execute('alert_test -R')
            self.log.info("Generating alerts...")
            self.trigger_alerts(alertcode=('0001', '1003', '2003'))  # Generate Critical, Warning and Informational alerts
            time.sleep(10)  # Waiting for alert mails being sent
        except Exception as e:
            self.log.error("Test Failed: BAT - Alert Messages test, exception: {}".format(repr(e)))
            self.take_screen_shot(message='In run()', prefix='run')
        else:
            self.log.info("PASS: BAT - Alert Messages")
        finally:
            self.log.info("========== Test Case Clean Up ===========")
            self.execute('alert_test -R')
            self.delete_sms_notification()
            self.delete_email_notification()
            self.log.info('######################## BAT - Alert Messages TESTS END ##############################')      

    def trigger_alerts(self, alertcode=None):
        """
            Trigger Critical Alerts which does not require parameters
            
            @alertcode: '0001' the alert code you want to trigger
            
            If the alert needs arguments, the command requires '-p PARM1,PARM2' after -a
            Sample alert codes:
            Critical
                0001        System Over Temperature
                0224        Replace Drive with Red Light
                0029        Fan Not Working
            Warning
                1002        Network Link Down
                1003        Firmware Update Failed
            Information
                1020        System Shutting down                 
                2003        Temperature Normal
                
        """
        if alertcode == None:
            alertcode = ('0001')
        
        # Generate alerts
        for code in alertcode:
            self.execute('alert_test -a {} -f'.format(code))
        
        # Retrieve current alerts
        resp = self.execute('alert_test -g')
        alerts = resp[1].split('---->')
        for a in alerts[1:]:
            alertnum = a.split('\n')[2]
            alertmsg = a.split('\n')[6]
            self.log.info('{}, {} is generated.'.format(alertnum, alertmsg)) # print the alertMSG
        self.log.info("Verify: Please check your mailbox/SMS if you receive alerts")

    def config_email_notification(self, testAddress="rick.lin@wdc.com", invalidAddressList=['usr.usr.usr', 'd@d$3d']):
        """
            Enable and Test email alert
            
            @testAddress: email address you'd like to add in the notification
            @invalidAddressList: invalid email test patterns        
        """
        email_status = None
        self.log.info("==> Configure Alert emails notification")
        self.get_to_page('Settings')
        self.click_wait_and_check('settings_notifications_link', "//div[@id='settings_notificationsDisplay_slider']/a", visible=True)
        self.log.info("Slide the notification display bar to All")
        time.sleep(2)
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
        # Get Current Level value
        element = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
        percent_text = element.get_attribute('style')
        percent = int(percent_text.split()[1][:-2])
        self.log.info("Notification Level is set to [{}]".format(percent))      
        time.sleep(2)
        self.log.info("Turn on Alert emails notification")
        email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
        if email_status != 'ON':
            self.click_element('css=#settings_notificationsAlert_switch+span .checkbox_container')
        email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')            
        self.log.info("Alert emails notification is enabled to {}".format(email_status))
        time.sleep(2)
        self.log.info("Click Alert emails configure")
        # self.click_wait_and_check('css=#settings_notificationsAlert_link > span._text',
        #                           "//div[@id='settings_notificationsAlert_slider']/a", visible=True)
        self.click_wait_and_check('settings_notificationsAlert_link',
                                  "//div[@id='settings_notificationsAlert_slider']/a", visible=True)
        
        # Slide email notifications to All
        self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_RIGHT) # 50%
        self.press_key("//div[@id='settings_notificationsAlert_slider']/a", Keys.ARROW_RIGHT) # 100%
        
        # Clear all emails (Max: 5)
        while self.is_element_visible('settings_notificationsDelMail1_link'):
            self.wait_and_click('settings_notificationsDelMail1_link')
            time.sleep(2)
        self.log.info("All email settings are deleted!")
        
        # Test invalid email
        if self.is_element_visible('settings_notificationsAddMail_button'):
            try:
                self.click_wait_and_check('settings_notificationsAddMail_button',
                                          'settings_notificationsSaveMail_button', 
                                          visible=True)
            except Exception as e:
                self.log.info("Cannot add new email, close pop-up window and retry")
                self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
                email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
                if email_status != 'ON':
                    self.click_element('css=#settings_notificationsAlert_switch+span .checkbox_container')
                email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')            
                self.log.info("Alert emails notification is enabled to {}".format(email_status))
                time.sleep(2)
                self.log.info("Click Alert emails configure")
                self.click_wait_and_check('settings_notificationsAlert_link',
                                          "//div[@id='settings_notificationsAlert_slider']/a", visible=True)                
                self.click_wait_and_check('settings_notificationsAddMail_button',
                                          'settings_notificationsSaveMail_button', 
                                          visible=True)

            for invalidAddress in invalidAddressList:
                self.log.info("Send email pattern: {}".format(invalidAddress))
                self.input_text_check('settings_notificationsMail_text', invalidAddress)
                time.sleep(2)
                self.wait_and_click('settings_notificationsSaveMail_button', timeout=5)
                if self.is_element_visible('popup_ok_button'):
                    self.log.info('Email check dialog: ' + self.get_text('error_dialog_message'))
                    self.click_wait_and_check('popup_ok_button', visible=False)
                else:
                    if self.is_element_visible('settings_notificationsDelMail1_link'):
                        self.log.error("ERROR: Invalid email test pattern [{}] is accepted.".format(invalidAddress))
            
        # Add valid email
        if self.is_element_visible('settings_notificationsMail_text'):
            self.log.info("Add valid email: {}".format(testAddress))
            self.input_text_check('settings_notificationsMail_text', testAddress, do_login=False)
            time.sleep(2)
            self.click_wait_and_check('settings_notificationsSaveMail_button', visible=False)
                
        # Send Test Email
        self.log.info("Click Send Test Email")
        self.wait_and_click('settings_notificationsTestMail_button')
        time.sleep(2)
        self.log.info("VERIFY: PLEASE CHECK YOUR TEST EMAILBOX FOR TEST MAIL")
        self.log.info("Click OK to finish Alert emails configuration")
        self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
        time.sleep(2)
    
    def set_sms_notification(self, vendor='kotsms',username='WDTW', userpwd='WDTWSEV', mobile=886912345678, msg="MSG from WD MyCloud EX4 NAS!!!"):
        """
            Configure SMS notification
            
            @username: kotsms's account name
            @userpwd: kotsms's account password
            @mobile: Your cellphone filled in Clickatell
            @msg: Message you'd like to transmit 
        """
        self.log.info("==> Configure SMS notification")
        self.get_to_page('Settings')
        time.sleep(2)
        self.click_wait_and_check('settings_notifications_link', "//div[@id='settings_notificationsDisplay_slider']/a", visible=True)
        self.log.info("Slide the notification display bar to All")
        time.sleep(1)
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
        time.sleep(2)
        self.log.info("Turn on SMS notification")
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                      'css=#settings_notificationsSms_switch+span .checkbox_container .checkbox_on',
                                      visible=True)
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')            
        self.log.info("SMS notification is enabled to {}".format(sms_status))
        time.sleep(2)
        self.log.info("Click SMS configure")
        # self.click_wait_and_check('css=#settings_notificationsSms_link > span._text', 'settings_notificationsSmsCancel1_button', visible=True)
        self.click_wait_and_check('settings_notificationsSms_link',
                                  'smsDiag_title', visible=True)
        if self.is_element_visible('settings_notificationsSmsDel_button', 10):
            self.log.info("Clean existing SMS configuration")
            self.click_element("settings_notificationsSmsDel_button")
            time.sleep(3)
        time.sleep(2)        
        self.input_text_check('settings_notificationsSmsName_text', vendor, False)
        SMS_URL = "http://202.39.48.216/kotsmsapi-1.php?username={}&password={}&dstaddr={}&smbody={}".format(username, userpwd, mobile, msg)
        self.log.info("SMS URL: {}".format(SMS_URL))
        time.sleep(2)
        self.input_text_check('settings_notificationsSmsUrl_text', SMS_URL, False)
        self.click_wait_and_check('settings_notificationsSmsNext1_button', 'settings_notificationsSmsSave_button', visible=True)
        time.sleep(2)
        self.log.info("Configure SMS settings")
        self.click_link_element('id_alias0', linkvalue='Username')
        self.click_link_element('id_alias1', linkvalue='Password')
        self.click_link_element('id_alias2', linkvalue='Phone number')
        self.click_link_element('id_alias3', linkvalue='Message content')
        self.click_wait_and_check('settings_notificationsSmsSave_button', visible=False)
        time.sleep(2)
        self.log.info("Click Test SMS button to send out SMS test message")
        self.wait_and_click('settings_notificationsSmsTest_button')      
        time.sleep(2)
        # self.wait_until_element_is_visible('popup_ok_button')
        count = 1
        while count <= 3:
            if self.is_element_visible('popup_ok_button'):
                break
            else:
                time.sleep(1)
                self.log.info("{}: Re-click Test SMS button".format(count))
                self.click_element('settings_notificationsSmsTest_button')
                count += 1
        sms_test = self.get_text('css=div#error_dialog_message')
        self.log.info("{}".format(sms_test))
        self.click_wait_and_check('popup_ok_button', visible=False)
        self.click_wait_and_check('settings_notificationsSmsSetSave_button', visible=False)

    def delete_sms_notification(self):
        """
            Delete SMS configuration
        """
        self.log.info("==> Clear SMS notification")
        self.get_to_page('Settings')
        self.click_wait_and_check('settings_notifications_link',
                                  'css=#settings_notificationsSms_switch+span .checkbox_container', visible=True)
        time.sleep(2)
        self.log.info("Turn on SMS notification")
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_element('css=#settings_notificationsSms_switch+span .checkbox_container')
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')            
        self.log.info("SMS notification is enabled to {}".format(sms_status))
        time.sleep(2)
        self.log.info("Click SMS configure")
        # self.wait_and_click('css=#settings_notificationsSms_link > span._text')
        self.wait_and_click('settings_notificationsSms_link')
        if self.is_element_visible('settings_notificationsSmsDel_button'):
            self.log.info("Clean existing SMS configuration")
            # self.wait_and_click("settings_notificationsSmsDel_button")
            self.click_wait_and_check("settings_notificationsSmsDel_button", visible=False)
            time.sleep(3)
            self.log.info("Close SMS Setting dialog window")
            # self.wait_and_click('css=#settings_notificationsSmsCancel1_button')
            self.click_wait_and_check('css=#settings_notificationsSmsCancel1_button', visible=False)
            self.log.info("Succeed to clear SMS notification")
            self.log.info("SMS notification will be turned to OFF due to no configuration...")
        time.sleep(2)
    
    def delete_email_notification(self):
        """
            Delete email settings
        """
        email_status = None
        self.log.info("==> Clear Email settings ")
        self.get_to_page('Settings')
        self.wait_and_click('settings_notifications_link')
        time.sleep(2)
        self.log.info("Turn on Alert emails notification")
        email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
        if email_status != 'ON':
            self.click_element('css=#settings_notificationsAlert_switch+span .checkbox_container')
        email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')            
        self.log.info("Alert emails notification is enabled to {}".format(email_status))
        time.sleep(2)
        self.log.info("Click Alert emails configure")
        # self.wait_until_element_is_visible('css=#settings_notificationsAlert_link > span._text', timeout=15)
        # self.click_wait_and_check('css=#settings_notificationsAlert_link > span._text',
        #                           'settings_notificationsMailSave_button', visible=True)
        self.wait_until_element_is_visible('settings_notificationsAlert_link', timeout=15)
        self.click_wait_and_check('settings_notificationsAlert_link',
                                  'settings_notificationsMailSave_button', visible=True)
        # Clear all emails (Max: 5)
        while self.is_element_visible('settings_notificationsDelMail1_link'):
            self.wait_and_click('settings_notificationsDelMail1_link')
            time.sleep(2)
        self.log.info("All email settings are deleted!")
        self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
        time.sleep(2)

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, timeout=5, retry=3):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break
        
alertMsgBat()



