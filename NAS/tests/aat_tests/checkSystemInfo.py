"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify manufacturer is Western Digital Corporation

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient



class CheckSystemInfo(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            result = self.get_system_information()
            manufacturer = self.get_xml_tag(result[1], 'manufacturer')
            self.log.info('Manufacturer: {}'.format(manufacturer))
            if manufacturer == 'Western Digital Corporation':
                self.log.info("Check system info test passed")
            else:
                self.log.error("Check system info test failed")
        except Exception, ex:
            self.log.exception('Failed to complete system info test case \n' + str(ex))

CheckSystemInfo()