'''
Created on May 8th, 2015

@author: tsui_b

Objective: Verify user can view installed apps at home page

Automation: Full
    
Supported Products: All
    
Test Status: 
            Local 
                    Lightning: Pass (FW: 2.00.215)
            Jenkins 
                    Lightning: Pass (FW: 2.00.215)
                    Kings Canyon: Pass (FW: 2.00.216)
                    Glacier: Pass (FW: 2.00.215)
                    Zion: Pass (FW: 2.00.215)
                    Yosemite: Pass (FW: 2.00.215)
                    Yellowstone: Pass (FW: 2.00.215)
                    Aurora: Pass (FW: 2.00.215)
                    Sprite: Pass (FW: 2.00.216)  
'''
import time
import requests

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

timeout = 20

class homePageUserCanViewInstalledApps(TestClient):

    def run(self):
        self.log.info('### Verifying user can view installed apps at home page ###')        
        try:
            self.delete_all_installed_apps()
            self.install_new_app(appname='IceCast')
            self.check_installed_apps(appname='IceCast')
        except Exception as e:
            self.log.error("User view installed apps failed! Exception: {}".format(repr(e)))        

    def tc_cleanup(self):
        self.delete_all_installed_apps()
                
    def delete_all_installed_apps(self):
        self.log.info('Check how many apps are installed.')
        result = self.execute('cat /var/www/xml/apkg_all.xml')[1]
        if 'No such file or directory' in result:
            self.log.info('There are no installed apps')
            return
            
        installed_apps = self.get_xml_tags(result, 'name')
        if not installed_apps:
            self.log.info('There are no installed apps')
        else:
            self.log.info('There are {0} installed apps, delete them.'.format(len(installed_apps)))
            for apps in installed_apps:
                self.log.info('Deleting app: {0}'.format(apps))
                result = self.send_cgi('cgi_apps_del&f_module_name={0}'.format(apps))
                cgi_result = self.get_xml_tags(result, 'result')
                if '1' in cgi_result:
                    self.log.info('App: {0} is deleted.'.format(apps))
                else:
                    raise Exception('Failed to delete app: {0}!'.format(apps))

    def install_new_app(self, appname):
        self.log.info('Installing app: {0}'.format(appname))
        self.send_cgi('cgi_apps_auto_install&f_module_name={0}'.format(appname))
        time.sleep(5)
        # Wait for 5 secs and then keep checking app install status until timeout
        # 0: Downloading, 1: Installing, 2: Success, Others: Error
        timer = 0
        while True:
            if timer >= 60:
                raise Exception('Check app_status.xml timeout, failed to install app: {0}!'.format(appname))
            result = self.execute('cat /var/www/xml/app_status.xml')
            if result[0] != 0:
                timer += 5
                time.sleep(5)
                continue
            else:
                app_status = self.get_xml_tags(result[1], 'Status')[0]
                if app_status == '0' or app_status == '1':
                    timer += 5
                    time.sleep(5)
                    continue
                elif app_status == '2':
                    self.log.info('App: {0} is installed.'.format(appname))
                    return
                else:
                    raise Exception('Failed to install app: {0}! Install status:{1}'.format(appname, app_status))    

    def check_installed_apps(self, appname):
        self.log.info('Checking app: {0} can be viewed at home page.'.format(appname))
        self.get_to_page('Home')
        app_number = self.get_text('home_apps_count')
        app_name_locator = 'xpath=(//tr[@id="row{}"]/td[2]/div)[2]'.format(int(app_number)-1)
        app_detail_locator = 'css=tbody#Home_APPList_tbody tr#row{} td div.edit_detail'.format(int(app_number)-1)
        self.click_wait_and_check(E.HOME_APPS_ARROW, E.HOME_APPS_DIAG)
        self.wait_until_element_is_visible(app_name_locator, timeout)
        self.element_text_should_be(app_name_locator, appname)
        self.click_wait_and_check(app_detail_locator, 'AppsDiag_detail')
        self.wait_until_element_is_visible(E.HOME_APPS_DIAG_APP_DETAIL_APP_NAME, timeout)
        self.element_text_should_be(E.HOME_APPS_DIAG_APP_DETAIL_APP_NAME, appname)
        self.click_wait_and_check(E.HOME_APPS_DIAG_DETAIL_CLOSE)
        self.close_webUI()

    def send_cgi(self,cmd):
        device_ip = self.uut[self.Fields.internal_ip_address]
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(device_ip)
        user_data = {'cmd':'wd_login','username':'admin','pwd':''}
        cmd_url = 'http://{0}/cgi-bin/apkg_mgr.cgi?cmd={1}'.format(device_ip, cmd)
        
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))
        
        r = s.get(cmd_url)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))
        
        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

homePageUserCanViewInstalledApps()