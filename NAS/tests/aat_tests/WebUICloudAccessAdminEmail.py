__author__ = 'fitbranded'
'''
Created on August 13, 2015
@Author: O.K.
'''

from testCaseAPI.src.testclient import TestClient
import time
staging_server = '0'
Cloud_Email_Address = 'okhub-s10@yahoo.com'
# stage10 please refer to the comments on "testclient.py"
# the set and get functions of staging server. i.e. "set_staging_server"

class WebUIFullTestCloudAccessPage(TestClient):
    def run(self):
        try:
            test_name = 'WebUICloudAccessAdminEmail'
            self.start_test(test_name)
            self.set_staging_server(staging_server)
            self.get_to_page('Cloud Access')
            self.click_element('cloud_account_admin')
            self.click_button('cloud_signUp_button')
            self.input_text(locator='cloud_mail_text', text=Cloud_Email_Address)
            self.click_button('cloud_editMailSave_button')
            self.logout()
            self.login_webUI()
            self.get_to_page('Cloud Access')
            conn_status = self.get_text('cloud_status_value')
            email_status = self.get_text('css=div.email')

            time.sleep(10)
            if conn_status == 'Connected' and email_status == Cloud_Email_Address:
                self.log.info('successfully signed up the email address for Cloud - Connection is established')
                self.pass_test()
            else:
                self.fail_test('WebUICloudAccessAdminEmail')

            # else:
            #     mac_add_text = self.get_text(self.Elements.NETWORK_MAC_ADDRESS)
            #     if (mac_add_text != '') and (mac_add_text is not None):
            #         self.pass_test(testname, 'MAC ADDRESS = {0}'.format(mac_add_text))
            #     else:
            #         self.fail_test(testname, 'Failed to read MAC Address')
        except Exception, ex:
            self.log.warning('unable to access web UI' + str(ex))

WebUIFullTestCloudAccessPage()