from testCaseAPI.src.testclient import TestClient
from selenium.webdriver.common.keys import Keys
import time
from global_libraries.testdevice import Fields
import os.path

class remoteBackup(TestClient):
    # Create tests as function in this class
    
    def run(self):
        self.execute('rm /shares/Public/file*')
        self.execute('rm -rf /shares/SmartWare/*')
        self.execute('rm -rf /shares/TimeMachineBackup/*')
        reference_unit_ip = '192.168.11.39' 
        UUT_ip = self.uut[Fields.internal_ip_address] 
        print 'Current UUT ip address is {0}'.format(UUT_ip)  
        file_size = 1024        
        file = self.create_random_file('/shares/Public/', filename='file0.jpg', blocksize=1024, count=file_size)
        hash = self.md5_checksum('/shares/Public/', 'file0.jpg')
        print hash
        self.enable_remote_backup(reference_unit_ip, UUT_ip)
        self.create_manual_backup(reference_unit_ip, UUT_ip)
        self.scheduled_remote_backup(reference_unit_ip, UUT_ip)
        self.recover_backup_data(reference_unit_ip, UUT_ip)
        self.abort_recovery_backup(reference_unit_ip, UUT_ip)
        self.backup_single_double_digit(reference_unit_ip, UUT_ip)
        self.backup_during_standby(reference_unit_ip, UUT_ip)
#         self.backup_alert_messages(reference_unit_ip, UUT_ip)
        self.synchronized_remote_backup(reference_unit_ip, UUT_ip)

         
    def enable_remote_backup(self, reference_unit_ip, UUT_ip):
        self.get_to_page('Settings')
        self.click_element('settings_network_link')
        remote_server_text = self.get_text('css = #settings_networkRemoteServer_switch+span .checkbox_container')
        if remote_server_text == 'OFF':
            self.click_element('css = #settings_networkRemoteServer_switch+span .checkbox_container')
        visible = self.is_element_visible('settings_networkRemoteServerPW_password')
        if visible == True:
            self.input_text('settings_networkRemoteServerPW_password', 'fituser')
            self.click_element('settings_networkRemoteServerSave_button')
        
        self.go_to_url('http://{0}/'.format(reference_unit_ip))
        time.sleep(3)
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test1')
        self.input_text('backups_rIP_text', UUT_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare0_chkbox+span')
        self.click_element('backups_rNext5_button')
        self.click_element('backups_rCreateJobSave_button')
        time.sleep(10)
        hash = self.md5_checksum('/shares/Public/', 'file0.jpg')
        print hash
        if hash != None:
            print "Enable remote backup is successful"
        
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
    def create_manual_backup(self, reference_unit_ip, UUT_ip):
        self.go_to_url('http://{0}/'.format(reference_unit_ip))
        self.get_to_page('Settings')
        self.click_element('settings_network_link')
        remote_server_text = self.get_text('css = #settings_networkRemoteServer_switch+span .checkbox_container')
        if remote_server_text == 'OFF':
            self.click_element('css = #settings_networkRemoteServer_switch+span .checkbox_container')
        visible = self.is_element_visible('settings_networkRemoteServerPW_password')
        if visible == True:
            self.input_text('settings_networkRemoteServerPW_password', 'fituser')
            self.click_element('settings_networkRemoteServerSave_button')
        self.go_to_url('http://{0}/'.format(UUT_ip))
        time.sleep(3)
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test1')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare1_chkbox+span')
        self.click_element('backups_rNext5_button')
        self.click_element('backups_rCreateJobSave_button')
        time.sleep(90)

        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/SmartWare/')
        location = list[1]
        hash = self.md5_checksum('/shares/SmartWare/{0}/remote_test1/Public/'.format(location), 'file0.jpg')
        print hash
        if hash != None:
            print 'Create manual remote backup is successful'
        self.execute('rm -rf /shares/Smartware/W*')
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
    def scheduled_remote_backup(self, reference_unit_ip, UUT_ip):
        
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        print 'Starting daily remote backup'
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test2-1')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rHour_select')
        self.click_element('link=2:00')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.05-01:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test2-1/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for daily backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
        print 'Starting weekly remote backup'
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test2-2')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rWeekly_button')
        self.click_element('backups_rHour2_select')
        self.click_element('link=2:00')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.05-01:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test2-2/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for weekly backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
        print 'Starting remote monthly backup'
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test2-3')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rMonthly_button')
        self.click_element('backups_rHour3_select')
        self.click_element('link=2:00')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.01-01:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test2-3/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')

    def recover_backup_data(self, reference_unit_ip, UUT_ip):
        print 'Starting recovery of backup data'
        file2 = self.create_zero_file('/shares/Public/', filename='zero.jpg', blocksize=102400, count=1024)
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test3')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        time.sleep(5)
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
#             self.click_element('backups_internal_link')
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare1_chkbox+span')
        self.click_element('backups_rNext5_button')
        self.click_element('backups_rCreateJobSave_button')
        time.sleep(90)
        hash1 = self.md5_checksum('/shares/Public/', 'zero.jpg')
        print hash1
        if hash1 != None:
            print 'Verifying zero file is still in Public folder. Removing file now'
        self.execute('rm /shares/Public/zero*')
        hash2 = self.md5_checksum('/shares/Public/', 'zero.jpg')
        if hash2 != hash1:
            print 'Removed zero file. Now going to recover backup data'
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rRecoverNow0_link')
            time.sleep(3)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
        time.sleep(90)
        hash3 = self.md5_checksum('/shares/Public/', 'zero.jpg')
        if hash3 == hash1:
            print 'Zero file has been recovered'
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
            
    def abort_recovery_backup(self, reference_unit_ip, UUT_ip):
        print 'Beginning abort recovery backup'
        hash1 = self.md5_checksum('/shares/Public/', 'zero.jpg')
        file2 = self.create_zero_file('/shares/Public/', filename='zero.jpg', blocksize=102400, count=1024)
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test4')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
#             self.click_element('backups_internal_link')
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare1_chkbox+span')
        self.click_element('backups_rNext5_button')
        self.click_element('backups_rCreateJobSave_button')
        time.sleep(120)
        self.execute('rm /shares/Public/zero.jpg')
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rRecoverNow0_link')
            time.sleep(3)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')

        visible = self.is_element_visible('backups_rDel0_link')
        while visible == False:
            self.click_element('backups_rStop0_link')
            time.sleep(5)
            visible = self.is_element_visible('backups_rDel0_link')
        hash2 = self.md5_checksum('/shares/Public/', 'zero.jpg')
        if hash2 != hash1:
            print 'Recovery of backup has successfully aborted'
        else:
            self.log.error('Recovery abort failed')
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        self.execute('rm /shares/Public/zero.jpg')
        
    def backup_single_double_digit(self, reference_unit_ip, UUT_ip):
        print 'Starting remote backup schedule at 6AM'
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test3-1')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rHour_select')
        self.click_element('link=6:00')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.05-05:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test3-1/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for 6AM backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
        print 'Starting remote backup schedule at 6PM'
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test3-2')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rHour_select')
        self.click_element('link=6:00')
        time.sleep(5)
        self.click_element('backups_rDailyAMPM_select')
        self.click_element('link=PM')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.05-17:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test3-2/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for 6PM backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
        print 'Starting remote backup schedule at 12AM'
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test3-3')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rHour_select')
        self.click_element('link=6:00')
        self.click_element('backups_rDailyAMPM_select')
        self.click_element('link=PM')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.04-23:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test3-3/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for 12AM backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
        print 'Starting remote backup schedule at 12PM'
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test3-4')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rHour_select')
        self.click_element('link=6:00')
        self.click_element('backups_rDailyAMPM_select')
        self.click_element('link=PM')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('date -s 2015.01.05-11:59:00')
        time.sleep(90)
        
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test3-4/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for 12PM backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
            
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
    def backup_during_standby(self, reference_unit_ip, UUT_ip):
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test4-1')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        time.sleep(3)
        self.click_element('css = #backups_rAutoupdate_switch+span .checkbox_container')
        self.click_element('backups_rHour_select')
        self.click_element('link=6:00')
        self.click_element('backups_rDailyAMPM_select')
        self.click_element('link=PM')
        self.click_element('backups_rCreateJobSave_button')
        self.execute('rm -rf /shares/TimeMachineBackup/test*')
        self.execute('date -s 2015.01.01-01:44:00')
        print 'Waiting for standby'
        time.sleep(600)
        print 'Elapsed 10 minutes'
        time.sleep(480)
        print '15 minutes passed for wait time'
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash2 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test4-1/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash1)
        print 'MD5 hash of reference unit file is {0}'.format(hash2)
        if hash1 == hash2:
            print 'Checksum for standby backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
    
    def backup_alert_messages(self, reference_unit_ip, UUT_ip):
        self.create_zero_file('/shares/Public/', 'zero_file.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test5-1')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        self.click_element('backups_rCreateJobSave_button')
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        ip = self.execute('ifconfig')
        ip = ip[1]
        network = ip[:5]
        time.sleep(30)
        self.serial_write('ifconfig {} down'.format(network))
        time.sleep(900)
 
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.get_to_page('Backups')  
        self.click_element('backups_remote_link')
        self.click_element('id_alertIcon')
        self.click_element('home_alertDetail1_link')
        alert = self.get_text('id_alert_msg')
        print alert
        if alert == 'Remote Backup Error':
            print 'Remote backup encountered an error. Test passed'
        self.click_element('home_alertClose_button')
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        self.serial_write('ifconfig {} up'.format(network))  
        time.sleep(60)
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
         
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rBackupNow0_link')
            time.sleep(3)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        time.sleep(120)
        self.execute('rm -rf /shares/Public/zero_file.jpg')
        time.sleep(15)
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rRecoverNow0_link')
            time.sleep(3)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        ip = self.execute('ifconfig')
        ip = ip[1]
        network = ip[:5]
        time.sleep(30)
        self.serial_write('ifconfig {} down'.format(network))
        time.sleep(900)
        self.get_to_page('Backups')  
        self.click_element('backups_remote_link')
        self.click_element('id_alertIcon')
        self.click_element('home_alertDetail1_link')
        alert = self.get_text('id_alert_msg')
        print alert
        if alert == 'Remote Backup Restore Error':
            print 'Remote backup restore encountered an error as intended. Test passed'
        self.click_element('home_alertClose_button')
        time.sleep(15)
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rRecoverNow0_link')
            time.sleep(3)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        time.sleep(120)
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        
    def synchronized_remote_backup(self, reference_unit_ip, UUT_ip):
        hash0 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        self.click_element('backups_rCreateJob_button') 
        self.input_text('backups_rJobName_text', 'remote_test5-1')
        self.input_text('backups_rIP_text', reference_unit_ip)
        self.input_text('backups_rpw_password', 'fituser')
        self.input_text('backups_rSSHPW_password', 'welc0me')
        visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
        self.log.info(visible)
        while visible == False:
            time.sleep(10)
            self.click_element('backups_rBrowseSource_button')
            time.sleep(5)
            visible = self.is_element_visible('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span')
            self.log.info(visible)
        self.click_element('//div[@id=\'rsync_tree_div\']/ul/li[1]/label/span') 
        self.click_element('backups_rNext2_button')
        self.click_element('backups_rBrowseDest_button')
        time.sleep(3)
        self.click_element('css = .LightningCheckbox #backups_rSelShare2_chkbox+span')
        self.click_element('backups_rNext5_button')
        self.click_element('backups_rType_select')
        self.click_element('link=Synchronize')
        self.click_element('backups_rCreateJobSave_button')
        time.sleep(90)
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash1 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test5-1/Public/'.format(location), 'file0.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash0)
        print 'MD5 hash of reference unit file is {0}'.format(hash1)
        if hash0 == hash1:
            print 'Checksum for standby backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        file2 = self.create_random_file('/shares/Public/', filename='file2.jpg', blocksize=1024, count=1024)
        file3 = self.create_random_file('/shares/Public/', filename='file3.jpg', blocksize=1024, count=1024)
        hash2 = self.md5_checksum('/shares/Public/'.format(location), 'file2.jpg')
        hash3 = self.md5_checksum('/shares/Public/'.format(location), 'file3.jpg')
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rBackupNow0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        time.sleep(90)
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash4 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test5-1/Public/'.format(location), 'file2.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash2)
        print 'MD5 hash of reference unit file is {0}'.format(hash4)
        if hash2 == hash4:
            print 'Checksum for synchronized backup has passed'
        else:
            self.log.error('Checksum did not pass')
            
        hash5 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test5-1/Public/'.format(location), 'file3.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash3)
        print 'MD5 hash of reference unit file is {0}'.format(hash5)
        if hash3 == hash5:
            print 'Checksum for synchronized backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        
        self.execute('rm /shares/Public/file2.jpg')
        self.execute('rm /shares/Public/file3.jpg')
        
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rBackupNow0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        time.sleep(90)
        self.uut[Fields.internal_ip_address] = reference_unit_ip
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address])
        self.ssh_connect()
        print 'Connecting via SSH to verify checksum on reference unit'
        list = self.execute('ls /shares/TimeMachineBackup/')
        location = list[1]
        hash4 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test5-1/Public/'.format(location), 'file2.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash2)
        print 'MD5 hash of reference unit file is {0}'.format(hash4)
        if hash2 != hash4:
            print 'File should not exist. Checksum for synchronized backup has passed'
        else:
            self.log.error('Checksum did not pass')
            
        hash5 = self.md5_checksum('/shares/TimeMachineBackup/{0}/remote_test5-1/Public/'.format(location), 'file3.jpg')
        print 'MD5 hash of UUT file is {0}'.format(hash3)
        print 'MD5 hash of reference unit file is {0}'.format(hash5)
        if hash3 != hash5:
            print 'File should not exist. Checksum for synchronized backup has passed'
        else:
            self.log.error('Checksum did not pass')
        self.execute('rm -rf /shares/TimeMachineBackup/{0}*'.format(location))
        self.uut[Fields.internal_ip_address] = UUT_ip
        self.ssh_connect()
        print 'Internal IP address is now {0}'.format(self.uut[Fields.internal_ip_address]) 
        
        self.get_to_page('Backups')
        self.click_element('backups_remote_link')
        visible = self.is_element_visible('popup_apply_button')
        while visible == False:
            self.click_element('backups_rDel0_link')
            time.sleep(5)
            visible = self.is_element_visible('popup_apply_button')
            time.sleep(0.5)    
        self.click_element('popup_apply_button')
        

        
                
remoteBackup()
