'''
Created on Jan, 8, 2015

@author: Carlos Vasquez

Objective: To verify the correct functionality of the Scan Disk operation for the NAS
'''

import time
import re
import subprocess
from testCaseAPI.src.testclient import TestClient
volumes4bay = ['Volume_1', 'Volume_2', 'Volume_3', 'Volume_4']
volumes2bay = ['Volume_1', 'Volume_2']
volumes1bay = ['Volume_1']
public_folders=['Public','TimeMachineBackup', 'SmartWare']
grep_e2fsck_Command="grep -m 1 e2fsck"
File_Size=1024

class nasScanDisk(TestClient):

    def run(self):           
        self._myCleanup()
        
        # Get number of volumes
        numberVolumes = len(self.get_xml_tags(self.get_volumes()[1], 'volume_id'))
        global folder_names
        generated_file=self.generate_test_file(File_Size)
        if numberVolumes == 4:
            folder_names = volumes4bay
        elif numberVolumes == 2:
            folder_names = volumes2bay
        else:
            folder_names = volumes1bay
        
        # configure raid (JBOD)
        self._configureJBOD()
        
        self.log.info('Setting up folders\n')
        self.nasScanDisk_Initial()
        
        self.log.info('Copying files to the unit\n')
        self.nasScanDisk_FileCopy(generated_file)
        
        self.log.info('Setting max web timeout')
        self.setMaxUITimeout()
        
        self.log.info('Scanning volumes\n')
        self.nasScanDisk_All()
        
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')
        
        if modelNumber in ('BWAZ', 'BBAZ', 'KC2A','BZVM', 'LT4A', 'BNEZ', 'BWZE'):
            self.nasScanDisk_Volume2()

    def _myCleanup(self):
        self.delete_all_users("['admin','ftp','sshd']")
        self.execute('smbif -b user1')
        self.execute('smbif -b user2') 
        self.delete_all_shares()
            
    def _isProccessRunning(self, proccess):
        # Verify the e2fsck process is running
        output = self.run_on_device('ps -ef |grep {0}'.format(proccess))
        if 'e2fsck' in output:
            self.log.info('SUCCESS: the {0} process is running'.format(proccess))
        else:
            self.log.error('FAILED: the {0} process is running'.format(proccess))
                        
    def _configureJBOD(self):   
        # configure raid (JBOD) if already JBOD skip configuration
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

        if modelNumber in ('BWAZ', 'BBAZ', 'KC2A','BZVM') and (self.get_raid_mode() <> 'JBOD'):  
            self.log.info('Configuring JBOD(2-Bay...')
            self.configure_jbod()   
        elif modelNumber in ('LT4A', 'BNEZ', 'BWZE') and (self.get_raid_mode() <> 'JBOD'):
            self.log.info('Configuring JBOD(4-Bay)...')
            self.configure_jbod()
        else: 
            self.log.info('No raid configuration needed')
             
    def setMaxUITimeout(self):
        self.log.info('Set max UI logout timeout')   
        time.sleep(1)
        self.get_to_page('Settings')    
        time.sleep(1)
        self.wait_until_element_is_visible('settings_general_link')
        self.click_element('settings_general_link')   
        self.click_element('settings_generalTimeout_select')
        time.sleep(1)
        self.drag_and_drop_by_offset('css=div.jspDrag', 0, 150)            
        self.click_element('settings_generalTimeoutLi26_select')

    def nasScanDisk_Initial(self):
        time.sleep(2)
        self.get_to_page('Shares')
        for n in folder_names:
            time.sleep(2)
            self.wait_until_element_is_visible('shares_createShare_button')
            self.click_element('shares_createShare_button')
            time.sleep(1)
            if folder_names != volumes1bay:
                self.wait_until_element_is_visible('shares_volume_select')
                time.sleep(1)
                self.click_element('shares_volume_select')
                self.click_element('link='+n)
            self.input_text('shares_shareName_text',"test_"+n, False)
            self.input_text('shares_shareDesc_text',n, False)
            time.sleep(1)
            self.click_element('shares_createSave_button')
            time.sleep(4)
 
    def nasScanDisk_FileCopy(self, generated_file):
        for x in public_folders:
            status=self.write_files_to_smb(files=generated_file,share_name=x, delete_after_copy=False)
            
        for x in folder_names:
            status=self.write_files_to_smb(files=generated_file,share_name="test_"+x, delete_after_copy=False)

    def check_e2fsck(self):
        self.log.info('Starting to check if process e2fsck exists')
        returnvalue= self.execute("ps -a|"+grep_e2fsck_Command)
        if returnvalue[1].find(grep_e2fsck_Command)==-1:
            return True
        else:
            return False          
       
    def nasScanDisk_Select_Volume(self, volume='allVolumes'):
        self.log.info('Scan Seleted Volumes')
        if volume=='allVolumes':
            volume=0    
        volume= 'settings_utilitiesScanDiskLi'+str(volume)+'_select'
        time.sleep(1)
        self.get_to_page('Settings')    
        time.sleep(1)
        self.wait_until_element_is_visible('settings_utilities_link')
        self.click_element('settings_utilities_link')
        time.sleep(1)
        if folder_names != volumes1bay:    
            self.click_element('settings_utilitiesScanDisk_select')
            self.click_element(volume)
        self.click_element('settings_utilitiesScanDisk_button')
        time.sleep(1)
        self.click_element('popup_apply_button')
        time.sleep(5)

    def nasScanDisk_All(self):
        self.log.info('Scanning all volumes, please stand by ...')   
        self.nasScanDisk_Select_Volume()
        
        while True:
            time.sleep(5)
            text_result=self.get_text ('scandsk_percent')
            split_result=text_result.split(' ')
            if split_result[1]=='Initializing':
                self.log.info('scanning message: Initializing')
            else:
                break               
        v1=False
        v2=False
        v3=False
        v4=False
        
        while True:
            time.sleep(5)
    
            if self.is_element_visible('scandsk_percent'):
                text_result=self.get_text('scandsk_percent')
                
                self._isProccessRunning('e2fsck')
                
                if str(text_result) != "":
                    split_result=text_result.split(' ')
                    split_result=split_result[1].split('_')
                    if split_result[1]=='1':
                        self.log.info('scanning message: scanning volume 1')
                        v1=True
                    elif split_result[1]=='2':
                        self.log.info('scanning message: scanning volume 2')
                        v2=True
                    elif split_result[1]=='3':
                        self.log.info('scanning message: scanning volume 3')
                        v3=True
                    elif split_result[1]=='4':
                        self.log.info('scanning message: scanning volume 4')
                        v4=True                
                    else:
                        pass
                
            elif self.is_element_visible('scandsk_reslst'):
                if v1 and (folder_names == volumes1bay):
                    self.nasScanDisk_CheckResult()
                    break
                                    
                elif v1&v2&(folder_names == volumes2bay):
                    self.nasScanDisk_CheckResult()
                    break
                
                elif v1&v2&v3&v4&(folder_names == volumes4bay):
                    self.nasScanDisk_CheckResult()
                    break
                else:
                    raise Exception('Not all volumes were scanned')            
             
    def nasScanDisk_Volume2(self):
        self.log.info('Starting to run scan volume 2') 
        self.nasScanDisk_Select_Volume(2)
        
        while True:
            time.sleep(3)
            text_result=self.get_text ('scandsk_percent')
            split_result=text_result.split(' ')
            if split_result[1]=='Initializing':
                self.log.info('scanning message: Initializing')
            else:
                break   
            
        while True:
            time.sleep(3)
            if self.is_element_visible('scandsk_percent'):
                text_result=self.get_text ('scandsk_percent')
                
                self._isProccessRunning('e2fsck')
                    
                    
                if str(text_result) != "":
                    split_result=text_result.split(' ')
                    split_result=split_result[1].split('_')
                    if split_result[1]=='2':
                        self.log.info('Scanning volume 2')
                    elif split_result[1]=='1':
                        raise Exception('Test Failed: Scanned volume 1 instead of volume 2')
                    elif split_result[1]=='3':
                        raise Exception('Test Failed: Scanned volume 3 instead of volume 2')
                    elif split_result[1]=='4':
                        raise Exception('Test Failed: Scanned volume 4 instead of volume 2')                    
                    else:
                        pass
                else:
                    time.sleep(3)
                    break
            else:
                break

        self.nasScanDisk_CheckResult()           
            
    def nasScanDisk_CheckResult(self):
        self.log.info('Checking scan disk results')        
        self.wait_until_element_is_visible('scandsk_reslst', 2)
        text_result=self.get_text('scandsk_reslst')
        new_result=text_result.split('\n')
        n=0
        for x in new_result:
            if n==1:
                split_result=x.split('.')
                status_result=split_result[1]
                if split_result[1]==" No errors found":
                    self.log.info('SUCCESS: No error(s) found during scan disk')
                else:       
                    raise Exception('Error(s) found in during scan disk')                  
                n=0
            elif n==0:
                n=1

        self.click_element('settings_utilitiesScanDiskFinish_button')     
        time.sleep(20)
        self.wait_until_element_is_visible('settings_utilitiesScanDisk_button')

nasScanDisk()


