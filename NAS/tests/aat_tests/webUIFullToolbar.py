'''
Created on Mar 13th, 2015

@author: tsui_b

Objective: Verify tab order on each page and that all links, buttons, drop-down boxes, 
           keyboard shortcuts etc. in the web UI lead to legitimate pages or display legitimate values.
Wiki URL: http://wiki.wdc.com/wiki/Web_UI
'''
 
import time
import sys
import re
import requests
import csv 
import os

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
from seleniumAPI.src.ui_map import ElementInfo
from selenium.common.exceptions import StaleElementReferenceException

timeout = 60  # Use for wait_until_element_is_(not_)vislble
new_help_link = ['LT4A', 'KC2A', 'GLCR'] # since fw. 2.00.036

link_device_id = {'GLCR':'g2',          # Glacier
                  'BZVM':'mirror',      # Zion
                  'BWVZ':'mirror',      # Grand Teton
                  'KC2A':'ex2',         # Kings Canyon
                  'LT4A':'ex4',         # Lightning
                  'BBAZ':'dl2100',      # Aurora
                  'BNEZ':'dl4100',      # Sprite
                  'BWAZ':'ex2100',      # Yosemite
                  'BWZE':'ex4100'}      # Yellowstone

link_device_name = {'GLCR':'mcg2',      # Glacier
                    'BZVM':'mcm',       # Zion
                    'BWVZ':'mcmg2',     # Grand Teton
                    'KC2A':'mcp2',      # Kings Canyon
                    'LT4A':'mcp4',      # Lightning
                    'BBAZ':'mcdl2100',  # Aurora
                    'BNEZ':'mcdl4100',  # Sprite
                    'BWAZ':'mcex2100',  # Yosemite
                    'BWZE':'mcex4100'}  # Yellowstone
model_number = ''
platform_name = ''
usb_mount_list = []
picturecounts = 0

class webUIFullToolbar(TestClient):
        
    def run(self):
        global model_number
        global platform_name
        global usb_mount_list
        
        # Get USB original mountpoints to remount it after it is ejected
        usb_info = self.execute('df | grep USB')
        if usb_info[1]:
            usb_list = usb_info[1].split('\n')
            for usb in usb_list:
                r = re.search('_(?P<value>.)', usb)
                usb_mount_list.append('sd{}'.format(r.group(1)))

        model_number = self.get_model_number()
        platform_name = 'My Cloud'
        if model_number not in ['GLCR']:
            # Glacier doesn't have 'G2' in platform_name
            if model_number in ['BZVM', 'BWVZ']:
                # The platform_name is 'Mirror' not 'MIRROR' in Zion and Grand Teton
                platform_name += ' {}'.format(link_device_id.get(model_number).title())
            else:
                platform_name += ' {}'.format(link_device_id.get(model_number).upper())

        toolbar_list = [self.toolbar_device, self.toolbar_notification, self.toolbar_help_gettingstarted,
                        self.toolbar_help_help, self.toolbar_help_support, self.toolbar_help_about, self.toolbar_logout]
        for toolbar_tab in toolbar_list:
            self.start_test(toolbar_tab.__name__)
            try:
                toolbar_tab()
                self.pass_test(toolbar_tab.__name__)
            except Exception as e:
                self.takeScreenShot(prefix=toolbar_tab.__name__)
                self.close_webUI()
                self.fail_test(toolbar_tab.__name__, 'Verify tab: {0} failed! Exception: {1}'.format(toolbar_tab.__name__, repr(e)))

    def tc_cleanup(self):
        self.delete_all_users()
        self.re_mount_usbs()
    
    def re_mount_usbs(self):
        if usb_mount_list:
            self.log.debug('Original USB mount points:{}'.format(usb_mount_list))
            for index, mountpoint in enumerate(usb_mount_list):
                self.log.debug('Remounting USB{0} on /dev/{1}'.format(index+1, mountpoint))
                self.execute('usbmount {}'.format(mountpoint))
        else:
            self.log.debug('There are no USB devices need to be remounted.')

    def check_element(self, element, cancel_button=None, expected_label=None, expected_string=None, element_name_suffix=None):
        # Wrapper for validate_element to allow catching exceptions and logging them instead of ending the test
        element_name = element.name
        if element_name_suffix:
            element_name += '-' + str(element_name_suffix)
        
        self.start_test(element_name)

        attempts = 5
        for attempts_remaining in xrange(attempts, 0, -1):
            try:
                self.validate_element(element, cancel_button, expected_label, expected_string)
                self.pass_test(element_name)
                break
            except StaleElementReferenceException:
                self.log.debug('Stale element. Try again.')
                continue
            except Exception as e:
                self.takeScreenShot(prefix=element_name)
                self.fail_test(element_name, repr(e))
                break

    def build_element_info(self, name, locator):
        name = name
        locator = locator
        page = None
        verification_element = None
        control_type = None
        is_hidden=False
        is_disabled=False
        
        element_info = ElementInfo(name=name,
                                   locator=locator,
                                   page=page,
                                   verification_element=verification_element,
                                   control_type=control_type,
                                   is_hidden=is_hidden,
                                   is_disabled=is_disabled)
        return element_info
    
    def toolbar_device(self):
        self.get_to_page('Home')
        # Check how many usb devices is connected
        usb_device_name = self.get_xml_tags(self.get_usb_info()[1], tag='name')
        usb_manufacturer = self.get_xml_tags(self.get_usb_info()[1], tag='vendor')
        usb_model = self.get_xml_tags(self.get_usb_info()[1], tag='model')
        usb_serial = self.get_xml_tags(self.get_usb_info()[1], tag='serial_number')
        usb_firmware = self.get_xml_tags(self.get_usb_info()[1], tag='revision')
        # Todo: The size on Web UI is incorrect, ITR #105018
        # usb_size = self.get_xml_tags(self.get_usb_info()[1], tag='capacity')
        
        usb_device_number = len(usb_device_name)
        if usb_device_number:
            # Check the information of first usb device and eject it, until there are no more usb devices.
            for i in xrange(1, len(usb_device_name)+1):
                self.log.info('Try if USB %s existed.' % i)
                self.click_wait_and_check(E.TOOLBAR_DEVICE, E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_ARROW)
                # Verify if the detail button work
                self.click_wait_and_check(E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_ARROW, E.TOOLBAR_DEVICE_DETAIL_DIAG)
                self.log.info('Verifing detail info of USB %s: %s' % (i, usb_device_name[i-1]))
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TITLE, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TEXT1, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_INFO1, expected_string=usb_device_name[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TEXT2, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_INFO2, expected_string=usb_manufacturer[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TEXT3, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_INFO3, expected_string=usb_model[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TEXT4, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_INFO4, expected_string=usb_serial[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TEXT5, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_INFO5, expected_string=usb_firmware[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_TEXT6, element_name_suffix=i)
                # self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_INFO6, expected_string=usb_size[i-1])
                
                self.check_element(E.TOOLBAR_DEVICE_DETAIL_DIAG_CLOSE, element_name_suffix=i)
                self.click_wait_and_check(E.TOOLBAR_DEVICE_DETAIL_DIAG_CLOSE)
                self.log.info('Verified detail info of USB %s: %s' % (i, usb_device_name[i-1]))
                # Add 5 secs sleep after close USB device diag
                time.sleep(5)
                
                # Click device button again and verify if the eject button work
                self.click_wait_and_check(E.TOOLBAR_DEVICE, E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_EJECT)
                self.click_wait_and_check(E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_EJECT, E.TOOLBAR_DEVICE_EJECT_DIAG)
                self.log.info('Verifying eject diag of USB %s: %s' % (i, usb_device_name[i-1]))
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_TITLE, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_OK, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_CANCEL, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_TEXT1, element_name_suffix=i)
                
                eject_diag_text2 = 'Ejecting this USB device will remove all associated shares ' \
                                 + 'with the ' + platform_name + ' system.' 
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_TEXT2, expected_string=eject_diag_text2, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_TEXT3, element_name_suffix=i)
                self.check_element(E.TOOLBAR_DEVICE_EJECT_DIAG_TEXT4, element_name_suffix=i)
                self.click_wait_and_check(E.TOOLBAR_DEVICE_EJECT_DIAG_CANCEL)
                self.log.info('Verified eject diag of USB %s: %s' % (i, usb_device_name[i-1]))
                # Open the device diag again and try to eject the usb
                self.click_wait_and_check(E.TOOLBAR_DEVICE, E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_EJECT)
                self.click_wait_and_check(E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_EJECT, E.TOOLBAR_DEVICE_EJECT_DIAG)
                self.log.info('Ejecting USB %s: %s' % (i, usb_device_name[i-1]))
                self.click_wait_and_check(E.TOOLBAR_DEVICE_EJECT_DIAG_OK, E.UPDATING_STRING)
                self.wait_until_element_is_not_visible(E.UPDATING_STRING)
                self.log.info('USB %s: %s is ejected.' % (i, usb_device_name[i-1]))
                # Add 20 secs sleep after ejecting USB device
                time.sleep(20)
        # There are no usb devices or all devices are removed, check empty log
        self.log.info('Try if there are no any USB devices.')
        self.click_wait_and_check(E.TOOLBAR_DEVICE, E.TOOLBAR_DEVICE_EMPTY_DIAG)
        self.check_element(E.TOOLBAR_DEVICE_EMPTY_DIAG)
        self.close_webUI()

    def toolbar_notification(self):
        # Get alerts information from cgi
        alert_info = self.send_cgi('cgi_get_alert')
        alerts_msg = self.get_xml_tags(alert_info, 'msg')
        alerts_description = self.get_xml_tags(alert_info, 'desc')
        alerts_time = self.get_xml_tags(alert_info, 'time')
        alerts_code = self.get_xml_tags(alert_info, 'code')

        # Nofitication Button
        firmware_version = self.get_firmware_version_number()
        if firmware_version.startswith('2.10') or firmware_version.startswith('2.11'):
            # Old element id in 2.10.xxx and 2.11.xxx
            notification_button = 'css=#id_alert > img'            
        else:
            # The latest id since 2.20.xxx
            notification_button = E.TOOLBAR_NOTIFICATION
        
        self.get_to_page('Home')
        
        if alerts_msg:
            # Check all elements exist on notification diag and view all button work
            self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG)
            self.check_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_INFO)
            self.check_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_WARNING)
            self.check_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_CRITICAL)
            self.check_element(E.TOOLBAR_NOTIFICATION_VIEW_ALL)
            self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_VIEW_ALL, E.TOOLBAR_NOTIFICATION_VIEW_ALL_DIAG)
            self.check_element(E.TOOLBAR_NOTIFICATION_VIEW_ALL_TITLE)
            self.check_element(E.TOOLBAR_NOTIFICATION_VIEW_ALL_DISMISS)
            self.check_element(E.TOOLBAR_NOTIFICATION_VIEW_ALL_CLOSE)
            self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_VIEW_ALL_CLOSE)
            
            self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG)
            # Check the information of first notification and dismiss it, until there are no more notifications
            for i in xrange(1, len(alerts_msg)+1):
                if i > 2:
                    # It will take too long to remove all notifications one by one,
                    # so if notifications > 2, use "dismiss all" to clean the rest notifications.
                    self.log.info('Dismiss all the rest notifications.')
                    self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_VIEW_ALL, E.TOOLBAR_NOTIFICATION_VIEW_ALL_DIAG)
                    self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_VIEW_ALL_DISMISS)
                    # Click notification button again to verify empty diag
                    self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_EMPTY_DIAG)
                    break
                
                self.log.info('Try if nofitications %s existed.' % i)
                self.check_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_TITLE, expected_string=alerts_msg[i-1], element_name_suffix=i)
                
                # The time format need to be transfered into web UI's format
                # The default format is 12-hour, YYYY-MM-DD
                datetime = time.strptime(alerts_time[i-1], "%a, %b %d, %Y %I:%M:%S %p")
                datetime_new = time.strftime("%A, %Y %B %d, %I:%M:%S %p", datetime)
                # If the "hour" starts with '0' like '01', remove '0' since web UI don't show '0'
                datetime_new = re.sub(r"(.+, )(0)(\d:.+)", r"\1\3", datetime_new)

                self.check_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_PUBTIME, expected_string=datetime_new, element_name_suffix=i)
                
                self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_ARROW, E.TOOLBAR_NOTIFICATION_ALERT_DIAG)
                self.check_element(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_TITLE, element_name_suffix=i)
                self.check_element(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_MSG, expected_string=alerts_msg[i-1], element_name_suffix=i)
                # Some alert descriptions are different between Web UI and Rest API, hide it temporarily
                # self.check_element(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_DESCRIPTION, expected_string=alerts_description[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_TIME, expected_string=datetime_new, element_name_suffix=i)
                self.check_element(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_CODE, expected_string=alerts_code[i-1], element_name_suffix=i)
                self.check_element(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_CLOSE, element_name_suffix=i)
                self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_ALERT_DIAG_CLOSE)
                # Reopen notification diag and try to dismiss first message
                self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG)

                self.log.info('Deleting the first notification')
                # Delete button might not be clicked successfully, retry once if we got an exception.
                if i == len(alerts_msg):
                    # If this is the last notification, the diag should be changed to empty diag.
                    self.log.info('This is the last notification, the diag should be changed to empty diag.')
                    self.click_wait_and_check(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_DELETE, E.TOOLBAR_NOTIFICATION_EMPTY_DIAG)
                else:
                    # If this is not the last notification, first title should be changed to next notification.
                    self.log.info('The first title should be changed to next notification message.')
                    self.click_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_DELETE)
                    # Check if notification is deleted after 5 secs
                    time.sleep(5)
                    try:
                        self.element_text_should_be(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_TITLE, alerts_msg[i])
                    except:
                        self.log.info('Delete button did not click. Retry 1 time.')
                        self.click_element(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_DELETE)
                        # Check if notification is deleted after 5 secs
                        time.sleep(5)
                        self.element_text_should_be(E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_TITLE, alerts_msg[i])
        else:
            # No notifications, just click button and check empty diag
            self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_EMPTY_DIAG)

        # There are no notifications or all notification are removed, check empty diag
        self.log.info('Try if the notifications field is empty.')
        self.wait_until_element_is_visible(E.TOOLBAR_NOTIFICATION_EMPTY_DIAG, timeout)
        self.check_element(E.TOOLBAR_NOTIFICATION_EMPTY_DIAG)
        self.close_webUI()

    def toolbar_help_gettingstarted(self):
        # Get cloud access status cause some texts/links will be determined by it
        device_info = self.get_device_info()
        cloud_access = self.get_xml_tags(device_info[1], 'remote_access')[0]
        device_name = link_device_name.get(model_number)
        self.get_to_page('Home')
        # Help Button
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
        # Clear all users first, we will add new user later 
        self.delete_all_users()
        # Help Button -> Getting Started Wizard
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED, E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
        # Verify elements in the step1 page
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG_TITLE)
        help_getting_started_step1_text1 = 'WD ' + platform_name \
                                         + ' lets you create your personal cloud. Store your photos, videos, ' \
                                         + 'music, and files in one place, and access your content anywhere. ' \
                                         + 'Access requires a MyCloud.com account, and each user needs one.'
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP1_TEXT1, expected_string=help_getting_started_step1_text1)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_CLOUD_TITLE)
        # Get cloud access info
        cmd = 'cat /var/www/xml/account.xml'
        result = self.execute(cmd)
        if result[0] != 0:
            self.log.error('Get cloud access info failed! Error message:{}'.format(result[1]))
        else:
            cloud_first_name = self.get_xml_tag(result[1], 'first_name')
            cloud_last_name = self.get_xml_tag(result[1], 'last_name')
            cloud_email = self.get_xml_tag(result[1], 'email')
            
        if cloud_first_name and cloud_last_name:
            username = '{0} {1}'.format(cloud_first_name, cloud_last_name)
        else:
            username = 'admin'
        
        if cloud_email:
            email = cloud_email
        else:
            email = '-'
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_ADMIN, expected_string=username)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_ADMIN_EMAIL, expected_string=email)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP1_TEXT2)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_NEXT1)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_CANCEL1)
        # Default texts in Setup Cloud Access textfields, they're placeholder attributes
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME_TEXT)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME_TEXT)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME_TEXT)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL_TEXT)
        # Verify if the cancel picture works in every textfield
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME_DELETE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME_DELETE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME_DELETE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL_DELETE)
        # Save button will display after inputting any texts in the textfield
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USER_SAVE)
        ''' Hide below test cases temporarily, automation quick restore need to run wd2go.sh or cloud access will not work
        # Verify if new user can be added successfully, add 2 secs sleep between inputing texts
        self.input_text_check(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME, 'aatuser')
        self.input_text_check(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME, 'firstname')
        self.input_text_check(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME, 'lastname')
        self.input_text_check(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL, 'aatuser@wdc.com')
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USER_SAVE, E.UPDATING_STRING)
        # Wait 20 secs until new user is created
        time.sleep(20)
        self.element_text_should_be(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USER1_FULLNAME, 'firstname lastname')
        self.element_text_should_be(E.TOOLBAR_HELP_GETTING_STARTED_SETUP_USER1_EMAIL, 'aatuser@wdc.com')
        '''
        # Step1 -> Next to Step2
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT1, E.TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
        # Verify elements in the step2 page
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_TITLE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TITLE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1_TIP)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2_TIP)        
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM2_TITLE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM2_TEXT1)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_BACK2)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_CANCEL2)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_NEXT2)
        # Try if hover will show tip information
        self.mouse_over(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1_TIP)
        self.current_frame_contains(self.default_values[E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1_TIP])
        time.sleep(2) # Wait for the tip shows up
        self.mouse_out(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1_TIP)
        self.mouse_over(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2_TIP)
        self.current_frame_contains(self.default_values[E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2_TIP])
        time.sleep(2) # Wait for the tip shows up
        self.mouse_out(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2_TIP)
        # Default texts in Setup Cloud Access textfields, they're placeholder attributes
        # Need to clear default account info if exist
        if cloud_first_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_DELETE)
        if cloud_last_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_DELETE)
        if cloud_email:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_DELETE)
            
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_TEXT)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_TEXT)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_TEXT)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_DELETE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_DELETE)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL,
                           cancel_button=E.TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL_DELETE)
        
        # Step2 -> Next to Step3
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT2, E.TOOLBAR_HELP_GETTING_STARTED_STEP3_DIAG)
        
        # The text will be different depend on the cloud_access is on or off
        if cloud_access == 'true':
            getting_started_step3_text1 = 'That\'s it. You\'ll receive an email from MyCloud.com with our login info.\nClick these links for additional software and info:'
        else:
            getting_started_step3_text1 = 'That\'s it. Click these links for additional software and info:'
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_STEP3_TEXT1, expected_string=getting_started_step3_text1)

        # The link url will be different depend on the cloud_access is on or off
        if cloud_access == 'true':
            upload_media_url = 'http://products.wdc.com/mycloud/?type=upload&device={}'.format(device_name)
            back_it_up_url = 'http://products.wdc.com/mycloud/?type=backup&device={}'.format(device_name)
            access_content_url = 'http://products.wdc.com/mycloud/?type=access&device={}'.format(device_name)
        else:
            upload_media_url = 'http://products.wdc.com/mycloud/?type=uploadlocal&device={}'.format(device_name)
            back_it_up_url = 'http://products.wdc.com/mycloud/?type=backuplocal&device={}'.format(device_name)
            access_content_url = 'http://products.wdc.com/mycloud/?type=accesslocal&device={}'.format(device_name)
        
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_HELP_UPDATE, expected_string=upload_media_url)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_HELP_BACKUP, expected_string=back_it_up_url)
        self.check_element(E.TOOLBAR_HELP_GETTING_STARTED_HELP_MOBILE, expected_string=access_content_url)

        if model_number in ['GLCR']:
            upload_media_redirect_url = 'title=My Cloud'
            back_it_up_redirect_url = 'title=My Cloud'
            access_content_redirect_url = 'title=My Cloud'
        else:
            help_url = 'url=http://setup.wd2go.com/'
            upload_media_redirect_url = help_url + '?mod=faqs&device={}&faq=uploadMedia'.format(device_name)
            back_it_up_redirect_url = help_url + '?mod=faqs&device={}&faq=backItUp'.format(device_name)
            access_content_redirect_url = help_url + '?mod=faqs&device={}&faq=accessContent'.format(device_name)
        # Try to clink the link and verify if new pop-up window can be selected by url
        # Below pages are not ready in Grand Teton FW.2.10.232
        if model_number not in ['BWVZ']:
            self.click_and_check_url(E.TOOLBAR_HELP_GETTING_STARTED_HELP_UPDATE, upload_media_redirect_url)
            self.click_and_check_url(E.TOOLBAR_HELP_GETTING_STARTED_HELP_BACKUP, back_it_up_redirect_url)
            self.click_and_check_url(E.TOOLBAR_HELP_GETTING_STARTED_HELP_MOBILE, access_content_redirect_url)
        
        # Check each button can be linked to the right pages
        # Step3 -> Back to Step2 -> Back to Step1 -> Cancel
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_BACK3, E.TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_BACK2, E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_CANCEL1)
        # Click Getting Started button again
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED, E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
        # Step1 -> Next to Step2 -> Cancel
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT1, E.TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_CANCEL2)
        # Click Getting Started button again
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED, E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
        # Step1 -> Next to Step2 -> Next tp Step3 -> Cancel
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT1, E.TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
        # Need to clear default account info if exist
        if cloud_first_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_DELETE)
        if cloud_last_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_DELETE)
        if cloud_email:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_DELETE)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT2, E.TOOLBAR_HELP_GETTING_STARTED_STEP3_DIAG)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_CANCEL3)
        # Click Getting Started button again
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED, E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
        # Step1 -> Next to Step2 -> Next tp Step3 -> Finish
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT1, E.TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
        # Need to clear default account info if exist
        if cloud_first_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_DELETE)
        if cloud_last_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_DELETE)
        if cloud_email:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_DELETE)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT2, E.TOOLBAR_HELP_GETTING_STARTED_STEP3_DIAG)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_FINISH)
        self.close_webUI()
    
    def toolbar_help_help(self):
        # Get help information by model_number
        with open('help_pages/{}.csv'.format(model_number)) as help_file:
            help_info = csv.reader(help_file)
        
            # Check every help page link is correct
            self.get_to_page('Home')
            # Help Button -> Help Link
            self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
            self.check_element(E.TOOLBAR_HELP_HELP)
            self.click_wait_and_check(E.TOOLBAR_HELP_HELP, E.TOOLBAR_HELP_HELP_DIAG)
    
            # Verify the link on search button in the iframe
            self.click_help_tab(E.TOOLBAR_HELP_HELP_SEARCH, E.TOOLBAR_HELP_HELP_LEFT_SEARCH)
            # Verify the link on content button in the iframe
            self.click_help_tab(E.TOOLBAR_HELP_HELP_CONTENT, E.TOOLBAR_HELP_HELP_LEFT_CONTENT)
    
            for index, row in enumerate(help_info):
                expected_string = row[0]
                locator = row[1]
                element = self.build_element_info(name='HELP_{0}_{1}'.format(index, expected_string.strip()), locator=locator)
                try:
                    self.check_help_page(element=element,
                                         expected_string=expected_string)
                except Exception as e:
                    self.log.error('Verify help page: "{}" failed. Exception: {}'.format(element.name, repr(e)))
                    self.unselect_frame()
                        
            # Close the wizard
            self.click_wait_and_check(E.TOOLBAR_HELP_HELP_DIAG_OK)
        self.close_webUI()

    def toolbar_help_support(self):
        self.get_to_page('Home')
        # Help Button -> Support Link
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
        self.check_element(E.TOOLBAR_HELP_SUPPORT)
        self.click_wait_and_check(E.TOOLBAR_HELP_SUPPORT, E.TOOLBAR_HELP_SUPPORT_DIAG)
        # Param1
        self.check_element(E.TOOLBAR_HELP_SUPPORT_DIAG_TITLE)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM1_TITLE)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM1_TEXT1)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM1_CHECKBOX1)

        # Kings Canyon, Lightning, Zion, Grand Teton urls are differnt
        if model_number in ['KC2A', 'LT4A', 'BZVM', 'BWVZ']:
            help_url = 'http://products.wd.com/?id=wdfmycloud_'
        else:
            help_url = 'http://products.wdc.com/?id=wdfmycloud_'

        support_privacy_policy_url = help_url + link_device_id.get(model_number)
        # Grand Teton has g2 after model_number in the links
        if model_number in ['BWVZ']:
            support_privacy_policy_url += 'g2'
        support_privacy_policy_url += '&type=privacypolicy'
        
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PRIVACY_POLICY, expected_string=support_privacy_policy_url)
        # try to clink the link and verify if new pop-up window can be selected by title
        # Glacier, Grand Teton pages are not ready in FW.2.10.239
        if model_number not in ['GLCR', 'BWVZ']:
            self.click_and_check_url(E.TOOLBAR_HELP_SUPPORT_PRIVACY_POLICY, 'title=Privacy Policy')
        # Param2
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM2_TITLE)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM2_TEXT1)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_CREATE_AND_SAVE)
        # Param3
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM3_TITLE)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM3_TEXT1)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM3_TEXT2)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM3_TEXT3)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM3_TEXT4)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM3_TEXT5)
        # Param4
        self.check_element(E.TOOLBAR_HELP_SUPPORT_PARAM4_TITLE)
        help_url = 'http://products.wdc.com/?id=wdfmycloud_' + link_device_id.get(model_number) 
        # Grand Teton has g2 after model_number in the links
        if model_number in ['BWVZ']:
            help_url += 'g2'
        help_url += '&type='
        support_doc_url = help_url + 'um' 
        support_faq_url = help_url + 'faq'
        support_forum_url = help_url + 'forum'
        support_contacts_url = help_url + 'contact'
        self.check_element(E.TOOLBAR_HELP_SUPPORT_DOC, expected_string=support_doc_url)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_FAQ, expected_string=support_faq_url)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_FORUM, expected_string=support_forum_url)
        self.check_element(E.TOOLBAR_HELP_SUPPORT_CONTACTS, expected_string=support_contacts_url)
        # try to clink the link and verify if new pop-up window can be selected by title
        # Below pages are not ready in Glacier and Grand Teton FW.2.10.232
        if model_number not in ['GLCR', 'BWVZ']:
            self.click_and_check_url(E.TOOLBAR_HELP_SUPPORT_DOC, 'title=WD Documentation')
            # Title is modified in FW.2.10.310
            self.click_and_check_url(E.TOOLBAR_HELP_SUPPORT_FAQ, 'title=Knowledge Base | WD Support')
            # This page is not ready in Zion FW.2.10.239
            if model_number not in ['BZVM']:
                self.click_and_check_url(E.TOOLBAR_HELP_SUPPORT_FORUM, 'title=WD Community')
            self.click_and_check_url(E.TOOLBAR_HELP_SUPPORT_CONTACTS, 'title=Contact WD')
        # Close the wizard
        self.check_element(E.TOOLBAR_HELP_SUPPORT_DIAG_OK)
        self.click_wait_and_check(E.TOOLBAR_HELP_SUPPORT_DIAG_OK)
        self.close_webUI()
        
    def toolbar_help_about(self):
        self.get_to_page('Home')
        # Help Button -> About Link
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD)
        self.check_element(E.TOOLBAR_HELP_ABOUT)
        self.click_wait_and_check(E.TOOLBAR_HELP_ABOUT, E.TOOLBAR_HELP_ABOUT_DIAG)
        self.element_should_be_visible(E.TOOLBAR_HELP_ABOUT_DIAG_LOGO)
        self.element_should_be_visible(E.TOOLBAR_HELP_ABOUT_DIAG_DEV)
        firmware_version = self.get_xml_tag(self.get_firmware_version(), 'firmware')
        firmware_text = 'Firmware Version : ' + firmware_version 
        self.check_element(E.TOOLBAR_HELP_ABOUT_DIAG_FIRMWARE_TEXT, expected_string=firmware_text)
        self.check_element(E.TOOLBAR_HELP_ABOUT_DIAG_COPYRIGHT, expected_string='/web/Copyright%20Info/Copyright%20Info.txt?v=2.00')
        self.check_element(E.TOOLBAR_HELP_ABOUT_DIAG_GPL, expected_string='/web/GNU%20Public%20License/gpl-3.0.html')
        # Try to clink the link and verify if new pop-up window can be selected by url/title
        copyright_url = 'http://' + self.uut.get('internalIPAddress') + u'/web/Copyright%20Info/Copyright%20Info.txt?v=2.00'
        self.click_and_check_url(E.TOOLBAR_HELP_ABOUT_DIAG_COPYRIGHT, 'url={}'.format(copyright_url))
        gpl_title = 'GNU General Public License - GNU Project - Free Software Foundation (FSF)'
        self.click_and_check_url(E.TOOLBAR_HELP_ABOUT_DIAG_GPL, 'title={}'.format(gpl_title))
        # Close the wizard
        self.check_element(E.TOOLBAR_HELP_ABOUT_DIAG_OK)
        self.click_wait_and_check(E.TOOLBAR_HELP_ABOUT_DIAG_OK)
        self.close_webUI()

    # @Brief: Check every help link will connect to the right page
    # @param locator: Locator of the help link
    # @param expect_string: The expected string for help link
    # @param title_element: Locator of the pages' title
    # @param expect_title: The expected string for the help pages' title
    def check_help_page(self, element, expected_string=None):
        # Need to select parent iframes before clicking help link
        self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
        self.select_frame(E.TOOLBAR_HELP_HELP_CONTENT_FRAME)
        self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L1)
        self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L2)
        self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L3)

        # Check the element of help link and click it
        # The help folder and link have a white space before expected_strings
        self.check_element(element, expected_string = ' ' + expected_string)
        
        if element.locator.startswith('B'):
            # If the locator starts with 'B', that means there are some sub links under it,
            # check if they are visible
            sub_link_column_locator = element.locator + 'C'
            self.click_wait_and_check(element, sub_link_column_locator)
            self.unselect_frame()
        else:
            # Check clicking sub links will lead to correct help pages
            # Select parent iframes before checking help page
            retry = 3
            while retry >= 0:
                try:
                    self.click_element(element)
                    self.unselect_frame()
                    self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
                    self.select_frame(E.TOOLBAR_HELP_HELP_CONTENT_FRAME)
                    self.select_frame(E.TOOLBAR_HELP_HELP_RIGHT_FRAME_L1)
                    self.current_frame_contains(expected_string)
                except Exception as e:
                    if retry == 0:
                        raise Exception(repr(e))
                    self.log.info('Help link:"{0}" did not click, remaning {1} retries...'.format(element.name, retry))
                    retry -= 1
                    self.unselect_frame()
                    self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
                    self.select_frame(E.TOOLBAR_HELP_HELP_CONTENT_FRAME)
                    self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L1)
                    self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L2)
                    self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L3)
                else:
                    break
            self.unselect_frame()
        
    def toolbar_logout(self):
        self.get_to_page('Home')
        # Logout Button
        self.click_wait_and_check(E.TOOLBAR_LOGOUT, E.TOOLBAR_LOGOUT_TABLE)
        self.check_element(E.TOOLBAR_LOGOUT_TITLE)
        # Test logout only, we do not test shutdown / reboot function temporarily.
        # Glacier doesn't have shutdown function in FW.2.10.239
        if model_number not in ['GLCR']:
            self.check_element(E.TOOLBAR_LOGOUT_SHUTDOWN)
        self.check_element(E.TOOLBAR_LOGOUT_REBOOT)
        self.check_element(E.TOOLBAR_LOGOUT_LOGOUT)
        # Will go back to login page
        self.click_wait_and_check(E.TOOLBAR_LOGOUT_LOGOUT)
        self.close_webUI()

    def send_cgi(self,cmd):
        url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd':'wd_login','username':'admin','pwd':''}
        head = 'http://{0}/cgi-bin/system_mgr.cgi?cmd='.format(self.uut[self.Fields.internal_ip_address])
        url_2 = head + cmd
        self.log.info('login cmd: '+url +'cmd=wd_login&username=admin&pwd=, setting cmd: '+url_2)
        with requests.session() as s:
            s.post(url,data=user_data)
            r = s.get(url_2)
            return r.text.encode('ascii', 'ignore')

    def click_and_check_url(self, locator, check_window, retry=3):
        while retry >= 0:
            self.log.debug('Clicking link:"{0}" and check the pop-up window:{1}'.format(locator.locator, check_window))
            try:
                self.click_element(locator)
                self.select_window(check_window)
                self.close_window()
                self.select_window()
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Link "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def click_help_tab(self, locator, check_locator, retry=3, timeout=timeout):
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if visible".format(locator.name, check_locator.name))
            try:
                self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
                self.select_frame(E.TOOLBAR_HELP_HELP_TOOLBAR_FRAME)
                self.wait_and_click(locator, timeout=timeout)
                self.unselect_frame()
                self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
                self.select_frame(E.TOOLBAR_HELP_HELP_CONTENT_FRAME)
                self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L1)
                self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L2)
                self.wait_until_element_is_visible(check_locator, timeout=timeout)
                self.unselect_frame()
            except Exception as e:
                self.unselect_frame()
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

webUIFullToolbar()
