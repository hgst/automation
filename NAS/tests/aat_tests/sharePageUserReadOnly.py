"""
title           :sharePageUserReadOnly.py
description     : Shares Page - to verify if a user can be authorized 'Read Only' access to a given folder
step            : 1. Click on 'share' category
                  2. Select a share (public = OFF)
                  3. Select a user, and give 'Read Only' privilege
                  4. Verify if this user cannot access the share
author          :yang_ni
date            :2015/06/01
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time,os
from smb.smb_structs import OperationFailure

picture_counts = 0

class sharePageUserReadOnly(TestClient):

    def run(self):

        try:
            #Delete all the users, groups and shares
            self.setup()

            # Create a user
            self.log.info('Creating a user ...')
            self.create_user(username='user1')

            # Create a share.
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=1)

            # Change share to private, share must be set to private instead of public. Toggle the Public switch off
            self.log.info('Toggling the share from private to public ...')
            self.update_share_network_access(share_name='share', public_access=False)
            #self.update_share()

            #Assigns user1 to share with the read only access level
            self.log.info('Assigning user1 with Read Only to share ...')
            self.assign_user_to_share(user_name='user1', share_name='share', access_type='r')

            #Check if 'Read Only' access of share is given to user1
            self.log.info('Checking if user1 has Read Only access to share ...')

            '''
            Check if user1 has 'Read Only' privilege to share (test on Web UI)
            '''
            time.sleep(20)
            self.get_to_page('Shares')
            time.sleep(5)
            self.click_button('shares_share_share')
            text = self.get_text("//div[@id=\'userlist\']/ul/li[2]/div[4]")

            if text == 'Read Only':
                self.log.info('User1 has read only access to share folder')
            else:
                self.log.error('Failed to authorize read only access of share folder to user1')

            self.close_webUI()

            '''
            Check if user1 has 'Read Only' privilege to share (test by SMB file transfer)
            '''
            test_file = self.generate_test_file(kilobytes=10)

            #Check if user1 has write access to the share folder, fail if yes.
            try:
                self.write_files_to_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.info('CORRECT : Failed to write file from share folder')
            else:
                self.log.error('FAIL : Successfully to write file to share folder')

            #Check if user1 has read access to the share folder, fail if yes.
            try:
                self.read_files_from_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.error('FAIL : Failed to read file from share folder')
            else:
                self.log.info('CORRECT : Successfully to read file to share folder')
                self.log.info('*** Successfully authorizes read only access to user1 ***')
        except Exception as e:
            self.log.error('FAIL : Failed to authorizes read only access to a user1, error: {}'.format(repr(e)))

# # @Clean up all the users/shares/groups before test starts
    def setup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users(use_rest=False)
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

# # @Clean up all the users/shares/groups after test finishes
    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

sharePageUserReadOnly()