""" Storage Page - Disk Status - Hard Drive status is displayed (MA-172)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=21189&execView=execDetails&view=details&pltab=steps&pId=50&nTP=117449&etab=8)
                1) Open webUI to login as system admin
                2) Click on the Storage category
                3) Select Disk status
                4) Click on an available drive
                5) Verify the hard drive info is shown
    
    Automation: Full
    
    Not supported product:
        Glacier (Does not have Storage page)
    
    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon
    
                   
    Test Status:
        Local Lightning: NA (FW: 2.00.190)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as Elem
from selenium.webdriver.common.keys import Keys
import time
from seleniumAPI.src.seleniumclient import ElementNotReady

products_info = {
        'LT4A': 'Lightning',      
        'BNEZ': 'Sprite',
        'BWZE': 'Yellowstone',
        'BWAZ': 'Yosemite',
        'BBAZ': 'Aurora',
        'KC2A': 'KingsCanyon',
        'BZVM': 'Zion',
        'GLCR': 'Glacier',
        'BWVZ': 'GrandTeton',
}


class storageDiskStatusHardDriveStatusDisplayed(TestClient):
        
    def run(self):
        self.log.info('######################## Storage Page - Disk Status - Hard Drive status is displayed TEST START ##############################')
        try:
            numOfBays = self.uut[Fields.number_of_drives]
            numOfDrives = self.get_drive_count()
            productName = self.uut[Fields.product]
            # productName = products_info[self.get_model_number()]
           
            # Glacier does not have Storage page 
            if productName == 'Glacier':
                self.log.info("UN-SUPPORTED PRODUCT: {}, (no storage page)".format(productName))
                return
            self.log.info("Product: {}, Supported Bays: {}, Current installed drives: {}".format(productName, numOfBays, numOfDrives))
            self.get_to_page('Storage')
            self.click_wait_and_check('nav_storage_link', 'storage_diskstatus_link', visible=True)
            self.click_wait_and_check('disk_mgmt', 'css=#disk_mgmt.LightningSubMenuOn', visible=True)
            time.sleep(2)
            # Get individual disk informaiton
            drivesInfoDict = self.verifyDiskStatus(numDrives=numOfDrives)
            self.verifyWDInfo(drivesDict=drivesInfoDict)
        except Exception as e:
            self.log.error("FAILED: Storage Page - Disk Status - Hard Drive status is displayed Test Failed!!, exception: {}".format(repr(e)))
        else:
            self.log.info("PASS: Storage Page - Disk Status - Hard Drive status is displayed Test PASS!!!")
            
        self.log.info('######################## Storage Page - Disk Status - Hard Drive status is displayed TEST END ##############################')    

    def verifyWDInfo(self, drivesDict):
        """
            @drivesDict: {1: [('Vendor', 'WD'),('Model', 'WDC WD20EFRX-68EUZN0'), ('Serial Number', 'WD-WMC4M2578991'), ('Capacity', '2 TB'), ('Firmware Version', '80.00A80')]}
        """
        if not drivesDict:
            return
        for i in drivesDict:
            self.log.info("*** Drive {} ***".format(i))
            k, v = drivesDict[i][0]
            if 'WD' in v:
                self.log.info('PASS: WD drive!')
            else:
                self.log.info('FAILED: Not WD drive, Vendor: {}'.format(v))
        
    def verifyDiskStatus(self, numDrives=None):
        """
            @numDrives: How many physical drives are installed
        """
        if numDrives is None:
            raise Exception("verifyDiskprofile() requires 1 argument")

        # Scroll the page to the bottom
        self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
        drivesInfoDict = {}
        try:
            for i in range(1, numDrives+1):
                diskNumLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[2]/div".format(i)              
                diskNum = self.get_text(diskNumLocator)
                diskNum = int(diskNum.split('Drive')[1])
                # Click Disk# to get more detail information
                self.click_wait_and_check(diskNumLocator, 'storage_diskstatusDriveClose1_button', visible=True)
                time.sleep(2)
                try:
                    drivesInfoDict.update(self.getDiskInfo(driveNum=diskNum))
                except Exception as e:
                    self.log.error("ERROR: Unable to get disk information, exception: {}".format(repr(e)))
                    raise
                self.click_element("storage_diskstatusDriveClose1_button")
                time.sleep(2)
        except ElementNotReady as e:
            self.log.error("ERROR: {}".format(repr(e)))
        else:
            return drivesInfoDict
            
    def getDiskInfo(self, driveNum):
        """
            @driveNum: disk number in the enclosure (integer type)
        """
        drivesInfo = {}
        try:
            self.log.info("=== Drive {} information ===".format(driveNum))
            for i in range(1, 6):
                self.wait_until_element_is_visible("//div[@class=\'dialog_content\']/table/tbody/tr[{}]/td[1]".format(i), timeout=5)
                titleText = self.get_text("//div[@class=\'dialog_content\']/table/tbody/tr[{}]/td[1]".format(i))
                valueText = self.get_text("//div[@class=\'dialog_content\']/table/tbody/tr[{}]/td[2]".format(i))
                if driveNum not in drivesInfo:
                    drivesInfo[driveNum] = []
                else:
                    drivesInfo[driveNum].append((titleText, valueText))
                self.log.info("{}: {}".format(titleText, valueText))
        except Exception as e:
            self.log.error("ERROR: {}".format(repr(e)))
        else:
            return drivesInfo
        
        
        
storageDiskStatusHardDriveStatusDisplayed()    