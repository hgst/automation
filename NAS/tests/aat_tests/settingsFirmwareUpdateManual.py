""" Settings Page - Firmware Update - Manual Update(MA-98)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?pId=50&nTP=117543&view=details&pltab=steps)
                1) Open webUI to login as system admin
                2) Go to Settings Page
                3) Select Firmware Update -> Manual Update
                4) Click update from file
                5) Navigate to the available file
                6) Verify that the NAS can be updated
    
    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.176)
        
    Change History:
    2015-08-20: Add product_info dictionary update to support new model without updating the products_info anymore
    2015-04-15: Remove &p=bin from the end of SEARCH_BASE_URL query string
                Replace the naming rule of firmware filename 
                (artifact-version.bin to artifact_version.bin) --> (My_Cloud_LT4A-2.00.190.bin to My_Cloud_LT4A_2.00.190.bin)
                Replace urllib with urllib2
                
""" 
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.configure import Device
from xml.etree.ElementTree import parse
import urllib2
import time
import os


REPO_LOCATION = 'http://repo.wdc.com'
ARTIFACT_BASE_URL = '{0}/service/local/repositories/projects/content/'.format(REPO_LOCATION)
SEARCH_BASE_URL = '{0}/service/local/lucene/search?repositoryID=projects'.format(REPO_LOCATION)

# Define the latest FW versions (Nexus)
# This dictionary will be updated with new model which was not added here in the beginning of the script
products_info = {
        'LT4A': ['Lightning', '2_10'],
        'BNEZ': ['Sprite', '2_10'],       # 2_10
        'BWZE': ['Yellowstone', '2_10'],  # 2_10
        'BWAZ': ['Yosemite', '2_10'],     # 2_10
        'BBAZ': ['Aurora', '2_10'],       # 2_10
        'KC2A': ['KingsCanyon', '2_10'],
        'BZVM': ['Zion', '2_10'],         # 2_10
        'GLCR': ['Glacier', '2_10'],       # 2_10
        'BWVZ': ['GrandTeton', '2_10']
}

class fwUpdateManual(TestClient):
    
    def run(self):
        self.log.info('######################## Settings Page - Firmware Update - Manual Update TEST START ##############################')
        try:
            # Get DUT's firmware version
            DUT_FW = self.get_firmware_version_number()
            DUT_VERSION, DUT_BUILD = DUT_FW.rsplit('.', 1)
            DUT_MODELNAME = self.get_model_number()
            tmpfiles = []

            # Add New Model Support
            if DUT_MODELNAME not in products_info.keys():
                product_name = self.uut[self.Fields.product].replace(' ', '')
                products_info[DUT_MODELNAME] = [product_name, DUT_VERSION.replace('.', '_')]
            else:
                # Update products_info FW version from 2_10 to current series
                products_info[DUT_MODELNAME][1] = DUT_VERSION.replace('.', '_')
            
            self.log.info("PRODUCT: " + products_info[DUT_MODELNAME][0] + ", MODEL: " + DUT_MODELNAME + ", FW: " + DUT_FW)
            
            # Get the latest firmware information from repo
            todo_fw_list = self.get_fw_todo_list(dutFW=DUT_FW, dutVersion=DUT_VERSION, dutModel=DUT_MODELNAME)
            UP_URL, UP_FILE, UP_BUILD = todo_fw_list[0]
            ORI_URL, ORI_FILE, ORI_BUILD = todo_fw_list[1]
            
            # self.log.info("{} / {} / {}".format(UP_URL, UP_FILE, UP_BUILD))
            # self.log.info("{} / {} / {}".format(ORI_URL, ORI_FILE, ORI_BUILD))
            
            # Download firmware file
            tmpfiles = self.download_fw_files(fwLink=todo_fw_list)
            
            # Initialize the CUR_FW
            CUR_FW = DUT_FW
            CUR_FW = self.firmware_flash(dutFW=DUT_FW, targetURL=UP_URL, targetFile=UP_FILE, targetBUILD=UP_BUILD)

        except Exception as e:
            self.log.error("FAILED: Settings Page - Firmware Update - Manual Update Test Failed!!, exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
        else:
            self.log.info("PASS: Settings Page - Firmware Update - Manual Update Test PASS!!")
        finally:
            # Restore firmware back to original  
            self.log.info('========== FINALLY: Firmware Clean Up ==========')
            CUR_FW = self.get_firmware_version_number()         
            if CUR_FW != DUT_FW:
                self.log.info("Restore the firmware from {} back to {}...".format(CUR_FW, DUT_FW))
                CUR_FW = self.firmware_flash(dutFW=CUR_FW, targetURL=ORI_URL, targetFile=ORI_FILE, targetBUILD=ORI_BUILD)

            if CUR_FW == DUT_FW:
                self.log.info("Succeed to restore the firmware back to {}".format(CUR_FW))
            else:
                self.log.warning("Failed to restore the firmware back to {} but stays at {}".format(DUT_FW, CUR_FW))
                self.update_inventory()
                
            # Remove temporary download files
            if tmpfiles:
                for f in tmpfiles:
                    if os.path.exists(f):
                        self.log.info("Cleaning temporary files: {}".format(f))
                        os.remove(f)            
            
        self.log.info('######################## Settings Page - Firmware Update - Manual Update TEST END ##############################')
        
    def firmware_flash(self, dutFW="", targetURL="", targetFile="", targetBUILD=""):
        """
            Flash DUT's firmware onto the target level
            
            @dutFW: Current DUT FW
            @targetURL: Target FW URL
            @targetFile: Target FW filename
            @targetBUILD: Target FW build
        """
        self.log.info("***** FW Flash: from {} to {} *****".format(dutFW, targetBUILD))
        cur_level = self.ui_firmware_flash(fwfile=targetFile)
        if cur_level == targetBUILD:
            self.log.info("***** PASS: FW is flashed to {} *****".format(cur_level))
            return cur_level
        else:
            self.log.info("DEBUG: Current level = {}, Target Level = {}".format(cur_level, targetBUILD))
            self.log.error("***** FAILED: FW failed to flash {} test *****".format(targetBUILD))
            self.log.warning("System Exit due to firmware flash failure...")
            raise SystemExit        

    def ui_firmware_flash(self, fwfile=""):
        """
            Browse to Firmware update page to flash the firmware
            
            @fwfile: Firmware Filename (Ex: My_Cloud_LT4A-2.00.026.bin)
        """
        self.log.info("Firmware flash by Selenium API")
        if not fwfile:
            self.log.error("ERROR: firmware_flash() requires 1 argument")
        try:
            fw_info = None
            self.access_webUI()
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.log.info("Click Firmware Update")
            self.click_wait_and_check('settings_firmware_link', 'css=#firmware.LightningSubMenuOn', visible=True)
            time.sleep(2)
            self.log.info("Manual firmware update")
            self.select_frame("upload_frame")
            self.log.info("Sending file: {}".format(os.path.abspath(fwfile)))
            #self.click_element("id_file")
            self.press_key("file", os.path.abspath(fwfile))
            self.log.info("Click Update from file")
            self.unselect_frame()
            time.sleep(2)
            self.wait_until_element_is_visible("popup_apply_button")
            self.click_wait_and_check('popup_apply_button', visible=False)
            time.sleep(5)  # wait for command applied to get the upload_firmware process
            # Wait for firmware flash and system back to ready
            # self.firmware_flash_wait()
            self.firmware_flash_wait2()
            fw_info = self.get_firmware_version_number()
            self.log.info("Firmware level after flash is: {}".format(fw_info))
            time.sleep(2)
            self.close_webUI()
            return fw_info
        except Exception as e:
            self.log.error('FAILED: Unable to complete firmware flash!! Exception: {}'.format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))

    def firmware_flash_wait2(self):
        """
            To check system status and wait for system back from reboot
        """
        fwStatusChkCmd = "xmldbc -i -g /runtime/firmware_percentage"
        count = 0
        while count < 60:  # 5 * 60 = 5 min
            try:
                fwStsResult = self.run_on_device(fwStatusChkCmd)
            except Exception:
                pass
            else:
                if fwStsResult:
                    self.log.info("[FW Percentage]: {}".format(fwStsResult))
                    percent = int(fwStsResult.strip())
                    if percent == 100:
                        self.log.info("=== FW flash completed! System is going to reboot NOW!!! ===")
                        break
                count += 1
                time.sleep(5)
        if count == 60:
            self.log.info(">>> Reach the maximum wait for percentage check <<<<")
        self.log.info("Waiting 5 minutes for system reboot...")
        time.sleep(300)  # 5min
        while True:
            try:
                if self.is_ready():
                    break
            except Exception as e:  # If RESTful API exception arises, continue to retry
                self.log.info("Retry: RESTful API is_ready() failure, exception: {}".format(repr(e)))
                continue
        return

    def firmware_flash_wait(self):
        """
            To check system status and wait for system back from reboot
        """
        fwUploadChkCmd = 'ps aux | grep -v grep | grep upload_firmware'
        fwStatusChkCmd = "xmldbc -i -g /runtime/firmware_percentage"
        try:
            fwChkResult = self.run_on_device(fwUploadChkCmd)
        except Exception:
            pass
        else:
            if fwChkResult:
                self.log.info("[Flashing]: {}".format(fwChkResult))
            else:
                self.log.info("[Did not find the upload_firmware process]")
        self.log.info("Waiting 5 minutes for firmware flash...")
        time.sleep(300)  # 5min
        count = 0
        while True:
            time.sleep(60)  # 1 min
            count += 1
            self.log.info("{} minute wait".format(count+5))
            if count >= 10:
                self.log.info("Reach the maximum 15 minutes wait for system ready")
                break
            try:
                fwChkResult = self.run_on_device(fwUploadChkCmd)
            except Exception as e:
                self.log.info("Retry: Unable to check upload status, exception: {}".format(repr(e)))
                continue
            else:
                if fwChkResult:
                    self.log.info("Firmware is still uploading...")
                    self.log.info("----------------------------")
                    self.log.info("{}".format(fwChkResult))
                    self.log.info("----------------------------")
                    continue
            time.sleep(2)
            try:
                fwChkResult = self.run_on_device(fwStatusChkCmd)
            except Exception as e:
                self.log.info("Retry: Unable to check flash status, exception: {}".format(repr(e)))
                continue
            else:
                if fwChkResult:
                    self.log.info("Firmware is still flashing...")            
                    self.log.info("----------------------------")
                    self.log.info("{}".format(fwChkResult))
                    self.log.info("----------------------------")
                    continue
            time.sleep(2)
            try:
                if self.is_ready():
                    break
            except Exception as e:  # If RESTful API exception arises, continue to retry
                self.log.info("Retry: RESTful API is_ready() failure, exception: {}".format(repr(e)))
                continue
        return

    def download_url_file(self, fw_url="", fw_file=""):
        """
            Open the url to download the file and save it locally
            
            @fw_url: URL where firmware file resides
            @fw_file: firmware filename
        """
        if not fw_url or not fw_file:
            self.log.error("ERROR: download_url_file() needs 2 arguments")
        try:
            self.log.info("Downloading the firmware file: {} ...".format(fw_file))
            # urllib.urlretrieve(fw_url, fw_file) # Replace by urllib2
            fw = urllib2.urlopen(fw_url)
            with open(fw_file, "wb") as localFile:
                localFile.write(fw.read())
            self.log.info("{} is saved at {}".format(fw_file, os.path.abspath(fw_file)))
        except urllib2.HTTPError as e:
            self.log.error("HTTP ERROR: {}, FILE NOT FOUND at {}!!".format(e.code, fw_url))
        except urllib2.URLError as e:
            self.log.error("URL ERROR: {}, FW Link: {}!!".format(e.reason, fw_url))
        except Exception as e:
            self.log.error("FAILED: Unable to download file {}, exception: {}\n link: {}".format(fw_file, repr(e), fw_url))

    def download_fw_files(self, fwLink):
        """
            Iterate the url list to download files and save them in local directory
            
            @fwLink: [(download url, firmware filename)]
        """
        tempfiles = []
        try:
            for l, f, b in fwLink:
                tempfiles.append(f)
                if os.path.exists(f):
                    #os.remove(f)
                    continue
                self.download_url_file(l, f)
            return tempfiles
        except Exception as e:
            self.log.error("FAILED: download_fw_files failed, exception: {}".format(repr(e)))

    def get_fw_todo_list(self, dutFW=None, dutVersion=None, dutModel=None):
        """
            Get the latest and previous build
            
            @dutFW: DUT firmware information (Ex: '2.00.026')
            @dutVersion: DUT Firmware version (Ex: '2.00')
            @dutModel: DUT Model (Ex: 'LT4A')
        """
        if not dutFW and not dutVersion and not dutModel:
            self.log.info("ERROR: get_fw_todo_list() requires 3 arguments")
            self.log.error("System EXIT due to insufficient arguments")
            raise SystemExit
        
        # Get the project version string      
        latest_project_version = products_info[dutModel][0] + '-' + products_info[dutModel][1]
        cur_project_version = products_info[dutModel][0] + '-' + dutVersion.replace('.', '_')
        
        # Reference link: http://repo.wdc.com/content/repositories/projects/
        """
        if products_info[dutModel][1] == '2_00':
            latest_project_version = products_info[dutModel][0] + '-iRadar' + '-' + products_info[dutModel][1]
            self.log.info("Adjust 2.00 project version query string...")
        """
        
        self.log.info("Latest Project Version: {}".format(latest_project_version))
        self.log.info("Current Project Version: {}".format(cur_project_version))        
              
        todo_fw_list = []
        try:
            latest_fw_list = self.build_fw_list(latest_project_version)

            if latest_fw_list == []:
                raise Exception("ERROR: Unable to get any firmware file information")
            
            # Build the latest download url and filename
            for g, a, v in latest_fw_list[:1]:
                link, filename = self.get_artifact_url(group=g, artifact=a, version=v)
                todo_fw_list.append((link, filename, v))
                self.log.info("The latest FW LINK: {}".format(link))
                
            if todo_fw_list == []: # Firmware file is not found on Nexus 
                self.log.info("Warning: Unable to locate the latest firmware {} on Nexus".format(dutFW))
                return 0                
                
            # Append the current firmware information
            download_list = []
            cur_fw_list = self.build_fw_list(cur_project_version)
            for g, a, v in cur_fw_list:
                if v == dutFW:
                    link, filename = self.get_artifact_url(group=g, artifact=a, version=v)
                    download_list.append((link, filename, v))
                    self.log.info("Current FW LINK: {}".format(link))
                    break
            
            if download_list == []: # Firmware file is not found on Nexus 
                self.log.info("Warning: Unable to locate current firmware {} on Nexus".format(dutFW))
                return 0
            
            # We have the latest firmware and original firmware information
            todo_fw_list.append(download_list[0])
            
        except Exception as e:
            self.log.error("FAILED: get_fw_todo_list() failed, exception: {}".format(repr(e)))
            self.log.warning("System Exit...")
            raise SystemExit
        
        return todo_fw_list

    def get_artifact_url(self, group="", artifact="", version=""):
        """
            @group: Sprite-1_06
            @artifact: My_Cloud_LT4A
            @version: 1.05.30
            
            return (firmware url, firmware filename)
        """
        global ARTIFACT_BASE_URL
        repo_query = '{0}/{1}/{2}/{1}_{2}.bin'.format(group, artifact, version)  # Change artifact-version.bin to artifact_version.bin
        FW_FILE = '{0}_{1}.bin'.format(artifact, version)
        repo_query = ARTIFACT_BASE_URL + repo_query
#         self.log.info("ARTIFACT URL: {}".format(repo_query))
#         self.log.info("FW FILE: {}".format(FW_FILE))
        return repo_query, FW_FILE

    def build_fw_list(self, groupversion=""):
        """
            @groupname: Sprite-1_06
            return [ (group, id, version) ]
        """
        global SEARCH_BASE_URL
        fw_list = []
        
        if not groupversion:
            self.log.error('groupname is not specified.')
        
        URL = SEARCH_BASE_URL + "&g={}".format(groupversion)
        
        # self.log.info("Building Firmware list...")
        try:
            u = urllib2.urlopen(URL)
            doc = parse(u)
            for artifact in doc.findall('.//data//artifact'):
                r_group = artifact.findtext('groupId')
                r_id = artifact.findtext('artifactId')
                r_version = artifact.findtext('version')
                fw_list.append((r_group, r_id, r_version))
    #             self.log.info('{} / {} / {}'.format(r_group, r_id, r_version))
            return fw_list
        except Exception as e:
            self.log.error("FAILED: build_fw_list() failed, exception: {}".format(repr(e)))

    def update_inventory(self):
        """
            Update information
        """
        device = Device(do_configure=False)
        if 'sctm_build' not in os.environ:
            self.log.info("=== Local run: no need to update inventory ===")
            return
        else:
            self.log.info("=== Silk run: going to update inventory ===")
        try:
            job_fw = "{0}.{1}".format(os.environ.get('sctm_version'), os.environ.get('sctm_build'))
            checkout_owner = "{0}-{1}".format(os.environ.get('sctm_execdef_name'), job_fw)
            platform = self.uut[self.Fields.product]
        except Exception as e:
            self.log.error("ERROR: fail to get checkout_owner/platform, exception: {0}".format(repr(e)))
            raise
        self.log.info("Checkout owner: {0}".format(checkout_owner))
        self.log.info("Target product: {0}".format(platform))
        uut = device.device_api.getDeviceByJob(jenkinsJob=checkout_owner)
        if uut:
            self.log.info("Found device, ip: {0}".format(uut['internalIPAddress']))
        else:
            self.log.error("Device: {} , not found, skip the information update".format(platform))
            return
        current_firmware = self.get_firmware_version_number()
        self.log.info("Going to update uut firmware information...")
        if not device.device_api.update(uut['id'], firmware=current_firmware):
            self.log.info("=== ERROR: fail to update device information ===")
        else:
            self.log.info("=== Succeed to update the uut fw: {0} ===".format(current_firmware))

fwUpdateManual()