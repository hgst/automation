from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.wd_wizards import RAIDWizard


class Staging(TestClient):
    def run(self):
        raid = RAIDWizard(self, 0)
        for test in range(1, 100):
            print 'Test #{}'.format(test)
            raid.open()
            raid.cancel()
            self.close_webUI()


Staging()
