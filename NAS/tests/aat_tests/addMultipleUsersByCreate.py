"""
title           :addMultipleUsersByCreate.py
description     :To verify if multiple users can be created by UI
author          :yang_ni
date            :2015/04/26
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time

class addMultipleUsersByCreate(TestClient):

    def run(self):
        try:
            if self.get_model_number() in ('GLCR',):
                self.log.info("=== Glacier de-feature does not support creating multiple users, skip the test ===")
                return

            self.log.info('Deleting all users ...')
            self.delete_all_users()
            time.sleep(3)

            self.log.info('Add multiple users : creating 5 users ...')
            self.create_multiple_users(start_prefix=1,numberOfusers=5)
            time.sleep(5)

            #Check if all the users are properly created
            self.check_users()

        except Exception as e:
            self.log.error("Test FAILED: exception: {}".format(repr(e)))

    def check_users(self):
        """
            To check if all the users are properly created
        """

        user_list = self.get_xml_tags(self.get_all_users()[1], 'user_id')
        self.log.info('user list: {0}'.format(user_list))

        self.log.info('Checking if all the users are created successfully...')
        for i in range(1,6):
            user = 'user' + str(i)
            if user in user_list:
                self.log.info('{0} is created successfully'.format(user))
            else:
                self.log.error('{0} is not created successfully'.format(user))

    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()


# This constructs the addMultipleUsersByCreate() class, which in turn constructs TestClient() which triggers the addMultipleUsersByCreate.run() function
addMultipleUsersByCreate()