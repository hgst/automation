'''
Create on Mar 4, 2015

@Author: lo_va

Objective: RAID Rebuild Test After Pulling Drive.
Wiki URL: http://wiki.wdc.com/wiki/RAID_Rebuild_Test_After_Pulling_Drive
'''
import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
from global_libraries.testdevice import Fields
sharename1 = 'test1hash1'
sharename2 = 'test2hash2'
block_size = 1024
file_size = 10240
timeout = 240

class raidRebuildTestAfterPullingDrive(TestClient):

    def run(self):

        # Test 1
        # Create RAID1 if UUT is not in RAID1
        raid_mode = self.get_raid_mode()
        raid_status = self.check_storage_page()
        if raid_mode == 'RAID1' and raid_status == 'Healthy':
                self.log.info('Raid type already in RAID1 and status is Healthy')
        else:
            self.log.info('Start to create RAID1.')
            self.configure_raid('RAID1', force_rebuild=True)
        self.delete_all_shares()

        # Enable Auto-Rebuild function
        self.log.info('Start to Enable Auto-Rebuild function.')
        self.enable_rebuild()

        # Create share and run hash with files.
        self.log.info('Create %s share and run hash with files.' % sharename1)
        self.create_shares(share_name=sharename1, force_webui=True)
        self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        self.close_webUI()
        file = self.create_random_file('/shares/%s/' % sharename1, filename='file0.jpg', blocksize=block_size, count=file_size)
        hash1 = self.md5_checksum('/shares/%s/' % sharename1, 'file0.jpg')
        print 'Hash1 = %s' % hash1
        self.log.info('Plug out disk 1')
        self.remove_drives(1)
        self.run_on_device('mkdir /shares/%s/testshare1' % sharename1)
        self.run_on_device('touch /shares/%s/testfile1.txt' % sharename1)
        time.sleep(1)
        self.log.info('Plug in disk 1')
        self.insert_drives(1)
        self.wait_rebuild_start()
        self.wait_rebuild_finished()
        self.log.info('Start to check raid status.')
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        if raid_status == 'Healthy':
            hash2 = self.md5_checksum('/shares/%s/' % sharename1, 'file0.jpg')
            print 'Hash2 = %s' % hash2
            if hash1 == hash2 and not None:
                self.log.info('Compare checksum successfully!')
            else:
                self.log.error('Checksum is not the same. File corrupted!!')
        else:
            self.log.error('Raid is not healthy! Test Failed!!')
        self.close_webUI()

        # Test 2
        self.delete_all_shares()
        self.create_shares(share_name=sharename2, force_webui=True)
        self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        self.close_webUI()
        file2 = self.create_random_file('/shares/%s/' % sharename2, filename='file1.jpg', blocksize=block_size, count=file_size)
        hash3 = self.md5_checksum('/shares/%s/' % sharename2, 'file1.jpg')
        print 'Hash3 = %s' % hash3
        self.log.info('Plug out disk 1')
        self.remove_drives(1)
        self.run_on_device('mkdir /shares/%s/testshare2' % sharename2)
        self.run_on_device('touch /shares/%s/testfile2.txt' % sharename2)
        time.sleep(5)
        self.reboot(500)
        time.sleep(5)
        self.wait_rebuild_start()
        self.wait_rebuild_finished()
        self.log.info('Start to check raid status.')
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        if raid_status == 'Healthy':
            hash4 = self.md5_checksum('/shares/%s/' % sharename2, 'file1.jpg')
            print 'Hash4 = %s' % hash4
            if hash3 == hash4 and not None:
                self.log.info('Compare checksum successfully!')
            else:
                self.log.error('Checksum is not the same. File corrupted!!')
        else:
            self.log.error('Raid is not healthy! Test Failed!!')
        self.close_webUI()

    def tc_cleanup(self):
        self.checked_disk_exist()
        raid_status1 = self.raid_status(1)
        if 'degraded' in raid_status1:
            self.configure_raid(raidtype='RAID0', volume_size=100)

    def checked_disk_exist(self):
        drive_mapping = self.get_drive_mappings()
        for x in range(1, self.uut[self.Fields.number_of_drives]+1):
            if x not in drive_mapping:
                self.log.info('Drive {} is disconnected, try to insert it!'.format(x))
                self.insert_drives(x)
        if self.get_drive_count() == self.uut[self.Fields.number_of_drives]:
            self.log.info('Clean_up for insert drives succeed!')
        else:
            self.log.warning('Clean_up for insert drives failed!')

    def enable_rebuild(self):
        self.get_to_page('Storage')
        check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
        time.sleep(2)
        check_status_attr = check_status.get_attribute('style')
        if 'block' in check_status_attr:
            self.log.info('Enable Auto-Rebuild')
            self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
            check_status_attr = check_status.get_attribute('style')
            while 'block' in check_status_attr:
                self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                check_status_attr = check_status.get_attribute('style')
                time.sleep(2)
            time.sleep(2)
        if 'none' in check_status_attr:
            self.log.info('Auto-Rebuild already enabled')
        self.close_webUI()

    def raid_status(self, raidnum):
        check_raid_status = 'mdadm --detail /dev/md%s | grep "State :"' % raidnum
        raid_status = self.run_on_device(check_raid_status)
        time.sleep(1) # Time to wait status return value
        while not raid_status:
            raid_status = self.run_on_device(check_raid_status)
            time.sleep(1)
        self.log.info('Raid_status = %s' % raid_status)
        raid_status1 = str(raid_status.split(': ')[1])

        return raid_status1

    def wait_rebuild_start(self):
        # Wait for recovering started
        self.log.info('Waiting up to 30 seconds for the RAID status become rebuild')
        timer = Stopwatch(timer=30)
        timer.start()

        while True:
            raid_status1 = self.raid_status(1)
            if 'recovering' in raid_status1:
                self.log.info('RAID status took {} seconds became rebuild'.format(timer.get_elapsed_time()))
                break
            else:
                if timer.is_timer_reached():
                    self.log.warning('RAID status never became rebuilding')
                    break
                else:
                    time.sleep(1)

    def check_storage_page(self):
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        while not raid_status:
            raid_status = self.get_text('raid_healthy')
            time.sleep(1)
        return raid_status

    def wait_rebuild_finished(self, web_check=True):
        # Wait for recovering finished
        self.log.info('Waiting up to 120 minutes for the RAID rebuilding finished')
        timer = Stopwatch(timer=7200)
        timer.start()

        while True:
            raid_status1 = self.raid_status(1)
            if 'recovering' not in raid_status1 and 'degraded' not in raid_status1:
                self.log.info('RAID took {} minutes for rebuilding'.format(timer.get_elapsed_time()/60))
                break
            if 'recovering' in raid_status1:
                if timer.is_timer_reached():
                    self.log.warning('Rebuilding out of time')
                    break
                else:
                    time.sleep(30)
            else:
                self.log.warning('Raid not in recovering mode')
                break

        if web_check:
            timer = Stopwatch(timer=60)
            timer.start()
            while True:
                raid_status = self.check_storage_page()
                self.log.info('Raid Drives Status = {}'.format(raid_status))
                if raid_status in 'Healthy':
                    self.log.info('Rebuild finished.')
                    break
                else:
                    if timer.is_timer_reached():
                        self.log.error('Page Rebuilding get STUCK!')
                        break
                    else:
                        time.sleep(2)

raidRebuildTestAfterPullingDrive()