#
#Objective: Configure group quota limits and validate that when exceeding
#           those limits the appropiate errors and alerts are triggered
# wiki URL: http://wiki.wdc.com/wiki/Quotas    
#

from testCaseAPI.src.testclient import TestClient
from smb.smb_structs import OperationFailure
import sys
user1 = 'user1'
user2 = 'user2'
group = 'group1'
share1 = 'user1'
share2 = 'user2'

class groupQuota(TestClient):
    def run(self):
        self.log.info('Group Quota Test started')
        self.setup()
        self.group_quota(file_size=92, size_of_group_quota=150, size_of_user_quota=95)
        self.setup()
        
    def setup(self):
        # a quick restore will work faster here.
        # Delete users: user1, user2 if they exist
        
        self.log.info('Deleting users ...')
        self.delete_user(user1)
        self.delete_user(user2)
        
        # Delete samba shares: user1, user2 if they exist
        self.log.info('Deleting shares ...')
        self.execute('smbif -b user1')
        self.execute('smbif -b user2') 
        
        self.log.info('Deleting groups ...')
        self.delete_group(group)
        
    def enough_space_validation(self, response, expected_failure = True):
        testname = sys._getframe().f_back.f_code.co_name 
        
        # Validate enough space
        if expected_failure == True:
            if 'file0.bmp' in response[0]:
                if response[2] < 92:
                    self.log.info('{0} Test Passed'.format(testname))
                else:
                    self.log.error('{0} Test Failed'.format(testname))         
        else:
            if 'file0.bmp' in response[0]:
                if response[2] >= 92:
                    self.log.info('{0} Test Passed'.format(testname))
                else:
                    self.log.error('{0} Test Failed'.format(testname))
            
    def group_quota(self,file_size, size_of_group_quota,size_of_user_quota):
        
        # The file size to transfer, this will convert KB to MB   
        file_size_MB = 1024 * (file_size)
        
        # Add two users with quotas and set the quota at 95MB
        self.create_multiple_users(numberOfusers=2)
                
        # Make sure user1 share was created otherwise create it.
        #if self.get_share(share1)[0] == 0 and self.get_share(share2)[0] == 0:
        #    pass
        #else:
        #    self.create_shares(share_name='user', number_of_shares=2, force_webui=True)
           
        self.set_quota_all_volumes(user1, 'user', size_of_user_quota)
        self.set_quota_all_volumes(user2, 'user', size_of_user_quota)
        
        # Create a group and set the quota at 150MB
        self.create_groups(group_name=group)
        self.set_quota_all_volumes(group, 'group', size_of_group_quota)
        
        # Assign user1 and user2 to group1
        self.update_group(group=group, memberusers='#user1#,#user2#')
        
        # Generate a jpg test file
        jpgFiles = self.generate_files_on_workspace(file_size_MB, 1)
        
        # Write the file to the unit as user1
        response = self.write_files_to_smb(username=user1, password='welc0me',files=jpgFiles, share_name=share1, delete_after_copy=True)
        
        # Generate a bmp test file
        bmpFiles = self.generate_files_on_workspace(file_size_MB, 1, 'bmp')
        
        # Write the file to the unit as user2
        try:
            self.write_files_to_smb(username=user2, password='welc0me', files=bmpFiles, share_name=share2, delete_after_copy=True)
        except OperationFailure:
            self.log.info('Quota met, unable to write files. Passed.')
        else:
            self.log.error('Able to write files despite quota being met.')
     
        response = self.read_files_from_smb(files=bmpFiles, share_name=share2)
        
        # Validate enough space
        self.enough_space_validation(response, expected_failure=True)
        
        # Remove user1 from group1
        self.update_group(group=group, memberusers='#user2#')
        
        # Generate a bmp test file
        bmpFiles = self.generate_files_on_workspace(file_size_MB, 1, 'bmp')
        
        # Write the file to the unit as user2
        try:
            self.write_files_to_smb(username=user2, password='welc0me', files=bmpFiles, share_name=share2, timeout=90, delete_after_copy=True)
        except OperationFailure:
            self.log.info('Quota met, unable to write files. Passed.')
        else:
            self.log.error('Able to write files despite quota being met.')
            
        response = self.read_files_from_smb(files=bmpFiles, share_name=share2)
        
        # Validate enough space
        self.enough_space_validation(response, expected_failure=True)
        
groupQuota()