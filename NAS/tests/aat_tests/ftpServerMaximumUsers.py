from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from ftplib import FTP
from ftplib import error_perm
from global_libraries import wd_exceptions
import time
import os
import requests

Share_folders=['Public', 'SmartWare', 'TimeMachineBackup']
numberOfusers = 3
max_user = 2
default_max_user = 10
password = 'welc0me'
time_out = 60

class FTPServerMaximumUsers(TestClient):
    def run(self):
        ftp_url = self.uut[Fields.internal_ip_address]
        self.log.info('**** Clean up user list first ****')
        self.delete_all_users(use_rest=False)
        self.log.info('**** Clean done !! *****')
        self.FtpServerMaximumUsers(ftp_url, numberOfusers, max_user, password)
        
    def tc_cleanup(self):
        self.UpdateFtpDefaultMaximumUser(user_no=default_max_user)
        self.Check_FtpMaximunUser(user_no=default_max_user)
        
    def FtpServerMaximumUsers(self, FTP_URL, numberOfusers, max_user, password):
        self.log.info('################## FTP Server Maximum User TESTS START ################')
        self.log.info('############ FTP Server Maximum User test procedure ###########')
        self.create_users(numberOfusers=3, password=password)
        self.check_users(numberOfusers=3)
        self.ftp_config(set='ON')
        self.SharesFtpEnable()
        self.UpdateFtpDefaultMaximumUser(user_no=max_user)
        self.Check_FtpMaximunUser(user_no=max_user)
        self.Ftp_login(FTP_URL=FTP_URL, numberOfusers=numberOfusers, max_user=max_user, password=password)
        self.log.info('################## FTP Server Maximum User TESTS END ###############')
                
    def create_users(self, numberOfusers, password):
        self.log.info('######### Create User #########')
        for i in range(1, (numberOfusers+1)):
            "add user account command: account -a -u username -p passsword"
            username = 'user' + str(i)
            command = 'account -a -u ' + str(username) + ' -p ' + str(password)
            self.execute(command) 
            self.log.info('create {0} by command: {1}'.format(username, command))

    def check_users(self, numberOfusers):
        # list all user command : account -i user
        self.log.info('******** Check If User Exist ********')
        
        all_user_list = self.execute('account -i user')[1].split()
        self.log.debug('result = {0}'.format(all_user_list))
        
        if not all_user_list:
            raise wd_exceptions.InvalidDeviceState('User list is empty')
        for i in range(1, (numberOfusers+1)):
            username = 'user' + str(i)
            if username in all_user_list:
                self.log.info('{0}  create successfully'.format(username))
            else:
                self.log.error('{0} could not be found'.format(username))
               
    def ftp_config(self, set):
        self.log.info('############ FTP Configuration ###########') 
        mgr_cmd = 'app_mgr.cgi'
        status_dict = {'ON': 1, 'OFF': 0}
        ftp_server_status_cmd = 'FTP_Server_Get_Config'
        result = self.send_cgi(ftp_server_status_cmd, mgr_cmd)
        status = self.get_xml_tags(result, tag='state')
        if str(status_dict[set]) not in status:
            cmd = 'FTP_Server_Enable&f_state=%s' % status_dict[set]
            self.send_cgi(cmd, mgr_cmd)
            result = self.send_cgi(ftp_server_status_cmd, mgr_cmd)
            status = self.get_xml_tags(result, tag='state')
            if str(status_dict[set]) in status:
                self.log.info('PASS :FTP set to {}'.format(set))
            else:
                self.log.error('FAIL :FTP fail to Set to {}'.format(set))
        else:
            self.log.info('PASS: FTP already set to %s' % set)
        
    def SharesFtpEnable(self):
        self.log.info('############ Share FTP Enable ###########')
        mgr_cmd = 'share.cgi'
        for list in Share_folders:
            cmd = 'cgi_modify_share&mtype=4&path=%2Fmnt%2FHD%2FHD_a2%2F{0}&sharename={1}&comment=&recycle=0&ftp=1&'.format(list, list)\
                  +'ftp_anonymous=n&webdav=0&nfs=0&host=&root_squash=&write=&media=0&publicFlag=&old_sharename={0}&oplocks=&remoteAccess='.format(list)
            self.log.info('Setting Shares {0} folder -FTP Access'.format(list))
            self.send_cgi(cmd, mgr_cmd)
            
    def Update_user_access(self, user_no):
        self.log.info('############ Update User Access ###########')
        self.get_to_page('Users')
        for i in range(1, (user_no+1)):
            try:
                self.click_element('users_user_user%s'%i)
                self.click_element('users_rw1_link')
                time.sleep(2)
                self.click_element('users_rw2_link')
                time.sleep(2)
                self.log.info('PASS: User{0} access update to R/W'.format(i))
            except Exception as e:
                self.log.error("fail to change User{0} access: {1}".format(i, repr(e)))
        self.close_webUI()
    
    def Check_FtpMaximunUser(self, user_no):
        self.log.info('*** Check FTP Maximum User Number ***')
        mgr_cmd = 'app_mgr.cgi'
        ftp_server_config_cmd = 'FTP_Server_Get_Config'
        result = self.send_cgi(ftp_server_config_cmd, mgr_cmd)
        maxclientsnumber = self.get_xml_tags(result, 'maxclientsnumber')
        self.log.info('max clients number: {0}'.format(maxclientsnumber))
        if str(user_no) in maxclientsnumber:
            self.log.info('Succefully to set maximun User to %s' % user_no)
        else:
            self.log.error('Failed to set maximun User to %s' % user_no)       
    
    def UpdateFtpDefaultMaximumUser(self, user_no):
        self.log.info('############ Update FTP Default Maximum User ###########')
        mgr_cmd = 'app_mgr.cgi'
        cmd = 'FTP_Server_Set_Config&f_client_char=UTF-8&f_connect_per_ip=5&f_external_ip=0.0.0.0' +\
            '&f_flow_value=0&f_forcepasvmod=0&f_fxp=0&f_idle_time=10&f_maxuser='+str(user_no) +\
            '&f_passive_port=55536%3A55663&f_port=21&f_tls_status=1'
        self.send_cgi(cmd, mgr_cmd)
   
    def send_cgi(self, cmd, mgr):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[Fields.internal_ip_address], mgr)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')
    
    def Ftp_login(self, FTP_URL, numberOfusers, max_user, password):
        
        self.log.info("#### Testing FTP Maximum User : Connecting to FTP Server: {0} #####".format(FTP_URL))
        ftp_list = []

        for i in range(0, numberOfusers):
            retry = 3
            close_previous_login = 0
            ftpconn = None
            while retry > 0:
                try:
                    ftpconn, ex = self.ftpConnection(ftp_ip=FTP_URL)
                    if ex:
                        raise ex
                    if ftpconn != None:
                        ftp_list.append(ftpconn)
                    self.log.info('ftp_list = %s' % ftp_list)
                    if len(ftp_list) > max_user and (close_previous_login == 0):
                        self.log.error("FAIL: User3 shold not have access to login") 
                        break 
                                           
                except Exception as e:                   
                    if (i+1) > max_user:
                    # Expect to fail on out of maximun user number to log in
                        if '421 2 users (the maximum) are already logged' in e.message:
                            self.log.info("PASS: {0}".format(repr(e)))
                            self.log.info('ftp_list = %s' % ftp_list)
                            ftp_list[i-1].quit()
                            ftp_list[i-1].close()
                            time.sleep(2)
                            close_previous_login = 1
                        else:
                            self.log.error("FAIL: {0}".format(repr(e))) 
                    else:
                        self.log.info('User{0} retry {1} times for re-establish FTP connection to {2}, exception: {3}'.format((i+1), (3-retry), FTP_URL, repr(e)))
                        retry -= 1
                else:
                    break
            try:
                ftp_list[i], ex = self.ftplogin(ftpconn=ftp_list[i], user_name='user'+str(i+1), user_pwd=password, anonymous=False)
                if ex:
                    raise ex
                tmp_file = self.generate_test_file(10240)
                self.write_file_to_ftp(ftp_list[i], tmp_file)
                self.read_file_from_ftp(ftp_list[i], tmp_file)
            except Exception as e:
                self.log.error('FAIL: {}'.format(repr(e)))

            finally:
                self.log.info('****  user%s login and transfer file ****' % (i+1))
                
    def ftpConnection(self, ftp_ip=""):
        """
            Create ftp connection and login by user account or anonymous
            Once login, it will return the ftp socket back and exception
            
            @ftp_ip: self.uut[Fileds.internal_ip_address]
        """
        ex = None
        if ftp_ip == "":
            ftp_ip = self.uut[Fields.internal_ip_address]

        try:
            ftpconn = FTP(ftp_ip)
        except Exception as ex:
            self.log.info("Warning: Unable to connect to FTP: {}, exception: {}".format(ftp_ip, repr(ex)))
            ftpconn = None
            return ftpconn, ex
        
        return ftpconn, ex

    def ftplogin(self, ftpconn, user_name, user_pwd, anonymous):
        """
            @anonymous: True (Anonymous login)
                        User login: new_user_name / new_user_pwd (Set to anonymous=False)
                        If you do not set anonymous to False, it always login as anonymous user
            
            Need to set anonymous=False to switch to user login mode
            @user_name: user account
            @user_pwd: user password               
                        
            Usage:
            ftpconn, ex = self.ftpConnection('192.168.1.91', new_user_name='user1', new_user_pwd='password1', anonymous=False)
            ftpconn, ex = self.ftpConnection('192.168.1.91', anonymous=True)
        """
        ex = None
        
        try:
            if anonymous:
                resp = ftpconn.login()
            else:
                resp = ftpconn.login(user_name, user_pwd)
        except error_perm as ex:
            self.log.info("Warning: Unable to login ftp, exception: {0}".format(repr(ex)))
            return ftpconn, ex
        except Exception as ex:
            self.log.info("FAILED: ftp login failed, exception: {0}".format(repr(ex)))
            return ftpconn, ex
                                
        if '230' in resp:
            if anonymous:
                self.log.info("Anonymous login succeed")
            else:
                self.log.info("{0} login succeed".format(user_name))
        else:
            if anonymous:
                self.log.error("Anonymous login failed")
            else:
                self.log.error("{0} login failed".format(user_name))
        
        return ftpconn, ex
        
    def write_file_to_ftp(self, ftp, tmp_file, filesize=10240):

        self.log.info("***** Initiate data upload test *****")
        ftp_path = ftp.pwd()
        self.log.info("current ftp path: {0}".format(ftp_path))
        
        if Share_folders[0] in ftp_path:
            self.log.info("Already switch to {0} folder".format(Share_folders[0]))
        else:
            resp = ftp.cwd(Share_folders[0]) # switch to 'Public' folder
            if '250' in resp:
                self.log.info("Switch to {0} folder".format(Share_folders[0]))
            else:
                self.log.error("Unable to switch to {0} folder".format(Share_folders[0]))

        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        src_file = ''.join(tmp_file)
        tmp_dir = os.path.dirname(src_file)
        tmpfile = os.path.join(tmp_dir, 'newfile.jpg')
        self.log.info("Rename temporary file file0.jpg to newfile.jpg")
        os.rename(src_file, tmpfile)

        self.log.info("Generate temporary file: {}".format(tmpfile))

        try:
            ftp.voidcmd("NOOP")  # keep connection alive
        except Exception as e:
            self.log.info("WARN: Connection might be closed, exception: {}".format(repr(e)))

        try:
            myfile = open(tmpfile, "rb")
            to_Sock = ftp.storbinary("STOR " + os.path.basename(tmpfile), myfile)
            myfile.close()
            if '226' in to_Sock: # File successfully transferred
                self.log.info("PASS:{0} is uploaded successfully".format(os.path.basename(tmpfile)))
            else:
                self.log.error("Upload {0} failed".format(os.path.basename(tmpfile)))

        except error_perm as e:
            raise e
        except Exception as e:
            raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. exception: {}".format(os.path.basename(tmpfile), repr(e)))

    def read_file_from_ftp(self, ftp, tmp_file, delete_after_download=True):
        self.log.debug('tmp_file: {0}'.format(tmp_file))
        self.log.info("***** Initiate data download test *****")
        
        ftp_path = ftp.pwd()
        self.log.info("current ftp path: {0}".format(ftp_path))
        
        if Share_folders[0] in ftp_path:
            self.log.info("Already switch to {0} folder".format(Share_folders[0]))
        else:
            resp = ftp.cwd(Share_folders[0]) # switch to 'Public' folder
            if '250' in resp:
                self.log.info("Switch to {0} folder".format(Share_folders[0]))
            else:
                self.log.error("Unable to switch to {0} folder".format(Share_folders[0]))
        
        # download newfile.jpg as download_newfile.jpg
        tmpfile = 'newfile.jpg'  # temporary file on FTP server
        download_file = "download_newfile.jpg"  # filename which is saved locally

        # if download_file exists, removes it
        if os.path.isfile(download_file):
            os.remove(download_file)   

        # Check if tmpfile exists or not
        listfiles = ftp.nlst()
        if tmpfile in listfiles:
            self.log.info("{0} exists, read test is ready to go".format(tmpfile))
        else:
            raise Exception("FAILED: {0} does not exist for read test, STOP!".format(tmpfile))
    
        self.log.info("*** Going to download file for read access test ***")

        try:            
            path = os.getcwd()
            self.log.info("local path: {0}".format(path))
            local_path = path +'/'+ download_file
            myfile = open(local_path, "wb")
            from_Sock = ftp.retrbinary("RETR "+tmpfile, myfile.write)
            myfile.close()
            if '226' in from_Sock: # File successfully transferred
                self.log.info("PASS:{0} is download successfully".format(os.path.basename(tmpfile)))
            else:
                self.log.error("Upload {0} failed".format(os.path.basename(tmpfile)))

        except Exception as e:
            raise Exception("FAILED: ftpReadTest failed, can not download {0}, exception: {1}".format(tmpfile, repr(e)))

        if delete_after_download:
            os.remove(download_file)
            self.log.info("Cleaning temporary files: {}".format(os.path.abspath(download_file)))
    
    def click_close_element(self, close_element, visible_element, attempts=3, attempt_delay=2):
        for attempts_remaining in range(attempts, -1, -1):
            if attempts_remaining > 0:
                try:
                    self.log.debug('Clicking element: "{0}" to close: "{1}"'.format(close_element, visible_element))
                    self.click_element(close_element)
                    self.wait_until_element_is_visible(visible_element, timeout=60)
                    self.log.debug('Element: "{}" is closed.'.format(visible_element))
                    break
                except:
                    self.log.info('Element: "{0}" did not click. Wait {1} seconds and retry. {2} attempts remaining.' \
                                  .format(close_element, attempt_delay, attempts_remaining))
                    time.sleep(attempt_delay)
            else:
                raise Exception('Failed to popup element: "{0}"'.format(visible_element))

FTPServerMaximumUsers()