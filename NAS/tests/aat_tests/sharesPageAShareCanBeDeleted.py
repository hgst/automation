"""
title           :sharesPageAShareCanBeDeleted.py
description     :To verify if a share can be deleted
author          :yang_ni
date            :2015/05/26
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time

class AShareCanBeDeleted(TestClient):

    def run(self):

        try:
            # Delete all the shares
            self.log.info('Deleting all shares before test case starts ...')
            self.delete_all_shares()

            # Create a share named 'share'
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=1)

            # Delete the share by UI
            self.delete_share_ui()

            # Check if the share still exists
            self.click_wait_and_check('nav_shares_link','shares_createShare_button')
            self.element_should_not_be_visible('shares_share_share')

            share_list = self.get_xml_tags(self.get_all_shares()[1], 'share_name')
            self.log.info('share list: {0}'.format(share_list))
            if 'share' in share_list:
                self.log.error('ERROR: share is not deleted successfully')
            else:
                self.log.info('share is deleted successfully')

        except Exception as e:
            self.log.error('ERROR: share is not deleted successfully: {}'.format(repr(e)))

    def delete_share_ui(self):
        self.get_to_page('Shares')
        time.sleep(8)
        self.element_should_be_visible('shares_share_share')
        self.click_element('shares_share_share')
        time.sleep(4)
        self.click_wait_and_check('shares_removeShare_button', 'popup_apply_button')
        time.sleep(5)
        self.click_wait_and_check('popup_apply_button', visible=False, timeout=30)
        time.sleep(15)

AShareCanBeDeleted()