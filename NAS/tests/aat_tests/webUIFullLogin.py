'''
Created on Mar 6th, 2015

@author: tsui_b

Objective: Verify tab order on each page and that all links, buttons, drop-down boxes, 
           keyboard shortcuts etc. in the web UI lead to legitimate pages or display legitimate values.
Wiki URL: http://wiki.wdc.com/wiki/Web_UI
'''

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

timeout = 10  # Use for wait_until_element_is_(not_)vislble

class webUIFullLogin(TestClient):

    def run(self):
        self.set_selenium_timeout(timeout)
        self.log.info('Verifying page: "Login"')
        self.login_page()
        self.log.info('Verified page: "Login"')

    def check_element(self, element, cancel_button=None):
        # Wrapper for validate_element to allow catching exceptions and logging them instead of ending the test
        self.start_test(element.name)
        try:
            self.validate_element(element, cancel_button)
            self.pass_test(element.name)
        except:
            self.fail_test(element.name, 'Failed validation')

    def login_page(self):
        self.access_webUI(do_login=False)
        # Login Diag
        self.wait_until_element_is_visible(E.LOGIN_DIAG, timeout)
        # WD Logo
        self.element_should_be_visible(E.LOGIN_WD_LOGO)
        # Check Password first because of ITR #104366
        # Password input default value
        self.check_element(E.LOGIN_PASSWORD)
        # Username input default value
        self.check_element(E.LOGIN_USERNAME, cancel_button=E.LOGIN_USERNAME_CANCEL)
        # Remember Me Checkbox          
        self.check_element(E.LOGIN_REMEMBER_ME)
        # Login Button
        self.check_element(E.LOGIN_BUTTON)
        self.close_webUI()

webUIFullLogin()