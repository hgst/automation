'''
Create on June 15, 2015

@Author: lo_va

Objective:  Verify that the NAS can serve files using all available network file sharing protocols
            and that file/folder permissions are consistent across all protocols. 

wiki URL:   http://wiki.wdc.com/wiki/File_Sharing_Protocols
'''

import time
import random
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
number = random.randint(1,100)
accessfolder = 'accessfolder{}'.format(number)
publicfolder = 'Public'
access_user = 'accessuser{}'.format(number)
scp_target_path = '/shares/Public/Shared\ Pictures/'
tmpfilesize = 102400
file_prefix = 'rwfile{}'.format(number)


class fileSharingProtocols(TestClient):
    
    def run(self):
        
        # Cleanup
        self.delete_all_shares()
        model = self.get_model_number()
        time.sleep(2)
        self.log.info('****** Start to test File Sharing Protocols test case ******')
        # Create share
        self.create_shares(share_name=accessfolder)
        time.sleep(1)
        # Create user to access share via all protocol
        self.create_user(username=access_user, password=access_user)
        self.set_quota_all_volumes(access_user, 'user', 0)
        
        # Enabled ftp access
        self.log.info('Enable ftp access')
        self.enable_ftp_share_access(share_name=accessfolder, access='Anonymous Read / Write')
        self.enable_ftp_share_access(share_name=publicfolder, access='Anonymous Read / Write')
        '''
        # Enable nfs access
        self.log.info('Enable nfs access')
        if model == 'LT4A' or model == 'GLCR':
            self.log.info('Model of {} no need to enable nfs service'.format(model))
        else:
            self.enable_nfs_share_access(share_name=accessfolder)
            self.enable_nfs_share_access(share_name=publicfolder)
        '''
        # Generated file
        self.log.info('Generated {}0.jpg for the R/W test'.format(file_prefix))
        tmp_file = self.generate_test_file(tmpfilesize, prefix=file_prefix)
        time.sleep(10)

        # Access share via SMB
        try:
            self.log.info('Start to access share via SMB protocol')
            self.write_files_to_smb(username=access_user, password=access_user, files=tmp_file, share_name=accessfolder, delete_after_copy=False)
            self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+accessfolder+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=accessfolder, delete_after_copy=True)
            self.write_files_to_smb(username=access_user, password=access_user, files=tmp_file, share_name=publicfolder, delete_after_copy=False)
            self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+publicfolder+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=publicfolder, delete_after_copy=True)
        except Exception as e:
            self.log.exception('Test R/W for SMB protocol failed. Exception: {}'.format(repr(e)))
            
        # Enabled Anonymous FTP Read/Write Accesss and used accessuser to access the folder.
        try:
            self.log.info('Start to access share via FTP protocol with anonymous enabled')
            self.write_files_to_ftp(username=access_user, password=access_user, files=tmp_file, share_name=accessfolder, delete_after_copy=False)
            self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+accessfolder+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_ftp(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=accessfolder, delete_after_copy=True)
            self.write_files_to_ftp(username=access_user, password=access_user, files=tmp_file, share_name=publicfolder, delete_after_copy=False)
            self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+publicfolder+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_ftp(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=publicfolder, delete_after_copy=True)
        except Exception as e:
            self.log.exception('Test R/W for FTP protocol with anonymous enabled failed. Exception: {}'.format(repr(e)))
        
        # Disabled Anonymous FTP Read/Write Accesss and used accessuser to access the folder.
        try:
            self.log.info('Start to access share via FTP protocol with anonymous disabled')
            self.enable_ftp_share_access(share_name=accessfolder, access='Anonymous None')
            self.write_files_to_ftp(username=access_user, password=access_user, files=tmp_file, share_name=accessfolder, delete_after_copy=False)
            self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+accessfolder+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_ftp(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=accessfolder, delete_after_copy=True)
        except Exception as e:
            self.log.exception('Test R/W for FTP protocol with anonymous disabled failed. Exception: {}'.format(repr(e)))

        # Access share via NFS
        if model != 'LT4A':
            self.log.info('Start to access share via NFS protocol')
            nfs_mount1 = self.mount_share(share_name=accessfolder, protocol=self.Share.nfs, user=access_user, password=access_user)
            if nfs_mount1:
                created_files = self.create_file(filesize=512000, dest_share_name=accessfolder, file_count=5)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=accessfolder):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
                    self.log.info('Start to access share via NFS protocol')
            nfs_mount2 = self.mount_share(share_name=publicfolder, protocol=self.Share.nfs, user=access_user, password=access_user)
            if nfs_mount2:
                created_files = self.create_file(filesize=512000, dest_share_name=publicfolder, file_count=5)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=publicfolder):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
        '''
        # Access share via AFP
        afp_mount = self.mount_share(share_name=accessfolder, protocol=self.Share.afp, user=access_user, password=access_user)
        if afp_mount:
            created_files = self.create_file(filesize=51200, dest_share_name=accessfolder, file_count=5)
            for next_file in created_files[1]:
                if not self.is_file_good(next_file):
                    self.log.error('FAILED: afp Read/Write test')
                else:
                    self.log.info('SUCCESS: afp Read/Write test')
        '''
        # Access share via SSH protocol
        self.log.info('Start to access share via SSH protocol')
        try:
            self.open_scp_connection()
            self.scp_file_to_device(files=tmp_file, remote_path=scp_target_path, recursive=False, preserve_times=False)
        except Exception as e:
            self.log.exception('Failed to access share via ssh protocol. Exception: {}'.format(repr(e)))
        '''
        # For Lightning, access USB drives via all protocol except NFS.
        if model == 'LT4A':
            usb_info = self.get_usb_info()[1]
            usb_share = self.get_xml_tags(usb_info, 'share')
            usb_bath_path = self.get_xml_tags(usb_info, 'base_path')
            if not usb_share:
                self.log.info('There is no USB share.')
            else:
                for item in usb_share:
                    try:
                        self.enable_ftp_share_access(share_name=item, access='Anonymous Read / Write')
                        self.log.info('Start to access USB share {} via SMB protocol'.format(item))
                        self.write_files_to_smb(username=access_user, password=access_user, files=tmp_file, share_name=item, delete_after_copy=False)
                        self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+item+'/', fileName=file_prefix+'0.jpg')
                        self.read_files_from_smb(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=item, delete_after_copy=True)
                        self.log.info('Start to access USB share {} via FTP protocol with anonymous enabled'.format(item))
                        self.write_files_to_ftp(username=access_user, password=access_user, files=tmp_file, share_name=item, delete_after_copy=False)
                        self.hash_check(localFilePath=tmp_file, uutFilePath='/shares/'+item+'/', fileName=file_prefix+'0.jpg')
                        self.read_files_from_ftp(username=access_user, password=access_user, files=file_prefix+'0.jpg', share_name=item, delete_after_copy=True)
                    except Exception as e:
                        self.log.exception('Test R/W to USB share via SMB protocol failed. Exception: {}'.format(repr(e)))
        '''
    def tc_cleanup(self):
        self.delete_all_shares()

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED!!')
        else:
            self.log.error('Hash test FAILED!!')
    '''
    def timeout_limit(self, timeout, func, args=(), kwargs={}):
        class TimeLimitExpired(Exception): pass

        def timelimit(timeout, func, args=(), kwargs={}):
            """ Run func with the given timeout. If func didn't finish running
                within the timeout, raise TimeLimitExpired
            """
            import threading
            class FuncThread(threading.Thread):
                def __init__(self):
                    threading.Thread.__init__(self)
                    self.result = None
        
                def run(self):
                    self.result = func(*args, **kwargs)
        
                def _stop(self):
                    if self.isAlive():
                        Thread._Thread__stop(self)
        
            it = FuncThread()
            it.start()
            it.join(timeout)
            if it.isAlive():
                it._stop()
                raise TimeLimitExpired()
            else:
                return it.result
    '''
    
fileSharingProtocols()