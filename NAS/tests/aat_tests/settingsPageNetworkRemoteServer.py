""" Settings page-Network-Remote Server

    @Author: Lee_e
    
    Objective: 
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 2.00.176)
                    KC       : Pass (FW: 2.00.155)
                    
              Jenkins Taiwan : Lightning    - Pass (FW: 2.00.185)
                             : Kings Canyon - Pass (FW: 2.00.174)
                             : Sprite       - Pass (FW: .00.174)
                             : Aurora       - Pass (FW: 2.00.171)
              Jenkins Irvine : Zion         - Pass (FW: 2.00.171)
                             : Glacier      - Pass (FW: 2.00.173)
                             : Yellowstone  - Pass (FW: 2.00.176)
                             : Yosemite     - Pass (FW: 2.00.173)
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os
import requests

server_password ='welc0me'


class RemoteServer(TestClient):
    def run(self):
        self.config_remote_server_cgi(enable=1, password=server_password)
        server_enable, server_pwd = self.get_remote_server_config_cgi()
        self.log.debug('server_enable:{0}, server_pwd:{1}'.format(server_enable, server_pwd))
        if (int(server_enable) == 1) and (server_pwd == server_password):
            self.log.info('PASS: remote server set to {0} successfully'.format(server_enable))
        else:
            self.log.error('FAIL: remote server set to {0}'.format(server_enable))
        
        self.config_remote_server_cgi(enable=0)
        server_enable, server_pwd = self.get_remote_server_config_cgi()
        self.log.debug('server_enable:{0}, server_pwd:{1}'.format(server_enable, server_pwd))
        if int(server_enable) == 0:
            self.log.info('PASS: remote server set to {0} successfully'.format(server_enable))
        else:
            self.log.error('FAIL: remote server set to {0}'.format(server_enable))       

    def tc_cleanup(self):
        self.config_remote_server_cgi(enable=0)    
    
    def get_remote_server_config_cgi(self):
        rsync_info_cmd = 'cgi_get_rsync_info'
        result = self.send_cgi(rsync_info_cmd)
        server_enable = ''.join(self.get_xml_tags(result, 'server_enable'))
        server_pwd = ''.join(self.get_xml_tags(result, 'server_pw'))
        
        return server_enable, server_pwd

    def config_remote_server_cgi(self, enable, password=None):
        if enable:
            password_config = '&f_password='+str(password)
        else:
            password_config = ''
        cmd = 'cgi_set_rsync_server&f_onoff='+str(enable) + password_config
        self.send_cgi(cmd)
        
    def send_cgi(self, cmd):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/remote_backup.cgi?cmd='.format(self.uut[self.Fields.internal_ip_address])
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {0}".format(r.status_code))

        r = s.get(cmd_url)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {0}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

RemoteServer()
