'''
Created on January 9, 2015

@author: Carlos Vasquez
'''

# Import the test case
from testCaseAPI.src.testclient import TestClient
from smb.smb_structs import OperationFailure
import sys
import time

user1 = 'user1'
user2 = 'user2'
group1 = 'group1'
group2 = 'group2'
group3 = 'group3'
group4 = 'group4'

share1 = 'user1'
share2 = 'user2'

class userGroupAssignment(TestClient):
    def run(self):
        
        self.log.info('Group Quota and Access Privilege Test started')
        self._cleanup()
        self.create_shares(share_name='share', number_of_shares=4, description='Share Description', public_access=False)
        
        self._createGroupSetQuota(groupName = group1, size_of_group_quota=10)
        self._createGroupSetQuota(groupName = group2, size_of_group_quota=15)
        self._createGroupSetQuota(groupName = group3, size_of_group_quota=20)
        self._createGroupSetQuota(groupName = group4, size_of_group_quota='')
        
        # Assign group to share
        for x in range(1,5):
            groupNumber = 'group{0}'.format(x)
            self.assign_group_to_share(group_name=groupNumber, share_number=x, access_type='rw')
        
        # validate an error occurs when the user quota is larger than group quota
        self._userGroupAssignment(user1, group1, 20)
        if self.element_find('popup_ok_button'):
            self.log.info('Success: Quota amount cannot be larger than group quota.')
        else:
            self.log.error('Able to set user quota larger than group quota.')

        self.click_element('popup_ok_button')
        time.sleep(2)
        self.click_element('uesrs_modQuotaCancel_button') 
        
        # set user quota to blank
        self._userGroupAssignment(user1, group1, '')
        time.sleep(1)
        
    def _cleanup(self):
        # a quick restore will work faster here.
        self.delete_all_users()
        self.execute('smbif -b user1')
        self.execute('smbif -b user2') 
        self.delete_all_shares()
        #TODO: delete all groups
        #For now, use the steps below
        self.delete_group('cloudholders')
        for i in range(1,5):
            groupName = 'group{}'.format(i)
            self.delete_group(groupName)
        
        
    def enough_space_validation(self, response, expected_failure = True):
        testname = sys._getframe().f_back.f_code.co_name 
        
        # Validate enough space
        if expected_failure == True:
            if 'file0.bmp' in response[0]:
                if response[2] < 92:
                    self.log.info('{0} Test Passed'.format(testname))
                else:
                    self.log.error('{0} Test Failed'.format(testname))         
        else:
            if 'file0.bmp' in response[0]:
                if response[2] >= 92:
                    self.log.info('{0} Test Passed'.format(testname))
                else:
                    self.log.error('{0} Test Failed'.format(testname))
    
    def _createGroupSetQuota(self, groupName, size_of_group_quota):
        try:
            quotaGB = size_of_group_quota * 1024
            # Create groupX and set the quota at XXGB
            self.create_groups(group_name=groupName)
            self.set_quota_all_volumes(groupName, 'group', quotaGB)
        except Exception:
            self.log.exception('Unable to create group or set group quota')
    
    def _userGroupAssignment(self, userName, groupName, quotaSize): 
        try:
            # Create userx and assign to groupx. Set user quota to xxGB
            self.create_user(userName)
            time.sleep(3)
            self.assign_user_to_group(user_name=userName, group_name=groupName)
            self.set_quota_all_volumes(groupName, 'user', amount=quotaSize)
          
            self.create_user_quota(user_name=userName, size_of_quota=quotaSize, unit_size='GB')
            
        except Exception:
            self.log.exception('Failed to set user quota') 
                  
    def group_quota(self,file_size, size_of_group_quota,size_of_user_quota):
        
        # The file size to transfer, this will convert KB to MB   
        file_size_MB = 1024 * (file_size)
        
        
 ###################################################################################################################       
        
        
        # Assign user1 and user2 to group1
        self.update_group(group=group, memberusers='#user1#,#user2#')
        
        # Generate a jpg test file
        jpgFiles = self.generate_files_on_workspace(file_size_MB, 1)
        
        # Write the file to the unit as user1
        response = self.write_files_to_smb(username=user1, files=jpgFiles, share_name=share1, delete_after_copy=True)
        
        # Generate a bmp test file
        bmpFiles = self.generate_files_on_workspace(file_size_MB, 1, 'bmp')
        
        # Write the file to the unit as user2
        try:
            self.write_files_to_smb(username=user2, files=bmpFiles, share_name=share2, delete_after_copy=True)
        except OperationFailure:
            self.log.info('Quota met, unable to write files. Passed.')
        else:
            self.log.error('Able to write files despite quota being met.')
     
        response = self.read_files_from_smb(files=bmpFiles, share_name=share2)
        
        # Validate enough space
        self.enough_space_validation(response, expected_failure=True)
        
        # Remove user1 from group1
        self.update_group(group=group, memberusers='#user2#')
        
        # Generate a bmp test file
        bmpFiles = self.generate_files_on_workspace(file_size_MB, 1, 'bmp')
        
        # Write the file to the unit as user2
        try:
            self.write_files_to_smb(username=user2, files=bmpFiles, share_name=share2, delete_after_copy=True)
        except OperationFailure:
            self.log.info('Quota met, unable to write files. Passed.')
        else:
            self.log.error('Able to write files despite quota being met.')
            
        response = self.read_files_from_smb(files=bmpFiles, share_name=share2)
        
        # Validate enough space
        self.enough_space_validation(response, expected_failure=True)
        
userGroupAssignment() 