""" Settings Page-General Cloud Access Rebuild

    @Author: Lee_e

    Objective: Verify that if wdphotos.db can be rebuild

    Automation: Full

    Supported Products:
        Aurora; Glacier; Kings Canyon; Lightning; Sprite; Yellowstone; Yosemite; Zion;

    Test Status:
              Local Lightning : Pass (FW: 2.10.159)
                    Lightning : Pass (FW: 2.10.182)
                    KC        : Pass (FW: 2.10.159)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.186 & 2.10.213)
                              : Yosemite     - PASS (FW: 2.10.180 & 2.10.213)
                              : Kings Canyon - PASS (FW: 2.10.186 & 2.10.213)
                              : Sprite       - PASS (FW: 2.10.182 & 2.10.213)
                              : Grand Teton  - PASS (FW: 2.10.209 & 2.10.215)
                              : Zion         - PASS (FW: 2.10.180 & 2.10.213)
                              : Aurora       - PASS (FW: 2.10.180 & 2.10.213)
                              : Yellowstone  - PASS (FW: 2.10.182 & 2.10.213)
                              : Glacier      - PASS (FW: 2.10.180 & 2.10.213)

"""
from testCaseAPI.src.testclient import TestClient


class SettingsPageGeneralCloudAccessRebuild(TestClient):

    def run(self):
        self.save_database_file()
        self.webUI_admin_login(username=self.uut[self.Fields.default_web_username])
        self.check_cloud_config()
        self.rebuild_content_database()

    def tc_cleanup(self):
        self.reset_cloud_db()

    def check_wdphotos_db(self):
        filename = 'wdphotos.db'
        output = self.execute('find / -name '+filename)[1]
        self.log.debug('check database if exist: {0}'.format(output))

        return output

    def check_cloud_config(self):
        self.log.info('### Check Cloud Config ###')
        self.get_to_page('Settings')
        self.wait_until_element_is_visible(self.Elements.GENERAL_BUTTON)
        self.click_wait_and_check(self.Elements.GENERAL_BUTTON, self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH)
        if self.is_element_visible(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK):
            self.log.info('Cloud Access already enable')
        else:
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH, self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK)
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK, self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK, timeout=60)
            self.log.info('turn on cloud access')

    def rebuild_content_database(self):
        self.start_test('rebuild content database')
        self.log.info('### Rebuild wdphotos database ###')
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK, self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_DIAG)
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_REBUILD, self.Elements.UPDATING_STRING, visible=False, timeout=60)
        result = self.check_wdphotos_db()
        if 'wdphotos.db' in result:
            self.pass_test('rebuild content database successfully')
        else:
            self.fail_test('rebuild content database successfully')

    def save_database_file(self):
        cloud_file = self.check_wdphotos_db()
        temp_name = ''
        if 'wdphotos.db' in cloud_file:
            temp_name = cloud_file+str('.org')
            self.execute('cp {0} {1}'.format(cloud_file, temp_name))
            self.log.info('cp {0} {1}'.format(cloud_file, temp_name))
            self.execute('rm -f '+str(cloud_file))
            self.log.info('rm -f {0}'.format(cloud_file))

        return temp_name, cloud_file

    def reset_cloud_db(self):
        reset_file, cloud_file = self.save_database_file()
        self.execute('mv {0} {1}'.format(reset_file, cloud_file))
        self.log.debug('mv {0} {1}'.format(reset_file, cloud_file))

    def webUI_admin_login(self, username):
        """
        :param username: user name you'd like to login
        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot('login')

SettingsPageGeneralCloudAccessRebuild()