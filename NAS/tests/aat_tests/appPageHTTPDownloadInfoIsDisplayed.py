""" Apps Page HTTP Download [MA-175]

    @Author: Vasquez_c
    
    Objective: Apps page is shown
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 2.00.229)
                    KC       : Pass (FW: 2.00.145)
              
              Jenkins Irvine : Glacier - pass (FW:2.10.116)
                             
""" 

import sys
import time
import os
import requests
import inspect
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as E
from testCaseAPI.src.testclient import TestClient

timeout = 20


httpFile = 'http://go.microsoft.com/fwlink/?LinkId=324638'
httpFileName = 'EIE11_EN-US_MCM_WIN764.EXE'
http_user = 'httpuser'
http_password = 'fituser'
rm_exe = 'rm /shares/Public/*.EXE'
folderName = 'Public'


class appsPageHTTPDownload(TestClient):
    
    def run(self):
        try:
#             self.click_element(E.APPS_HTTP_DOWNLOAD)
            self.get_to_page('Apps')
            self.input_text(E.APPS_HTTP_DOWNLOAD_URL, httpFile)
            self.click_element(E.APPS_HTTP_DOWNLOAD_BROWSE)
            time.sleep(5)
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            time.sleep(3)
            self.click_button('home_treeOk_button')
            self.input_text(E.APPS_HTTP_DOWNLOAD_RENAME, httpFileName)
            
            # Schedule download at 2AM (15 minutes schedule)
            self.click_element('f_hour')
            self.click_element('link=2AM')
            self.click_element('f_min')
            self.click_element('link=00')
            self.click_element('apps_httpdownloadsCreate_button')
            time.sleep(3)
            
            # Check that file was created
            self.is_file_good(filename=httpFileName, dest_share_name=folderName)
        except Exception, ex:
            raise Exception('Failed to HttpFtpDownloader \n' + str(ex))
        finally:
            self.tc_cleanup()
            self.end()
    

    def tc_cleanup(self):
        self.log.info('Delete downloaded file')
        self.execute(rm_exe)
        self.log.info('Resetting user and password')
        self.uut[self.Fields.web_username] = 'admin'
        self.uut[self.Fields.web_password] = ''
        self.log.info('Delete user')
        self.delete_user(http_user)
    
    def add_new_user(self, http_user, http_password):
        """
        Create a new user (RESTful API)
        @http_user: new user name
        @http_password: new user's password
        """        
        try:
            self.log.info("==> Create new user (by RESTAPI)")
            resp = self.get_user(http_user)[0]
            if resp == 0: # user exists
                self.delete_user(http_user)
                self.create_user(http_user, http_password)
            else: #404, user does not exist
                self.create_user(http_user, http_password)
            self.log.info("User: {} is created successfully.".format(http_user))
        except Exception as e:
            self.log.exception('FAILED: Unable to create new user, exception: {}'.format(repr(e)))
appsPageHTTPDownload()