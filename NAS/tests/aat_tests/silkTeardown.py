import psutil

#Kill any firefox instances that may be hanging around
#Need to wrap this in an exception handler because Windows throws an exception for getting the
#name of system processes
print "Killing Firefox processes"
for proc in psutil.process_iter():
	try:
		if proc.name.startswith('firefox'):
			proc.kill()
			try:
				proc.wait(timeout=30)
			except psutil.TimeoutExpired:
				print "Found a FireFox process but couldn't kill it...pid = %s" % proc.pid
				exit(1)
	except Exception:
		pass
		

exit(0)
