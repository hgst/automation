""" Test case template
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

class Tests(TestClient):
    # Create tests as function in this class
    def run(self):
        self.log.info('Test started')
        # Call all tests from this function

# This constructs the Tests() class, which in turn constructs TestClient() which triggers the Tests.run() function
Tests()