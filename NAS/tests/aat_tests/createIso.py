""" Create ISO (MA-382)

    @Author: James Banh, lin_ri
    Procedure @ http://wiki.wdc.com/wiki/Create_ISO


    Automation: Partial

    Not supported product:
        Glacier

    Support Product:
        Lightning
        YellowStone
        Yosemite
        Sprite
        Aurora
        Kings Canyon

    Test Status:
        Local Yellowstone: Pass (FW: 2.10.287)
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import os

from testCaseAPI.src.testclient import TestClient
import time
import os

TCNAME = '(MA-382) Create ISO'

class CreateIso(TestClient):
    # Create tests as function in this class
    def run(self):
        self.log.info('######################## {} TEST START ##############################'.format(TCNAME))
        if self.uut[self.Fields.product] in ('Glacier',):
            self.log.info("**** Glacier does not support ISO utility, skip the test!! ****")
            return
        share_name = 'isoShare'
        test_filename = 'isotest0.jpg'
        try:
            self.delete_all_shares()
            self.delete_iso_share_by_ui()
            self.run_on_device('rm -rf /shares/{}/*.*'.format(share_name))
            self.log.info("Create new share: {}".format(share_name))
            self.create_shares(share_name=share_name, force_webui=True)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            self.log.info("Generate random file on /shares/{}/".format(share_name))
            self.create_random_file('/shares/{}/'.format(share_name), filename=test_filename, blocksize=1024, count=10240)
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info("--- NFS service check ---")
            # Enable NFS service
            if self.uut[self.Fields.product] in ('Lightning', ):
                self.log.info("Lightning does not support NFS, skip nfs service enable")
            else:
                self.enable_nfs_share_access(share_name=share_name)
            self.log.info("--- Go to utilities under Settings ---")
            # Go to Utilities
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('diagnostics', 'css=#diagnostics.LightningSubMenuOn', visible=True)
            # CD-ROM
            self.create_iso_image(share_name, test_filename, 'test1', 'CDROM(650MB/74MIN)')
            self.mount_iso_share(share_name=share_name)
            self.verify_iso_settings(share_name, test_filename, 'test1')
            # DVD5
            self.create_iso_image(share_name, test_filename, 'test2', 'DVD5(4.7GB)')
            self.mount_iso_share(share_name=share_name)
            self.verify_iso_settings(share_name, test_filename, 'test2')
            # DVD9
            self.create_iso_image(share_name, test_filename, 'test3', 'DVD9(8.5GB)')
            self.mount_iso_share(share_name=share_name)
            self.verify_iso_settings(share_name, test_filename, 'test3')
            time.sleep(5)
        except Exception, ex:
            self.log.error('Failed to create ISO or mount ISO as share, ex: {0}'.format(ex))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
        else:
            self.log.info('===== ISO test success! =====')
        finally:
            self.log.info("====== Clean Up section ======")
            self.execute('rm -rf /shares/{}/*.*'.format(share_name))
            self.log.info("Clean all files under {} share folder".format(share_name))
            self.log.info('######################## {} TEST END ##############################'.format(TCNAME))

    def delete_iso_share_by_ui(self):
        try:
            self.log.info("=== Clean ISO Share by UI ===")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('settings_utilities_link', "settings_utilitiesLogs_button", visible=True)
            # Scroll the page to the bottom
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            row_elem = self.element_find("//div[@id=\'iso_list\']")
            rows = len(row_elem.find_elements_by_xpath("//div[@id=\'iso_list\']/ul/li"))
            self.log.info("ROWS: {}".format(rows))
            if rows == 0:
                self.log.info("No ISO mount share is created, skip the deletion!")
                return
            for i in range(rows):
                self.click_wait_and_check('settings_utilitiesISODel0_link',
                                          'popup_apply_button', visible=True)
                self.click_wait_and_check('popup_apply_button', visible=False)
                self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
                time.sleep(5)   # Waiting for updating
            self.log.info("=== All ISO shares are clean! ===")
        except Exception as e:
            self.log.exception("ERROR: Fail to delete ISO share by Web UI, exception: {}".format(repr(e)))
        finally:
            self.close_webUI()

    def create_iso_image(self, share_name, file_name, iso_name, iso_size):
        self.log.info('=== Creating ISO Image: {} ==='.format(iso_name))
        try:
            self.click_wait_and_check('settings_utilitiesIsoCreate_button', 'isoCD_title', visible=True)
            self.log.info('Entering ISO Size format')
            self.click_link_element('id_iso_size', iso_size)
            self.log.info('Selecting ISO Path')
            self.click_wait_and_check('settings_utilitiesIsoPath_button', 'settings_utilitiesIsoPathTreeCancel_button')
            self.log.info('Clicking {}'.format(share_name))
            # self.click_element('xpath=(//a[contains(text(), \'Public\')])[2]')
            self.click_wait_and_check("//div[@id=\'isotree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']".format(share_name),
                                      "//div[@id=\'isotree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/new/\']".format(share_name))
            self.log.info('Selected Path as {}'.format(share_name))
            self.click_wait_and_check('settings_utilitiesIsoPathTreeOk_button', visible=False)
            self.log.info('Inputting ISO name')
            self.input_text_check('settings_utilitiesIsoName_text', iso_name, do_login=False)
            self.log.info('Clicking Next')
            self.click_wait_and_check('settings_utilitiesIsoNext1_button', 'settings_utilitiesIsoNext2_button', visible=True)
            self.log.info("Select {} as ISO content".format(file_name))
            if self.is_element_visible("//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/Public/\']"):
                self.click_wait_and_check("//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/Public/\']",
                                          "//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/Public/new/\']",
                                          visible=False)
            self.click_wait_and_check("//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']".format(share_name),
                                      "//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/new/\']".format(share_name))
            iso_locator = "//a[@class=\'tree_click_focus\']/following-sibling::ul/li/label/span".format(share_name, file_name)
            iso_checkbox = "//a[@class=\'tree_click_focus\']/following-sibling::ul/li/label//input".format(share_name, file_name)
            check_status = self.element_find(iso_checkbox).get_attribute('checked')
            while check_status != 'true':
                self.click_element(iso_locator)
                check_status = self.element_find(iso_checkbox).get_attribute('checked')
            self.log.info("{} is selected: {}".format(file_name, check_status))
            self.click_element('settings_utilitiesIsoAdd_button')
            added_file = self.get_text("//div[@id=\'img_tree_div2\']/ul/li/ul/li/a")
            while added_file != file_name:
                self.log.info("added file: {}".format(added_file))
                self.log.info("Re-click Add button")
                self.click_element('settings_utilitiesIsoAdd_button')
                added_file = self.get_text("//div[@id=\'img_tree_div2\']/ul/li/ul/li/a")
            self.log.info("added file: {}".format(added_file))
            self.click_wait_and_check('settings_utilitiesIsoNext2_button', visible=False)
            time.sleep(10)  # Waiting for ISO image creation
            self.log.info('Finishing ISO Image Creation')
            self.click_wait_and_check('settings_utilitiesIsoSave_button', visible=False)
            time.sleep(5)
        except Exception as e:
            self.log.error("ERROR: Failed to create iso image due to exception: {}".format(repr(e)))
        else:
            self.log.info("=== PASS: Complete ISO image creation ===")

    def mount_iso_share(self, share_name):
        self.log.info('=== Mounting ISO as share: {} ==='.format(share_name))
        try:
            self.click_wait_and_check('settings_utilitiesISOShare_button', 'settings_utilitiesISOShareCancel_button')
            self.click_wait_and_check("//div[@id=\'iso_tree_container\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']".format(share_name),
                                      "//div[@id=\'iso_tree_container\']//a[@rel=\'/mnt/HD/HD_a2/{}/new/\']".format(share_name), visible=True)
            iso_locator = "//div[@id=\'iso_tree_container\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']/following-sibling::ul/li/label/span".format(share_name)
            iso_checkbox = "//div[@id=\'iso_tree_container\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']/following-sibling::ul/li/label//input".format(share_name)
            check_status = self.element_find(iso_checkbox).get_attribute('checked')
            while check_status != 'true':
                self.click_element(iso_locator)
                check_status = self.element_find(iso_checkbox).get_attribute('checked')
            file_name = os.path.basename(self.element_find(iso_checkbox).get_attribute('value'))
            self.log.info("{} is selected: {}".format(file_name, check_status))
            self.click_wait_and_check('settings_utilitiesISOShareNext_button',
                                      'settings_utilitiesISOShareNext1_button', visible=True)
            self.click_wait_and_check('settings_utilitiesISOShareNext1_button',
                                      'settings_utilitiesISOShareNext2_button', visible=True)
            if self.uut[self.Fields.product] in ('Lightning', ):
                self.click_wait_and_check('settings_utilitiesISOShareNext2_button', visible=False)
            else:
                self.click_wait_and_check('settings_utilitiesISOShareNext2_button',
                                          'settings_utilitiesISOShareSave_button', visible=True)
                self.click_wait_and_check('settings_utilitiesISOShareSave_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(5)  # Wait for updating
        except Exception as e:
            self.log.error("ERROR: Failed to mount iso share due to exception: {}".format(repr(e)))
        else:
            self.log.info("=== PASS: Complete ISO Mount share ===")

    # iso number starts at 0 and is a string
    def verify_iso_settings(self, share_name, file_name, iso_name):
        self.log.info('=== Verify ISO settings: {} ==='.format(iso_name))
        try:
            self.click_wait_and_check('settings_utilitiesISODetail0_link', 'settings_utilitiesISODetailCancel_button', visible=True)
            self.click_wait_and_check('settings_utilitiesISODetailCancel_button', visible=False)
            self.click_wait_and_check('settings_utilitiesISOEdit0_link', 'settings_utilitiesISOShareNext1_button', visible=True)
            self.click_wait_and_check('settings_utilitiesISOShareNext1_button', 'settings_utilitiesISOShareNext2_button', visible=True)
            time.sleep(5)
            self.log.info("Switch off Public ISO share")
            self.click_wait_and_check('css=#settings_utilitiesISOSharePublic_switch+span .checkbox_container',
                                      'css=#settings_utilitiesISOSharePublic_switch+span .checkbox_container .checkbox_off')
            if self.uut[self.Fields.product] in ('Lightning',):
                self.log.info("Skip NFS setting change")
                self.click_wait_and_check('settings_utilitiesISOShareNext2_button')
                self.log.info("Click Read Only for Admin")
                self.click_wait_and_check('settings_utilitiesISOShareR0_link', 'css=#settings_utilitiesISOShareR0_link.rDown')
            else:
                self.click_wait_and_check('settings_utilitiesISOShareNext2_button', 'settings_utilitiesISOShareNext3_button')
                self.log.info("Switch off NFS ISO share")
                self.click_wait_and_check('css=#settings_utilitiesISOShareNFS_switch+span .checkbox_container',
                                          'css=#settings_utilitiesISOShareNFS_switch+span .checkbox_container .checkbox_on')
                self.input_text_check('settings_utilitiesISOShareNFSHost_text', '*', do_login=False)
                self.click_wait_and_check('settings_utilitiesISOShareNext3_button', 'settings_utilitiesISOShareSave2_button')
                self.log.info("Click Read Only for Admin")
                self.click_wait_and_check('settings_utilitiesISOShareR0_link', 'css=#settings_utilitiesISOShareR0_link.rDown')
            self.click_wait_and_check('settings_utilitiesISOShareSave2_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(5)
            try:
                self.log.info(">>> SMB read access test <<<")
                self.read_files_from_smb(files=file_name, share_name=iso_name, delete_after_copy=False)
            except Exception as e:
                self.log.error("!!! ERROR: SMB read access failed, exception: {}".format(repr(e)))
            else:
                self.log.info(">>> PASS: SMB read access test <<<")
            time.sleep(5)
            self.click_wait_and_check('settings_utilitiesISODel0_link', 'popup_apply_button', visible=True)
            self.click_wait_and_check('popup_apply_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(5)
        except Exception as e:
            self.log.error("ERROR: Failed to verify ISO setting, exception: {}".format(repr(e)))
        else:
            self.execute('rm -rf /shares/{}/*.iso'.format(share_name))
            self.log.info("=== PASS: Complete ISO verification test ===")

# This constructs the Tests() class, which in turn constructs TestClient() which triggers the Tests.run() function
CreateIso()


