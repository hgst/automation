""" Settings page-General-Device Profile

    @Author: Lee_e
    
    Objective: Device profile page shows general info
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 1.05.30)
                    Lightning: Pass (FW: 2.00.40)
                    KC       : Pass (FW: 2.00.31)
              Jenkins Lightning: Pass (FW: 2.00.141)
                      Yellowstone: Pass (FW: 2.00.141)
                      Glacier: Pass (FW:2.00.144)
                      Yosemite:Pass (FW: 2.00.144)
                      Sprite: Pass (FW:2.00.144)
                      KC:Pass (FW: 2.00.148)
                      Aurora:Pass (FW: 2.00.148)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

update_machine_name = 'MyCloud'
update_machine_desc = 'NAS'

class DeviceProfile(TestClient):
    def run(self):
        global default_machine_name, default_machine_desc
        default_machine_name, default_machine_desc = self.read_device_profile()
        self.log.info('default_machine_name: {0}, default_machine_desc: {1}'.format(default_machine_name, default_machine_desc))
        self.update_device_profile_from_UI(update_machine_name, update_machine_desc)
        new_machine_name, new_machine_desc = self.read_device_profile_from_UI()
        self.log.info('new_machine_name: {0}, new_machine_desc: {1}'.format(new_machine_name, new_machine_desc))
        
        if (''.join(new_machine_name)) != update_machine_name:
            self.log.error('Fail to update machine name')
        else:
            self.log.info('PASS: update machine name successfully')
        
        if (''.join(new_machine_desc)) != update_machine_desc:
            self.log.error('Fail to update machine description')
        else:
            self.log.info('PASS: update machine description successfully')
     
    def tc_cleanup(self):
        global default_machine_name, default_machine_desc
        self.set_device_description(machine_name=default_machine_name, machine_desc=default_machine_desc)
        time.sleep(10)

    def read_device_profile(self):
        machine_name = self.get_xml_tags(self.get_device_description()[1], 'machine_name')
        time.sleep(5)
        machine_desc = self.get_xml_tags(self.get_device_description()[1], 'machine_desc')  
        return machine_name, machine_desc
    
    def read_device_profile_from_UI(self):
        ## Add workaround:click Generl Button for refresh UI (Device name & Description) 
        self.log.info('*** Get Device Profile ***')
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', self.Elements.GENERAL_BUTTON)
        self.click_wait_and_check(self.Elements.GENERAL_BUTTON, self.Elements.SETTINGS_DEVICE_NAME)
        machine_name = self.get_value(self.Elements.SETTINGS_DEVICE_NAME)
        machine_desc = self.get_value(self.Elements.SETTINGS_DEVICE_DESCRIPTION)
        self.close_webUI()  
        return machine_name, machine_desc
    
    def update_device_profile_from_UI(self, update_machine_name, update_machine_desc):
        self.set_device_name_from_UI(update_machine_name)
        self.set_device_description_from_UI(update_machine_desc)
        
    def set_device_name_from_UI(self, update_machine_name):
        self.log.info(' *** Set Device Name from UI ***')
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', self.Elements.GENERAL_BUTTON)
        self.click_wait_and_check(self.Elements.GENERAL_BUTTON, self.Elements.SETTINGS_DEVICE_NAME)
        self.input_text_check(self.Elements.SETTINGS_DEVICE_NAME, update_machine_name)
        self.click_wait_and_check(self.Elements.SETTINGS_DEVICE_NAME_APPLY, self.Elements.SETTINGS_DEVICE_NAME_SAVE)
        self.click_wait_and_check(self.Elements.SETTINGS_DEVICE_NAME_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
   
    def set_device_description_from_UI(self, update_machine_desc):
        self.log.info(' *** Set Device Description from UI ***')
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', self.Elements.GENERAL_BUTTON)
        self.click_wait_and_check(self.Elements.GENERAL_BUTTON, self.Elements.SETTINGS_DEVICE_DESCRIPTION)
        self.input_text_check(self.Elements.SETTINGS_DEVICE_DESCRIPTION, update_machine_desc)
        self.click_wait_and_check(self.Elements.SETTINGS_DEVICE_DESCRIPTION_APPLY, self.Elements.UPDATING_STRING, visible=False, timeout=60)

DeviceProfile()
