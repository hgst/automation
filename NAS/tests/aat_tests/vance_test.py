"""
Create on Jan 8, 2016
@Author: lo_va
Objective:  Use for vance testing
"""

import time

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
kamino_waiting_string = '~#'


class EnableNetworkandSSH(TestClient):

    def run(self):
        self.yocto_init()
        self.open_kamino_serial_connection()
        self.serial_wait_for_string(kamino_waiting_string, 30)
        self.serial_write('ifconfig egiga2')
        self.serial_wait_for_string('inet addr:', 20)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
            return fw_ver
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.open_kamino_serial_connection()
            self.serial_wait_for_string(kamino_waiting_string, 30)
            self.serial_write('ifconfig egiga2')
            self.serial_wait_for_string('inet addr:', 20)
            read_result = self.serial_read()
            self.log.info('read result: {0}'.format(read_result))
            return read_result

    def open_kamino_serial_connection(self):
        login_prompt = 'grandteton login:'
        prompt = 'grandteton:~#'
        # Open serial connection
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))
        loop_counter = 0

        # waiting for login prompt or prompt
        while read_result != login_prompt and read_result != prompt:
            self.serial_write('\n')
            time.sleep(5)
            read_result = self.serial_read()
            self.log.info('read result: {0}'.format(read_result))
            loop_counter += 1
            if loop_counter == 12:
                break

        if loop_counter < 12:
            self.log.info('Found a prompt')
        else:
            self.log.warning('No prompt is displayed')

        # if prompt is displayed, nothing to do
        # if login prompt is displayed, continue
        if read_result == login_prompt:
            self.log.info('Found login prompt')
            self.serial_wait_for_string('grandteton login:', 30)
            self.serial_write('root')
            self.serial_wait_for_string('Password:', 30)
            self.serial_write('\n')
            read_result = self.serial_read()
            self.log.info('read result: {0}'.format(read_result))

EnableNetworkandSSH(checkDevice=False)
