""" Storage-Page-Volume Virtualization Can Be Created

    @Author: Lee_e

    Objective: check if can create 2 set of multiple user with different parameter

    Automation: Full

    Supported Products:
        All (exlude Glacier on FW-2.01)

    Test Status:
              Local Lightning : Pass (FW: 2.00.215 & 2.00.216)
                    Lightning : Pass (FW: 2.10.105)
                    KC        : Pass (FW: 2.00.215 & 2.00.216)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.105 & 2.00.216)
                              : Yosemite     - PASS (FW: 2.10.105 & 2.00.216)
                              : Kings Canyon - PASS (FW: 2.10.105 & 2.00.216)
                              : Sprite       - PASS (FW: 2.00.216)
              Jenkins Irvine  : Zion         - PASS (FW: 2.10.105 & 2.00.216)
                              : Aurora       - PASS (FW: 2.10.105 & 2.00.216)
                              : Yellowstone  - PASS (FW: 2.10.105 & 2.00.216)
                              : Glacier      - PASS (FW: 2.00.216)

"""
from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.ui_map import Page_Info
import time
import requests

default_web_access_timeout_value = 5
iscsi_target_name = 'iqn.2013-03.com.wdc:wdmycloudex4:iscsi-target1'
iscsi_target = '192.168.1.108'
iscsi_share_folder_name = 'iscsi_folder'
user_list = ['user1']
user_password = 'welc0me'
group_list = ['g1']

class StoragePageVolumeVirtualizationCanBeCreated(TestClient):

    def run(self):
        #backup_server_ip = self.uut[self.Fields.iscsi_target]
        #iscsi_target_name = 'iqn.2013-03.com.wdc:dmycloudex4:iscsi-target1'
        #iscsi_share_folder_name = 'iscsi_folder'
        self.web_access_timeout_config(timeout_value=20)
        self.delete_all_shares()
        self.check_iscsi_target_cgi(iscsi_target_name=iscsi_target_name)
        self.log.info('Remote backup server: {}'.format(iscsi_target))
        self.create_volume_virtualization(iscsi_ip=iscsi_target, iscsi_target_name=iscsi_target_name, share_folder_name=iscsi_share_folder_name)
        self.check_iscsi_target_ui(iscsi_target_name=iscsi_target_name)

    def tc_cleanup(self):
        self.check_iscsi_target_cgi(iscsi_target_name=iscsi_target_name)
        self.web_access_timeout_config(timeout_value=default_web_access_timeout_value)

    def create_volume_virtualization(self, iscsi_ip=None, iscsi_target_name=None, share_folder_name=None):
        self.webUI_admin_login(username=self.uut[self.Fields.default_web_username])
        try:
            self.click_wait_and_check(Page_Info.info['Storage'].link, self.Elements.STORAGE_VOLUME_VIRTUALIZATION, timeout=10)
            self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE, timeout=10)
            self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_DIAG, timeout=10)
            if not self.get_text('CreateDiag_IQN'):
                raise Exception('Device does not has IQN')

            self.input_text_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_IP, iscsi_ip)
            self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT1, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT2, timeout=10)
            self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT2, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT3, timeout=10)
            iscsi_target_name_value = self.get_text('Create_Target')
            self.log.info('iSCSI Target Name: {0}'.format(iscsi_target_name_value))

            if iscsi_target_name in iscsi_target_name_value:
                self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT3, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT4, timeout=10)
                self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT4, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT5, timeout=10)
                self.input_text_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_SHARE_FOLDER, share_folder_name)
                self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT5, self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_SAVE, timeout=10)
                self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
            else:
                self.take_screen_shot('iSCSI_Target_Name')
                self.log.error('Wrong iSCSI Target Name: {0}'.format(iscsi_target_name_value))
        except Exception as e:
            self.take_screen_shot('create_volume_virtualization')
            self.log.exception('Exception: {0}'.format(repr(e)))

    def check_iscsi_target_ui(self, iscsi_target_name):
        value = self.get_text('css=#row1 > td > div')
        if iscsi_target_name in value:
            self.log.info('PASS: iSCSI target created successfully')
        else:
            self.log.error('FAIL: Failed to create iSCSI target')

    def connect_volume_virtualization(self):
        self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_ISCSI_TARGET_CONNECT_BUTTON, self.Elements.UPDATING_STRING, visible= False, timeout=60)
        self.click_wait_and_check(self.Elements.STORAGE_VOLUME_VIRTUALIZATION_ISCSI_TARGET_DIAG_CLOSE_BUTTON, self.Elements.STORAGE_VOLUME_VIRTUALIZATION)

    def check_iscsi_share_folder(self, share_folder_name=None):
        try:
            self.click_wait_and_check(Page_Info.info['Shares'].link, self.Elements.SHARES_CREATE_SHARE)
            resp = self.get_text('css= ul.LightningSubMenu.userMenu.shareMenuList')
            if not resp:
                raise Exception('Share folder does not exist')
            if share_folder_name in resp:
                self.log.info('PASS: iSCSI share folder exist')
            else:
                self.log.error('FAIL: iSCSI share folder does not exist')
        except Exception as e:
            self.take_screen_shot('create_volume_virtualization')
            self.log.exception('Exception: {0}'.format(repr(e)))

    def update_iscsi_share_permission(self, share_folder_name=None):

    def delete_iscsi_target(self, iscsi_target_name):
        cmd_head = 'virtual_vol.cgi'
        cmd = 'cgi_VirtalVol_Target_Del&f_target=' + iscsi_target_name
        self.send_cgi(cmd=cmd, url2_cgi_head=cmd_head)

    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

    def webUI_admin_login(self, username):
        """
        :param username: user name you'd like to login
        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("PASS: Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot('login')

    def check_iscsi_target_cgi(self, iscsi_target_name=None, delete=True):
        self.log.info('*** Check iSCSI target ***')
        cmd_head = 'virtual_vol.cgi'
        cmd = 'cgi_VirtalVol_List&user='
        response = self.send_cgi(cmd=cmd, url2_cgi_head=cmd_head)
        value = self.get_xml_tags(response, 'cell')
        self.log.info('Virtual Volume List: {0}' .format(value))
        if iscsi_target_name in value:
            if delete:
                self.delete_iscsi_target(iscsi_target_name=iscsi_target_name)
            else:
                self.log.info('iSCSI target already exist')
        else:
            if not delete:
                self.log.error('Failed to create iSCSI target')

    def web_access_timeout_config(self, timeout_value):
        self.log.info('*** Web Access Timeout set to {0} minutes ***'.format(timeout_value))
        cgi_idle = 'cgi_idle&f_idle='+str(timeout_value)
        head_url = 'system_mgr.cgi'
        self.send_cgi(cgi_idle, head_url)

    def check_update_user(self):
        users = self.get_user_list()
        for i in range(0, len(user_list)):
            username = user_list[i]
            if username in users:
                self.log.info('Modify user')
                self.modify_user_account(user=username, passwd=user_password)
            else:
                self.log.info('Add user')
                add_user_command = 'account -a -u {0} -p {1}'.format(username,user_password)
                self.run_on_device(add_user_command)

    def check_update_group(self):


StoragePageVolumeVirtualizationCanBeCreated()
