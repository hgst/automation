""" Apps page - Apps can be removed (MA-181)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=22214&execView=execDetails&view=details&pltab=steps&pId=50&nTP=117477&etab=8
        1) Open WebUI and login as admin
        2) Click on the Apps Category
        3) Select a non default app
        4) Click on the remove apps icon
        5) Verify that the app is removed. (Apps can be removed)

    Automation: Full

    Not supported product:
        N/A

    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon


    Test Status:
        Local Yosemite: Pass (FW: 2.00.201)
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import requests
import os
import inspect

availableApps = {
    'AcronisTrueImage': 'css=.LightningCheckbox,f_apps_install_0',
    'aMule': 'css=.LightningCheckbox,f_apps_install_1',
    'Anti-Virus Essentials': 'css=.LightningCheckbox,f_apps_install_2',
    'Dropbox': 'css=.LightningCheckbox,f_apps_install_3',
    'DVBLink': 'css=.LightningCheckbox,f_apps_install_4',
    'Icecast': 'css=.LightningCheckbox,f_apps_install_5',
    'Joomla': 'css=.LightningCheckbox,f_apps_install_6',
    'phpBB': 'css=.LightningCheckbox,f_apps_install_7',
    'phpMyAdmin': 'css=.LightningCheckbox,f_apps_install_8',
    'plexmediaserver': 'css=.LightningCheckbox,f_apps_install_9',
    'SqueezeCenter': 'css=.LightningCheckbox,f_apps_install_10',
    'Transmission': 'css=.LightningCheckbox,f_apps_install_11',
    'WordPress': 'css=.LightningCheckbox,f_apps_install_12',
    'Z-way': 'css=.LightningCheckbox,f_apps_install_13',
    'nzbget': 'css=.LightningCheckbox,f_apps_install_14',
}
picturecounts = 0
USE_CGI = 1

class appsPageAppsCanBeRemoved(TestClient):

    def run(self):
        self.log.info('######################## Apps page - Apps can be removed (MA-181) TEST START ##############################')
        if self.get_model_number() in ('GLCR', ):
            self.log.info("Glacier de-feature does not support app install, skip the test")
            return
        global USE_CGI
        try:
            self.initialize_available_apps()
            self.clean_all_installed_apps()
            # CGI
            if USE_CGI == 1:
                self.addAppCgi(appName='aMule')
                self.wait_for_app_install(appName='aMule')
            # UI
            else:
                self.get_to_page('Apps')
                self.click_wait_and_check('nav_addons', 'css=#nav_addons.current', visible=True)
                self.install_app_dialog()
                self.verifyAppWizard()
                # self.updateAvailableAppsLocator()  # 2.10.1xx and earlier
                # self.addAppUI(appName='aMule')
                self.addAppUI_2_10(appName='aMule')
            self.verify_installed_apps(app_name='aMule')
            # self.verifyApp(appName='aMule')
            self.removeAppUi2(appName='aMule')
        except Exception as e:
            self.log.error("FAILED: Apps page - Apps can be removed (MA-181) Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: Apps page - Apps can be removed (MA-181) Test PASS!!!")
        finally:
            self.log.info("========== (MA-180) Clean Up ==========")
            # self.clean_ff_profiles()
            self.clean_all_installed_apps()
            self.log.info('######################## Apps page - Apps can be removed (MA-181) TEST END ##############################')

    def clean_ff_profiles(self):
        import shutil, glob
        try:
            if os.path.exists('ff'):
                self.log.info("Delete profile folder: ff")
                shutil.rmtree('ff')
            cur_dir = os.getcwd()
            self.log.info("Switch to /tmp folder to clean up temporary folders")
            os.chdir('/tmp')
            tmp_files = glob.glob('tmp*')
            if tmp_files:
                for f in tmp_files:
                    self.log.info("Delete temp dir: {}".format(f))
                    shutil.rmtree(f)
            else:
                self.log.info("No tmp* folder is found")
            os.chdir(cur_dir)
            self.log.info("Switch back to original directory: {}".format(cur_dir))
        except Exception as e:
            self.log.exception("ERROR: Failed to clean ff profile, ex: {}".format(repr(e)))

    def initialize_available_apps(self):
        """
            Update all available app data
        :return:
        """
        global availableApps
        result = self.execute('cat /var/www/xml/app_info.xml')[1]
        app_list = self.get_xml_tags(result, 'Name')
        self.log.info("Found: {} apps".format(len(app_list)))
        for i, a in enumerate(app_list):
            availableApps[a] = 'css=.LightningCheckbox,f_apps_install_{}'.format(i)
        self.log.info("=== All available apps are initialized ===")
        return

    def install_app_dialog(self, retry=5):
        """
            Click 'Apps' page with retry to load available apps
        :param retry: how many attempts you'd like to try
        """
        while retry >= 0:
            self.click_wait_and_check('nav_apps_link', 'apps_httpdownloads_link', visible=True)
            time.sleep(10)  # wait for app server to load the app list
            self.wait_until_element_is_clickable('apps_installed_button', timeout=30)
            time.sleep(10)    # wait for loading
            self.accept_App_Eula()  # accept EULA
            try:
                self.click_wait_and_check('apps_installed_button', 'Apps_Cancel1_button', visible=True)
            except Exception as e:
                self.log.info("WARN: Fail to click app install button with 3 times retry. exception: {0}".format(repr(e)))
                self.take_screen_shot(prefix='noInstallAppButton')
                self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                continue
            if self.is_element_visible('SPAN_no_available_apps_desc', wait_time=10):
                if retry == 0:
                    self.log.error('ERROR: Unable to load the available app with maximum retries')
                    self.take_screen_shot(prefix='noAvailableApps')
                    raise Exception('Unable to load the available app due to unavailable xml from WD server')
                self.click_wait_and_check('Apps_Cancel1_button', visible=False)
                self.log.info("No available app, remaining {} retry".format(retry))
                retry -= 1
                continue
            elif self.is_element_visible('TR_AppsDiag_Browse_title1', wait_time=10):
                self.log.info("Available app is loaded successfully.")
                break

    def removeAppUi2(self, appName='IceCast'):
        """
        :param appName: App name you'd like to remove
        """

        try:
            retry = 3
            while retry >= 0:
                self.get_to_page('Apps')
                self.click_wait_and_check('nav_addons', 'css=#nav_addons.current', visible=True)
                time.sleep(5)  # Give some time to load the icon
                self.click_wait_and_check('{}'.format(appName),
                                          'css=#{}.apkg_menu.LightningSubMenuEnd.LightningSubMenuOn'.format(appName),
                                          visible=True)
                time.sleep(5)
                try:
                    self.log.info("Going to click Remove {} app button".format(appName))
                    self.click_wait_and_check('apps_del_button', 'popup_apply_button', visible=True)
                except Exception as e:
                    if retry == 0:
                        raise Exception("ERROR: fail to click {} app del button, ex:{}".format(appName, repr(e)))
                    self.log.info("App del button is not clickable, close UI. Remaining {} retries".format(retry))
                    self.close_webUI()
                    retry -= 1
                    continue
                else:
                    # Succeed to click App del button
                    self.log.info("App del button is clicked, continue the app removal test")
                    break
        except Exception as e:
            self.log.error("ERROR: Unable to click app remove button, exception: {}".format(repr(e)))
        try:
            self.click_wait_and_check('popup_apply_button', visible=False)
            self.log.info("Click OK to confirm {} app removal action".format(appName))
        except Exception as e:
            self.log.error("ERROR: Unable to remove {} app, exception: {}".format(appName, repr(e)))
        else:
            self.reload_page()
            self.get_to_page('Apps')
            self.click_wait_and_check('nav_addons', 'css=#nav_addons.current', visible=True)
            time.sleep(3)
            if self.is_element_visible('css=#{} > div'.format(appName), wait_time=5):
                self.log.error("ERROR: Failed to remove {} app".format(appName))
                self.takeScreenShot(prefix='FailtoRemoveApp')
            else:
                self.log.info("PASS: Succeed to remove {} app".format(appName))
        finally:
            self.close_webUI()

    def removeAppUi(self, appName='IceCast'):
        """
        :param appName: App name you'd like to remove
        """
        # self.set_browser_profile()
        try:
            self.get_to_page('Apps')
            self.click_wait_and_check('nav_addons', 'css=#nav_addons.current', visible=True)
            time.sleep(5)  # Give some time to load the icon
            if self.is_element_visible('css=#{} > div'.format(appName), wait_time=30):
                self.click_wait_and_check('{}'.format(appName),
                                          'css=#{}.apkg_menu.LightningSubMenuEnd.LightningSubMenuOn'.format(appName),
                                          visible=True)
                if not self.is_element_visible("apps_del_button", wait_time=15):
                    self.log.info("App del button is not visible, click other button then back")
                    self.click_wait_and_check('http_downloads',
                                              'css=#http_downloads.LightningSubMenuOn.LightningSubMenuFirst',
                                              visible=True)
                    self.click_wait_and_check('{}'.format(appName),
                                              'css=#{}.apkg_menu.LightningSubMenuEnd.LightningSubMenuOn'.format(appName),
                                              visible=True)
                # Click App del button
                self.log.info("Click Remove {} app button".format(appName))
                self.click_wait_and_check('apps_del_button',
                                          'popup_apply_button', visible=True)
                self.click_wait_and_check('popup_apply_button', visible=False)
                self.log.info("Click OK to confirm {} app removal action".format(appName))
            else:
                raise Exception("ERROR: Unable to click {} app to launch due to invisible element...".format(appName))
        except Exception as e:
            self.log.error("ERROR: Unable to remove {} app, exception: {}".format(appName, repr(e)))
        else:
            self.reload_page()
            self.get_to_page('Apps')
            self.click_wait_and_check('nav_addons', 'css=#nav_addons.current', visible=True)
            time.sleep(3)
            if self.is_element_visible('css=#{} > div'.format(appName), wait_time=5):
                self.log.error("ERROR: Failed to remove {} app".format(appName))
                self.takeScreenShot(prefix='FailtoRemoveApp')
            else:
                self.log.info("PASS: Succeed to remove {} app".format(appName))
        finally:
            self.close_webUI()

    def set_browser_profile(self, uut_ip=None, fp=None, profile_dir=None):
        """
            Set Firefox profile @ Author: Rick Lin
        :param uut_ip: IP address you'd like to browse
        :param fp: FirefoxProfile instance if you want to set other additional settings
        :param profile_dir: Firefox profile directory you'd like to initialize
        """
        from selenium.webdriver import FirefoxProfile
        if not uut_ip:
            uut_ip = self.uut[Fields.internal_ip_address]
        if not profile_dir:
            if not os.path.exists('ff'):
                self.log.info("Create firefox Profile folder: ff")
                os.mkdir('ff')
            # /home/fitbranded/source-files/aat/ff
            ff_dir = os.path.join(os.path.abspath(os.curdir), 'ff')
            self.log.info("(ff_dir)profile directory: {}".format(ff_dir))
        else:
            ff_dir = profile_dir
        if not fp:
            fp = FirefoxProfile(profile_directory=ff_dir)
        fp.set_preference("browser.cache.disk.enable", False)
        fp.set_preference("browser.cache.memory.enable", False)
        fp.set_preference("browser.cache.offline.enable", False)
        fp.set_preference("network.http.use-cache", False)
        fp.set_preference("places.history.enable", False)
        fp.set_preference("privacy.clearOnShutdown.cache", True)
        fp.set_preference("privacy.clearOnShutdown.cookie", True)
        fp.set_preference("privacy.clearOnShutdown.history", True)
        fp.set_preference("privacy.clearOnShutdown.sessions", True)
        fp.set_preference("browser.download.folderList", 2)
        fp.set_preference("browser.download.manager.showWhenStarting", False)
        fp.set_preference("browser.download.dir", os.getcwd())
        fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/plan")
        fp.update_preferences()
        # print fp.default_preferences
        # /tmp/tmpFQvmCw/webdriver-py-profilecopy (fp.path)
        self.log.info("FF profile directory: {}".format(fp.path))
        uut_url = 'http://{}'.format(uut_ip)
        self._sel.driver.open_browser(url=uut_url, ff_profile_dir=fp.path)
        self.log.info("Customized firefox profile is set")
        return fp.path

    def verify_installed_apps(self, app_name='aMule'):
        self.log.info('Verify installed apps for {}'.format(app_name))
        result = self.execute('cat /var/www/xml/apkg_all.xml')[1]
        if 'No such file' in result:
            self.log.info('No installed apps')
            return
        else:
            installed_apps = self.get_xml_tags(result, 'name')
            if not installed_apps:
                self.log.error("ERROR: No installed apps found, aMule is not installed")
                self.log.info("apkg_all.xml result: {}".format(result))
            else:
                if app_name in installed_apps:
                    self.log.info('PASS: {} installed apps, {} is installed'.format(len(installed_apps), app_name))
                else:
                    self.log.error('ERROR: {} installed apps, {} is not installed'.format(len(installed_apps), app_name))
                    self.log.info('Installed apps: {}'.format(','.join(installed_apps)))

    def verifyApp(self, appName='IceCast'):
        # self.set_browser_profile()
        # self._sel.driver.delete_all_cookies()
        # self.log.info("Browser cookies are deleted")
        self.get_to_page('Apps')
        self.click_wait_and_check('nav_addons', 'css=#nav_addons.current', visible=True)
        time.sleep(3)
        # <div class="">Acronis True Image</div>
        if self.is_element_visible('css=#{} > div'.format(appName), wait_time=30):
            self.log.info("PASS: Going to select {} app".format(appName))
            self.click_wait_and_check('{}'.format(appName),
                                      'css=#{}.apkg_menu.LightningSubMenuEnd.LightningSubMenuOn'.format(appName),
                                      visible=True)
            self.log.info("Click APP: {}, wait for page loading".format(appName))
            time.sleep(10)
            self.wait_until_element_is_visible('app_show_name', timeout=5)
            app_text = ''.join(self.get_text('app_show_name').split())
            if app_text.lower() == appName.lower():
                self.log.info("PASS: {} app is launched successfully!!".format(app_text))
            else:
                self.log.error("FAILED: {} app can not found, web shows [{}] app".format(appName, app_text))
        else:
            self.log.error("ERROR: Unable to click {} app to launch!!!".format(appName))
            self.takeScreenShot(prefix='verifyApp')
        self.close_webUI()
        # self.reload_page()

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

    def clean_all_installed_apps(self):
        self.log.info('Check apkg_all.xml for installed apps.')
        result = self.execute('cat /var/www/xml/apkg_all.xml')[1]
        if 'No such file' in result:
            self.log.info('No installed apps')
            return

        installed_apps = self.get_xml_tags(result, 'name')
        if not installed_apps:
            self.log.info('There are no installed apps')
        else:
            self.log.info('There are {0} installed apps'.format(len(installed_apps)))
            for apps in installed_apps:
                self.log.info('Deleting app: {0}'.format(apps))
                result = self.delAppCgi(apps)
                cgi_result = self.get_xml_tags(result, 'result')
                if '1' in cgi_result:
                    self.log.info('App: {0} is deleted.'.format(apps))
                else:
                    raise Exception('Failed to delete app: {0}!, cgi_resp = {1}'.format(apps, cgi_result))
        self.log.info("=== All Apps are deleted ===")

    def cleanApp(self):
        global availableApps
        for k in availableApps.keys():
            self.log.debug("Remove {} app".format(k))
            self.delAppCgi(appName=k)
        self.log.info("*** All apps are cleared! ***")

    def addAppUI_2_10(self, appName='IceCast'):
        """
            Add IceCast App
        :param appName: App Name
        """
        global availableApps
        if appName not in availableApps.keys():
            raise Exception("ERROR: Unable to find the {} app in current available app list".format(appName))
        old_top = 0.0
        cur_top = 0.0
        while True:
            try:
                self.click_wait_and_check(availableApps[appName].rsplit(',')[-1], 'Apps_next_button_3', visible=True)
                # self.select_checkbox(availableApps[appName], timeout=5)
            except Exception:  # ElementNotVisibleException
                self.log.debug("Going to scroll the scrollbar to find the {} app".format(appName))
                scroll = self.element_find('css=.jspDrag')
                scrolltext = scroll.get_attribute('style')
                self.log.info("DBG - STYLE: {}".format(scrolltext))
                if 'top' not in scrolltext:
                    scrollcount = 50.0
                else:
                    cur_top = float(scrolltext.split('top:')[-1].split('px;')[0].strip())
                    if cur_top > old_top:    # Lightning: 124px, Yellowstone: 109px
                        scrollcount = 50.0
                    else:
                        self.log.info("Reach the bottom of the scrollbar, current top: {} px".format(cur_top))
                        raise Exception("ERROR: Can not find the app {} and hit the bottom of the scroll bar".format(appName))
                try:
                    self.drag_and_drop_by_offset('css=.jspDrag', 0, scrollcount)  # max = 124px
                except Exception:
                    pass
                finally:
                    old_top = cur_top
            else:
                break
        if self.is_element_visible('Apps_next_button_3'):
            self.click_wait_and_check('Apps_next_button_3', visible=False)
            self.log.info("Click Install to continue the APP installation")
        time.sleep(1)
        appcmd = 'ps aux | grep -v grep | grep download.wdc.com'
        count = 0
        while True:
            if count >= 10:
                self.log.info("Reach the maximum 10 minutes wait for App installation")
                break
            try:
                appResult = self.run_on_device(appcmd)
            except Exception as e:
                self.log.info("Retry: Unable to check app status, exception: {}".format(repr(e)))
                time.sleep(60)
                count += 1
                self.log.info("{} minute wait".format(count))
                continue
            else:
                if appResult:
                    self.log.info("{} app is still installing...".format(appName))
                    self.log.debug("== {} ==".format(appResult))
                    time.sleep(60)
                    count += 1
                    self.log.info("{} minute wait".format(count))
                    continue
                else:
                    self.log.info("{} app completes the installation".format(appName))
                    break
        time.sleep(5)
        if self.is_element_visible('popup_ok_button'):
            self.log.info("Click OK to confirm the APP installation")
            self.click_wait_and_check('popup_ok_button', visible=False)
        # self.close_webUI()
        self.reload_page()
        time.sleep(2)

    def addAppUI(self, appName='IceCast'):
        """
            Add IceCast App
        :param appName: App Name
        """
        global availableApps
        if appName not in availableApps.keys():
            raise Exception("ERROR: Unable to find the {} app in current available app list".format(appName))
        old_top = 0.0
        cur_top = 0.0
        while True:
            try:
                self.select_checkbox(availableApps[appName], timeout=5)
            except Exception: # ElementNotVisibleException
                self.log.debug("Going to scroll the scrollbar to find the {} app".format(appName))
                scroll = self.element_find('css=.jspDrag')
                scrolltext = scroll.get_attribute('style')
                self.log.info("DBG - STYLE: {}".format(scrolltext))
                if 'top' not in scrolltext:
                    scrollcount = 50.0
                else:
                    cur_top = float(scrolltext.split('top:')[-1].split('px;')[0].strip())
                    if cur_top > old_top:    # Lightning: 124px, Yellowstone: 109px
                        scrollcount = 50.0
                    else:
                        self.log.info("Reach the bottom of the scrollbar, current top: {} px".format(cur_top))
                        raise Exception("ERROR: Can not find the app {} and hit the bottom of the scroll bar".format(appName))
                try:
                    self.drag_and_drop_by_offset('css=.jspDrag', 0, scrollcount)  # max = 124px
                except Exception:
                    pass
                finally:
                    old_top = cur_top
            else:
                break
        count = 3
        while count > 0:
            try:
                if not self.is_checkbox_selected(availableApps[appName]):
                    self.select_checkbox(availableApps[appName])
                    self.log.info("Select {} app to install".format(appName))
                else:
                    self.log.info("*** {} is selected ***".format(appName))
            except Exception as e:
                self.log.error("ERROR: Unable to select {} app, exception: {}".format(appName, repr(e)))
            time.sleep(1)
            if self.is_element_visible('Apps_next_button_3'):
                self.wait_and_click('Apps_next_button_3')
                self.log.info("Click Install to continue the APP installation")
                break
            else:
                self.log.info("RETRY: Unable to click Install button, going to re-check the checkbox")
                count -= 1
                time.sleep(1)
                continue
        time.sleep(1)
        appcmd = 'ps aux | grep -v grep | grep download.wdc.com'
        count = 0
        while True:
            if count >= 10:
                self.log.info("Reach the maximum 10 minutes wait for App installation")
                break
            try:
                appResult = self.run_on_device(appcmd)
            except Exception as e:
                self.log.info("Retry: Unable to check app status, exception: {}".format(repr(e)))
                time.sleep(60)
                count += 1
                self.log.info("{} minute wait".format(count))
                continue
            else:
                if appResult:
                    self.log.info("{} app is still installing...".format(appName))
                    self.log.debug("== {} ==".format(appResult))
                    time.sleep(60)
                    count += 1
                    self.log.info("{} minute wait".format(count))
                    continue
                else:
                    self.log.info("{} app completes the installation".format(appName))
                    break
        time.sleep(5)
        if self.is_element_visible('popup_ok_button'):
            self.log.info("Click OK to confirm the APP installation")
            self.click_wait_and_check('popup_ok_button', visible=False)
        # self.close_webUI()
        self.reload_page()
        time.sleep(2)

    def updateAvailableAppsLocator(self):
        """
            Update available apps locator
        """
        global availableApps
        appdict = {}
        count = 3
        while count > 0:
            try:
                elementlist = self._sel.driver._element_find("css=.LightningCheckbox", False, True)
                for element in elementlist:
                    applist = element.find_elements(by="xpath", value=".//*")
                    if applist:
                        for app in applist:
                            id = app.get_attribute('id')
                            name = app.get_attribute('value')
                            if name in availableApps.keys():
                                appdict[name] = id
            except Exception as e:
                self.log.error("ERROR: Unable to locate the element, exception: {}".format(repr(e)))
                count -= 1
                self.log.info("Going to re-locate elements, remaining {} retry".format(count))
                continue
            else:
                num_of_apps = int(len(appdict.keys()))
                if num_of_apps == 0:
                    count -= 1
                    self.log.info("Going to reload the app wizard, remaining {} retry".format(count))
                    self.wait_and_click('Apps_Cancel1_button', timeout=5)
                    self.wait_and_click('apps_installed_button', timeout=5)
                    continue
                self.log.info("Found {} apps available".format(num_of_apps))
                for k, v in appdict.items():
                    availableApps[k] = 'css=.LightningCheckbox,' + v
                break

    def verifyAppWizard(self):
        """
            Verify App Wizard window is launched!
        """
        time.sleep(3)
        while self.is_element_visible('apps_next_button_7', wait_time=5): # App term of service
            self.log.info("Going to accept APP term of service!")
            self.click_element('apps_next_button_7')
        self.wait_until_element_is_visible('AppsDiag_title')
        if self.is_element_visible('AppsDiag_title'):
            self.log.info("PASS: [Add an App] wizard window is launched!")
        else:
            self.log.error("ERROR: [Add an App] wizard window is NOT FOUND!!!")

    def wait_for_app_install(self, appName='IceCast'):
        """
            Waiting for app installation
        :return:
        """
        appcmd = 'ps aux | grep -v grep | grep download.wdc.com'
        count = 0
        while True:
            if count >= 10:
                self.log.info("Reach the maximum 10 minutes wait for App installation")
                break
            try:
                appResult = self.run_on_device(appcmd)
            except Exception as e:
                self.log.info("Retry: Unable to check app status, exception: {}".format(repr(e)))
                time.sleep(60)
                count += 1
                self.log.info("{} minute wait".format(count))
                continue
            else:
                if appResult:
                    self.log.info("{} app is still installing...".format(appName))
                    self.log.debug("== {} ==".format(appResult))
                    time.sleep(60)
                    count += 1
                    self.log.info("{} minute wait".format(count))
                    continue
                else:
                    self.log.info("{} app completes the installation".format(appName))
                    break
        time.sleep(10)

    ###############################################################################
    # CGI
    ###############################################################################
    def send_cgi_cmd(self, command, uutIP=None):
        destip = uutIP or self.uut[Fields.internal_ip_address]
        loginurl = 'http://{}/cgi-bin/login_mgr.cgi?'.format(destip)
        logindata = {
            'cmd': 'wd_login',
            'port': '',
            'username': 'admin',
            'pwd': ''
        }
        s = requests.session()
        r = s.post(url=loginurl, data=logindata)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))
        cmdUrl = 'http://{}/cgi-bin/'.format(destip)
        r = s.get(cmdUrl+command)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))
        self.log.debug("Successful CGI CMD: {}".format(cmdUrl+command))

    def addAppCgi(self, appName='IceCast'):
        if appName not in availableApps.keys():
            self.log.error("{} is NOT an available app!!".format(appName))
            raise Exception("Invalid App Name: {}".format(appName))
        cmd = 'apkg_mgr.cgi?cmd=cgi_apps_auto_install&f_module_name={}'.format(appName)
        resp = self.send_cgi_cmd(cmd)
        time.sleep(5)
        self.log.info("*** {} app is installed using CGI command ***".format(appName))
        return resp

    def delAppCgi(self, appName='IceCast'):
        if appName not in availableApps.keys():
            self.log.warning("WARN: {} is NOT an available app!!".format(appName))
        cmd = 'apkg_mgr.cgi?cmd=cgi_apps_del&f_module_name={}'.format(appName)
        resp = self.send_cgi_cmd(cmd)
        return resp

    def accept_App_Eula(self):
        """
            Accept Apps EULA
        """
        head = 'apkg_mgr.cgi?'
        cmd = 'cgi_apps_eula_set'
        self._cgi._send_cgi_cmd(command=cmd, header=head)

appsPageAppsCanBeRemoved()