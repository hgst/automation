"""
Create on Jan 28, 2016
@Author: lo_va
Objective:  Verify Conn Manager module is loaded in firmware
            Verify connmgr daemon is running
            KAM-534 (M3 feature)
wiki URL: http://wiki.wdc.com/wiki/Load_Conn_Manager_in_firmware
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadConnManager(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        connmanager_daemon = ''
        connmanager_files_list = ''
        try:
            connmanager_daemon = self.execute('/etc/init.d/connmgrd status')[1]
            connmanager_files_list = self.execute('find / -name *connmgr*')[1]
            self.log.debug('Conn Manager Daemon: {}'.format(connmanager_daemon))
            self.log.debug('Conn Manager find files list: {}'.format(connmanager_files_list))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['connmgr is running']
        check_list2 = ['/usr/local/connmgr/connmgr', '/etc/init.d/connmgrd']
        if all(word in connmanager_daemon for word in check_list1) \
                and all(word in connmanager_files_list for word in check_list2):
            self.log.info('Load Conn Manager in firmware test PASSED!!')
        else:
            self.log.error('Load Conn Manager in firmware test FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

LoadConnManager(checkDevice=False)
