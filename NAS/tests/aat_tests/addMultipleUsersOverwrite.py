""" Add Multiple Users - Overwrite

    @Author: Lee_e
    
    Objective: check if user can be overwrite when select overwrite user checkbox 
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lghtning : Pass (FW: 2.00.215 & 2.00.205)
                    KC       : Pass (FW: 2.00.215 & 2.00.205)
              Jenkins Taiwan : Lightning    - PASS (FW: 2.00.215)
                             : Yosemite     - PASS (FW: 2.00.215)
                             : Kings Canyon - PASS (FW: 2.00.215)
                             : Sprite       - PASS (FW: 2.00.215)
                             : Yellowstone  - PASS (FW: 2.00.212)
                             : Glacier      - PASS (FW: 2.00.215)
              Jenkins Irvine : Zion         - PASS (FW: 2.00.215)
                             : Aurora       - PASS (FW: 2.00.215)
                             : Glacier      - PASS (FW: 2.00.215)
                             : Yellowstone  - PASS (FW: 2.00.215)
     
""" 
from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.ui_map import Page_Info
import time
import os

user_list = ['user1', 'user2']
user_quota = 5120
user_password = 'welc0me'

class AddMultipleUsersOverwrite(TestClient):
    def run(self):
        users = self.get_user_list()
        for i in range(0, len(user_list)):
            username = user_list[i]
            if username in users:
                delete_user_command = 'account -d -u {0}'.format(username)
                self.log.info('delete {0}'.format(username))
                self.run_on_device(delete_user_command)
        self.add_multiple_user(user_list, user_quota, user_password)
        self.webUI_admin_login(self.uut[self.Fields.default_web_username])
        self.overwrite_user_from_UI(checked=False)
        self.overwrite_user_from_UI(checked=True)

    def tc_cleanup(self):
        self.log.info('delete user')
        self.delete_all_users(use_rest=False)

    def get_user_list(self):
        self.log.info('*** get user list ***')
        users = self.execute('account -i user')[1].split()

        return users

    def add_multiple_user(self, user_list=None, user_quota=None, password=None):
        self.log.info('*** Testing status: Add User ***')
        for user in user_list:
            self.execute("account -a -u '{0}' -p '{1}'".format(user, password))
            self.execute("/usr/sbin/quota_set -u {0} -s {1} -a".format(user, user_quota))
    
    def overwrite_user_from_UI(self, checked, timeout=60):
        self.log.info('*** Testing status: Overwrite select-{0} User From UI Check ***'.format(checked))
        try:
            self.click_wait_and_check(Page_Info.info['Users'].link, self.Elements.USERS_ADD_MULTIPLE_USERS_LINK, timeout=timeout)
            self.click_wait_and_check(self.Elements.USERS_ADD_MULTIPLE_USERS_LINK, self.Elements.USERS_ADD_MULTIPLE_USERS_DIAG, timeout=timeout)
            self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_USERS, self.Elements.USERS_CREATE_MULTIPLE_NEXT1,timeout=timeout)
            self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT1, self.Elements.USERS_CREATE_MULTIPLE_NAME_PREFIX, timeout=timeout)
            self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_NAME_PREFIX, text='user')
            self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_ACCOUNT_PREFIX, text='1')
            self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_NUMBER, text='2')
            self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_PASSWORD, text=user_password)
            self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_PASSWORD_CONFIRM, text=user_password)
            if checked:
                if not self.is_checkbox_selected(self.Elements.USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX):
                    self.select_checkbox(self.Elements.USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX)
            else:
                if self.is_checkbox_selected(self.Elements.USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX):
                    self.unselect_checkbox(self.Elements.USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX)
            self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT2, self.Elements.USERS_CREATE_MULTIPLE_NEXT3, timeout=timeout)
            self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT3, self.Elements.USERS_CREATE_MULTIPLE_QUOTAS, timeout=timeout)
            self.input_text_check(self.Elements.USERS_CREATE_MULTIPLE_QUOTAS, text='5')
            self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_NEXT4, self.Elements.USERS_CREATE_MULTIPLE_SAVE, timeout=timeout)

            if not checked:
                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_SAVE, 'css=div.LightningStatusPanel', timeout=timeout)
                self.element_text_should_be('error_dialog_message_list', expected_text='Import user error.')
                self.log.info('PASS')
                self.click_wait_and_check('popup_ok_button', 'users_batchCancel2_button', timeout=timeout)
                self.click_wait_and_check('users_batchCancel2_button', self.Elements.USERS_ADD_MULTIPLE_USERS_LINK, timeout=timeout)
            else:
                self.click_wait_and_check(self.Elements.USERS_CREATE_MULTIPLE_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=timeout)
                self.log.info('PASS')
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot('Overwrite_select_'+str(checked))

    def webUI_admin_login(self, username):
        """
        :param username: user name you'd like to login

        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("PASS: Succeed to login as [{}] user".format(username))
        except Exception as e:
            self.log.exception('Exception: {0}'.format(repr(e)))
            self.take_screen_shot('web_ui_login')
AddMultipleUsersOverwrite()