""" Configuration Backup Restore (MA-217)

    @Author: yang_ni

    Procedure : http://wiki.wdc.com/wiki/Configuration_Backup-Restore

                1. On UUT, take note of the current state of the all settings in the table below
                2. Save the configuration
                3. Change each of the settings below to something different than what they are now
                4. Restore the configuration from the saved configuration file
                5. Verify all settings are restored to what was previously saved
                6. Reboot the unit. Verify that the settings are still retained

                Setting	                            3GNAS	Zermatt	    Sequoia	    Lightning 4A	Kings Canyon
                Machine name	                    Y*	    Y*	        Y*	            Y	            Y
                Machine description	                Y*	    Y*	        Y*	            Y	            Y
                Network settings	                Y*	    Y*	        Y*	            Y	            Y
                DHCP/static	                        Y*	    Y*	        Y*	            Y	            Y
                IP	                                Y*	    Y*	        Y*	            Y	            Y
                Netmask	                            Y*  	Y*	        Y*	            Y	            Y
                Gateway	                            Y*	    Y*	        Y*	            Y	            Y
                DNS0, DNS1, DNS2	                Y*	    Y*	        Y*	            Y	            Y
                Time Zone	                        Y	    Y	        Y	            Y	            Y
                NTP Enable	                        Y	    Y	        Y	            Y	            Y
                Remote Access enable	            Y	    N	        N	            Y	            Y
                Alert enable	                    Y	    Y	        N	            N	            N
                Email return path	                Y	    Y	        N	            N	            N
                Email recipient(s)	                Y	    Y	        Y	            Y	            Y
                Firmware update auto install enable	Y	    Y	        Y	            Y	            Y
                Firmware update auto install day	Y	    Y	        Y	            Y	            Y
                Firmware update auto install hour	Y	    Y	        Y	            Y	            Y
                SSH Enable	                        Y	    Y	        Y	            Y	            Y
                HDD time to enter standby	        Y	    Y	        Y	            Y	            Y
                FTP enable	                        Y	    Y	        Y	            Y	            Y
                Anonymous FTP Enable	            Y	    Y	        N	            Y	            Y
                (self.enable_ftp_share_access)
                NFS enable	                        Y	    Y	        N	            Y	            Y
                AFP enable	                        Y	    Y	        N               Y	            Y
                CIFS enable	                        Y	    Y	        N	            Y	            Y
                Itunes server enable	            Y	    Y	        Y	            Y	            Y
                Itunes rescan interval minutes	    Y	    Y	        N	            Y	            Y
                Workgroup name	                    Y	    Y	        Y	            Y	            Y
                Media server enable	                Y	    Y	        Y	            Y	            Y
                Owner name/password	                N	    N	        N	            Y	            Y
                Owner Full name	                    N/A	    N	        N	            Y	            Y
                user name(s)/Password(y)	        N/A	    N	        N	            Y	            Y
                User full name	                    N/A	    N	        N	            Y	            Y
                Sharename(s)	                    N/A	    N	        N	            Y	            Y
                Share remote access enable	        N/A	    N	        N	            N	            N
                Share media server setting          N/A	    N	        N	            N	            N
                (none/all/music only/video only/picture only)
                share public access	                N/A	    N	        N	            Y	            Y
                full access-share username(s)	    N/A	    N	        N	            N	            N
                Read only access - share username   N/A	    N	        N	            N	            N
                USB Share settings	                N/A	    N	        N	            N	            N

    Automation: Partial
                   
    Test Status: PASS except 2 issue
        1. Firmware issue : SKY-2642 'Cloud Access' setting can't be restored.
        2. Behavior not confirmed : Need to wait for developer to confirm if a deleted 'share' can be restored.

"""

from testCaseAPI.src.testclient import TestClient
import time,os,requests

NFS_exclude_list = ['LT4A', 'GLCR']
AFP_exclude_list = ['GLCR']

class configurationBackupRestore(TestClient):

    def run(self):

        try:
            global model_number
            model_number = self.get_model_number()
            self.log.info('model number: {0}'.format(model_number))

            # Check all the value of the settings before saving configuration
            self.check_before_backup()

            # Backup the configuration file
            self.log.info('*** Saving configuration file... ***')
            self.download_config_file('backup.config')

            # Change the configuration to something different than they are now
            self.change_config_after_backup()

            # Restore to previous configuration
            self.config_restore()

            # Check restored configuration
            self.check_after_restore()

        except Exception as e:
            self.log.error('*** ERROR: Configuration backup-restore are not successful: {} ***'.format(repr(e)))
        else:
            self.log.info('*** Configuration backup-restore are performed successfully ***')
        finally:
            self.log.info('Deleting all shares ...')
            self.delete_all_shares()
            time.sleep(7)
            self.log.info('Deleting all users ...')
            self.delete_all_users(use_rest=True)

    def check_before_backup(self):

        self.log.info('*** Check setting values before backup... ***')
        self.delete_all_shares()
        time.sleep(7)
        self.delete_all_users(use_rest=True)
        time.sleep(7)

        global originalDeviceName,originalDescription,originalSSH,original_timezone,originalFTP,originalWorkgroup
        global originalAFP,originalRemoteAccess,originalNFS,originalMediaServing,originalPublicAccess,originalUserShareAccess
        global originalUserFullName,originalUserPassword,originalMediaServer,originalItunesSetting,originalItunesRescan
        global originalAutoInstallEnable,originalAutoInstallDay,originalAutoInstallHour,originalNtpConfig
        global originalAlertSetting,originalAnoFTP

        # Store original device name and description
        originalDeviceName = self.get_xml_tag(self.get_device_description()[1], tag='machine_name')
        originalDescription = self.get_xml_tag(self.get_device_description()[1], tag='machine_desc')
        self.log.info('The device name now is : {}'.format(originalDeviceName))
        self.log.info('The device description now is : {}'.format(originalDescription))

        # Store original users
        self.create_user(username='user1')
        time.sleep(10)
        self.create_user(username='user2',password='12345',fullname='nick yang')
        time.sleep(10)
        userList = self.get_xml_tags(self.get_all_users()[1],tag='username')
        self.log.info('User list : {0}'.format(userList))
        if ('user1' in userList) and ('user2' in userList):
            self.log.info('Users are created successfully')
        else:
            self.log.error('Users are not created successfully')

        # Store user profile
        originalUserPassword = self._get_user_password(user='user2')
        self.log.info('The user password now is : {}'.format(originalUserPassword))
        originalUserFullName = self.get_xml_tag(self.get_user(username='user2')[1],tag='fullname')
        self.log.info('The user password now is : {}'.format(originalUserFullName))

        # Store original share names
        self.create_shares(share_name='test123')
        time.sleep(10)
        self.create_shares(share_name='testShareDelete')
        time.sleep(10)
        shareList = self.get_xml_tags(self.get_all_shares()[1], tag='share_name')
        self.log.info('Shares list : {0}'.format(shareList))
        if ('test123' in shareList) and ('testShareDelete' in shareList):
            self.log.info('Shares are created successfully')
        else:
            self.log.error('Shares are not created successfully')

        # Store user privilege to the share
        self.create_shares(share_name='share1')
        time.sleep(7)
        self.log.info('Toggling the share from public to private ...')
        self.update_share_network_access(share_name='share1', public_access=False)
        time.sleep(8)
        self.create_share_access(share_name='share1',username='user1',access='RW')
        time.sleep(8)
        originalUserShareAccess = self.get_xml_tag(self.get_share_access(share_name='share1',username='user1')[1],tag='access')
        self.log.info('The share access setting now is : {}'.format(originalUserShareAccess))

        # Store Anonymous FTP enable setting
        modify_FTP_result = self.send_cgi('cgi_get_modify_session&sharename=test123','share.cgi')
        time.sleep(7)
        originalAnoFTP = ''.join(self.get_xml_tags(modify_FTP_result, tag='ftp_anonymous'))
        self.log.info('The anonymous FTP enable setting now is : {}'.format(originalAnoFTP))

        # Store 'media serving' value of the share
        originalMediaServing = self.get_xml_tag(self.get_share_network_access(share_name='test123')[1] , tag='media_serving')
        self.log.info('The media serving setting now is : {}'.format(originalMediaServing))

        # Store 'public access' value of the share
        originalPublicAccess = self.get_xml_tag(self.get_share_network_access(share_name='test123')[1] , tag='public_access')
        self.log.info('The public access setting now is : {}'.format(originalPublicAccess))

        # Store original media server setting
        originalMediaServer = self.get_xml_tag(self.get_media_server_configuration()[1],tag='enable_media_server')
        self.log.info('The media server setting now is : {}'.format(originalMediaServer))

        # Store original remote access setting
        originalRemoteAccess = self.get_xml_tag(self.get_device_info()[1], tag='remote_access')
        self.log.info('The remote access setting now is : {}'.format(originalRemoteAccess))

        # Store original FTP setting
        originalFTP = self.get_xml_tag(self.get_FTP_server_configuration()[1], tag='enable_ftp')
        self.log.info('The FTP setting now is : {}'.format(originalFTP))

        # Store original SSH setting
        originalSSH = self.get_xml_tag(self.get_ssh_configuration()[1], tag='enablessh')
        self.log.info('The ssh config now is : {}'.format(originalSSH))

        # Store original firmware update setting
        fw_result = self.get_firmware_update_configuration()[1]
        originalAutoInstallEnable = self.get_xml_tag(fw_result, tag='auto_install')
        originalAutoInstallDay = self.get_xml_tag(fw_result, tag='auto_install_day')
        originalAutoInstallHour = self.get_xml_tag(fw_result, tag='auto_install_hour')
        self.log.info('The firmware update auto install setting now is : {}'.format(originalAutoInstallEnable))
        self.log.info('The firmware update auto install day now is : {}'.format(originalAutoInstallDay))
        self.log.info('The firmware update auto install hour now is : {}'.format(originalAutoInstallHour))

        # Store original NTP config
        ntp_result = self.get_date_time_configuration()[1]
        originalNtpConfig = self.get_xml_tag(ntp_result, tag='ntpservice')
        self.log.info('The NTP setting now is : {}'.format(originalNtpConfig))

        # Store alert email setting
        alert_result = self.get_alert_configuration()[1]
        originalAlertSetting = self.get_xml_tag(alert_result, tag='email_enabled')
        self.log.info('The alert email setting now is : {}'.format(originalAlertSetting))

        # Store original timezone
        original_timezone = self.get_xml_tag(self.get_date_time_configuration()[1], tag='time_zone_name')
        self.log.info('The timezone now is : {}'.format(original_timezone))

        # Store original Itunes server setting
        itunes_result = self.send_cgi('iTunes_Server_Get_XML', 'app_mgr.cgi')
        originalItunesSetting = ''.join(self.get_xml_tags(itunes_result, tag='enable'))
        self.log.info('The Itunes server setting now is : {}'.format(originalItunesSetting))
        time.sleep(5)

        # Store original Itunes rescan interval setting
        originalItunesRescan = ''.join(self.get_xml_tags(itunes_result, tag='rescan_interval'))
        self.log.info('The Itunes rescan interval now is {0} minutes'.format(originalItunesRescan))
        time.sleep(5)

        # Store workgroup name
        result = self.send_cgi('cgi_get_device_info', 'system_mgr.cgi')
        originalWorkgroup = ''.join(self.get_xml_tags(result, tag='workgroup'))
        self.log.info('The WorkGroup name now is : {}'.format(originalWorkgroup))
        time.sleep(6)

        # Store original AFP setting
        if model_number not in AFP_exclude_list:
            afp_result = self.send_cgi('cgi_get_afp_info', 'account_mgr.cgi')
            originalAFP = ''.join(self.get_xml_tags(afp_result, tag='enable'))
            self.log.info('The AFP setting now is : {}'.format(originalAFP))

        # Store original NFS setting
        if model_number not in NFS_exclude_list:
            nfs_result = self.send_cgi('cgi_get_nfs_info', 'account_mgr.cgi')
            originalNFS = ''.join(self.get_xml_tags(nfs_result, tag='enable'))
            self.log.info('The NFS setting now is : {}'.format(originalNFS))

    def change_config_after_backup(self):

        self.log.info('*** Change the configurations after system backup... ***')

        # Change the device name and device description
        self.log.info('*** Change device name and description... ***')
        self.set_device_description(machine_name='test1', machine_desc='testBackupRestore')
        time.sleep(15)

        # Check if device name and description are changed successfully
        changedDeviceName = self.get_xml_tag(self.get_device_description()[1], tag='machine_name')
        changedlDescription = self.get_xml_tag(self.get_device_description()[1], tag='machine_desc')
        self.log.info('*** Verify if device name and description are changed successfully... ***')
        if (changedDeviceName == 'test1') and (changedlDescription == 'testBackupRestore'):
            self.log.info('Machine name and description are changed, Machine Name = {0}, description = {1}'.format(changedDeviceName,changedlDescription))
        else:
            self.log.error('Machine name and description are not changed, Machine Name = {0}, description = {1}'.format(changedDeviceName,changedlDescription))

        # Delete a user
        self.log.info('*** Delete a user... ***')
        self.delete_user(user='user2')
        time.sleep(10)

        # Check if user2 is changed successfully
        userList = self.get_xml_tags(self.get_all_users()[1],tag='username')
        self.log.info('User list : {0}'.format(userList))
        if 'user2' not in userList:
            self.log.info('User2 is deleted successfully')
        else:
            self.log.error('User2 is not deleted successfully')

        # Add/delete the share test
        self.log.info('*** Add a new share... ***')
        self.create_shares(share_name='test456')
        time.sleep(10)
        self.delete_share(share_name='testShareDelete')
        time.sleep(10)

        # Check if the share name is added successfully
        shareList = self.get_xml_tags(self.get_all_shares()[1], tag='share_name')
        self.log.info('Shares list : {0}'.format(shareList))
        if 'test456' in shareList:
            self.log.info('Share is added successfully')
        else:
            self.log.error('Fail to add the new share')
        if 'testShareDelete' not in shareList:
            self.log.info('Share is deleted successfully')
        else:
            self.log.error('Fail to delete the share')

        # Change user privilege to the share
        self.log.info('*** Change User privilege setting... ***')
        self.update_share_access(share_name='share1',username='user1',access='RO')
        time.sleep(8)

        # Check if user privilege is changed
        changeUserShareAccess = self.get_xml_tag(self.get_share_access(share_name='share1',username='user1')[1],tag='access')
        if changeUserShareAccess == 'RO':
            self.log.info('The share access setting now is : {}'.format(changeUserShareAccess))
        else:
            self.log.error('Fail to change the share access setting')

        # Change FTP anonymous setting
        self.log.info('*** Change FTP anonymous setting... ***')
        if originalAnoFTP == 'n':
            self.send_cgi('cgi_modify_share&mtype=4&sharename=test123&ftp=1&ftp_anonymous=w','share.cgi')
        else:
            self.send_cgi('cgi_modify_share&mtype=4&sharename=test123&ftp=1&ftp_anonymous=n','share.cgi')

        # Check if FTP anonymous setting is changed successfully
        self.log.info("*** Verify if FTP anonymous setting is changed ***")
        ftp_anoResult = self.send_cgi('cgi_get_modify_session&sharename=test123','share.cgi')
        time.sleep(7)
        changeAnoFTP = ''.join(self.get_xml_tags(ftp_anoResult, tag='ftp_anonymous'))
        if changeAnoFTP != originalAnoFTP:
            self.log.info('The FTP anonymous setting is changed successfully to : {}'.format(changeAnoFTP))
        else:
            self.log.error('Failed to change the FTP anonymous setting!')

        # Change the media serving setting
        self.log.info('*** Change media serving setting... ***')
        if originalMediaServing == 'any':
            self.log.info('Turn media serving from ON to OFF...')
            self.update_share(share_name='test123',media_serving='none')
        else:
            self.log.info('Turn media serving from OFF to ON...')
            self.update_share(share_name='test123',media_serving='any')

        # Check if media serving setting is changed successfully
        self.log.info("*** Verify if media serving setting is changed ***")
        changeMediaServing = self.get_xml_tag(self.get_share_network_access(share_name='test123')[1] , tag='media_serving')
        if changeMediaServing != originalMediaServing:
            self.log.info('The media serving setting is changed successfully to : {}'.format(changeMediaServing))
        else:
            self.log.error("Failed to change the media serving setting!")

        # Change the public access setting
        self.log.info('*** Change public access setting... ***')
        if originalPublicAccess == 'true':
            self.log.info('Turn public access from ON to OFF...')
            self.update_share(share_name='test123',public_access=False)
        else:
            self.log.info('Turn media serving from OFF to ON...')
            self.update_share(share_name='test123',public_access=True)

        # Check if public access setting is changed successfully
        self.log.info("*** Verify if public access setting is changed ***")
        changePublicAccess = self.get_xml_tag(self.get_share_network_access(share_name='test123')[1] , tag='public_access')
        if changePublicAccess != originalPublicAccess:
            self.log.info('The public access setting is changed successfully to : {}'.format(changePublicAccess))
        else:
            self.log.error("Failed to change the public access setting!")

        # Change the media server setting
        self.log.info('*** Change media server setting... ***')
        if originalMediaServer == 'true':
            self.log.info('Turn media server from ON to OFF...')
            self.set_media_server_configuration(enable_media_server=False)
        else:
            self.log.info('Turn media server from OFF to ON...')
            self.set_media_server_configuration(enable_media_server=True)

        # Check if media server setting is changed successfully
        self.log.info("*** Verify if media server setting is changed ***")
        changeMediaServer = self.get_xml_tag(self.get_media_server_configuration()[1],tag='enable_media_server')
        if changeMediaServer != originalMediaServer:
            self.log.info('The media server setting is changed successfully to : {}'.format(changeMediaServer))
        else:
            self.log.error("Failed to change the media server setting!")

        # Change the remote access setting
        self.log.info('*** Change remote access setting... ***')
        if originalRemoteAccess == 'true':
            self.log.info('Turn remote access from ON to OFF...')
            self.disable_remote_access()
        else:
            self.log.info('Turn remote access from OFF to ON...')
            self.enable_remote_access()

        # Check if remote access setting is changed successfully
        self.log.info("*** Verify if remote access setting is changed ***")
        changeRemoteAccess = self.get_xml_tag(self.get_device_info()[1], tag='remote_access')
        if changeRemoteAccess != originalRemoteAccess:
            self.log.info('The remote access setting is changed successfully to : {}'.format(changeRemoteAccess))
        else:
            self.log.error("Failed to change the remote access setting!")

        # Change the FTP setting
        self.log.info("*** Change FTP config ***")
        if originalFTP == 'true':
            self.set_FTP_server_configuration(enable_ftp=False)
        else:
            self.set_FTP_server_configuration(enable_ftp=True)

        # Check if FTP setting is changed successfully
        self.log.info("*** Verify if FTP config is changed ***")
        changeFTP = self.get_xml_tag(self.get_FTP_server_configuration()[1], tag='enable_ftp')
        if changeFTP != originalFTP:
            self.log.info('The FTP setting is change successfully to : {}'.format(changeFTP))
        else:
            self.log.error("Failed to change the FTP setting!")

        # Change the SSH setting
        self.log.info("*** Change SSH config ***")
        self.set_ssh(enable=True)
        time.sleep(10)

        # Check if SSH setting is changed successfully
        self.log.info("*** Verify if SSH config is changed to true ***")
        changeSSH = self.get_xml_tag(self.get_ssh_configuration()[1], tag='enablessh')
        if changeSSH == 'true':
            self.log.info('The SSH setting is changed successfully to : {}'.format(changeSSH))
        else:
            self.log.error("Failed to change the SSH setting!")

        # Change firmware update setting
        self.log.info("*** Change firmware update setting ***")
        if originalAutoInstallEnable == 'false':
            if originalAutoInstallDay == '4':
                if originalAutoInstallHour == '15':
                    self.set_firmware_update_configuration(auto_install='enable',auto_install_day='3',auto_install_hour='16')
                else:
                    self.set_firmware_update_configuration(auto_install='enable',auto_install_day='3',auto_install_hour='15')
            else:
                if originalAutoInstallHour == '15':
                    self.set_firmware_update_configuration(auto_install='enable',auto_install_day='4',auto_install_hour='16')
                else:
                    self.set_firmware_update_configuration(auto_install='enable',auto_install_day='4',auto_install_hour='15')
        else:
             if originalAutoInstallDay == '4':
                if originalAutoInstallHour == '15':
                    self.set_firmware_update_configuration(auto_install='disable',auto_install_day='3',auto_install_hour='16')
                else:
                    self.set_firmware_update_configuration(auto_install='disable',auto_install_day='3',auto_install_hour='15')
             else:
                if originalAutoInstallHour == '15':
                    self.set_firmware_update_configuration(auto_install='disable',auto_install_day='4',auto_install_hour='16')
                else:
                    self.set_firmware_update_configuration(auto_install='disable',auto_install_day='4',auto_install_hour='15')
        time.sleep(8)

        # Check if firmware update setting is changed successfully
        fw_result = self.get_firmware_update_configuration()[1]
        changedAutoInstallEnable = self.get_xml_tag(fw_result, tag='auto_install')
        changedAutoInstallDay = self.get_xml_tag(fw_result, tag='auto_install_day')
        changedAutoInstallHour = self.get_xml_tag(fw_result, tag='auto_install_hour')
        if changedAutoInstallEnable != originalAutoInstallEnable:
            self.log.info('The firmware update auto install setting is changed successfully to : {}'.format(changedAutoInstallEnable))
        else:
            self.log.error('Failed to change firmware update auto install setting! Setting : {}'.format(changedAutoInstallEnable))
        if changedAutoInstallDay != originalAutoInstallDay:
            self.log.info('The firmware update auto install day is changed successfully to : {}'.format(changedAutoInstallDay))
        else:
            self.log.error('Failed to change firmware update auto day setting! Day : {}'.format(changedAutoInstallDay))
        if changedAutoInstallHour != originalAutoInstallHour:
            self.log.info('The firmware update auto install hour is changed successfully to : {}'.format(changedAutoInstallHour))
        else:
            self.log.error('Failed to change firmware update auto install hour! Hour : {}'.format(changedAutoInstallHour))

        # Change the alert email setting
        self.log.info("*** Change the alert email setting ***")
        if originalAlertSetting == 'false':
            self.log.info('Turn alert email setting from OFF to ON...')
            self.send_cgi('cgi_mail_enable&enable=1','system_mgr.cgi')
        else:
            self.log.info('Turn alert email setting from ON to OFF...')
            self.send_cgi('cgi_mail_enable&enable=0','system_mgr.cgi')

        # Check if alert email setting is changed successfully
        self.log.info("*** Verify if alert email setting is changed successfully ***")
        alert_result = self.get_alert_configuration()[1]
        changedAlertSetting = self.get_xml_tag(alert_result, tag='email_enabled')
        if changedAlertSetting != originalAlertSetting:
            self.log.info('The alert email setting is changed successfully to : {}'.format(changedAlertSetting))
        else:
            self.log.error("Failed to change the alert email setting!")

        # Change the timezone to Alaska
        self.log.info("*** Change Timezone to Alaska ***")
        cmd = 'cgi_timezone&f_timezone=4'
        header = 'system_mgr.cgi?'
        self._cgi._send_cgi_cmd(command=cmd, header=header)
        time.sleep(7)

        # Check if timezone is changed successfully
        self.log.info("*** Verify if Timezone is changed to Alaska ***")
        changed_timezone = self.get_xml_tag(self.get_date_time_configuration()[1], tag='time_zone_name')
        if changed_timezone == 'US/Alaska':
            self.log.info('The timezone is changed successfully to : {}'.format(changed_timezone))
        else:
            self.log.error("Failed to change the timezone to Alaska!")

        # Change the NTP setting
        self.log.info("*** Change NTP config setting ***")
        if originalNtpConfig == 'false':
            self.log.info('Switch NTP config from OFF to ON')
            self.send_cgi('cgi_ntp_time&f_ntp_enable=1', 'system_mgr.cgi')
        else:
            self.log.info('Switch NTP config from ON to OFF')
            self.send_cgi('cgi_ntp_time&f_ntp_enable=0', 'system_mgr.cgi')

        # Check if NTP setting is changed successfully
        self.log.info("*** Verify if NTP setting is changed ***")
        ntp_result = self.get_date_time_configuration()[1]
        changedNtpConfig = self.get_xml_tag(ntp_result, tag='ntpservice')
        if changedNtpConfig != originalNtpConfig:
            self.log.info('NTP setting is changed successfully, status: {0}'.format(changedNtpConfig))
        else:
            self.log.error('NTP setting fails to change, status: {0}'.format(changedNtpConfig))

        # Change the itunes server setting
        self.log.info("*** Change Itunes server setting ***")
        if originalItunesSetting == '0':
            self.log.info('Switch itunes server setting from OFF to ON')
            self.send_cgi('cgi_itunes&f_iTunesServer=1', 'app_mgr.cgi')
        else:
            self.log.info('Switch itunes server setting from ON to OFF')
            self.send_cgi('cgi_itunes&f_iTunesServer=0', 'app_mgr.cgi')

        # Check if Itunes server setting is changed successfully
        self.log.info("*** Verify if Itunes server setting is changed ***")
        itunes_result = self.send_cgi('iTunes_Server_Get_XML', 'app_mgr.cgi')
        changedItunesSetting = ''.join(self.get_xml_tags(itunes_result, tag='enable'))
        if changedItunesSetting != originalItunesSetting:
            self.log.info('Itunes server setting is changed successfully, status: {0}'.format(changedItunesSetting))
        else:
            self.log.error('Itunes server setting fails to change, status: {0}'.format(changedItunesSetting))

        # Change the Itunes rescan interval
        self.log.info("*** Change Itunes rescan interval ***")
        if originalItunesRescan != '900':
            self.log.info('Change Itunes rescan interval minute to 15 minutes')
            self.send_cgi('iTunes_Server_Setting&f_rescan_interval=900', 'app_mgr.cgi')
        else:
            self.log.info('Change Itunes rescan interval minute to 5 minutes')
            self.send_cgi('iTunes_Server_Setting&f_rescan_interval=300', 'app_mgr.cgi')

        # Check if Itunes rescan interval is changed successfully
        self.log.info("*** Verify if Itunes rescan interval is changed ***")
        itunes_result = self.send_cgi('iTunes_Server_Get_XML', 'app_mgr.cgi')
        changedItunesRescan = ''.join(self.get_xml_tags(itunes_result, tag='rescan_interval'))
        if changedItunesRescan != originalItunesRescan:
            self.log.info('Itunes rescan interval is changed successfully to {0} minutes'.format(changedItunesRescan))
        else:
            self.log.error('Itunes rescan interval fails to change, {0} minutes'.format(changedItunesRescan))

        # Change the workgroup setting
        self.send_cgi('cgi_device&workgroup=testWorkGroup', 'system_mgr.cgi')
        time.sleep(7)

        # Check if workgroup is changed successfully
        self.log.info("*** Verify if workgroup name is changed to testWorkGroup ***")
        result = self.send_cgi('cgi_get_device_info', 'system_mgr.cgi')
        updatedWorkgroup = ''.join(self.get_xml_tags(result, tag='workgroup'))
        if updatedWorkgroup == 'testWorkGroup':
            self.log.info('The workgroup name is changed successfully to : {}'.format(updatedWorkgroup))
        else:
            self.log.error('Failed to update workgroup')

        if model_number not in AFP_exclude_list:
            # Change the AFP setting
            self.log.info("*** Change AFP config ***")
            if originalAFP == '0':
                self.log.info('Switch AFP setting from OFF to ON')
                self.send_cgi('cgi_set_afp&afp=1', 'account_mgr.cgi')
            else:
                self.log.info('Switch AFP setting from ON to OFF')
                self.send_cgi('cgi_set_afp&afp=0', 'account_mgr.cgi')

            # Check if AFP setting is changed successfully
            self.log.info("*** Verify if AFP config is changed ***")
            afp_result = self.send_cgi('cgi_get_afp_info', 'account_mgr.cgi')
            changedAFP = ''.join(self.get_xml_tags(afp_result, tag='enable'))
            if changedAFP != originalAFP:
                self.log.info('The AFP setting is change successfully to : {}'.format(changedAFP))
            else:
                self.log.error("Failed to change the AFP setting!")

        # Change the NFS setting
        if model_number not in NFS_exclude_list:
            self.log.info("*** Change NFS config ***")
            if originalNFS == '0':
                self.log.info('Switch NFS setting from OFF to ON')
                self.send_cgi('cgi_nfs_enable&nfs_status=1', 'account_mgr.cgi')
            else:
                self.log.info('Switch NFS setting from ON to OFF')
                self.send_cgi('cgi_nfs_enable&nfs_status=0', 'account_mgr.cgi')

            # Check if NFS setting is changed successfully
            self.log.info("*** Verify if NFS config is changed ***")
            nfs_result = self.send_cgi('cgi_get_nfs_info', 'account_mgr.cgi')
            changedNFS = ''.join(self.get_xml_tags(nfs_result, tag='enable'))
            if changedNFS != originalNFS:
                self.log.info('The NFS setting is change successfully to : {}'.format(changedNFS))
            else:
                self.log.error("Failed to change the NFS setting!")

    def check_after_restore(self):

        self.log.info('*** Verify all the configurations are restored... ***')

        # Check if device name and description are restored successfully
        self.log.info('*** Verify if device name and description are restored successfully... ***')
        restoreDeviceName = self.get_xml_tag(self.get_device_description()[1], tag='machine_name')
        restoreDescription = self.get_xml_tag(self.get_device_description()[1], tag='machine_desc')

        if (originalDeviceName == restoreDeviceName) and (originalDescription == restoreDescription):
            self.log.info('Machine name and description are restored (Test PASSED), Machine Name = {0}, description = {1}'.format(restoreDeviceName,restoreDescription))
        else:
            self.log.error('Machine name and description are not restored (Test FAILED), Machine Name = {0}, description = {1}'.format(restoreDeviceName,restoreDescription))

        # Check if user2 is restored successfully
        self.log.info("*** Verify if deleted user is restored successfully ***")
        userList = self.get_xml_tags(self.get_all_users()[1],tag='username')
        self.log.info('User list : {0}'.format(userList))
        if 'user2' in userList:
            self.log.info('User2 is restored successfully (Test PASSED)')
        else:
            self.log.error('User2 is not restored successfully (Test FAILED)')

        # Check if user2 fullname and password are restored successfully
        self.log.info("*** Verify if user password and fullname are restored successfully ***")
        restoreUserPassword = self._get_user_password(user='user2')
        if restoreUserPassword == originalUserPassword:
            self.log.info('User password is restored (Test PASSED), user password = {0}'.format(restoreUserPassword))
        else:
            self.log.error('User password is not restored (Test FAILED), user password = {0}'.format(restoreUserPassword))

        restoreUserFullName = self.get_xml_tag(self.get_user(username='user2')[1],tag='fullname')
        if restoreUserFullName == originalUserFullName:
            self.log.info('User fullname is restored (Test PASSED), user fullname = {0}'.format(restoreUserFullName))
        else:
            self.log.error('User fullname is not restored (Test FAILED), user fullname = {0}'.format(restoreUserFullName))

        # Check if the share name is restore successfully
        self.log.info("*** Verify if share setting is restored successfully ***")
        shareList = self.get_xml_tags(self.get_all_shares()[1], tag='share_name')
        if 'test456' not in shareList:
            self.log.info('The share setting is restored successfully (Add share test PASSED)')
        else:
            self.log.error('The share setting is not restored successfully (Add share test FAILED)')
        '''
        if 'testShareDelete' in shareList:
            self.log.info('The share setting is restored successfully (Delete share test PASSED)')
        else:
            self.log.error('The share setting is not restored successfully (Delete share test FAILED)')
        '''

        # Check if user privilege is restored successfully
        self.log.info("*** Verify if user privilege setting is restored successfully ***")
        restoreUserShareAccess = self.get_xml_tag(self.get_share_access(share_name='share1',username='user1')[1],tag='access')
        if restoreUserShareAccess == 'RW':
            self.log.info('User privilege setting is restored (Test PASSED), user privilege setting = {0}'.format(restoreUserShareAccess))
        else:
            self.log.error('User privilege setting is not restored (Test FAILED), user privilege setting = {0}'.format(restoreUserShareAccess))

        # Check if FTP anonymous enable setting is restored successfully
        self.log.info("*** Verify if FTP anonymous enable setting is restored successfully ***")
        result = self.send_cgi('cgi_get_modify_session&sharename=test123','share.cgi')
        time.sleep(7)
        restoreAnoFTP = ''.join(self.get_xml_tags(result, tag='ftp_anonymous'))
        if restoreAnoFTP == originalAnoFTP:
            self.log.info('FTP anonymous enable setting is restored (Test PASSED), Setting = {0}'.format(restoreAnoFTP))
        else:
            self.log.error('FTP anonymous enable setting is not restored (Test FAILED), Setting = {0}'.format(restoreAnoFTP))

        # Check 'media serving' value is restored successfully
        self.log.info("*** Verify if media serving value is restored successfully ***")
        restoreMediaServing = self.get_xml_tag(self.get_share_network_access(share_name='test123')[1] , tag='media_serving')
        if restoreMediaServing == originalMediaServing:
            self.log.info('Media serving value is restored (Test PASSED), media serving value = {0}'.format(restoreMediaServing))
        else:
            self.log.error('Media serving value setting is not restored (Test FAILED), media serving value = {0}'.format(restoreMediaServing))

        # Check 'public access' value is restored successfully
        self.log.info("*** Verify if public access value is restored successfully ***")
        restorePublicAccess = self.get_xml_tag(self.get_share_network_access(share_name='test123')[1] , tag='public_access')
        if restorePublicAccess == originalPublicAccess:
            self.log.info('Public access value is restored (Test PASSED), public access value = {0}'.format(restorePublicAccess))
        else:
            self.log.error('Public access value setting is not restored (Test FAILED), public access value = {0}'.format(restorePublicAccess))

        # Check if media server setting is restored successfully
        self.log.info("*** Verify if media server setting is restored successfully ***")
        restoreMediaServer = self.get_xml_tag(self.get_media_server_configuration()[1],tag='enable_media_server')
        if restoreMediaServer == originalMediaServer:
            self.log.info('Media server setting is restored (Test PASSED), media server setting = {0}'.format(restoreMediaServer))
        else:
            self.log.error('Media server setting is not restored (Test FAILED), media server setting = {0}'.format(restoreMediaServer))

        '''
        # Check if remote access setting is restored successfully
        self.log.info("*** Verify if remote access setting is restored successfully ***")
        restoreRemoteAccess = self.get_xml_tag(self.get_device_info()[1], tag='remote_access')
        if restoreRemoteAccess == originalRemoteAccess:
            self.log.info('Remote access setting is restored (Test PASSED), remote access setting = {0}'.format(restoreRemoteAccess))
        else:
            self.log.error('Remote access setting is not restored (Test FAILED), remote access setting = {0}'.format(restoreRemoteAccess))
        '''

        # Check if FTP setting is restored successfully
        self.log.info("*** Verify if FTP setting is restored successfully ***")
        restoreFTP = self.get_xml_tag(self.get_FTP_server_configuration()[1], tag='enable_ftp')
        if restoreFTP == originalFTP:
            self.log.info('FTP setting is restored (Test PASSED), FTP setting = {0}'.format(restoreFTP))
        else:
            self.log.error('FTP setting is not restored (Test FAILED), FTP setting = {0}'.format(restoreFTP))

        # Check if firmware update setting is restored successfully
        self.log.info("*** Verify if firmware update setting is restored successfully ***")
        fw_result = self.get_firmware_update_configuration()[1]
        restoreAutoInstallEnable = self.get_xml_tag(fw_result, tag='auto_install')
        restoreAutoInstallDay = self.get_xml_tag(fw_result, tag='auto_install_day')
        restoreAutoInstallHour = self.get_xml_tag(fw_result, tag='auto_install_hour')
        if restoreAutoInstallEnable == originalAutoInstallEnable:
            self.log.info('Firmware update auto install setting is restored (Test PASSED), setting = {0}'.format(restoreAutoInstallEnable))
        else:
            self.log.error('Firmware update auto install setting is not restored (Test FAILED), setting = {0}'.format(restoreAutoInstallEnable))
        if restoreAutoInstallDay == originalAutoInstallDay:
            self.log.info('Firmware update auto install day is restored (Test PASSED), day = {0}'.format(restoreAutoInstallDay))
        else:
            self.log.error('Firmware update auto install day is not restored (Test FAILED), day = {0}'.format(restoreAutoInstallDay))
        if restoreAutoInstallHour == originalAutoInstallHour:
            self.log.info('Firmware update auto install hour is restored (Test PASSED), hour = {0}'.format(restoreAutoInstallHour))
        else:
            self.log.error('Firmware update auto install hour is not restored (Test FAILED), hour = {0}'.format(restoreAutoInstallHour))

        # Check if SSH setting is restored successfully
        self.log.info("*** Verify if SSH setting is restored successfully ***")
        restoreSSH = self.get_xml_tag(self.get_ssh_configuration()[1], tag='enablessh')
        if restoreSSH == originalSSH:
            self.log.info('SSH setting is restored (Test PASSED), SSH setting = {0}'.format(restoreSSH))
        else:
            self.log.error('SSH setting is not restored (Test FAILED), SSH setting = {0}'.format(restoreSSH))

        # Check if alert email setting is restored successfully
        self.log.info("*** Verify if alert email setting is restored successfully ***")
        alert_result = self.get_alert_configuration()[1]
        restoreAlertSetting = self.get_xml_tag(alert_result, tag='email_enabled')
        if restoreAlertSetting == originalAlertSetting:
            self.log.info('Alert email setting is restored (Test PASSED), Setting = {0}'.format(restoreAlertSetting))
        else:
            self.log.error('Alert email setting is not restored (Test FAILED), Setting = {0}'.format(restoreAlertSetting))

        # Check if timezone is restored successfully
        self.log.info("*** Verify if Timezone is restored successfully ***")
        restoreTimezone = self.get_xml_tag(self.get_date_time_configuration()[1], tag='time_zone_name')
        if restoreTimezone == original_timezone:
            self.log.info('Timezone is restored (Test PASSED), Timezone = {0}'.format(restoreTimezone))
        else:
            self.log.error('Timezone is not restored (Test FAILED), Timezone = {0}'.format(restoreTimezone))

        # Check if NTP setting is restored successfully
        self.log.info("*** Verify if NTP setting is restored successfully ***")
        ntp_result = self.get_date_time_configuration()[1]
        restoreNtpConfig = self.get_xml_tag(ntp_result, tag='ntpservice')
        if restoreNtpConfig == originalNtpConfig:
            self.log.info('NTP setting is restored (Test PASSED), Setting = {0}'.format(restoreNtpConfig))
        else:
            self.log.error('NTP setting is not restored (Test FAILED), Setting = {0}'.format(restoreNtpConfig))

        # Check if Itunes server setting is restored successfully
        self.log.info("*** Verify if Itunes server setting is restored successfully ***")
        itunes_result = self.send_cgi('iTunes_Server_Get_XML', 'app_mgr.cgi')
        restoreItunesSetting = ''.join(self.get_xml_tags(itunes_result, tag='enable'))
        if restoreItunesSetting == originalItunesSetting:
            self.log.info('Itunes server setting is restored (Test PASSED), Itunes server setting = {0}'.format(restoreItunesSetting))
        else:
            self.log.error('Itunes server setting is not restored (Test FAILED), Itunes server setting = {0}'.format(restoreItunesSetting))

        # Check if Itunes rescan interval is restored successfully
        self.log.info("*** Verify if Itunes rescan interval is restored successfully ***")
        itunes_result = self.send_cgi('iTunes_Server_Get_XML', 'app_mgr.cgi')
        restoreItunesRescan = ''.join(self.get_xml_tags(itunes_result, tag='rescan_interval'))
        if restoreItunesRescan == originalItunesRescan:
            self.log.info('Itunes rescan interval is restored (Test PASSED) to {0} minutes'.format(restoreItunesRescan))
        else:
            self.log.error('Itunes rescan interval is not restored (Test FAILED), Value : {0} minutes'.format(restoreItunesRescan))

        # Check if workgroup name is restored successfully
        self.log.info("*** Verify if workgroup name is restored successfully ***")
        result = self.send_cgi('cgi_get_device_info', 'system_mgr.cgi')
        restoreWorkgroup =  ''.join(self.get_xml_tags(result, tag='workgroup'))
        if restoreWorkgroup == originalWorkgroup:
            self.log.info('Workgroup is restored (Test PASSED), workgroup = {0}'.format(restoreWorkgroup))
        else:
            self.log.error('Workgroup is not restored (Test FAILED), workgroup = {0}'.format(restoreWorkgroup))

        # Check if AFP setting is restored successfully
        if model_number not in AFP_exclude_list:
            self.log.info("*** Verify if AFP setting is restored successfully ***")
            afp_result = self.send_cgi('cgi_get_afp_info', 'account_mgr.cgi')
            restoreAFP = ''.join(self.get_xml_tags(afp_result, tag='enable'))
            if restoreAFP == originalAFP:
                self.log.info('AFP setting is restored (Test PASSED), AFP setting = {0}'.format(restoreAFP))
            else:
                self.log.error('AFP setting is not restored (Test FAILED), AFP setting = {0}'.format(restoreAFP))

        # Check if NFS setting is restored successfully
        if model_number not in NFS_exclude_list:
            self.log.info("*** Verify if NFS setting is restored successfully ***")
            nfs_result = self.send_cgi('cgi_get_nfs_info', 'account_mgr.cgi')
            restoreNFS = ''.join(self.get_xml_tags(nfs_result, tag='enable'))
            if restoreNFS == originalNFS:
                self.log.info('NFS setting is restored (Test PASSED), NFS setting = {0}'.format(restoreNFS))
            else:
                self.log.error('NFS setting is not restored (Test FAILED), NFS setting = {0}'.format(restoreNFS))

    def config_restore(self):
        self.log.info('*** Restoring saved configuration... ***')
        config_file = os.path.join(os.getcwd(),'backup.config')

        # Restore to previous configuration
        self.log.info('Restoring saved configuration...')
        self.get_to_page('Settings')
        self.click_wait_and_check('settings_utilities_link', 'settings_utilitiesConfig_button', visible=True)
        self.log.info("Click upload config")
        self.press_key('settings_utilitiesImport_button',config_file)
        self.log.info("System going to reboot...")
        time.sleep(70)

        #self.upload_config_file('backup.config')

        # wait for system to be ready
        count = 0
        while self.is_ready() == False:
            self.log.info("System is not ready yet, wait for a minute...")
            time.sleep(60)
            count += 1
            if count > 10:
                self.log.error('FAILED: Unit not ready after 10 minutes')
                break

        self.close_webUI()
        os.remove(config_file)

    def send_cgi(self, cmd, url2_cgi_head):
        url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        url_2 = head + cmd
        self.log.debug('login cmd: '+url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+url_2)
        with requests.session() as s:
            s.post(url, data=user_data)
            r = s.get(url_2)
            time.sleep(10)
            response = r.text.encode('ascii', 'ignore')
            self.log.debug('response : {0}'.format(response))
            if 'Internal Server Error' in response:
                raise Exception(str(response))

            return response

configurationBackupRestore()