"""
title           :userLogOut.py
description     :To verify if system admin can logout from web UI successfully
author          :yang_ni
date            :2015/03/12
notes           :
"""

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
import time
from global_libraries.testdevice import Fields

class userLogOut(TestClient):

    def run(self):

            if self.get_model_number() in ('GLCR',):
                self.log.info("=== Glacier de-feature does not support user login, skip the test ===")
                return

            try:

                testname = 'User Log out'
                self.start_test(testname)

                '''
                Keep the default username/password of uut Fields
                '''
                Original_username =  self.uut[Fields.web_username]
                Original_password =  self.uut[Fields.web_password]

                '''
                Delete all the users first
                '''
                self.log.info('Deleting all users ...')
                self.delete_all_users()
                time.sleep(3)

                '''
                Create a non admin user
                '''
                self.log.info('Creating a non admin user ...')
                non_admin_user = self.create_user(username='user1', password='password', is_admin=False)

                if non_admin_user == 0:
                    self.log.info('user1 was created successfully')
                else:
                    self.log.error('user1 was not created successfully')

                '''
                Set uut Fields username/password to user1/password
                '''
                self.uut[Fields.web_username] = 'user1'
                self.uut[Fields.web_password] = 'password'

                self.log.info('Login with User1, and logs out')
                self.access_webUI()
                time.sleep(8)
                self.click_element('logout_toolbar')
                time.sleep(5)
                self.wait_until_element_is_visible('home_logout_link',15)
                self.click_element('home_logout_link')
                time.sleep(10)

                # Go back to login page
                self.wait_until_element_is_visible(E.LOGIN_BUTTON,15)
                self.pass_test(testname, 'User1 successfully logs out')

            except Exception as e:
                self.fail_test(testname, 'User1 fails to log out: {}'.format(e))
            finally:
                # Reset back the web username and password, and delete all the users
                self.uut[Fields.web_username] = Original_username
                self.uut[Fields.web_password] = Original_password
                self.delete_all_users()

# This constructs the userLogOut() class, which in turn constructs TestClient() which triggers the userLogOut.run() function
userLogOut()