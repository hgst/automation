"""
Created on April 22, 2015
@Author: tran_jas
    @ Brief: User shall able to query for available update, set auto update schedule
    # Check for latest available firmware on update server
    # Set update schedule
    # Verify firmware is updated correctly after scheduled time
    # Update UUT back to testing firmware version

http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?pId=50&view=details&nTP=117541&pltab=steps
http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?pId=50&view=details&nTP=117543

Note: query with 00.00.002 will return a testing firmware uploaded on Production (table maintained by CAE Chhay Lim)

"""


import time
from testCaseAPI.src.testclient import TestClient


class FirmwareUpdate(TestClient):

    def run(self):
        try:
            self.log.info('Firmware update, set auto update schedule test case')
            testing_firmware = self.uut[self.Fields.actual_fw_version]
            device_ip = self.uut[self.Fields.internal_ip_address]
            product = self.uut[self.Fields.product]
            group = self.uut[self.Fields.group]

            # Fake UUT firmware
            self.execute('auto_fw -s 00.00.002')
            update_firmware_version = self.check_available_update()
            self.log.info('Available update version: {0}'.format(update_firmware_version))

            # Disable NTP and set auto update to 2AM
            self.set_auto_update_schedule()

            self.log.info('Auto update should start in 2 minutes, wait additional 6 minutes')
            time.sleep(480)
            self.wait_for_factory_restore_to_finish()

            # Buffer time after firmware update
            # # Couple times test failed because it reports unit didn't get update, but it does
            time.sleep(90)

            # get firmware version after auto update
            uut_current_firmware = self.get_firmware_version_number()

            self.log.info('uut_current_firmware: {0}'.format(uut_current_firmware))
            self.log.info('update_firmware_version: {0}'.format(update_firmware_version))

            if uut_current_firmware == update_firmware_version:
                self.log.info('Unit had been updated to available firmware')
            else:
                self.log.error('Unit failed to update to available firmware')

            if testing_firmware == uut_current_firmware:
                self.log.info('Unit already has testing firmware version')
            else:
                self.log.info('Need to revert firmware back to testing version')
                loop_count = 0
                successful_update = False
                while loop_count < 3:
                    try:
                        self.firmware_update_nexus(device_ip, testing_firmware, product, group)
                    except Exception, ex:
                        self.log.exception('Failed to do firmware update Nexus' + str(ex))

                    # get firmware version after update
                    uut_current_firmware = self.get_firmware_version_number()
                    if testing_firmware == uut_current_firmware:
                        self.log.info('Unit updated back to testing firmware version')
                        loop_count = 3
                        successful_update = True
                    else:
                        loop_count += 1
                        self.log.info('Not testing firmware version, loop count: {0}'.format(loop_count))

                if not successful_update:
                    self.log.error('Not able to update unit back to testing version')

                # Buffer time before passing unit to next test right after firmware update
                time.sleep(90)

        except Exception, ex:
            self.log.exception('Failed to perform firmware update test...\n' + str(ex))
        finally:
            self.log.info('Enable NTP...')
            self.set_date_time_configuration(datetime='', ntpservice=True, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                             time_zone_name='US/Pacific')
            self.disable_auto_update_schedule()

    def set_auto_update_schedule(self):
        try:
            self.get_to_page('Settings')
            self.click_element('settings_firmware_link')

            # Switch auto update to ON if it's not
            current_status = self.get_text('css=#settings_fwAutoupdate_switch+span .checkbox_container')
            if current_status == 'OFF':
                self.click_element('css=#settings_fwAutoupdate_switch+span .checkbox_container')

            # Disable NTP and set date time to 1:58AM
            self.log.info('Disable NTP and set date')
            self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                             time_zone_name='US/Pacific')
            self.execute('date -s 2015.01.05-01:58:00')

            # Set auto update schedule to 2AM daily
            current_value = self.get_text('id_sch')
            if current_value != 'Daily':
                self.select_item_drop_down_menu(dropdown_menu='id_sch', dropdown_item='link=Daily')

            current_value = self.get_text('id_time')
            if current_value != '2:00':
                self.select_item_drop_down_menu(dropdown_menu='id_time', dropdown_item='link=2:00')

            current_value = self.get_text('id_pm')
            if current_value != 'AM':
                self.select_item_drop_down_menu(dropdown_menu='id_pm', dropdown_item='link=AM')

            if self.is_element_visible("//div[@id='id_tb_sch_save']/button"):
                self.click_element("//div[@id='id_tb_sch_save']/button")

        except Exception, ex:
            self.log.exception('Failed to set auto update schedule\n' + str(ex))

    def disable_auto_update_schedule(self):
        try:
            self.get_to_page('Settings')
            self.click_element('settings_firmware_link')

            # Switch auto update to OFF if it's not
            current_status = self.get_text('css=#settings_fwAutoupdate_switch+span .checkbox_container')
            if current_status == 'ON':
                self.click_element('css=#settings_fwAutoupdate_switch+span .checkbox_container')

        except Exception, ex:
            self.log.exception('Failed to disable auto update schedule\n' + str(ex))

    def check_available_update(self):
        try:
            self.get_to_page('Settings')
            self.click_element('settings_firmware_link')
            self.click_element('settings_fwCheckUpdate_button')
            # self.click_element(Elm.SETTINGS_FIRMWARE_UPDATE_CHECK)
            available_version = self.get_text('id_auto_new_firm_version')
            self.click_element('settings_fwUpdateClose_button')
            return available_version

        except Exception, ex:
            self.log.exception('Failed to check available firmware update\n' + str(ex))

FirmwareUpdate()