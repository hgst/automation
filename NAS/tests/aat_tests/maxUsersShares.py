#
#Objective: To validate that the device is capable of supporting a large number
#           of users/shares/groups
# wiki URL: http://wiki.wdc.com/wiki/Max Users Shares
#
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
import time


maxNumberOfShares = 128
maxNumberOfGroups = 64
fourBayNAS = ['LT4A', 'BNEZ', 'BWZE']

class maxUsersShares(TestClient):
    
    def run(self): 
        self.systemRestore()
        self.populate()
        self.validation()   
        self.systemRestore()
        
    def systemRestore(self):
        self.log.info('System Restore')
        
        numberDrives = self.uut[self.Fields.number_of_drives]
        
        global modelNumber
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')
        
        self.click_element(E.SETTINGS_UTILITY_RESTORE_TO_DEFAULT)
        self.click_element("//li[@id='settings_utilitiesReset2_select']/a/span")
        self.click_element(E.SETTINGS_UTILITY_RESTORE)
        self.wait_until_element_is_visible('popup_apply_button', 5)
        self.click_button('popup_apply_button') 
        
        self.wait_until_element_is_not_visible('RestoreFullDiag',1800)
            
        if modelNumber in fourBayNAS:
            time.sleep(240)
        else:
            time.sleep(120)
        
        # wait for system to be ready
        count = 0
        while self.is_ready() == False:
            time.sleep(60)
            count += 1
            if count > 10:
                self.log.error('FAILED: Unit not ready after 10 minutes')
                break             
        self.set_ssh()
        self.close_webUI()
        
    def populate(self): 
        
        #Create 128 shares. 
        self.log.info('Creating maximun number of shares. Please Standby...')
        self.create_shares(share_name='share', number_of_shares=128) 
        
        self.get_to_page('Users')            
        #create 511 users
        self.log.info('Creating maximun number of users. Please Standby...')
        self.create_multiple_users(start_prefix=1, numberOfusers=511)
          
        #Create max number of groups (64)
        self.log.info('Creating maximun number of groups. Please Standby...')
        self.create_groups(group_name='group', number_of_groups=64)
               
    def validation(self):
        
        #Validate max number of groups 
        last_group = self.find_element_in_sublist('group64', 'group')
        self.log.info('Maximun Number of Groups (64) validation')
        if last_group:
            self.log.info('PASS')
        else:
            self.log.error('FAIL') 
            
        #Validate max number of shares  
        response = self.get_all_shares()
        total_shares = len(self.get_xml_tags(response[1], 'share_name'))
        self.log.info ('Maximun Number of Shares ({0}) validation'.format(total_shares))
        if total_shares >= maxNumberOfShares:
            self.log.info('PASS')
        else:
            self.log.error('FAIL') 
            
        #Change share1, share60, and share128 to private
        self.update_share('share1', public_access=False) 
        self.update_share('share60', public_access=False)
        self.update_share('share128', public_access=False) 
        
            
        #Change group1's quota to 100MB for each volume and allow it read/write access to share1
        self.set_quota_all_volumes(name='group1', amount=100)
        self.assign_group_to_share(group_name='group1', share_number=1, access_type='RW')
        
        #Change group30's quota to 300MB for each volume and allow it read/write access to share60
        self.set_quota_all_volumes(name='group30', amount=300)
        self.assign_group_to_share(group_name='group30', share_number=60, access_type='RW')
        
        #Change group64's quota to 500MB for each volume and allow it read/write access to share128  
        self.set_quota_all_volumes(name='group64', amount=500)
        self.assign_group_to_share(group_name='group64', share_number=128, access_type='RW')
        
        #Assign  user1 to group1
        self.update_group(group='group1', memberusers='#user1#')
        self.create_share_access(share_name='share1', username='user1', access='RW')
        
        #Assign  user300 to group30
        self.update_group(group='group30', memberusers='#user300#')
        self.create_share_access(share_name='share60', username='user300', access='RW')
        
        #Assign  user511 to group64
        self.update_group(group='group64', memberusers='#user511#')
        self.create_share_access(share_name='share128', username='user511', access='RW')
        
        #verify user1 has RW access to share1
        user1_access = self.get_share_access(share_name='share1', username='user1')
        if (self.get_xml_tag(xml_content=user1_access[1], tag='access')) == 'RW':
            self.log.info('User1 has RW access to share1 validation, PASS')
        else:
            self.log.error('User1 has RW access to share1 validation, FAIL')
        
        #verify user300 has RW access to share60
        user300_access = self.get_share_access(share_name='share60', username='user300')
        if (self.get_xml_tag(xml_content=user300_access[1], tag='access')) == 'RW':
            self.log.info('User300 has RW access to share60 validation, PASS')
        else:
            self.log.error('User1 has RW access to share60 validation, FAIL')
                
        #verify user511 has RW access to share128
        user511_access = self.get_share_access(share_name='share128', username='user511')
        if (self.get_xml_tag(xml_content=user511_access[1], tag='access')) == 'RW':
            self.log.info('User511 has RW access to share128 validation, PASS')
        else:
            self.log.error('User511 has RW access to share128 validation, FAIL')
    
maxUsersShares()          