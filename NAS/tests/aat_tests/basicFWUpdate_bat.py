""" 
    Basic Firmware update - BAT

    @Author: lin_ri
    Test Procedure: @silk
    
    N, N-1 needs to be the same SILK_VERSION such as 2.00 or 1.06
    1) FW upgrade to latest version  (N-1 to N)  e.g. 2.00.100 to 2.00.101
    2) FW upgrade to same version    (N to N)
    3) FW downgrade to older version (N to N-1)    
    
    Automation: Partial

    Note:
    products_info dictionary has model number, product name and firmware version
    Add new model in product_info dictionary automatically

    Change History:
    2015-08-20: Add product_info dictionary update to support new model without updating the products_info anymore
    2015-04-15: Remove &p=bin from the end of SEARCH_BASE_URL query string
                Replace the naming rule of firmware filename
                (artifact-version.bin to artifact_version.bin) --> (My_Cloud_LT4A-2.00.190.bin to My_Cloud_LT4A_2.00.190.bin)
                Replace urllib with urllib2
""" 
from testCaseAPI.src.testclient import TestClient
from xml.etree.ElementTree import parse
from testCaseAPI.src.configure import Device
import time
import urllib2
import os
import re

REPO_LOCATION = 'http://repo.wdc.com'
ARTIFACT_BASE_URL = '{0}/service/local/repositories/projects/content/'.format(REPO_LOCATION)
SEARCH_BASE_URL = '{0}/service/local/lucene/search?repositoryID=projects'.format(REPO_LOCATION)
REST = 1  # 0: Use Selenium API to flash firmware, 1: Use RESTful API to flash firmware

# Define the latest FW versions (Nexus)
# This dictionary will be updated with new model which was not added here in the beginning of the script
products_info = {
        'LT4A': ['Lightning', '2_10'],
        'BNEZ': ['Sprite', '2_10'],
        'BWZE': ['Yellowstone', '2_10'],
        'BWAZ': ['Yosemite', '2_10'],
        'BBAZ': ['Aurora', '2_10'],
        'KC2A': ['KingsCanyon', '2_10'],
        'BZVM': ['Zion', '2_10'],
        'GLCR': ['Glacier', '2_10'],
        'BWVZ': ['GrandTeton', '2_10'],
}

class basicFWUpdate(TestClient):
    def run(self):
        self.log.info('######################## BAT basic firmware update TESTS START ##############################')
        try:
            # Get DUT's firmware version
            DUT_FW = self.get_firmware_version_number()
            DUT_VERSION, DUT_BUILD = DUT_FW.rsplit('.', 1)
            DUT_MODELNAME = self.get_model_number()
            
            # Add New Model Support
            if DUT_MODELNAME not in products_info.keys():
                product_name = self.uut[self.Fields.product].replace(' ', '')  # Kings Canyon -> KingsCanyon
                products_info[DUT_MODELNAME] = [product_name, DUT_VERSION.replace('.', '_')]
            else:
                # Update products_info FW version from 2_10 to current series
                products_info[DUT_MODELNAME][1] = DUT_VERSION.replace('.', '_')
            
            self.log.info("PRODUCT: " + products_info[DUT_MODELNAME][0] + ", MODEL: " + DUT_MODELNAME + ", FW: " + DUT_FW)

            todo_fw_list = []
            self.log.info("Going to get the latest firmware and current firmware information ...")
            todo_fw_list.append(self.get_fw_todo_list(dutFw=DUT_FW, dutVersion=DUT_VERSION, dutModel=DUT_MODELNAME))
            cur_fw_info = self.current_firmware_info(dutFW=DUT_FW, dutVersion=DUT_VERSION, dutModel=DUT_MODELNAME)
            if cur_fw_info not in todo_fw_list:
                todo_fw_list.append(cur_fw_info)

            if REST == 0:
                self.log.info("====== Use Web UI to flash firmware ======")
                tmpfiles = self.download_fw_files(fwLink=todo_fw_list)

            # Gather the latest firmware information to kick off flash test
            same_to_same = 0
            if len(todo_fw_list) <= 0:
                self.log.error("No firmware files is found on Nexus, raise System Exit")
                raise SystemExit
            elif len(todo_fw_list) == 1:
                UP_URL, UP_FILE, UP_BUILD = todo_fw_list[0]
                same_to_same = 1
                self.log.info("Only One firmare file is found on Nexus")
                self.log.info("===== Same to Same flash - level N: {} =====".format(UP_FILE, UP_FILE))
            else:
                UP_URL, UP_FILE, UP_BUILD = todo_fw_list[0]
                DOWN_URL, DOWN_FILE, DOWN_BUILD = todo_fw_list[1]
                self.log.info("===== Full flash - level N: {}, level N-1: {} =====".format(UP_FILE, DOWN_FILE))

            # Get 2 firmware files from Nexus
            CUR_FW = DUT_FW
            if same_to_same == 0:

                if DUT_FW != UP_BUILD:
                    # Firmware Flash from old to the latest
                    CUR_FW = self.firmware_flash(direction=2, dutFW=DUT_FW, targetURL=UP_URL, targetFile=UP_FILE, targetBUILD=UP_BUILD, rest=REST)

                # Firmware Flash from Same to Same
                CUR_FW = self.firmware_flash(direction=1, dutFW=CUR_FW, targetURL=UP_URL, targetFile=UP_FILE, targetBUILD=UP_BUILD, rest=REST)

                # Firmware Flash from the latest to old
                CUR_FW = self.firmware_flash(direction=0, dutFW=CUR_FW, targetURL=DOWN_URL, targetFile=DOWN_FILE, targetBUILD=DOWN_BUILD, rest=REST)

            # Only get one firmware file from Nexus
            else:
                if DUT_FW != UP_BUILD:
                    # Firmware Flash from old to the latest
                    CUR_FW = self.firmware_flash(direction=2, dutFW=DUT_FW, targetURL=UP_URL, targetFile=UP_FILE, targetBUILD=UP_BUILD, rest=REST)

                # Firmware Flash from Same to Same
                CUR_FW = self.firmware_flash(direction=1, dutFW=CUR_FW, targetURL=UP_URL, targetFile=UP_FILE, targetBUILD=UP_BUILD, rest=REST)

        except Exception as e:
            self.log.error("FAILED: BAT - Basic FW Update Test Failed!!, exception: {}".format(repr(e)))
        finally:
            # Restore the firmware back to original level
            CUR_FW = self.get_firmware_version_number()
            self.log.info('******** FINALLY: Firmware Clean Up ********')
            if CUR_FW != DUT_FW:
                self.log.info("DUT FW is not at original level: {}".format(DUT_FW))
                self.log.info("CleanUp: Flash fw={}".format(DUT_FW))
                try:
                    self.firmware_flash(direction=0, dutFW=CUR_FW, targetURL=DOWN_URL, targetFile=DOWN_FILE, targetBUILD=DOWN_BUILD, rest=REST)
                except Exception:
                    pass
                finally:
                    CUR_FW = self.get_firmware_version_number()
                    if CUR_FW != DUT_FW:
                        self.log.error("ERROR: DUT FW is not at the original level: {}".format(DUT_FW))
                        # self.update_inventory()
            else:
                self.log.info("Firmware is already at the original level: {}".format(CUR_FW))

            if REST == 0:
                for f in tmpfiles:
                    if os.path.exists(f):
                        self.log.info("Cleaning temporary files: {}".format(f))
                        os.remove(f)
            self.log.info('######################## BAT basic firmware update TESTS END ##############################')

    def current_firmware_info(self, dutFW='', dutVersion='', dutModel=''):
        """
            Restore the DUT firmware back to original level

            @dutFW: Original DUT Firmware (ex: 2.00.031)
            @dutVersion: Original DUT FW version (ex: 2.00)
            @dutModel: DUT model name (ex: LT4A)
        """
        cur_project_version = products_info[dutModel][0] + '-' + dutVersion.replace('.', '_')
        self.log.info("[Current FW] Project: {}".format(cur_project_version))
        fw_list = self.build_fw_list(cur_project_version)
        download_list = []
        for g, a, v in fw_list:
            if v == dutFW:
                link, filename, p_link = self.get_artifact_url(group=g, artifact=a, version=v)
                download_list.append((link, filename, v))
                break
        if download_list == []: # Firmware file is not found on Nexus
            self.log.info("Warning: Unable to locate the firmware {} on Nexus".format(dutFW))
            return 0
        return download_list[0]

    def firmware_flash(self, direction=1, dutFW="", targetURL="", targetFile="", targetBUILD="", rest=0):
        """
            Flash DUT's firmware onto the target level
            
            @direction: 0 means downgrade
                        1 means same to same
                        2 means upgrade
            @rest: 0 means Use WebUI to do the firmware flash
                   1 means Use RESTful API to do the firmware flash
        """
        # Firmware Flash from old to the latest
        if direction == 0:
            msg = 'Downgrade'
        elif direction == 1:
            msg = 'Same to Same'
        else:
            msg = 'Upgrade'
        self.log.info("***** FW Flash: {}, from {} to {} *****".format(msg, dutFW, targetBUILD))
        if rest == 1:
            self.log.info("RESTful API uses HTTP to flash firmware...")
            cur_level = self.rest_firmware_flash(fwfile=targetURL, version=targetBUILD)
        else:
            cur_level = self.ui_firmware_flash(fwfile=targetFile)
        if cur_level == targetBUILD:
            self.log.info("***** PASS: FW {} test *****".format(msg))
            return cur_level
        else:
            self.log.info("DEBUG: Current level = {}, Target Level = {}".format(cur_level, targetBUILD))
            self.log.error("***** FAILED: FW {} test *****".format(msg))

    def download_fw_files(self, fwLink):
        """
            Iterate the url list to download files and save them in local directory
            
            @fwLink: [(download url, firmware filename)]
        """
        tempfiles = []
        try:
            for l, f, b in fwLink:
                tempfiles.append(f)
                if os.path.exists(f):
                    os.remove(f)
                self.download_url_file(l, f)
            return tempfiles
        except Exception as e:
            self.log.error("FAILED: download_fw_files failed, exception: {}".format(repr(e)))
    
    def get_fw_todo_list(self, dutFw="", dutVersion="", dutModel=""):
        """
            Get the latest and previous build
            
            @dutFw: DUT firmware information (Ex: '2.00.026')
            @dutVersion: DUT Firmware version (Ex: '2.00')
            @dutModel: DUT Model (Ex: 'LT4A')
        """
        if not dutFw and not dutVersion and not dutModel:
            self.log.info("ERROR: get_fw_todo_list() requires 3 arguments")
            self.log.error("System EXIT due to insufficient arguments")
            raise SystemExit
        
        # Get the project version string      
        latest_project_version = products_info[dutModel][0] + '-' + products_info[dutModel][1]
        cur_project_version = products_info[dutModel][0] + '-' + dutVersion.replace('.', '_')
        
        # Reference link: http://repo.wdc.com/content/repositories/projects/
        #if products_info[dutModel][1] == '2_00':
        #    latest_project_version = products_info[dutModel][0] + '-iRadar' + '-' + products_info[dutModel][1]
        #    self.log.info("Adjust 2.00 project version query string...")
        
        self.log.info("Latest Project Version: {}".format(latest_project_version))
        self.log.info("Current Project Version: {}".format(cur_project_version))        
        
        latest_silk_version = products_info[dutModel][1].split('_')[0]
        cur_silk_version = dutVersion.split('.')[0]
        
        todo_fw_list = []
        try:
            # Check which firmware version we need to do the flash test
            if cur_silk_version == latest_silk_version:
                self.log.info("DUT is the same as the latest level series, Version: (DUT, the latest) = ({}, {})".format(cur_silk_version, latest_silk_version))
                latest_fw_list = self.build_fw_list(latest_project_version)
            else:
                self.log.info("DUT is different from the the latest, Version: (DUT, the latest) = ({}, {})".format(cur_silk_version, latest_silk_version))
                latest_fw_list = self.build_fw_list(cur_project_version)
            
            if latest_fw_list == []:
                raise Exception("ERROR: Unable to get any firmware file information")

            todo_fw_list = self.filter_promoted_fw_list(fw_list=latest_fw_list)[:1]
        except Exception as e:
            self.log.error("FAILED: get_fw_todo_list() failed, exception: {}".format(repr(e)))
            self.log.warning("System Exit...")
            raise SystemExit
        
        return todo_fw_list[0]

    def filter_promoted_fw_list(self, fw_list=None):
        """
            Construct Firmware list with promoted set to True
        :param fw_list: all firmware list
        :return: promoted firmware list
        """
        if fw_list is None:
            self.log.error("ERROR: Requires non-empty firmware list")
            return

        promoted_fw_list = []
        # Build the download url and filename
        for g, a, v in fw_list[:10]:
            link, filename, p_link = self.get_artifact_url(group=g, artifact=a, version=v)
            promoted_flag = urllib2.urlopen(p_link).read().strip('\n')
            if promoted_flag == 'true':
                promoted_fw_list.append((link, filename, v))
        return promoted_fw_list

    def firmware_flash_wait2(self):
        """
            To check system status and wait for system back from reboot
        """
        fwStatusChkCmd = "xmldbc -i -g /runtime/firmware_percentage"
        count = 0
        while count < 60:  # 5 * 60 = 5 min
            try:
                fwStsResult = self.run_on_device(fwStatusChkCmd)
            except Exception:
                pass
            else:
                if fwStsResult:
                    self.log.info("[FW Percentage]: {}".format(fwStsResult))
                    percent = int(fwStsResult.strip())
                    if percent == 100:
                        self.log.info("=== FW flash completed! System is going to reboot NOW!!! ===")
                        break
                count += 1
                time.sleep(5)
        if count == 60:
            self.log.info(">>> Reach the maximum wait for percentage check <<<<")
        self.log.info("Waiting 5 minutes for system reboot...")
        time.sleep(300)  # 5min
        while True:
            try:
                if self.is_ready():
                    break
            except Exception as e:  # If RESTful API exception arises, continue to retry
                self.log.info("Retry: RESTful API is_ready() failure, exception: {}".format(repr(e)))
                continue
        return

    def firmware_flash_wait(self):
        """
            To check system status and wait for system back from reboot
        """
        fwUploadChkCmd = 'ps aux | grep -v grep | grep upload_firmware'
        fwStatusChkCmd = "xmldbc -i -g /runtime/firmware_percentage"
        try:
            fwChkResult = self.run_on_device(fwUploadChkCmd)
        except Exception:
            pass
        else:
            if fwChkResult:
                self.log.info("[Flashing]: {}".format(fwChkResult))
            else:
                self.log.info("[Did not find the upload_firmware process]")
        self.log.info("Waiting 5 minutes for firmware flash...")
        time.sleep(300)  # 5min
        count = 0
        while True:
            time.sleep(60)  # 1 min
            count += 1
            self.log.info("{} minute wait".format(count+5))
            if count >= 10:
                self.log.info("Reach the maximum 15 minutes wait for system ready")
                break
            try:
                fwChkResult = self.run_on_device(fwUploadChkCmd)
            except Exception as e:
                self.log.info("Retry: Unable to check upload status, exception: {}".format(repr(e)))
                continue
            else:
                if fwChkResult:
                    self.log.info("Firmware is still uploading...")
                    self.log.info("----------------------------")
                    self.log.info("{}".format(fwChkResult))
                    self.log.info("----------------------------")
                    continue
            time.sleep(2)
            try:
                fwChkResult = self.run_on_device(fwStatusChkCmd)
            except Exception as e:
                self.log.info("Retry: Unable to check flash status, exception: {}".format(repr(e)))
                continue
            else:
                if fwChkResult:
                    self.log.info("Firmware is still flashing...")
                    self.log.info("----------------------------")
                    self.log.info("{}".format(fwChkResult))
                    self.log.info("----------------------------")
                    continue
            time.sleep(2)
            try:
                if self.is_ready():
                    break
            except Exception as e:  # If RESTful API exception arises, continue to retry
                self.log.info("Retry: RESTful API is_ready() failure, exception: {}".format(repr(e)))
                continue
        return
    
    def rest_firmware_flash(self, fwfile="", version=""):
        """
            Use RESTful API to do the firmware flash
            2015-2-12: RESTful API is not supported
            
            @fwfile: Firmware Filename (Ex: My_Cloud_LT4A-2.00.026.bin)
            @version: Firmware build version (Ex: 2.00.026)
        """
        self.log.info("Firmware flash by RESTful API")
        try:
            if not fwfile:
                self.log.error("ERROR: firmware_flash() requires 1 argument")
            fw_info = None
            resultCode = self.firmware_update(fwfile)
            self.log.info("Firmware Flash REST API result code: {}".format(resultCode))
            time.sleep(5)
            # Wait for firmware flash
            # self.firmware_flash_wait()
            self.firmware_flash_wait2()
            fw_info = self.get_firmware_version_number()
            self.log.info("Firmware level after flash is: {}".format(fw_info))
            if fw_info == version:
                self.log.info("PASS: Succeed to complete firmware flash")
            else:
                self.log.error("ERROR: Firmware flash failed, DUT FW currrent: {}, target: {}".format(fw_info, version))
            return fw_info        
        except Exception as e:
            self.log.error('FAILED: Unable to complete firmware flash!! Exception: {}'.format(e))
    
    def ui_firmware_flash(self, fwfile=""):
        """
            Browse to Firmware update page to flash the firmware
            
            @fwfile: Firmware Filename (Ex: My_Cloud_LT4A-2.00.026.bin)
        """     
        self.log.info("Firmware flash by Selenium API")
        try:
            if not fwfile:
                self.log.error("ERROR: firmware_flash() requires 1 argument")
            fw_info = None
            self.accept_eula(use_UI=False)
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.log.info("Click Firmware Update")
            self.click_wait_and_check('settings_firmware_link', 'css=#firmware.LightningSubMenuOn', visible=True)
            self.log.info("Manual firmware update")
            self.select_frame("upload_frame")
            self.log.info("Sending file: {}".format(os.path.abspath(fwfile)))
            #self.click_element("id_file")
            self.press_key("file", os.path.abspath(fwfile))
            self.log.info("Click Update from file")
            self.unselect_frame()
            self.wait_until_element_is_visible("popup_apply_button")
            self.click_wait_and_check("popup_apply_button", visible=False)
            time.sleep(5)
            # Wait for firmware flash
            # self.firmware_flash_wait()
            self.firmware_flash_wait2()
            fw_info = self.get_firmware_version_number()
            self.log.info("Firmware level after flash is: {}".format(fw_info))
            time.sleep(2)
            self.close_webUI()
            return fw_info
        except Exception as e:
            self.log.error('FAILED: Unable to complete firmware flash!! Exception: {}'.format(e))       

    def download_url_file(self, fw_url="", fw_file=""):
        """
            Open the url to download the file and save it locally
            
            @fw_url: URL where firmware file resides
            @fw_file: firmware filename
        """
        if not fw_url or not fw_file:
            self.log.error("ERROR: download_url_file() needs 2 arguments")
        retry = 2
        while retry > 0:
            try:
                self.log.info("Downloading the firmware file: {} ...".format(fw_file))
                # urllib.urlretrieve(fw_url, fw_file) # Replace by urllib2
                fw = urllib2.urlopen(fw_url)
                with open(fw_file, "wb") as localFile:
                    localFile.write(fw.read())
                self.log.info("{} is saved at {}".format(fw_file, os.path.abspath(fw_file)))
            except urllib2.HTTPError as e:
                retry -= 1
                self.log.info("WARN: HTTP ERROR: {}, FILE NOT FOUND, remaining {} retry".format(e.code, fw_url, retry))
                if retry == 0:
                    self.log.error("HTTP ERROR: {}, FILE NOT FOUND at {}!!".format(e.code, fw_url))
            except urllib2.URLError as e:
                self.log.error("URL ERROR: {}, FW Link: {}!!".format(e.reason, fw_url))
            except Exception as e:
                self.log.error("FAILED: Unable to download file {}, exception: {}\n link: {}".format(fw_file, repr(e), fw_url))
            else:
                break

    def build_fw_list(self, groupversion=""):
        """
            @groupname: Sprite-1_06
            return [ (group, id, version) ]
        """
        global SEARCH_BASE_URL
        fw_list = []
        
        if not groupversion:
            self.log.error('groupname is not specified.')
        
        URL = SEARCH_BASE_URL + "&g={}".format(groupversion)
        
        self.log.info("Building Firmware list...")
        
        try:
            u = urllib2.urlopen(URL)
            doc = parse(u)
            for artifact in doc.findall('.//data//artifact'):
                r_group = artifact.findtext('groupId')
                r_id = artifact.findtext('artifactId')
                r_version = artifact.findtext('version')
                fw_list.append((r_group, r_id, r_version))
    #             self.log.info('{} / {} / {}'.format(r_group, r_id, r_version))
            return fw_list
        except Exception as e:
            self.log.error("FAILED: build_fw_list() failed, exception: {}".format(repr(e)))

    def get_artifact_url(self, group="", artifact="", version=""):
        """
            @group: Sprite-1_06
            @artifact: My_Cloud_LT4A
            @version: 1.05.30
            
            return (firmware url, firmware filename, promoted text url)
        """
        global ARTIFACT_BASE_URL
        repo_link = '{0}/{1}/{2}/{1}_{2}.bin'.format(group, artifact, version)
        promoted_link = '{0}/{1}/{2}/{1}-{2}-promoted.txt'.format(group, artifact, version)
        FW_FILE = '{0}_{1}.bin'.format(artifact, version)
        repo_link = ARTIFACT_BASE_URL + repo_link
        promoted_link = ARTIFACT_BASE_URL + promoted_link
#         self.log.info("ARTIFACT URL: {}".format(repo_query))
#         self.log.info("FW FILE: {}".format(FW_FILE))
        return repo_link, FW_FILE, promoted_link

    def update_inventory(self):
        """
            Update information
        """
        device = Device(do_configure=False)
        if 'sctm_build' not in os.environ:
            self.log.info("=== Local run: no need to update inventory ===")
            return
        else:
            self.log.info("=== Silk run: going to update inventory ===")
        try:
            job_fw = "{0}.{1}".format(os.environ.get('sctm_version'), os.environ.get('sctm_build'))
            checkout_owner = "{0}-{1}".format(os.environ.get('sctm_execdef_name'), job_fw)
            platform = self.uut[self.Fields.product]
        except Exception as e:
            self.log.error("ERROR: fail to get checkout_owner/platform, exception: {0}".format(repr(e)))
            raise
        self.log.info("Checkout owner: {0}".format(checkout_owner))
        self.log.info("Target product: {0}".format(platform))
        uut = device.device_api.getDeviceByJob(jenkinsJob=checkout_owner)
        if uut:
            self.log.info("Found device, ip: {0}".format(uut['internalIPAddress']))
        else:
            self.log.error("Device: {} , not found, skip the information update".format(platform))
            return
        current_firmware = self.get_firmware_version_number()
        self.log.info("Going to update uut firmware information...")
        if not device.device_api.update(uut['id'], firmware=current_firmware):
            self.log.info("=== ERROR: fail to update device information ===")
        else:
            self.log.info("=== Succeed to update the uut fw: {0} ===".format(current_firmware))

basicFWUpdate()
