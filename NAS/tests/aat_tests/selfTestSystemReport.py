"""
title           :selfTestSystemReport.py
description     :To verify that the following procedures finish successfully:
                                         1. Quick Test
                                         2. Full Test
                                         3. Automated Support
                                         4. Local System Report
author          :yang_ni
date            :2015/02/02
notes           :http://wiki.wdc.com/wiki/Self-test_and_System_Report
"""

from testCaseAPI.src.testclient import TestClient
import time,os,zipfile
from selenium import webdriver
from global_libraries.testdevice import Fields


class selfTestSystemReport(TestClient):
    def run(self):
        try:
            self.log.info('########################Test START ##############################')         
            
            model_number = self.get_model_number()
            raid_mode = self.get_raid_mode()
            
            bay2_model =  ['BWAZ', 'BBAZ', 'KC2A', 'BZVM', 'BWVZ']
            bay4_model =  ['LT4A', 'BNEZ', 'BWZE']
            
            # 1. Create a Raid 1 or Raid 5 depending on NAS device (If 2 Bay : create Raid1, If 4 Bay: create Raid5):
            if (model_number in bay2_model) and (raid_mode != "RAID1"):
                self.log.info('Raid mode is not in Raid1, configuring to Raid1 now')
                self.configure_raid(raidtype='RAID1')
                
            elif (model_number in bay4_model) and (raid_mode != "RAID5"):
                self.log.info('Raid mode is not in Raid5, configuring to Raid5 now')
                self.configure_raid(raidtype='RAID5',volume_size=200)
                
            # 2. Support -> Create and save System Report
            
            # Lightning
            if model_number == 'LT4A':
                self.systemReport(zipname = "MyCloudEX4")
            # Sprite
            elif model_number == 'BNEZ':
                self.systemReport(zipname = "MyCloudDL4100")
            # Aurora
            elif model_number == 'BBAZ':
                self.systemReport(zipname = "MyCloudDL2100")
            # YellowStone
            elif model_number == 'BWZE':
                self.systemReport(zipname = "MyCloudEX4100")
            # YoseMite
            elif model_number == 'BWAZ':
                self.systemReport(zipname = "MyCloudEX2100")
            # Kings Canyon
            elif model_number == 'KC2A':
                self.systemReport(zipname = "MyCloudEX2")
            # Glacier
            elif model_number == 'GLCR':
                self.systemReport(zipname = "MyCloudGen2")
            # Zion
            elif model_number == 'BZVM':
                self.systemReport(zipname = "MyCloudEX4100")

            # Grand Teton
            elif model_number == 'BWVZ':
                self.systemReport(zipname = "MyCloudMirrorG2")

            # 3. Settings -> Utilities -> QuickTest :
            #self.quickTest()
            
            # 4. Settings -> Utilities -> Full Test :
            #self.fullTest()
                        
            # 5. Support -> Request Automate Support
            self.automateSupport()
            
            self.close_webUI()
            self.log.info('########################Test END ##############################')         

        except Exception as e:
            self.log.error("Test FAILED: exception: {}".format(repr(e)))

    # # @Perform the Quick test on the 'setting' page
    def quickTest(self):
         
         self.log.info('########################Quick Test START ##############################')  
         
         self.access_webUI()
         time.sleep(5)
         self.get_to_page('Settings')
         time.sleep(5)
                
         self.click_button('settings_utilities_link')
         self.wait_until_element_is_clickable('settings_utilitiesQuickTest_button')
         self.click_button('settings_utilitiesQuickTest_button')
         time.sleep(165)
         self.wait_until_element_is_clickable('settings_utilitiesDiskTestClose_button')         
         result = str(self.get_text('DIV_SMART_RES')).splitlines()
                  
         self.click_button('settings_utilitiesDiskTestClose_button')
         
         # Check if return message of each disk contains word 'Pass'
         for i in range(0,len(result)):
            if("Pass" in result[i]):
                self.log.info(result[i])
            else:
                self.log.info(result[i])
                self.log.warning("Disk" + str(i+1) + " fails the quicktest")
    
         self.log.info('########################Quick Test END ##############################\n')  

    # # @Perform the Full Test on the 'setting' page
    def fullTest(self):
         
         self.log.info('########################Full Test START ##############################')  

         time.sleep(20)
                
         self.click_button('settings_utilities_link')
         self.wait_until_element_is_clickable('settings_utilitiesFullTest_button')
         self.click_button('settings_utilitiesFullTest_button')
         
         self.wait_until_element_is_visible('settings_utilitiesDiskTestClose_button', timeout=50000)
         result = str(self.get_text('DIV_SMART_RES')).splitlines()
                  
         self.click_button('settings_utilitiesDiskTestClose_button')
         
         # Check if return message of each disk contains word 'Pass'
         for i in range(0,len(result)):
            if("Pass" in result[i]):
                self.log.info(result[i])
            else:
                self.log.info(result[i])
                self.log.warning("Disk" + str(i+1) + " fails the quicktest") 
         
         self.log.info('########################Full Test END ##############################\n')  
         
    # # @Request automate support
    def automateSupport(self):
         self.log.info('########################Automate Support Test START ##############################')
         self.access_webUI()
         time.sleep(8)

         self.wait_until_element_is_clickable('css=#id_help > img')
         self.click_button('css=#id_help > img')
         self.log.info('Help icon clicked')

         self.wait_until_element_is_clickable('home_support_link')
         self.click_button('home_support_link')
         time.sleep(5)
         self.select_checkbox('css=.LightningCheckbox,support_requestSupport_chkbox')

         self.wait_until_element_is_clickable('support_requestSupport_button')
         self.click_button('support_requestSupport_button')
         #print self._sel.driver.get_window_titles()
         
         # Wait for Loading icon to disappear
         self.wait_until_element_is_not_visible('css=div.updateStr')
         #print self._sel.driver.get_window_titles()

         self._sel.driver.select_window('title=Ask A Question')
         result = str(self.get_text('css=span.pagetitle'))

         if result == 'Service & Support':
            self.log.info('Service & Support page is opened successfully.')
         else:
            self.log.error('Service & Support page is not opened.')
         
         self.log.info('########################Automate Support Test END ##############################')  

# # @Collect the system report
    def systemReport(self, zipname):
         
         sys_report = zipname + ".zip"

         # Clean up the system report zip file first if this test case has been run previously
         if os.path.isfile(sys_report):
             os.remove(sys_report)
             self.log.info('Remove previously downloaded system report')
             self.log.info('########################System Report test START ##############################')
         else:
             self.log.info('########################System Report test START ##############################')

         FTP_URL = self.uut[Fields.internal_ip_address]

         # Create Firefox profile to make the download dialog never prompts
         fp = webdriver.FirefoxProfile()
         fp.set_preference("browser.download.folderList",2)
         fp.set_preference("browser.download.manager.showWhenStarting",False)
         fp.set_preference("browser.download.dir", os.getcwd())
         fp.set_preference("browser.helperApps.neverAsk.saveToDisk","text/plan")

         browser = webdriver.Firefox(firefox_profile=fp)

         browser.get("http://" + FTP_URL)
         time.sleep(7)

         # Make sure login button works successfully.
         maximum_retry = 5
         cur_retry = 1

         while cur_retry < maximum_retry:

                self.log.info('Try: ' + str(cur_retry))

                try:
                    browser.find_element_by_id('login_login_button').click()
                    time.sleep(7)
                    if  browser.find_element_by_id("nav_users_link").is_displayed():
                        self.log.info("Successfully login the page")
                        break
                    else:
                        raise Exception

                except Exception as e:
                   cur_retry += 1
                   self.log.info("exception: {}".format(repr(e)))
                   time.sleep(3)

                   if cur_retry == maximum_retry:
                        self.log.error("Fail to click the login button")

         time.sleep(15)
         browser.find_element_by_xpath("//*[@id=\"id_help\"]").click()

         time.sleep(6)
         browser.find_element_by_id('home_support_link').click()

         time.sleep(10)

         browser.find_element_by_id('support_create_button').click()
         time.sleep(20)
         self.log.info('Successfully downloaded')

         browser.close()

         sys_report_path = os.path.join(os.getcwd(),sys_report)

         if os.path.isfile(sys_report_path):
             self.log.info('System report is saved successfully. Checking the zip content....')

             # Check if the zip file content is correct
             zip = zipfile.ZipFile(file(sys_report_path))
             check_zip_content = zipname + "/"

             if check_zip_content in zip.namelist():
                 self.log.info("Content is correct in the zip file")
                 zip.close()
                 self.log.info('########################System Report test END ##############################\n')
             else:
                 self.log.warning("Content is incorrect in the zip file")
                 self.log.info('########################System Report test END ##############################\n')

         else:
             self.log.error("System report downloaded fail")
             self.log.info('########################System Report test END ##############################\n')
             raise Exception("System report downloaded fail")

selfTestSystemReport()