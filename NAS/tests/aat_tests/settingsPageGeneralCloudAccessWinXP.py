""" Settings Page- General- Cloud Access-WinXP

    @Author: Lee_e
    
    Objective: verify that WinXP setting can be saved
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.00.223)
                    Lightning : PASS (FW: 2.10.109)
                    KC        : PASS (FW: 2.00.221)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.120 & 2.00.224)
                              : Yosemite     - PASS (FW: 2.10.120 & 2.00.224)
                              : Kings Canyon - PASS (FW: 2.10.120 & 2.00.224)
                              : Sprite       - PASS (FW: 2.10.120 & 2.00.224)
                              : Zion         - PASS (FW: 2.10.120 & 2.00.224)
                              : Aurora       - PASS (FW: 2.10.120 & 2.00.224)
                              : Yellowstone  - PASS (FW: 2.10.120 & 2.00.224)
                              : Glacier      - PASS (FW: 2.10.120 & 2.00.224)
     
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.constants import Modes


class SettingsPageGeneralCloudAccessWinXP(TestClient):
    def run(self):
        self.configure_cloud_access()
        self.settings_page_general_cloud_access_winxp_test()

    def tc_cleanup(self):
        self.set_cloud_access_connection_options(connectivity=Modes.CLOUD_ACCESS_AUTO)

    def settings_page_general_cloud_access_winxp_test(self):
        self.log.info('*** Start Testing ***')
        self.get_to_page('Settings')
        self.click_wait_and_check(self.Elements.GENERAL_BUTTON, self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK)
        self.update_cloud_access_configuration()
        self.check_cloud_access_configuration()

    def configure_cloud_access(self):
        self.log.info('*** Configure Cloud Access ***')
        result = self.get_device_info()
        cloud_access_status = ''.join(self.get_xml_tags(result, 'remote_access'))
        self.log.debug('Cloud Access Status: {0}'.format(cloud_access_status))
        if cloud_access_status != 'true':
            self.log.info('Enable Cloud Access')
            self.enable_remote_access()
        else:
            self.log.info('Cloud Access already ON')

    def update_cloud_access_configuration(self):
        self.log.info('*** Update Cloud Access Configuration ***')
        if self.is_element_visible(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK):
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_LINK, self.Elements.SETTINGS_GENERAL_CLOUD_ACCESS_CONNECTION_DIAG)
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_WIN_XP, self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_APPLY)
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_CLOUD_SERVICE_APPLY, self.Elements.UPDATING_STRING, visible=False, timeout=60)
        else:
            self.log.error('configure link doest not exist')

    def check_cloud_access_configuration(self):
        self.log.info('*** Check Cloud Access Configuration ***')
        result = self.get_device_info()
        default_ports_only_status = ''.join(self.get_xml_tags(result, 'default_ports_only'))
        manual_port_forward_status = ''.join(self.get_xml_tags(result, 'manual_port_forward'))
        self.log.debug('default_ports_only_status: {0}, manual_port_forward_status:{1}'.format(default_ports_only_status, manual_port_forward_status))
        if (default_ports_only_status == 'true') and (manual_port_forward_status == 'FALSE'):
            self.log.info('PASS: set to Win XP successfully')
        else:
            self.log.error('FAIL: failed to set Win XP')

SettingsPageGeneralCloudAccessWinXP()