"""
## @Brief Verify unit will logout after set timeout value
#  @details Verify unit will logout after 5 minutes (default)
#   then set timeout value to 9 minutes, verify unit will logout after 9 minutes
"""

import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient


class TimeoutConfiguration(TestClient):

    def run(self):

        self.access_webUI()
        self.get_to_page("Settings")

        self.log.info("Verify default timeout value is 5 minutes")
        try:
            self.element_text_should_be("id_timeout", "5 minutes")
        except Exception, ex:
            self.log.exception(ex)

        self.log.info("Wait 5 minutes to verify system will logout")
        time.sleep(330)

        if self.is_element_visible('login_login_button'):
            self.log.info("Unit timeout as expected")
        else:
            self.log.error("Unit didn't timeout as expected")

        self.log.info("Set timeout value to 9 minutes")
        self.set_web_access_timeout("9")

        self.log.info("Wait 8 minutes...")
        time.sleep(480)

        self.log.info("Should NOT be at login page before 9 minutes")
        if self.is_element_visible('login_login_button'):
            self.log.error("Unit timeout before 9 minutes")
        else:
            self.log.info("Unit didn't timeout before 9 minutes")

        self.log.info("Reset timeout value to default")
        self.set_web_access_timeout("5")

TimeoutConfiguration()




"""
'''
Created on July 14, 2014

@author: vasquez_c, tran_jas
'''

import time
import os
import sys
import logging
import global_libraries.CommonTestLibrary
from testCaseAPI.src.testclient import TestClient

ctl = CommonTestLibrary()
ctl.initialize_test_unit()

# instantiate TESTClient class
test = TestClient(ctl.ip_address, ctl.power_switch_ip, ctl.serial_server_ip, ctl.serial_port_mapping, ctl.username, 
                  ctl.password, ctl.ssh_username, ctl.ssh_password, ctl.ssh_port, ctl.serial_user, ctl.serial_password)


from UnitTestFramework import mylogger
l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('testCases')

# JT - working on (moved from testcases)
## @Brief Verify unit will logout after set timeout value
#  @details Verify unit will logout after 5 minutes (default)
#   then set timeout value to 9 minutes, verify unit will logout after 9 minutes
#  @param --

def configure_timeout(self):
    try:
        # Set pw for admin account so login page is enabled
        test.modify_user_account(username, new_user_password)

        test.access_webUI()

        test.get_to_page("Settings")

        #test.element_text_should_be("id_timeout", "5")

        time.sleep(3)

        test.close_webUI()

        # Reset pw for admin account to null
        test.modify_user_account(username, current_user_password)

    except Exception,ex:
        logger1.info('Failed to configure timeout value, %s', str(ex))
"""