'''
Create on Aug 6, 2015

@Author: lo_va

Objective:  Verify that default shares exist: Public, SmartWare, and TimeMachineBackup. 
'''
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

class sharesDefaultShares(TestClient):
    
    def run(self):
        self.delete_all_shares()
        try:
            self.get_to_page('Shares')
            s1 = self.is_element_visible('shares_share_Public', 2)
            s2 = self.is_element_visible('shares_share_SmartWare', 2)
            s3 = self.is_element_visible('shares_share_TimeMachineBackup', 2)
            if s1 and s2 and s3:
                self.log.info('PASS!! Default Shares is in the list!!')
            else:
                self.log.error('Failed!! Default Shares is not in the list.')
            #self.close_webUI()
        except Exception, ex:
            self.log.exception('Error to check Default Shares List!')
    
sharesDefaultShares()