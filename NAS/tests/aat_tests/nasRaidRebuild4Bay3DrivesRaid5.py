'''
Create on April 1(Fools) , 2015

@Author: vasquez_c

Objective: To validate RAID5 configuration
            rebuild scenarios for 4 bay - 3 drives Consumer NAS.
wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Rebuild
'''

import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
clean_up_disk1 = 'echo -e "o\ny\nw\ny\n" | gdisk /dev/sda'
sharename = 'RebuildR5'
generated_file_size = 10240
timeout = 240

class nasRaidRebuild4Bay3DrivesRaid5(TestClient):

    def run(self):  
            
        try:
            
            # Remove drive 4
            self.remove_drives(4)
            
            # Create RAID5
            self.log.info('Start to create RAID5. This may take a while, please stand by ...')
            self.configure_raid(raidtype='RAID5', volume_size=3000)
            self.verifyMountVolume(volumeid=1)
            self.delete_all_shares()
            self.disable_auto_rebuild()

            # Create share and validate access via smb protocol
            self.log.info('Create %s folder and validate access via smb protocol.' % sharename)
            self.create_shares(share_name=sharename, force_webui=True)
            
            self.read_write_access_check(sharename=sharename, prot='SAMBA')
        except Exception, ex:
            self.log.exception('Configure RAID1 was not successful. Please check RAID settings')
            self.log.info('Failed to Configure RAID5\n' + str(ex)) 
        
        self.read_write_access_check(sharename=sharename, prot='NFS')
        
        # Clean up and Plug out the disk 1
        self.log.info('Plug out Disk 1 !!')
        self.run_on_device(clean_up_disk1)
        self.remove_drives(1)
        
        # Check RAID status was in degraded and validate access
        self.log.info('Check RAID5 status was in degraded and validate access.')
        raid_status1 = self.raid_status(1)
        if 'degraded' in raid_status1:
            try:
                self.log.info('Raid status: Degraded.')
                self.check_home_page('degraded')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
                self.read_write_access_check(sharename=sharename, prot='NFS')
            except Exception, ex:
                self.log.exception('Failed to access Degraded_RAID5\n' + str(ex))
        else:
            self.log.error('RAID5 is not in degrade mode\n')
                                           
        # Plug in Disk 1 and validate RAID access
        self.log.info('Plug in Disk 1 !')
        self.insert_drives(1)
        self.manual_start_rebuild()
        # Time to wait rebuilding function starting. Optional
        self.wait_rebuild_start()
            
        self.log.info('Start to check rebuilding RAID access.')
        raid_status1 = self.raid_status(1)
        if 'recovering' in raid_status1:
            try:
                self.log.info('Raid status: Rebuilding.')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
            except Exception, ex:
                self.log.exception('Failed to access Rebuilding_RAID5\n' + str(ex))
        else:
            self.log.warning('RAID status is not in recovering')

        # Wait until rebuild is finished and validate RAID access.
        self.wait_rebuild_finished()
        self.log.info('Check RAID5 status.')
        raid_status = self.check_storage_page()
        if raid_status == 'Healthy' or raid_status == 'Verifying RAID parity':
            try:
                self.log.info('Raid status is healthy.')
                self.check_home_page('healthy')
                self.read_write_access_check(sharename=sharename, prot='SAMBA')
                self.log.info('****** NAS Rebuild for RAID5 test PASSED!! ******')
            except Exception, ex:
                self.log.exception('Failed to access Healthy_RAID5\n' + str(ex))
                self.log.info('****** NAS Rebuild for RAID5 test FAILED!! ******')
        else:
            self.log.info('Raid status = %s' % raid_status)
            self.log.critical('****** NAS Rebuild for RAID5 test FAILED!! ******')

        # Cleanup
        self._myCleanup()
            
    def tc_cleanup(self):
        self.checked_disk_exist()

    def checked_disk_exist(self, check_drives_num=4):
        drives_exist_list = self.run_on_device('cat /proc/partitions | awk \'{print $4}\' | grep sd[a-z] | grep -v [0-9]')
        if check_drives_num >= 2:
            if not 'sda' in drives_exist_list:
                self.insert_drives(1)
            if not 'sdb' in drives_exist_list:
                self.insert_drives(2)
        if check_drives_num == 4:
            if not 'sdc' in drives_exist_list:
                self.insert_drives(3)
            if not 'sdd' in drives_exist_list:
                self.insert_drives(4)

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED!!')
        else:
            self.log.error('Hash test FAILED!!')
    
    
    def read_write_access_check(self, sharename, prot):

        if prot == 'SAMBA':
            generated_file = self.generate_test_file(generated_file_size)
            time.sleep(10)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            # Wait for transfer finished
            time.sleep(10)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName='file0.jpg')
            self.read_files_from_smb(files='file0.jpg', share_name=sharename, delete_after_copy=True)
            time.sleep(120)
            
        elif prot == 'NFS':
            nfs_mount = self.mount_share(share_name=sharename, protocol=self.Share.nfs)
            if nfs_mount:
                created_files = self.create_file(filesize=generated_file_size, dest_share_name=sharename)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=sharename):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
        else:
            self.log.info('Protocol {} is not supported'.format(protocol))
            
    def raid_status(self, raidnum):
        check_raid_status = 'mdadm --detail /dev/md%s | grep "State :"' % raidnum
        raid_status = self.run_on_device(check_raid_status)
        time.sleep(1) # Time to wait status return value
        while not raid_status:
            raid_status = self.run_on_device(check_raid_status)
            time.sleep(1)
        self.log.info('Raid_status = %s' % raid_status)
        raid_status1 = str(raid_status.split(': ')[1])

        return raid_status1
    
    def wait_rebuild_start(self):
        # Wait for recovering started
        self.log.info('Waiting up to 30 seconds for the RAID status become rebuild')
        timer = Stopwatch(timer=30)
        timer.start()

        while True:
            raid_status1 = self.raid_status(1)
            if 'recovering' in raid_status1:
                self.log.info('RAID status took {} seconds became rebuild'.format(timer.get_elapsed_time()))
                break
            else:
                if timer.is_timer_reached():
                    self.log.warning('RAID status never became rebuilding')
                    break
                else:
                    time.sleep(1)
    
    def wait_rebuild_finished(self, web_check=True):
        # Wait for recovering finished
        self.log.info('Waiting up to 120 minutes for the RAID rebuilding finished')
        timer = Stopwatch(timer=7200)
        timer.start()

        while True:
            raid_status1 = self.raid_status(1)
            if 'recovering' not in raid_status1 and 'degraded' not in raid_status1:
                self.log.info('RAID took {} minutes for rebuilding'.format(timer.get_elapsed_time()/60))
                break
            if 'recovering' in raid_status1:
                if timer.is_timer_reached():
                    self.log.warning('Rebuilding out of time')
                    break
                else:
                    time.sleep(30)
            else:
                self.log.warning('Raid not in recovering mode')
                break
            
        if web_check:
            timer = Stopwatch(timer=60)
            timer.start()
            while True:
                raid_status = self.check_storage_page()
                self.log.info('Raid Drives Status = {}'.format(raid_status))
                if raid_status in 'Healthy':
                    self.log.info('Rebuild finished.')
                    break
                else:
                    if timer.is_timer_reached():
                        self.log.error('Page Rebuilding get STUCK!')
                        break
                    else:
                        time.sleep(2)
    
    def disable_auto_rebuild(self):
        try:
            self.get_to_page('Storage')
            check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
            time.sleep(2)
            check_status_attr = check_status.get_attribute('style')
            if 'none' in check_status_attr:
                self.log.info('Disable Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                check_status_attr = check_status.get_attribute('style')
                while 'none' in check_status_attr:
                    self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                    check_status_attr = check_status.get_attribute('style')
                    time.sleep(2)
                time.sleep(2)
            if 'block' in check_status_attr:
                self.log.info('Auto-Rebuild already has been disabled')
            self.close_webUI()
        except Exception, ex:
                self.log.exception('Failed to disable auto-rebuild\n' + str(ex))
                
    def manual_start_rebuild(self):
        try:
            self.log.info('Manual start raid rebuild.')
            self.get_to_page('Storage')
            time.sleep(1)
            self.click_element('storage_raidManuallyRebuild_button')
            try:
                self.wait_until_element_is_visible('storage_raidManuallyRebuildNext1_button', timeout)
            except:
                self.log.info('Sometimes Click button will be hanged, try 1 more time.')
                self.click_element('storage_raidManuallyRebuild_button')
                self.wait_until_element_is_visible('storage_raidManuallyRebuildNext1_button', timeout)
            self.click_element('storage_raidManuallyRebuildNext1_button')
            self.wait_until_element_is_clickable('storage_raidManuallyRebuildNext2_button', timeout)
            self.click_element('storage_raidManuallyRebuildNext2_button')
            self.wait_until_element_is_not_visible(E.UPDATING_STRING, timeout)
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Failed to manual start rebuilding!')
            
    def check_storage_page(self):
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        while not raid_status:
            raid_status = self.get_text('raid_healthy')
            time.sleep(1)
        return raid_status       
 
    def check_home_page(self, raidstate):    
        try:
            self.log.info('Start to check raid status in home page')
            self.get_to_page('Home')
            if raidstate == 'degraded':
                dignostics_state = self.get_text('diagnostics_state')
                if 'Caution' in dignostics_state:
                    self.click_element('smart_info')
                    self.drag_and_drop_by_offset('css = div.jspDrag', 0, 80)
                    diagnosticsRaidStatus = self.get_text('home_diagnosticsRaidStatus_value')
                    if 'Degraded' in diagnosticsRaidStatus:
                        self.log.info('Raid status is Degraded!')
                else:
                    self.log.error('Raid status is not in Degraded')         
                    
            if raidstate == 'healthy':
                dignostics_state = self.get_text('diagnostics_state')
                if dignostics_state == 'Healthy':
                    self.log.info('Raid status is Healthy!')
                else:
                    self.log.error('Raid status is not in Healthy')
                    
            # Capacity verification
            if self.is_element_visible('home_b4_capacity_info'):
                raid_capacity = self.get_text('home_volcapity_info')
            elif self.is_element_visible('home_b4_free'):
                raid_capacity = self.get_text('home_b4_free')

            if raid_capacity:
                self.log.info('Raid capacity is {}'.format(raid_capacity))
            if not raid_capacity:
                self.log.error('Checked raid capacity failed in home page!')
            self.close_webUI()
            
        except Exception, ex:
            self.log.exception('Checked raid status failed in home page!')
    
    def verifyMountVolume(self, volumeid):
        vol = ''
        try:
            self.get_to_page('Storage')
            if self.is_element_visible('css = #vol_list > tbody:nth-child(1) > tr:nth-child(%s) > td:nth-child(1) > div:nth-child(1)' % 1):
                vol = self.get_text('css = #vol_list > tbody:nth-child(1) > tr:nth-child(%s) > td:nth-child(1) > div:nth-child(1)' % 1)
               
            if 'Volume_1' in vol:
                self.log.info('Volume: {} Mount Succeeded!!'.format(vol))
            else:
                self.log.error('Volume: {} Mount Failed!!'.format(vol))
        except Exception, ex:
            self.log.exception('Volume did not mount!!')
                       
    def _myCleanup(self):
        self.insert_drives(4)
        
            
nasRaidRebuild4Bay3DrivesRaid5()