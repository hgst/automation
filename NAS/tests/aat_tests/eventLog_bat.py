""" 
    BAT - Event log
    - SMB
    - FTP
    - NFS

    @Author: Carlos Vasquez
        
    Automation: Partial
    
    Tested Product:
        Lightning : unit does not support this feature
    
    Test Status:
              Local Yosemite: Pass (FW: 2.00.157)
            Jenkins Kings Canyon: Pass (FW: 2.00.164)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries import CommonTestLibrary
from testCaseAPI.src.testclient import Elements as E

from ftplib import FTP
from ftplib import error_perm
import time
import os
import shutil

smbShare = 'smbShare'
ftpShare = 'ftpShare'
nfsShare = 'nfsShare'
notSupportedModel = ['LT4A', 'GLCR']
shareList = ['smbShare', 'ftpShare', 'nfsShare']

filesize = 10240

class batEventLog(TestClient):
    
    def run(self):
        self.log.info('######################## Setup START ##############################')
        self.delete_all_shares()
         
        # Create shares
        for sharename in shareList:
            self.log.info('Creating share: {}'.format(sharename))
            self.create_shares(share_name=sharename, force_webui=True)
                     
        self.log.info('######################## Setup END ##############################')
        self.log.info('######################## SMB Files Transfer START ##############################')
        tmp_file = os.path.abspath(self.generate_test_file(1024))
        tmpfile = ''.join(tmp_file)          
        self.log.info("Generated temporary file: {}".format(tmpfile))
        self.write_files_to_smb(files=tmpfile, share_name=smbShare, delete_after_copy=False)
        try:
            self.read_files_from_smb(files='file0.jpg', share_name='smbShare', delete_after_copy=True)
            time.sleep(120)
            self.log.info('SUCCESS: smb read test')
        except Exception as e:
            self.log.error(repr(e))
              
        self.log.info('######################## SMB Files Transfer END ##############################') 
        
        self.log.info('######################## FTP Files Transfer START ##############################')         
        self.add_new_user(new_user_name='user1', new_user_pwd='password1')
        self.enable_ftp_share_access(share_name=ftpShare, access='Anonymous None')
        time.sleep(10)   
        self.ftp_rw_test(folder=ftpShare)
        self.log.info('######################## FTP Files Transfer END ##############################') 
    
        self.log.info('######################## NFS Files Transfer START ##############################') 
                      
        nfs_mount = self.mount_share(share_name=nfsShare, protocol=self.Share.nfs)
        
        try:
            created_files = self.create_file(filesize=filesize, dest_share_name=nfsShare)
            for next_file in created_files[1]:
                if not self.is_file_good(next_file, dest_share_name=nfsShare):
                    self.log.error('FAILED: nfs read test')
                else:
                    self.log.info('SUCCESS: nfs read test')
                
        except Exception as e:
            self.log.error(repr(e))  
              
        self.log.info('######################## NFS Files Transfer END ##############################') 
        self.view_logs()
    
    def add_new_user(self, new_user_name='user1', new_user_pwd='password1'):
        """
            Create a new user (RESTful API)
            @new_user_name: new user name
            @new_user_pwd: new user's password
        """        
        try:
            self.log.info("==> Create new user (by RESTAPI)")
            resp = self.get_user(new_user_name)[0]
            if resp == 0: # user exists
                self.delete_user(new_user_name)
                self.create_user(new_user_name, new_user_pwd)
            else: #404, user does not exist
                self.create_user(new_user_name, new_user_pwd)
            self.log.info("User: {} is created successfully.".format(new_user_name))
        except Exception as e:
            self.log.exception('FAILED: Unable to create new user, exception: {}'.format(repr(e)))            
      
    def view_logs(self):
        self.click_button(E.SETTINGS_UTILITY_VIEWLOGS)
        # Get number of lines before clearing logs
        beginingLogNumber = self.get_text('settings_utilitiesLogTotal_value')
        
        
        log_level = self.get_text('id_level')
        if log_level == 'All':
            self.log.info('SUCCESS: log level is set to All')
        else:
            self.log.error('FAILED: log level is not set to All')
        filter_by = self.get_text('id_filter')
        if filter_by == 'All':
            self.log.info('SUCCESS: filter_by is set to All')
        else:
            self.log.error('FAILED: filter_by is not set to All')
            
        self.click_button('settings_utilitiesLogClear_button')
        
        self.click_button(E.SETTINGS_UTILITY_VIEWLOGS) 
        endingLogNumber = self.get_text('settings_utilitiesLogTotal_value')
        
        # Verify logs cleared
        if beginingLogNumber > endingLogNumber:
            self.log.info('SUCCESS: Alerts were cleared from the log')
        else:
            self.log.error('FAILED: Alerts were not cleared from the log')
           
        self.click_button('settings_utilitiesLogClose_button')
        
        # Transfer content over SMB
        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        tmpfile = ''.join(tmp_file)
        self.log.info("Generate temporary file: {}".format(tmpfile))
        self.write_files_to_smb(files=tmpfile, share_name=ftpShare)
        
        self.click_button(E.SETTINGS_UTILITY_VIEWLOGS)
        # Verify log for recently file transfer
        # Get log number
        logNumber = self.get_text('settings_utilitiesLogTotal_value')
        
        for i in range(1,int(logNumber)): 
            if self.get_text("//tr[@id='row{}']/td[3]/div".format(i)):
                self.log.info('SUCCESS: SAMBA file transfer found in row {}'.format(i))
                break
            else:
                print ('FAILED: SAMBA file transfer Not Found')
        
        
        
        self.click_button('settings_utilitiesLogClose_button')
        
    def ftp_rw_test(self, folder='ftpShare'):
        """
            Validate anonymous r/w access
            
            @folder: The folder name anonymous user will browse            
        """
        try:
            ftpconn, ex = self.ftpConnection(anonymous=False)
            self.ftpWriteTest(ftpconn, share_folder=folder, delete_after_upload=True)
            self.log.info('SUCCESS: ftp write test')
        except Exception as e:
            self.log.error(repr(e))
            
        try:
            ftpconn, ex = self.ftpConnection(anonymous=False)
            self.ftpReadTest(ftpconn, share_folder=folder, delete_after_download=False)
            self.log.info('SUCCESS: ftp read test')
        except Exception as e:
            self.log.error(repr(e))         
        

    def ftpConnection(self, ftp_ip="", new_user_name='user1', new_user_pwd='password1', anonymous=True):
        """
            Create ftp connection and login by user account or anonymous
            Once login, it will return the ftp socket back and exception
            
            @ftp_ip: '192.168.1.91' or self.uut[Fields.internal_ip_address]
            @anonymous: True (Anonymous login)
                        User login: new_user_name / new_user_pwd (Set to anonymous=False)
                        If you do not set anonymous to False, it always login as anonymous user
            
            Need to set anonymous=False to switch to user login mode
            @new_user_name: user account
            @new_user_pwd: user password               
            
            return (ftpconn, exception)
            
            Usage:
            ftpconn, ex = self.ftpConnection('192.168.6.104', new_user_name='user1', new_user_pwd='password1', anonymous=False)
            ftpconn, ex = self.ftpConnection('192.168.6.104', anonymous=True)
        """
        ex = None
        if ftp_ip == "":
            ftp_ip = self.uut[Fields.internal_ip_address]
            
        try:
            ftpconn = FTP(ftp_ip)
    
        except EOFError:
            connected = False
            for i in range(0,3):
                while not connected:
                    self.log.info('Unable to connect to FTP. Retrying...')
                    time.sleep(5)
                    ftpconn = FTP(ftp_ip)
                    if ftpconn <> None:
                        connected = True      
                        break
            self.log.exception("Warning: Unable to connect to FTP: {}, exception: {}".format(ftp_ip, repr(ex)))
            ftpconn = None
            return (ftpconn, ex)

        try:
            if anonymous:
                resp = ftpconn.login()
            else:
                resp = ftpconn.login(new_user_name, new_user_pwd)
        except error_perm as ex:
            self.log.info(CommonTestLibrary.exception("Warning: Unable to login ftp")) 
            raise
       
                                
        if '230' in resp:
            if anonymous:
                self.log.info("Anonymous login succeed")
            else:
                self.log.info("{} login succeed".format(new_user_name))
        else:
            if anonymous:
                self.log.error("Anonymous login failed")
            else:
                self.log.error("{} login failed".format(new_user_name))
        
        return (ftpconn, ex)

    def ftpReadTest(self, ftpconn, filesize=filesize, share_folder='ftpShare', delete_after_download=False):
        """
            Pre-requiste:
                You need to have temporary file called 'file0.jpg' under the share_folder for test
                Normally, run the upload(write) test first to generate the temporary file there
                Then, execute the download(read) test
             
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_download: True (Delete the temporary file after downloading test)
        """
        resp = ftpconn.cwd(share_folder) # switch to 'ftpShare' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))        
        
        # download file0.jpg as file_check.jpg
        tmpfile = 'file0.jpg' # temporary file on FTP server       
        download_file = "download_file0.jpg" # filename which is saved locally

        # if download_file exists, removes it
        if os.path.isfile(download_file):
            os.remove(download_file)

        # Check if tmpfile exists or not
        listfiles = ftpconn.nlst()
        if tmpfile in listfiles:
            self.log.info("{} exists, read test is ready to go".format(tmpfile))
        else:
            raise Exception("FAILED: {} does not exist for read test, STOP!".format(tmpfile))
    
        self.log.info("*** Going to download file for read access test ***")
        
        try:
            with open(download_file, "wb") as f:
                def callback(data):
                    f.write(data)
                resp = ftpconn.retrbinary("RETR "+tmpfile, callback, blocksize=1048576)
            if '226' in resp:
                self.log.info("{} is saved locally".format(download_file))
                self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("FAILED: {} download failed".format(download_file))
            
        except Exception as e:
            raise Exception("FAILED: ftpReadTest failed, can not download {}, exception: {}".format(tmpfile, repr(e)))
        ftpconn.close()
        
        if delete_after_download:
            os.remove(download_file)
            self.log.info("Cleaning temporary files: {}".format(os.path.abspath(download_file)))
        
        self.log.info("*** Read access test END ***")                 
    
    def ftpWriteTest(self, ftpconn, filesize=filesize, share_folder='ftpShare', delete_after_upload=True):
        """
            Generate temporary file called file0.jpg for uploading test
            
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_upload: True (Delete the temporary file after uploading test)
        """
        resp = ftpconn.cwd(share_folder) # switch to 'ftpShare' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))   
        
        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        tmpfile = ''.join(tmp_file)
        self.log.info("Generate temporary file: {}".format(tmpfile))
                    
        self.log.info("** Going to upload temporary file for write access test **")
        # upload file0.jpg
        try:
            with open(tmpfile, "rb") as f:
                resp = ftpconn.storbinary("STOR "+os.path.basename(tmpfile), f, 1048576) # 1048576 = 1MB
            if '226' in resp: # File successfully transferred
                self.log.info("{} is uploaded successfully".format(os.path.basename(tmpfile)))
                self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("Upload {} failed".format(os.path.basename(tmpfile)))
        except error_perm as e:
            raise e
        except Exception as e:
            raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. exception: {}".format(os.path.basename(tmpfile), repr(e)))
        ftpconn.close()
        
        #os.chdir('..') # switch to parent directory instead of staying in temporary folder
        if delete_after_upload:
            shutil.rmtree(os.path.dirname(tmpfile))
            self.log.info("Cleaning temporary files: {}".format(tmpfile))
        
        self.log.info("*** Write access test END ***")    
    
batEventLog()