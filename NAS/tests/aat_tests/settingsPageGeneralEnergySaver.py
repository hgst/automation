""" Settings Page-General-Energy Saver

    @Author: Lee_e
    
    Objective: verify each config in energy saver can be modified
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.00.216)
                    Lightning : PASS (FW: 2.10.107)
                    KC        : PASS (FW: 2.00.215)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.107 & 2.00.216)
                              : Yosemite     - PASS (FW: 2.10.107 & 2.00.216)
                              : Kings Canyon - PASS (FW: 2.10.107 & 2.00.216)
                              : Sprite       - PASS (FW: 2.10.107 & 2.00.216)
                              : Zion         - PASS (FW: 2.10.109 & 2.00.216)
                              : Aurora       - PASS (FW: 2.10.107 & 2.00.221)
                              : Yellowstone  - PASS (FW: 2.10.107 & 2.00.216)
                              : Glacier      - PASS (FW: 2.10.107 & 2.00.221)
     
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.product_info import Attributes
import sys
import time
import os
import requests

default_web_access_timeout_value = 5
default_drive_sleep_status = 1
default_power_sch_status = 0
default_led_status = 1
led_exclude_list = ['BWZE', 'BWAZ', 'BNEZ', 'BBAZ']
power_sch_exclude_list = ['GLCR']

class SettingsPageGeneralEnergySaver(TestClient):
    def run(self):
        model_number = self.get_model_number_check()
        self.log.debug('model number: {0}'.format(model_number))
        ori_web_access_timeout_value = None
        ori_led_status = None
        ori_drive_sleep_status = None
        ori_power_sch_status = None
        
        settings_general_result = self.get_settings_general_info_cgi()
        ori_drive_sleep_status = self.get_drive_sleep_status(settings_general_result)
        ori_web_access_timeout_value = self.get_web_access_timeout_status(settings_general_result)
        
        if model_number not in power_sch_exclude_list:
            self.power_sch_config_reset_cgi()
            ori_power_sch_status = self.check_power_sch_enable() 
            
        if model_number not in led_exclude_list:
            ori_led_status = self.get_led_status(settings_general_result)
       
        self.SettingsPageGeneralEnergySaver_test(ori_drive_sleep_status, ori_led_status, ori_power_sch_status)
    
    def SettingsPageGeneralEnergySaver_test(self, ini_drive_sleep_status=None, ini_led_status=None, ini_power_sch_status=None):
        self.log.info('*** Start Settings Page - General -Energy Saver Testing ***')
        self.get_to_page('Settings')
        self.click_wait_and_check(self.Elements.GENERAL_BUTTON, 'mainbody')
        self.drive_sleep_toggle_test(ini_drive_sleep_status)
        if ini_led_status is not None:
            self.led_toggle_test(ini_led_status)
        if ini_power_sch_status is not None:
            self.power_sch_toggle_test(ini_power_sch_status)
        self.web_access_timeout_setting_test()
        self.close_webUI()          
       
    def tc_cleanup(self):
        model_number = self.get_model_number_check()
        self.log.debug('model number: {0}'.format(model_number))
        self.drive_sleep_cgi(default_drive_sleep_status)
        self.web_access_timeout_config(default_web_access_timeout_value)
        if model_number not in power_sch_exclude_list:
            self.power_sch_config_reset_cgi()
            self.power_sch_enable_cgi(default_power_sch_status)

        if model_number not in led_exclude_list:
            self.led_cgi(default_led_status)

    def drive_sleep_toggle_test(self, ori_drive_sleep_status):
        self.start_test('*** Start Drive Sleep Toggle Testing ***')
        self.log.debug('ori_drive_sleep_status: {0}'.format(ori_drive_sleep_status))
        drive_UI_initial = self.get_drive_slepp_status_from_UI()
        drive_status_dict = {'ON': 1, 'OFF': 0}
        if int(ori_drive_sleep_status) == drive_status_dict[drive_UI_initial]:
            self.pass_test('PASS: Drive Sleep UI shows {0} correctly'.format(drive_UI_initial))
        else:
            self.fail_test('FAIL: Drive Sleep UI shows {0} incorrectly'.format(drive_UI_initial))
        
        for i in range(0, 2):
            drive_current_toggle = self.drive_sleep_config_from_UI(drive_UI_initial)
            drive_UI_initial = drive_current_toggle
    
    def drive_sleep_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.start_test('*** Toggle Drive Sleep to {0} Test ***'.format(value_oppsite_dict[value]))
        self.click_check_element(self.Elements.SETTINGS_GENERAL_DRIVE_SLEEP_SWITCH, self.Elements.UPDATING_STRING, 'invisible',
                                 'settings_generalDriveSleep_switch', str(value_dict[value]), 'value')
        current_value = self.get_drive_slepp_status_from_UI()
        self.log.debug('current_value: {0} , previous value: {1}'.format(current_value, value))
        if value == 'ON':    
            if current_value == 'OFF':
                self.pass_test('PASS: Drive Sleep can switch to {0} and shows correctly'.format(current_value))
            else:
                self.fail_test('FAIL: Drive Sleep button shows {0} but should be OFF'.format(current_value))
        else:            
            if current_value == 'ON':
                self.pass_test('PASS: Drive Sleep can switch to {0} and shows correctly'.format(current_value))
            else:
                self.fail_test('FAIL: Drive Sleep button shows {0} but should be ON'.format(current_value))
        return current_value        
        
    def get_drive_slepp_status_from_UI(self):
        self.log.info('*** Get Drive Sleep Status From UI')
        value = self.get_text(self.Elements.SETTINGS_GENERAL_DRIVE_SLEEP_SWITCH)
        
        return value
    
    def drive_sleep_cgi(self, enable):
        self.log.info('*** Drive Sleep Config set to {0} ***'.format(enable))
        drive_sleep_config_cgi = 'cgi_power_management&f_hdd_hibernation_enable='+str(enable)
        head_url = 'system_mgr.cgi'
        self.send_cgi(drive_sleep_config_cgi, head_url)        
        
    def led_toggle_test(self, ori_led_status):
        self.start_test('*** Start LED Toggle Testing ***')
        self.log.debug('ori_led_status: {0}'.format(ori_led_status))
        led_UI_initial = self.get_led_status_from_UI()
        led_status_dict = {'ON': 1, 'OFF': 0}
        if int(ori_led_status) == led_status_dict[led_UI_initial]:
            self.pass_test('PASS: LED UI shows {0} correctly'.format(led_UI_initial))
        else:
            self.fail_test('FAIL: LED UI shows {0} incorrectly'.format(led_UI_initial))
        
        for i in range(0, 2):
            led_current_toggle = self.led_config_from_UI(led_UI_initial)
            led_UI_initial = led_current_toggle
    
    def led_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.start_test('*** Toggle LED to {0} Test ***'.format(value_oppsite_dict[value]))
        self.click_check_element(self.Elements.SETTINGS_GENERAL_LED_SWITCH, self.Elements.UPDATING_STRING, 'invisible',
                                 'settings_generalLed_switch', str(value_dict[value]), 'value')
        current_value = self.get_led_status_from_UI()
        self.log.debug('current_value: {0} , previous value: {1}'.format(current_value, value))
        if value == 'ON':    
            if current_value == 'OFF':
                self.pass_test('PASS: LED can switch to {0} and shows correctly'.format(current_value))
            else:
                self.fail_test('FAIL: LED button shows {0} but should be OFF'.format(current_value))
        else:            
            if current_value == 'ON':
                self.pass_test('PASS: LED can switch to {0} and shows correctly'.format(current_value))
            else:
                self.fail_test('FAIL: LED button shows {0} but should be ON'.format(current_value))
        return current_value        
        
    def get_led_status_from_UI(self):
        self.log.info('*** Get LED Status From UI')
        value = self.get_text(self.Elements.SETTINGS_GENERAL_LED_SWITCH)
        
        return value
     
    def led_cgi(self, enable):
        self.log.info('*** LED Config set to {0} ***'.format(enable))
        led_config_cgi = 'cgi_led&f_led_enable='+str(enable)
        head_url = 'system_mgr.cgi'
        self.send_cgi(led_config_cgi, head_url)

    def power_sch_toggle_test(self, ori_power_sch_status):
        self.start_test('*** Start Power Schedule Toggle Testing ***')
        self.log.debug('ori_power_sch_status: {0}'.format(ori_power_sch_status))
        power_sch_UI_initial = self.get_power_sch_status_from_UI()
        power_sch_status_dict = {'ON': 1, 'OFF': 0}
        if int(ori_power_sch_status) == power_sch_status_dict[power_sch_UI_initial]:
            self.pass_test('PASS: Power Schedule UI shows {0} correctly'.format(power_sch_UI_initial))
        else:
            self.fail_test('FAIL: Power Schedule UI shows {0} incorrectly'.format(power_sch_UI_initial))
        
        for i in range(0, 2):
            power_sch_current_toggle = self.power_sch_config_from_UI(power_sch_UI_initial)
            power_sch_UI_initial = power_sch_current_toggle
    
    def power_sch_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.start_test('*** Toggle Power Schedule to {0} Test ***'.format(value_oppsite_dict[value]))
        if value == 'ON':
            self.click_check_element(self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE_SWITCH, self.Elements.UPDATING_STRING, 'invisible',
                                 'settings_generalPowerSch_switch', str(value_dict[value]), 'value')
            current_value = self.get_power_sch_status_from_UI()
            self.log.debug('current_value: {0} , privious value: {1}'.format(current_value, value))
            
            if current_value == 'OFF':
                self.pass_test('PASS: Power Schedule can switch to {0} and shows correctly'.format(current_value))
            else:
                self.fail_test('FAIL: Power Schedule button shows {0} but should be OFF'.format(current_value))
        else:
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE_SWITCH, self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE)
            self.power_schedule_config_from_UI()
            current_value = self.get_power_sch_status_from_UI()
            self.log.debug('current_value: {0} , privious value: {1}'.format(current_value, value))
            if current_value == 'ON':
                self.pass_test('PASS: Power Schedule can switch to {0} and shows correctly'.format(current_value))
            else:
                self.fail_test('FAIL: Power Schedule button shows {0} but should be ON'.format(current_value))
        return current_value    
     
    def power_schedule_config_from_UI(self):
        self.log.info('*** Config Power Schedule ***')
        self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE, self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE_DIAG)
        self.click_wait_and_check('settings_generalPowerSun', 'settings_power1_button')
        self.click_wait_and_check('settings_power1_button', self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE_SAVE)
        self.click_check_element(self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE_SAVE, self.Elements.UPDATING_STRING, 'invisible',
                                 'settings_generalPowerSch_switch', '1', 'value')
        
    def get_power_sch_status_from_UI(self):
        self.log.info('*** Get Power Schedule Status From UI')
        value = self.get_text(self.Elements.SETTINGS_GENERAL_POWER_SCHEDULE_SWITCH)
        
        return value

    def check_power_sch_enable(self):
        self.log.info('*** Get Power Schedule Status ***')
        power_off_scheduling_enable = 'xmldbc -g system_mgr/power_management/power_off_scheduling_enable'
        output = self.run_on_device(power_off_scheduling_enable)
        self.log.info('current power_off_scheduling status: {0}'.format(output))
        
        return output
        
    def power_sch_enable_cgi(self, enable):
        self.log.info('*** Power Schedule Enable set to {0} ***'.format(enable))
        fw_version = self.get_fw_version()
        head_url = 'system_mgr.cgi'
        if int(fw_version) == 1:
            cmd = 'cgi_power_off_sch&f_power_off_enable='
        else:
            cmd = 'cgi_power_sch_enable&enable='
        power_sch_config_cmd = cmd + str(enable)
        
        self.send_cgi(power_sch_config_cmd, head_url)
    
    def power_sch_config_reset_cgi(self):  
        fw_version = self.get_fw_version()
        head_url = 'system_mgr.cgi'          
        if int(fw_version) == 1:
            on = ['0 0 0']*7
            off = ['0 23 59']*7
            reset_schedule_cmd = 'cgi_power_off_sch&f_power_off_enable=1&schedule=' + ','.join(on) + '&off_schedule=' + ','.join(off)
        else:
            set = ['1,1,1,1,1,1,1,1,1,1,1,1']*7
            reset_schedule_cmd = 'cgi_power_sch&enable=1&schedule=' + ','.join(set) 
        self.send_cgi(reset_schedule_cmd, head_url)

    def web_access_timeout_setting_test(self, web_access_timeout=9):
        self.start_test('*** Start Web Access Timeout Setting Testing ***')
        self.web_access_timeout_config_from_UI(value=web_access_timeout)
        value = self.get_web_access_timeout_value_from_UI()
        self.log.debug('web access timeout value: {0} from UI'.format(value))
        if int(value) == web_access_timeout:
            self.pass_test('PASS: Web Access Timeout Value set to {0} correctly'.format(web_access_timeout))
        else:
            self.fail_test('FAIL: Web Access Timeout shows {0} incorrectly'.format(value))
    
    def web_access_timeout_config_from_UI(self, value):
        self.log.info('*** Web Access Timeout Value set to {0} minutes Test ***'.format(value)) 
        timeOut = 'link=' + str(value) + ' minutes'            
        retry = 3
        
        while retry > 0:
            try:
                self.click_element(self.Elements.SETTINGS_GENERAL_WEB_ACCESS_TIMEOUT_LIST)
                self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_WEB_ACCESS_TIMEOUT_LIST, 'id_timeout_li')
                count = 0

                # scroll down if element is not displayed on drop down menu
                while self.is_element_visible(timeOut) is False and count < 10:
                    self.log.info('Timeout value: {0} minutes not visible on dropdown menu'.format(value))

                    # if drop down menu closed, re-open it
                    if not self.is_element_visible('css=div.jspDrag'):
                        self.click_element(self.Elements.SETTINGS_GENERAL_WEB_ACCESS_TIMEOUT_LIST)
                        time.sleep(3)

                    # scroll down
                    self.drag_and_drop_by_offset('css=div.jspDrag', 0, 30)
                    time.sleep(3)
                    count += 1

                    # if element is visible, exit loop
                    if self.is_element_visible(timeOut):
                        break
                
                #self.drag_and_drop_by_offset(source='css=.jspDrag', xoffset=0, yoffset=px)
                self.click_check_element(timeOut, self.Elements.UPDATING_STRING, 'invisible',
                                 'id_timeout', str(value), 'rel')
                break
            except Exception as e:
                if retry > 0:
                    self.log.info('Retry {0} times to re-set Web Access Timeout'.format(3-retry))
                    retry -= 1
                else:
                    self.log.exception('Exception:{0}'.format(repr(e)))
        
    def get_web_access_timeout_value_from_UI(self):
        self.log.info('*** Get LED Status From UI')
        value = self.get_text(self.Elements.SETTINGS_GENERAL_WEB_ACCESS_TIMEOUT_LIST)
        
        return value.split(' ')[0]       
       
    def web_access_timeout_config(self, timeout_value):
        self.log.info('*** Web Access Timeout set to {0} ***'.format(timeout_value))
        cgi_idle = 'cgi_idle&f_idle='+str(timeout_value)
        head_url = 'system_mgr.cgi'
        self.send_cgi(cgi_idle, head_url)
        
    def get_settings_general_info_cgi(self):
        self.log.info('*** Get Settings General Information ***')
        general_cgi = 'cgi_get_general'
        head_url = 'system_mgr.cgi'
        result = self.send_cgi(general_cgi, head_url)
        return result
    
    def get_web_access_timeout_status(self, result):
        self.log.info('*** Get Web Access Timeout info ***')
        web_access_timeout_value = ''.join(self.get_xml_tags(result, tag='idle'))
        self.log.info('Web Access Timeout info: {0}'.format(web_access_timeout_value))
        return web_access_timeout_value
        
    def get_drive_sleep_status(self, result):
        self.log.info('*** Get Drive Sleep Status info ***')
        drive_sleep_enable_status = ''.join(self.get_xml_tags(result, tag='hdd_hibernation_enable'))
        self.log.info('Drive Sleep status: {0}'.format(drive_sleep_enable_status))
        return drive_sleep_enable_status

    def get_led_status(self, result):
        self.log.info('*** Get LED info ***')
        led_enable_status = ''.join(self.get_xml_tags(result, tag='led_enable'))
        self.log.info('LED status: {0}'.format(led_enable_status))
        return led_enable_status
        
    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

    def get_fw_version(self):
        result = self.get_firmware_version_number()
        version = result.split('.')[0]
        self.log.info('Head of firmware is :' + version)
        
        return version
            
    def click_check_element(self, element, next_element, visible='visible', check_locator=None, expected_value=None, attribute_type=None, attempts=3, attempt_delay=2):
        # #@ Brief: Click an element and verify if the check_element is visible/invisible
        #
        # @param element: The element which is clicked
        # @param next_element: The expected element after clicking element
        # @param visible: next_element is 'visible' or 'invisible', default is 'invisible'
        # @param attempts: allowed retry times
        # @param attempt_delay: interval between attempts
        # @param check_locator: locator which used to check_attribute
        # @param expected_value: expected value of check_locator should be after clicked
        # @param attribute_type: type of check_locator which used to check 
        for attempts_remaining in range(attempts, -1, -1):
            if attempts_remaining > 0:
                try:
                    self.log.debug('Clicking element: "{0}" and wait element: "{1}"'.format(element, next_element))
                    self.click_element(element)
                    if visible == 'visible':
                        self.wait_until_element_is_visible(next_element, timeout=60)
                    else:
                        self.wait_until_element_is_not_visible(next_element)
                    if check_locator is not None:
                        self.check_attribute(check_locator, expected_value, attribute_type)
                    time.sleep(5)
                    self.log.debug('Element: "{0}" is {1}.'.format(next_element, visible))
                    break
                except:
                    self.log.info('Element: "{0}" did not click. Wait {1} seconds and retry. {2} attempts remaining.' \
                                  .format(element.name, attempt_delay, attempts_remaining))
                    time.sleep(attempt_delay)
            else:
                self.log.exception('Failed to popup element: "{0}"'.format(element))
                self.take_screen_shot('Click issue')

    def get_model_number_check(self, retry=3):
        model_number = ''
        while retry >= 0:
            try:
                model_number = self.get_model_number()
                return model_number
            except Exception as e:
                if retry == 0:
                    self.log.exception('Exception: {0}'.format(repr(e)))
                    self.log.debug('result : {0}'.format(model_number))
                self.log.info('Exception: {0} ,Retry {1} times for get model number'.format(repr(e), (3-retry)))
                retry -= 1

SettingsPageGeneralEnergySaver()