from testCaseAPI.src.testclient import TestClient
import re
import subprocess
import time
import commands
import os
import os.path
from global_libraries.testdevice import Fields
from ftplib import FTP
from ftplib import error_perm

import shutil
from nfspy.nfspy import NfSpy
#from global_libraries import CommonTestLibrary as ctl
#from subprocess import Popen

# private_share = ['PrivateShare1', 'PrivateShare2', 'PrivateShare3', 'PrivateShare4']
# 
# 
# shareDictionary = {'PublicShare_1':{'user1':'RW', 'user2':'RW'},
#                    'PublicShare_2':{'user1':'RW', 'user2':'RW'},
#                    'PublicShare_3':{'user1':'RW', 'user2':'RW'},
#                    'PublicShare_4':{'user1':'RW', 'user2':'RW'},
#                    'PrivateShareA_1':{'user1':'RO', 'user2':'RW'},
#                    'PrivateShareA_2':{'user1':'RO', 'user2':'RW'},
#                    'PrivateShareA_3':{'user1':'RW', 'user2':'D'},
#                    'PrivateShareA_4':{'user1':'D', 'user2':'RO'},
#                    'PrivateShareB_1':{'user1':'D', 'user2':'RO'},
#                    'PrivateShareB_2':{'user1':'RW', 'user2':'D'},
#                    'PrivateShareB_3':{'user1':'D', 'user2':'RO'},
#                    'PrivateShareB_4':{'user1':'RO', 'user2':'RW'}}
NETMASKS = ['255.255.0.0','255.255.128.0','255.255.192.0','255.255.224.0','255.255.240.0','255.255.248.0',
             '255.255.252.0','255.255.254.0',  '255.255.255.0']

class testscript(TestClient):

    def run(self):
        self.create_shares('nfsShare', force_webui=True)
        tmp_file = os.path.abspath(self.generate_test_file(10240))
        tmpfile = ''.join(tmp_file)
        self.write_files_to_nfs(files=tmpfile, share_name='nfsShare')
        
        '''
        self.delete_all_shares()   
        
        share_name='digital'
#         #self.create_user(username='user4')
        jpgFiles = self.generate_files_on_workspace(10240, 1)
#         #self.create_shares(share_name='user4')      
        self.create_shares(share_name, force_webui=True)
#         #self.execute('/usr/sbin/smbif -a {0};smbcv;smbwddb;smbcom'.format(share_name)) 
#         self.execute('/usr/sbin/smbif -a excel')
        response = self.write_files_to_smb(files=jpgFiles, share_name=share_name)
        '''
        '''
        self.log.info('######################## NFS Files Transfer START ##############################')         
        
        tmp_file = os.path.abspath(self.generate_test_file(10240))
        tmpfile = ''.join(tmp_file)
        self.log.info("Generate temporary file: {}".format(tmpfile))
        
        conn = self._nsc.connect_nfs()
        
        #conn.Write(service_name='nfsShare', file_obj=tmpfile)
        self.write_files_to_nfs(files=tmpfile, share_name='nfsShare')
        #conn.Write(service_name=share_name, path=target_path, file_obj=file_obj, timeout=timeout)
        
        self.log.info('######################## NFS Files Transfer END ##############################') 
        '''
        '''
        self.uut[Fields.internal_ip_address] = '192.168.6.204'
        raise Exception('exception')
    
        '''      
#         originalIP = '192.168.6.104'    
#         newIP = '192.168.6.204'
              
#         for netmask in NETMASKS:
#             self.uut[Fields.internal_ip_address] = originalIP
#             ifnameReturned = ''.join(self.get_xml_tags(self.get_network_configuration()[1],'ifname'))
#              
#             print 'Setting network to Static'
#             self.execute('setNetworkStatic.sh ifname={0} {1} {2}'.format(ifnameReturned, newIP, netmask), timeout=-1)
#             print ifnameReturned
#             print 'Static set'
#              
#             print 'Setting network to DHCP'
#             self.execute('setNetworkDhcp.sh ifname={0}'.format('bond0'), timeout= -1)
#             print ifnameReturned
#             print 'DHCP set'
#             

        
        
        #self.execute('setNetworkStatic.sh ifname=egiga0 {0} {1}'.format(newIP, netmask), timeout=-1)
        #self.access_webUI()
        #self.uut[Fields.internal_ip_address] = '192.168.6.204'
        
        #ifname = ''.join(self.get_xml_tags(self.get_network_configuration()[1],'ifname'))
        #print ifname
#         self.execute('setNetworkDhcp.sh ifname=egiga0', timeout= -1)
#         self.execute('setNetworkDhcp.sh ifname=bond0', timeout= -1)
        '''
        print self.uut[Fields.internal_ip_address]
        self.uut[Fields.internal_ip_address] = ''.join(self.get_xml_tags(self.get_network_configuration()[1],'ip'))
        print ''.join(self.uut[Fields.internal_ip_address])
        '''
        #self.execute('setNetworkDhcp.sh ifname=egiga0')
        #self.execute('setNetworkStatic.sh ifname=egiga0 {0} {1}'.format('192.168.6.104', '192.168.6.1'), timeout=-1)
        
        '''
        bootup_time = self.time_setting()
        self.bootupUnit(bootup_time)
        
        
        
#         self.log.info('Sending shutdown command to device')
#         # Let it go 1 1/2 times the maximum so we can complete the cycle even if it takes longer than expected
#         self.shutdown(timeout=shutdown_time * 2.5)
        
    def bootupUnit(self, max_boot_time):
        # Turn off the outlet
        self.power_off()
        time.sleep(1)

        # Now, wait for the system to come back up.
        start_time = time.time()
        # Turn it back on
        self.log.info('Starting unit up')
        self.power_on()
        self.log.info('Waiting for unit to boot up ...')
        
        while True:
            if self.is_ready():
                break

            if time.time() - start_time > max_boot_time * 1.5:
                raise Exception('Timed out waiting to boot')

        time.sleep(max_boot_time)
        self.access_webUI()

        
    def time_setting(self):
        # Get device's model then set reboot time values accoSenrdingly
        model_number = self.get_model_number()

        # Lightning ID
        if model_number == 'LT4A':
            reboot_cycle = 240
            shutdown_time = 35
            bootup_time = 205
            device = "Lightning"
            
        elif model_number == 'BNEZ':
            reboot_cycle = 210
            shutdown_time = 30
            bootup_time = 180
            device = "Sprite"
        
        else:
            reboot_cycle = 210
            shutdown_time = 30
            bootup_time = 180

            if model_number == 'BWZE':
                device = "Yellowstone"

            elif model_number == 'BZVM':
                device = "Zion"

            elif model_number == 'KC2A':
                device = "Kings Canyon"

            elif model_number == 'BWAZ':
                device = "Yosemite"

            elif model_number == 'BBAZ':
                device = "Aurora"

            elif model_number == 'GLCR':
                device = "Glacier"
            else:
                device = 'Unknown'

        self.log.info('setting reboot time: %d, shutdown time:%d, bootup time:%d for %s\n' % (
            reboot_cycle, shutdown_time, bootup_time, device))

        return bootup_time
        
        '''
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        '''
        
        self.power_off()
        time.sleep(1)

        # Now, wait for the system to come back up.
        start_time = time.time()
        # Turn it back on
        self.log.info('Starting unit up ...')
        self.power_on()
        self.log.info('Waiting for unit to boot ...')
        while True:
            if self.is_ready():
                break

            if time.time() - start_time > max_boot_time * 1.5:
                raise Exception('Timed out waiting to boot')
        '''

         
        
        '''
        file_size = 92
        file_units = 'mib'
        destination_share='user1'
        self.create_file(filesize=file_size,
                        dest_share_name=destination_share)
                        
        '''
        '''
        #self.copy_file(source_file=generated_files, dest_file='testfile_1', dest_share_name='user1')
        '''
        
        '''
        files=self.generate_files_on_workspace(1024*92, 1, 'bmp')
        
        from subprocess import Popen
        p = subprocess.Popen("/usr/local/sbin/setNetworkStatic.sh ifname=egiga0 192.168.6.204 255.255.255.0 192.168.6.1 &", shell=True)
        #p = Popen(['/usr/local/sbin/setNetworkStatic.sh ifname=egiga0 192.168.6.202 255.255.255.0 192.168.6.1']) # something long running
        # ... do other stuff while subprocess is running
        #p.terminate()
        #p = Popen(['/usr/local/sbin/setNetworkStatic.sh ifname=egiga0 192.168.6.202 255.255.255.0 192.168.6.1'])
        #print 'I am done'
        
        # ... do other stuff while subprocess is running
        #p.terminate()
      
    
        self.access_webUI()
        #if self.element_find(self.Elements.LOGIN_WD_LOGO):
        if self.element_find('Home'):
            self.log.info('Success: Able to reach WebUI using new IP Address.')
        else:
            self.log.error('Test Failed')
        '''
        '''
        self.log.info('Validating WebUI can be reached using new IP Address.')
        self.access_webUI()
        if self.is_visible('top_usericon', 30) == True:
            self.log.info('Success: Able to reach WebUI using new IP Address.')
        else:
            self.log.error('Test Failed')
        
        
        # Generate a jpg test file
        #jpgFiles = self.generate_files_on_workspace(92*1024, 1)
        self.access_browser('http://192.168.6.125')
        time.sleep(30)
        '''
        '''
        file_size = 92
        file_units = 'mib'
        destination_share='user1'
        generated_files = self.create_file(filesize=file_size,
                                           units=file_units,
                                           dest_share_name=destination_share)
        pass
        '''
        #self.copy_file(source_file=generated_files, dest_file='testfile_1', dest_share_name='user1')
        
        
        # Write the file to the unit as user1
        #response = self.write_files_to_smb(username='user1', files=jpgFiles, share_name='user1', timeout=90, delete_after_copy=True)
        '''
        self.log.info('Creating maximun number of users. Please Standby...')
        for i in range(1,5):
            user_name = 'user_carlos{0}'.format(i)
            #self._ssh.delete_user_account(user_name)
            self.create_user(username=user_name, fullname='user user')
        '''
        '''
        jpgFiles='/mnt/hgfs/aat_tests/testfile/file0.jpg'
        # Write the file to the unit as user1
        response = self.write_files_to_smb(username='user1', files=jpgFiles, share_name='user1', delete_after_copy=False)
        '''
        '''
        user = 'kimberly'
        self.execute('/usr/local/sbin/addUser.sh {0} false'.format(user))
        time.sleep(5)
        self.set_quota_all_volumes(user, 'user', 95)
        '''
        '''
        #self.create_user('NewUser')
        self.update_user(user='NewUser',fullname='New User')
        print self.get_user('newuser')[1].content
        #self._ssh.add_user_account(user='carlos', passwd='fituser', firstname='carlos')
        '''
        '''
        self.delete_user('usuario')
        self.create_user('usuario')
        
        self.create_shares('usuario')
        self.update_share_network_access(share_name='usuario', public_access=True) 
        
        # Generate a jpg test file
        jpgFiles = self.generate_files_on_workspace(1, 1)
        
        # Write the file to the unit as user1
        response = self.write_files_to_smb(username='usuario', files=jpgFiles, share_name='usuario', delete_after_copy=True)
        '''
        '''
        alert = self.get_xml_tags(self.get_alerts()[1], 'title')
        print alert
        '''
        '''
        firmwareVersion = self.get_firmware_version_number()
        print firmwareVersion[:4]
        '''
        '''
        speed = '100 Mbps'
        duplex = 'Full Duplex'
        
        presentSpeed = self.execute('dmesg | grep -i egiga0:')[1].split('\n')[-1].strip('.').lstrip()
        if speed and duplex in presentSpeed:
            print 'Found them'
        '''
        '''
        Lightning egiga0: link up, full duplex, speed 1 Gbps
                  egiga0: link up, full duplex, speed 100 Mbps
                  egiga0: link up, full duplex, speed 1 Gbps
            
        Yosemite: egiga0: link up, full duplex, speed 1 Gbps
                  egiga0: link up, full duplex, speed 100 Mbps
                  egiga0: link up, full duplex, speed 1 Gbps
        
     YellowStone: egiga0: link up, full duplex, speed 1 Gbps
                  egiga0: link up, full duplex, speed 100 Mbps
                  egiga0: link up, full duplex, speed 1 Gbps
       
     KingsCanyon: No link agregation, only link speed
                  egiga0: link up, full duplex, speed 1 Gbps
                  egiga0: link up, full duplex, speed 100 Mbps
                  egiga0: link up, full duplex, speed 1 Gbps
     
                  
        Sprite:   [  416.170954] igb 0000:00:14.1 egiga0: igb: egiga0 NIC Link is Up 100 Mbps Full Duplex, Flow Control: None
                  [  739.188891] igb 0000:00:14.1 egiga0: igb: egiga0 NIC Link is Up 1000 Mbps Full Duplex, Flow Control: None
      
       Aurora:    It has link agregation, but no link speed
       
                        
        Zion:     No link agregation ??????????
                  egiga0: link up, full duplex, speed 1 Gbps
                  egiga0: link up, full duplex, speed 100 Mbps 
                  egiga0: link up, full duplex, speed 1 Gbps
                  
       
       
       
                
    Black Ice:    No network speed
        
      Glacier:    No network speed
        
       

        '''
        '''        
        
#         returned_duplex = result.split(',')[-2]
#         returned_speed = result.split(',')[-1]
#         link_speed = (returned_duplex + returned_speed).lstrip()
#         numberDrives = self.uut[self.Fields.number_of_drives]
#         print numberDrives
        
     
        volume=self.get_volumes()
        print volume
        
        numberVolumes = len(self.get_xml_tags(self.get_volumes()[1], 'volume_id'))
        print numberVolumes
        '''
        '''
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')
        if modelNumber in ('BWAZ', 'BBAZ', 'KC2A','BZVM', 'LT4A', 'BNEZ', 'BWZE'):
            print "1 or 2 bay unit"  
        else:
            print "single unit"  
        '''
        '''
        model = self.get_model_number()
        print model
        '''
        '''
        link='Active Backup'
        speed= 'full duplex speed 1 Gbps'
        result = self.execute('dmesg | grep -i bps')[1].split('\n')[-1].strip('.').lstrip()
        returned_duplex = result.split(',')[-2]
        returned_speed = result.split(',')[-1]
        link_speed = (returned_duplex + returned_speed).lstrip()
            
        if speed == link_speed:
            self.log.info('SUCESS: the speed is back to {0}'.format(link_speed))
        else:
            self.log.error('Failed: the speed is not as expected')
        '''
        
#         returned_duplex = result.split(',')[-2]
#         returned_speed = result.split(',')[-1]
#         link_speed = (returned_duplex + returned_speed).lstrip()
#         print link_speed
        
        '''
        self.get_to_page('Settings')
        time.sleep(2)
        self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(3)
        '''
        
        
        
        #self.get_model_number()
        #rint os.path.basename('/mnt/www/file0.jpg')
        #print self.get_all_users()[1]
        '''
        self.input_text(locator, text, do_login)
        
        file = self.generate_test_files(kilobytes=1, file_count=1)
        
        print file
        self.write_files_to_smb(files=file)
        
        print time.strftime('%m/%d/%Y', time.gmtime(os.path.getmtime(file[0])))
        
        formatVolume = 'Volume_2'
        time.sleep(2)
        self.get_to_page('Settings')    
        time.sleep(2)
        self.wait_until_element_is_visible('settings_utilities_link')
        self.click_element('settings_utilities_link')
        time.sleep(2)
        if formatVolume == 'Volume_2':
                self.click_element('settings_utilitiesFormatDisk_volume')
                self.click_element("xpath=(//a[contains(text(),'Volume_2')])[2]")
    
        output = self.run_on_device('ps -ef |grep mke2fs')
        if 'mke2fs' in output:
            print 'it is alive'
        #self.is_running('firefox')
        
        output=commands.getoutput('ps -A')
        print output
        if 'firefox' in output:
            self.log.info('It is alive')
        
    def is_running(self, process):
        s = subprocess.Popen(["ps", "axw"],stdout=subprocess.PIPE)
        for x in s.stdout:
            if re.search(process, x):
                print 'It is alive'
                return True
        return False    
        
        # Remove temporary folder
        path=os.getcwd()
        if os.path.basename(path).startswith('tmp'):
            os.chdir('..')
        subDirs = [s for s in os.listdir(os.getcwd()) if 'tmp' in s]
        for tmpDir in subDirs:
            shutil.rmtree(tmpDir)
        #share_number = user_name.replace("user", "")
        share_number = share_name.replace(str(share_name),"")
        share_link = 'users_{0}{1}_link'.format(access_text, share_number) #users_rw4_link
        user_link = 'users_user_{0}'.format(user_name) #users_user_user2
        time.sleep(2)
        self.find_element_in_sublist(user_name, 'user')
        self.driver.click_element(user_link)
        time.sleep(2)
        self.driver.click_element(share_link)
        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)
        #self.clear_cache()
        '''
        #self.create_user(username='user1')
        #generated_files = self.generate_test_files(kilobytes=1, file_count=1)
        #self.write_files_to_smb(username='user1', files=generated_files)
        
        '''
        self.read_files_from_smb(files='file5.jpg', share_name='PublicShare_1', delete_after_copy=False)
        self.write_files_to_smb(files=generated_files[8], share_name='PublicShare_1', delete_after_copy=False)
        self.read_files_from_smb(files='file8.jpg', share_name='PublicShare_1', delete_after_copy=False)
        self.write_files_to_smb(files=generated_files[9], share_name='PublicShare_1', delete_after_copy=False)
        self.read_files_from_smb(files='file9.jpg', share_name='PublicShare_1', delete_after_copy=True)
        
        self.get_to_page('Apps')
        self.click_element('css=#http_downloads > a > span._text')
        self.click_element('//td[2]/div/div[2]/table/tbody/tr/td[2]/span/input')
        self.input_text('css=#f_URL', 'http://go.microsoft.com/fwlink/?LinkId=324638',False)
        self.click_button('But_HDownloads_Test')
        time.sleep(10)
        self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        '''
        
        
        
        #self._nsc.connect_smb(username='admin', password='', timeout=3)
        '''
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

        if modelNumber in ('BWAZ', 'BBAZ') and (self.get_raid_mode() <> 'JBOD'):  
            self.log.info('Configuring JBOD...')
            self.configure_jbod()   
        elif modelNumber in ('LT4A', 'BNEZ', 'BWZE') and (self.get_raid_mode() <> 'JBOD'):
            self.log.info('Configuring JBOD...')
            self.configure_jbod()
        else: 
            log.info('No raid configuration needed')
        
   
        
        # If already JBOD (4 Volumes) then skip configure_jbod
        #print self.get_raid_mode()
        #if self.get_raid_mode() <> 'JBOD':
        #    self.configure_jbod()
       
        #print shareDictionary['PrivateShareB_3']['user2']
        
                
        #self.find_element_in_sublist('group1','group')
        #self.click_element(locator='<div class=\"name\">PrivateShare3</div>')
        #generated_files = self.generate_test_file(kilobytes=1)
        #self.local_login(username='user2', password='fituser')
        #status = self.write_files_to_smb(username='user2', password='fituser', files=generated_files,share_name='PrivateShareA_1')
        #self._sel.assign_user_to_share(user_name='user2', share_name='PrivateShareB_2', access_type='rw')
        #share_number = re.search(r'(\d+)', 'PrivateShare3')
        
      
        self.log.info('######################## Group Quota and Access Privilege ########################')
        self._create_shares(private_share, public=False)
        self._create_group_set_quota(group_name='group1', quota=10, share_name = private_share[0])
        self._create_group_set_quota(group_name='group2', quota=15, share_name = private_share[1])
        self._create_group_set_quota(group_name='group3', quota=20, share_name = private_share[2])
        self._create_group_set_quota(group_name='group4', quota='', share_name = private_share[3])
        
        share_number = int(filter(str.isdigit, 'PrivateShare3'))
        print share_number
        #print private_share[0]
        #print private_share[3]
        
        sharename = 'Public'
        generated_file = self.generate_test_files()
        self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
        self.read_files_from_smb(performance=True)
        
        response = self.create_user_quota(user_name='user1', size_of_quota=20, unit_size='GB')
        print response
        
        def get_group_share_access(self):
            self.find_element_in_sublist(name, group_user_or_share='group')
        
        '''  
         
            
testscript()

