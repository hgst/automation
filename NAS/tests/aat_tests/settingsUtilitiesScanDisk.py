'''
Create on April 20, 2015

@Author: lo_va

Objective: Verify scan disk function from WebUI.
Test ID: 117536
'''
import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E

class settingsUtilitiesScanDisk(TestClient):
    
    def run(self):
        
        self.set_web_access_timeout('20')
        testname = 'Scan Disk Test'
        self.start_test(testname)
        self.get_to_page('Settings')
        self.click_element(E.SETTINGS_UTILITY)
        model_number = self.get_model_number()
        if not 'GLCR' in model_number:
            self.click_element(E.SETTINGS_UTILITY_SCANDISK_VOL_SELECT)
            self.click_element('link=Volume_1')
        self.click_element(E.SETTINGS_UTILITY_SCANDISK)
        self.click_wait_and_check('popup_apply_button')
        self.wait_until_element_is_visible('scandsk_percent')
        while self.is_element_visible('scandsk_percent'):
            if not self.is_element_visible('ScanDskDiag_res', 0):
                try:
                    scan_disk_percent = self.get_text('scandsk_percent')
                    self.log.info('Scan status: {}'.format(scan_disk_percent))
                    time.sleep(2)
                except:
                    self.log.warning('Sometimes scandisk percent will disappear immediately. Skip get_text in this situation')
                    break
            else:
                break
        self.wait_until_element_is_visible('ScanDskDiag_res')
        scan_disk_result = self.get_text('ScanDskDiag_res')
        if 'successfully' in scan_disk_result:
            self.pass_test(testname)
        else:
            self.fail_test(testname)
        self.click_element('settings_utilitiesScanDiskFinish_button')
        self.wait_icon_loading()
        
    def tc_cleanup(self):
        self.get_to_page('Settings')
        self.click_element(E.SETTINGS_UTILITY)
        if self.is_element_visible('settings_utilitiesScanDiskFinish_button', 20):
            self.click_element('settings_utilitiesScanDiskFinish_button')
        self.set_web_access_timeout('5')
            
    def wait_icon_loading(self):
        self.wait_until_element_is_visible('icon_loading', 10)
        timer = Stopwatch(timer=100)
        timer.start()
        while True:
            if not self.is_element_visible('icon_loading'):
                self.log.info('Loading finished')
                break
            else:
                if timer.is_timer_reached():
                    raise Exception.ActionTimeout('Loading Timeout')
                else:
                    time.sleep(1)
    
settingsUtilitiesScanDisk()