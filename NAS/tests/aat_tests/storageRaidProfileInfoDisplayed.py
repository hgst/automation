""" Storage Page - RAID Profile info is displayed (MA-169)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?pId=50&view=details&nTP=117446&pltab=steps)
                1) Open webUI to login as system admin
                2) Click on the Storage category
                3) Select RAID
                4) Verify the RAID profile info is displayed
    
    Automation: Full
    
    Not supported product:
        Glacier (Does not have Storage page)
    
    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon
    
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.201)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as Elem
from selenium.common.exceptions import StaleElementReferenceException
from seleniumAPI.src.seleniumclient import ElementNotReady
import time
import os
import inspect

products_info = {
        'LT4A': 'Lightning',      
        'BNEZ': 'Sprite',
        'BWZE': 'Yellowstone',
        'BWAZ': 'Yosemite',
        'BBAZ': 'Aurora',
        'KC2A': 'KingsCanyon',
        'BZVM': 'Zion',
        'GLCR': 'Glacier',
        'BWVZ': 'GrandTeton',
}

picturecounts = 0

class storageRaidProfileInfoDisplayed(TestClient):
    
    def run(self):
        self.log.info('######################## Storage Page - RAID Profile info is displayed (MA-169) TEST START ##############################')
        try:
            numOfBays = self.uut[Fields.number_of_drives]
            numOfDrives = self.get_drive_count()
            # productName = products_info[self.get_model_number()]
            productName = self.uut[Fields.product]

            # Glacier does not have Storage page 
            if productName == 'Glacier':
                self.log.info(">>>>>> UN-SUPPORTED PRODUCT: {}, (no storage page) <<<<<<".format(productName))
                return
            self.log.info("Product: {}, Supported Bays: {}, Current installed drives: {}".format(productName, numOfBays, numOfDrives))
            self.get_to_page('Storage')
            self.click_wait_and_check('nav_storage_link', 'storage_raid_link', visible=True)
            self.click_wait_and_check('raid', 'css=#raid.LightningSubMenuOn', visible=True)
            # Verify RAID Profile information
            self.log.info("== RAID Profile Information ==")
            self.verifyRAIDHealth()
            self.verifyAutoRebuild()
            self.log.info("== RAID Volume Information ==")
            # self.verifyRAIDVolume()
            self.verifyRAIDVolumeSoup()
        except Exception as e:
            self.log.error("FAILED: Storage Page - RAID Profile info is displayed (MA-169) Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: Storage Page - RAID Profile info is displayed (MA-169) Test PASS!!!")
            
        self.log.info('######################## Storage Page - RAID Profile info is displayed (MA-169) TEST END ##############################')

    def verifyRAIDHealth(self):
        """
            Verify the RAID Health         
        """
        count = 3
        while count > 0:
            try:
                self.wait_until_element_is_visible("//div[@id=\'div_raid_desc\']/table/tbody/tr/td[1]/span", timeout=5)
                healthTitleText = self.get_text("//div[@id=\'div_raid_desc\']/table/tbody/tr/td[1]/span")
                self.wait_until_element_is_visible("raid_healthy")
                raidHealthText = self.get_text('raid_healthy')
                self.wait_until_element_is_visible('raid_healthy_desc')
                raidHealthDescText = self.get_text('raid_healthy_desc')
                self.log.info("{}: {}, {}".format(healthTitleText, raidHealthText, raidHealthDescText))
            except (StaleElementReferenceException, ElementNotReady) as e:
                self.log.info("WARN: RaidHealth {} retry, due to exception: {}".format(4-count, repr(e)))
                self.close_webUI()
                self.get_to_page('Storage')
                self.click_wait_and_check('nav_storage_link', 'storage_raid_link', visible=True)
                self.click_wait_and_check('raid', 'css=#raid.LightningSubMenuOn', visible=True)
                count -= 1
                if count == 0:
                    self.log.error("ERROR: Reach 3 retries due to exception: {}!!".format(repr(e)))
                    self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                    break
                continue
            except Exception as e:
                self.log.error("ERROR: RAID Health information is not properly displayed! Exception: {}".format(repr(e)))
                self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            else:
                break

    def hasAutoRebuild(self):
        """
            Verify if it is not JBOD, Spanning and R0
            RAID levels:
                    11: JBOD
                    12: spanning
                     0: RAID 0
                     1: RAID 1
                     5: RAID 5
                    10: RAID 10               
        """
        rebuildRaids = ('1', '5', '10')
        supportedRAID = {'11': 'JBOD', '12': 'Spanning', 
                         '0': 'R0', '1': 'R1', 
                         '5': 'R5', '10': 'R10'}

        # Get RAID level
        result = self.get_RAID_drives_status()[1]
        raidLst = self.get_xml_tags(xml_content=result.content, tag='raid_mode')
        while '<raid_mode />' in raidLst:
            raidLst.remove('<raid_mode />')
        raidLevel = raidLst[0] or '0'

        self.log.info("RAID Level: {}".format(supportedRAID[raidLevel]))
        if raidLevel in rebuildRaids:
            return True
        else:
            return False
            
    def verifyAutoRebuild(self):
        """
            Verify the Auto-Rebuild when RAID 1, 5, 10 are used
            
            RAID levels:
                    11: JBOD
                    12: spanning
                     0: RAID 0
                     1: RAID 1
                     5: RAID 5
                    10: RAID 10
        """
        checkAutoRebuild = self.hasAutoRebuild()
        if checkAutoRebuild:
            count = 3
            while count > 0:
                try:
                    self.wait_until_element_is_visible("//div[@id=\'div_raid_desc\']/table/tbody/tr[3]/td[1]/span", timeout=5)
                    rebuildTitleText = self.get_text("//div[@id=\'div_raid_desc\']/table/tbody/tr[3]/td[1]/span")
                    self.wait_until_element_is_visible('css=#storage_raidAutoRebuild_switch+span .checkbox_container', timeout=5)
                    rebuildStatusText = self.get_text('css=#storage_raidAutoRebuild_switch+span .checkbox_container')
                    self.log.info("{}: {}".format(rebuildTitleText, rebuildStatusText))
                except (StaleElementReferenceException, ElementNotReady) as e:
                    self.log.info("WARN: AutoRebuild {} retry, due to exception: {}".format(4-count, repr(e)))
                    self.close_webUI()
                    self.get_to_page('Storage')
                    self.click_wait_and_check('nav_storage_link', 'storage_raid_link', visible=True)
                    self.click_wait_and_check('raid', 'css=#raid.LightningSubMenuOn', visible=True)
                    count -= 1
                    if count == 0:
                        self.log.error("ERROR: Reach 3 retries, due to exception: {}!!".format(repr(e)))
                        self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                        self.takeScreenShot(prefix='verifyRAIDRebuildRetry')
                        break
                    time.sleep(2)
                    continue
                except Exception as e:
                    self.log.error("ERROR: AutoRebuild information is not properly displayed! Exception: {}".format(repr(e)))
                    self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                    raise
                else:
                    break
        else:
            self.log.info("SKIP: No Auto Rebuild information to check due to not in RAID 1, 5 and 10")

    def verifyRAIDVolumeSoup(self):
        """
            Verify RAID volumes
        """
        result = self.get_volumes()[1]
        volumesList = self.get_xml_tags(xml_content=result.content, tag='base_path')
        # Remove non Volume_X shares
        for i in volumesList:
            if 'Volume' not in i:
                volumesList.remove(i)
        vols = len(volumesList)
        if not vols:
            vols = 1
        from BeautifulSoup import BeautifulSoup
        try:
            page_contents = BeautifulSoup(self._sel.driver.get_source())
            raid_vol = {}
            for i in range(1, vols+1):
                tr_row = page_contents.find("tr", {"id": "row{0}".format(i)})
                divs = tr_row.findAll("div")
                row_list = []
                for div in divs:
                    row_list.append(div.contents[0])
                raid_vol[row_list[0]] = ", ".join(row_list[1:])
            for k, v in raid_vol.items():
                self.log.info("{} : {}".format(k, v))
        except Exception as e:
            self.log.error("ERROR: RAID volume information is not properly displayed! Exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            self.takeScreenShot(prefix='verifyRAIDVolume')
            raise

    def verifyRAIDVolume(self):
        """
            Verify RAID volumes
        """
        result = self.get_volumes()[1]
        volumesList = self.get_xml_tags(xml_content=result.content, tag='base_path')
        # Remove non Volume_X shares
        for i in volumesList:
            if 'Volume' not in i:
                volumesList.remove(i)
        vols = len(volumesList)
        if not vols:
            vols = 1
        count = 3
        while count > 0:
            try:
                for i in range(1, vols+1):
                    volNumLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[1]/div".format(i)
                    self.wait_until_element_is_visible(volNumLocator, timeout=5)
                    volNumText = self.get_text(volNumLocator)
                    volRaidLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[2]/div".format(i)
                    self.wait_until_element_is_visible(volRaidLocator, timeout=5)
                    volRaidText = self.get_text(volRaidLocator)
                    volSizeLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[3]/div".format(i)
                    self.wait_until_element_is_visible(volSizeLocator, timeout=5)
                    volSizeText = self.get_text(volSizeLocator)
                    volStatusLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[4]/div".format(i)
                    self.wait_until_element_is_visible(volStatusLocator, timeout=5)
                    volStatusText = self.get_text(volStatusLocator)
                    self.log.info("{}: {}, {}, {}".format(volNumText, volRaidText, volSizeText, volStatusText))
                    time.sleep(1)
            except (StaleElementReferenceException, ElementNotReady) as e:
                self.log.info("WARN: RaidVolume {} retry, due to exception: {}".format(4-count, repr(e)))
                # self.close_webUI()
                # self.get_to_page('Storage')
                # self.click_wait_and_check('nav_storage_link', 'storage_raid_link', visible=True)
                # self.click_wait_and_check('raid', 'css=#raid.LightningSubMenuOn', visible=True)
                count -= 1
                if count == 0:
                    self.log.error("ERROR: Reach 3 retries, due to exception: {}!!".format(repr(e)))
                    self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                    break
                # time.sleep(5)
                continue
            except Exception as e:
                self.log.error("ERROR: RAID volume information is not properly displayed! Exception: {}".format(repr(e)))
                self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                self.takeScreenShot(prefix='verifyRAIDVolume')
                raise
            else:
                break

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            filename = 'Error_{}.png'.format(picturecounts)
        else:
            prefix = os.path.splitext(__file__)[0]
            filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

storageRaidProfileInfoDisplayed()