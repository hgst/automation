""" (MA-178) Apps page - P2P Downloads info is displayed

    @Author: yang_ni
    Procedure:
    http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=34442&execView=execDetails&view=details&tdetab=0&pltab=steps&fiTpId=-1&pId=81&nTP=270457&etab=1

    1) Open WebUI and login as System admin
    2) Click on the Apps category
    3) Select P2P Downloads
    4) Click configure link to set the date and time
    5) Enter info for Torrent URL or from torrent file
    6) Verify that the file is downloaded (File is downloaded)

    Automation: Full

    Not supported product:
        Glacier

    Support Product:
        Lightning
        YellowStone
        Yosemite
        Sprite
        Aurora
        Grand Teton
        Kings Canyon
        Zion

    Defect:
        N/A

    Test Status:
        Local Lightning: PASS (FW: 2.10.302)

"""

from testCaseAPI.src.testclient import TestClient
from datetime import datetime
import time

#torrent_link = 'http://10.136.139.17/torrents/raspbian.torrent'
torrent_link = 'http://10.136.139.26/nick.torrent'

class appsPageP2PdownloadsInfoIsDisplayed(TestClient):
    def run(self):
        self.log.info('############### (MA-178) Apps page - P2P Downloads info is displayed TESTS START ###############')
        if self.get_model_number() in ('GLCR',):
            self.log.info("=== Glacier de-feature does not support P2P downloads, skip the test ===")
            return
        try:
            today = self.get_today_name()
            self.enable_p2p_download()
            self.configure_p2p_download(day=today, start=0, end=24)
            self.clear_p2p_live_torrent()
            self.set_p2p_download_torrent()
            self.wait_for_p2p_download()
            #self.file_exist_check(foldername='complete', filename='2013-07-26-wheezy-raspbian.zip')
            self.file_exist_check(foldername='complete', filename='nick.txt')
        except Exception as e:
            self.log.error("Test Failed: (MA-178) Apps page - P2P Downloads info is displayed, exception: {}".format(repr(e)))
        finally:
            self.log.info("====== Clean Up section ======")
            self.log.info('############### (MA-178) Apps page - P2P Downloads info is displayed TESTS END ###############')

    def get_today_name(self):
        try:
            now = datetime.now()
            day = now.strftime("%A")[:3]
            self.log.info("Now is {}".format(day))
            return day
        except Exception as e:
            self.log.exception("ERROR: fail to get today name, exception: {0}".format(repr(e)))

    def enable_p2p_download(self):
        try:
            self.get_to_page('Apps')
            self.click_wait_and_check('nav_apps_link', 'apps_p2pdownloads_link', visible=True)
            self.click_wait_and_check('apps_p2pdownloads_link',
                                      'css=#apps_p2pdownloads_switch+span .checkbox_container', visible=True)

            # Switch P2P to ON
            p2p_status = self.get_text('css=#apps_p2pdownloads_switch+span .checkbox_container')
            if p2p_status == 'OFF':
                self.click_wait_and_check('css=#apps_p2pdownloads_switch+span .checkbox_container',
                                          'css=#apps_p2pdownloads_switch+span .checkbox_container .checkbox_on', visible=True)
            else:
                self.log.info("P2P is already set to ON")

            # Click configure
            self.click_wait_and_check('apps_p2pdownloadsConfigure_link',
                                      'apps_p2pdownloadsConfigureCancel1_button', visible=True)
            # Auto
            auto_status = self.get_text('css=#apps_p2pdownloadsConfigureAutoDownload_switch+span .checkbox_container')
            if auto_status == 'OFF':
                self.click_wait_and_check('css=#apps_p2pdownloadsConfigureAutoDownload_switch+span .checkbox_container',
                                          'css=#apps_p2pdownloadsConfigureAutoDownload_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            else:
                self.log.info("P2P auto download is already set to ON")

            # Port
            port_status = self.get_text('css=#apps_p2pdownloadsConfigurePort_switch+span .checkbox_container')
            if port_status == 'OFF':
                self.click_wait_and_check('css=#apps_p2pdownloadsConfigurePort_switch+span .checkbox_container',
                                          'css=#apps_p2pdownloadsConfigurePort_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            else:
                self.log.info("P2P download port switch is already set to ON")
            self.click_wait_and_check('apps_p2pdownloadsConfigureNext1_button',
                                      'apps_p2pdownloadsConfigureNext2_button', visible=True)
            self.click_wait_and_check('apps_p2pdownloadsConfigureNext2_button',
                                      'apps_p2pdownloadsConfigureNext3_button', visible=True)
            # Clear bandwidth to empty
            self.input_text_check('apps_p2pdownloadsConfigureDownloadRate_text', '', do_login=False)
            self.input_text_check('apps_p2pdownloadsConfigureUploadRate_text', '', do_login=False)
            # Apply
            self.click_wait_and_check('apps_p2pdownloadsConfigureNext3_button', visible=False)
        except Exception as e:
            self.log.error("ERROR: fail to enable p2p download, exception: {}".format(repr(e)))

    def configure_p2p_download(self, day='Sun', start=0, end=24):
        """
        :param start: 0 ~ 24
        :param end: 24 ~ 0
        """
        if start < 0 or start > 24:
            self.log.error("ERROR: Invalid start time: {}".format(start))
            return
        if end > 24 or end < 0:
            self.log.error("ERROR: Invalid end time: {}".format(end))
            return
        if start > end:
            self.log.error("ERROR: Start time should less then end time. start={0}, end={1}".format(start, end))
            return

        self.log.info("Click P2P configure")
        self.click_wait_and_check('apps_p2pdownloadsschedule_button',
                                  'apps_p2pdownloadsConfigureCancel0_button', visible=True)
        day_dict = {
            'Sun': 7,
            'Mon': 1,
            'Tue': 2,
            'Wed': 3,
            'Thu': 4,
            'Fri': 5,
            'Sat': 6,
        }

        # Clean
        while self.is_element_visible('img_off_{}'.format(day_dict[day]), wait_time=10):
            self.log.info("Schedule for {0} is all day off, going to turn on".format(day))
            self.click_element('img_off_{}'.format(day_dict[day]))
        self.log.info("Adjust the time to all day on before new setting applied")
        self.drag_and_drop_by_offset("//span[@id=\'SliderMain{}\']//div[@class=\'jslider-pointer\']".format(day_dict[day]), -200, 0)
        self.drag_and_drop_by_offset("//span[@id=\'SliderMain{}\']//div[@class=\'jslider-pointer jslider-pointer-to\']".format(day_dict[day]), 200, 0)

        # Set left
        self.log.info("=== Set start time ===")
        left = self.get_schedule_value(day=day)[0]
        while left < start:
            self.drag_and_drop_by_offset("//span[@id=\'SliderMain{}\']//div[@class=\'jslider-pointer\']".format(day_dict[day]), 5, 0)
            left = self.get_schedule_value(day=day)[0]
            if left == start:
                self.log.info("Succeed to set the start time: {}".format(left))

        # Set right
        self.log.info("=== Set end time ===")
        right = self.get_schedule_value(day=day)[1]
        while right > end:
            self.drag_and_drop_by_offset("//span[@id=\'SliderMain{}\']//div[@class=\'jslider-pointer jslider-pointer-to\']".format(day_dict[day]), -5, 0)
            right = self.get_schedule_value(day=day)[1]
            if right == end:
                self.log.info("Succeed to set the end time: {}".format(right))

        # Apply settings
        self.log.info("Applying the schedule downloads setting...")
        self.click_wait_and_check('apps_p2pdownloadsConfigureNext0_button', visible=False)
        self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING, time_out=10)

    def get_schedule_value(self, day='Sun'):
        day_dict = {
            'Sun': 7,
            'Mon': 1,
            'Tue': 2,
            'Wed': 3,
            'Thu': 4,
            'Fri': 5,
            'Sat': 6,
        }
        try:
            element = self.element_find("//input[@id=\'P2P_Slider{}\']".format(day_dict[day]))
            hours_text = element.get_attribute('value')
            self.log.info("hours: {}".format(hours_text))
            start, end = hours_text.strip().split(';')
        except Exception as e:
            self.log.exception("ERROR: fail to get the schedule time, exception: {0}".format(repr(e)))
        else:
            return int(start), int(end)

    def clear_p2p_live_torrent(self):
        if self.is_element_visible('p2p_downloads_list', wait_time=15):
            self.log.info("Clean existing seeds")
            self.click_wait_and_check("//table[@id=\'p2p_downloads_list\']//div[@class=\'del TooltipIcon\']",
                                      'popup_apply_button', visible=True)
            self.click_wait_and_check('popup_apply_button', visible=False)
        else:
            self.log.info("Empty Torrents")

    def set_p2p_download_torrent(self):
        global torrent_link
        try:
            self.input_text_check('f_torrent_url', str(torrent_link), do_login=False)
            self.click_wait_and_check('apps_p2pdownloadsAddURL_button',
                                      'apps_p2pdownloadsAddURLClose_button', visible=True)
            self.wait_until_element_is_visible('tr_p2p_download_get_url_status_desc', timeout=15)
            status_text = self.get_text('tr_p2p_download_get_url_status_desc')
            if 'Successfully' in status_text:
                self.click_wait_and_check('apps_p2pdownloadsAddURLClose_button', visible=False)
            else:
                raise Exception('Torrent file added failure')
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING, time_out=10)
        except Exception as e:
            self.log.error("ERROR: Fail to add P2P torrent link, exception: {0}".format(repr(e)))

    def wait_for_p2p_download(self):
        self.log.info("Waiting for file download and switch to seeding...")
        download_status = None
        count = 0
        while download_status != 'Seeding':
            if count >= 60:
                self.log.error("ERROR: Reach the maximum wait for file download")
                break
            download_status = self.get_text("//table[@id=\'p2p_downloads_list\']//tr[@id=\'row1\']/td[3]/div")
            self.log.info("Status: {0}".format(download_status))
            time.sleep(10)
            count += 1

    def file_exist_check(self, foldername='complete', filename='nick.txt'):
        files = self.run_on_device('ls /shares/P2P/{0}'.format(foldername))
        file_list = files.split()
        if filename in file_list:
            self.log.info("Succeed to download file: {0}".format(filename))
        else:
            self.log.error("Fail to download file: {0}".format(filename))
            self.log.info("Available files: {}".format(','.join(file_list)))

appsPageP2PdownloadsInfoIsDisplayed()