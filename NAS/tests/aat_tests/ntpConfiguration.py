"""Created on Jan 26, 2015

@Author: lee_e, hoffman_t

1.Reboot first to make sure /etc/hosts is correct, 
  Due to if system ever be terminated during this test cases then will make /etc/hosts is uncorrectly
2.Add close_webUI() to ntp_config for system can update to new NTP server correctly

"""

import time

from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
import requests

NTP_SERVER_LIST = ['asia.pool.ntp.org', 'europe.pool.ntp.org', 'north-america.pool.ntp.org', 'oceania.pool.ntp.org',
                   'south-america.pool.ntp.org']
HOUR_OFFSET = 5
time_out = 60

class WDNtpError(Exception):
    pass

class NTPConfiguration(TestClient):
    def run(self):
        self.save_hosts_file()
        self.change_time_format(format=24)
        self.update_ntp_config(enable=1)
        self.ntpconfiguration()
       
    def tc_cleanup(self):
        self.change_time_format(format=12)
        self.reset_hosts_file()
        self.delete_ntp_user_server()
        
    def save_hosts_file(self):
        self.execute('cp /etc/hosts /etc/hosts.ntpconfiguration')

    def reset_hosts_file(self):
        self.execute('cp /etc/hosts.ntpconfiguration /etc/hosts')
        self.execute('rm /etc/hosts.ntpconfiguration')

    def ntpconfiguration(self):
        self.log.info('********* Start to Test ************')        

        for i in range(0, len(NTP_SERVER_LIST)):
            self.update_ntp_server(enable=1, ntp_server=NTP_SERVER_LIST[i])
            current_ntp_hour_value, year, month, day = self.get_current_datetime()
            self.update_hosts_file(server=NTP_SERVER_LIST[i], eliminating=False)
            self.update_ntp_server(enable=0, ntp_server=NTP_SERVER_LIST[i])
            target_time, set_hour = self.set_time(hour_offset=HOUR_OFFSET)
            result = self.check_time(chk_hour=set_hour)
            if result:
                self.log.info('PASS : set time pass')
            else:
                raise WDNtpError('Failed to set time. Test failed.')
            self.update_ntp_server(enable=1, ntp_server=NTP_SERVER_LIST[i])
            time.sleep(5)
            result = self.check_time(chk_hour=set_hour)
            if result:
                self.log.info('PASS: time still not sync with ntp')
            else:
                self.log.error('FAIL: time shouldn\'t be updated')
            self.update_hosts_file(server=NTP_SERVER_LIST[i], eliminating=True)
            retry = 3
            while retry > 0:
                ## Switch ntp for get /etc/hosts file which been updated
                self.update_ntp_server(enable=0, ntp_server=NTP_SERVER_LIST[i])
                self.update_ntp_server(enable=1, ntp_server=NTP_SERVER_LIST[i])
                result = self.check_time(chk_hour=set_hour)
                if result:
                    self.log.info('Retry {0} times for sync with ntp'.format(3-retry))
                    retry -= 1
                else:
                    self.log.info('PASS: time sync with ntp')
                    break
            if result:
                self.log.error('FAIL: time does not sync with ntp')                
        
        try:
            self.log.debug('*** Testing status: serial connection ***')
            # Disable NTP
            self.update_ntp_server(enable=0, ntp_server=NTP_SERVER_LIST[len(NTP_SERVER_LIST)-1])
            target_time, set_hour = self.set_time(hour_offset=HOUR_OFFSET)
            result = self.check_time(set_hour)
            if result:
                self.log.info('Time set to ' + self.get_time()[0])
            else:
                raise WDNtpError('Failed to set time. Test failed.')
            # Re-enable NTP
            self.update_ntp_server(enable=1, ntp_server=NTP_SERVER_LIST[len(NTP_SERVER_LIST)-1])
            new_time, year, month, day = self.get_current_datetime()
            result = self.check_time(new_time)
            if result:
                self.log.info('PASS: time sync with ntp')
            else:
                self.log.error('FAIL: time should update')
            # Bring down and up to simulate network cable be disconnected and connected behavior
            self.network_config('down')
            # Simulate lose network for one minute
            time.sleep(60)
            self.network_config('up')
            current_hour, year, month, day = self.get_current_datetime()
            if self.is_device_time_correct(current_hour):
                self.log.info('PASS: sync to NTP server')
            else:
                self.log.error('Failed to reset time with NTP')
        except Exception as e:
            self.log.error('FAIL:{}'.format(repr(e)))   
           
        self.log.info('********* Test End ************')
        
    def is_device_time_correct(self, target_hour, timeout=60):
        self.log.info('Checking to see if device hour is set to ' + str(target_hour))
        timer = Stopwatch(timer=timeout)
        timer.start()

        # Keep looping until the hour matches, or the timeout is reached.
        while str(target_hour) != str(self.get_current_datetime()[0]):
            if timer.is_timer_reached():
                return False
            time.sleep(1)
        return True
    
    def update_ntp_server(self, enable, ntp_server):
        self.log.info('****** NTP Server update to %s *******' % ntp_server)
        self.update_ntp_config(enable=enable, ntp_server=ntp_server)
    
    def delete_ntp_user_server(self):
        self.log.info('**** Delete NTP User Server')
        self.update_ntp_config(enable=1)
        
    def update_ntp_config(self, enable, ntp_server=''):
        self.log.info('****** NTP config switch to %s *******' % enable)
        enable_table = {1: 'true', 0: 'false'}
            
        ntp_cgi_cmd = 'cgi_ntp_time&f_ntp_enable=%s&f_ntp_server=%s' % (enable, ntp_server)
        self.send_cgi(cmd=ntp_cgi_cmd)
        
        status_code, result = self.get_date_time_configuration()
        ntpservice = self.get_xml_tags(result, 'ntpservice')
        ntpserver = self.get_xml_tags(result, 'ntpsrv_user')
        self.log.debug('current ntpservice in restcall is {0}'.format(ntpservice))
        self.log.debug('current ntpserver in restcall is {0}'.format(ntpserver))

        if enable_table[enable] in ntpservice:
            self.log.info('ntp service correctly set to '+str(enable))
        else:
            self.log.error('ntp service set to %s fail'%(enable))
        
        if ntp_server == '':
            pass
        elif ntp_server in ntpserver:
            self.log.info('ntp server correctly set to '+str(ntp_server))
        else:
            self.log.error('ntp service set to %s fail' % (ntp_server))

    def send_cgi(self,cmd):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/system_mgr.cgi?cmd='.format(self.uut[self.Fields.internal_ip_address])
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

    def change_time_format(self, format):
        self.log.info('****** time format update to %s *******' % format)
        cgi_cmd = 'cgi_time_format&f_time_format='+str(format)
        self.send_cgi(cmd=cgi_cmd)
        
    def check_time(self, chk_hour):
        current_time, year, month, day = self.get_current_datetime()
        self.log.info('current_time = %s, chk_hour = %s' % (current_time, chk_hour))
        if str(current_time) == str(chk_hour):
            self.log.debug('Time set to ' + str(current_time))
            result = True
        else:
            result = False
        return result
    
    def set_time(self, hour_offset):
        current_hour, year, month, day = self.get_current_datetime()
        target_hour = current_hour + hour_offset
        # Too close to midnight, so roll back, then take another 5 hours off (10 total)
        if target_hour > 23:
            target_hour -= 10

        target_time = str(target_hour) + ':00'

        if target_hour < 10:
            target_hour = '0' + target_time

        self.log.info('******* time setting ********')
        hour = target_time.split(':')[0]
        minute = target_time.split(':')[1]
        self.log.info('*** hour set to %s and minute set to %s ***' % (hour, minute))
        month_table = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07',
                       'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
        
        datetime_update_cmd = 'cgi_manual_time&f_year=%s&f_month=%s&f_day=%s&f_hour=%s&f_min=%s&f_sec=0' % (year, month_table[month], day, hour, minute)
        self.send_cgi(datetime_update_cmd)

        return target_hour, hour

    def get_time(self,  attempt_delay=5):
        command = 'date'
        ## Add Retry for timing issue between get time command 'date' and set time
        retry = 3
        while retry >= 0:
            try:
                returnvalue = self.run_on_device(command)
                self.log.debug('result : {0}'.format(returnvalue))
                if returnvalue != '':
                    self.log.info('result : {0}'.format(returnvalue))
                    break
            except Exception as e:
                if retry == 0:
                    self.log.exception('Exception: {0}'.format(repr(e)))
                    self.log.debug('result : {0}'.format(returnvalue))
                self.log.info('Exception: {0} ,Wait {1} seconds and retry {2} for get model number. '.format(repr(e), attempt_delay, (3-retry)))
                time.sleep(attempt_delay)
                retry -= 1
        self.log.debug('result : {0}'.format(returnvalue))

        datetime = returnvalue.split(' ')
        print datetime
        month = datetime[1]
        day = datetime[len(datetime)-4]
        year = datetime[len(datetime)-1]

        ## time format : xx:xx:xx
        time_value = datetime[len(datetime)-3]
        print time_value, year, month, day
        return time_value, year, month, day

    def get_current_datetime(self):

        device_time, year, month, day = self.get_time()
        hour_value, minute_value, sec_value = device_time.split(':')
        self.log.info('device date & time: {0} {1} {2} {3}, hour_value: {4}'.format(year, month, day, device_time, hour_value))
        tmp_value = float(hour_value)
        
        return int(tmp_value), year, month, day

    def update_hosts_file(self, server, eliminating):
        loopback = '127.0.0.1'
        self.execute('echo "127.0.0.1 time.windows.com" > /etc/hosts')
        self.execute('echo "127.0.0.1 pool.ntp.org" >> /etc/hosts')
        if not eliminating:
            server_to_loopback = 'echo "127.0.0.1 '+ server + '" >> /etc/hosts'
            self.log.info('Set NTP Server: {0} to loopback: {1}'.format(server, loopback))
            self.execute(server_to_loopback)
        self.log.info('Successfully updated /etc/hosts file')

    def network_config(self, config):
        waiting_string = '/ #'        
        if self.uut[self.Fields.serial_server] is None:
            self.log.warning('Could not set network port. No serial connection.')
            return False

        self.log.info('###### Open serial connection ######')
        model_command = 'cat /usr/local/twonky/resources/devicedescription-custom-settings.txt | grep MODELNUMBER'
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))
            
        #Get device's model number
        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write(model_command)
        self.serial_wait_for_string('MODELNUMBER', 10)
        return_value = self.serial_read()
        self.log.info('return value: {0}'.format(return_value))
        a, b, model_number = return_value.split("#")
        self.log.info('###### device model number: {0} ######'.format(model_number))
        network_port = ['egiga0']
            
        # 2 eth port: Aurora, Yosemite, Sprite, Yellowstone, Lightning    
        if model_number in ('LT4A', 'BNEZ', 'BWZE', 'BWAZ', 'BBAZ'):
            network_port.append('egiga1') 

        for port in network_port:
            network_command = 'ifconfig %s %s' % (port, config)
            self.log.info('command: {}'.format(network_command))
            self.serial_write(network_command)
            time.sleep(3)
            if port in ['egiga1']:
                self.serial_wait_for_string(waiting_string, 30)
            else:
                if config == 'down':
                    down_string = 'bonding: bond0: now running without any active interface !'
                    self.serial_wait_for_string(down_string, 30)
                else:
                    up_string = 'Set Date & time :'
                    self.serial_wait_for_string(up_string, 30)
                    time.sleep(10)

NTPConfiguration()