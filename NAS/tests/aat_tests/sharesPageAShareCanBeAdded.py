"""
title           :sharesPageAShareCanBeAdded.py
description     :To verify if a new share can be created
author          :yang_ni
date            :2015/05/22
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
import os

picture_counts = 0


class AShareCanBeAdded(TestClient):

    def run(self):

        try:
            # Delete all the shares
            self.log.info('Deleting all shares before test case starts ...')
            self.delete_all_shares()

            # Create a share named 'share'
            self.get_to_page('Shares')
            time.sleep(10)
            self.create_shares_webUI()
            time.sleep(8)
            self.close_webUI()
            self.get_to_page('Shares')
            time.sleep(5)
            self.element_should_be_visible('shares_share_share')
        except Exception as e:
            self.log.error('ERROR: share is not added successfully: {}'.format(repr(e)))
            self.screenshot(prefix='run')
        else:
            self.log.info('*** Share is added successfully ***')
        finally:
            self.log.info('*** Deleting all shares after test case finishes ... ***')
            self.delete_all_shares()

    def screenshot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1

# This constructs the AShareCanBeAdded() class, which in turn constructs TestClient() which triggers the AShareCanBeAdded.run() function
AShareCanBeAdded()