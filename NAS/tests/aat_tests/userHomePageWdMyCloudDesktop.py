"""
Created on June 8th, 2015

@author: tran_jas

## @brief User able to follow WD Desktop App link to download
#
#  @detail
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=33260&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=81&nTP=270537&etab=8

"""

import time
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as Element

user_account = 'jtran'
user_password = 'fituser'


class MyCloudDesktop(TestClient):

    def run(self):

        try:
            self.log.info('User Home Page - WD MyCloud App Desktop link')

            # ensure accept EULA as admin - in case EULA hasn't been accepted
            self.access_webUI()
            self.close_webUI()

            self.create_user(user_account, password=user_password)
            self.uut[self.Fields.web_username] = user_account
            self.uut[self.Fields.web_password] = user_password
            self.download_wd_desktop_app()

        except Exception, ex:
            self.log.exception('Failed to open WD Desktop App link \n' + str(ex))
        finally:
            self.log.info('Reset user back to admin')
            self.delete_user(user_account)
            self.uut[self.Fields.web_username] = 'admin'
            self.uut[self.Fields.web_password] = ''
            self.close_webUI()

    def download_wd_desktop_app(self):
        try:
            self.access_webUI()
            self.click_element(Element.NON_ADMIN_HOME_WD_DESKTOP_APP_LINK)
            time.sleep(5)
            nas_window = self._sel.driver._current_browser().current_window_handle
            all_window = self._sel.driver._current_browser().window_handles
            all_window.remove(nas_window)
            pop_window = all_window[0]
            self.log.info("NAS Handle: {}".format(nas_window))
            self.log.info("Pop Handle: {}".format(pop_window))
            self._sel.driver._current_browser().switch_to_window(pop_window)
            url_text = self._sel.driver.get_location()
            self.log.info("Current URL: {}".format(url_text))

            # Verify URL matched expected model
            self.log.info(self.uut[self.Fields.download_page])
            expected_url = "http://setup.wd2go.com/?mod=download&device=" + self.uut[self.Fields.download_page]
            expected_url_index = "http://setup.wd2go.com/index.php?mod=download&device=" + \
                                 self.uut[self.Fields.download_page]
            if expected_url == url_text:            
                self.log.info("expected_url: {}".format(expected_url))
                self.log.info('PASSED: correct page is displayed to download WD Desktop App')
            elif expected_url_index == url_text:
                self.log.info("expected_url: {}".format(expected_url_index))
                self.log.info('PASSED: correct page is displayed to download WD Desktop App')
            else:
                self.log.error('FAILED: incorrect page is displayed to download WD Desktop App')

        except Exception as e:
            self.log.exception("ERROR: Failed to verify WD Desktop download link due to {}".format(repr(e)))


MyCloudDesktop()
