__author__ = 'hoffman_t'
import os

from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.wd_wizards import RAIDWizard
from global_libraries import wd_formatting

class FullDataVolume(TestClient):
    """
    http://wiki.wdc.com/wiki/Full_Data_Volume

    Notes:

    * Formats the volume as RAID 0 since this gives us the largest possible capacity. Although Spanning may be faster
      to format, RAID 0 gives us better write performance, so the volume will fill more quickly.

    Changes to manual procedure:
        * Scenario 1 skipped:
            This scenario creates a large sparse file using dd and fills the volume almost instantly. This is a quick
            smoke test designed to find a major failure immediately. It was triggered by a bug in the 4NC/Firefly NAS
            products where the web UI wouldn't load if there wasn't enough space on the user volume. The idea for manual
            testing was to find this failure immediately and, if present, end the test and save manual test hours.
            Since this test is automated, this step is not necessary.
        * Validation:
            * Does not run a complete test of the GUI. Instead, it checks every page of the GUI to make sure it loads
              and brings up the storage page and home page.
            * Cannot test LCD
    """
    def run_aat(self):
        """ To speed up AAT, use a small volume
        """
        self.volume_size=10
        self.media_resolution = (500, 500)
        self.total_files = 50
        self.run_test()

    def run_art(self):
        """ For ART, use the maximum capacity
        """
        self.volume_size = None
        self.media_resolution = (5000, 5000)
        self.total_files = 600000
        self.run_test()

    def run_test(self):
        """
        1) Format the drive
        2) Run each scenario
        """
        self.configure_raid(raidtype=0, volume_size=self.volume_size, force_rebuild=self.volume_size is not None)
        self.scenario_2()

    def tc_cleanup(self):
        self.configure_raid(raidtype=0, force_rebuild=self.volume_size is not None)
        self.execute('rm -rf /shares/Public/Shared\\ Pictures/aat')
        self._files_copied_network = []
        self._paths_created_network = []

    def scenario_2(self):
        """ Verify the behavior of the media crawlers. Use valid JPG files to create a large database, then fill
            the rest of the volume up with a sparse file. Parameters as follows:

            files_per_folder - The number of JPG files to put in each folder
            subfolders_per_folder - The maximum number of subfolders per folder
            total_files - The total number of files to create
            destination - The directory in the Public share to copy the JPGs
        """

        self.start_monitoring()

        self.generate_media_files(media_info=('jpg', self.media_resolution[0], self.media_resolution[1]),
                                  total_files=self.total_files,
                                  destination=r'Shared Pictures/aat',
                                  share_name='Public',
                                  copy_through_ssh=True,
                                  files_per_folder=20,
                                  max_folders=100,
                                  all_unique=False,
                                  media_variance=50,
                                  random_data=True)

        # Get the free space
        free_space = self.get_share_freespace('Public')

        # Fill the rest of the drive with 2 GiB files
        number_of_files = (free_space / (2 * 1024 * 1024 * 1024)) + 1

        self.create_file(filesize=2,
                         units='GiB',
                         file_count=number_of_files,
                         dest_share_name='Public',
                         dest_dir_path='FullDataVolume',
                         run_on_uut=True)

        self.verify_uut()

    def start_monitoring(self):
        # Starts the resource monitors in the background
        self.execute('top -b > /shares/Public/top.txt', timeout=-1)

    def stop_monitoring(self):
        # Stops the resource monitors
        self.execute('killall -9 top')

    def verify_uut(self):
        # Check the following:
        # * Resource Consumption
        # * GUI
        # * LAN access - Can see existing files, but cannot copy new files
        # * Remote Access - Can see existing files, but cannot copy new files
        # * App Downloads - Cannot download apps due to low space
        # * Firmware update fails with low space warning
        pass

    def fill_volume(self):
        filesize = 500
        filesize_units = 'MiB'
        share_name = 'Public'
        root_dir = 'full_volume'
        files_per_directory = 1000
        current_dir = 1

        # Create a mount point
        mountpoint = self.mount_share(share_name, self.Share.samba)
        free_space = self.get_directory_freespace(mountpoint)
        self.log.info('Starting free space is ' + wd_formatting.bytes_to_units(free_space, use_binary=True))

        samba_path = os.path.join(mountpoint, root_dir)

        # Make a directory on the share
        if os.path.isdir(samba_path):
            self.log.info('Deleting previous test contents')
            os.rmdir(samba_path)

        while True:
            dir_name = '%05d' % current_dir
            samba_destination = os.path.join(samba_path, dir_name)
            ssh_destination = os.path.join(root_dir, dir_name)

            os.makedirs(samba_destination)

            self.log.info('Creating {} files in {}'.format(files_per_directory, ssh_destination))
            self.create_file(filesize=filesize,
                             units=filesize_units,
                             file_count=files_per_directory,
                             dest_share_name=share_name,
                             dest_dir_path=ssh_destination,
                             run_on_uut=True,
                             extension='bin')

            current_dir += 1

FullDataVolume()
