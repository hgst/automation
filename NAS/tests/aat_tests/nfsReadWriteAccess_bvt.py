'''
Create on May 18, 2015
@Author: lo_va
Objective: Test NFS protocol Read/Write Access for BVT project 
'''
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
import time
import os

class nfsReadWriteAccess(TestClient):

    def run(self):
        product_info = self.uut[self.Fields.product]
        if product_info == 'Lightning':
            self.log.warning('Lightning does not support NFS service, no need to test it.')
            exit()
        try:
            sharename = 'Public'
            nfs_mount = self.mount_share(share_name=sharename, protocol=self.Share.nfs)
            if nfs_mount:
                created_files = self.create_file(filesize=51200, dest_share_name=sharename)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=sharename):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
            '''
            tmp_file = os.path.abspath(self.generate_test_file(10240))
            tmpfile = ''.join(tmp_file)
            self.write_files_to_nfs(files=tmpfile,share_name='Public', delete_after_copy=False)
            '''
        except Exception as ex:
            self.log.exception('NFS Read/Write Access FAILED!! Exception: {}'.format(ex))

nfsReadWriteAccess()