#
#Objective: Configure user quota limits and validate that when exceeding
#           those limits the appropiate errors and alerts are triggered
# wiki URL: http://wiki.wdc.com/wiki/Quotas
#

from testCaseAPI.src.testclient import TestClient
from smb.smb_structs import OperationFailure
import sys
user1 = 'user1'
group = 'group1'
 
share1 = 'user1'

class userAccountQuota(TestClient):
    def run(self):
        self.log.info('User Account Quota Test started')
        self.setup()
        self.user_account_quota(file_size=10, size_of_user_quota=95)
        
    def setup(self):
        self.delete_all_users()
        self.execute('smbif -b user1')
        self.delete_all_shares()
        self.delete_group(group)
                 
    def enough_space_validation(self, response, expected_failure = True):
        testname = sys._getframe().f_back.f_code.co_name 
        
        # Validate enough space
        if expected_failure == True:    
            if 'file0.bmp' in response[0]:
                if response[2] < 10:
                    self.log.info('{0} Test Passed'.format(testname))
                else:
                    self.log.error('{0} Test Failed'.format(testname))         
        else:
            if 'file0.bmp' in response[0]:
                if response[2] >= 10:
                    self.log.info('{0} Test Passed'.format(testname))
                else:
                    self.log.error('{0} Test Failed'.format(testname))
                
    def user_account_quota(self, file_size, size_of_user_quota):
        # The file size to transfer, this will convert KB to MB
        file_size_MB = 1024 * (file_size)
              
        # Add an user with quotas
        self.log.info('Creating user1 ...')
        self.create_multiple_users(numberOfusers=1)
        self.log.info('Setting user1 quota ...')
        self.set_quota_all_volumes(user1, 'user', size_of_user_quota)
        
        # Make sure user1 share was created otherwise create it.
        if self.get_share(share1) == 0:
            self.log.info('Share: {} already created'.format(share1))
            pass
        else:
            self.execute('smbif -a {0}'.format(share1))
            self.log.info('Creating Share: {}...'.format(share1))
         
        # Generate the test file
        jpgFiles = self.generate_files_on_workspace(file_size_MB, 9, 'jpg')
        
        # Write the file to the unit as user1
        jpg_Result = self.write_files_to_smb(username=user1, password='welc0me', files=jpgFiles, share_name=share1, delete_after_copy=True)
        
        # Generate the test file
        bmpFiles = self.generate_files_on_workspace(file_size_MB, 1, 'bmp')
        
        # Write the file to the unit as user1
        try:
            self.write_files_to_smb(username=user1, password='welc0me', files=bmpFiles, share_name=share1, delete_after_copy=True)
        except OperationFailure:
            self.log.info('Quota met, unable to write files. Passed.')
        else:
            self.log.error('Able to write files despite quota being met.') 
            
        response = self.read_files_from_smb(files=bmpFiles, share_name=share1)
        
        # Validate enough space
        self.enough_space_validation(response, expected_failure = True)
        
userAccountQuota()