"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify correct firmware is installed on device

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

class CorrectFirmware(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            firmware = self.get_firmware_version_number()
            if firmware == self.uut[self.Fields.expected_fw_version]:
                self.log.info("Correct firmware: {}".format(firmware))
            else:
                self.log.error("Incorrect firmware: on device - {}, "
                               "on Inventory Server - {}".format(firmware, self.uut[self.Fields.expected_fw_version]))
        except Exception, ex:
            self.log.exception('Failed to complete firmware check test case \n' + str(ex))


CorrectFirmware()