"""
Created on June 25th, 2015

@author: tran_jas, lin_ri

@Procedure:
 http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=33733&execView=execDetails&tdetab=0&view=details&nReq=6905&reqView=reqDetails&pltab=steps&pId=81&nTP=270510&etab=8
 1) Login UI as admin
 2) Go to Settings page
 3) Select ISO Mount
 4) Click on Create ISO Image
 5) Provide info for image size, image path, image name
 6) Verify that the image is created

 Automation: Full

    Not supported product:
        Glacier

    Support Product:
        Lightning
        YellowStone
        Yosemite
        Sprite
        Aurora
        Kings Canyon

    Test Status:
        Local Yellowstone: Pass (FW: 2.10.287)


"""

from testCaseAPI.src.testclient import TestClient
import time

TCNAME = '(MA-398) Settings page/ISO Mount/Create ISO Image'

class settingsPageIsoMountCreateIsoImage(TestClient):

    def run(self):
        self.log.info('######################## {} TEST START ##############################'.format(TCNAME))
        if self.uut[self.Fields.product] in ('Glacier',):
            self.log.info("**** Glacier does not support ISO utility, skip the test!! ****")
            return
        share_name = 'isoImage'
        test_filename = 'isoTest.jpg'
        try:
            self.delete_all_shares()
            self.delete_iso_share_by_ui()
            self.run_on_device('rm -rf /shares/{}/*.*'.format(share_name))
            self.log.info("Create new share: {}".format(share_name))
            self.create_shares(share_name=share_name, force_webui=True)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            self.log.info("Generate random file on /shares/{}/".format(share_name))
            self.create_random_file('/shares/{}/'.format(share_name), filename=test_filename, blocksize=1024, count=10240)
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info("--- Go to utilities under Settings ---")
            # Go to Utilities
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('diagnostics', 'css=#diagnostics.LightningSubMenuOn', visible=True)
            # CD-ROM
            self.create_iso_image(share_name, test_filename, 'test1', 'CDROM(650MB/74MIN)')
            self.verify_image_check(share_name=share_name, filename='test1.iso')
            time.sleep(5)
        except Exception as ex:
            self.log.error('Failed to create ISO image, ex: {0}'.format(ex))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            output = self.run_on_device('ls -la /shares/')
            self.log.info("Current shares: \n {}".format(output))
        else:
            self.log.info('===== ISO image test success! =====')
        finally:
            self.log.info("====== Clean Up section ======")
            self.execute('rm -rf /shares/{}/*.*'.format(share_name))
            self.log.info("Clean all files under {} share folder".format(share_name))
            self.log.info('######################## {} TEST END ##############################'.format(TCNAME))

    def verify_image_check(self, share_name, filename):
        cmd = 'test -f /shares/{}/{}; echo $?'.format(share_name, filename)
        result = self.execute(cmd)[1]
        self.log.info('file check: {}'.format(result))
        if result == '0':  # Success: the file exists
            self.log.info("PASS: ISO image file: {} is created successfully".format(filename))
        else:
            self.log.error("ERROR: ISO image file: {} is not found!!".format(filename))

    def delete_iso_share_by_ui(self):
        try:
            self.log.info("=== Clean ISO Share by UI ===")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('settings_utilities_link', "settings_utilitiesLogs_button", visible=True)
            # Scroll the page to the bottom
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            row_elem = self.element_find("//div[@id=\'iso_list\']")
            rows = len(row_elem.find_elements_by_xpath("//div[@id=\'iso_list\']/ul/li"))
            self.log.info("ROWS: {}".format(rows))
            if rows == 0:
                self.log.info("No ISO mount share is created, skip the deletion!")
                return
            for i in range(rows):
                self.click_wait_and_check('settings_utilitiesISODel0_link',
                                          'popup_apply_button', visible=True)
                self.click_wait_and_check('popup_apply_button', visible=False)
                self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
                time.sleep(5)   # Waiting for updating
            self.log.info("=== All ISO shares are clean! ===")
        except Exception as e:
            self.log.exception("ERROR: Fail to delete ISO share by Web UI, exception: {}".format(repr(e)))
        finally:
            self.close_webUI()

    def create_iso_image(self, share_name, file_name, iso_name, iso_size):
        self.log.info('=== Creating ISO Image: {} ==='.format(iso_name))
        try:
            self.click_wait_and_check('settings_utilitiesIsoCreate_button', 'isoCD_title', visible=True)
            self.log.info('Entering ISO Size format')
            self.click_link_element('id_iso_size', iso_size)
            self.log.info('Selecting ISO Path')
            self.click_wait_and_check('settings_utilitiesIsoPath_button', 'settings_utilitiesIsoPathTreeCancel_button')
            self.log.info('Clicking {}'.format(share_name))
            # self.click_element('xpath=(//a[contains(text(), \'Public\')])[2]')
            self.click_wait_and_check("//div[@id=\'isotree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']".format(share_name),
                                      "//div[@id=\'isotree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/new/\']".format(share_name))
            self.log.info('Selected Path as {}'.format(share_name))
            self.click_wait_and_check('settings_utilitiesIsoPathTreeOk_button', visible=False)
            self.log.info('Inputting ISO name')
            self.input_text_check('settings_utilitiesIsoName_text', iso_name, do_login=False)
            self.log.info('Clicking Next')
            self.click_wait_and_check('settings_utilitiesIsoNext1_button', 'settings_utilitiesIsoNext2_button', visible=True)
            self.log.info("Select {} as ISO content".format(file_name))
            if self.is_element_visible("//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/Public/\']"):
                self.click_wait_and_check("//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/Public/\']",
                                          "//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/Public/new/\']",
                                          visible=False)
            self.click_wait_and_check("//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/\']".format(share_name),
                                      "//div[@id=\'img_tree_div\']//a[@rel=\'/mnt/HD/HD_a2/{}/new/\']".format(share_name))
            iso_locator = "//a[@class=\'tree_click_focus\']/following-sibling::ul/li/label/span".format(share_name, file_name)
            iso_checkbox = "//a[@class=\'tree_click_focus\']/following-sibling::ul/li/label//input".format(share_name, file_name)
            check_status = self.element_find(iso_checkbox).get_attribute('checked')
            while check_status != 'true':
                self.click_element(iso_locator)
                check_status = self.element_find(iso_checkbox).get_attribute('checked')
            self.log.info("{} is selected: {}".format(file_name, check_status))
            self.click_element('settings_utilitiesIsoAdd_button')
            added_file = self.get_text("//div[@id=\'img_tree_div2\']/ul/li/ul/li/a")
            while added_file != file_name:
                self.log.info("added file: {}".format(added_file))
                self.log.info("Re-click Add button")
                self.click_element('settings_utilitiesIsoAdd_button')
                added_file = self.get_text("//div[@id=\'img_tree_div2\']/ul/li/ul/li/a")
            self.log.info("added file: {}".format(added_file))
            self.click_wait_and_check('settings_utilitiesIsoNext2_button', visible=False)
            time.sleep(10)  # Waiting for ISO image creation
            self.log.info('Finishing ISO Image Creation')
            self.click_wait_and_check('settings_utilitiesIsoSave_button', visible=False)
            time.sleep(5)
        except Exception as e:
            self.log.error("ERROR: Failed to create iso image due to exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            output = self.run_on_device('ls -la /shares/')
            self.log.info("Current shares: \n {}".format(output))
        else:
            self.log.info("=== PASS: Complete ISO image creation ===")

settingsPageIsoMountCreateIsoImage()


