"""
Create on Mar 22, 2016
@Author: lo_va
Objective: Verify OTA firmware auto-update work.
wiki URL: http://wiki.wdc.com/wiki/OTA_auto-update_BAT
"""

import os
import time
import requests
import json
import shutil
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields

fw_update_version = os.environ["Kamino_FW"]
yocto_download_path = os.environ["NEXUS_BASE_URL"]
ota_enable_config = '8a808fe35411186b015413d4504f0008'
ota_disable_config = '8a808b9754111821015413d4bf290013'
updateOk = 'updateOk'
updateReboot = 'updateReboot'
downloading = 'downloading'
updating = 'updating'
downloadFail = 'downloadFail'
updateFail = 'updateFail'
adjustWisb = 'adjustWisb'
adjustWiri = 'adjustWiri'
updateFailAfterReboot = 'updateFailAfterReboot'


class OTAfwupdate(TestClient):

    def run(self):
        self.yocto_init()
        self.yocto_check()
        wddevice1 = self.execute('cat /Data/devmgr/db/wddevice.ini | grep deviceid')[1]
        wddevice2 = self.execute('cat /Data/devmgr/db/wddevice.ini | grep accesstoken')[1]
        deviceid = wddevice1.split('deviceid = ')[1]
        accesstoken = wddevice2.split('accesstoken = ')[1]
        self.url = 'http://qa1-device.remotewd1.com/device/v1/device/{0}?&access_token={1}'.format(deviceid, accesstoken)
        try:
            wisb = self.get_values()['wisb']
            wiri = self.get_values()['wiri']
            self.log.info('Current wisb: {0}, wiri: {1}'.format(wisb, wiri))
            self.update_wisb_firmware(wisb=fw_update_version)
            self.ota_enable()
            status = self.get_values().get('status')
            loop = 1
            while not status == (downloading or updating or updateReboot) and loop < 35:
                self.log.warning('Current status is: {}, send update wisb request again.. loop {}'.format(status, loop))
                self.update_wisb_firmware(wisb=fw_update_version)
                time.sleep(10)
                loop += 1
                status = self.get_values().get('status')
                if loop >= 35:
                    raise Exception('OTA not start to auto update after send request {} times, update Failed'.format(loop-1))
            self.check_fail_case()

            self.wait_firmware_downloading()
            self.wait_firmware_updating()
            self.wait_device_back()
        except Exception, ex:
            self.log.exception('OTA auto-update Failed!! Exception: {}'.format(ex))
            with open("OTAUpdateFailed.txt", "w") as f:
                f.write('OTA Updated Firmware Failed\n')
            shutil.copy('OTAUpdateFailed.txt', '{}/OTAUpdateFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)

        status = self.check_fail_case()
        wiri = self.get_values().get('wiri')
        wisb = self.get_values().get('wisb')
        if wisb == wiri and status == updateOk:
            self.log.info('OTA Auto-Update Successful!!')
            with open("UpdateSuccess.txt", "w") as f:
                f.write('Updated Firmware Success\n')
            shutil.copy('UpdateSuccess.txt', '{}/UpdateSuccess.txt'.format(os.environ["WORKSPACE"]))
        else:
            self.log.error('Current update status is : {}'.format(status))
            self.log.error('wisb: {0}, wiri: {1}, wisb and wiri are not same, Update Firmware Failed!!'.format(wisb, wiri))
            with open("OTAUpdateFailed.txt", "w") as f:
                f.write('OTA Updated Firmware Failed\n')
            shutil.copy('OTAUpdateFailed.txt', '{}/OTAUpdateFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)

    def tc_cleanup(self):
        try:
            self.ota_disable()
        except Exception as e:
            self.log.exception('Failed to disable OTA auto-update. Exception: {}'.format(repr(e)))
        os.chdir(os.environ["WORKSPACE"])
        try:
            self.log.info('Start to SCP lastpassbuild file to the NAS')
            self.open_scp_connection()
            self.scp_file_to_device(files='lastpassbuild', remote_path='/Data/devmgr/db', recursive=False, preserve_times=False)
        except Exception as e:
            self.log.exception('Failed to transfer lastpassbuild file. Exception: {}'.format(repr(e)))

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            boot_finished = self.execute('ls /tmp')[1]
            fw_ver = self.execute('cat /etc/version')[1]
            if 'boot_finished' in boot_finished:
                self.log.info('Device is Ready')
                self.log.info('Firmware Version: {}'.format(fw_ver))
            else:
                raise Exception('boot_finished is not exist, device not ready!')
            return fw_ver
        except Exception as ex:
            self.log.exception('Device check failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

    def wait_firmware_downloading(self):
        max_download_time = 500
        start_time = time.time()
        status = self.check_fail_case()
        self.log.info('Status : {}'.format(status))
        while status == downloading:
            time.sleep(2)
            status = self.check_fail_case()
            self.log.info('Status : {}'.format(status))
            if time.time() - start_time > max_download_time:
                with open("OTADownloadFailed.txt", "w") as f:
                    f.write('OTA Download Firmware Failed\n')
                shutil.copy('OTADownloadFailed.txt', '{}/OTADownloadFailed.txt'.format(os.environ["WORKSPACE"]))
                raise Exception('Timed out to waiting firmware download')

    def wait_firmware_updating(self):
        max_updating_time = 180
        start_time = time.time()
        status = self.check_fail_case()
        self.log.info('Status : {}'.format(status))
        while status == updating:
            time.sleep(2)
            status = self.check_fail_case()
            self.log.info('Status : {}'.format(status))
            if time.time() - start_time > max_updating_time:
                raise Exception('Timed out to waiting firmware updating')

    def wait_device_back(self):
        max_boot_time = 300
        start_time = time.time()
        status = self.check_fail_case()
        self.log.info('Status : {}'.format(status))
        while not status == updateOk:
            time.sleep(2)
            status = self.check_fail_case()
            self.log.info('Status : {}'.format(status))
            if time.time() - start_time > max_boot_time:
                raise Exception('Timed out waiting to boot')

    def update_wisb_firmware(self, wisb):
        data = {"firmware":{"wisb":wisb}}
        headers = {"Content-type": "application/json"}
        r = requests.put(self.url, data=json.dumps(data), headers=headers)
        loop = 1
        while not r.json()['data']['firmware']['wisb'] == fw_update_version and loop <35:
            self.log.warning('wisb not updated, send request again.. loop {}'.format(loop))
            r = requests.put(self.url, data=json.dumps(data), headers=headers)
            loop += 1
            if loop >= 35:
                raise Exception('wisb not updated after send request {} times, wisb update Failed'.format(loop-1))
        else:
            self.log.info('wisb: {} update succeed!!'.format(wisb))

    def ota_enable(self):
        data = {"configuration": {"wisb":ota_enable_config}}
        headers = {"Content-type": "application/json"}
        requests.put(self.url, data=json.dumps(data), headers=headers)
        time.sleep(10)
        loop = 1
        while not self.get_values().get('wiri_conf') == ota_enable_config and loop < 35:
            self.log.warning('ota not enabled, send request again.. loop {}'.format(loop))
            time.sleep(10)
            requests.put(self.url, data=json.dumps(data), headers=headers)
            loop += 1
            if loop >= 35:
                raise Exception('ota not enabled after send request {} times, ota update Failed'.format(loop-1))
        else:
            self.log.info('ota: {} enable succeed!!'.format(ota_enable_config))

    def ota_disable(self):
        data = {"configuration": {"wisb":ota_disable_config}}
        headers = {"Content-type": "application/json"}
        requests.put(self.url, data=json.dumps(data), headers=headers)
        time.sleep(10)
        loop = 1
        while not self.get_values().get('wiri_conf') == ota_disable_config and loop < 35:
            self.log.warning('ota not disabled, send request again.. loop {}'.format(loop))
            time.sleep(10)
            requests.put(self.url, data=json.dumps(data), headers=headers)
            loop += 1
            if loop >= 35:
                raise Exception('ota not disabled after send request {} times, update Failed'.format(loop-1))
        else:
            self.log.info('ota: {} disable succeed!!'.format(ota_disable_config))

    def get_values(self):
        response = requests.get(self.url)
        wisb_conf = response.json()['data']['configuration']['wisb']
        wiri_conf = response.json()['data']['configuration']['wiri']
        wisb = response.json()['data']['firmware']['wisb']
        wiri = response.json()['data']['firmware']['wiri']
        status = response.json()['data']['firmware']['status']
        return {'wiri': wiri, 'wisb': wisb, 'status': status, 'wiri_conf': wiri_conf, 'wisb_conf': wisb_conf}

    def check_fail_case(self):
        status = self.get_values().get('status')
        if status == updateFail:
            self.log.error('Update status is : {}, updated firmware FAILED!!'.format(status))
            with open("OTAUpdateFailed.txt", "w") as f:
                f.write('Updated Firmware Failed\n')
            shutil.copy('OTAUpdateFailed.txt', '{}/OTAUpdateFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)
        '''
        if status == downloadFail:
            self.log.error('Update status is : {}, downloaded firmware FAILED!!'.format(status))
            with open("OTADownloadFailed.txt", "w") as f:
                f.write('OTA Download Firmware Failed\n')
            shutil.copy('OTADownloadFailed.txt', '{}/OTADownloadFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)
        '''
        if status == updateFailAfterReboot:
            self.log.error('Update status is : {}, updated firmware FAILED!!'.format(status))
            with open("OTAUpdateFailed.txt", "w") as f:
                f.write('OTA Updated Firmware Failed\n')
            shutil.copy('OTAUpdateFailed.txt', '{}/OTAUpdateFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)
        if status == adjustWisb:
            self.log.error('Update status is : {}, updated firmware FAILED!!'.format(status))
            with open("OTAUpdateFailed.txt", "w") as f:
                f.write('OTA Updated Firmware Failed\n')
            shutil.copy('OTAUpdateFailed.txt', '{}/OTAUpdateFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)
        if status == adjustWiri:
            self.log.error('Update status is : {}, updated firmware FAILED!!'.format(status))
            with open("OTAUpdateFailed.txt", "w") as f:
                f.write('OTA Updated Firmware Failed\n')
            shutil.copy('OTAUpdateFailed.txt', '{}/OTAUpdateFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)

        return status


OTAfwupdate(checkDevice=False)
