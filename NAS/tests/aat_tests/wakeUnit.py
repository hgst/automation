""" Test case template
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient


class WakeOnLan(TestClient(checkDevice=False)):
    # Create tests as function in this class
    def run(self):
        self.log.info('Test started')

        mac_address = self.uut[self.Fields.mac_address]
        self.send_magic_packet(mac_address)

# This constructs the Tests() class, which in turn constructs TestClient() which triggers the Tests.run() function
WakeOnLan()