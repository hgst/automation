'''
Created on Jul 23th, 2015

@author: tsui_b

Objective: Full test of Web File Viewer with non-admin user

Automation: Partial (Upload/Download function is not tested, Open all format files is not tested)

Supported Products: All except de-featured Glacier

Test Status: 
            Local 
                    Lightning: Pass (FW: 2.10.192)
            Jenkins 
                    Lightning: Pass (FW: 2.10.197)
                    Kings Canyon: Pass (FW: 2.10.197)
                    Glacier: Not Support
                    Zion: Pass (FW: 2.10.196)
                    Yosemite: Pass (FW: 2.10.197)
                    Yellowstone: Pass (FW: 2.10.189)
                    Aurora: Pass (FW: 2.10.196)
                    Sprite: Pass (FW: 2.10.197)
                    Grand Teton: Pass (FW: 2.10.197)  
'''
import time
import os

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

invalid_chars = ['\\', '/', ':', '*', '?', '"', '<', '>', '|', '.', '..']
invalid_names = ['.', '..']
max_chars = 226
share_rw = 'AAT_Share_RW'
share_ro = 'AAT_Share_RO'
share_deny = 'AAT_Share_Deny'
sub_share = 'AAT_Sub_Share'
sub_share_rename = 'AAT_Sub_Share_Rename'
textfile = 'dummyfile'
src_share = 'Public'
dst_share = 'Test123'
tarfile = 'dummytarfile'
unsupport_file='unsupportfile'
hidden_file='.hidden_file.txt'
hidden_folder='.hidden_folder'
username='aatuser'

class webFileViewerUser(TestClient):

    def run(self):
        self.log.info('### Verifying Web File Viewer Full Test ###')
        try:
            self.delete_all_users()
            self.delete_all_shares()
            self.create_user(username=username, password='')
            self.check_default_share_lists()
            self.check_share_permissions()
            self.create_sub_folder()
            self.rename_sub_folder()
            self.delete_file_and_folder()
            self.copy_files()
            self.move_files()
            self.check_file_properties()
            # Skip zip tests because there's a bug: SKY-3762
            # self.create_add_zip_and_unzip_file()
            self.untar_files()
            self.open_files()
        except Exception as e:
            self.log.error("Verify Web File Viewer Full Test failed! Exception: {}".format(repr(e)))
        
    def tc_cleanup(self):
        self.delete_local_file('{}.tar'.format(tarfile))
        self.delete_local_file('{}1.txt'.format(textfile))
        self.delete_local_file('{}2.txt'.format(textfile))
        self.clean_share('Public')
        self.delete_all_users()
        self.delete_all_shares()
                
    def check_default_share_lists(self):
        self.log.info('### Testing all default shares are listed ###')
        # Get current share list
        result = self.get_all_shares()
        share_list = self.get_xml_tags(result[1].text, 'share_name')
        
        self.access_webUI(do_login=False)
        self.input_text_check(E.LOGIN_USERNAME, username)
        self.click_wait_and_check(E.LOGIN_BUTTON, visible=False)
        self._sel.check_for_popups()
        # Go to web file viewer and check info
        self.get_to_page('Non Admin Web File Viewer')        
        # Sometimes it takes a few seconds to wait the shares show up
        time.sleep(10)
        self.current_frame_contains('Web File Viewer')
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.element_text_should_be(E.APPS_WEB_FILE_PATH, 'Shares')
        for share in share_list:
            self.log.info('Checking if share:{} is shown up on the web UI.'.format(share))
            self.current_frame_contains(share)

    def check_share_permissions(self):
        self.log.info('Creating 3 shares with rw, ro, and deny permission')
        self.create_shares(number_of_shares=1, share_name=share_rw, description=share_rw)
        self.update_share(share_name=share_rw, public_access=False)
        self.create_share_access(share_name=share_rw, username=username, access='RW')
        self.create_shares(number_of_shares=1, share_name=share_ro, description=share_ro)
        self.create_text_file_in_share(share_ro, textfile, text='text_{}'.format(share_ro))
        self.update_share(share_name=share_ro, public_access=False)
        self.create_share_access(share_name=share_ro, username=username, access='RO')
        self.create_shares(number_of_shares=1, share_name=share_deny, description=share_deny)
        self.update_share(share_name=share_deny, public_access=False)
        # Refresh cause a new share folder is created
        self.unselect_frame()
        self.click_wait_and_check('nav_wfs_link', E.APPS_WEB_FILE_FRAME)
        # Sometimes it takes a few seconds to wait the shares show up
        time.sleep(10)    
        self.current_frame_contains('Web File Viewer')
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.element_text_should_be(E.APPS_WEB_FILE_PATH, 'Shares')
        self.log.info('Checking shares with RW and RO permission is displayed')
        self.current_frame_contains(share_rw)
        self.current_frame_contains(share_ro)
        self.log.info('Checking shares with deny permission is not displayed')
        self.current_frame_should_not_contain(share_deny)
        self.log.info('Checking shares with RO permission cannot create a sub folder')
        share_locator = self.find_share_locator(share_ro)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_ro))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('You do not have sufficient permission to access this directory.')
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)        
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.log.info('Checking shares with RO permission cannot upload a file')
        self.click_unselect_frame_check(E.APPS_WEB_FILE_UPLOAD, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('You do not have sufficient permission to access this directory.')
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.log.info('Checking shares with RO permission cannot delete a file')
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), share_ro, 'file')
        self.open_context_menu_and_check(file_locator, E.APPS_WEB_FILE_MENU_DELETE)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('You do not have sufficient permission to access this directory.')
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.log.info('Checking shares with RO permission cannot rename a file')
        self.open_context_menu_and_check(file_locator, E.APPS_WEB_FILE_RENAME)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('You do not have sufficient permission to access this directory.')
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

    def create_sub_folder(self):
        self.log.info('### Testing create sub share folder ###')
        share_locator = self.find_share_locator(share_rw)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        self.log.info('Creating sub folder in {}'.format(share_rw))
        self.double_click_folder(share_rw, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG)
        # Test sub share cannot be created with invalid chars
        for chars in invalid_chars:
            self.log.info('Testing sub shares cannot be created with invalid chars: {0}_{1}'.format(sub_share, chars))
            self.input_text_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_NAME_TEXT, '{0}_{1}'.format(sub_share, chars))
            self.click_wait_and_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_SAVE, E.APPS_WEB_FILE_ERROR_DIAG)
            if chars in invalid_names:
                self.current_frame_contains('Not a valid folder name')
            else:
                self.current_frame_contains('Folder name cannot include the following characters: \ / : * ? " < > | ')
            self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        
        # Test sub share name cannot be empty
        self.log.info('Testing sub shares cannot be created with empty name')
        self.input_text_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_NAME_TEXT, '')
        self.click_wait_and_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_SAVE, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('Not a valid folder name')
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        # Test sub share max name length
        self.log.info('Testing create sub shares max name length: {}'.format(max_chars))
        share_name = sub_share
        for length in range(1, max_chars):
            share_name += 'a'
        self.input_text_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_NAME_TEXT, share_name)
        self.click_wait_and_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_SAVE, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('Folder name length should be 1-{}'.format(max_chars))
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        # Test normal sub share can be created
        self.input_text_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_NAME_TEXT, sub_share)
        self.click_wait_and_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_SAVE, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_contains(sub_share)
    
    def rename_sub_folder(self):
        self.log.info('### Testing rename sub share folder ###')
        row, share_locator = self.find_file_locator(sub_share, share_rw, 'folder')
        self.click_and_check_attribute(share_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_RENAME, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_RENAME_DIAG)
        for chars in invalid_chars:
            self.log.info('Testing sub shares cannot be renamed with invalid chars: {0}_{1}'.format(sub_share, chars))
            self.input_text_check(E.APPS_WEB_FILE_RENAME_DIAG_NAME_TEXT, '{0}_{1}'.format(sub_share, chars))
            self.click_wait_and_check(E.APPS_WEB_FILE_RENAME_DIAG_SAVE, E.APPS_WEB_FILE_ERROR_DIAG)
            if chars in invalid_names:
                self.current_frame_contains('Not a valid file name')
            else:
                self.current_frame_contains('The file name cannot include the following characters: \ / : * ? " < > |')
            self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        
        # Test sub share name cannot be empty
        self.log.info('Testing sub shares cannot be renamed with empty name')
        self.input_text_check(E.APPS_WEB_FILE_RENAME_DIAG_NAME_TEXT, '')
        self.click_wait_and_check(E.APPS_WEB_FILE_RENAME_DIAG_SAVE, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('Enter name')
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        # Test sub share max name length
        self.log.info('Testing rename sub shares max name length: {}'.format(max_chars))
        share_name = sub_share
        for length in range(1, max_chars):
            share_name += 'a'
        self.input_text_check(E.APPS_WEB_FILE_RENAME_DIAG_NAME_TEXT, share_name)
        self.click_wait_and_check(E.APPS_WEB_FILE_RENAME_DIAG_SAVE, E.APPS_WEB_FILE_ERROR_DIAG)
        self.current_frame_contains('File name length should be 1-{}'.format(max_chars))
        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
        # Test sub share can be renamed with normal chars
        self.input_text_check(E.APPS_WEB_FILE_RENAME_DIAG_NAME_TEXT, sub_share_rename)
        self.click_wait_and_check(E.APPS_WEB_FILE_RENAME_DIAG_SAVE, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        # If the share is not successfully renamed, find_share_locator will raise Exception cause share not exist 
        self.find_share_locator(sub_share_rename)
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

    def delete_file_and_folder(self):
        self.log.info('### Testing delete file and sub share folder ###')

        self.create_text_file_in_share(share_rw, textfile, 'text_{}'.format(share_rw))
        self.log.info('Deleting file: {0} in {1}'.format(textfile, share_rw))
        share_locator = self.find_share_locator(share_rw)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), share_rw, 'file')
        self.open_context_menu_and_check(file_locator, E.APPS_WEB_FILE_MENU_DELETE)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_DELETE_DIAG)
        self.click_wait_and_check(E.APPS_WEB_FILE_DELETE_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_should_not_contain('{}.txt'.format(textfile))
        self.log.info('File is deleted')

        self.log.info('Deleting folder: {0} in {1}'.format(sub_share_rename, share_rw))
        row, folder_locator = self.find_file_locator(sub_share_rename, share_rw, 'folder')
        self.click_and_check_attribute(folder_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_DELETE_DIAG)
        self.click_wait_and_check(E.APPS_WEB_FILE_DELETE_DIAG_OK, E.UPDATING_STRING, visible=False)
        #=======================================================================
        # This is weird, folder can still be found by selenium after it is deleted by pressing delete on right-clicked menu
        # Use delete icon instead of delete button on right-clicked menu
        # self.open_context_menu_and_check(folder_locator, E.APPS_WEB_FILE_MENU_DELETE)
        # self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_DELETE_DIAG)
        #=======================================================================
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_should_not_contain(sub_share_rename)
        self.log.info('Folder is deleted')

    def copy_files(self):
        self.log.info('### Testing copy files ###')
        self.clean_share('Public')
        self.create_shares(number_of_shares=1, share_name=dst_share, description=dst_share)
        self.create_text_file_in_share(src_share, textfile, 'text_src')
        self.create_text_file_in_share(dst_share, textfile, 'text_dst')
        # Refresh cause a new share folder is created 
        self.unselect_frame()
        self.click_wait_and_check('nav_wfs_link', E.APPS_WEB_FILE_FRAME)
        # Sometimes it takes a few seconds to wait the shares show up
        time.sleep(10)
        self.current_frame_contains('Web File Viewer')
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.element_text_should_be(E.APPS_WEB_FILE_PATH, 'Shares')
        src_share_locator = self.find_share_locator(src_share)
        dst_share_locator = self.find_share_locator(dst_share)

        self.log.info('Verifying Copy:Overwrite file')
        self.double_click_folder(src_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(src_share))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), src_share, 'file')
        self.click_and_check_attribute(file_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_COPY, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Copy method: rel=0 -> overwrite, rel=1 -> skip, rel=2 -> keep both
        self.click_and_check_attribute(E.APPS_WEB_FILE_METHOD_OVERWRITE, 'f_file_method', 'rel', '0')
        self.click_coordinates_and_check_attribute('link={}'.format(dst_share), -77, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(dst_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(dst_share))
        self.current_frame_contains('{}.txt'.format(textfile))
        self.current_frame_should_not_contain('{}(1).txt'.format(textfile)) # Verify file is not copied with keep both file method 
        text = self.get_text_file_content(dst_share, textfile)
        if text != 'text_src':
            raise Exception('Copy file with overwrite method failed! The text should be "text_src" but it is {}'.format(text))
        else:
            self.log.info('Copy:Overwrite file passed')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

        self.log.info('Verifying Copy:Skip file')
        self.create_text_file_in_share(dst_share, textfile, 'text_dst')
        self.double_click_folder(src_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(src_share))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), src_share, 'file')
        self.click_and_check_attribute(file_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_COPY, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Copy method: rel=0 -> overwrite, rel=1 -> skip, rel=2 -> keep both
        self.click_and_check_attribute(E.APPS_WEB_FILE_METHOD_SKIP, 'f_file_method', 'rel', '1')
        self.click_coordinates_and_check_attribute('link={}'.format(dst_share), -77, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(dst_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(dst_share))
        self.current_frame_contains('{}.txt'.format(textfile))
        self.current_frame_should_not_contain('{}(1).txt'.format(textfile)) # Verify file is not copied with keep both file method 
        text = self.get_text_file_content(dst_share, textfile)
        if text != 'text_dst':
            raise Exception('Copy file with skip method failed! The text should be "text_dst" but it is {}'.format(text))
        else:
            self.log.info('Copy:Skip file passed')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        
        self.log.info('Verifying Copy:KeepBoth file')
        self.double_click_folder(src_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(src_share))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), src_share, 'file')
        self.click_and_check_attribute(file_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_COPY, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Copy method: rel=0 -> overwrite, rel=1 -> skip, rel=2 -> keep both
        self.click_and_check_attribute(E.APPS_WEB_FILE_METHOD_KEEPBOTH, 'f_file_method', 'rel', '2')
        self.click_coordinates_and_check_attribute('link={}'.format(dst_share), -77, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(dst_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(dst_share))
        self.current_frame_contains('{}.txt'.format(textfile))
        self.current_frame_contains('{}(1).txt'.format(textfile))
        text1 = self.get_text_file_content(dst_share, textfile)
        if text1 != 'text_dst':
            raise Exception('Copy file with keep both method failed! The text in original file should be "text_dst" but it is {}'.format(text1))
        
        text2 = self.get_text_file_content(dst_share, '{}\(1\)'.format(textfile))
        if text2 != 'text_src':
            raise Exception('Copy file with keep both method failed! The text in new file should be "text_src" but it is {}'.format(text2))
        
        self.log.info('Copy:KeepBoth file passed')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.clean_share(src_share)
        self.clean_share(dst_share)

    def move_files(self):
        self.log.info('### Testing move files ###')
        self.create_text_file_in_share(src_share, textfile, 'text_src')
        self.create_text_file_in_share(dst_share, textfile, 'text_dst')
        src_share_locator = self.find_share_locator(src_share)
        dst_share_locator = self.find_share_locator(dst_share)

        self.log.info('Verifying Move:Overwrite file')
        self.double_click_folder(src_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(src_share))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), src_share, 'file')
        self.click_and_check_attribute(file_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MOVE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Copy method: rel=0 -> overwrite, rel=1 -> skip, rel=2 -> keep both
        self.click_and_check_attribute(E.APPS_WEB_FILE_METHOD_OVERWRITE, 'f_file_method', 'rel', '0')
        self.click_coordinates_and_check_attribute('link={}'.format(dst_share), -77, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        # Check if file is not exist after refresh page
        self.current_frame_contains('{}.txt'.format(textfile))
        self.click_wait_and_check(E.APPS_WEB_FILE_REFRESH, E.UPDATING_STRING, visible=False)
        self.current_frame_should_not_contain('{}.txt'.format(textfile))
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(dst_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(dst_share))
        self.current_frame_contains('{}.txt'.format(textfile))
        self.current_frame_should_not_contain('{}(1).txt'.format(textfile)) # Verify file is not moved with keep both file method 
        text = self.get_text_file_content(dst_share, textfile)
        if text != 'text_src':
            raise Exception('Move file with overwrite method failed! The text should be "text_src" but it is {}'.format(text))
        else:
            self.log.info('Move:Overwrite file passed')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
                    
        self.log.info('Verifying Copy:Skip file')
        self.create_text_file_in_share(src_share, textfile, 'text_src')
        self.create_text_file_in_share(dst_share, textfile, 'text_dst')
        self.double_click_folder(src_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(src_share))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), src_share, 'file')
        self.click_and_check_attribute(file_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MOVE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Copy method: rel=0 -> overwrite, rel=1 -> skip, rel=2 -> keep both
        self.click_and_check_attribute(E.APPS_WEB_FILE_METHOD_SKIP, 'f_file_method', 'rel', '1')
        self.click_coordinates_and_check_attribute('link={}'.format(dst_share), -77, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        # Check if file still exist after refresh page
        self.click_wait_and_check(E.APPS_WEB_FILE_REFRESH, E.UPDATING_STRING, visible=False)
        self.current_frame_contains('{}.txt'.format(textfile))   
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(dst_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(dst_share))
        self.current_frame_contains('{}.txt'.format(textfile))
        self.current_frame_should_not_contain('{}(1).txt'.format(textfile)) # Verify file is not copied with keep both file method 
        text = self.get_text_file_content(dst_share, textfile)
        if text != 'text_dst':
            raise Exception('Move file with skip method failed! The text should be "text_dst" but it is {}'.format(text))
        else:
            self.log.info('Move:Skip file passed')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

        self.log.info('Verifying Move:KeepBoth file')
        self.double_click_folder(src_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(src_share))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), src_share, 'file')
        self.click_and_check_attribute(file_locator, row, 'class', self.get_select_class(row))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MOVE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Copy method: rel=0 -> overwrite, rel=1 -> skip, rel=2 -> keep both
        self.click_and_check_attribute(E.APPS_WEB_FILE_METHOD_KEEPBOTH, 'f_file_method', 'rel', '2')
        self.click_coordinates_and_check_attribute('link={}'.format(dst_share), -77, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        # Check if file is not exist after refresh page
        self.current_frame_contains('{}.txt'.format(textfile))
        self.click_wait_and_check(E.APPS_WEB_FILE_REFRESH, E.UPDATING_STRING, visible=False)
        self.current_frame_should_not_contain('{}.txt'.format(textfile))     
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(dst_share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(dst_share))
        self.current_frame_contains('{}.txt'.format(textfile))
        self.current_frame_contains('{}(1).txt'.format(textfile))
        text1 = self.get_text_file_content(dst_share, textfile)
        if text1 != 'text_dst':
            raise Exception('Move file with keep both method failed! The text in original file should be "text_dst" but it is {}'.format(text1))
        
        text2 = self.get_text_file_content(dst_share, '{}\(1\)'.format(textfile))
        if text2 != 'text_src':
            raise Exception('Move file with keep both method failed! The text in new file should be "text_src" but it is {}'.format(text2))
        
        self.log.info('Move:KeepBoth file passed')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

    def check_file_properties(self):
        self.log.info('### Testing check file properties ###')
        self.clean_share(share_rw)
        self.create_text_file_in_share(share_rw, textfile, 'text_{}'.format(share_rw))
        parent_share_path = self.get_absolute_path(share_rw)
        result = self.get_file_info_cgi('{}.txt'.format(textfile), parent_share_path)
        file_modify_time = self.get_xml_tags(result, 'modify_time')[0]
        file_owner = self.get_xml_tags(result, 'owner')[0]
        file_group = self.get_xml_tags(result, 'group')[0]
        file_permission = self.get_xml_tags(result, 'permission')[0]
        
        share_locator = self.find_share_locator(share_rw)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        row, file_locator = self.find_file_locator('{}.txt'.format(textfile), share_rw, 'file')
        self.open_context_menu_and_check(file_locator, E.APPS_WEB_FILE_MENU_PROPERTIES)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_PROPERTIES, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PROPERTIES_DIAG)
        
        # Check file info is correct
        self.element_text_should_be('p_name', '{}.txt'.format(textfile))
        self.element_text_should_be('p_path', '{0}/{1}.txt'.format(share_rw, textfile))
        self.element_text_should_be('p_modify_time', file_modify_time)
        self.element_text_should_be('p_owner', file_owner)
        self.element_text_should_be('p_group', file_group)
        
        permission_checkbox_list = ['owner_r', 'owner_w', 'owner_x', 'user_r', 'user_w', 'user_x', 'other_r', 'other_w', 'other_x']
        permission_status_list = list(file_permission)
         
        for i, checkbox in enumerate(permission_checkbox_list):
            msg = 'Permission: {0} is {1}'.format(checkbox, permission_status_list[i])
            if permission_status_list[i] == '-':
                msg += ', checkbox should not be selected'
                self.log.debug(msg)
                self._sel.driver.checkbox_should_not_be_selected(checkbox)
            else:
                msg += ', checkbox should be selected'
                self.log.debug(msg)
                self._sel.driver.checkbox_should_be_selected(checkbox)

        self.click_wait_and_check(E.APPS_WEB_FILE_PROPERTIES_DIAG_CLOSE)
        # Go back to the first page
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
    
    def create_add_zip_and_unzip_file(self):
        self.log.info('### Testing create zip, add zip, and unzip files ###')
        self.clean_share(share_rw)
        for suffix in range(1, 5):
            self.create_text_file_in_share(share_rw, textfile+str(suffix), 'text_{}'.format(share_rw))
        share_locator = self.find_share_locator(share_rw)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        row1, file_locator1 = self.find_file_locator('{}1.txt'.format(textfile), share_rw, 'file')
        row2, file_locator2 = self.find_file_locator('{}2.txt'.format(textfile), share_rw, 'file')
        self.click_and_check_attribute(file_locator1, row1, 'class', self.get_select_class(row1))     
        self.open_context_menu_and_check(file_locator2, E.APPS_WEB_FILE_MENU_CREATE_ZIP)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_CREATE_ZIP, E.APPS_WEB_FILE_FRAME)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_contains('{}.zip'.format(share_rw))
        row3, file_locator3 = self.find_file_locator('{}3.txt'.format(textfile), share_rw, 'file')
        row4, file_locator4 = self.find_file_locator('{}4.txt'.format(textfile), share_rw, 'file')
        self.click_and_check_attribute(file_locator3, row3, 'class', self.get_select_class(row3))
        self.open_context_menu_and_check(file_locator4, E.APPS_WEB_FILE_MENU_ADD_ZIP)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_ADD_ZIP, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_MENU_ADD_ZIP_DIAG)
        self.click_wait_and_check('link={}'.format(share_rw), 'link={}.zip'.format(share_rw))
        
        retry = 3
        while retry >= 0:
            try:
                self.click_coordinates_and_check_attribute('link={}.zip'.format(share_rw), -108, 0, E.APPS_WEB_FILE_MENU_ADD_ZIP_DIAG_OK, 'class', 'ButtonRightPos2')
                self.wait_and_click(E.APPS_WEB_FILE_MENU_ADD_ZIP_DIAG_OK)
                if self.is_element_visible(E.APPS_WEB_FILE_MENU_ADD_ZIP_DIAG, wait_time=5):
                    if self.is_element_visible(E.APPS_WEB_FILE_ERROR_DIAG_OK, wait_time=5):
                        self.log.info('File is not selected, close error diag and retry')    
                        self.click_wait_and_check(E.APPS_WEB_FILE_ERROR_DIAG_OK)
                    self.log.info('Remaining {} retries...'.format(retry))
                    retry -= 1
                    continue
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.info('Element "{0}" did not click, remaining {1} retries...'.format('link={}.zip'.format(share_rw), retry))
                retry -= 1
                continue
            else:
                break
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        row_zip, zip_locator = self.find_file_locator('{}.zip'.format(share_rw), share_rw, 'file')
        self.click_and_check_attribute(zip_locator, row_zip, 'class', self.get_select_class(row_zip))
        self.open_context_menu_and_check(zip_locator, E.APPS_WEB_FILE_MENU_UNZIP)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_UNZIP, E.APPS_WEB_FILE_FRAME)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_contains(share_rw)
        row_unzip, unzip_locator = self.find_file_locator(share_rw, share_rw, 'folder')
        self.double_click_folder(unzip_locator, E.APPS_WEB_FILE_PATH, 'Shares > {0} > {1}'.format(share_rw, share_rw))
        for suffix in range(1, 5):
            self.current_frame_contains(textfile+str(suffix))
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

    def untar_files(self):
        self.log.info('### Testing untar files ###')
        self.clean_share(share_rw)
        self.log.info('Creating a tar file:{0} with 2 text files:{1}, {2}'.format(tarfile, textfile+'1', textfile+'2'))
        for suffix in range(1, 3):
            cmd = 'echo "{0}" > ./{1}.txt'.format(textfile+str(suffix), textfile+str(suffix))
            os.system(cmd)
        cmd = 'tar -cvf {0}.tar ./{1}.txt ./{2}.txt --remove-files'.format(tarfile, textfile+'1', textfile+'2')
        os.system(cmd)
        self.write_files_to_smb(files='{0}.tar'.format(tarfile), username=username, share_name=share_rw, delete_after_copy=True)
        
        share_locator = self.find_share_locator(share_rw)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        row, file_locator = self.find_file_locator('{}.tar'.format(tarfile), share_rw, 'file')
        self.open_context_menu_and_check(file_locator, E.APPS_WEB_FILE_MENU_UNTAR)
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MENU_UNTAR, E.APPS_WEB_FILE_FRAME)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_contains(tarfile)
        row_untar, untar_locator = self.find_file_locator(tarfile, share_rw, 'folder')
        self.double_click_folder(untar_locator, E.APPS_WEB_FILE_PATH, 'Shares > {0} > {1}'.format(share_rw, tarfile))
        for suffix in range(1, 3):
            self.current_frame_contains(textfile+str(suffix)+'.txt')
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

    def open_files(self):
        self.log.info('### Testing open files ###')
        self.clean_share(share_rw)
        self.log.info('Creating a file:{0} with unsupport format in share folder:{1}.'.format(unsupport_file, share_rw))
        cmd = 'touch > /shares/{0}/{1}.bin'.format(share_rw, unsupport_file)
        self.execute(cmd)
        self.log.info('Creating a hidden file:{0} and hidden folder:{1} in share folder:{2}'.format(hidden_file, hidden_folder, share_rw))
        cmd = 'touch {}'.format(hidden_file)
        self.execute(cmd)
        cmd = 'mkdir {}'.format(hidden_folder)
        self.execute(cmd)
        self.log.info('Check if the hidden file/folder are invisible in Web File Viewer')
        share_locator = self.find_share_locator(share_rw)
        self.double_click_folder(share_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share_rw))
        self.current_frame_should_not_contain(hidden_file)
        self.current_frame_should_not_contain(hidden_folder)
        self.log.info('Check unsupported file format cannot be opened, the link should be disabled.')
        row, file_locator = self.find_file_locator('{}.bin'.format(unsupport_file), share_rw, 'file')
        self.open_context_menu_and_check(file_locator, E.APPS_WEB_FILE_MENU_OPEN)
        self.element_should_be_visible('css=#myMenu.contextMenu li.open.disabled')
        
    def create_text_file_in_share(self, share_name, file_name, text):
        self.log.debug('Creating a text file: {0} in share folder: {1}, text is: {2}.'.format(file_name, share_name, text))
        cmd = 'echo "{0}" > /shares/{1}/{2}.txt'.format(text, share_name, file_name)
        self.execute(cmd)

    def get_text_file_content(self, share_name, file_name):
        self.log.debug('Getting text content in file: {0} in share: {1}'.format(file_name, share_name))
        cmd = 'cat /shares/{0}/{1}.txt'.format(share_name, file_name)
        result = self.execute(cmd)
        if result[0] != 0:
            raise Exception('Get text content failed! Return value: {}'.format(result[0]))
        return result[1]
    
    def find_share_locator(self, share_name):
        # Use to find parent shares
        share_info = self.get_all_shares()
        if share_info[0] != 0:
            raise Exception('Get shares info from Rest API failed! Return value: {}'.format(usb_info[0]))
        share_name_list = self.get_xml_tags(share_info[1], 'share_name')
        self.log.debug('Current share list: {}'.format(share_name_list))
        if share_name_list:
            for i, share in enumerate(share_name_list):
                locator_temp = 'css=#row{} > td > div > span.table_folder > span'.format(i+1)
                share_name_temp = self.get_text(locator_temp)
                if share_name_temp == share_name:
                    return locator_temp
            raise Exception('Non of the share folder name matches "{}"'.format(share_name))
        else:
            raise Exception('There are no share folders exist!')

    def find_file_locator(self, file_name, parent_share, type):
        # Use to find sub shares or files under parent shares
        # @type: can be 'folder' or 'file'
        file_list = self.execute('ls /shares/{}/'.format(parent_share))
        if file_list[0] != 0:
            raise Exception('Unable to list files from share folder:{}'.format(parent_share))
        self.log.debug('Current file list: {}'.format(file_list))
        if file_list[1]:
            files = file_list[1].split()
            for row_index in xrange(1, len(files)+1):
                row = 'row{}'.format(row_index)
                row_info = self.get_text(row).split()
                # row info format: [u'Public', u'Folder', u'2015-07-01', u'00:50:11']
                if row_info[0] == file_name:
                    if (type == 'folder' and row_info[1] != 'Folder') or (type == 'file' and row_info[1] == 'Folder'):
                        continue
                    else:
                        locator = 'css=#{0} > td > div > span.table_{1} > span'.format(row, type)
                        return row, locator
            raise Exception('Non of the file name matches "{}"'.format(file_name))
        else:
            raise Exception('There are no files exist in share folder:{}!'.format(parent_share))

    def double_click_folder(self, locator, check_locator, expected_path, retry=3):
        while retry >= 0:
            self.log.debug('Double clicking share folder:"{0}" and check the text of element:"{1}" is "{2}"'.
                           format(locator, check_locator.name, expected_path))
            try:
                self.double_click_element(locator)
                current_path = self.get_text(check_locator.locator)
                if current_path != expected_path:
                    raise Exception('Current path is:{0} and not match expected_path:{1}!'.format(current_path, expected_path))
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not double click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def click_and_check_attribute(self, locator, check_locator, attribute, expected_value, retry=3):
        while retry >= 0:
            self.log.debug('Clicking element:"{0}"\nand check the "{1}" attribute of locator "{2}" is "{3}"'.
                           format(locator, attribute, check_locator, expected_value))
            try:
                self.click_element(locator)
                self.check_attribute(check_locator, expected_value, attribute)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def select_frame_check(self, frame, check_locator, retry=3):
        while retry >= 0:
            self.log.debug('Select frame:{0} and check the element:"{1}" is visible'.
                           format(frame.name, check_locator.name))
            try:
                self.select_frame(frame)
                self.wait_until_element_is_visible(check_locator)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Select frame:{0} failed, remaining {1} retries...'.format(frame.name, retry))
                self.unselect_frame()
                retry -= 1
                continue
            else:
                break

    def click_unselect_frame_check(self, locator, frame, check_locator='', retry=3):
        while retry >= 0:
            self.log.debug('Clicking element:"{0}", unselect frame and check the element:"{1}" is "{2}"'.
                           format(locator.name, check_locator.name if check_locator else locator.name, 'visible' if check_locator else 'invisible'))
            try:
                self.wait_and_click(locator.locator)
                self.unselect_frame()
                if not check_locator:
                    self.wait_until_element_is_not_visible(locator.locator)
                else:
                    self.wait_until_element_is_visible(check_locator.locator)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator.name, retry))
                self.unselect_frame()
                self.select_frame(frame)
                retry -= 1
                continue
            else:
                break

    def click_coordinates_and_check_attribute(self, locator, xoffset, yoffset, check_locator, attribute, expected_value, retry=3):
        while retry >= 0:
            self.log.debug('Clicking at x:{0}, y:{1} coordinates of element:"{2}" and check the "{3}" attribute of locator "{4}" is "{5}"'.
                           format(xoffset, yoffset, locator, attribute, check_locator.name, expected_value))
            try:
                self.click_element_at_coordinates(locator, xoffset, yoffset)
                self.check_attribute(check_locator.locator, expected_value, attribute)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def click_and_check_path(self, locator, check_locator, expected_path, retry=3):
        while retry >= 0:
            self.log.debug('Clicking element "{0}", check "{1}" is "{2}"'.
                           format(locator, check_locator.name, expected_path))
            try:
                self.wait_and_click(locator)
                current_path = self.get_text(check_locator.locator)
                if current_path != expected_path:
                    raise Exception('Current path is:{0} and not match expected_path:{1}!'.format(current_path, expected_path))
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def get_absolute_path(self, share_name):
        self.log.debug('Checking the absolute path of share:{0}'.format(share_name))
        result = self.execute('readlink -f /shares/{0}'.format(share_name))
        if result[0] != 0:
            raise Exception('Get absolute path of share:{0} failed! Return value: {1}'.format(share_name, result[0]))
        return result[1]

    def get_select_class(self, row):
        # If the file/folder is selected, the odd row class will be 'trSelected', 
        # and the even row class will be 'erow trSelected'
        if int(row.split('row')[1])%2 == 0:
            select_class = 'erow trSelected'
        else:
            select_class = 'trSelected'
        return select_class
        
    def clean_share(self, share_name, folder_list=[]):
        self.log.debug('Cleaning share folder: {}'.format(share_name))
        result = self.execute('ls /shares/{}'.format(share_name))
        if result[0] != 0:
            raise Exception('Check share folders failed!')
        else:
            if result[1]:
                if folder_list:
                    for folder in folder_list:
                        self.execute('rm -r /shares/{0}/{1}'.format(share_name, folder))
                self.execute('rm /shares/{}/*'.format(share_name))
            else:
                self.log.debug('No files or folders need to be deleted in share folder:{}.'.format(share_name))

    def delete_local_file(self, file_name):
        if os.path.exists(file_name):
            self.log.debug('Removing local file:{}'.format(file_name))
            os.system('rm {}'.format(file_name))

webFileViewerUser()