""" Users page-Set up Groups
    @Author: Lee_e
    
    Objective: Setup group and check quota of group should not smaller than user
    
    Automation: Full
    
    Supported Products:
        All

    Test Status: 
              Local Lightning: Pass (FW: 2.00.176)
                    KC       : Pass (FW: 2.00.155)
              Jenkins Taiwan : Lightning    - pass (FW: 2.00.205)
                             : Yosemite     - pass (FW: 2.00.205)
                             : Kings Canyon - pass (FW: 2.00.205)
                             : Sprite       - pass (FW: 2.00.205)
              Jenkins Irvine : Zion         - pass (FW: 2.00.205)
                             : Aurora       - pass (FW: 2.00.205)
                             : Glacier      - pass (FW: 2.00.200)
                             : Yellowstone  - pass (FW: 2.00.205)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

user ='user1'
groupname = 'a1'
user_amount = '10240'
group_amount = '5'
user_password = 'welc0me'
picture_counts = 0

class SetUpGroups(TestClient):
    def run(self):
        self.delete_all_users(use_rest=False)
        self.delete_all_groups()
        self.add_user(user, user_amount, user_password)
        self.create_group_quota(group_name=groupname, memberusers=user)
        result = self.check_group_from_UI(group_name=groupname)

        if result:
            self.log.info("PASS: Group '{0}' correctly setup and shows on UI".format(groupname))
        else:
            self.log.error("FAIL: Group '{0}' dose not shows on UI".format(groupname))
        
        result = self.check_group_quota_set_info(groupname, group_amount, capacity='GB')
        if not result:
            self.log.info("PASS: Group amount error info correct shows on UI")
        else:
            self.log.error("FAIL: Wrong group error info shows on UI")          
     
    def tc_cleanup(self):
        self.log.info('delete user')
        self.execute("account -d -u '{0}'".format('user1'))
        self.log.info('delete group')
        self.execute("account -d -g '{0}'".format('a1'))
    
    def add_user(self, username=None, user_quota=None, password=None):
        self.log.info('*** Testing status: Add User ***')
        self.execute("account -a -u '{0}' -p '{1}'".format(username, password))
        self.execute("/usr/sbin/quota_set -u {0} -s {1} -a".format(username, user_quota))
    
    def create_group_quota(self, numberOfgroups=1, group_name=None, memberusers=None):
        self.log.info('*** Testing status: Create Group & assign Quota ***')
        self.create_groups(group_name, numberOfgroups, memberusers)
        self.execute("/usr/sbin/quota_set -g '{0}' -s '{1}' -a".format(group_name, 20480))
        self.log.info("command :/usr/sbin/quota_set -g '{0}' -s {1} -a".format(group_name, 20480))
    
    def check_group_from_UI(self, group_name):
        self.log.info('*** Testing status: Check Group From UI ***')
        self.get_to_page('Users')
        self.wait_until_element_is_visible(self.Elements.GROUPS_BUTTON, timeout=5)
        self.click_wait_and_check(self.Elements.GROUPS_BUTTON, 'users_group_'+str(group_name))
        result = self.is_element_visible('users_group_'+str(group_name))
        
        return result
    
    def check_group_quota_set_info(self, group_name=None, group_quota=None, capacity=None):
        self.log.info('*** Testing status: Check Group Quota Setting Info ***')
        result = 0
        self.click_wait_and_check('users_group_'+str(group_name), 'css=#users_editGroupQuota_link > span._text')
        self.click_wait_and_check('css=#users_editGroupQuota_link > span._text', 'users_v1Size_text')
        self.input_text_check('users_v1Size_text', str(group_quota))
        self.click_wait_and_check('quota_unit_1', 'link='+str(capacity))
        self.click_wait_and_check('link='+str(capacity), 'users_editQuotaSave_button')
        self.click_wait_and_check('users_editQuotaSave_button', 'error_dialog_message_list')
        time.sleep(5)
        try:
            self.element_text_should_be('error_dialog_message_list', expected_text='The quota amount cannot be smaller than the user quota amount.(10GB)')
        except Exception as e:
            self.log.error('Exception error:{0}'.format(repr(e)))
            self.screenshot('check_group_quota_set_info')
            result = 1
        self.click_wait_and_check('popup_ok_button', 'users_editQuotaCancel_button')
        self.click_button('users_editQuotaCancel_button')        
        
        return result

    def screenshot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1
        
SetUpGroups()