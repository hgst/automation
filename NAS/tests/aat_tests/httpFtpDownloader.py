"""
Created on Jan 12th, 2015

@author: tran_jas

## @brief User shall able to create HTTP and FTP downloads via web UI
#
#  @detail 
#     HTTP case 1: schedule http download job to start in 15 minutes, then click Start button
#     HTTP case 2: schedule http download job to start in 1 hour, modify time and location
#        Verify daily, weekly and monthly backup  
#     FTP case 1: Download file with authentication, no schedule
#     FTP case 2: Download folder with no authentication, scheduled

"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

httpFile = 'http://go.microsoft.com/fwlink/?LinkId=324638'
httpFileSum = '1eda16a52b3c855cb80e9de4b0a78eac'
httpFileName = 'EIE11_EN-US_MCM_WIN764.EXE'
"""
httpFile2 = 'http://go.microsoft.com/fwlink/?LinkId=302184'
httpFileSum2 = '922524575ca11babc6b8cdcb2d521c39'
httpFileName2 = 'WindowsBlue-ClientwithApps-32bit-Swedish-X1899626.iso'
"""
httpFile2 = 'http://go.microsoft.com/fwlink/p/?LinkId=522100'
httpFileSum2 = '8d0e461198854496502dc0293ea13d7a'
httpFileName2 = 'Windows10_TechnicalPreview_x64_EN-US_9926.iso'

# Local FTP
# ftpServerFolder = 'ftp://192.168.1.124/Public/FTPTest'
# ftpServerFile = 'ftp://192.168.1.124/ftpshare/AFP-74BD.mp4'

# Lab FTP
ftpServerFolder = 'ftp://192.168.11.39/Public/FTPTest'
ftpServerFile = 'ftp://192.168.11.39/ftpshare/AFP-74BD.mp4'

ftpFilename = 'AFP-74BD.mp4'
ftpFilePath = '/shares/Public/'
httpFilePath = '/shares/SmartWare/'
ftpuser = 'ftpuser'
ftpPassword = 'fituser'
filemd5sum = 'b776a446fa43d7b735854de315cbb840'
filemd5sum2 = '185236549de96b693b6cd7b2afb25427'
ftpFilename2 = 'diaryofacamper_raw.mp4'
successfulText = 'Successful'
rmEXE = 'rm /shares/SmartWare/*.EXE'
rmEXEpub = 'rm /shares/Public/*.EXE'
rmISO = 'rm /shares/Public/*.iso'
rmJpg = 'rm /shares/Public/*.jpg'
rmMp4 = 'rm /shares/Public/*.mp4'


class HttpFtpDownloader(TestClient):
    # Create tests as function in this class
    def run(self):

        self.log.info('httpFtpDownloader started')
        self.set_web_access_timeout()
        self.http_downloader()
        self.http_downloader_recurring()
        self.ftp_downloader()
        self.ftp_downloader_scheduled()
        self.set_web_access_timeout('5')

    # HTTP Downloader
    # Test case 1: schedule http download job to start in 15 minutes, then click Start button
    def http_downloader(self):

        self.get_to_page('Apps')
        self.click_element('apps_httpdownloads_link')
        self.click_element('apps_httpdownloadsLoginMethodAnonymous_button')
        time.sleep(3)
        self.input_text('apps_httpdownloadsURL_text', httpFile2, True)
        time.sleep(3)
        self.click_element('apps_httpdownloadsTest_button')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('apps_httpdownloadsTestClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successfulText:
            self.log.info('Valid URL was entered: {0}'.format(httpFile2))
            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            time.sleep(5)
            self.click_element('home_treeOk_button')

            # Disable NTP and set date time to 1:45AM
            self.log.info('Disable NTP and set date')
            self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                             time_zone_name='US/Pacific')
            self.execute('date -s 2015.01.05-01:45:00')

            # Schedule download at 2AM (15 minutes schedule)
            self.click_element('f_hour')
            self.click_element('link=2AM')
            self.click_element('f_min')
            self.click_element('link=00')
            self.click_element('apps_httpdownloadsCreate_button')
            time.sleep(3)

            # click Start button
            while self.is_element_visible('css=div.tip.tip_backup_wait > img'):
                self.log.info('Start http download job...')
                self.click_element('//tr[@id=\'row1\']/td[7]/div/a/img')
                time.sleep(10)

            self.log.info("4 minutes wait for downloading...")
            time.sleep(240)

            # refresh UI
            self.click_element('apps_httpdownloads_link')

            loop_count = 0
            while (self.is_element_visible('css=div.tip.tip_backup_success > img') is False) and (loop_count < 10):
                self.log.info("Waiting for successful icon...")
                loop_count += 1
                time.sleep(60)
                self.click_element('apps_httpdownloads_link')
                time.sleep(10)

            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('http download successful image is displayed')

            time.sleep(30)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(ftpFilePath, httpFileName2)

            if httpFileSum2 == httpdownloadedfilesum:
                self.log.info('HTTP download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            self.log.info('Delete downloaded file')
            self.execute(rmISO)
            
            self.log.info('Delete http job')
            if self.is_element_visible('apps_ftpdownloads_link'):
                self.click_element('apps_ftpdownloads_link')
            time.sleep(3)
            if self.is_element_visible('apps_httpdownloads_link'):
                self.click_element('apps_httpdownloads_link')
            time.sleep(10)
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsDel_button')
                time.sleep(3)
                self.log.info('Failed to select http job, close error message then try again')
                while self.is_element_visible('popup_ok_button'):
                    self.click_button('popup_ok_button')
                    time.sleep(3)
                    if self.is_element_visible('apps_httpdownloads_link'):
                        self.click_element('apps_httpdownloads_link')
                        time.sleep(10)
                        self.click_element('//tr[@id=\'row1\']/td[2]/div')
                        self.click_element('apps_httpdownloadsDel_button')
                        time.sleep(3)
                        self.click_element('popup_apply_button')
                        time.sleep(3)
                        while self.is_element_visible('popup_apply_button'):
                            self.click_button('popup_apply_button')
                            time.sleep(3)
            
                while self.is_element_visible('popup_apply_button'):
                    self.click_button('popup_apply_button')
                    time.sleep(3)

        else:
            self.log.error('Invalid URL was entered: {0}'.format(httpFile2))

    # HTTP Downloader        
    # Test case 2: schedule http download job to start in 1 hour, modify time and location
    # Verify daily, weekly and monthly backup
    def http_downloader_recurring(self):

        # Disable NTP and set date time to 1:00AM
        self.log.info('Disable NTP and set date')
        self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                         time_zone_name='US/Pacific')
        self.execute('date -s 2015.01.05-01:00:00')
        self.log.info('Current date time - before change schedule: {0}'.format(self.execute('date')))

        self.get_to_page('Apps')
        self.click_element('apps_httpdownloads_link')
        self.click_element('apps_httpdownloadsLoginMethodAnonymous_button')

        self.input_text('apps_httpdownloadsURL_text', httpFile, True)
        self.click_element('apps_httpdownloadsTest_button')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('apps_httpdownloadsTestClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successfulText:
            self.log.info('Valid URL was entered: {0}'.format(httpFile))
            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            self.click_element('home_treeOk_button')

            # Schedule download at 2AM (1 hour schedule)
            self.click_element('f_hour')
            self.click_element('link=2AM')
            self.click_element('f_min')
            self.click_element('link=00')
            self.click_element('apps_httpdownloadsCreate_button')
            time.sleep(3)

            # Modify time and location (SmartWare folder)
            if self.is_element_visible('css=div.tip.tip_backup_wait > img'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')

            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/label/span', 120)
            self.click_element('home_treeOk_button')

            self.click_element('f_hour')
            self.click_element('link=1AM')
            self.click_element('f_min')
            self.click_element('link=05')
            self.click_element('apps_httpdownloadsSave_button')

            # wait for successful image
            self.wait_until_element_is_visible('css=div.tip.tip_backup_success > img', 180)
            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('http download successful image is displayed')

            time.sleep(15)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(httpFilePath, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP scheduled download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP scheduled download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rmEXE)

            # Modify to daily back up
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')
            time.sleep(3)

            # Turn on Recurring Backup
            if not self.is_element_visible('apps_httpdownloadsPeriodDay_button'):
                self.log.info('Recurring switch is not turn on, turn it on')
                self.click_element('css=#apps_httpdownloadsPeriod_switch+span .checkbox_container')

            self.click_element('apps_httpdownloadsPeriodDay_button')
            # self.click_element('f_period_hour')
            # self.click_element('link=1AM')
            self.click_element('f_period_min')
            self.click_element('link=05')
            self.click_element('apps_httpdownloadsSave_button')
            time.sleep(5)

            self.log.info('time before set: {0}'.format(self.execute('date')))
            self.execute('date -s 2015.01.06-12:03:00')
            self.log.info('time after set: {0}'.format(self.execute('date')))

            # will not able ot use successful image, as it will disappear quickly after download
            self.log.info('Waiting for timer to kick off backup...')
            time.sleep(300)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(httpFilePath, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP daily download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP daily download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rmEXE)

            # Weekly backup
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')
            time.sleep(3)

            # Select weekly
            self.click_element('apps_httpdownloadsPeriodWeek_button')
            # self.click_element('f_period_hour')
            # self.click_element('link=1AM')
            self.click_element('f_period_min')
            self.click_element('link=05')
            self.click_element('f_period_week')
            self.click_element('link=Mon')
            self.click_element('apps_httpdownloadsSave_button')
            time.sleep(3)

            self.log.info('time before set: {0}'.format(self.execute('date')))
            self.execute('date -s 2015.01.12-12:03:00')
            self.log.info('time after set: {0}'.format(self.execute('date')))

            # will not able ot use successful image, as it will disappear quickly after download
            self.log.info('Waiting for timer to kick off backup...')
            time.sleep(300)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(httpFilePath, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP weekly download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP weekly download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rmEXE)

            # Monthly backup
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')
            time.sleep(3)

            # Select monthly
            self.click_element('apps_httpdownloadsPeriodMonth_button')
            # self.click_element('f_period_hour')
            # self.click_element('link=1AM')
            self.click_element('f_period_min')
            self.click_element('link=05')
            self.click_element('f_period_month')
            self.click_element('link=01')
            self.click_element('apps_httpdownloadsSave_button')
            time.sleep(3)

            self.log.info('time before set: {0}'.format(self.execute('date')))
            self.execute('date -s 2015.02.01-12:03:00')
            self.log.info('time after set: {0}'.format(self.execute('date')))

            # will not able ot use successful image, as it will disappear quickly after download
            self.log.info('Waiting for timer to kick off backup...')
            time.sleep(300)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(httpFilePath, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP monthly download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP monthly download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rmEXE)

            self.log.info('Delete http job')
            if self.is_element_visible('apps_ftpdownloads_link'):
                self.click_element('apps_ftpdownloads_link')
            time.sleep(3)
            if self.is_element_visible('apps_httpdownloads_link'):
                self.click_element('apps_httpdownloads_link')
            time.sleep(10)
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsDel_button')
                time.sleep(3)
                self.log.info('Failed to select http job, close error message then try again')
                while self.is_element_visible('popup_ok_button'):
                    self.click_button('popup_ok_button')
                    time.sleep(3)
                    if self.is_element_visible('apps_httpdownloads_link'):
                        self.click_element('apps_httpdownloads_link')
                        time.sleep(10)
                        self.click_element('//tr[@id=\'row1\']/td[2]/div')
                        self.click_element('apps_httpdownloadsDel_button')
                        time.sleep(3)
                        self.click_element('popup_apply_button')
                        time.sleep(3)
                        while self.is_element_visible('popup_apply_button'):
                            self.click_button('popup_apply_button')
                            time.sleep(3)

                while self.is_element_visible('popup_apply_button'):
                    self.click_button('popup_apply_button')
                    time.sleep(3)
            """
            # delete http job
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsDel_button')

                # Failed to select http job, close error message then try again
                if self.is_element_visible('popup_ok_button'):
                    self.click_button('popup_ok_button')
                    time.sleep(3)
                    self.click_element('apps_httpdownloads_link')
                    time.sleep(3)
                    self.click_element('//tr[@id=\'row1\']/td[2]/div')
                    self.click_element('apps_httpdownloadsDel_button')
                    time.sleep(3)
                    self.click_element('popup_apply_button')
                else:
                    self.click_element('popup_apply_button')
                time.sleep(5)
            """

        else:
            self.log.error('Invalid URL was entered: {0}'.format(httpFile))

        # Enable NTP and change time
        self.set_date_time_configuration(datetime='1421057768', ntpservice=True, ntpsrv0='time.windows.com',
                                         ntpsrv1='pool.ntp.org', ntpsrv_user='', time_zone_name='US/Pacific')

    # FTP downloader                
    # Test case 1: Download file with authentication, no schedule
    def ftp_downloader(self):
        self.get_to_page('Apps')
        self.is_element_visible('apps_ftpdownloads_link')
        self.click_element('apps_ftpdownloads_link')
        self.click_element('FDownlaods_Login_Method_Account')

        self.input_text('f_user', ftpuser, True)
        self.input_text('f_pwd', ftpPassword, True)
        self.input_text('f_URL', ftpServerFile, True)
        time.sleep(3)
        self.click_element('But_FDownloads_Test')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('Apps_FTPDownloadClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successfulText:

            self.click_element('But_FDownloads_Browse')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            self.click_element('home_treeOk_button')
            self.click_element('But_FDownloads_Create')

            # wait for successful image
            self.wait_until_element_is_visible('css=div.tip.tip_backup_success > img', 180)
            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('ftp download successful image is displayed')

            time.sleep(10)

            # Verify download successfully via checksum comparison
            ftpdownloadedfilesum = self.md5_checksum(ftpFilePath, ftpFilename)
            if filemd5sum == ftpdownloadedfilesum:
                self.log.info('FTP file download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
            else:
                self.log.error('FTP file download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

            # Delete downloaded file
            self.execute(rmMp4)

            # Delete FTP download job
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('But_FDownloads_Del')

                # Failed to select FTP job, close error message then try again
                if self.is_element_visible('popup_ok_button'):
                    self.click_button('popup_ok_button')
                    self.click_element('//tr[@id=\'row1\']/td[2]/div')
                    self.click_element('But_FDownloads_Del')
                    self.click_element('popup_apply_button')
                else:
                    self.click_element('popup_apply_button')
                time.sleep(3)
        else:
            self.log.error('Invalid FTP download link was entered')

    # FTP downloader                
    # Test case 2: Download folder with no authentication, scheduled
    def ftp_downloader_scheduled(self):

        # Disable NTP and change time
        self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                         time_zone_name='US/Pacific')
        self.execute('date -s 2015.01.05-01:56:00')

        self.get_to_page('Apps')
        self.is_element_visible('apps_ftpdownloads_link')
        self.click_element('apps_ftpdownloads_link')
        self.click_element('FDownlaods_Login_Method_Anonymous')
        self.click_element('FDownloads_f_type')
        self.click_element('//ul[@id=\'FDownloads_f_type_li\']/div/li[2]/a')
        self.input_text('f_URL', ftpServerFolder, True)
        time.sleep(3)
        self.click_element('But_FDownloads_Test')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('Apps_FTPDownloadClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successfulText:
            self.click_element('But_FDownloads_Browse')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            self.click_element('home_treeOk_button')
            self.click_element('FDownloads_f_hour')
            self.click_element('link=2AM')
            self.click_element('FDownloads_f_min')
            self.click_element('link=00')
            self.click_element('But_FDownloads_Create')
            time.sleep(2)

            # Modify scheduled download, change time
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('But_FDownloads_Modify')
            self.click_element('FDownloads_f_min')
            self.click_element('link=01')
            self.click_element('But_FDownloads_Save')

            # wait for successful image
            self.wait_until_element_is_visible('css=div.tip.tip_backup_success > img', 180)
            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('ftp download successful image is displayed')

            time.sleep(10)

            # Verify download successfully via checksum comparison
            ftpdownloadedfilesum = self.md5_checksum(ftpFilePath, ftpFilename2)

            if filemd5sum2 == ftpdownloadedfilesum:
                self.log.info('FTP folder download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
            else:
                self.log.error('FTP folder download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

            # Delete downloaded files
            self.execute(rmMp4)
            self.execute(rmJpg)

            # Delete FTP download job
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('But_FDownloads_Del')

                # Failed to select FTP job, close error message then try again
                if self.is_element_visible('popup_ok_button'):
                    self.click_button('popup_ok_button')
                    self.click_element('//tr[@id=\'row1\']/td[2]/div')
                    self.click_element('But_FDownloads_Del')
                    self.click_element('popup_apply_button')
                else:
                    self.click_element('popup_apply_button')

        else:
            self.log.error('Invalid FTP download link was entered')

        # Enable NTP and change time
        self.set_date_time_configuration(datetime='1421057768', ntpservice=True, ntpsrv0='time.windows.com',
                                         ntpsrv1='pool.ntp.org', ntpsrv_user='', time_zone_name='US/Pacific')


HttpFtpDownloader()