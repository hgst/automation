"""
Created on Dec 21, 2017

@author: tsui_b

## @brief Stress reboot test
#  Reboot device for many iterations
#
#  Using serial connection because test unit might be in bad state
#   where ssh, network are not working
"""

import time
import os
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries import wd_localfilesystem as wdlfs

reboot_command = '/usr/sbin/do_reboot'
seq_reboot_command = 'reboot'
get_vols_state = 'cat /var/www/xml/sysinfo.xml | grep state'
# prompt
seq_waiting_string = '~#'
waiting_string = '/ #'
waitingStringGlacier = 'WDMyCloud / #'
iterations = 150

class RebootStress(TestClient):

    def run(self):
        self.open_serial_connection()
        passed_counts = 0
        failed_counts = 0
        for i in range(iterations):
            self.log.info("Running iteration: {}".format(i+1))
            try:
                self.do_reboot()
                if not self.checked_raid_status():
                    self.log.error('RAID status is not healthy, test failed!')
                    failed_counts += 1
                else:
                    self.log.info('Reboot test successful!')
                    passed_counts += 1
            except Exception as e:
                self.log.error('Reboot failed, error message: {}'.format(repr(e)))
                failed_counts += 1

        self.log.info('*** Test run total {} iterations ***'.format(iterations))
        self.log.info('Passed: {}'.format(passed_counts))
        if failed_counts > 0:
            self.log.error('Failed: {}'.format(failed_counts))
        else:
            self.log.info('Failed: {}'.format(failed_counts))

    def do_reboot(self):
        max_boot_time = self.uut[Fields.reboot_time]

        if self.uut[self.Fields.product] == 'Sequoia':
            self.serial_wait_for_string(seq_waiting_string, 30)
            self.serial_write(seq_reboot_command)
            self.log.info('Rebooting: {0}'.format(seq_reboot_command))
            self.serial_wait_for_string('The system is going down for reboot NOW!', 60)
            time.sleep(120)
        else:
            self.serial_wait_for_string(waiting_string, 30)
            self.serial_write(reboot_command)
            self.log.info('Rebooting: {0}'.format(reboot_command))

            # Wait for the shutdown to finish
            start = time.time()
            self.log.info('Device is powering off for reboot')
            while self.is_ready():
                time.sleep(1)
                if time.time() - start >= max_boot_time:
                    raise Exception('Device failed to power off within {} seconds'.format(max_boot_time))

        # Now, wait for the system to come back up.
        self.log.info('Waiting for device to power back up')
        start_time = time.time()
        while not self.is_ready():
            time.sleep(5)
            self.log.debug('Web is not ready')
            if time.time() - start_time > (max_boot_time+180):
                raise Exception('Timed out waiting to boot within {} seconds'.format(max_boot_time+180))
        self.log.info('Web is ready')

    def open_serial_connection(self):
        # Open serial connection
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))

    def checked_raid_status(self):
        # Checked the RAID status is in degraded or not.
        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write(get_vols_state)
        self.log.info('get_vols_state: {0}'.format(get_vols_state))
        self.serial_wait_for_string('state', 20)
        get_state = self.serial_read()
        self.log.info('RAID status is : {0}'.format(get_state))
        if 'degraded' in str(get_state):
            return False
        else:
            return True

# instantiate class without running through the basic checks (in case the unit is stuck)
RebootStress()
