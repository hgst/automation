""" Settings page - Network - Workgroup (MA-194)

    @Author: vasquez_c
    Procedure @ silk:
                1) Open the webUI -> login as system admin
                2) Click on the Settings category
                3) Select Network -> Workgroup
                4) Verify that workgroup can be renamed
                5) Verify that local master browser can be On or Off; Workgroup name can be changed
                6) Verify the SMB Protocol can be changed
                

    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.216)
"""             
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as E
import time
import random
import string


class settingsNetworkWorkgroup(TestClient):
    
    def run(self):
        self.workgroup_rename_verification()
        self.master_browser_toggle()
        self.SMB_protocol_verification()
        
    def workgroup_rename_verification(self):    
        # Rename Workgroup
        randomString = ''.join(random.choice(string.ascii_uppercase) for i in range(12))
    
        self.click_element(E.SETTINGS_NETWORK_WORKGROUP_TEXT,timeout=60)
        originalWorkgroup = self.get_value(E.SETTINGS_NETWORK_WORKGROUP_TEXT)
        
        self.input_text(E.SETTINGS_NETWORK_WORKGROUP_TEXT, text=randomString)
        self.wait_and_click(E.SETTINGS_NETWORK_WORKGROUP_SAVE)
        self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        
        updatedWorkgroup = self.get_value(E.SETTINGS_NETWORK_WORKGROUP_TEXT)
        if updatedWorkgroup == randomString:
            self.log.info('Network workgroup was updated succesfully')
        else:
            self.log.error('Failed to update workgroup')
        
    def master_browser_toggle(self):
        self.click_element(E.SETTINGS_NETWORK_WORKGROUP_TEXT,timeout=60)
        master_browser_text = self.get_text('//table[@id="wins_service"]/tbody/tr[2]/td[2]/table/tbody/tr/td/span/div')
       
        if master_browser_text == 'OFF':
            self.click_button('//table[@id="wins_service"]/tbody/tr[2]/td[2]/table/tbody/tr/td/span/div')
            self.element_text_should_be('//table[@id="wins_service"]/tbody/tr[2]/td[2]/table/tbody/tr/td/span/div', 'ON')
        else:
            self.click_button('//table[@id="wins_service"]/tbody/tr[2]/td[2]/table/tbody/tr/td/span/div')
            self.element_text_should_be('//table[@id="wins_service"]/tbody/tr[2]/td[2]/table/tbody/tr/td/span/div', 'OFF')   
         
        self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        if master_browser_text <> self.get_text('//table[@id="wins_service"]/tbody/tr[2]/td[2]/table/tbody/tr/td/span/div'):
            self.log.info('Master Browser was updated successfully')
        else:
            self.log.error('Failed to update Master Browser')     
        self.workgroup_rename_verification()
        
    def SMB_protocol_verification(self):
        smb_protocol_text = self.get_text('settings_networkMaxSmbProtocol_select')
        if smb_protocol_text == 'SMB 1':
            self.click_element('settings_networkMaxSmbProtocol_select')
            self.click_element('link={}'.format('SMB 2'))
        else:
            self.click_element('settings_networkMaxSmbProtocol_select')
            self.click_element('link={}'.format('SMB 1'))
        self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        
        if smb_protocol_text <> self.get_text('settings_networkMaxSmbProtocol_select'):
            self.log.info('SMB Protocol was updated successfully')
        else:
            self.log.error('Failed to update SMB Protocol')                
settingsNetworkWorkgroup()
        
        
            