"""
title           :sharePageUserReadWrite.py
description     : Shares Page - to verify if a user can be authorized 'Read Write' access to a given folder
step            : 1. Click on 'share' category
                  2. Select a share (public = OFF)
                  3. Select a user, and give 'Read Write' privilege
                  4. Verify if this user can access the share with read/write privilege
author          :yang_ni
date            :2015/06/12
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time,os
from smb.smb_structs import OperationFailure

picture_counts = 0

class sharePageUserReadWrite(TestClient):

    def run(self):

        try:
            #Delete all the users, groups and shares
            self.setup()

            # Create a user
            self.log.info('Creating a user ...')
            self.create_user(username='user1')

            # Create a share.
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=1)

            # Change share to private, share must be set to private instead of public. Toggle the Public switch off
            self.log.info('Toggling the share from private to public ...')
            self.update_share_network_access(share_name='share', public_access=False)
            #self.update_share()

            #Assigns user1 to share with the read write access level
            self.log.info('Assigning user1 with Read Write access to the share ...')
            self.assign_user_to_share(user_name='user1', share_name='share', access_type='rw')

            #Check if 'Read Write' access of share is given to user1
            self.log.info('Checking if user1 has Read Write access to share ...')

            '''
            Check if user1 has 'Read Write' privilege to share (test on Web UI)
            '''
            time.sleep(20)
            self.get_to_page('Shares')
            time.sleep(5)
            self.click_button('shares_share_share')
            text = self.get_text("//div[@id=\'userlist\']/ul/li[2]/div[4]")

            if text == 'Read / Write':
                self.log.info('User1 has read Write access to share folder')
            else:
                self.log.error('Failed to authorize read Write access of share folder to user1')

            self.close_webUI()

            '''
            Check if user1 has 'Read Write' privilege to share (test by SMB file transfer)
            '''
            test_file = self.generate_test_file(kilobytes=10)

            #Check if user1 has write access to the share folder, fail if yes.
            try:
                self.write_files_to_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.error('FAIL : Fail to write file to share folder')
            else:
                self.log.info('CORRECT : Successfully to write file to share folder')

            #Check if user1 has read access to the share folder, fail if yes.
            try:
                self.read_files_from_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.error('FAIL : Failed to read file from share folder')
            else:
                self.log.info('CORRECT : Successfully to read file from share folder')
                self.log.info('*** Successfully authorizes read write access to user1 ***')
        except Exception as e:
            self.log.error('FAIL : Failed to authorizes read write access to a user1, error: {}'.format(repr(e)))
            self.screenshot(prefix='run')


# # @Clean up all the users/shares/groups before test starts
    def setup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

# # @Clean up all the users/shares/groups after test finishes
    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

    def screenshot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1

sharePageUserReadWrite()