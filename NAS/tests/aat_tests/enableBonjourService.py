"""
Create on Jan 29, 2016
@Author: lo_va
Objective:  Verify Bonjour Service is enabled and auto-start and running w/o error in the firmware
            KAM-390 (M3 feature)
wiki URL: http://wiki.wdc.com/wiki/Enable_Bonjour_Service
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class EnableBonjourService(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        avahi_daemon = ''
        avahi_files_list = ''
        try:
            avahi_daemon = self.execute('ps -ef | grep avahi | grep -v "grep avahi"')[1]
            avahi_files_list = self.execute('find / -name *avahi*')[1]
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['avahi-daemon: running', 'avahi-daemon: chroot helper']
        check_list2 = ['/usr/sbin/avahi-daemon', '/etc/init.d/avahi-daemon', '/var/lib/dpkg/info/avahi-daemon.md5sums']
        if all(word in avahi_daemon for word in check_list1) and all(word in avahi_files_list for word in check_list2):
            self.log.info('Verify Enable Bonjour Service PASSED!!')
        else:
            self.log.error('Verify Enable Bonjour Service FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test!!')
            exit()

EnableBonjourService(checkDevice=False)
