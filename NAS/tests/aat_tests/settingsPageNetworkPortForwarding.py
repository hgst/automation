""" Settings page - Network - Port Forwarding (MA-196)

    @Author: vasquez_c
    Procedure @ silk:
                1) Open the webUI -> login as system admin
                2) Click on the Settings category
                3) Select Network -> Port Forwarding
                4) Click Add
                5) Verify that a port forwarding service can be added
                               

    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.216)
"""             
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
import time
import requests

port_forwarding_list = ['HTTP', 'HTTPS', 'SSH']

class settingsNetworkPortForwarding(TestClient):
    
    def run(self):
        self.config_ftp_access(state=0)
        self.delete_port_forwarding(type_list=['HTTPS', 'HTTP', 'SSH', 'FTP'])
        self.network_workgroup_verification()

    def config_ftp_access(self, state=0):
        self.log.info('Config FTP Access to {0}'.format(state))
        header = 'app_mgr.cgi'
        cmd = 'FTP_Server_Enable&f_state={0}'.format(state)
        self.send_cgi(cmd=cmd, url2_cgi_head=header)

    def tc_cleanup(self):
        self.delete_port_forwarding(type_list=port_forwarding_list)
        self.close_webUI()

    def delete_port_forwarding(self, type_list=None):
        self.log.info('Cleanup Port Forwarding')
        header = 'network_mgr.cgi'

        if 'HTTPS' in type_list:
            self.log.info('Delete Port Forwarding: HTTPS')
            e_port = 443
            p_port = 443
            cmd = 'cgi_portforwarding_del&protocol=TCP&e_port={0}&p_port={1}&service=HTTPS&scan=1'.format(e_port, p_port)
            self.send_cgi(cmd=cmd, url2_cgi_head=header)

        if 'SSH' in type_list:
            self.log.info('Delete Port Forwarding: SSH')
            e_port = 22
            p_port = 22
            cmd = 'cgi_portforwarding_del&protocol=TCP&e_port={0}&p_port={1}&service=SSH&scan=1'.format(e_port, p_port)
            self.send_cgi(cmd=cmd, url2_cgi_head=header)

        if 'HTTP' in type_list:
            self.log.info('Delete Port Forwarding: HTTP')
            e_port = 80
            p_port = 80
            cmd = 'cgi_portforwarding_del&protocol=TCP&e_port={0}&p_port={1}&service=HTTP&scan=1'.format(e_port, p_port)
            self.send_cgi(cmd=cmd, url2_cgi_head=header)

        if 'FTP' in type_list:
            self.log.info('Delete Port Forwarding: FTP')
            port = [20, 21]
            for i in port:
                p_port = i
                e_port = i
                cmd = 'cgi_portforwarding_del&protocol=TCP&e_port={0}&p_port={1}&service=FTP&scan=1'.format(e_port, p_port)
                self.send_cgi(cmd=cmd, url2_cgi_head=header)

    def network_workgroup_verification(self):    
        self.click_element(E.SETTINGS_NETWORK_PORT_FORWARDING_ADD, timeout=60)
        self.click_button(E.SETTINGS_NETWORK_PORT_FORWARDING_NEXT)
        self.select_checkbox('settings_networkPortForService1_chkox')
        self.checkbox_should_be_selected('settings_networkPortForService1_chkox')
        self.select_checkbox('settings_networkPortForService2_chkox')
        self.checkbox_should_be_selected('settings_networkPortForService2_chkox')
        self.select_checkbox('settings_networkPortForService3_chkox')
        self.checkbox_should_be_selected('settings_networkPortForService3_chkox')
        self.click_wait_and_check('settings_networkPortForDefaultSave_button')
        self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        for i in range(1, 4):
            if self.is_element_visible('settings_networkPortForName{0}_value'.format(i)):
                service = self.get_text('settings_networkPortForName{0}_value'.format(i))

            self.log.info('service: {0}'.format(service))
         
            if service in port_forwarding_list:
                self.log.info('Services:{0} were added successfully'.format(service))
            else:
                self.log.error('Services:{0} were not added'.format(service))

    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))

        return r.text.encode('ascii', 'ignore')
              
settingsNetworkPortForwarding()