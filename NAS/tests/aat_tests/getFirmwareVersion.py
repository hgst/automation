import logging
import os
from global_libraries import CommonTestLibrary as ctl
from testCaseAPI.src.testclient import TestClient


class GetFirmwareVersion(TestClient):
    
    def run(self):
        get_firmware = self.get_firmware_version_number()
        self.log.info(get_firmware)
        directory = ctl.get_silk_results_dir()
        outputfile = os.path.join(directory, 'Firmware.txt')
        output = open(outputfile, 'w')
        output.write(get_firmware)
        output.close
            
GetFirmwareVersion()