""" Home page-Device firmware shows firmware info

    @Author: Lee_e
    
    Objective: Firmware info is display
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 1.05.30)
                    Lightning: Pass (FW: 2.00.40)
                    KC       : Pass (FW: 2.00.31)
              Jenkins Lightning: Pass (FW: 2.00.141)
                      Yellowstone: Pass (FW:2.00.141)
                      Glacier: Pass (FW:2.00.144)
                      Yosemite: Pass (FW:2.00.144)
                      Sprite: Pass (FW:2.00.144)
                      Kings Canyon (FW:2.00.145)
                      Aurora: Pass (FW:2.00.145)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

time_out = 60

class DeviceFirmwareShowsFirmwareInfo(TestClient):
    def run(self):    
        firmware_version_rest = self.get_firmware_version_number()
        self.log.info('firmware_version_rest: '+str(firmware_version_rest))
        firmware_version_UI = self.get_firmware_info_from_UI()
        if firmware_version_rest == firmware_version_UI :
            self.log.info('PASS: firmware info correctly shows on home page')
        else:
            self.log.error('FAIL: firmware info uncorrectly-{0}'.format(firmware_version_UI))
       
    def get_firmware_info_from_UI(self):
        self.get_to_page('Home')
        self.wait_until_element_is_visible('home_firmwareInfo_value',timeout=time_out)
        firmware_version_UI = self.get_text('home_firmwareInfo_value')
        self.log.info('firmware_version_UI: '+str(firmware_version_UI))
        return firmware_version_UI  
        
DeviceFirmwareShowsFirmwareInfo()