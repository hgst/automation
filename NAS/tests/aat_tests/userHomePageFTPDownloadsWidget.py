""" User Home Page-FTP Downloads Widget

    @Author: Lee_e
    
    Objective: verify that the user is redirected to the FTP Download page
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.00.223)
                    Lightning : PASS (FW: 2.10.109)
                    KC        : PASS (FW: 2.00.215)
                    Glacier   : PASS (FW: 2.10.111)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.116 & 2.00.224)
                              : Yosemite     - PASS (FW: 2.10.116 & 2.00.224)
                              : Kings Canyon - PASS (FW: 2.10.116 & 2.00.224)
                              : Sprite       - PASS (FW: 2.10.116 & 2.00.224)
                              : Zion         - PASS (FW: 2.10.116 & 2.00.224)
                              : Aurora       - PASS (FW: 2.10.116 & 2.00.222)
                              : Yellowstone  - PASS (FW: 2.10.116 & 2.00.224)
                              : Glacier      - PASS (FW: 2.10.116 & 2.00.224)
     
"""
import os
from testCaseAPI.src.testclient import TestClient

username = 'user1'
user_password = '12345'


class UserHomePageFTPDownloadWidget(TestClient):
    def run(self):
        user_list = self.get_user_list()
        if username in user_list:
            self.log.info('Modify user')
            self.modify_user_account(user=username, passwd=user_password)
        else:
            self.log.info('Add user')
            add_user_command = 'account -a -u {0} -p {1}'.format(username, user_password)
            self.run_on_device(add_user_command)
        
        self.user_home_page_ftp_download_widget_test()
    
    def tc_cleanup(self):
        self.execute('account -d -u {0}'.format(username))
        
    def user_home_page_ftp_download_widget_test(self):
        retry = 3
        while retry >= 0:
            self.uut[self.Fields.web_username] = username
            self.uut[self.Fields.web_password] = user_password
            try:
                self.get_to_page('Non Admin Home')
                self.ftp_download_icon()
                if self.get_text('css=div.h1_content.header_2 > span._text') == 'FTP Downloads':
                    self.log.info('PASS: user is redirected to the FTP Download page')
                else:
                    self.log.error('FAIL: user is not be redirected to the FTP Download page')
                break
            except Exception as e:
                if retry == 0:
                    self.log.exception('Exception : {0}'.format(repr(e)))
                    self.take_screen_shot('Exception')
                retry -= 1
                self.close_webUI()
                self.log.debug('Retry {0} times'.format(3-retry))
    
    def ftp_download_icon(self):
        self.click_wait_and_check(self.Elements.NON_ADMIN_HOME_FTP_DOWNLOAD_ICON, 'css=div.h1_content.header_2 > span._text')
        
    def get_user_list(self):
        command = 'account -i user'
        output = self.run_on_device(command)
        
        return output

UserHomePageFTPDownloadWidget()