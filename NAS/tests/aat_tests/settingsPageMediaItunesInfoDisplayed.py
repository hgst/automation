"""
## @author: tran_jas
## @Brief Test unit's iTunes info is displayed
#  @details
#   Verify iTunes is ON by default
#   Able to rescan (refresh)
#   Auto refresh (set timer)
"""


import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as Element

off_default = ['BWZE', 'BNEZ', 'BWAZ', 'BBAZ']
on_default = ['GLCR', 'BZVM','LT4A', 'KC2A']

SHARENAME = 'iTunesShare'


class ITunesInfo(TestClient):

    need_to_reset = False

    def run(self):
        # self.skip_cleanup = True
        try:
            self.log.info("'iTunes info is displayed' test case")

            self.populate_media_server()

            # Eject usb drive, if present, as it may contain media files.
            if self.find_usb_drive()[1] <> '':
                usb_info = self.get_usb_info()
                usb_handle = self.get_xml_tag(usb_info[1], 'handle')
                self.eject_usb_drive(usb_handle)
                self.log.info('USB drive was ejected')

            self.verify_itunes_switch_default_setting()
            self.refresh_button()
            self.set_password()
            self.set_auto_refresh_time()
            self.reset_itunes_switch()

        except Exception, ex:
            self.log.exception('iTunes info test case failed \n' + str(ex))

    def verify_itunes_switch_default_setting(self):
            self.click_element(Element.SETTINGS_MEDIA)
            itunes_switch = self.get_text(Element.SETTINGS_MEDIA_ITUNES_SWITCH)
            self.log.info(itunes_switch)

            uut_model = self.uut[self.Fields.model_number]

            if uut_model in off_default:
                if itunes_switch == 'OFF':
                    self.log.info('iTunes default setting is OFF for: {}'.format(uut_model))
                    self.click_element(Element.SETTINGS_MEDIA_ITUNES_SWITCH)
                    self.wait_until_element_is_visible('popup_ok_button')
                    self.click_element('popup_ok_button')
                    self.need_to_reset = True
                else:
                    self.log.error('iTunes default setting is not OFF: {}'.format(uut_model))

            elif uut_model in on_default:
                if itunes_switch == 'ON':
                    self.log.info('iTunes default setting is ON: {}'.format(uut_model))
                else:
                    self.log.error('iTunes default setting is not ON: {}'.format(uut_model))
                    self.click_element(Element.SETTINGS_MEDIA_ITUNES_SWITCH)
                    self.wait_until_element_is_visible('popup_ok_button')
                    self.click_element('popup_ok_button')
                    self.need_to_reset = True
            else:
                self.log.warning('Device is not in the list: {}'.format(uut_model))
                if itunes_switch == 'OFF':
                    self.log.info('Switch iTunes to ON')
                    self.click_element(Element.SETTINGS_MEDIA_ITUNES_SWITCH)
                    self.wait_until_element_is_visible('popup_ok_button')
                    self.click_element('popup_ok_button')

    def refresh_button(self):
        try:
            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_REFRESH)
            self.click_element(Element.SETTINGS_MEDIA_ITUNES_REFRESH)
            self.wait_until_element_is_visible('itunes_progressbar')
            percent_text = self.get_text('itunes_percent')
            self.log.info('percent_text: {}'.format(percent_text))

            # desc_text not displayed sometimes?
            try:
                desc_text = self.get_text('itunes_desc')
                self.log.info('desc_text: {}'.format(desc_text))
            except Exception, ex:
                self.log.warning('desc_text was not displayed ' + str(ex))

            # maybe need to check progress?
            time.sleep(60)

        except Exception, ex:
            self.log.exception('Failed to click refresh button \n' + str(ex))

    def set_password(self):
        try:
            loop_count = 0
            while loop_count < 5:
                self.log.info('Waiting for iTunes advanced link, count: ' + str(loop_count))
                self.click_element(Element.SETTINGS_MEDIA)
                if self.is_element_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK):
                    loop_count = 5
                time.sleep(60)
                loop_count += 1

            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.click_element(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_PWD_SWITCH)
            pwd_switch = self.get_text(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_PWD_SWITCH)
            self.log.info('settings_mediaiTunesPWD_switch: {}'.format(pwd_switch))
            if pwd_switch == 'OFF':
                self.click_element(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_PWD_SWITCH)
            self.wait_until_element_is_visible('settings_mediaiTunesPWD_password')
            self.input_text('settings_mediaiTunesPWD_password', 'fituser')
            self.wait_until_element_is_visible('settings_mediaiTunesSave_button')
            self.click_element('settings_mediaiTunesSave_button')
            time.sleep(5)

            # Verify password is set
            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.click_element(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_PWD_SWITCH)
            pwd_switch = self.get_text(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_PWD_SWITCH)
            self.log.info('settings_mediaiTunesPWD_switch: {}'.format(pwd_switch))
            if pwd_switch == 'ON':
                self.log.info('Password is set')
            else:
                self.log.error('Password not saved')

        except Exception, ex:
            self.log.exception('Failed to set password \n' + str(ex))

    def set_auto_refresh_time(self):
        try:
            self.click_element(Element.SETTINGS_MEDIA)
            # handle case: iTunes switch is not turn on?
            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.click_element(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.wait_until_element_is_visible('f_itunes_rescan_interval')
            self.click_element('f_itunes_rescan_interval')
            self.click_element('link=5 minutes')
            self.wait_until_element_is_visible('settings_mediaiTunesSave_button')
            self.click_element('settings_mediaiTunesSave_button')
            time.sleep(5)

            # Verify password is set
            self.wait_until_element_is_visible(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.click_element(Element.SETTINGS_MEDIA_ITUNES_ADVANCED_LINK)
            self.wait_until_element_is_visible('f_itunes_rescan_interval')
            self.click_element('f_itunes_rescan_interval')

            auto_refresh = self.get_text('f_itunes_rescan_interval')
            self.log.info('f_itunes_rescan_interval: {}'.format(auto_refresh))
            if auto_refresh == '5 minutes':
                self.log.info('Auto rescan interval is set')
            else:
                self.log.error('Auto rescan interval not saved')

        except Exception, ex:
            self.log.exception('Failed to set auto refresh time \n' + str(ex))

    def populate_media_server(self):
        try:
            self.delete_all_shares()
            self.create_shares(share_name=SHARENAME, media_serving='any', force_webui=True)
            # Refresh page
            self.get_to_page('Home')
            self.enable_media_serving(share_name=SHARENAME)

            self.generate_media_files(media_info=('jpg', 1024, 768),
                                      total_files=30,
                                      destination='',
                                      share_name=SHARENAME,
                                      media_variance=10,
                                      random_data=True)
            self.create_file(filesize=1024, file_count=30, dest_share_name=SHARENAME, extension='mp3')
            self.create_file(filesize=1024, file_count=30, dest_share_name=SHARENAME, extension='mpeg')
        except Exception, ex:
            self.log.exception('Failed to generate media files \n' + str(ex))

    def reset_itunes_switch(self):
        if self.need_to_reset:
            self.log.info('Need to reset iTunes switch')
            self.click_element(Element.SETTINGS_MEDIA_ITUNES_SWITCH)
            itunes_switch = self.get_text(Element.SETTINGS_MEDIA_ITUNES_SWITCH)
            self.log.info('itunes_switch: {}'.format(itunes_switch))
        else:
            self.log.info('Does not need to reset iTunes switch')


ITunesInfo()
