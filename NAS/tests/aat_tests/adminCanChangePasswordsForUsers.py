"""
Created on July 27th, 2015

@author: tran_jas

## @brief FW-0620 User management.Admin can change passwords for users
#   Create new user
#   Update user with password
#   Verify user has password
#   Update user with new password
#   Verify user has password

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

username = 'elijah'
password1 = 'fituser'
password2 = 'branded'

class AdminCanDeleteUser(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            self.create_user(username)
            self.update_user(username, new_password=password1)
            result = self.get_user(username)
            has_password = self.get_xml_tag(result, 'is_password')
            self.log.info('password: {}'.format(has_password))
            self.update_user(username, old_password=password1, new_password=password2)
            result = self.get_user(username)
            has_password = self.get_xml_tag(result, 'is_password')
            self.log.info('password: {}'.format(has_password))
            if has_password == 'true':
                self.log.info("Update user password successfully")
            else:
                self.log.error("Failed to update user password")

        except Exception, ex:
            self.log.exception('Failed to complete change user password test case \n' + str(ex))

        finally:
            self.log.info("Delete user {}".format(username))
            self.delete_user(username)

AdminCanDeleteUser()