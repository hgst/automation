"""
title           :driveStandbyTest.py
description     :To verify that NAS are able to enter standby mode and are able to be waken by the following access use cases:
                 1. SSH login and SCP file transfer
                 2. Web access actions
                 3. FTP file transfer
                 4. SMB file transfer
author          :yang_ni
date            :2015/03/01
notes           :http://wiki.wdc.com/wiki/Drive_Standby_Test
"""

from testCaseAPI.src.testclient import TestClient
import time,requests

# prompt
waitingString = '/ #'
waitingStringGlacier = 'WDMyCloud / #'
            
# Command
Start_stop_count_com = 'smartctl -a /dev/sda | grep Start_Stop_Count'
check_idle_time = 'cat /tmp/minutes_since_disk_access'

scp_pix_target_path = '/shares/Public/Shared\ Pictures/'

class driveStandbyTest(TestClient):
    def run(self):
        try:
            self.log.info('########################Test START ##############################')         
            self.delete_all_shares()
            time.sleep(7)

            # Change some settings before test starts.
            self.checkStatus()
            
            time.sleep(10)
            
            self.log.info('###### Open serial connection ######')
            self.start_serial_port()
            time.sleep(5)
            self.serial_write('\n')
            time.sleep(2)
            self.serial_wait_for_string(waitingString, 30)            
            
            idle_time = self.actionsBeforeWakeUp()
            if(idle_time == 10):
                self.wakeUpFromSSH()   # Test wakeup from Standby through SSH login and SCP file transfer
            
            idle_time = self.actionsBeforeWakeUp()
            if(idle_time == 10):
                self.wakeUpFromSMB()   # Test wakeup from Standby through SMB
            
            idle_time = self.actionsBeforeWakeUp()
            if(idle_time == 10):
                self.wakeUpFromFTP()   # Test wakeup from Standby through FTP file transfer

            idle_time = self.actionsBeforeWakeUp()
            if(idle_time == 10):
                self.wakeUpFromGUI()   # Test wakeup from Standby through Web Access
                
            self.log.info('########################Test END ##############################')         

        except Exception as e:
            self.log.error("DriveStandbyTest Test FAILED: exception: {}".format(repr(e)))
            self.log.info("Available Volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1],'base_path')))
        finally:
            time.sleep(6)
            self.log.info('Turn cloud access back from OFF to ON...')
            self.enable_remote_access()

     # # @Turn on 'Drive Sleep' and turn off 'Power Schedule' before entering standby mode.
     # # @Turn on 'FTP Setting' and turn on 'Anonymous Read / Write' access to 'Public' folder
     # # @Turn off 'DLNA media setting', 'itunes setting' and 'cloud access' setting
    def checkStatus(self):
         
         self.log.info('########################Check Status START ##############################')  
         model_number = self.get_model_number()

         # Turn off DLNA media setting
         originalMediaServer = self.get_xml_tag(self.get_media_server_configuration()[1],tag='enable_media_server')
         self.log.info('The media server setting now is : {}'.format(originalMediaServer))
         if originalMediaServer == 'true':
            self.log.info('Turn media server from ON to OFF...')
            self.set_media_server_configuration(enable_media_server=False)
         time.sleep(8)

         # Turn off cloud access service
         originalRemoteAccess = self.get_xml_tag(self.get_device_info()[1], tag='remote_access')
         self.log.info('The cloud access setting now is : {}'.format(originalRemoteAccess))
         self.log.info('*** Change cloud access setting... ***')
         if originalRemoteAccess == 'true':
            self.log.info('Turn cloud access from ON to OFF...')
            self.disable_remote_access()
         time.sleep(8)

         # Turn off Itunes server setting
         itunes_result = self.send_cgi('iTunes_Server_Get_XML', 'app_mgr.cgi')
         originalItunesSetting = ''.join(self.get_xml_tags(itunes_result, tag='enable'))
         self.log.info('The Itunes server setting now is : {}'.format(originalItunesSetting))
         time.sleep(5)
         if originalItunesSetting == '1':
            self.log.info('Switch itunes server setting from ON to OFF')
            self.send_cgi('cgi_itunes&f_iTunesServer=0', 'app_mgr.cgi')
         time.sleep(8)

         self.access_webUI()
         time.sleep(5)
         self.get_to_page('Settings')
         time.sleep(7)
         
         # Turn on 'Drive Sleep'
         self.log.info('Check Drive Sleep status')
         self.wait_until_element_is_visible('css=#settings_generalDriveSleep_switch+span .checkbox_container')
         Drive_Sleep_status = self.get_text('css=#settings_generalDriveSleep_switch+span .checkbox_container')
         
         if Drive_Sleep_status == 'ON':
            self.log.info("Drive Sleep has been turned to ON, status = {}".format(Drive_Sleep_status))
         else:
            self.log.info('Drive sleep is OFF. Switching the drive sleep to ON')
            self.click_element('css=#settings_generalDriveSleep_switch+span .checkbox_container')
        
         self.log.info('Drive Sleep is enabled to ON')
         
         time.sleep(5)

         # Glacier does not have 'Power Schedule' toggle
         if model_number != 'GLCR':
             # Turn off 'Power Schedule'
             self.log.info('Check Power Schedule status')
             self.wait_until_element_is_visible('css=#settings_generalPowerSch_switch+span .checkbox_container')
             Power_schedule_status = self.get_text('css=#settings_generalPowerSch_switch+span .checkbox_container')

             if Power_schedule_status == 'OFF':
                self.log.info("Power schedule has been turned to OFF, status = {}".format(Power_schedule_status))
             else:
                self.log.info('Power schedule is ON. Switching the power schedule to OFF')
                self.click_element('css=#settings_generalPowerSch_switch+span .checkbox_container')

             self.log.info('Power Schedule is enabled to OFF')

             time.sleep(5)
         
         # Turn on 'FTP Setting' and 'Anonymous Read / Write' function
         self.enable_ftp_share_access(share_name='Public', access="Anonymous Read / Write")

         self.close_webUI()
         self.log.info('########################Check Status END ##############################\n')  

    # Before wake up by any method, system needs to log start_stop_count first, and sleep for 10 minutes
    # Finally check if start_stop_count increase 1 when entering standby mode
    def actionsBeforeWakeUp(self):
                    
         # Wait for 10 minutes to enter standby mode
         self.log.info('Sleep for 10 minutes to enter standby mode.')
         time.sleep(630)
            
         idle_time = self.checkIdleTime()

         # If system has not entered standby mode yet, wait for 30 seconds more until it enters standby mode
         while(idle_time != 10):
            self.log.info('System is not in standby mode yet.')
            time.sleep(30)
            idle_time = self.checkIdleTime()   
        
         return idle_time       

    # Check Start_stop_count value
    # Each time system enters standby mode, Start_stop_count should increase 1
    def checkStartStopCount(self):
        self.serial_wait_for_string(waitingString, 30)
        self.serial_write(Start_stop_count_com)
        self.serial_wait_for_string('Start_Stop_Count', 30)
        Start_stop_count = self.serial_read()
        Start_stop_count = int(Start_stop_count.split()[9])
        self.log.info('Start_stop_count is {0}'.format(Start_stop_count))
        self.serial_wait_for_string(waitingString, 30)
        return Start_stop_count
         
    # Check system idle time(in minutes)
    # If idle time = 10, system is in standby mode
    # If idle time = 0 to 9, system is not in standby mode yet
    def checkIdleTime(self):
        count = 5
        while count > 0:
            self.serial_wait_for_string(waitingString, 30)
            self.serial_write(check_idle_time)
            idle_time = self.serial_read()
            idle_time = self.serial_read()
            self.log.info('{}: idle time before split is {}'.format(5-count, idle_time))
            count -= 1
            if 'root' not in idle_time:
                continue
            else:
                idle_time = int(idle_time.split('root')[0])            
                self.log.info('idle_time: {0}'.format(idle_time))
                self.serial_wait_for_string(waitingString, 30)
                break
        return idle_time
    
    # Perform SMB read-write access
    def wakeUpFromSMB(self):
        self.log.info('########################Perform SMB action START ##############################')
        
        Start_stop_count = self.checkStartStopCount()
        time.sleep(5)
        
        test_file = self.generate_test_file(kilobytes=10240)
        self.write_files_to_smb(files=test_file)
        #self.read_files_from_smb(files='file0.jpg', delete_after_copy=True)
        
        time.sleep(10)
        
        idle_time = self.checkIdleTime()
        if(idle_time != 10):
            self.log.info('System is waken up by SMB successfully.')
        else:
            self.log.error('System fails to be waken up by SMB.')
        
        Start_stop_count_after_wake = self.checkStartStopCount()
        
        # Check if Start_stop_count increases by 1.
        if(Start_stop_count < Start_stop_count_after_wake):
            self.log.info('Start_stop_count value increases by 1 after leaving from standby mode.')
        else:
            self.log.warning('Start_stop_count value does not increase by 1 after leaving from standby mode.')
            
        self.log.info('########################Perform SMB action END ##############################\n')
    
    # Enable ssh login and scp file transfer
    def wakeUpFromSSH(self):
        
        self.log.info('########################Perform SSH action START ##############################')

        Start_stop_count = self.checkStartStopCount()
        time.sleep(5)

        try:
            self.ssh_connect()
        except Exception as e:
            self.log.error('ssh login fail : {0}'.format(repr(e)))
        else:
            self.log.info('ssh login success')

        time.sleep(10) 
            
        self.log.info('Start to upload and download by SCP')
        generated_file=self.generate_test_file(10240)

        count = 3
        while count >= 0:    
            try:
                self.open_scp_connection()
                #self.scp_from_device(path=scp_pix_target_path, is_dir=False, timeout=20, verbose=False, local_path='.', retry=False)
                self.scp_file_to_device(files=generated_file, remote_path=scp_pix_target_path, recursive=False, preserve_times=False)
                self.log.info('PASS: upload file by SCP') 
            except Exception as e:
                self.log.info("Retrying {} times".format(3-count))
                count = count -1
                if count == 0:
                    self.log.error("SCP file FAILED: exception: {}".format(repr(e)))
            else:
                break
               
        idle_time = self.checkIdleTime()
        if idle_time != 10:
            self.log.info('System is waken up by SSH login successfully.')
        else:
            self.log.error('System fails to be waken up by SSH login.')
            
        Start_stop_count_after_wake = self.checkStartStopCount()
        
        # Check if Start_stop_count increases by 1.
        if(Start_stop_count < Start_stop_count_after_wake):
            self.log.info('Start_stop_count value increases by 1 after leaving from standby mode.')
        else:
            self.log.warning('Start_stop_count value does not increase by 1 after leaving from standby mode.')
        
        self.log.info('########################Perform SSH action END ##############################\n')

    # Enable web access
    def wakeUpFromGUI(self):
        
        self.log.info('########################Perform Web Login action START ##############################')

        Start_stop_count = self.checkStartStopCount()
        time.sleep(5)

        self.get_to_page('Settings')
        time.sleep(8)
        self.click_wait_and_check('settings_firmware_link',
                                    'css=#settings_fwAutoupdate_switch+span .checkbox_container', visible=True)
        time.sleep(8)
        self.log.info("Successfully login web GUI")
        self.close_webUI()
        
        time.sleep(10)
        
        idle_time = self.checkIdleTime()
        if(idle_time != 10):
            self.log.info('System is waken up by Web access login successfully.')
        else:
            self.log.error('System fails to be waken up by web access login.')
        
        Start_stop_count_after_wake = self.checkStartStopCount()
        
        # Check if Start_stop_count increases by 1.
        if(Start_stop_count < Start_stop_count_after_wake):
            self.log.info('Start_stop_count value increases by 1 after leaving from standby mode.')
        else:
            self.log.warning('Start_stop_count value does not increase by 1 after leaving from standby mode.')
        
        self.log.info('########################Perform Web Login action END ##############################')

    # Enable FTP function and perform FTP write access
    def wakeUpFromFTP(self):
        self.log.info('########################Perform FTP action START ##############################')
        
        Start_stop_count = self.checkStartStopCount()        
        time.sleep(5)
        
        # Check if FTP setting is ON
        resp, resp_xml = self.get_FTP_server_configuration()
        result = self.get_xml_tag(xml_content=resp_xml.content, tag='enable_ftp')
        self.log.info("FTP status: {}".format(result))

        time.sleep(8)
        
        # FTP write access test
        tmp_file = self.generate_test_file(102400)
        retry = 3
        while retry >= 0:
            try:
                self.write_files_to_ftp(files=tmp_file, share_name='Public', delete_after_copy=True)
            except Exception as e:
                if retry == 0:
                    self.log.error('FTP Write ERROR: Reach maximum retries due to exception {}'.format(repr(e)))
                    raise
                self.log.info("FTP Write exception: {}, remaining {} retries...".format(repr(e), retry))
                retry -= 1
                time.sleep(1)
            else:
                break

        time.sleep(10)
        
        idle_time = self.checkIdleTime()
        if(idle_time != 10):
            self.log.info('System is waken up by FTP transfer successfully.')
        else:
            self.log.error('System fails to be waken up by FTP transfer.')
            
        Start_stop_count_after_wake = self.checkStartStopCount()
        
        # Check if Start_stop_count increases by 1.
        if(Start_stop_count < Start_stop_count_after_wake):
            self.log.info('Start_stop_count value increases by 1 after leaving from standby mode.')
        else:
            self.log.warning('Start_stop_count value does not increase by 1 after leaving from standby mode.')
        
        self.log.info('########################Perform FTP action END ##############################')

    def send_cgi(self, cmd, url2_cgi_head):
        url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        url_2 = head + cmd
        self.log.debug('login cmd: '+url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+url_2)
        with requests.session() as s:
            s.post(url, data=user_data)
            r = s.get(url_2)
            time.sleep(10)
            response = r.text.encode('ascii', 'ignore')
            self.log.debug('response : {0}'.format(response))
            if 'Internal Server Error' in response:
                raise Exception(str(response))

            return response

driveStandbyTest()