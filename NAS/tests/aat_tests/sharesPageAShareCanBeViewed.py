""" Shares page - a share can be viewed (MA-155)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=20018&execView=execDetails&view=details&tdetab=0&pltab=steps&fiTpId=13015&pId=50&nTP=117412&etab=8
    1) Open WebUI and login as Admin
    2) Click on Shares category
    3) Select a share
    4) Verify that the share profile can be viewed ( Share profile is viewable )

    Automation: Full

    Not supported product:
        N/A

    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon


    Test Status:
        Local Yellowstone: Pass (FW: 2.10.182)
"""
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import os


class sharesPageAShareCanBeViewed(TestClient):
    def run(self):
        self.log.info('############### (MA-155) Shares page - a share can be viewed TESTS START ###############')
        new_share_name = 'ShareCanView'
        try:
            self.log.info("Delete all shares")
            self.delete_all_shares()
            self.log.info("Create new share: {}".format(new_share_name))
            self.create_shares(share_name=new_share_name, force_webui=True)
            self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)
            self.close_webUI()
            self.get_to_page('Shares')
            self.click_wait_and_check('shares_share_{}'.format(new_share_name),
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            share_name_text = self.get_value('shares_editShareName_text')
            if share_name_text == new_share_name:
                self.log.info("PASS: Share name is matched!")
            else:
                self.log.error("ERROR: Share name({}) does not match correct "
                               "name({})!!".format(share_name_text, new_share_name))
        except Exception as e:
            self.log.error("Test Failed: (MA-155) A share can be viewed test, exception: {}".format(repr(e)))
        finally:
            self.log.info("====== Clean Up section ======")
            self.log.info('############### (MA-155) Shares page - a share can be viewed TESTS END ###############')


sharesPageAShareCanBeViewed()