""" Storage Page - Disk Status info is displayed (MA-171)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=20018&execView=execDetails&view=details&pltab=steps&pId=50&nTP=117448&etab=8)
                1) Open webUI to login as system admin
                2) Click on the Storage category
                3) Select Disk status
                4) Verify the Disks profile is displayed
                   (Disk status is displayed)
    
    Automation: Full
    
    Not supported product:
        Glacier (Does not have Storage page)
    
    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon
    
                   
    Test Status:
        Local Lightning: Pass (FW: 2.00.171)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as Elem
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.webdriver.common.keys import Keys
import time
import os


products_info = {
        'LT4A': 'Lightning',      
        'BNEZ': 'Sprite',
        'BWZE': 'Yellowstone',
        'BWAZ': 'Yosemite',
        'BBAZ': 'Aurora',
        'KC2A': 'KingsCanyon',
        'BZVM': 'Zion',
        'GLCR': 'Glacier',
        'BWVZ': 'GrandTeton',
}

picture_counts = 0

class storageDiskStatusInfoDisplayed(TestClient):
        
    def run(self):
        self.log.info('######################## Storage Page - Disk Status info is displayed TEST START ##############################')
        try:
            # productName = products_info[self.get_model_number()]
            productName = self.uut[Fields.product]
            if productName == 'Glacier':
                self.log.info("UN-SUPPORTED PRODUCT: {}, (no storage page)".format(productName))
                return
            
            numOfBays = self.uut[Fields.number_of_drives]
            numOfDrives = self.get_drive_count()
            self.log.info("Product: {}, Supported Bays: {}, Current installed drives: {}".format(productName, numOfBays, numOfDrives))
            # self.click_element(Elem.STORAGE_DISK_STATUS)
            self.get_to_page('Storage')
            self.click_wait_and_check('nav_storage_link',
                                      'storage_diskstatus_link', visible=True, timeout=15)
            self.click_wait_and_check('storage_diskstatus_link',
                                      'disk_mgmt_state', visible=True)
            time.sleep(2)
            # Get Disk Health information
            self.verifyDiskProfile()
            # Get individual disk informaiton
            self.verifyDiskStatus(numDrives=numOfDrives)
        except Exception as e:
            self.log.error("FAILED: Storage Page - Disk Status info is displayed Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: Storage Page - Disk Status info is displayed Test PASS!!!")
            
        self.log.info('######################## Storage Page - Disk Status info is displayed TEST END ##############################')
    
    def verifyDiskProfile(self):
        """
            Verify the Disk Profile
        """
        try:
            healthTitleText = self.get_text("//div[@class=\'field_top\']/table/tbody/tr/td[1]/span")
            diskHealthText = self.get_text("//div[@id=\'disk_mgmt_state\']/span")
            self.log.info("{}: {}".format(healthTitleText, diskHealthText))
        except Exception as e:
            self.log.error("ERROR: Disk profile information is not properly displayed! Exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='diskProfile')

    def verifyDiskStatus(self, numDrives=None):
        """
            @numDrives: How many physical drives are installed
        """
        # Glacier does not have Storage page
        if numDrives is None:
            raise Exception("verifyDiskprofile() requires 1 argument")
        
        try:
            for i in range(1, numDrives+1):
                diskNumLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[2]/div".format(i)              
                diskNum = self.get_text(diskNumLocator)
                diskSizeLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[3]/div".format(i)
                diskSize = self.get_text(diskSizeLocator)
                diskStatusLocator = "//div[@class=\'bDiv\']/table/tbody/tr[{}]/td[5]/div".format(i)
                diskStatus = self.get_text(diskStatusLocator)
                self.log.info("{}: Size = {}, Status = {}".format(diskNum, diskSize, diskStatus))
                time.sleep(2)
        except ElementNotReady as e:
            self.log.error("ERROR: {}".format(repr(e)))
            self.takeScreenShot(prefix='diskStatus')

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1


storageDiskStatusInfoDisplayed()    