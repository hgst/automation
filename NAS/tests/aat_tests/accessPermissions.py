"""
Created on Jan 2, 2015
Author: Carlos Vasquez
"""

from testCaseAPI.src.testclient import TestClient
from smb.smb_structs import OperationFailure
import time
    
group = 'group1'
public_share_name = ['PublicShare_1', 'PublicShare_2', 'PublicShare_3','PublicShare_4']

private_share_name = ['PrivateShareA_1', 'PrivateShareA_2', 'PrivateShareA_3','PrivateShareA_4', 
                      'PrivateShareB_1', 'PrivateShareB_2', 'PrivateShareB_3','PrivateShareB_4']

private_share_name_A = ['PrivateShareA_1', 'PrivateShareA_2', 'PrivateShareA_3','PrivateShareA_4']
private_share_name_B = ['PrivateShareB_1', 'PrivateShareB_2', 'PrivateShareB_3','PrivateShareB_4']

private_share = ['PrivateShare1', 'PrivateShare2', 'PrivateShare3', 'PrivateShare4']
share_desc = 'share volume'

allShares = ['PublicShare_1', 'PublicShare_2', 'PublicShare_3','PublicShare_4', 'PrivateShareA_1', 
             'PrivateShareA_2', 'PrivateShareA_3','PrivateShareA_4', 'PrivateShareB_1', 'PrivateShareB_2',
             'PrivateShareB_3','PrivateShareB_4']

shareDictionary = {'PublicShare_1':{'user1':'RW', 'user2':'RW'},
                   'PublicShare_2':{'user1':'RW', 'user2':'RW'},
                   'PublicShare_3':{'user1':'RW', 'user2':'RW'},
                   'PublicShare_4':{'user1':'RW', 'user2':'RW'},
                   'PrivateShareA_1':{'user1':'RO', 'user2':'RW'},
                   'PrivateShareA_2':{'user1':'RO', 'user2':'RW'},
                   'PrivateShareA_3':{'user1':'RW', 'user2':'D'},
                   'PrivateShareA_4':{'user1':'D', 'user2':'RO'},
                   'PrivateShareB_1':{'user1':'D', 'user2':'RO'},
                   'PrivateShareB_2':{'user1':'RW', 'user2':'D'},
                   'PrivateShareB_3':{'user1':'D', 'user2':'RO'},
                   'PrivateShareB_4':{'user1':'RO', 'user2':'RW'}}

   
class accessPermissions(TestClient):
    
    def run(self):      
       self.tc_cleanup()
       
       # Check if in JBOD
       if self.get_raid_mode() != 'JBOD':
           # configure raid (JBOD)
           self._configureJBOD()
       
       # Create or modify share folders
       self._create_shares(public_share_name, public=True)
       self._create_shares(private_share_name_A, public=False)
       self._create_shares(private_share_name_B, public=False)
       
       # Create an fituser user (no authenticated)
       createdUser1 = self.create_user(username='fituser', fullname='fituser user', is_admin=False)
       self.public_share_accessible_no_authentication('fituser')
       self.private_share_accessible_no_authentication('fituser')
       
       # Create or modify users
       self._update_user_shares()
       
       self._populate_all_shares(public_share_name)
       self._populate_all_shares(private_share_name)

       self._validate_share_access_permissions(username='user1', password = 'fituser')
       self._validate_share_access_permissions(username='user2', password = 'fituser')
       self.tc_cleanup()
    
    def tc_cleanup(self): 
        self.local_login()
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        self.execute('smbif -b user1')
        self.execute('smbif -b user2')
        self.execute('smbif -b fituser')
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()
        
        self.log.info('Deleting all groups ...')
        for group in range (1,65):
            group_name = 'group{0}'.format(group)
            self.delete_group(group_name) 
            
         
    def _configureJBOD(self):   
        # configure raid (JBOD) if already JBOD skip configuration
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

        if modelNumber in ('BWAZ', 'BBAZ', 'KC2A','BZVM') and (self.get_raid_mode() <> 'JBOD'):  
            self.log.info('Configuring JBOD(2-Bay...')
            self.configure_jbod()   
        elif modelNumber in ('LT4A', 'BNEZ', 'BWZE') and (self.get_raid_mode() <> 'JBOD'):
            self.log.info('Configuring JBOD(4-Bay)...')
            self.configure_jbod()
        else: 
            self.log.info('No raid configuration needed')
            
    def _create_shares(self, share_name = [], public=True):
        self.log.info('######################## Create Shares ########################')
        try:                      
            for i in range (1,5):
                self.log.info('Creating share: {0}'.format(share_name[i-1]))
                self.create_shares(share_name=share_name[i-1], description=share_desc)
                self.update_share_network_access(share_name=share_name[i-1], public_access=public)      
        except Exception,ex:
            self.log.exception('Failed to create share')
            
    def _populate_all_shares(self, share_type):
        # Create shares access for admin user in all private shares
        self.log.info('######################## Populate All Shares ########################')
        
        for share in share_type:
            try:
                if share_type == private_share_name:
                    self.create_share_access(share_name=share)
                else:
                    self.update_share_access(share_name=share)
                generated_files = self.generate_test_file(kilobytes=1)
                self.write_files_to_smb(files=generated_files,share_name=share, delete_after_copy=True)
                time.sleep(1)
            except Exception,ex:
                self.log.error('Unable to populate shares: ' + str(share)) 
             
    def public_share_accessible_no_authentication(self, username):
        self.log.info('######################## Public Share Accessible No Authentication ########################')         
        for share in public_share_name:
            try:
                xml_content = self.read_files_from_smb(share_name=share, username=username, delete_after_copy=True)
                self.log.info('Success: Able to access public share without authentication: ' + str(share))
            except Exception,ex:
                self.log.error('Unable to access public share: ' + str(share))
            
    def private_share_accessible_no_authentication(self, username):
        self.log.info('######################## Private Share Accessible No Authentication ########################')
        for share in private_share_name:
            try:
                xml_content = self.read_files_from_smb(share_name=share, username=username, delete_after_copy=True)
                self.log.error('Able to access private share without authentication: ' + str(share))
            except OperationFailure:
                self.log.info('Success: Unable to access private share: ' + str(share))
                
    def _update_user_shares(self):
        try:   
            self.log.info('######################## Update User Shares ########################')
            # Create user1 to be used to access shares
            createdUser1 = self.create_user(username='user1', fullname='User One', password='fituser', is_admin=False)
            if createdUser1 == 0:
                self.log.info('user1 was created successfully')
            else:
                self.log.error('user1 was not created successfully')
                
            # For user1 set RO for PrivateShareA_1, PrivateShareA_2, PrivateShareB_4 
            self.create_share_access(share_name='PrivateShareA_1', username='user1', access='RO')
            self.create_share_access(share_name='PrivateShareA_2', username='user1', access='RO')
            self.create_share_access(share_name='PrivateShareB_4', username='user1', access='RO') 
            
            # For user1 set RW Only for PrivateShareA_3, PrivateShareB_2
            self.create_share_access(share_name='PrivateShareA_3', username='user1', access='RW')
            self.create_share_access(share_name='PrivateShareB_2', username='user1', access='RW') 
            
            # For user1 deny access for PrivateShareA_4, PrivateShareB_1, PrivateShareB_3
            self.create_share_access(share_name='PrivateShareA_4', username='user1', access='D')
            self.create_share_access(share_name='PrivateShareB_1', username='user1', access='D')
            self.create_share_access(share_name='PrivateShareB_3', username='user1', access='D') 
            
            # Create user2 to be used to access shares
            createdUser2 = self.create_user(username='user2', fullname='User One', password='fituser', is_admin=False)
            if createdUser2 == 0:
                self.log.info('user2 was created successfully')
            else:
                self.log.error('user2 was not created successfully') 
                        
            # For user2 set RO for PrivateShareA_4, PrivateShareB_1, PrivateShareB_3 
            self.create_share_access(share_name='PrivateShareA_4', username='user2', access='RO')
            self.create_share_access(share_name='PrivateShareB_1', username='user2', access='RO')
            self.create_share_access(share_name='PrivateShareB_3', username='user2', access='RO') 
            
            # For user2 set RW Only for PrivateShareA_3, PrivateShareB_2
            self.create_share_access(share_name='PrivateShareA_1', username='user2', access='RW')
            self.create_share_access(share_name='PrivateShareA_2', username='user2', access='RW')
            self.create_share_access(share_name='PrivateShareB_4', username='user2', access='RW')  
            
            # For user2 deny access for PrivateShareA_3, PrivateShareB_2
            self.create_share_access(share_name='PrivateShareA_3', username='user2', access='D')
            self.create_share_access(share_name='PrivateShareB_2', username='user2', access='D')                
        except Exception,ex:
            self.log.info('Failed to update share\n' + str(ex))
        
    def _validate_share_access_permissions(self, username, password):
        # Read/Write to PublicShare(s)
        self.log.info('######################## Validate Share Access Permissions ########################')

        self.local_login(username=username, password=password)
        for share in allShares:
            # generate a test file in local directory
            testFile = self.generate_test_file(kilobytes=1)
            
            # write file to share 
            try:
                self.write_files_to_smb(username=username, password=password,files=testFile, share_name=share, delete_after_copy=True)
            except OperationFailure:
                write_status = False
            else:
                write_status = True
                   
            # read file from share
            try:
                self.read_files_from_smb(username=username, password=password, share_name=share, files=testFile, delete_after_copy=False)
            except OperationFailure:
                read_status = False
            else:
                read_status = True
            
            # determine access permissions        
            if write_status == False and read_status == True:
                self.log.info('{0} has RO access permissions to {1}'.format(username,share))
                access = 'RO'
            elif write_status == True and read_status == True:
                self.log.info('{0} has RW access permissions to {1}'.format(username,share))
                access ='RW'
            elif write_status == False and read_status == False:
                self.log.info ('{0} has Deny access permissions to {1}'.format(username,share))
                access = 'D'     
            
            if shareDictionary[share][username] == access:
                self.log.info('Success: access permission passed')
            else:
                self.log.error('Failed: access permission failed')                               
accessPermissions()