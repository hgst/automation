""" User Home Page-P2P Download Widget

    @Author: Lee_e
    
    Objective: verify that the user is redirected to the P2P Download page
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.00.223)
                    Lightning : PASS (FW: 2.10.109)
                    KC        : PASS (FW: 2.00.221)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.130 & 2.00.224)
                              : Yosemite     - PASS (FW: 2.10.128 & 2.00.224)
                              : Kings Canyon - PASS (FW: 2.10.128 & 2.00.224)
                              : Sprite       - PASS (FW: 2.10.130 & 2.00.224)
                              : Zion         - PASS (FW: 2.10.130 & 2.00.224)
                              : Aurora       - PASS (FW: 2.10.130 & 2.00.224)
                              : Yellowstone  - PASS (FW: 2.10.127 & 2.00.224)
                              : Glacier      - PASS (FW: 2.10.127 & 2.00.224)
     
""" 
from testCaseAPI.src.testclient import TestClient

username = 'user1'
user_password = '12345'


class UserHomePageP2PDownloadWidget(TestClient):
    def run(self):
        user_list = self.get_user_list()
        if username in user_list:
            self.log.info('Modify user')
            self.modify_user_account(user=username, passwd=user_password)
        else:
            self.log.info('Add user')
            add_user_command = 'account -a -u {0} -p {1}'.format(username, user_password)
            self.run_on_device(add_user_command)
        
        self.userHomePageP2PDownloadWidget_test()

    def tc_cleanup(self):
        self.execute('account -d -u {0}'.format(username))
        
    def userHomePageP2PDownloadWidget_test(self):
        self.webUI_user_login(username, user_password)
        try:
            self.P2P_Download_icon()
        except Exception as e:
            self.log.Exception('Exception : {0}'.format(repr(e)))
            self.take_screen_shot('Exception')

        if 'P2P Download' in self.get_text('css=div#_enable_setting'):
            self.log.info('PASS: user is redirected to the P2P Download page')
        else:
            self.log.error('FAIL: user is not be redirected to the P2P Download page')

    def P2P_Download_icon(self):
        self.click_wait_and_check(self.Elements.NON_ADMIN_HOME_P2P_DOWNLOAD_ICON, 'css=div#_enable_setting')
        self.log.debug('text of css=div#_enable_setting is : {0}'.format(self.get_text('css=div#_enable_setting')))

    def get_user_list(self):
        command = 'account -i user'
        output = self.run_on_device(command)
        
        return output

    def webUI_user_login(self, username, userpwd):
        """
        :param username: user name you'd like to login
        :param userpwd: user password for user name account
        """
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.input_text_check('login_pw_password', userpwd, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.log.info("PASS: Succeed to login as [{}] user".format(username))
        except Exception, ex:
            self.log.Exception('Exception : {0}'.format(repr(ex)))
            self.take_screen_shot('login_fail')

UserHomePageP2PDownloadWidget()