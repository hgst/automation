"""
title           : groupSharePermissions.py
description     : 1. Group R/W permission to private share
                  2. User with deny access in a group with R/W permission to a private share
                  3. User with R/W access in a group with R/W permission to a private share
                  4. Group deny permission to private share
author          :yang_ni
date            :2015/09/08
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
from smb.smb_structs import OperationFailure

class GroupSharePermissions(TestClient):

    def run(self):
        if self.get_model_number() in ('GLCR',):
            self.log.info("=== Glacier de-feature does not support group feature, skip the test ===")
            return

        try:
            self.tc_cleanup()
            time.sleep(8)

            # Create a user
            self.log.info('Creating 2 users ...')
            self.create_user(username='user1')
            time.sleep(10)
            self.create_user(username='user2')
            time.sleep(10)
            userList = self.get_xml_tags(self.get_all_users()[1],tag='username')
            self.log.info('User list : {0}'.format(userList))
            if ('user1' in userList) and ('user2' in userList):
                self.log.info('Users are created successfully')
            else:
                self.log.error('Users are not created successfully')

            # Create a share.
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=1)
            time.sleep(6)
            share_list = self.get_xml_tags(self.get_all_shares()[1], 'share_name')
            self.log.info('share list: {0}'.format(share_list))

            # Change share to private, share must be set to private instead of public. Toggle the Public switch off
            self.log.info('Toggling the share from private to public ...')
            self.update_share_network_access(share_name='share', public_access=False)
            time.sleep(8)

            # Assigns user1 to share with the deny access level
            self.log.info('Assigning user1 with Deny access to the share ...')
            self.assign_user_to_share(user_name='user1', share_name='share', access_type='d')

            # Assigns user2 to share with the deny access level
            self.log.info('Assigning user2 with r/w access to the share ...')
            self.assign_user_to_share(user_name='user2', share_name='share', access_type='rw')

            # Create a group and add 'user1' and 'user2' into this group
            self.log.info('Create a group and add the two users into the group ...')
            self.create_groups(group_name='group1', number_of_groups=1, memberusers='#user1#,#user2#')

            # Assigns group1 to share with the read/write access level
            self.log.info('Assign r/w privilege share access to group1...')
            self.assign_group_to_share(group_name='group1', share_number=1, access_type='rw')
            time.sleep(7)

            # Test1: User with deny access in a group with R/W access to share
            self.log.info('1. User1 with deny access in a group with R/W access to share')
            self.smbReadWriteTest(user_name='user1',share_name='share',access_type='d')
            time.sleep(8)
            self.log.info('PASS : User1 with deny access in a group with R/W access to share')

            # Test2: User with R/W access in a group with R/W access to share
            self.log.info('2. User2 with R/W access in a group with R/W access to share')
            self.smbReadWriteTest(user_name='user2',share_name='share',access_type='rw')
            time.sleep(8)
            self.log.info('PASS : User2 with r/w access in a group with R/W access to share')

            # Assigns group1 to share with the deny access level
            self.log.info('Assign share deny access to group1...')
            self.assign_group_to_share(group_name='group1', share_number=1, access_type='d')
            time.sleep(7)

            # Test3: User with deny access in a group with deny access to share
            self.log.info('3. User1 with deny access in a group with deny access to share')
            self.smbReadWriteTest(user_name='user1',share_name='share',access_type='d')
            time.sleep(8)
            self.log.info('PASS : User1 with deny access in a group with deny access to share')

            # Test2: User with R/W access in a group with deny access to share
            self.log.info('4. User2 with R/W access in a group with deny access to share')
            self.smbReadWriteTest(user_name='user2',share_name='share',access_type='d')
            time.sleep(8)
            self.log.info('PASS : User2 with R/W access in a group with deny access to share')

        except Exception as e:
            self.log.error('Exception: {0}'.format(repr(e)))
        else:
            self.log.info('*** Group Share permission test PASS ***')

    # SMB read/write access check
    def smbReadWriteTest(self, user_name, share_name, access_type):
        self.log.info('*** Check if {0} has {1} access to the {2} ***'.format(user_name,access_type,share_name))

        test_file = self.generate_test_file(kilobytes=10)
        time.sleep(7)

        # Check user write access to the share folder.
        try:
            self.write_files_to_smb(files=test_file, username=user_name, delete_after_copy=True, share_name=share_name)
        except OperationFailure:
            if access_type == 'rw':
                raise Exception('FAIL : Fail to write file to share folder')
            else:
                self.log.info('CORRECT : Fail to write file to share folder')
        else:
            if access_type == 'rw':
                self.log.info('CORRECT : Success to write file to share folder')
            else:
                raise Exception('FAIL : Success to write file to share folder')

        # Check user1 read access to the share folder.
        try:
            self.read_files_from_smb(files=test_file, username=user_name, delete_after_copy=True, share_name=share_name)
        except OperationFailure:
            if access_type == 'd':
                self.log.info('CORRECT : Failed to read file from share folder')
            else:
                raise Exception('FAIL : Failed to read file from share folder')
        else:
            if access_type == 'd':
                raise Exception('FAIL : Success to read file from share folder')
            else:
                self.log.info('CORRECT : Success to read file from share folder')

    # # @Clean up all the users/shares/groups after test finishes
    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users(use_rest=False)
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

GroupSharePermissions()