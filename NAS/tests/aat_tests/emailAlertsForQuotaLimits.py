""" E-mail Alerts for Quota Limits (MA-214)

    @Author: lin_ri
    Procedure @ http://wiki.wdc.com/wiki/Quotas

    1) Open device WebUI and go to the Settings>Notifications page
    2) Enable Alert emails and add in an email address to send the alerts
    3) Go through steps 1-6 from the User Account Quota test, but make sure user1's E-Mail address is entered
        1) Open device WebUI and go to the Users page
        2) Add a new user (Ex. user1) by clicking on the + sign under the user list
        3) Enter in a Quota value of 95MB for each Volume listed and Save
        4) From the PC open a share on Volume 1 via Samba and log in with user1
        5) Copy nine 10MB files to the share
        6) Copy the last 10MB file to the share ==> Prompt should appear stating that there is not enough free space
    4) After getting to 90% of the quota being filled, an email notification should be sent stating 90% of quota has been reached
    5) After adding more data and getting to 100% of the quota being filled, an email notification should be sent stating 100% of the quota has been reached.

    Automation: Partial

    Defect:
    1) SKY-3385 It can't display the alert of quota almost full(2038) and full(2039)
    2) SKY-2887(Can not login Web UI after batch trigger email/SMS alerts)
       a. With email/SMS alert enabled, Web UI will no longer respond when multiple alerts are generated (alert_test)
       b. The root cause is the DNS server setting in NAS.
       If we have incorrect DNS server being set in NAS,
       the web UI starts to behave abnormal when multiple alerts are generated.

    Not supported product:
        Glacier

    Support Product:
        Lightning
        YellowStone
        Yosemite
        Sprite
        Aurora
        Kings Canyon
        Zion

    Test Status:
        Local Yellowstone: Pass (FW: 2.10.302)

"""

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
from selenium.webdriver.common.keys import Keys
import global_libraries.wd_exceptions as exceptions
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time

alertTable = {
# Critical
#    code       Title
    '2038': {'MSG': 'Storage Quota Almost Full',    'PRI': 2 , 'PARM': 'test1'},
    '2039': {'MSG': 'Storage Quota Full',           'PRI': 2 , 'PARM': 'test1'},
    '1038': {'MSG': 'Storage Quota Almost Full',    'PRI': 2 , 'PARM': 'test1'},  # (Previously 2038) Storage quota for [%1] is almost full
    '1039': {'MSG': 'Storage Quota Full',           'PRI': 2 , 'PARM': 'test1'},  # (Previously 2039) Storage quota for [%1] has reached the limit,
}

class emailAlertsQuotaLimits(TestClient):
    def run(self):
        user_name = 'ma214'
        user_pwd = ''
        # quota_alerts = ['2038', '2039']
        self.log.info('############### (MA-214) E-mail Alerts for Quota Limits TESTS START ###############')
        if self.get_model_number() in ('GLCR', ):
            self.log.info("=== Glacier does not support user quota feature, skip the test ===")
            return
        try:
            self.log.info("Delete all users")
            try:
                self.delete_all_users(use_rest=False)
            except exceptions.InvalidDeviceState:
                self.log.info("User list is empty")
            self.log.info("Delete all shares")
            self.delete_all_shares()  # Clean all shares except Public, SmartWare and TimeMachineBackup
            # Config email
            # self.config_email_notification(testAddress='fituser@mailinator.com', level='a')
            self.create_new_user(username=user_name, password=user_pwd)
            self.set_user_quota(username=user_name, quota='95', unit='MB')
            self.verify_user_quota(username=user_name, password=user_pwd)
            # VERIFY UI ALERTS fro 95% and FULL, 1038(2038) and 1039(2039)
            self.log.info("Due to defect SKY-3385, you can not see 2038 and 2039 alerts, skip the alert UI check")
            # self.ui_clear_alert(alertList=quota_alerts)
        except Exception as e:
            self.log.error("Test Failed: (MA-214) E-mail Alerts for Quota Limits test, exception: {}".format(repr(e)))
            output = self.run_on_device('ps aux | grep -v grep | grep sync; ps aux | grep -v grep | grep httpd')
            self.log.info("Current processes:\n {}".format(output))
        finally:
            self.log.info("====== Clean Up section ======")
            self.set_web_access_timeout(timeout='30')   # re-set back to default(30)
            # self.delete_email_settings()
            self.log.info("Clean all files under /shares/{} folder".format(user_name))
            self.run_on_device('rm -rf /shares/{}/*.*'.format(user_name))
            self.execute('alert_test -R')
            self.log.info('############### (MA-214) E-mail Alerts for Quota Limits TESTS END ###############')

    def verify_user_quota(self, username, password):
        """
        :param username: new user name
        :param password: new user password
        """
        self.log.info("*** Clear all alerts ***")
        self.execute('alert_test -R')
        self.log.info("Generate 10 of 10MB test files for testing")
        file_list = self.generate_test_files(kilobytes=10240, file_count=10, prefix='10MB')
        self.log.info("=== Writing 9 of 10MB files to share: {0} ===".format(username))
        try:
            self.write_files_to_smb(username=username, password=password, files=file_list[:-1], share_name=username)
        except Exception as e:
            self.log.error("ERROR: Failed to write 9 of 10MB files with exception: {}".format(repr(e)))
            output = self.run_on_device('ls -la /shares/{}/'.format(username))
            self.log.info("Available files: {}".format(output))
        try:
            self.log.info("=== Writing 10th 10MB file to share: {0} ===".format(username))
            self.write_files_to_smb(username=username, password=password, files=file_list[-1], share_name=username)
        except Exception as e:
            self.log.info("PASS: Due to limit of 95MB, fail to write more than 5MB file, exception: {}".format(repr(e)))
            output = self.run_on_device('ls -la /shares/{}/'.format(username))
            self.log.info("Available files: {}".format(output))
            time.sleep(10)  # Wait for alerts generation
        else:
            self.log.error("ERROR: Succeed to write more than 100MB for 95MB quota limit")

    def set_user_quota(self, username, quota='95', unit='MB'):
        """
        :param username: a new user you'd like to create
        :param quota: string type of value
        :param unit: MB, GB, TB
        """
        supported_units = ('MB', 'GB', 'TB')
        if str(unit) not in supported_units:
            raise Exception("ERROR: unit({}) value is incorrect".format(unit))
        try:
            self.get_to_page('Users')
            self.click_wait_and_check('nav_users_link', 'users_createUser_link', visible=True)
            self.click_wait_and_check('users_user_{}'.format(username), 'users_editUserName_text', visible=True)
            self.log.info("Configure user quota")
            # This needs to wait for updating string
            self.click_wait_and_check('users_editUserQuota_link', 'uesrs_modQuotaCancel_button', visible=True, timeout=30)
            self.input_text_check('users_v1Size_text', str(quota), do_login=False)
            self.click_link_element('quota_unit_main_1', str(unit))
            self.click_wait_and_check('uesrs_modQuotaSave_button', visible=False)
            self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        except Exception as e:
            self.log.error("ERROR: Fail to set user quota to {0}{1}, exception: {}".format(quota, unit, repr(e)))
        else:
            self.log.info("User({0}) quota is set to {1}{2}".format(username, quota, unit))
            self.reload_page()

    def create_new_user(self, username, password):
        """
        :param username: new user name
        :param password: new user password
        """
        try:
            self.get_to_page('Users')
            self.log.info("Create new user - {}".format(username))
            self.click_wait_and_check('nav_users_link', 'users_createUser_link', visible=True)
            self.click_wait_and_check('users_createUser_link', 'users_addUserCancel1_button', visible=True)
            self.input_text_check('users_userName_text', str(username), do_login=False)
            self.input_text_check('users_newPW_password', str(password), do_login=False)
            self.input_text_check('users_comfirmPW_password', str(password), do_login=False)
            self.click_wait_and_check('users_addUserSave_button', visible=False)
        except Exception as e:
            self.log.error("ERROR: Failed to create user quota, exception: {0}".format(repr(e)))
        else:
            self.log.info("New user: {} is created".format(username))
            self.reload_page()

    def config_email_notification(self, testAddress="fituser@mailinator.com", level='a'):
        """
            Enable and Test email alert

            @testAddress: email address you'd like to add in the notification
                          if you set to fituser@mailinator.com, it will set 5 emails from fituser1 to fituser5
            @severity: 'c': critical only
                       'w': critical and warning
                       'a': All
        """
        try:
            self.log.info("==> Configure Alert emails notification")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('notifications', 'css=#notifications.LightningSubMenuOn', visible=True)

            self.log.info("Turn on Alert emails notification")
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            if email_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsAlert_switch+span .checkbox_container',
                                          'css=#settings_notificationsAlert_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            self.log.info("Alert emails notification is set to {}".format(email_status))
            time.sleep(2)
            self.log.info("Click Alert emails configure")
            self.click_wait_and_check('settings_notificationsAlert_link',
                                      'settings_notificationsMailSave_button', visible=True)

            # Select Severity to Critical Only, Critical and Warning, All
            self.config_sev_notification(severity=level)

            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link'):
                self.log.info("Delete one email address")
                self.wait_and_click('settings_notificationsDelMail1_link')
                time.sleep(2)
            self.log.info("All email settings are deleted!")

            # Add One valid email
            self.wait_until_element_is_visible('settings_notificationsAddMail_button')
            self.click_wait_and_check('settings_notificationsAddMail_button',
                                      'settings_notificationsMail_text', visible=True)
            if self.is_element_visible('settings_notificationsMail_text'):
                self.log.info("Add valid email: {}".format(testAddress))
                self.input_text_check('settings_notificationsMail_text', testAddress, do_login=False)
                time.sleep(2)
                self.click_wait_and_check('settings_notificationsSaveMail_button', visible=False)
            self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
            self.wait_until_element_is_not_visible(E.UPDATING_STRING)
        except Exception as e:
            self.take_screen_shot(prefix='configEmail')
            self.log.error("FAILED: Config email settings failed, exception: {}".format(repr(e)))

    def config_sev_notification(self, severity='c'):
        """
            Configure notification Level

            @severity: 'c': critical only
                       'w': critical and warning
                       'a': All
        """
        self.wait_until_element_is_visible("//div[@id='settings_notificationsDisplay_slider']/a")
        # Get Current Level value
        element = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
        percent_text = element.get_attribute('style')
        percent = int(percent_text.split()[1][:-2])
        self.log.info("Current Notification Level: {}".format(percent))
        time.sleep(2)

        if severity == 'c':
            self.log.info("Setting Notification Level to Critical Only(0%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 50%
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 0%
            else:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 0%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 0:
                self.log.info("PASS: Notification Display has been set to Critical ONLY successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set Critical ONLY...")
            return
        elif severity == 'w':
            self.log.info("Setting Notification Level to Critical and Warning(50%)...")
            if percent > 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_LEFT) # 50%
            elif percent < 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 50:
                self.log.info("PASS: Notification Display has been set to Critical and Warning successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set Critical and Warning...")
            return
        elif severity == 'a':
            self.log.info("Setting Notification Level to All(100%)...")
            if percent <= 50:
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
                self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
            time.sleep(2)
            # Get New Level value
            elem = self.element_find("//div[@id='settings_notificationsDisplay_slider']/a")
            curPercent = int(elem.get_attribute('style').split()[1][:-2])
            self.log.info("New Notification Level: {}".format(curPercent))
            if curPercent == 100:
                self.log.info("PASS: Notification Display has been set to ALL successfully...")
            else:
                self.log.error("FAIL: Notification Display failed to set ALL...")
            return

    def delete_email_settings(self):
        """
            Delete email settings
        """
        try:
            self.log.info("==> Clear Email settings ")
            self.get_to_page('Settings')
            self.wait_and_click('settings_notifications_link')
            time.sleep(2)
            self.log.info("Turn on Alert emails notification")
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            if email_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsAlert_switch+span .checkbox_container',
                                          'css=#settings_notificationsAlert_switch+span .checkbox_container .checkbox_on',
                                          visible=True)
            email_status = self.get_text('css=#settings_notificationsAlert_switch+span .checkbox_container')
            self.log.info("Alert emails notification is switched to {}".format(email_status))
            time.sleep(2)
            self.log.info("====== Click Alert emails configure ======")
            # self.click_wait_and_check('css=#settings_notificationsAlert_link > span._text',
            #                           'settings_notificationsMailSave_button', visible=True)
            self.wait_until_element_is_visible('settings_notificationsAlert_link', timeout=15)
            self.click_wait_and_check('settings_notificationsAlert_link',
                                      'settings_notificationsMailSave_button', visible=True)
            # Clear all emails (Max: 5)
            while self.is_element_visible('settings_notificationsDelMail1_link', wait_time=10):
                self.wait_and_click('settings_notificationsDelMail1_link')
                self.log.info("One email address is deleted.")
                time.sleep(2)
            self.log.info("All email settings are deleted!")
            self.click_wait_and_check('settings_notificationsMailSave_button', visible=False)
            time.sleep(2)
        except Exception as e:
            self.log.error("ERROR: Unable to clear email alert settings due to exception: {}".format(repr(e)))
            self.take_screen_shot(prefix='clearEmail')

    def get_scrollbar_position(self):
        """
            Get the current scroll bar position
        :return: the current top px
        """
        scroll = self.element_find('css=.jspDrag')
        scroll_text = scroll.get_attribute('style')
        cur_top = 0.0
        self.log.info("STYLE: {}".format(scroll_text))
        if 'top' not in scroll_text:
            cur_top = 0.0
        else:
            cur_top = float(scroll_text.split('top:')[-1].split('px;')[0].strip())
        return cur_top

    def ui_clear_alert(self, alertList=None):
        """
            Check if generated alert code is on UI page
            Clear alerts after check

            @alertCode: Pass the generated alert code for verification

            Note: SKY-3385 It can't display the alert of quota almost full(2038) and full(2039)
        """
        if alertList is None:
            self.log.error("ui_clear_alert() requires 1 argument")
            return 0
        self.set_web_access_timeout(timeout='60')   # default is 30
        self.get_to_page('Home')
        # self.wait_and_click('css=#notification_toolbar')
        time.sleep(5)
        try:
            self.click_wait_and_check('css=#id_alert', 'css=#home_alertView_button', visible=True)
        except Exception as e:
            self.wait_and_click('css=#id_alert', timeout=10)
            self.log.error("ERROR: No alerts found, exception: {}".format(repr(e)))
            self.take_screen_shot(prefix='noalerts')
            return

        # Check if alerts exist
        if self.is_element_visible('css=#home_alertView_button'):
            self.click_wait_and_check('css=#home_alertView_button',
                                      'css=#home_alertViewClose_button', visible=True)
            # code = self.get_text("//div[@class=\'alert_tb\']/table/tbody/tr[3]/td[3]") # Get the first alert code string
            numOfAlerts = len(alertList)
            icon_sev = {
                'a_icon_c': 0,  # critical icon (PRI == 0)
                'a_icon_w': 1,  # warning icon (PRI == 1)
                'a_icon_i': 2,  # information icon (PRI == 2)
            }
            sev = 'a_icon_c'  # Default value set to critical (PRI == 0)
            row_elem = self.element_find("//div[@id=\'id_alert_all\']")
            rows = len(row_elem.find_elements_by_xpath("//div[@id=\'id_alert_all\']/div"))
            if rows != numOfAlerts:
                self.log.error("ERROR: Generated {} alerts but UI only has {} alerts".format(numOfAlerts, rows))
            else:
                self.log.info("Detect {} alerts on UI, same as generated alerts".format(rows))
            code_dict = {}
            count = 1
            retry = 5
            while count <= rows:
                time.sleep(1)
                while retry >= 0:
                    if count % 2 == 0:
                        if self.is_element_visible('css=div.jspDrag', wait_time=5):
                            cur_top = self.get_scrollbar_position()
                            retry = 3
                            while cur_top >= 0.0:
                                try:
                                    self.drag_and_drop_by_offset('css=.jspDrag', 0, 80)
                                    self.log.info("Scrolling the bar")
                                except Exception:
                                    pass
                                finally:
                                    if retry == 0:
                                        self.log.info("Scroll bar reaches the bottom")
                                        break
                                    new_top = self.get_scrollbar_position()
                                    if new_top == cur_top:
                                        retry -= 1
                                        self.log.info("Did not scroll bar, retry again")
                                    else:
                                        break
                    try:
                        icon = self.element_find("//div[@id=\'id_alert_all\']/div[{}]/table/tbody/tr[1]/td[2]/div".format(count))
                        sev = icon.get_attribute('class')
                        code = self.get_text("//div[@id=\'id_alert_all\']/div[{}]/table/tbody/tr[3]/td[3]".format(count))  # Get the first alert code string
                    except Exception as e:
                        if retry == 0:
                            self.log.error('ERROR: Reach the maximum retry to get UI alert code text')
                            raise
                        self.log.debug("Going to retry, exception: {}".format(repr(e)))
                        retry -= 1
                        continue
                    else:
                        # self.log.info('SEV: {}'.format(sev))
                        code = code.split(':')[1]
                        break
                self.log.info("UI ALERT CODE: {}, SEV: {}".format(code, sev))
                if len(code) == 1:
                    code = '0' * 3 + code
                elif len(code) == 2:
                    code = '0' * 2 + code
                elif len(code) == 3:
                    code = '0' * 1 + code
                # codeList.append(code)
                code_dict[code] = icon_sev[sev]
                count += 1
            # Check alerts
            self.log.info("====== Verify UI alerts ======")
            for k in alertList:
                if k in code_dict.keys():
                    if alertTable[k]['PRI'] == code_dict[k]:
                        self.log.info("PASS: Generated code({}) is on UI page...".format(k))
                        # codeList.remove(k)
                        code_dict.pop(k)
                    else:
                        self.log.error("ERROR: Generated code({}) with PRI={} does NOT match PRI={} on UI...".format(
                            k, alertTable[k]['PRI'], code_dict[k]))
                else:
                    self.log.error("ERROR: Generated code({}) can not be found on UI".format(k))
                    # self.takeScreenShot(prefix=k)
            time.sleep(1)
            self.log.info("Click Dismiss All button to clear alert messages")
            self.click_wait_and_check('css=#home_alertDelAll_button', visible=False)
        else:  # no alerts on UI, might be removed
            self.log.error("No alerts are found, take screenshot")
            self.take_screen_shot(prefix='noAlertsFound')
        self.execute('alert_test -R')
        # self.close_webUI()
        self.reload_page()
        # self.web_admin_logout()
        time.sleep(5)

emailAlertsQuotaLimits()