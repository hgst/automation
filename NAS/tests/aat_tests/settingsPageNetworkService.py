""" Settings Page-Network-Service

    @Author: Lee_e
    
    Objective: verify default value of network service if correct and can be modified on UI
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning : PASS (FW: 2.00.216)
                    Lightning : PASS (FW: 2.10.109)
                    KC        : PASS (FW: 2.00.215)
                    Glacier   : PASS (FW: 2.10.111 & 2.00.223)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.112 & 2.00.216)
                              : Yosemite     - PASS (FW: 2.10.113 & 2.00.216)
                              : Kings Canyon - PASS (FW: 2.10.113 & 2.00.216)
                              : Sprite       - PASS (FW: 2.10.113 & 2.00.216)
                              : Zion         - PASS (FW: 2.10.107 & 2.00.216)
                              : Aurora       - PASS (FW: 2.10.113 & 2.00.221)
                              : Yellowstone  - PASS (FW: 2.10.113 & 2.00.216)
                              : Glacier      - PASS (FW: 2.10.127)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os
import requests


default_FTP_value = 0
default_AFP_value = 1
default_NFS_value = 0
default_webDAV_value = 0
NFS_exclude_list = ['LT4A', 'GLCR']
AFP_exclude_list = ['GLCR']
webDAV_exclude_list = ['GLCR']


class SettingsPageNetworkService(TestClient):
    def run(self):
        self.log.info('*** load system config default ***')
        self.run_on_device('kill_running_process ; load_default 1 ; touch /var/www/system_boot.html')
        self.reboot(max_boot_time=600, use_rest=True)
        self.set_ssh(enable=True)
        time.sleep(3)
        model_number = self.get_model_number()
        self.log.debug('model number: {0}'.format(model_number))      
        self.network_service_test(model_number)
    
    def tc_cleanup(self):
        self.turn_on_by_WOL()
        self.config_ftp_cgi(enable=default_FTP_value)
        model_number = self.get_model_number(use_rest=False)
        self.log.debug('model number: {0}'.format(model_number))
        if model_number not in AFP_exclude_list:
            self.config_afp_cgi(enable=default_AFP_value)

        if model_number not in NFS_exclude_list:        
            self.config_nfs_cgi(enable=default_NFS_value)
            
        if model_number not in webDAV_exclude_list:
            self.config_webDAV_cgi(enable=default_webDAV_value)
    
    def network_service_test(self, model_number):
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', self.Elements.NETWORK_BUTTON)
        self.click_check_element(self.Elements.NETWORK_BUTTON, 'mainbody')
        self.ftp_server_test(default_value=default_FTP_value)
        if model_number not in AFP_exclude_list:
            self.afp_test(default_value=default_AFP_value)
        
        if model_number not in NFS_exclude_list:
            self.nfs_test(default_value=default_NFS_value)
        
        if model_number not in webDAV_exclude_list:                
            self.webDAV_test(default_value=default_webDAV_value)
        self.close_webUI()

    def ftp_server_test(self, default_value):
        self.log.info('*** Start FTP Server default value and Toggle Testing ***')
        self.log.debug('default_ftp_server_status: {0}'.format(default_value))
        ftp_server_UI_initial = self.get_ftp_server_status_from_UI()
        ftp_server_status_dict = {'ON': 1, 'OFF': 0}
        if default_value == ftp_server_status_dict[ftp_server_UI_initial]:
            self.log.info('PASS: FTP Server UI default shows {0} correctly'.format(ftp_server_UI_initial))  
        else:
            self.log.error('FAIL: FTP Server UI shows {0} but should be \'OFF\''.format(ftp_server_UI_initial)) 
        
        for i in range(0, 1):
            ftp_server_current_toggle = self.ftp_server_config_from_UI(ftp_server_UI_initial)
            ftp_server_UI_initial = ftp_server_current_toggle
            
    def afp_test(self, default_value):
        self.log.info('*** Start AFP default value and Toggle Testing ***')
        self.log.debug('default_afp_status: {0}'.format(default_value))
        afp_UI_initial = self.get_afp_status_from_UI()
        afp_status_dict = {'ON': 1, 'OFF': 0}
        if default_value == afp_status_dict[afp_UI_initial]:
            self.log.info('PASS: AFP UI default shows {0} correctly'.format(afp_UI_initial))  
        else:
            self.log.error('FAIL: AFP Server UI shows {0} but should be \'ON\''.format(afp_UI_initial)) 
        
        for i in range(0, 1):
            afp_current_toggle = self.afp_config_from_UI(afp_UI_initial)
            afp_UI_initial = afp_current_toggle

    def nfs_test(self, default_value):
        self.log.info('*** Start NFS default value and Toggle Testing ***')
        self.log.debug('default_nfs_status: {0}'.format(default_value))
        nfs_UI_initial = self.get_nfs_status_from_UI()
        nfs_status_dict = {'ON': 1, 'OFF': 0}
        if default_value == nfs_status_dict[nfs_UI_initial]:
            self.log.info('PASS: NFS UI default shows {0} correctly'.format(nfs_UI_initial))  
        else:
            self.log.error('FAIL: NFS UI shows {0} but should be \'OFF\''.format(nfs_UI_initial)) 
        
        for i in range(0,1):
            nfs_current_toggle = self.nfs_config_from_UI(nfs_UI_initial)
            nfs_UI_initial = nfs_current_toggle

    def webDAV_test(self, default_value):
        self.log.info('*** Start webDAV default value and Toggle Testing ***')
        self.log.debug('default_webDAV_status: {0}'.format(default_value))
        webDAV_UI_initial = self.get_webDAV_status_from_UI()
        webDAV_status_dict = {'ON': 1, 'OFF': 0}
        if default_value == webDAV_status_dict[webDAV_UI_initial]:
            self.log.info('PASS: webDAV UI default shows {0} correctly'.format(webDAV_UI_initial))  
        else:
            self.log.error('FAIL: webDAV UI shows {0} but should be \'OFF\''.format(webDAV_UI_initial)) 
        
        for i in range(0,1):
            webDAV_current_toggle = self.webDAV_config_from_UI(webDAV_UI_initial)
            webDAV_UI_initial = webDAV_current_toggle
    
    def ftp_server_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.log.info('*** Toggle FTP Server to {0} Test ***'.format(value_oppsite_dict[value]))
        if value == 'ON':
            self.click_check_element(self.Elements.SETTINGS_NETWORK_FTP_SWITCH, self.Elements.UPDATING_STRING, 'invisible',\
                                     'settings_networkFTPAccess_switch', str(value_dict[value]), 'value')
            current_value = self.get_ftp_server_status_from_UI()
            self.log.debug('current_value: {0} , privious value: {1}'.format(current_value, value))
            if current_value == 'OFF':
                self.log.info('PASS: FTP Server can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: FTP Server button shows {0} but should be OFF'.format(current_value))
        else:
            self.click_check_element(self.Elements.SETTINGS_NETWORK_FTP_SWITCH, self.Elements.SETTINGS_NETWORK_FTP_SAVE)
            self.click_check_element(self.Elements.SETTINGS_NETWORK_FTP_SAVE, self.Elements.SETTINGS_NETWORK_FTP_LINK)
            current_value = self.get_ftp_server_status_from_UI()
            self.log.debug('current_value: {0} , privious value: {1}'.format(current_value, value))
            if current_value == 'ON':
                self.log.info('PASS: FTP Server can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: FTP Server button shows {0} but should be ON'.format(current_value))
        return current_value                 
            
    def get_ftp_server_status_from_UI(self):
        self.log.info('*** Get FTP Server Status From UI')
        value = self.get_text(self.Elements.SETTINGS_NETWORK_FTP_SWITCH)
        
        return value
    
    def afp_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.log.info('*** Toggle AFP {0} Test ***'.format(value_oppsite_dict[value]))
        self.click_check_element(self.Elements.SETTINGS_NETWORK_AFP_SWITCH, self.Elements.UPDATING_STRING ,'invisible',\
                                 'settings_networkAFP_switch', str(value_dict[value]), 'value')
        current_value = self.get_afp_status_from_UI()
        self.log.debug('current_value: {0} , privious value: {1}'.format(current_value, value))
        if value == 'ON': 
            if current_value == 'OFF':
                self.log.info('PASS: AFP can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: AFP button shows {0} but should be OFF'.format(current_value))
        else:         
            if current_value == 'ON':
                self.log.info('PASS: AFP can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: AFP button shows {0} but should be ON'.format(current_value))
        return current_value                 
            
    def get_afp_status_from_UI(self):
        self.log.info('*** Get AFP Status From UI')
        value = self.get_text(self.Elements.SETTINGS_NETWORK_AFP_SWITCH)
        
        return value

    def nfs_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.log.info('*** Toggle NFS {0} Test ***'.format(value_oppsite_dict[value]))
        self.click_check_element(self.Elements.SETTINGS_NETWORK_NFS_SWITCH, self.Elements.UPDATING_STRING, 'invisible',\
                                 'settings_networkNFS_switch', str(value_dict[value]), 'value')
        current_value = self.get_nfs_status_from_UI()
        self.log.debug('current_value: {0} , privious value: {1}'.format(current_value,value))  
        if value == 'ON': 
            if current_value == 'OFF':
                self.log.info('PASS: NFS can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: NFS button shows {0} but should be OFF'.format(current_value))
        else:         
            if current_value == 'ON':
                self.log.info('PASS: NFS can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: NFS button shows {0} but should be ON'.format(current_value))
        return current_value                 
            
    def get_nfs_status_from_UI(self):
        self.log.info('*** Get NFS Status From UI')
        value = self.get_text(self.Elements.SETTINGS_NETWORK_NFS_SWITCH)
        
        return value    

    def webDAV_config_from_UI(self, value):
        value_oppsite_dict = {'ON': 'OFF', 'OFF': 'ON'}
        value_dict = {'ON': 0, 'OFF': 1}
        self.log.info('*** Toggle webDAV {0} Test ***'.format(value_oppsite_dict[value]))
        self.click_check_element(self.Elements.SETTINGS_NETWORK_WEBDAV_SWITCH, self.Elements.UPDATING_STRING, 'invisible',\
                                 'settings_networkWebdav_switch', str(value_dict[value]), 'value')
        current_value = self.get_webDAV_status_from_UI()
        self.log.debug('current_value: {0} , privious value: {1}'.format(current_value, value))
        if value == 'ON': 
            if current_value == 'OFF':
                self.log.info('PASS: webDAV can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: webDAV button shows {0} but should be OFF'.format(current_value))
        else:         
            if current_value == 'ON':
                self.log.info('PASS: webDAV can switch to {0} and shows correctly'.format(current_value))
            else:
                self.log.error('FAIL: webDAV button shows {0} but should be ON'.format(current_value))
        return current_value                 
            
    def get_webDAV_status_from_UI(self):
        self.log.info('*** Get webDAV Status From UI')
        value = self.get_text(self.Elements.SETTINGS_NETWORK_WEBDAV_SWITCH)
        
        return value

    def config_ftp_cgi(self, enable):
        self.log.info('*** Config FTP: {0} ***'.format(enable))
        head_url = 'app_mgr.cgi'
        ftp_server_cgi = 'FTP_Server_Enable&f_state='+str(enable)
        self.send_cgi(ftp_server_cgi, head_url)
    
    def config_afp_cgi(self, enable):
        self.log.info('*** Config AFP: {0} ***'.format(enable))
        head_url = 'account_mgr.cgi'
        afp_cgi = 'cgi_set_afp&afp='+str(enable)
        self.send_cgi(afp_cgi, head_url)
        
    def config_nfs_cgi(self, enable):
        self.log.info('*** Config NFS: {0} ***'.format(enable))
        head_url = 'account_mgr.cgi'
        cgi_nfs_enable = 'cgi_nfs_enable&nfs_status='+str(enable)
        self.send_cgi(cgi_nfs_enable, head_url)
        
    def config_webDAV_cgi(self, enable):
        self.log.info('*** Config webDAV: {0} ***'.format(enable))
        head_url = 'webdav_mgr.cgi'
        cgi_webdav_enable = 'cgi_webdav_enable&webdav='+str(enable)
        self.send_cgi(cgi_webdav_enable, head_url)        
    
    def get_ftp_server_config_cgi(self):
        self.log.info('*** Get FTP Server Information ***')
        ftp_server_get_config_cgi = 'FTP_Server_Get_Config'
        head_url = 'app_mgr.cgi'
        result = self.send_cgi(ftp_server_get_config_cgi, head_url)
        ftp_server_value = ''.join(self.get_xml_tags(result, tag='state'))
        self.log.info('ftp server: {0}'.format(ftp_server_value))
        return ftp_server_value
        
    def cgi_get_afp_info(self):
        self.log.info('*** Get AFP Info ***')
        get_afp_info = 'cgi_get_afp_info'
        head_url = 'account_mgr.cgi'
        result = self.send_cgi(get_afp_info, head_url)
        afp_value = ''.join(self.get_xml_tags(result, tag='enable'))
        self.log.info('afp status: {0}'.format(afp_value))
        return afp_value

    def cgi_get_nfs_info(self):
        self.log.info('*** Get NFS Info ***')
        get_nfs_info = 'cgi_get_nfs_info'
        head_url = 'account_mgr.cgi'
        result = self.send_cgi(get_nfs_info, head_url)
        nfs_value = ''.join(self.get_xml_tags(result, tag='enable'))
        self.log.info('nfs status: {0}'.format(nfs_value))
        return nfs_value

    def cgi_get_webdav_info(self):
        self.log.info('*** Get WebDAV Info ***')
        get_webdav_info = 'cgi_get_device_info'
        head_url = 'system_mgr.cgi'
        result = self.send_cgi(get_webdav_info, head_url)
        webdav_value = ''.join(self.get_xml_tags(result, tag='webdav'))
        self.log.info('webdav status: {0}'.format(webdav_value))
        return webdav_value
    
    def send_cgi(self, cmd, url2_cgi_head):
        url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        url_2 = head + cmd
        self.log.debug('login cmd: '+url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+url_2)
        with requests.session() as s:
            s.post(url, data=user_data)
            r = s.get(url_2)
            time.sleep(10)
            response = r.text.encode('ascii', 'ignore')
            self.log.debug('response : {0}'.format(response))
            if 'Internal Server Error' in response:
                raise Exception(str(response))
            
            return response
            
    def click_check_element(self, element, next_element, visible='visible', check_locator=None, expected_value=None, attribute_type=None, attempts=3, attempt_delay=2):
        # #@ Brief: Click an element and verify if the check_element is visible/invisible
        #
        # @param element: The element which is clicked
        # @param next_element: The expected element after clicking element
        # @param visible: next_element is 'visible' or 'invisible', default is 'invisible'
        # @param attempts: allowed retry times
        # @param attempt_delay: interval between attempts
        # @param check_locator: locator which used to check_attribute
        # @param expected_value: expected value of check_locator should be after clicked
        # @param attribute_type: type of check_locator which used to check 
        for attempts_remaining in range(attempts, -1, -1):
            if attempts_remaining > 0:
                try:
                    self.log.debug('Clicking element: "{0}" and wait element: "{1}"'.format(element, next_element))
                    self.click_element(element)
                    if visible == 'visible':
                        self.wait_until_element_is_visible(next_element, timeout=60)
                    else:
                        self.wait_until_element_is_not_visible(next_element)
                    if check_locator is not None:
                        self.check_attribute(check_locator, expected_value, attribute_type)
                    time.sleep(5)
                    self.log.debug('Element: "{0}" is {1}.'.format(next_element, visible))
                    break
                except:
                    self.log.info('Element: "{0}" did not click. Wait {1} seconds and retry. {2} attempts remaining.' \
                                  .format(element.name, attempt_delay, attempts_remaining))
                    time.sleep(attempt_delay)
            else:
                self.log.exception('Failed to popup element: "{0}"'.format(element))

    def turn_on_by_WOL(self):
        self.log.info('********** state = turn_on_by_WOL *********')
        retry = 2
        while retry > 0:
            try:
                self.send_magic_packet()
                time.sleep(3)
                start_time = time.time()
      
                while not self.is_ready():
                    self.log.info('ping system')
                    time.sleep(20)
                    if time.time() - start_time > 380:
                        raise Exception('Timed out for waiting to boot by WoL:{0} seconds '.format(time.time()-start_time))  
                if self.is_ready():
                    self.log.info(' WoL spend time: {0} seconds'.format(time.time()-start_time))  
                    self.log.info('PASS: Wake on Lan test Success')   
                    break
            except Exception as e:
                self.log.debug('Retry {0} times to WoL'.format(2-retry))
                retry = retry -1

SettingsPageNetworkService()