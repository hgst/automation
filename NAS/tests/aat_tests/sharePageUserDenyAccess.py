"""
title           :sharePageUserDenyAccess.py
description     : Shares Page - to verify if a user can be authorized 'Deny' access to a given folder
step            : 1. Click on 'share' category
                  2. Select a share (public = OFF)
                  3. Select a user, and give 'deny access' privilege
                  4. Verify if this user cannot access the share
author          :yang_ni
date            :2015/05/04
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
from smb.smb_structs import OperationFailure

class sharePageUserDenyAccess(TestClient):

    def run(self):

        try:
            #Delete all the users, groups and shares
            self.setup()

            # Create a user
            self.log.info('Creating a user ...')
            self.create_user(username='user1')

            # Create a share.
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=1)

            # Change share to private, share must be set to private instead of public. Toggle the Public switch off
            self.log.info('Toggling the share from private to public ...')
            self.update_share_network_access(share_name='share', public_access=False)

            # Assigns user1 to share with no r/w access
            self.log.info('Assigning user1 with deny access to share ...')
            self.assign_user_to_share(user_name='user1', share_name='share', access_type='d')

            #Check if 'Deny' access of share is given to user1
            self.log.info('Checking if user1 has deny access to share ...')

            '''
            Check if user1 has 'Deny Access' privilege to share (test on Web UI)
            '''
            time.sleep(20)
            self.get_to_page('Shares')
            time.sleep(5)
            self.click_button('shares_share_share')
            text = self.get_text("//div[@id=\'userlist\']/ul/li[2]/div[4]")

            if text == 'Deny Access':
                self.log.info('User1 has deny access to share folder')
            else:
                self.log.error('Failed to authorize deny access of share folder to user1')

            self.close_webUI()

            '''
            Check if user1 has 'Deny Access' privilege to share (test by SMB file transfer)
            '''
            test_file = self.generate_test_file(kilobytes=10)

            #Check if user1 has write access to the share folder, fail if yes.
            try:
                self.write_files_to_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.info('Failed to write file from share folder, correct behavior')
            else:
                self.log.error('Successfully to write file to share folder, wrong behavior')

            #Check if user1 has read access to the share folder, fail if yes.
            try:
                self.read_files_from_smb(files=test_file, username='user1', delete_after_copy=True, share_name='share')
            except OperationFailure:
                self.log.info('Failed to read file from share folder, correct behavior')
                self.log.info('Successfully authorizes deny access to user1')
            else:
                self.log.error('Successfully to read file to share folder, wrong behavior')
        except Exception as e:
            self.log.error('Failed to authorizes deny access to a user1, error: {}'.format(repr(e)))


# # @Clean up all the users/shares/groups before test starts
    def setup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

# # @Clean up all the users/shares/groups after test finishes
    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

# This constructs the sharePageUserDenyAccess() class, which in turn constructs TestClient() which triggers the sharePageUserDenyAccess.run() function
sharePageUserDenyAccess()