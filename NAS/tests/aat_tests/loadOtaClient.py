"""
Create on Jan 29, 2016
@Author: lo_va
Objective:  Verify otaclient module is loaded in firmware
            Verify otaclient daemon is running w/o error
            KAM-349
wiki URL: http://wiki.wdc.com/wiki/Load_otaclient_in_firmware
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import time


class LoadOtaClient(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        otaclient_daemon = ''
        otaclient_files_list = ''
        try:
            otaclient_daemon = self.execute('/etc/init.d/otaclientd status')[1]
            otaclient_files_list = self.execute('find / -name *otaclient*')[1]
            if not otaclient_daemon:
                time.sleep(20)
                otaclient_daemon = self.execute('/etc/init.d/otaclientd status')[1]
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['otaclient is running']
        check_list2 = ['/usr/local/otaclient/otaclient', '/etc/init.d/otaclientd', '/var/lib/dpkg/info/otaclient.md5sums']
        if all(word in otaclient_daemon for word in check_list1) and all(word in otaclient_files_list for word in check_list2):
            self.log.info('Verify OTA Client is loaded in firmware PASSED!!')
        else:
            self.log.error('Verify OTA Client is loaded in firmware FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test!!')
            exit()

LoadOtaClient(checkDevice=False)
