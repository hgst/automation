""" Settings Page - Notifications - SMS (MA-94)

    @Author: lin_ri
    Procedure @ silk (http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?execView=execDetails&pId=50&nTP=117539&view=details&nEx=20018&pltab=steps)
                1) Open webUI to login as system admin
                2) Go to Settings Page
                3) Select Notifications -> SMS
                4) Enable SMS
                5) Click configure
                6) Enter the info to configure SMS
                7) Verify the settings are saved => SMS messages can be sent
                   You have to verify if the cellphone can receive the Test SMS
    
    Automation: Partial
                1) Need to input the mobile phone number you'd like to receive SMS messages
                2) Verify if you do receive the Test SMS message on the phone number you check-in 
    
    Manual:
            I. Uses kotsms which WD TW registered (www.kotsms.com.tw) 
            
               1) No need to register the account 
               
               API: http://202.39.48.216/kotsmsapi-1.php?username=WDTW&password=WDTWSEV&dstaddr=886912345678&smbody=MSG test fromNAS
               
               2) You have to verify if you get Test SMS messages from 0961-238-382(Manual)
               
            II. Use clickatell, Need to use self.set_clickatell_sms_notification()
              
            Register an account manually and pass them to set_sms_notification parameters
            1) Go to www.clickatell.com
            2) Select "Developers' Central" and "International"
            3) Fill all of your information including email and mobile#
            4) Check your email and click the authentiation link to complete the registration
            5) Go to https://central.clickatell.com/api/manage
               Login with your username/password and ClientID
            6) You can see the values you have to pass to the set_sms_notification()
            Ex: 
               self.set_clickatell_sms_notification(vendor='Clickatell', username='TestUser', userpwd='TestPassword', apiId=3148203, mobile=886123456789, msg="Hello World")
            7) You have to verify if you get Test SMS message
    
    Note: Your SMS configuration will be deleted at the end of the test
                   
    Status: Local lightning FW 2.00.133: PASS
""" 

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import subprocess
import time
from selenium.webdriver.common.keys import Keys
import os

picturecounts = 0

class nasSMSMsg(TestClient):
    
    def run(self):
        self.log.info('######################## Settings Notifications SMS TESTS START ##############################')
        try:
            self.access_webUI()
            #self.set_sms_notification(username='TestUser', userpwd='TestPassword', mobile=886123456789, msg="Hello World")
            self.set_sms_notification(vendor='kotsms', username='WDTW', userpwd='WDTWSEV', mobile=886912345678, msg="MSG from WD MyCloud EX4 NAS!!!")
            #self.set_clickatell_sms_notification(username='TestUser', userpwd='TestPassword', apiId=3148203, mobile=8860912345678, msg="Hello World")
            #self.set_clickatell_sms_notification(username="rickhau", userpwd="IbcKRSFbECdTHf", apiId=3521620, mobile=886912345678, msg="WDMYCLOUDEX4-NAS SMS MSG")
            self.close_webUI()
            self.log.info("Please check your mobile if you get the Test SMS")
        except Exception as e:
            self.takeScreenShot(prefix='run')
            self.log.error("FAILED: Settings Notifications SMS Test Failed!!, exception: {}".format(repr(e)))
        else:
            self.log.info("Conditional PASS: Settings Notifications SMS Test Pass, You have to check your mobile phone for Test SMS message!!")
        finally:
            self.log.info("========== (MA-94) Clean Up ==========")
            self.delete_sms_notification()
        self.log.info('######################## END Settings Notifications SMS Messages TESTS ##############################')

    def set_sms_notification(self, vendor='kotsms', username='WDTW', userpwd='WDTWSEV',
                             mobile='886912345678', msg="MSG from WD MyCloud EX4 NAS!!!"):
        """
            Configure SMS notification

            @username: kotsms's account name
            @userpwd: kotsms's account password
            @mobile: Your cellphone filled in Clickatell
            @msg: Message you'd like to transmit
        """
        try:
            self.log.info("==> Configure SMS notification")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            # self.wait_and_click('settings_notifications_link')
            self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
            self.log.info("Slide the notification display bar to All")
            time.sleep(1)
            self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
            self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
            time.sleep(2)
            self.log.info("====== Turn on SMS notification ======")
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'ON':
                self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                          'settings_notificationsSms_link', visible=True)
            sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
            if sms_status != 'ON':
                raise Exception("ERROR: Fail to enable SMS switch button")
            self.log.info("SMS notification is enabled to {}".format(sms_status))
            time.sleep(2)
            self.log.info("====== Click SMS configure ======")
            self.click_wait_and_check('settings_notificationsSms_link',
                                      'smsDiag_title', visible=True)
            # self.wait_and_click('css=#settings_notificationsSms_link > span._text', timeout=15)
            if self.is_element_visible('settings_notificationsSmsDel_button', 10):
                self.log.info("Clean existing SMS configuration")
                self.click_element("settings_notificationsSmsDel_button")
                time.sleep(3)
            time.sleep(2)
            self.log.info("Input SMS Provider Name: {}".format(vendor))
            self.input_text_check('settings_notificationsSmsName_text', vendor, False)
            SMS_URL = "http://202.39.48.216/kotsmsapi-1.php?username={}&password={}&dstaddr={}&smbody={}".format(username, userpwd, mobile, msg)
            self.log.info("SMS URL: {}".format(SMS_URL))
            time.sleep(2)
            self.input_text_check('settings_notificationsSmsUrl_text', SMS_URL, False)
            self.log.info("SMS setting is configured successfully...")
            self.click_wait_and_check('settings_notificationsSmsNext1_button',
                                      'settings_notificationsSmsBack2_button', visible=True)
            time.sleep(2)
            self.log.info("====== Configure SMS link settings ======")
            self.click_link_element('id_alias0', 'Username')
            self.click_link_element('id_alias1', 'Password')
            self.click_link_element('id_alias2', 'Phone number')
            self.click_link_element('id_alias3', 'Message content')
            self.click_wait_and_check('settings_notificationsSmsSave_button', visible=False)
            time.sleep(2)
            self.log.info("Click Test SMS button to send out SMS test message")
            self.click_element('settings_notificationsSmsTest_button')
            time.sleep(2)
            # self.wait_until_element_is_visible('popup_ok_button')
            count = 1
            while count <= 3:
                if self.is_element_visible('popup_ok_button', 30):
                    break
                else:
                    time.sleep(1)
                    self.log.info("{}: Re-click Test SMS button".format(count))
                    self.click_element('settings_notificationsSmsTest_button')
                    count += 1
            sms_test = self.get_text('css=div#error_dialog_message')
            self.log.info("{}".format(sms_test))
            self.click_wait_and_check('popup_ok_button', visible=False)
            time.sleep(1)
            self.click_wait_and_check('settings_notificationsSmsSetSave_button', visible=False)
        except Exception as e:
            self.log.error("ERROR: Fail to set SMS notification message, exception: {}".format(repr(e)))
        
    def set_clickatell_sms_notification(self, vendor='Clickatell', username='ricklin', userpwd='SDCHgCJDYORefR', apiId=3520459, mobile=886912345678, msg="MSG from WD NAS"):
        """
            Configure SMS notification
            
            @username: Clickatell's account name (ex: Fituser)
            @userpwd: Clickatell's account password (ex: Fituser102013)
            @apiId: Clickatell's API ID (3442171)
            @mobile: Your cellphone filled in Clickatell
            @msg: Message you'd like to transmit (It seems that you can not send out the msg unless you purchase)
        """
        sms_status = None
        self.log.info("==> Configure SMS notification")
        self.get_to_page('Settings')
        self.click_element('settings_notifications_link')
        self.log.info("Slide the notification display bar to All")
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 50%
        self.press_key("//div[@id='settings_notificationsDisplay_slider']/a", Keys.ARROW_RIGHT) # 100%
        time.sleep(1)
        self.log.info("Turn on SMS notification")
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_element('css=#settings_notificationsSms_switch+span .checkbox_container')
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')            
        self.log.info("SMS notification is enabled to {}".format(sms_status))
        time.sleep(2)
        self.log.info("Click SMS configure")
        # self.click_element('css=#settings_notificationsSms_link > span._text')
        self.click_element('settings_notificationsSms_link')
        if self.is_element_visible('settings_notificationsSmsDel_button'):
            self.log.info("Clean existing SMS configuration")
            self.click_element("settings_notificationsSmsDel_button")
            time.sleep(2)        
        self.input_text_check('settings_notificationsSmsName_text', vendor, False)
        SMS_URL = "http://api.clickatell.com/http/sendmsg?user={}&password={}&api_id={}&to={}&text={}".format(username, userpwd, apiId, mobile, msg)
        self.log.info("SMS URL: {}".format(SMS_URL))
        self.input_text_check('settings_notificationsSmsUrl_text', SMS_URL, False)
        self.click_element('settings_notificationsSmsNext1_button')
        time.sleep(1)
        self.log.info("Configure SMS settings")
        self.click_element('id_alias0')
        self.click_element('link=Username')
        self.click_element('id_alias1')
        self.click_element('link=Password')
        self.click_element('id_alias2')
        self.click_element('link=Other')                
        self.click_element('id_alias3')
        self.click_element('link=Phone number')         
        self.click_element('id_alias4')
        self.click_element('link=Message content')
        self.click_element('settings_notificationsSmsSave_button')
        time.sleep(2)
        self.log.info("Click Test SMS button to send out SMS test message")
        self.click_element('settings_notificationsSmsTest_button')
        self.wait_until_element_is_visible('popup_ok_button')
        sms_test = self.get_text('css=div#error_dialog_message')
        self.log.info("{}".format(sms_test))
        self.click_element('popup_ok_button')      
        self.click_element('settings_notificationsSmsSetSave_button')

    def delete_sms_notification(self):
        """
            Delete SMS configuration
        """
        sms_status = None
        self.log.info("==> Clear SMS notification")
        self.get_to_page('Settings')
        self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
        self.click_wait_and_check('css=#notifications', 'css=#notifications.LightningSubMenuOn', visible=True)
        self.log.info("Turn on SMS notification")
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')
        if sms_status != 'ON':
            self.click_wait_and_check('css=#settings_notificationsSms_switch+span .checkbox_container',
                                      'css=#settings_notificationsSms_switch+span .checkbox_container .checkbox_on',
                                      visible=True)
        sms_status = self.get_text('css=#settings_notificationsSms_switch+span .checkbox_container')            
        self.log.info("SMS notification is enabled to {}".format(sms_status))
        time.sleep(2)
        self.log.info("Click SMS configure")
        # self.wait_and_click('css=#settings_notificationsSms_link > span._text')
        self.click_wait_and_check('settings_notificationsSms_link',
                                  'smsDiag_title', visible=True)
        if self.is_element_visible('settings_notificationsSmsDel_button'):
            self.log.info("Clean existing SMS configuration")
            self.click_wait_and_check("settings_notificationsSmsDel_button", visible=False)
            time.sleep(3)
            self.log.info("Close SMS Setting dialog window")
            self.click_wait_and_check('css=#settings_notificationsSmsCancel1_button', visible=False)
            self.log.info("Succeed to clear SMS notification")
            self.log.info("SMS notification will be turned to OFF due to no configuration...")
        time.sleep(2)

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

        
nasSMSMsg()