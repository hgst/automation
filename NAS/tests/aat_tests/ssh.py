'''
2015/03/25
Due to procedure change --> update the test scrip 
'''

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields

File_Size = 1024
scp_pix_target_path = '/shares/Public/Shared\ Pictures/'
sftp_pix_target_path = '/shares/Public/Shared Pictures/'
default_password = 'welc0me'
newpassword='passw0rd1'

class Ssh(TestClient):

    def run(self):      
        self.log.info('**** Start Test ****')
        self.blank_ssh_password_webui_test(union=False, newpassword=newpassword)
        self.blank_ssh_password_webui_test(union=True, newpassword=newpassword)
        self.close_webUI()
        self.log.info('current self.uut[Fields.ssh_password]:  '+str(self.uut[Fields.ssh_password]))
        result = self.login_test(username='root', wrong_password_test=True)
        if result:
            self.log.error('FAILED: log in SSH by root & welc0me should not pass')
        else:
            self.log.info('PASS: log in SSH by root & welc0me is failed')
            try:
                self.log.info('Try if sshd password does not be updated, use default password: welc0me and sshd connect')
                result = self.login_test(username='sshd', wrong_password_test=True)
                if result:
                    self.log.info('SSH login with sshd & welc0me')
            except Exception as e:
                self.log.info('Exception: {0}'.format(repr(e)))
            
        result = self.login_test(username='user1')
        if result:
            self.log.error('FAILED: log in SSH by other user should not pass')
        else:
            self.log.info('PASS: log in SSH by other user is failed')

        result = self.login_test(username='sshd')
        if result:
            self.log.info('SSH login by sshd & passw0rd1 successfully')
        else:
            self.log.error('SSH login by sshd & passw0rd1 failed')
        
        self.log.info('current self.uut[Fields.ssh_password]:  '+str(self.uut[Fields.ssh_password]))
        
        self.log.info('*** sshd & root login with default password test ***')
        self.log.info('*** Update password to default ***')
        self.update_ssh_username(username='root')
        self.set_ssh_password()
        self._set_password_through_ssh(user='sshd', password=self.uut[Fields.default_ssh_password])
        
        result = self.login_test(username='root')
        if result:
            self.log.info('PASS: log in SSH by root & default password should pass')
        else:
            self.log.error('FAIL: log in SSH by root & default password is failed')
        
        result = self.login_test(username='sshd')
        if result:
            self.log.info('SSH login by sshd & default password successfully')
        else:
            self.log.error('SSH login by sshd & default password failed')
            
        self.sftp_file_test()
        self.ssh_scp_file_test()
    
    def tc_cleanup(self):
        self._set_password_through_ssh(user='root', password=self.uut[Fields.default_ssh_password])
        self._set_password_through_ssh(user='sshd', password=self.uut[Fields.default_ssh_password])
    
    def blank_ssh_password_webui_test(self, union=True, newpassword=None, confirm_password=None, use_web_ui=True):
        test_name = 'Testing blank of ssh password setting on WEB UI, union:{0}'.format(union)
        self.log.info("#### {0} ####".format(test_name))
        if union:
            confirm_password = newpassword
        try:
            result = self.set_ssh_password(newpassword=newpassword, confirm_password=confirm_password, use_web_ui=use_web_ui)
        except Exception as e:
            self.log.exception('FAIL with exception: {0}'.format(repr(e)))
            result = 1

        if result == 0:
            self.log.info('PASS: {0}'.format(test_name))
        else:
            self.log.error('FAILED: {0}'.format(test_name))
            self.take_screen_shot('union is '+str(union))
        
    def login_test(self, username, wrong_password_test=False):
        right_pwd = self.uut[Fields.ssh_password]
        
        if wrong_password_test:
            self.uut[Fields.ssh_password] = self.uut[Fields.default_ssh_password]

        self.log.info('*** login SSH by %s and %s test ***' % (username, self.uut[Fields.ssh_password]))
        self.update_ssh_username(username=username)
        self.log.info('right password: '+str(right_pwd))
        success = True
        try:
            self.ssh_connect()
        except Exception as e:
            self.log.info('Failed to connect {0}'.format(repr(e)))
            success = False 
        self.log.debug("login result: %s" % success)
        # do login with default password done, now refill right password to dictionary
        self.uut[Fields.ssh_password] = right_pwd     
        self.log.debug('right_pwd : {0}'.format(right_pwd))
        return success  
        
    def sftp_file_test(self):
        self.log.debug('Start to upload and download by SFTP')
        generated_file = self.generate_test_file(File_Size)
        try:
            self.open_sftp_connection()
            self.sftp_file_to_device(files=generated_file, remote_path=sftp_pix_target_path, recursive=False, preserve_times=False)
            self.sftp_from_device(path=sftp_pix_target_path, target_path='.', preserve_mtime=False)
            self.log.info('PASS: upload and download file by SFTP')
            
        except Exception as e:
            self.log.exception('Unable to upload and download file by SFTP:{0}'.format(repr(e)))
    
    def ssh_scp_file_test(self):
        self.log.debug('Start to upload and download by SCP')
        generated_file = self.generate_test_file(File_Size)
        try:
            self.open_scp_connection()
            self.scp_from_device(path=scp_pix_target_path, is_dir=False, timeout=20, verbose=False, local_path='.', retry=False)
            self.scp_file_to_device(files=generated_file, remote_path=scp_pix_target_path, recursive=False, preserve_times=False)
            self.log.info('PASS: upload and download file by SCP') 
            
        except Exception as e:
            error_msg = str(e).split(':')
            
            if error_msg[2].find(' not a regular file') != 0:
                self.log.exception('FAIL:Unable to upload and download file by SCP:{0}'.format(repr(e)))
            else:
                self.log.info('PASS :{0} {1}'.format(repr(e),'upload and download file by SCP'))

Ssh()