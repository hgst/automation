'''
Created on Jun 3rd, 2015

@author: tsui_b

Objective: Verify Web File Viewer info is displayed and funcions are all available

Automation: Full
    
Supported Products: All
    
Test Status: 
            Local 
                    Lightning: Pass (FW: 2.10.120)
            Jenkins 
                    Lightning: Pass (FW: 2.10.120)
                    Kings Canyon: Pass (FW: 2.10.120)
                    Glacier: Not Support
                    Zion: Pass (FW: 2.10.120)
                    Yosemite: Pass (FW: 2.10.120)
                    Yellowstone: Pass (FW: 2.10.120)
                    Aurora: Pass (FW: 2.10.120)
                    Sprite: Pass (FW: 2.10.120)
                    Grand Teton: Pass (FW: 2.10.120)
'''
import time
import os
import requests
import urllib2
import commands

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
from selenium import webdriver
from global_libraries.testdevice import Fields
from BeautifulSoup import BeautifulSoup

picture_counts = 0
volume = 'Volume_1'
share1 = 'AAT_Share1'
share2 = 'AAT_Share2'
sub_share = 'AAT_Sub_Share'
dummyfile = 'dummyfile'
renamed_file = 'dummyfile_rename'

class appsPageWebFileViewerDownloadsInfoIsDisplayed(TestClient):

    def run(self):
        self.log.info('### Verifying Web File Viewer info is displayed and funcions are all available ###')
        try:
            self.delete_all_shares()
            self.create_shares(share_name=share1, description=share1)
            self.create_shares(share_name=share2, description=share2)
            self.clean_share(share_name=share1)
            self.clean_share(share_name=share2)
            self.create_file_in_share(share1, dummyfile)
            self.check_web_file_viewer_info()
        except Exception as e:
            self.log.error("Verify Web File Viewer info failed! Exception: {}".format(repr(e)))
            self.screenshot(prefix='run')

    def tc_cleanup(self):
        # Use delete_share instead of delete_all_shares cause delete_all_shares is vis ssh cmd 
        # and cannot delete the sub folders and files we created
        self.delete_share(share_name=share1)
        self.delete_share(share_name=share2)
        self.delete_local_file(file_name=dummyfile)

    def clean_share(self, share_name):
        self.log.info('Cleaning share folder: {}'.format(share_name))
        result = self.execute('ls /shares/{}'.format(share_name))
        if result[0] != 0:
            raise Exception('Check share folders failed!')
        else:
            if result[1]:
                del_result = self.execute('rm -r /shares/{}/*'.format(share_name))
                if del_result[0] != 0:
                    raise Exception('Failed to clean share folder:{0}! Exception:{1}'.format(share_name, del_result[1]))
                else:
                    self.log.info('Share folder: {} is cleaned.'.format(share_name))
            else:
                self.log.info('No files or folders need to be deleted in share folder:{}.'.format(share_name))

    def create_file_in_share(self, share_name, file_name):
        self.log.info('Creating a 1MB dummy file in share folder: {}.'.format(share_name))
        cmd = 'dd if=/dev/urandom of=/shares/{0}/{1} bs=1024 count=1'.format(share_name, file_name)
        result = self.execute(cmd)
        if '1+0 records in\n1+0 records out\n1024 bytes (1.0KB) copied' not in result[1]:
            raise Exception('Failed to create file in share folder: {0}! Exception:{1}'.format(share_name, result[1]))

    def check_web_file_viewer_info(self):
        # Go to web file viewer and check info
        self.get_to_page('Apps')
        self.click_wait_and_check(E.APPS_WEB_FILE, E.APPS_WEB_FILE_FRAME)
        self.current_frame_contains('Web File Viewer')
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.element_text_should_be(E.APPS_WEB_FILE_PATH, 'Shares')
        # Get share locator first
        share1_locator = self.find_share_locator(share1)
        share2_locator = self.find_share_locator(share2)
        # Test.1 : Verify copy file
        self.log.info('Copying file from {0} to {1}'.format(share1, share2))
        self.double_click_folder(share1_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share1))
        row, file_locator = self.find_file_locator(dummyfile, share1)
        # If the file is selected, the row class will be 'trSelected'
        self.click_and_check_attribute(file_locator, row, 'class', 'trSelected')
        self.click_unselect_frame_check(E.APPS_WEB_FILE_COPY, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Spec change again in FW 2.10.140..no need to select volume in this version
        # self.click_wait_and_check('link={}'.format(volume), 'link={}'.format(share2)) # Need to select vol first since FW 2.10.123
        self.click_coordinates_and_check_attribute('link={}'.format(share2), -92, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        # Check if file is copied to share folder2
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        self.double_click_folder(share2_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share2))
        # If the file is not successfully copied, find_file_locator will raise Exception cause file not exist 
        self.find_file_locator(dummyfile, share2)
        self.log.info('File is copied from {0} to {1}'.format(share1, share2))
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
    
        # Test.2 : Verify delete file
        self.log.info('Deleting file in {}'.format(share2))
        self.double_click_folder(share2_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share2))
        row, file_locator = self.find_file_locator(dummyfile, share2)
        self.click_and_check_attribute(file_locator, row, 'class', 'trSelected')
        self.click_unselect_frame_check(E.APPS_WEB_FILE_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_DELETE_DIAG)
        self.click_wait_and_check(E.APPS_WEB_FILE_DELETE_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_should_not_contain(dummyfile)
        self.log.info('File is deleted in {}'.format(share2))
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

        # Test.3 : Verify download file
        '''
            The download button will pop-up a native browser download diag which cannot be handled by selenium,
            so we use the same cgi cmd to emulate download behavior.
        '''
        self.log.info('Downloading the file from {}'.format(share1))
        result = self.execute('ls -al /shares/ | grep {}'.format(share1))
        if result[0] != 0:
            raise Exception('Check share folders failed!')
        else:
            if result[1]:
                # the info format: 
                # lrwxrwxrwx 1 root root 24 Jun 10 01:30 AAT_Share1 -> /mnt/HD/HD_a2/AAT_Share1
                share_path = result[1].split()[-1]
            else:
                raise Exception('Cannot get the info of share folder: {}!'.format(share1))
        
        result = self.send_cgi('cgi_compress&path={0}&type=File&name={1}&os=MacOS'.format(share_path, dummyfile))
        device_ip = self.uut[self.Fields.internal_ip_address]
        # cgi will return a download path
        download_url = 'http://{}/{}'.format(device_ip, self.get_xml_tags(result, 'url')[0])
        try:
            file = urllib2.urlopen(download_url)
            output = open(dummyfile, 'wb')
            output.write(file.read())
            output.close()
        except Exception as e:
            raise Exception('Download file:{0} from share folder:{1} failed! Exception:{2}'.format(dummyfile, share1, repr(e)))
        
        result = commands.getoutput('ls -al *{}'.format(dummyfile))
        if dummyfile not in result:
            raise Exception('Download file:{0} from share folder:{1} failed! File cannot be found in local os.'.format(dummyfile, share1))
        
        self.log.info('File is download from {}'.format(share1))

        # Test.4 : Verify rename file
        self.log.info('Renaming the file in {}'.format(share1))
        self.double_click_folder(share1_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share1))
        row, file_locator = self.find_file_locator(dummyfile, share1)
        self.click_and_check_attribute(file_locator, row, 'class', 'trSelected')
        self.click_unselect_frame_check(E.APPS_WEB_FILE_RENAME, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_RENAME_DIAG)        
        self.input_text_check(E.APPS_WEB_FILE_RENAME_DIAG_NAME_TEXT, renamed_file)
        self.click_wait_and_check(E.APPS_WEB_FILE_RENAME_DIAG_SAVE, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        # If the file is not successfully renamed, find_file_locator will raise Exception cause file not exist 
        self.find_file_locator(renamed_file, share1)        
        self.log.info('File is renamed in {}'.format(share1))
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

        # Test.5 : Verify move file and refresh function
        self.log.info('Moving the file from {0} to {1}'.format(share1, share2))
        self.double_click_folder(share1_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share1))
        row, file_locator = self.find_file_locator(renamed_file, share1)        
        self.click_and_check_attribute(file_locator, row, 'class', 'trSelected')
        self.click_unselect_frame_check(E.APPS_WEB_FILE_MOVE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_SELECT_PATH_DIAG)
        # Spec change again in FW 2.10.140..no need to select volume in this version
        #self.click_wait_and_check('link={}'.format(volume), 'link={}'.format(share2)) # Need to select vol first since FW 2.10.123                
        self.click_coordinates_and_check_attribute('link={}'.format(share2), -92, 0, E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, 'class', 'ButtonRightPos2 OK')
        self.click_wait_and_check(E.APPS_WEB_FILE_SELECT_PATH_DIAG_OK, E.UPDATING_STRING, visible=False)
        # Now the file will still exist until we click refresh, a defect?
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_contains(renamed_file)
        # Check if file is not exist after refresh page
        self.click_wait_and_check(E.APPS_WEB_FILE_REFRESH, E.UPDATING_STRING, visible=False)
        self.current_frame_should_not_contain(renamed_file)
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        # Check if file is moved to share folder2        
        self.double_click_folder(share2_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share2))
        self.current_frame_contains(renamed_file)
        self.log.info('File is moved from {0} to {1}'.format(share1, share2))
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')

        # Test.6 : Verify create sub folder
        self.log.info('Creating sub folder in {}'.format(share1))
        self.double_click_folder(share1_locator, E.APPS_WEB_FILE_PATH, 'Shares > {}'.format(share1))
        self.click_unselect_frame_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG)
        self.input_text_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_NAME_TEXT, sub_share)
        self.click_wait_and_check(E.APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_SAVE, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_contains(sub_share)
        self.log.info('Sub folder is created in {}'.format(share1))
        
        # New folder will be in the first row, try to delete it
        self.log.info('Deleting sub folder in {}'.format(share1))
        self.click_and_check_attribute('css=#row1 > td > div > span.table_folder > span', 'row1', 'class', 'trSelected')
        self.click_unselect_frame_check(E.APPS_WEB_FILE_DELETE, E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_DELETE_DIAG)
        self.click_wait_and_check(E.APPS_WEB_FILE_DELETE_DIAG_OK, E.UPDATING_STRING, visible=False)
        self.select_frame_check(E.APPS_WEB_FILE_FRAME, E.APPS_WEB_FILE_PATH)
        self.current_frame_should_not_contain(sub_share)
        self.log.info('Sub folder is deleted in {}'.format(share1))
        # Go back to the first page
        self.click_and_check_path('link=Shares', E.APPS_WEB_FILE_PATH, 'Shares')
        
        # Todo: Test.7 : Verify upload file
        '''
            Upload file is using abode flash which cannot be handled by selenium
            Skip this test case until we find solutions.
        '''

    def find_share_locator(self, share_name):
        share_info = self.get_all_shares()
        if share_info[0] != 0:
            raise Exception('Get shares info from Rest API failed! Return value: {}'.format(usb_info[0]))

        share_name_list = self.get_xml_tags(share_info[1], 'share_name')
        self.log.debug('Current share list: {}'.format(share_name_list))
        if share_name_list:
            for i, share in enumerate(share_name_list):
                locator_temp = 'css=#row{} > td > div > span.table_folder > span'.format(i+1)
                share_name_temp = self.get_text(locator_temp)
                if share_name_temp == share_name:
                    return locator_temp

            raise Exception('Non of the share folder name matches "{}"'.format(share_name))
        else:
            raise Exception('There are no share folders exist!')

    def find_file_locator(self, file_name, parent_share):
        file_list = self.execute('ls /shares/{}/'.format(parent_share))
        if file_list[0] != 0:
            raise Exception('Unable to list files from share folder:{}'.format(parent_share))
        
        self.log.debug('Current file list: {}'.format(file_list))
        if file_list[1]:
            files = file_list[1].split()
            for row_index in xrange(1, len(files)+1):
                locator_temp = 'css=#row{} > td > div > span.table_file > span'.format(row_index)
                file_name_temp = self.get_text(locator_temp)
                if file_name_temp == file_name:
                    row = 'row' + str(row_index)
                    return row, locator_temp
            raise Exception('Non of the file name matches "{}"'.format(file_name))
        else:
            raise Exception('There are no files existr in share folder:{}!'.format(parent_share))

    def double_click_folder(self, locator, check_locator, expected_path, retry=3):
        while retry >= 0:
            self.log.debug('Double clicking share folder:"{0}" and check the text of element:"{1}" is "{2}"'.
                           format(locator, check_locator.name, expected_path))
            try:
                self.double_click_element(locator)
                current_path = self.get_text(check_locator.locator)
                if current_path != expected_path:
                    raise Exception('Current path is:{0} and not match expected_path:{1}!'.format(current_path, expected_path))
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not double click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def click_and_check_attribute(self, locator, check_locator, attribute, expected_value, retry=3):
        while retry >= 0:
            self.log.debug('Clicking element:"{0}"\nand check the "{1}" attribute of locator "{2}" is "{3}"'.
                           format(locator, attribute, check_locator, expected_value))
            try:
                self.click_element(locator)
                self.check_attribute(check_locator, expected_value, attribute)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def select_frame_check(self, frame, check_locator, retry=3):
        while retry >= 0:
            self.log.debug('Select frame:{0} and check the element:"{1}" is visible'.
                           format(frame.name, check_locator.name))
            try:
                self.select_frame(frame)
                self.wait_until_element_is_visible(check_locator)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Select frame:{0} failed, remaining {1} retries...'.format(frame.name, retry))
                self.unselect_frame()
                retry -= 1
                continue
            else:
                break

    def click_unselect_frame_check(self, locator, frame, check_locator, retry=3):
        while retry >= 0:
            self.log.debug('Clicking element:"{0}", unselect frame and check the element:"{1}" is visible'.
                           format(locator.name, check_locator.name))
            try:
                self.wait_and_click(locator.locator)
                self.unselect_frame()
                self.wait_until_element_is_visible(check_locator.locator)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator.name, retry))
                self.unselect_frame()
                self.select_frame(frame)
                retry -= 1
                continue
            else:
                break

    def click_coordinates_and_check_attribute(self, locator, xoffset, yoffset, check_locator, attribute, expected_value, retry=3):
        while retry >= 0:
            self.log.debug('Clicking at x:{0}, y:{1} coordinates of element:"{2}" and check the "{3}" attribute of locator "{4}" is "{5}"'.
                           format(xoffset, yoffset, locator, attribute, check_locator.name, expected_value))
            try:
                self.click_element_at_coordinates(locator, xoffset, yoffset)
                self.check_attribute(check_locator.locator, expected_value, attribute)
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break

    def click_and_check_path(self, locator, check_locator, expected_path, retry=3):
        while retry >= 0:
            self.log.debug('Clicking element "{0}", check "{1}" is "{2}"'.
                           format(locator, check_locator.name, expected_path))
            try:
                self.wait_and_click(locator)
                current_path = self.get_text(check_locator.locator)
                if current_path != expected_path:
                    raise Exception('Current path is:{0} and not match expected_path:{1}!'.format(current_path, expected_path))
            except Exception as e:
                if retry == 0:
                    raise Exception(repr(e))
                self.log.debug('Element "{0}" did not click, remaining {1} retries...'.format(locator, retry))
                retry -= 1
                continue
            else:
                break
        
    def send_cgi(self,cmd):
        device_ip = self.uut[self.Fields.internal_ip_address]
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(device_ip)
        user_data = {'cmd':'wd_login','username':'admin','pwd':''}
        cmd_url = 'http://{0}/cgi-bin/webfile_mgr.cgi?cmd={1}'.format(device_ip, cmd)
        
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))
        
        r = s.get(cmd_url)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))
        
        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')
    
    def delete_local_file(self, file_name):
        if os.path.exists(file_name):
            self.log.info('Removing local file:{}'.format(file_name))
            os.system('rm {}'.format(file_name))

    def screenshot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1
    
appsPageWebFileViewerDownloadsInfoIsDisplayed()