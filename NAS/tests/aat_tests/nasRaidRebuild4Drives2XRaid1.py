'''
Create on Mar 11, 2015

@Author: lo_va

Objective: To validate 2X RAID1 configuration
            rebuild scenarios for 4 bay Consumer NAS.
wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Rebuild
'''

import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
from global_libraries import CommonTestLibrary as ctl
from global_libraries import wd_exceptions
sharename = 'Rebuild2XR1V'
sharename1 = 'Rebuild2XR1V1'
sharename2 = 'Rebuild2XR1V2'
generated_file_size = 10240
timeout = 240

class nasRaidRebuild4Drives2XRAID1(TestClient):

    def run(self):
        
        # Check Drives account
        if self.get_drive_count() != 4:
            raise wd_exceptions.InvalidDeviceState('Device is not with 4 drives, cannot test 2X RAID1 rebuild Test case!!!')
        # Set Web Acces Timeout value to 30 mins
        self.set_web_access_timeout()

        try:
            # Create 2X RAID1 if UUT is not in 2X RAID1
            raid_mode = self.get_raid_mode()
            raid_status = self.check_storage_page()
            get_raid_status = self.get_RAID_drives_status()
            raid_drives_number = self.get_xml_tags(get_raid_status, 'raid_mode').count('1')
            if raid_mode == 'RAID1' and raid_drives_number == 4 and raid_status == 'Healthy' and self.verifyMountVolume(volumeid=[1,2]):
                self.log.info('Raid type already in 2X RAID1 and status is Healthy')
            else:
                self.log.info('Start to create 2X RAID1.')
                self.configure_raid(raidtype='RAID1', number_of_drives=4, force_rebuild=True)
                if not self.verifyMountVolume(volumeid=[1,2]):
                    raise Exception('Verified mount failed')
            self.delete_all_shares()
            self.disable_auto_rebuild()

            # Create share and validate access via smb protocol
            self.log.info('Start to create shares and validate access via smb protocol.')
            self.createR1Share(volumeid=[1,2])
            self.read_write_access_check(sharename=sharename1, prot='SAMBA')
            self.read_write_access_check(sharename=sharename2, prot='SAMBA')
        except Exception, ex:
            self.log.exception('Configure 2X RAID1 was not successful. Please check RAID settings')
        
        # Clean up and Plug out the disk 1
        self.log.info('Plug out Disk 1 !!')
        self.clean_up_drive(1)
        self.remove_drives(1)

        # Check First RAID1 status was in degraded and validate access
        self.log.info('Check First RAID1 status was in degraded and validate access.')
        raid_status1 = self.raid_status(1)
        if 'degraded' in raid_status1:
            try:
                self.log.info('Raid status: Degraded.')
                self.check_home_page('degraded')
                self.read_write_access_check(sharename=sharename1, prot='SAMBA')
            except Exception, ex:
                self.log.exception('Failed to access First Degraded_RAID1\n' + str(ex))
        else:
            self.log.error('First Raid1 did not into degraded mode!\n')

        # Clean up and Plug out the disk 3
        self.log.info('Plug out Disk 3 !!')
        self.clean_up_drive(3)
        self.remove_drives(3)

        # Check Second RAID1 status was in degraded and validate access
        self.log.info('Check Second RAID1 status was in degraded and validate access.')
        raid_status1 = self.raid_status(2)
        if 'degraded' in raid_status1:
            try:
                self.log.info('Raid status: Degraded.')
                self.check_home_page('degraded')
                self.read_write_access_check(sharename=sharename2, prot='SAMBA')
            except Exception, ex:
                self.log.exception('Failed to access Second Degraded_RAID1\n' + str(ex))
        else:
            self.log.error('Second Raid1 did not into degraded mode!\n')      

        # Plug in Disk 1 and Disk 3
        self.log.info('Plug in Disk 1 !')
        self.insert_drives(1)
        self.log.info('Plug in Disk 3 !')
        self.insert_drives(3)
        self.manual_start_rebuild() # Manual start rebuilding
        
        '''
        # Time to wait rebuilding function starting. Optional
        self.wait_rebuild_start(volumeid=1)
        self.wait_rebuild_start(volumeid=2)
        '''
        # Validate RAID access with Disks rebuilding
        self.log.info('Start to check rebuilding RAID access.')
        raid_status1 = self.raid_status(1)
        raid_status2 = self.raid_status(2)
        if 'recovering' in raid_status1 and 'recovering' in raid_status2:
            try:
                self.log.info('Raid status: Rebuilding.')
                self.read_write_access_check(sharename=sharename1, prot='SAMBA')
                self.read_write_access_check(sharename=sharename2, prot='SAMBA')
            except Exception, ex:
                self.log.exception('Failed to access Rebuilding_RAID10\n' + str(ex))
        else:
            self.log.warning('RAID status is not in recovering')

        # Wait until rebuild is finished and validate RAID access.
        self.wait_rebuild_finished(volumeid=1, web_check=False)
        self.wait_rebuild_finished(volumeid=2)
        self.log.info('Check RAID1 status.')
        raid_status = self.check_storage_page()
        if raid_status == 'Healthy':
            try:
                self.log.info('Raid status is healthy.')
                self.check_home_page('healthy')
                self.read_write_access_check(sharename=sharename1, prot='SAMBA')
                self.read_write_access_check(sharename=sharename2, prot='SAMBA')
                self.log.info('****** NAS Rebuild for RAID1 test PASS!! GOOD JOB!! ******')
            except Exception, ex:
                self.log.exception('Failed to access Healthy_RAID1\n' + str(ex))
                self.log.info('****** NAS Rebuild for RAID1 test FAILED!! ******\n')
        else:
            self.log.info('Raid status = %s' % raid_status)
            self.log.critical('****** NAS Rebuild for RAID1 test FAILED!! ******\n')
            
    def tc_cleanup(self):
        self.checked_disk_exist()
        raid_status1 = self.raid_status(1)
        raid_status2 = self.raid_status(2)
        if 'degraded' in raid_status1 or 'degraded' in raid_status2:
            self.configure_raid(raidtype='RAID0', volume_size=100)

    def checked_disk_exist(self):
        drive_mapping = self.get_drive_mappings()
        for x in range(1, self.uut[self.Fields.number_of_drives]+1):
            if x not in drive_mapping:
                self.log.info('Drive {} is disconnected, try to insert it!'.format(x))
                self.insert_drives(x)
        if self.get_drive_count() == self.uut[self.Fields.number_of_drives]:
            self.log.info('Clean_up for insert drives succeed!')
        else:
            self.log.warning('Clean_up for insert drives failed!')
            
    def clean_up_drive(self, drive_number):
        drive_mapping = self.get_drive_mappings()
        clean_up_cmd = 'echo -e "o\ny\nw\ny\n" | gdisk /dev/{}'.format(drive_mapping.get(drive_number))
        self.run_on_device(clean_up_cmd)

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED!!')
        else:
            self.log.error('Hash test FAILED!!')

    def read_write_access_check(self, sharename, prot):

        if prot == 'SAMBA':
            generated_file = self.generate_test_file(generated_file_size)
            time.sleep(10)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            # Wait for transfer finished
            time.sleep(10)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName='file0.jpg')
            self.read_files_from_smb(files='file0.jpg', share_name=sharename, delete_after_copy=True)
            time.sleep(10)
            
        elif prot == 'NFS':
            nfs_mount = self.mount_share(share_name=sharename, protocol=self.Share.nfs)
            if nfs_mount:
                created_files = self.create_file(filesize=generated_file_size, dest_share_name=sharename)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=sharename):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
        else:
            self.log.info('Protocol {} is not supported'.format(protocol))

    def raid_status(self, raidnum):
        check_raid_status = 'mdadm --detail /dev/md%s | grep "State :"' % raidnum
        raid_status = self.run_on_device(check_raid_status)
        time.sleep(1) # Time to wait status return value
        while not raid_status:
            raid_status = self.run_on_device(check_raid_status)
            time.sleep(1)
        self.log.info('Raid_status = %s' % raid_status)
        raid_status1 = str(raid_status.split(': ')[1])

        return raid_status1

    def wait_rebuild_start(self, volumeid):
        # Wait for recovering started
        self.log.info('Waiting up to 30 seconds for the RAID status become rebuild')
        timer = Stopwatch(timer=30)
        timer.start()

        while True:
            raid_status1 = self.raid_status(volumeid)
            if 'recovering' in raid_status1:
                self.log.info('RAID status took {} seconds became rebuild'.format(timer.get_elapsed_time()))
                break
            else:
                if timer.is_timer_reached():
                    self.log.warning('RAID status never became rebuilding')
                    break
                else:
                    time.sleep(0.5)

    def wait_rebuild_finished(self, volumeid, web_check=True):
        # Wait for recovering finished
        self.log.info('Waiting up to 120 minutes for the RAID rebuilding finished')
        timer = Stopwatch(timer=7200)
        timer.start()

        while True:
            raid_status1 = self.raid_status(volumeid)
            if 'recovering' not in raid_status1 and 'degraded' not in raid_status1:
                self.log.info('RAID took {} minutes for rebuilding'.format(timer.get_elapsed_time()/60))
                break
            if 'recovering' in raid_status1:
                if timer.is_timer_reached():
                    self.log.warning('Rebuilding out of time')
                    break
                else:
                    time.sleep(10)
            else:
                self.log.warning('Raid not in recovering mode')
                break
            
        if web_check:
            timer = Stopwatch(timer=60)
            timer.start()
            while True:
                raid_status = self.check_storage_page()
                self.log.info('Raid Drives Status = {}'.format(raid_status))
                if raid_status in 'Healthy':
                    self.log.info('Rebuild finished.')
                    break
                else:
                    if timer.is_timer_reached():
                        self.log.error('Page Rebuilding get STUCK!')
                        break
                    else:
                        time.sleep(1)

    def enable_auto_rebuild(self):
        try:
            self.get_to_page('Storage')
            if self.is_element_visible('storage_raidFormatFinish9_button'):
                self.log.info('Close Storage completed page.')
                self.click_element('storage_raidFormatFinish9_button')
            check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
            time.sleep(2)
            check_status_attr = check_status.get_attribute('style')
            if 'block' in check_status_attr:
                self.log.info('Enable Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .toggle_off')
                check_status_attr = check_status.get_attribute('style')
                while 'block' in check_status_attr:
                    self.click_element('css = #storage_raidAutoRebuild_switch+span .toggle_off')
                    check_status_attr = check_status.get_attribute('style')
                    time.sleep(2)
                time.sleep(2)
            if 'none' in check_status_attr:
                self.log.info('Auto-Rebuild already enabled')
            self.close_webUI()
        except Exception, ex:
                self.log.exception('Failed to enable auto-rebuild\n' + str(ex))

    def disable_auto_rebuild(self):
        try:
            self.get_to_page('Storage')
            check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
            time.sleep(2)
            check_status_attr = check_status.get_attribute('style')
            if 'none' in check_status_attr:
                self.log.info('Disable Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                check_status_attr = check_status.get_attribute('style')
                while 'none' in check_status_attr:
                    self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                    check_status_attr = check_status.get_attribute('style')
                    time.sleep(2)
                time.sleep(2)
            if 'block' in check_status_attr:
                self.log.info('Auto-Rebuild already has been disabled')
            self.close_webUI()
        except Exception, ex:
                self.log.exception('Failed to disable auto-rebuild\n' + str(ex))

    def manual_start_rebuild(self):
        try:
            self.log.info('Manual start raid rebuild.')
            self.get_to_page('Storage')
            self.click_wait_and_check('storage_raidManuallyRebuild_button', 'storage_raidManuallyRebuildNext1_button')
            self.click_wait_and_check('storage_raidManuallyRebuildNext1_button', visible=False)
            self.wait_until_element_is_clickable('storage_raidManuallyRebuildNext2_button', timeout)
            self.click_element('storage_raidManuallyRebuildNext2_button')
            self.wait_until_element_is_not_visible(E.UPDATING_STRING, timeout)
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Failed to manual start rebuilding!' + str(ex))

    def check_storage_page(self):
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        while not raid_status:
            raid_status = self.get_text('raid_healthy')
            time.sleep(1)
        self.close_webUI()
        return raid_status
    
    def check_home_page(self, raidstate):
        try:
            self.log.info('Start to check raid status in home page')
            self.get_to_page('Home')
            if raidstate == 'degraded':
                dignostics_state = self.get_text('diagnostics_state')
                if 'Caution' in dignostics_state:
                    self.click_element('smart_info')
                    self.drag_and_drop_by_offset('css = div.jspDrag', 0, 80)
                    diagnosticsRaidStatus = self.get_text('home_diagnosticsRaidStatus_value')
                    if 'Degraded' in diagnosticsRaidStatus:
                        self.log.info('Raid status is Degraded!')
                else:
                    self.log.error('Raid status is not in Degraded')

            if raidstate == 'healthy':
                dignostics_state = self.get_text('diagnostics_state')
                if dignostics_state == 'Healthy':
                    self.log.info('Raid status is Healthy!')
                else:
                    self.log.error('Raid status is not in Healthy')

            if self.is_element_visible('home_diagnosticsClose1_button', 10):
                self.click_element('home_diagnosticsClose1_button')
            
            # Capacity verification
            if self.is_element_visible(E.HOME_DEVICE_CAPACITY_FREE_SPACE, 10):
                raid_capacity = self.get_text(E.HOME_DEVICE_CAPACITY_FREE_SPACE)
            elif self.is_element_visible('home_volcapity_info', 10):
                raid_capacity = self.get_text('home_volcapity_info')
            
            if raid_capacity:
                self.log.info('Raid capacity is %s' % (raid_capacity))
            if not raid_capacity:
                self.log.error('Checked raid capacity failed in home page!')
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Checked raid status failed in homp page!')

    def createR1Share(self, volumeid):
        try:
            self.log.info('Start to create Shares')
            self.get_to_page('Shares')

            for x in volumeid:
                time.sleep(3)
                self.click_element('shares_createShare_button')
                self.click_element('shares_volume_select')
                self.click_element('link=Volume_%s' % x)
                self.input_text('shares_shareName_text', sharename+str(x), False)
                self.input_text('shares_shareDesc_text', 'testvolume%s' % x, False)
                self.click_element('shares_createSave_button')
                self.wait_until_element_is_not_visible(E.UPDATING_STRING)
                time.sleep(1)
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Create Shares failed!')

    def verifyMountVolume(self, volumeid):
        vol = ''
        self.get_to_page('Storage')
        if self.is_element_visible('css = div.flexigrid', 2):
            vol = self.get_text('css = div.flexigrid')
        for x in volumeid:    
            if 'Volume_%s' % x in vol:
                self.log.info('Volume_%s Mount Successed!' % x)
                bool = True
            else:
                self.log.warning('Volume_%s not Mount!' % x)
                bool = False
        self.close_webUI()
        
        return bool

nasRaidRebuild4Drives2XRAID1()