"""
title           : userSharePermissions.py
description     : 1. Private Share R/W access test
                  2. Private Share negative test
                  3. Private share Read Only access test
                  4. Private share delete access test
author          :yang_ni
date            :2015/08/04
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
from smb.smb_structs import OperationFailure

class UserSharePermissions(TestClient):

    def run(self):

        try:
            self.tc_cleanup()
            time.sleep(8)

            # Create a user
            self.log.info('Creating a user ...')
            self.create_user(username='user1')

            # Create a share.
            self.log.info('Creating a share ...')
            self.create_shares(share_name='share', number_of_shares=2)
            share_list = self.get_xml_tags(self.get_all_shares()[1], 'share_name')
            self.log.info('share list: {0}'.format(share_list))

            # Change share to private, share must be set to private instead of public. Toggle the Public switch off
            self.log.info('Toggling the share from private to public ...')
            self.update_share_network_access(share_name='share1', public_access=False)
            time.sleep(8)
            self.update_share_network_access(share_name='share2', public_access=False)
            time.sleep(6)

            # Assigns user1 to share with the r/w access level
            self.log.info('Assigning user1 with Read Write access to the share ...')
            self.assign_user_to_share(user_name='user1', share_name='share1', access_type='rw')

            # 1. Private Share R/W access test : Test if user1 has r/w privilege to share1
            self.log.info('1. Private Share R/W access test')
            self.smbReadWriteTest(user_name='user1',share_name='share1',access_type='rw')
            time.sleep(8)
            self.log.info(' PASS : 1. Private Share R/W access test')

            # 2. Private Share negative test : Test if user1 has no r/w privilege to share2
            self.log.info('2. Private Share negative test')
            self.smbReadWriteTest(user_name='user1',share_name='share2',access_type='d')
            time.sleep(8)
            self.log.info(' PASS : 2. Private Share negative test')

            # 3. Private share Read Only access test : Test if user1 has r/o privilege to share1
            self.update_share_access(share_name='share1',username='user1',access='RO')
            time.sleep(6)
            self.log.info('3. Private share Read Only access test')
            self.smbReadWriteTest(user_name='user1',share_name='share1',access_type='ro')
            time.sleep(7)
            self.log.info(' PASS : 3. Private share Read Only access test')

            # 4. Private share delete access test : Test if user1 has no r/o privilege to share1
            self.delete_share_access(share_name='share1',username='user1')
            time.sleep(6)
            self.log.info('4. Private share delete access test')
            self.smbReadWriteTest(user_name='user1',share_name='share1',access_type='d')
            time.sleep(8)
            self.log.info(' PASS : 4. Private share delete access test')

        except Exception as e:
            self.log.error('Exception: {0}'.format(repr(e)))
        else:
            self.log.info('*** User Share permission test PASS ***')

    # SMB read/write access check
    def smbReadWriteTest(self, user_name, share_name, access_type):
        self.log.info('*** Check if {0} has {1} access to the {2} ***'.format(user_name,access_type,share_name))

        test_file = self.generate_test_file(kilobytes=10)
        time.sleep(7)

        # Check user1 write access to the share folder.
        try:
            self.write_files_to_smb(files=test_file, username=user_name, delete_after_copy=True, share_name=share_name)
        except OperationFailure:
            if access_type == 'rw':
                raise Exception('FAIL : Fail to write file to share folder')
            else:
                self.log.info('CORRECT : Fail to write file to share folder')
        else:
            if access_type == 'rw':
                self.log.info('CORRECT : Success to write file to share folder')
            else:
                raise Exception('FAIL : Success to write file to share folder')

        # Check user1 read access to the share folder.
        try:
            self.read_files_from_smb(files=test_file, username=user_name, delete_after_copy=True, share_name=share_name)
        except OperationFailure:
            if access_type == 'd':
                self.log.info('CORRECT : Failed to read file from share folder')
            else:
                raise Exception('FAIL : Failed to read file from share folder')
        else:
            if access_type == 'd':
                raise Exception('FAIL : Success to read file from share folder')
            else:
                self.log.info('CORRECT : Success to read file from share folder')

    # # @Clean up all the users/shares/groups after test finishes
    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users(use_rest=False)
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()

UserSharePermissions()