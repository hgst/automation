""" Users page- a user can be deleted

    @Author: Lee_e
    
    Objective: Users can be deleted from UI
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 1.05.30)
                    Lightning: Pass (FW: 2.00.40)
                    KC       : Pass (FW: 2.00.145)
              Jenkins Taiwan : Lightning -pass (FW: 2.00.174)
                             : Yosemite - pass (FW: 2.00.174)
                             : Kings Canyon - pass (FW:2.00.174)
                             : Sprite - pass (FW:2.00.174)
                             : Aurora - pass (FW: 2.00.171)
              Jenkins Irvine : Zion -pass (FW: 2.00.174)
                             : Glacier - pass (FW:2.00.174)
                             : Yellowstone -pass (FW:2.00.175)
""" 


import time
from testCaseAPI.src.testclient import TestClient

user = 'user1'

class AUserCanBeDeleted(TestClient):
    
    def run(self):
        self.create_user(username=user, password=None, fullname=None, is_admin=False, group_names='cloudholders')
        result = self.run_on_device('account -i user').split('\n')
        self.log.info('user list: {0}'.format(result))
        time.sleep(10)
        self.delete_user_from_UI(username=user)
        self.check_users(username=user)
        
    def tc_cleanup(self):
        self.execute('account -d -u {0}'.format(user))
        
    def check_users(self, username):
        result_code, result = self.get_all_users()
        user_list = self.get_xml_tags(result, 'user_id')
        self.log.info('user list: {0}'.format(user_list))
        if username in user_list:
            self.log.error('{0} should not existed'.format(username))
        else:
            self.log.info('{0} deleted successfully'.format(username))
    
    def delete_user_from_UI(self, username):
        self.get_to_page('Users')
        time.sleep(2)
        try:
            self.click_wait_and_check('users_user_'+str(username), self.Elements.USERS_REMOVE_USER)
            self.click_wait_and_check(self.Elements.USERS_REMOVE_USER, self.Elements.USERS_REMOVE_USER_OK)
            self.click_wait_and_check(self.Elements.USERS_REMOVE_USER_OK, self.Elements.UPDATING_STRING, visible=False)
        except Exception as e:
            self.log.error('Failed with exception: {0}'.format(repr(e)))
            self.take_screen_shot()
        time.sleep(3)

AUserCanBeDeleted()