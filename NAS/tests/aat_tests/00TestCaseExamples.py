""" Test Case Template

This template is used for new test cases. It inherits from TestClient so it does not need to worry about reporting
the test results, configuring the logger, etc.

"""

# Import the test case
from testCaseAPI.src.testclient import TestClient

class MyTest(TestClient):
    # The test case class. "MyTest" can be any name you want, but TestClient must be inherited

    def run(self):
        # This method starts all tests you want to run. It is automatically called by the TestClient constructor
        self.test_info()
        self.test_warning('This is a warning')
        self.test_error()

        try:
            # This test may throw an exception, but we don't want to abort the entire test. We do, however, want to
            # count it as an error.
            self.test_exception()
        except:
            self.log.exception('An exception was encountered. And this part of the test failed.')

        try:
            # This particular test can sometimes throw an exception, but that will not be considered an error
            self.test_exception()
        except:
            self.warn_exception('An exception has occurred, but it is expected.')

        # This time, if the exception is thrown, we don't want to continue since there's no point in continuing if this
        # fails.
        self.test_exception()

        self.test_never_run()

    def test_info(self):
        # This is a test that just logs an info message. An Info message will go to the screen and the Silk log,
        # but not be counted as a failure.
        self.log.info('Just an info message')

    def test_warning(self, message):
        # This test logs a warning message and takes a parameter.
        # It will go to the screen and the Silk log and counted as a warning
        self.log.warning(message)

    def test_error(self):
        # This test logs an error message.
        # It will go to the screen and the Silk log and counted as an error
        self.log.error('An error has occured')

    def test_exception(self):
        # This test raises an exception. It's up to the run function to decide whether to ignore the exception
        # Log it as an error, or raise it
        self.log.info('Antoher info message from test_exception')
        raise Exception('Something bad has happened. Mooooooo!')

    def test_never_run(self):
        # This test shouldn't run because the exception was raised
        self.log.critical('If you see this, the test is broken.')


# This constructs the MyTest() class, which in turns constructs the TestClient class and kicks off the MyTest.run()
# method.
MyTest()


