'''
Created on Feb 16th, 2015

@author: tsui_b

Objective: Verify all links on all pages lead to a page without errors. 
Wiki URL: http://wiki.wdc.com/wiki/Web_UI_quick_check
'''

import time
import os

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

timeout = 30
no_storage_page_list = ['GLCR', 'BAGX']
not_support_group_list = ['GLCR']
not_support_iscsi_list = ['GLCR', 'BZVM', 'BWVZ', 'BAGX']
not_support_volumevirtualization_list = ['GLCR', 'BZVM', 'BWVZ', 'BAGX']
not_support_camera_backup_list = ['GLCR']
not_support_cloud_backup_list = ['GLCR', 'BAGX']
not_support_FTP_download_list = ['GLCR']
not_support_P2P_download_list = ['GLCR']
not_support_web_file_viewer_list = ['GLCR', 'BAGX']
not_support_shutdown = ['GLCR']
cloud_access_default_on = ['LT4A', 'KC2A']

model_number = ''

class webUIQuickCheck(TestClient):
    
    def run(self):
        global model_number
        model_number = self.get_model_number()
        page_list = [self.toolbar, self.home, self.users, self.shares, self.cloud_access,
                     self.backups, self.storage, self.apps, self.settings]
        for page in page_list:
            # There is no storage page in Glacier
            if page.__name__ == 'storage' and model_number in no_storage_page_list:
                continue

            try:
                self.log.info('Verifying page: {}'.format(page.__name__))
                page()
                self.log.info('Verified page: {}'.format(page.__name__))
            except Exception as e:
                self.log.error('Verify page: {0} failed! Exception: {1}'.format(page.__name__, repr(e)))
                self.close_webUI()

    def toolbar(self):
        self.get_to_page('Home')
        # Verify links are valid in Help tool bar buttons
        # Device Button
        # Check if there are any USB devices now
        usb_info = self.get_usb_info()
        if usb_info[0] != 0:
            raise Exception('Get USB info from Rest API failed! Return value: {}'.format(usb_info[0]))

        usb_device_name = self.get_xml_tags(usb_info[1], 'name')
        self.log.debug('Current USB list: {}'.format(usb_device_name))
        if usb_device_name:
            self.log.debug('Check if USB devices exist.')
            self.click_wait_and_check(E.TOOLBAR_DEVICE, E.TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_ARROW, timeout=timeout)
        else:
            self.log.debug('There are no USB devices, check if empty diag exist.')
            self.click_wait_and_check(E.TOOLBAR_DEVICE, E.TOOLBAR_DEVICE_EMPTY_DIAG, timeout=timeout)

        # Nofitication Button
        firmware_version = self.get_firmware_version_number()
        if firmware_version.startswith('2.10') or firmware_version.startswith('2.11'):
            # Old element id in 2.10.xxx and 2.11.xxx
            notification_button = 'css=#id_alert > img'            
        else:
            # The latest id since 2.20.xxx
            notification_button = E.TOOLBAR_NOTIFICATION

        # Check if there are any notification now
        alerts_info = self.get_alerts_cgi()
        alerts_msg = self.get_xml_tags(alerts_info.text, 'msg')
        self.log.debug('Current alerts list: {}'.format(alerts_msg))
        if alerts_msg:
            # Check non-empty notification diag if there are any alert exists
            self.log.debug('Check if notification diag exist.')
            self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_NONEMPTY_DIAG, timeout=timeout)
        else:
            self.log.debug('There are no notifications, check if empty diag exist.')
            self.click_wait_and_check(notification_button, E.TOOLBAR_NOTIFICATION_EMPTY_DIAG, timeout=timeout)

        # Help Button
        # There are 4 links in help wizard: Getting Started / Help / Support / About
        # Help Button -> Getting Started Wizard
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD, timeout=timeout)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED, E.TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG, timeout=timeout)
        # Step1 -> Next to Step2
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT1, E.TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG, timeout=timeout)
        # Step2 -> Next to Step3
        # Get cloud access info
        cmd = 'cat /var/www/xml/account.xml'
        result = self.execute(cmd)
        if result[0] != 0:
            self.log.error('Get cloud access info failed! Error message:{}'.format(result[1]))
        else:
            cloud_first_name = self.get_xml_tag(result[1], 'first_name')
            cloud_last_name = self.get_xml_tag(result[1], 'last_name')
            cloud_email = self.get_xml_tag(result[1], 'email')
        # Need to clear default account info if exist
        if cloud_first_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_DELETE)
        if cloud_last_name:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_DELETE)
        if cloud_email:
            self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_DELETE)
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_NEXT2, E.TOOLBAR_HELP_GETTING_STARTED_STEP3_DIAG, timeout=timeout)
        # Click finish button to close diag
        self.click_wait_and_check(E.TOOLBAR_HELP_GETTING_STARTED_FINISH, timeout=timeout)
        # Help Button -> Help Link
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD, timeout=timeout)
        self.click_wait_and_check(E.TOOLBAR_HELP_HELP, E.TOOLBAR_HELP_HELP_DIAG, timeout=timeout)
        # Verify the link on search button in the iframe
        self.click_help_tab(E.TOOLBAR_HELP_HELP_SEARCH, E.TOOLBAR_HELP_HELP_LEFT_SEARCH)
        # Verify the link on content button in the iframe
        self.click_help_tab(E.TOOLBAR_HELP_HELP_CONTENT, E.TOOLBAR_HELP_HELP_LEFT_CONTENT)
        # Close the wizard
        self.click_wait_and_check(E.TOOLBAR_HELP_HELP_DIAG_OK, E.TOOLBAR_HELP_HELP_DIAG, visible=False, timeout=timeout)

        # Help Button -> Support Link
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD, timeout=timeout)
        self.click_wait_and_check(E.TOOLBAR_HELP_SUPPORT, E.TOOLBAR_HELP_SUPPORT_DIAG, timeout=timeout)
        # Close the wizard
        self.click_wait_and_check(E.TOOLBAR_HELP_SUPPORT_DIAG_OK, E.TOOLBAR_HELP_SUPPORT_DIAG, visible=False, timeout=timeout)

        # Help Button -> About Link
        self.click_wait_and_check(E.TOOLBAR_HELP, E.TOOLBAR_HELP_WIZARD, timeout=timeout)
        self.click_wait_and_check(E.TOOLBAR_HELP_ABOUT, E.TOOLBAR_HELP_ABOUT_DIAG, timeout=timeout)
        # Close the wizard
        self.click_wait_and_check(E.TOOLBAR_HELP_ABOUT_DIAG_OK, E.TOOLBAR_HELP_ABOUT_DIAG, visible=False, timeout=timeout)

        # Logout Button
        self.click_wait_and_check(E.TOOLBAR_LOGOUT, E.TOOLBAR_LOGOUT_TABLE, timeout=timeout)
        # We do not test shutdown / reboot function in quick test.
        if model_number not in not_support_shutdown:
            self.wait_until_element_is_visible(E.TOOLBAR_LOGOUT_SHUTDOWN, timeout)
        self.wait_until_element_is_visible(E.TOOLBAR_LOGOUT_REBOOT, timeout)
        # Click logout will go back to login page
        self.click_wait_and_check(E.TOOLBAR_LOGOUT_LOGOUT, E.LOGIN_BUTTON, timeout=timeout)
        
    def home(self):
        self.get_to_page('Home')
        # Verify links in Device Column
        # Diagnostics
        self.click_wait_and_check(E.HOME_DEVICE_DIAGNOSTICS_ARROW, E.HOME_DEVICE_DIAGNOSTICS_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAGNOSTICS_DIAG_CLOSE, E.HOME_DEVICE_DIAGNOSTICS_DIAG, visible=False, timeout=timeout)
        # Firmware
        self.click_wait_and_check(E.HOME_DEVICE_FIRMWARE_ARROW, E.HOME_DEVICE_FIRMWARE_DIAG, timeout=60) # This takes long time, need to specify timeout
        self.click_wait_and_check(E.HOME_DEVICE_FIRMWARE_DIAG_OK, E.HOME_DEVICE_FIRMWARE_DIAG, visible=False, timeout=timeout)
        # Verify links in Network Activity
        # Click Network Picture
        self.click_wait_and_check(E.HOME_NETWORK_NETWORK_PIC, E.HOME_NETWORK_NETWORK_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_NETWORK_DIAG_CLOSE, E.HOME_NETWORK_NETWORK_DIAG, visible=False, timeout=timeout)
        # Click CPU Picture
        self.click_wait_and_check(E.HOME_NETWORK_CPU_PIC, E.HOME_NETWORK_CPU_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_CPU_DIAG_CLOSE, E.HOME_NETWORK_CPU_DIAG, visible=False, timeout=timeout)
        # Click RAM Picture
        self.click_wait_and_check(E.HOME_NETWORK_RAM_PIC, E.HOME_NETWORK_RAM_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_RAM_DIAG_CLOSE, E.HOME_NETWORK_RAM_DIAG, visible=False, timeout=timeout)

        # Click Device Activity Arrow
        self.click_wait_and_check(E.HOME_NETWORK_ARROW, E.HOME_DEVICE_DIAG, timeout=timeout)
        # There will be 4 arrows: CPU / Memory / Network / Process
        # Verify CPU arrow
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_CPU_ARROW, E.HOME_NETWORK_CPU_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_CPU_DIAG_BACK_TEXT, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_CPU_ARROW, E.HOME_NETWORK_CPU_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_CPU_DIAG_BACK, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_CPU_ARROW, E.HOME_NETWORK_CPU_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_CPU_DIAG_CLOSE, E.HOME_NETWORK_CPU_DIAG, visible=False, timeout=timeout)

        # Verify RAM arrow
        self.click_wait_and_check(E.HOME_NETWORK_ARROW, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_RAM_ARROW, E.HOME_NETWORK_RAM_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_RAM_DIAG_BACK_TEXT, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_RAM_ARROW, E.HOME_NETWORK_RAM_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_RAM_DIAG_BACK, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_RAM_ARROW, E.HOME_NETWORK_RAM_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_RAM_DIAG_CLOSE, E.HOME_NETWORK_RAM_DIAG, visible=False, timeout=timeout)

        # Verify Network arrow
        self.click_wait_and_check(E.HOME_NETWORK_ARROW, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_NETWORK_ARROW, E.HOME_NETWORK_NETWORK_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_NETWORK_DIAG_BACK_TEXT, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_NETWORK_ARROW, E.HOME_NETWORK_NETWORK_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_NETWORK_DIAG_BACK, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_NETWORK_ARROW, E.HOME_NETWORK_NETWORK_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_NETWORK_DIAG_CLOSE, E.HOME_NETWORK_NETWORK_DIAG, visible=False, timeout=timeout)
        
        # Verify Process arrow
        self.click_wait_and_check(E.HOME_NETWORK_ARROW, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_PROCESS_ARROW, E.HOME_NETWORK_PROCESS_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_PROCESS_DIAG_BACK_TEXT, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_PROCESS_ARROW, E.HOME_NETWORK_PROCESS_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_PROCESS_DIAG_BACK, E.HOME_DEVICE_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_DEVICE_DIAG_PROCESS_ARROW, E.HOME_NETWORK_PROCESS_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_NETWORK_PROCESS_DIAG_CLOSE, E.HOME_NETWORK_PROCESS_DIAG, visible=False, timeout=timeout)
        
        if model_number in cloud_access_default_on:
            # Verify Cloud Device arrow
            self.click_wait_and_check(E.HOME_CLOUD_ARROW, E.HOME_CLOUD_DIAG, timeout=timeout)
            self.click_wait_and_check(E.HOME_CLOUD_DIAG_CANCEL, E.HOME_CLOUD_DIAG, visible=False, timeout=timeout)
        
        # Verify User arrow
        self.click_wait_and_check(E.HOME_USER_ARROW, E.HOME_USER_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_USER_DIAG_CANCEL, E.HOME_USER_DIAG, visible=False, timeout=timeout)
        
        # Verify Apps arrow
        self.click_wait_and_check(E.HOME_APPS_ARROW, E.HOME_APPS_DIAG, timeout=timeout)
        self.click_wait_and_check(E.HOME_APPS_DIAG_CLOSE, E.HOME_APPS_DIAG, visible=False, timeout=timeout)
        
    def users(self):
        self.get_to_page('Users')
        # Verify User tab        
        if model_number not in not_support_group_list:
            self.click_wait_and_check(E.USERS_BUTTON, E.USERS_ADD_LINK, timeout=timeout)
            self.check_page_contains('About Users', E.USERS_BUTTON)
        else:
            self.current_frame_contains('About Users')
            
        if model_number not in not_support_group_list:
            # Verify Group tab
            self.click_wait_and_check(E.GROUPS_BUTTON, E.GROUPS_ADD_LINK)
            self.check_page_contains('About Groups', E.GROUPS_BUTTON)

    def shares(self):
        self.get_to_page('Shares')
        self.check_page_contains('About Shares', 'nav_shares_link')
    
    def cloud_access(self):
        self.get_to_page('Cloud Access')
        self.check_page_contains('About Cloud Access', 'nav_cloudaccess_link')
        # Verify Admin Tab
        self.click_wait_and_check(E.CLOUD_ADMIN_ICON, E.CLOUD_ADMIN_SIGN_UP, timeout=timeout)
        self.check_page_contains('Cloud Access Status', E.CLOUD_ADMIN_ICON)

    def backups(self):
        self.get_to_page('Backups')
        # Go to "USB Backups" column
        self.click_wait_and_check(E.BACKUPS_USB, E.BACKUPS_CREATE_USB_JOB, timeout=timeout)
        self.check_page_contains('About USB Backups', E.BACKUPS_USB)
        # Go to "Remote Backups" column
        self.click_wait_and_check(E.BACKUPS_REMOTE, E.BACKUPS_REMOTE_CREATE_JOB, timeout=timeout)
        self.check_page_contains('About Remote Backups', E.BACKUPS_REMOTE)
        # Go to "Internal Backups" column
        self.click_wait_and_check(E.BACKUPS_INTERNAL, E.BACKUPS_INTERNAL_CREATE_JOB, timeout=timeout)
        self.check_page_contains('About Internal Backups', E.BACKUPS_INTERNAL)

        if model_number not in not_support_cloud_backup_list:
            # Go to "Cloud Backups" column
            self.click_wait_and_check(E.BACKUPS_CLOUD, E.BACKUPS_CLOUD_GRAPHIC, timeout=timeout)
            self.check_page_contains('What are Cloud Backups?', E.BACKUPS_CLOUD)
            self.click_wait_and_check(E.BACKUPS_CLOUD_ELEPHANTDRIVE, E.BACKUPS_CLOUD_ELEPHANTDRIVE_TITLE, timeout=timeout)
            self.check_page_contains('ElephantDrive', E.BACKUPS_CLOUD_ELEPHANTDRIVE)
            self.click_wait_and_check(E.BACKUPS_CLOUD_AMAZON, E.BACKUPS_CLOUD_AMAZON_CREATE, timeout=timeout)

        if model_number not in not_support_camera_backup_list:
            # Go to "Camera Backups" column
            self.click_wait_and_check(E.BACKUPS_CAMERA, E.BACKUPS_CAMERA_TRANSFER, timeout=timeout)
            self.check_page_contains('About Camera Backups', E.BACKUPS_CAMERA)

    def storage(self):
        self.get_to_page('Storage')
        # Verify "Raid" column
        try:
            self.click_wait_and_check(E.STORAGE_RAID, E.STORAGE_CHANGE_RAID_MODE, timeout=timeout)
        except:
            self.click_wait_and_check(E.STORAGE_RAID, E.STORAGE_SETUP_RAID_MODE, timeout=timeout)
            self.log.warning('Raid might be in status: Damaged or Unknown')

        self.check_page_contains('RAID Profile', E.STORAGE_RAID)
        # Verify "Disk Status" column
        self.click_wait_and_check(E.STORAGE_DISK_STATUS, E.STORAGE_DISK_STATUS_LIST, timeout=timeout)
        self.check_page_contains('Disks Profile', E.STORAGE_DISK_STATUS)

        if model_number not in not_support_iscsi_list:
            # Verify "iSCSI" column
            self.click_wait_and_check(E.STORAGE_ISCSI, E.STORAGE_ISCSI_ENABLE, timeout=timeout)
            self.check_page_contains('iSCSI Targets', E.STORAGE_ISCSI)

        if model_number not in not_support_volumevirtualization_list:
            # Verify "Volume Virtualization" column
            self.click_wait_and_check(E.STORAGE_VOLUME_VIRTUALIZATION, E.STORAGE_VOLUME_VIRTUALIZATION_CREATE, timeout=timeout)
            self.check_page_contains('About Volume Virtualization', E.STORAGE_VOLUME_VIRTUALIZATION)

    def apps(self):
        self.get_to_page('Apps')
        # Test HTTP Downloads column
        self.click_wait_and_check(E.APPS_HTTP_DOWNLOAD, E.APPS_HTTP_DOWNLOAD_CREATE, timeout=timeout)
        self.check_page_contains('HTTP Downloads', E.APPS_HTTP_DOWNLOAD)
        
        if model_number not in not_support_FTP_download_list:
            # Test FTP Downloads column
            self.click_wait_and_check(E.APPS_FTP_DOWNLOAD, E.APPS_FTP_DOWNLOAD_CREATE, timeout=timeout)
            self.check_page_contains('FTP Downloads', E.APPS_FTP_DOWNLOAD)
        
        if model_number not in not_support_P2P_download_list:
            # Test P2P Downloads column
            self.click_wait_and_check(E.APPS_P2P_DOWNLOAD, E.APPS_P2P_DOWNLOAD_TITLE, timeout=timeout)
            self.check_page_contains('P2P Settings', E.APPS_P2P_DOWNLOAD)
        
        if model_number not in not_support_web_file_viewer_list:
            # Test Web File Viewer column
            self.click_wait_and_check(E.APPS_WEB_FILE, E.APPS_WEB_FILE_FRAME, timeout=timeout)
            self.check_page_contains('Web File Viewer', E.APPS_WEB_FILE)

    def settings(self):
        self.get_to_page('Settings')
        # Verify General Column
        self.click_wait_and_check(E.GENERAL_BUTTON, E.GENERAL_DEVICE_NAME, timeout=timeout)
        self.check_page_contains('Device Profile', E.GENERAL_BUTTON)
        # Verify Network Column
        self.click_wait_and_check(E.NETWORK_BUTTON, E.NETWORK_MAC_ADDRESS, timeout=timeout)
        self.check_page_contains('Network Profile', E.NETWORK_BUTTON)
        # Verify Media Column
        self.click_wait_and_check(E.SETTINGS_MEDIA, E.SETTINGS_MEDIA_STREAMING, timeout=timeout)
        self.check_page_contains('DLNA Media Server', E.SETTINGS_MEDIA)
        # Verify Utilities Column
        self.click_wait_and_check(E.SETTINGS_UTILITY, E.SETTINGS_UTILITY_QUICKTEST, timeout=timeout)
        self.check_page_contains('System Diagnostics', E.SETTINGS_UTILITY)
        # Verify Notifications Column
        self.click_wait_and_check(E.SETTINGS_NOTIFICATION, E.SETTINGS_NOTIFICATION_EMAIL_TITLE, timeout=timeout)
        self.check_page_contains('Notifications Email', E.SETTINGS_NOTIFICATION) 
        # Verify Firmware Update Column
        self.click_wait_and_check(E.SETTINGS_FIRMWARE, E.SETTINGS_FIRMWARE_CHECKUPDATE, timeout=timeout)
        self.check_page_contains('Auto Update', E.SETTINGS_FIRMWARE)
    
    def check_page_contains(self, text, retry_button, attempts=3, attempt_delay=2):
        for attempts_remaining in range(attempts, -1, -1):
            if attempts_remaining > 0:
                try:
                    self.current_frame_contains(text)
                    break
                except:
                    self.log.info('Did not find text: "{0}". Wait {1} seconds and retry. {2} attempts remaining.' \
                                  .format(text, attempt_delay, attempts_remaining))
                    self.click_element(retry_button)
                    time.sleep(attempt_delay)
            else:
                raise Exception('Page should contain text: "{}" but it is not.'.format(text))
            
    def click_help_tab(self, locator, check_locator, retry=3, timeout=timeout):
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if visible".format(locator.name, check_locator.name))
            try:
                self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
                self.select_frame(E.TOOLBAR_HELP_HELP_TOOLBAR_FRAME)
                self.wait_and_click(locator, timeout=timeout)
                self.unselect_frame()
                self.select_frame(E.TOOLBAR_HELP_HELP_MAIN_FRAME)
                self.select_frame(E.TOOLBAR_HELP_HELP_CONTENT_FRAME)
                self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L1)
                self.select_frame(E.TOOLBAR_HELP_HELP_LEFT_FRAME_L2)
                self.wait_until_element_is_visible(check_locator, timeout=timeout)
                self.unselect_frame()
            except Exception as e:
                self.unselect_frame()
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

webUIQuickCheck()