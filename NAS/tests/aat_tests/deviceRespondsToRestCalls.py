"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify REST calls work on test device

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

class RespondsToRestCalls(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            result = self.get_system_state()
            status = self.get_xml_tag(result, 'status')
            ssh_result = self.set_ssh()
            remote_result = self.enable_remote_access()

            if status and ssh_result[0] == 0 and remote_result == 0:
                self.log.info("Device responds to REST call passed, status : {}".format(status))
            else:
                self.log.error("Device responds to REST call failed")
        except Exception, ex:
            self.log.exception('Failed to complete device responds to REST calls test case \n' + str(ex))


RespondsToRestCalls()