'''
Create on April 20, 2015

@Author: lo_va

Objective: Verify format disk function from WebUI.
Test ID: 117537
'''
import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
sharename1 = 'Public'
sharename2 = 'formatdisktest'

class settingsUtilitiesFormatDisk(TestClient):
    
    def run(self):
        
        self.set_web_access_timeout('19')
        self.create_shares(share_name=sharename2, force_webui=True)
        self.create_random_file('/shares/%s/' % sharename1, filename='file0.jpg', blocksize=1024, count=10240)
        self.create_random_file('/shares/%s/' % sharename2, filename='file0.jpg', blocksize=1024, count=10240)
        self.run_on_device('mkdir /shares/%s/testshare1' % sharename1)
        self.run_on_device('touch /shares/%s/testfile1.txt' % sharename1)
        try:
            testname = 'Format Disk Test'
            self.log.info(testname)
            self.get_to_page('Settings')
            self.click_element(E.SETTINGS_UTILITY)
            model_number = self.get_model_number()
            if not 'GLCR' in model_number:
                self.click_element(E.SETTINGS_UTILITY_FORMATDISK_VOL_SELECT)
                self.click_element('link=Volume_1')
            self.click_element(E.SETTINGS_UTILITY_FORMATDISK)
            self.click_element('settings_utilitiesFormatDiskNext5_button')
            self.wait_until_element_is_visible('format_percent')
            while self.is_element_visible('format_percent') and self.is_element_visible('format_state'):
                try:
                    format_disk_percent = self.get_text('format_percent')
                    format_disk_state = self.get_text('format_state')
                    self.log.info('Format disk status = {} {}'.format(format_disk_state, format_disk_percent))
                    time.sleep(5)
                except:
                    self.log.warning('Sometimes format disk percent will disappear immediately. Skip get_text in this situation')
                    break
        except Exception, ex:
            self.log.exception('Failed to Format Disk Test!!')
        get_share_list = self.get_all_shares()
        share_list = self.get_xml_tags(get_share_list, tag='share_name')
        if not sharename2 in share_list:
            self.log.info('PASS!!! {} is not in the share list'.format(sharename2))
        else:
            self.log.error('FAILED! {} still in the share list'.format(sharename2))
        get_file1 = self.get_file(sharename1, file_name='file0.jpg')
        get_file2 = self.get_file(sharename1, file_name='testshare1')
        get_file3 = self.get_file(sharename1, file_name='testfile1.txt')
        if not (get_file1 and get_file2 and get_file3) == 0:
            self.log.info('PASS!!! Files is not in the {}'.format(sharename1))
        else:
            self.log.error('FAILED!!! Files still in the {}'.format(sharename1))
        
    def tc_cleanup(self):
        self.delete_all_shares()
        self.set_web_access_timeout('5')
            
settingsUtilitiesFormatDisk()