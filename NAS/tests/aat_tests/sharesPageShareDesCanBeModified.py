""" Shares page - share description can be modified (MA-156)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=21119&execView=execDetails&view=details&pltab=steps&pId=50&nTP=256733&etab=8


    Automation: Full

    Not supported product:
        N/A

    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon


    Test Status:
        Local Lightning: Pass (FW: 2.00.215)
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import os

picturecounts = 0

class sharesPageShareDesCanBeModified(TestClient):

    def run(self):
        self.log.info('######################## Shares page - share description can be modified (MA-156) TEST START ##############################')
        try:
            self.delete_all_shares()  # except 3 default shares
            self.log.info("*** Clean all shares ***")
            testdesc = 'PublicShare_description_test'
            self.change_share_desc(sharename='Public', sharedesc=testdesc)
            self.verify_share_desc(sharename='Public', sharedesc=testdesc)
        except Exception as e:
            self.log.error("FAILED: Shares page - share description can be modified (MA-156) Test Failed!!, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        else:
            self.log.info("PASS: Shares page - share description can be modified (MA-156) Test PASS!!!")
        finally:
            self.log.info("========== (MA-156) Clean Up ==========")
            self.change_share_desc(sharename='Public', sharedesc=' ')
            self.log.info('######################## Shares page - share description can be modified (MA-156) TEST END ##############################')

    def change_share_desc(self, sharename='Public', sharedesc='Test Share Description'):
        """
        :param sharename: Share name you're going to change the description
        :param sharedesc: The description you're going to set
        """
        self.get_to_page('Shares')
        count = 60
        while not self.is_element_visible('shares_share_{}'.format(sharename), wait_time=5):
            self.log.info("Waiting for {} share visible...".format(sharename))
            time.sleep(1)
            count -= 1
            if count == 0:
                self.log.error("ERROR: Unable to locate {} share for 1 min, going to take screenshot for debug".format(sharename))
                self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
                self.takeScreenShot(prefix='locateShare')
                raise Exception("[change_share_desc]: Can not locate {} share".format(sharename))
        count = 3
        while count > 0:
            try:
                self.click_wait_and_check('shares_share_{}'.format(sharename),
                                          'css=#shares_share_{}.uTooltip.LightningSubMenuOn'.format(sharename), visible=True)
                self.input_text_check('shares_editShareDesc_text', str(sharedesc), False)
                self.click_wait_and_check('shares_descSave_button', visible=False)  # Apply
                self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)  # Wait for updating
            except Exception as e:
                count -= 1
                self.log.info("WARN: Going to retry with remaining {} attempts, due to exception: {}".format(count, repr(e)))
                continue
            else:
                if sharedesc == ' ':
                    self.log.info("Succeed to clean the description on {} share".format(sharename))
                else:
                    self.log.info("PASS: Succeed to change the description of {} share to {}".format(sharename, sharedesc))
                break
        self.close_webUI()

    def verify_share_desc(self, sharename='Public', sharedesc='Test Share Description'):
        """
        :param sharename: Share name you're going to change the description
        :param sharedesc: The description you're going to set
        """
        self.get_to_page('Shares')
        count = 60
        while not self.is_element_visible('shares_share_{}'.format(sharename), wait_time=5):
            self.log.info("Waiting for {} share visible...".format(sharename))
            time.sleep(1)
            count -= 1
            if count == 0:
                self.log.error("ERROR: Can not locate {} share for 1 min, going to take screenshot for debug".format(sharename))
                self.takeScreenShot(prefix='verifyShare')
                raise Exception("[verify_share_desc]: Can not locate {} share".format(sharename))
        self.wait_and_click('shares_share_{}'.format(sharename), timeout=5)
        desc_text = self.get_value('shares_editShareDesc_text')
        self.log.info("GOT: {}".format(desc_text))
        if desc_text == sharedesc:
            self.log.info("PASS: {} share's description can be modified successfully!".format(sharename))
        else:
            raise Exception("FAILED: {} share's description should be {} but it shows up {}".format(sharename, sharedesc, desc_text))

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1


sharesPageShareDesCanBeModified()