"""
Create on May 6, 2016
@Author: lo_va
Objective:  Verify Connection Manager module is loaded in firmware
            Verify connectmgr daemon is running
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class LoadConnectionManager(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        connectmgr_daemon = ''
        connectmgr_files_list = ''
        try:
            connectmgr_daemon = self.execute('ps -ef | grep connman')[1]
            connectmgr_files_list = self.execute('find / -name *connman*')[1]
            self.log.debug('Connection Manager Daemon: {}'.format(connectmgr_daemon))
            self.log.debug('Connection Manager find files list: {}'.format(connectmgr_files_list))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['/usr/sbin/connmand']
        check_list2 = ['/etc/init.d/connman', '/etc/dbus-1/system.d/connman.conf', '/usr/sbin/connmand']
        if all(word in connectmgr_daemon for word in check_list1) \
                and all(word in connectmgr_files_list for word in check_list2):
            self.log.info('Load Connection Manager in firmware test PASSED!!')
        else:
            self.log.error('Load Connection Manager in firmware test FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            boot_finished = self.execute('ls /tmp')[1]
            fw_ver = self.execute('cat /etc/version')[1]
            if 'boot_finished' in boot_finished:
                self.log.info('Device is Ready')
                self.log.info('Firmware Version: {}'.format(fw_ver))
            else:
                raise Exception('boot_finished is not exist, device not ready!')
            return fw_ver
        except Exception as ex:
            self.log.exception('Device check failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

LoadConnectionManager(checkDevice=False)
