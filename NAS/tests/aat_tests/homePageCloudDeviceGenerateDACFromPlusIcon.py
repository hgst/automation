'''
Created on May 5th, 2015

@author: tsui_b

Objective: Verify user can generate DAC from plus icon at home page

Automation: Full
    
Supported Products: All
    
Test Status: 
            Local 
                    Lightning: Pass (FW: 2.00.215)
            Jenkins 
                    Lightning: Pass (FW: 2.00.215)
                    Kings Canyon: Pass (FW: 2.00.215)
                    Glacier: Pass (FW: 2.00.215)
                    Zion: Pass (FW: 2.00.215)
                    Yosemite: Pass (FW: 2.00.215)
                    Yellowstone: Pass (FW: 2.00.215)
                    Aurora: Pass (FW: 2.00.215)
                    Sprite: Pass (FW: 2.00.215)  
'''
import time

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E

timeout = 120

class homePageCloudDeviceGenerateDACFromPlusIcon(TestClient):

    def run(self):
        self.log.info('### Verifying user can generate DAC from plus icon at home page ###')
        try:
            self.delete_all_device_users()
            self.delete_all_users()
            self.add_new_user(username='aatuser')
            self.add_new_device_user()
            self.check_device_user()
        except Exception as e:
            self.take_screen_shot('homeGenerateDAC')
            self.log.error("User generate DAC from plus icon failed! Exception: {}".format(repr(e)))

    def tc_cleanup(self):
        self.delete_all_device_users()
        self.delete_all_users()
                
    def delete_all_device_users(self):
        self.log.info('Deleting all device users')
        device_user_info = self.get_device_user()[1]
        device_id_list = self.get_xml_tags(device_user_info, 'device_user_id')
        device_auth_list = self.get_xml_tags(device_user_info, 'device_user_auth_code')
        
        if not device_id_list:
            self.log.info('There are no device users exist')
            return
        
        for i, device_id in enumerate(device_id_list):
            result = self.delete_device_user(device_id, device_auth_list[i])
            if result != 0:
                raise Exception('Failed to delete device user, id:{} auth:{}'.format(device_id, device_auth_list[i]))

        self.log.info('All device users are deleted')
        
    def add_new_user(self, username):
        self.log.info('Adding new user:{}'.format(username))
        result = self.create_user(username)
        if result != 0:
            raise Exception('Failed to add new user:{}'.format(username))
        
        result = self.get_xml_tags(self.get_user(username)[1], 'user_id')
        if result == username:
            raise Exception('Failed to add new user:{}'.format(username))
        else:
            self.log.info('New user:{} is added'.format(username))

    def add_new_device_user(self):
        self.get_to_page('Settings')
        self.log.info('Check if Cloud Access is enabled.')
        cloud_access_text = self.get_text(E.SETTINGS_GENERAL_CLOUD_SWITCH)
        
        if cloud_access_text == 'ON':
            self.log.info('Clouad Access is enabled, status = {}'.format(cloud_access_text))
        else:
            self.log.info('Turn ON Cloud Access')
            self.click_wait_and_check(E.SETTINGS_GENERAL_CLOUD_SWITCH, E.SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK)
            self.click_wait_and_check(E.SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK, E.UPDATING_STRING, visible=False)

            cloud_access_text = self.get_text(E.SETTINGS_GENERAL_CLOUD_SWITCH)
            if cloud_access_text == 'ON':
                self.log.info("Cloud Access is enabled successfully. Status = {}".format(cloud_access_text))
            else:
                raise Exception('Failed to enable Cloud Access! Status = {}'.format(cloud_access_text))

        self.get_to_page('Home')
        self.log.info('Verifying admin can generate DAC from plus icon')
        self.click_wait_and_check(E.HOME_CLOUD_ACCESS, E.HOME_CLOUD_ACCESS_DIAG)
        self.click_link_element(E.HOME_CLOUD_ACCESS_DIAG_SELECT, 'admin')
        self.click_wait_and_check(E.HOME_CLOUD_ACCESS_DIAG_GETCODE, E.UPDATING_STRING, visible=False)
        self.current_frame_contains('Activation code successfully')
        self.click_wait_and_check(E.HOME_CLOUD_ACCESS_DIAG_GETCODE_DIAG_OK)

        self.log.info('Verifying user can generate DAC from plus icon')
        self.click_wait_and_check(E.HOME_CLOUD_ACCESS, E.HOME_CLOUD_ACCESS_DIAG)
        self.click_link_element(E.HOME_CLOUD_ACCESS_DIAG_SELECT, 'aatuser')
        self.click_wait_and_check(E.HOME_CLOUD_ACCESS_DIAG_GETCODE, E.UPDATING_STRING, visible=False)
        self.current_frame_contains('Activation code successfully')
        self.click_wait_and_check(E.HOME_CLOUD_ACCESS_DIAG_GETCODE_DIAG_OK)

    def check_device_user(self):
        self.log.info('Verifying if the DAC is added successfully')
        device_user_info = self.get_device_user()[1]
        user_list = self.get_xml_tags(device_user_info, 'username')
        if not user_list:
            raise Exception('Device user info list is empty, add DAC failed!')
        
        if 'admin' not in user_list:
            raise Exception('Add DAC from user:admin failed!')
        
        if 'aatuser' not in user_list:
            raise Exception('Add DAC from user:aatuser failed!')

homePageCloudDeviceGenerateDACFromPlusIcon()