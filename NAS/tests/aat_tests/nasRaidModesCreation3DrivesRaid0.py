"""
## @author: tran_jas, lo_va
# Objective: To exercise 3 drives configuration for Raid0 creation scenarios for multi-bay Consumer NAS.
# wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Modes_Creation
"""

import time
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Elements as E
sharename = 'Public'
generated_file_size = 10240
timeout = 240


class nasRaidModesCreation3DrivesRaid0(TestClient):

    def run(self):
        # Cleanup Drives first
        self.log.info('Clean up drive {}'.format(4))
        self.clean_up_drive(4)

        self.log.info('RAID0 creation test')
        self.configure_raid(raidtype='RAID0', number_of_drives=3, volume_size=500, force_rebuild=True)
        self.verifyMountVolume(volumeid=[1])
        self.log.info('Read/Write access check with SMB protocol')
        self.read_write_access_check(sharename=sharename, prot='SAMBA')
        self.check_home_page('healthy')

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED!!')
        else:
            self.log.error('Hash test FAILED!!')

    def read_write_access_check(self, sharename, prot):

        if prot == 'SAMBA':
            generated_file = self.generate_test_file(generated_file_size)
            time.sleep(10)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            # Wait for transfer finished
            time.sleep(10)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName='file0.jpg')
            self.read_files_from_smb(files='file0.jpg', share_name=sharename, delete_after_copy=True)
            time.sleep(10)

        elif prot == 'NFS':
            nfs_mount = self.mount_share(share_name=sharename, protocol=self.Share.nfs)
            if nfs_mount:
                created_files = self.create_file(filesize=generated_file_size, dest_share_name=sharename)
                for next_file in created_files[1]:
                    if not self.is_file_good(next_file, dest_share_name=sharename):
                        self.log.error('FAILED: nfs read test')
                    else:
                        self.log.info('SUCCESS: nfs read test')
        else:
            self.log.info('Protocol {} is not supported'.format(prot))

    def check_home_page(self, raidstate):
        try:
            self.log.info('Start to check raid status in home page')
            self.get_to_page('Home')
            if raidstate == 'degraded':
                dignostics_state = self.get_text('diagnostics_state')
                if 'Caution' in dignostics_state:
                    self.click_element('smart_info')
                    self.drag_and_drop_by_offset('css = div.jspDrag', 0, 80)
                    diagnosticsRaidStatus = self.get_text('home_diagnosticsRaidStatus_value')
                    if 'Degraded' in diagnosticsRaidStatus:
                        self.log.info('Raid status is Degraded!')
                else:
                    self.log.error('Raid status is not in Degraded')

            if raidstate == 'healthy':
                dignostics_state = self.get_text('diagnostics_state')
                if dignostics_state == 'Healthy':
                    self.log.info('Raid status is Healthy!')
                else:
                    self.log.error('Raid status is not in Healthy')

            if self.is_element_visible('home_diagnosticsClose1_button', 10):
                self.click_element('home_diagnosticsClose1_button')

            # Capacity verification
            raid_capacity = ''
            if self.is_element_visible(E.HOME_DEVICE_CAPACITY_FREE_SPACE, 10):
                raid_capacity = self.get_text(E.HOME_DEVICE_CAPACITY_FREE_SPACE)
            elif self.is_element_visible('home_volcapity_info', 10):
                raid_capacity = self.get_text('home_volcapity_info')

            if raid_capacity:
                self.log.info('Raid capacity is %s' % (raid_capacity))
            if not raid_capacity:
                self.log.error('Checked raid capacity failed in home page!')
            self.close_webUI()
        except Exception, ex:
            self.log.exception('Checked raid status failed in homp page!')

    def verifyMountVolume(self, volumeid):
        vol = ''
        try:
            self.get_to_page('Storage')
            if self.is_element_visible('css = div.flexigrid', 2):
                vol = self.get_text('css = div.flexigrid')
            for x in volumeid:
                if 'Volume_%s' % x in vol:
                    self.log.info('Volume_%s Mount Successed!!' % x)
                else:
                    self.log.error('Volume_%s Mount Failed!!' % x)
        except Exception, ex:
            self.log.exception('Volumes Mount not normally!!')
        self.close_webUI()

    def clean_up_drive(self, drive_number):
        drive_mapping = self.get_drive_mappings()
        clean_up_cmd = 'echo -e "o\ny\nw\ny\n" | gdisk /dev/{}'.format(drive_mapping.get(drive_number))
        self.run_on_device(clean_up_cmd)

nasRaidModesCreation3DrivesRaid0()
