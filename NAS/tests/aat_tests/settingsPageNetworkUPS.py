""" Settings page - Network - UPS (MA-193)

    @Author: vasquez_c
    Procedure @ silk:
                1) Open the webUI -> login as system admin
                2) Click on the Settings category
                3) Select Network -> UPS
                4) Enter target IP address
                5) Verify that the setting can be turned On or Off - Service can be enabled/disabled
                

    Automation: Full
                   
    Test Status:
        Local Lightning: Pass (FW: 2.10.XX)
"""             
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import Elements as E
import time

class settingsPageNetworkUPS(TestClient):
    
    def run(self):
        self.ups_slave_toggle()
        self.ups_slave_verification()
        
    def ups_slave_toggle(self):
        ups_ip = self.uut[self.Fields.ups_unit]
        ftp = self.uut[self.Fields.ftp_server]
        print ('The ups ip: {}'.format(ups_ip))
        print ('The ftp server ip: {}'.format(ftp))
        ups_slave_toggle_text = self.get_text(E.SETTINGS_NETWORK_UPS_SWITCH)
       
        if ups_slave_toggle_text == 'OFF':
            self.click_button(E.SETTINGS_NETWORK_UPS_SWITCH)
            self.log.info('Enabling UPS Slave')
        else:  
            self.log.info('UPS Slave was already enabled. Setting it OFF and ON')
            self.click_button(E.SETTINGS_NETWORK_UPS_SWITCH)
            time.sleep(5)
            self.click_button(E.SETTINGS_NETWORK_UPS_SWITCH)
        time.sleep(10)
        self.click_element('settings_networkUPSIP_text')
        self.input_text('settings_networkUPSIP_text',ups_ip)
        self.click_button('settings_networkUPSNext1_button')
        
    def ups_slave_verification(self):
        ups_status_text = self.get_text('ups_show_status')
        if ups_status_text == 'On Line':
            self.log.info('Success: Service can be enabled/disabled')
        else:
            self.log.error('Failed: Service cannot be enabled/disabled')
                 
settingsPageNetworkUPS()
        
        
            