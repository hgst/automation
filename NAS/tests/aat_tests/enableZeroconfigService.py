"""
Create on Jan 29, 2016
@Author: lo_va
Objective:  Verify Zeroconfig Service is enabled,
            so device will auto-acquire an IP address when no DHCP server is present to assign ip to the device
            KAM-390 or KAM-376 (M3 feature)
wiki URL: http://wiki.wdc.com/wiki/Enable_Zeroconfig_Service
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields


class EnableZeroconfigService(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        zeroconfig_files_list = ''
        try:
            zeroconfig_files_list = self.execute('find / -name zeroconf')[1]
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
        check_list1 = ['/usr/sbin/zeroconf', '/etc/network/if-up.d/zeroconf', '/etc/default/zeroconf']
        if all(word in zeroconfig_files_list for word in check_list1):
            self.log.info('Verify Enable Zeroconfig Service PASSED!!')
        else:
            self.log.error('Verify Enable Zeroconfig Service FAILED!!')

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test!!')
            exit()

EnableZeroconfigService(checkDevice=False)
