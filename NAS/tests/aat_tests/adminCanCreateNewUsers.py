"""
Created on July 27th, 2015

@author: tran_jas

## @brief FW-0620 User management.Admin can create new users
#   Get list of current users
#   Create new user
#   Get list of current users
#   Compare two lists to verify new user has been created successfully

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

username = 'elijah'

class AdminCanCreateUser(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            users_before = self.get_all_users()
            list_users_before = self.get_xml_tags(users_before, 'username')
            self.log.info('list_users_before: {}'.format(list_users_before))
            self.create_user(username)
            users_after = self.get_all_users()
            list_users_after = self.get_xml_tags(users_after, 'username')
            self.log.info('list_users_after: {}'.format(list_users_after))

            # Get the different between two user lists
            for a in list_users_before+list_users_after:
                if (a not in list_users_before) or (a not in list_users_after):
                    self.log.info("New user {}".format(username))

            if a == username:
                self.log.info("Create new user {} successfully".format(username))
            else:
                self.log.error("Failed to create user {}".format(username))
        except Exception, ex:
            self.log.exception('Failed to complete create user test case \n' + str(ex))
        finally:
            self.log.info("Delete user {}".format(username))
            self.delete_user(username)

AdminCanCreateUser()