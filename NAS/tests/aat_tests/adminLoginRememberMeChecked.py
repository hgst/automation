"""
title           :adminLoginRememberMeChecked.py
description     :To verify if the system admin credentials are kept after 'remember me' is checked on login page, after logout the system.
author          :yang_ni
date            :2015/03/22
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time
from testCaseAPI.src.testclient import Elements

class adminLoginRememberMeChecked(TestClient):

    def run(self):
            testname = 'System Admin Login - Remember me checked'
            self.start_test(testname)

            try:
                self.access_webUI(do_login=False)

                self.select_checkbox(Elements.LOGIN_REMEMBER_ME)
                self.checkbox_should_be_selected(Elements.LOGIN_REMEMBER_ME)

                self.click_button(Elements.LOGIN_BUTTON)
                time.sleep(8)
                self.wait_until_element_is_clickable(Elements.TOOLBAR_LOGOUT,15)
                self.click_button(Elements.TOOLBAR_LOGOUT)
                self.wait_until_element_is_visible(Elements.TOOLBAR_LOGOUT_LOGOUT,15)
                self.click_element(Elements.TOOLBAR_LOGOUT_LOGOUT)
                time.sleep(20)

                element = self.element_find('login_userName_text')
                username2 = element.get_attribute('value')

                self.log.info("The username after logout is :{}".format(username2))
                chkbox = self.is_checkbox_selected(Elements.LOGIN_REMEMBER_ME)
                self.log.info("Remember Me checkbox is checked? :{}".format(chkbox))

                if (username2 == 'admin') and (chkbox == True):
                    self.pass_test(testname, 'System Admin successfully login with Remember Me Checked')
                else:
                    self.fail_test(testname, 'System Admin name or remember me checkbox status are not kept after logging out')

            except Exception as e:
                    self.fail_test(testname, 'System Admin failed to login with Remember Me Checked: {}'.format(e))

# This constructs the loginNonAdminUser() class, which in turn constructs TestClient() which triggers the loginNonAdminUser.run() function
adminLoginRememberMeChecked()