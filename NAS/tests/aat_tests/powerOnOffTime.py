"""Created on Oct 14, 2014

@Author: lee_e, hoffman_t

Power On-Off Time
    [Tags]    immediate    power
    ${platform}    get platform
    import resource    ${CURDIR}/libraries/${platform}_test_cases.txt
    get to settings page
    wait until element is visible    link=Utilities
    click element    link=Utilities
    wait until element is visible    settings_utilitiesReboot_button
    click element    settings_utilitiesReboot_button
    wait until element is visible    id=popup_apply_button
    click element    id=popup_apply_button
    wait until system is booted
    wait until element is visible    nav_dashboard_link
    click element    nav_dashboard_link

    Pass/Fail Criteria:
    The test passes if:
    1.    Boot time is no longer than 180s
    2.    Power off time in no longer than 30s
    ***Note, however, that there is no spec for required bootup or shut down times. Lightning 4A does have
    requirement of reboot time of 2 minutes (without USB connection) and 25 secs shutdown time

"""

import time

from testCaseAPI.src.testclient import TestClient


class PowerOnOffTime(TestClient):
    def run(self):
        print self.is_ready()
        reboot_cycle, shutdown_time, bootup_time = self.time_setting()
        reboot_result = self.get_reboot_time(reboot_cycle)
        self.log.info('Waiting 5 seconds for the unit to finish booting')
        time.sleep(5)
        shutdown_result = self.get_shutdown_time(shutdown_time)
        self.log.info('Unit has powered off. Powering it back on in 5 seconds.')
        time.sleep(5)
        bootup_result = self.get_bootup_time(bootup_time)

        if reboot_result <= reboot_cycle:
            self.log.info('Reboot Time = %d <=%d , PASS' % (reboot_result, reboot_cycle))
        else:
            self.log.error('Reboot Time = %d > %d, FAIL' % (reboot_result, reboot_cycle))

        if shutdown_result <= shutdown_time:
            self.log.info('Shutdown Time = %d <=%d , PASS' % (shutdown_result, shutdown_time))
        else:
            self.log.error('Shutdown Time = %d > %d, FAIL' % (shutdown_result, shutdown_time))

        if bootup_result <= bootup_time:
            self.log.info('Bootup Time = %d <=%d , PASS' % (bootup_result, bootup_time))
        else:
            self.log.error('Bootup Time = %d > %d, FAIL' % (bootup_result, bootup_time))

    def get_reboot_time(self, reboot_cycle):
        # Let it go 1 1/2 times the maximum so we can complete the cycle even if it takes longer than expected
        start = time.time()
        self.log.info('Sending reboot command to device')
        self.reboot(max_boot_time=reboot_cycle * 2.5)
        reboot_time = time.time() - start
        self.log.info('Unit rebooted in ' + str(reboot_time) + ' seconds')

        return reboot_time

    def get_shutdown_time(self, shutdown_time):
        # self.login()
        start = time.time()
        self.log.info('Sending shutdown command to device')
        # Let it go 1 1/2 times the maximum so we can complete the cycle even if it takes longer than expected
        self.shutdown(timeout=shutdown_time * 2.5)
        shutdown_time = time.time() - start
        self.log.info('Unit shut down in ' + str(shutdown_time) + ' seconds')
        return shutdown_time

    def get_bootup_time(self, max_boot_time):
        # Turn off the outlet
        self.power_off()
        time.sleep(1)

        # Now, wait for the system to come back up.
        start_time = time.time()
        # Turn it back on
        self.log.info('Starting unit up')
        self.power_on()
        self.log.info('Waiting for unit to boot')
        while True:
            if self.is_ready():
                break

            if time.time() - start_time > max_boot_time * 1.5:
                raise Exception('Timed out waiting to boot')

        boot_time = time.time() - start_time
        self.log.info('Unit booted in ' + str(boot_time) + ' seconds')

        return boot_time

    def time_setting(self):
        # Get device's model then set reboot time values accoSenrdingly
        model_number = self.get_model_number()

        # Lightning ID
        if model_number == 'LT4A':
            reboot_cycle = 240
            shutdown_time = 35
            bootup_time = 205
            device = "Lightning"
            
        elif model_number == 'BNEZ':
            reboot_cycle = 210
            shutdown_time = 30
            bootup_time = 180
            device = "Sprite"
        
        else:
            reboot_cycle = 210
            shutdown_time = 30
            bootup_time = 180

            if model_number == 'BWZE':
                device = "Yellowstone"

            elif model_number == 'BZVM':
                device = "Zion"

            elif model_number == 'KC2A':
                device = "Kings Canyon"

            elif model_number == 'BWAZ':
                device = "Yosemite"

            elif model_number == 'BBAZ':
                device = "Aurora"

            elif model_number == 'GLCR':
                device = "Glacier"
            else:
                device = 'Unknown'

        self.log.info('setting reboot time: %d, shutdown time:%d, bootup time:%d for %s\n' % (
            reboot_cycle, shutdown_time, bootup_time, device))

        return reboot_cycle, shutdown_time, bootup_time


PowerOnOffTime()