import time

from testCaseAPI.src.testclient import TestClient


class Performance(TestClient):
    # Create tests as function in this class
    def run_aat(self):
        self.test_performance(1, 'GiB', 5)

    def run_art(self):
        self.test_performance(5, 'GiB', 5)

    def test_performance(self, file_size, file_units, count):
        destination_share = 'SmartWare'

        raid_mode = self.get_raid_mode()
        self.log.info('RAID mode is {}'.format(raid_mode))
        raid_level = self.get_raid_level(raid_mode)

        spec_write = self.uut[self.Fields.expected_write_speed][raid_level]
        self.log.info('Target write rate is {} MiB/second'.format(spec_write))
        write_tolerance_neg = self.uut[self.Fields.write_speed_tolerance][0]
        write_tolerance_pos = self.uut[self.Fields.write_speed_tolerance][1]

        transfer_lower_limit_write = spec_write * (1 + write_tolerance_neg)
        self.log.info('Minimum allowed write rate is {} MiB/second'.format(transfer_lower_limit_write))

        transfer_upper_limit_write = spec_write * (1 + write_tolerance_pos)
        self.log.info('Maximum allowed write rate is {} MiB/second'.format(transfer_upper_limit_write))

        # Start by testing the write performance
        write_test = 'Write Performance'
        self.start_test(write_test)
        for i in xrange(3, 0, -1):
            # 3 attempts
            avg_write, generated_files = self.create_file(filesize=file_size,
                                                          units=file_units,
                                                          file_count=count,
                                                          dest_share_name=destination_share)

            self.log.info('The actual write rate is {} MiB/second'.format(avg_write))

            if transfer_lower_limit_write < avg_write < transfer_upper_limit_write:
                self.log.info('Write rate is within the transfer threshold')
                self.pass_test(write_test)
                break
            else:
                if i == 1:
                    self.fail_test(write_test, 'Write rate is outside of the threshold')

        spec_read = self.uut[self.Fields.expected_read_speed][raid_level]
        self.log.info('Target read rate is {} MiB/second'.format(spec_read))
        read_tolerance_neg = self.uut[self.Fields.read_speed_tolerance][0]
        read_tolerance_pos = self.uut[self.Fields.read_speed_tolerance][1]

        transfer_lower_limit_read = spec_read * (1 + read_tolerance_neg)
        self.log.info('Minimum allowed read rate is {} MiB/second'.format(transfer_lower_limit_read))

        transfer_upper_limit_read = spec_read * (1 + read_tolerance_pos)
        self.log.info('Maximum allowed write rate is {} MiB/second'.format(transfer_upper_limit_read))


        # Validate the files and check performance
        for next_file in generated_files:
            # Get the file's read performance
            read_test = 'Read Performance - {}'.format(next_file)
            self.start_test(read_test)
            for i in xrange(3, 0, -1):
                file_read = self.get_read_speed(next_file, dest_share_name=destination_share)

                self.log.info('The actual read rate of {} is {} MiB/second'.format(next_file, file_read))

                if transfer_lower_limit_read < file_read < transfer_upper_limit_read:
                    self.pass_test(read_test, 'Read rate is within the transfer threshold')
                    break
                else:
                    if i == 1:
                        self.fail_test(read_test, 'Write rate is outside of the threshold')

            # Verify the file. We can't verify during the read performance while doing this since the memory compares
            # being done by the tool impact performance.
            integrity_test = 'Data Integrity - {}'.format(next_file)
            self.start_test(integrity_test)
            if not self.is_file_good(next_file, dest_share_name=destination_share):
                # Do not count this files read performance since a corrupted file may impact performance
                self.fail_text(integrity_test, '{} is corrupt.'.format(next_file))
            else:
                self.pass_test(integrity_test)


Performance(checkDevice=False)
