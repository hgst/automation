'''
Create on June 1, 2015

@Author: lo_va

Objective: This test will be to determine if the system recognizes a RAID Degradation 
           from each supported RAID level and to determine if all data is still accessable, in degrade mode.
wiki URL: http://wiki.wdc.com/wiki/RAID_Degradation
'''

import time
import sys
# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch
from testCaseAPI.src.testclient import Elements as E
from global_libraries import CommonTestLibrary as ctl
from global_libraries import wd_exceptions
generated_file_size = 1024000
timeout = 240
share_name = 'Public'
volumesize = 250

class degradedRaid(TestClient):
    
    def run(self):
        
        # Set Web Acces Timeout value to 30 mins
        self.set_web_access_timeout()
        
        # Check Drives account
        if self.get_drive_count() == 1:
            raise wd_exceptions.InvalidDeviceState('Device is not with 2 or 4 drives, cannot test Degraded RAID1 Test case!!!')
        if self.get_drive_count() == 2:
            self.RAID1_degraded_test(2, 1)
            self.RAID1_degraded_test(2, 2)
        if self.get_drive_count() == 4:
            self.RAID1_degraded_test(4, 1)
            self.RAID1_degraded_test(4, 2)
        
    def RAID1_degraded_test(self, drives, plug_disk):
        # UUT1
        raid_mode = self.get_raid_mode()
        raid_status = self.check_storage_page()
        get_raid_status = self.get_RAID_drives_status()
        raid_drives_number = self.get_xml_tags(get_raid_status, 'raid_mode').count('1')
        if drives == 2:
            # Create RAID1 if UUT is not in RAID1
            if raid_mode == 'RAID1' and raid_drives_number == 2 and raid_status == 'Healthy':
                self.log.info('Raid type already in RAID1 and status is Healthy')
            else:
                self.log.info('Start to create RAID1.')
                self.configure_raid(raidtype='RAID1', number_of_drives=2, force_rebuild=True)
        if drives == 4:
            # Create 2X RAID1 if UUT is not in 2X RAID1
            if raid_mode == 'RAID1' and raid_drives_number == 4 and raid_status == 'Healthy':
                self.log.info('Raid type already in 2X RAID1 and status is Healthy')
            else:
                self.log.info('Start to create 2X RAID1.')
                self.configure_raid(raidtype='RAID1', number_of_drives=4, force_rebuild=True, volume_size=volumesize)
        self.disable_auto_rebuild()
            
        # Validate access via smb protocol
        global generated_file
        try:
            self.log.info('Start to validate Normal RAID1 access via smb protocol.')
            self.read_write_access_check(sharename=share_name)
        except Exception, ex:
                self.log.exception('Failed to access Normal RAID1\n' + str(ex))

        # Plug out the disk
        self.log.info('Plug out Disk {} !!'.format(plug_disk))
        self.remove_drives(plug_disk)

        # Check RAID1 status was in degraded and validate access
        self.log.info('Check RAID1 status was in degraded and validate access.')
        raid_status1 = self.raid_status(1)
        if 'degraded' in raid_status1:
            try:
                self.log.info('Raid status: Degraded.')
                self.read_write_access_check(sharename=share_name, write_access=False)
            except Exception, ex:
                self.log.exception('Failed to access Degraded_RAID1\n' + str(ex))
        else:
            self.log.error('RAID1 did not into degraded mode!\n')

        self.close_webUI()
        
        for x in range(1, 4):
            self.log.info('Reboot {} time test'.format(x))
            self.reboot(500)
            time.sleep(20) # Wait for service enabled.
            # Validate access after reboot via smb protocol
            try:
                self.log.info('Start to validate Degraded RAID1 access after system reboot.')
                self.read_write_access_check(sharename=share_name, write_access=False)
            except Exception, ex:
                self.log.exception('Failed to access Degraded RAID1 after system reboot\n' + str(ex))

    def tc_cleanup(self):
        self.checked_disk_exist()
        if self.get_drive_count() == 2:
            raid_status1 = self.raid_status(1)
            if 'degraded' in raid_status1:
                self.configure_raid(raidtype='RAID0',volume_size=100)
        if self.get_drive_count() == 4:
            raid_status1 = self.raid_status(1)
            raid_status2 = self.raid_status(2)
            if 'degraded' in raid_status1 or 'degraded' in raid_status2:
                self.configure_raid(raidtype='RAID0', volume_size=100)

    def checked_disk_exist(self):
        drive_mapping = self.get_drive_mappings()
        for x in range(1, self.uut[self.Fields.number_of_drives]+1):
            if x not in drive_mapping:
                self.log.info('Drive {} is disconnected, try to insert it!'.format(x))
                self.insert_drives(x)
            else:
                self.log.info('Drive {} is connected.'.format(x))
        if self.get_drive_count() == self.uut[self.Fields.number_of_drives]:
            self.log.info('Clean_up for insert drives succeed!')
        else:
            self.log.warning('Clean_up for insert drives failed!')

    def check_storage_page(self):
        self.get_to_page('Storage')
        self.wait_until_element_is_visible('raid_healthy')
        raid_status = self.get_text('raid_healthy')
        while not raid_status:
            raid_status = self.get_text('raid_healthy')
            time.sleep(1)
        return raid_status

    def disable_auto_rebuild(self):
        try:
            self.get_to_page('Storage')
            check_status = self.element_find('css = #storage_raidAutoRebuild_switch+span .toggle_off')
            time.sleep(2)
            check_status_attr = check_status.get_attribute('style')
            if 'none' in check_status_attr:
                self.log.info('Disable Auto-Rebuild')
                self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                check_status_attr = check_status.get_attribute('style')
                while 'none' in check_status_attr:
                    self.click_element('css = #storage_raidAutoRebuild_switch+span .checkbox_container')
                    check_status_attr = check_status.get_attribute('style')
                    time.sleep(2)
                time.sleep(2)
            if 'block' in check_status_attr:
                self.log.info('Auto-Rebuild already has been disabled')
            #self.close_webUI()
        except Exception, ex:
                self.log.exception('Failed to disable auto-rebuild\n' + str(ex))

    def read_write_access_check(self, sharename, write_access=True):
        global generated_file
        if write_access:
            generated_file = self.generate_test_file(generated_file_size)
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
        self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName='file0.jpg')
        self.read_files_from_smb(files='file0.jpg', share_name=sharename, delete_after_copy=False)

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('Hash test SUCCEEDED!!')
        else:
            self.log.error('Hash test FAILED!!')
            
    def raid_status(self, raidnum):
        check_raid_status = 'mdadm --detail /dev/md%s | grep "State :"' % raidnum
        raid_status = self.run_on_device(check_raid_status)
        time.sleep(1) # Time to wait status return value
        while not raid_status:
            raid_status = self.run_on_device(check_raid_status)
            time.sleep(1)
        self.log.info('Raid_status = %s' % raid_status)
        raid_status1 = str(raid_status.split(': ')[1])

        return raid_status1

degradedRaid()