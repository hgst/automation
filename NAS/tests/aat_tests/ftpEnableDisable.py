""" FTP Server Enable/Disable

    @Author: lin_ri
    wiki URL: http://wiki.wdc.com/wiki/FTP
    
    Automation: Full
    
    Test Status:
              Local Lightning: Pass (FW: 2.00.026)
""" 

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from selenium.common.exceptions import StaleElementReferenceException
import global_libraries.wd_exceptions as exceptions
import xml.etree.ElementTree as ET
from ftplib import FTP
from ftplib import error_perm
import time
import os
import shutil


picturecounts = 0

class ftpEnDisable(TestClient):
    def run(self):
        try:
            self.log.info('######################## FTP Enable/Disable TESTS START##############################')
            self.log.info("Delete all groups")
            self.delete_all_groups()
            self.log.info("Delete all users")
            try:
                self.delete_all_users(use_rest=False)
            except exceptions.InvalidDeviceState:
                self.log.info("User list is empty")
            self.log.info("Delete all shares")
            self.delete_all_shares()  # Clean all shares except Public, SmartWare and TimeMachineBackup
            # Enable FTP and create new user
            self.log.info("====== TEST 1: Enable FTP and create new user test ======")
            self.configure_ftp()
            self.add_new_user(new_user_name='user1', new_user_pwd='password1')  # RESTful API
            # # self.create_new_user(new_user_name='user1', new_user_pwd='password1') # Selenium API
            self.check_ftp_state()
            # FTP enable Per Share
            self.log.info("====== TEST 2: FTP enable Per share test ======")
            self.config_public_share("Anonymous None")
            self.config_user_share(new_share='test1', new_share_desc='for test1 test')
            time.sleep(3)
            self.test_anonymous_login()  # Procedure error, in here, we can login but unable to see the Public folder
            self.test_user_access(new_share='test1', user_name='user1', user_pwd='password1')
            time.sleep(3)
            self.test_ftp_setting(new_share='test1')
            # Checking that disabling the entire FTP server should stop FTP access
            self.log.info("====== TEST 3: FTP service disabled test ======")
            self.disable_ftp()
            self.test_anonymous_fail_connect()
            self.configure_ftp()
            self.config_public_share("Anonymous None")
            self.config_user_share(new_share='test1', new_share_desc='for test1 test')
            time.sleep(3)
            self.test_user_access(new_share='test1', user_name='user1', user_pwd='password1')
            # Checking that disabling the share's FTP access will not display the share/folder when accessing via FTP
            self.log.info("====== TEST 4: (Disabling FTP) No share folder display access test ======")
            self.configure_ftp()
            self.disable_ftp_public_share()
            time.sleep(3)
            self.test_disable_public_share(user_name='user1', user_pwd='password1')
            time.sleep(3)
            self.test_ftp_access_shares()
            # Anonymous access
            self.log.info("====== TEST 5: Anonymous access test ======")
            self.config_public_share(access="Anonymous Read / Write")
            time.sleep(3)
            self.test_anonymous_rw_access(folder='Public')
            time.sleep(2)
            self.config_public_share(access="Anonymous Read Only")
            time.sleep(3)
            self.test_anonymous_r_access(folder='Public')
            self.close_webUI()
            time.sleep(5)
        except Exception as e:
            self.log.exception("Test Failed: FTP Enable/Disable test, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        finally:
            self.log.info('######################## FTP Enable/Disable TESTS END ##############################')

    def configure_ftp(self):
        """
            Configure FTP setting to ON
        """
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info('==> Configure ftp')
            ftp_status_text = self.get_text('css = #settings_networkFTPAccess_switch+span .checkbox_container')
            if ftp_status_text == 'ON':
                self.log.info('FTP is enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.info('Turn ON FTP service')
                self.click_wait_and_check('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                          'popup_ok_button', visible=True)
                self.click_wait_and_check('popup_ok_button', visible=False)
                time.sleep(3)  # wait for updating
                ftp_status_text = self.get_text('css = #settings_networkFTPAccess_switch+span .checkbox_container')
                if ftp_status_text == 'ON':
                    self.log.info("FTP is enabled successfully. Status = {}".format(ftp_status_text))
            time.sleep(2)
        except Exception as e:
            self.takeScreenShot(prefix='configFtp')
            self.log.exception('FAILED: Unable to configure ftp!! Exception: {}'.format(repr(e)))
        else:
            self.close_webUI()

    def add_new_user(self, new_user_name='user1', new_user_pwd='password1'):
        """
            Create a new user (RESTful API)
            @new_user_name: new user name
            @new_user_pwd: new user's password
        """        
        try:
            self.log.info("==> Create new user (by RESTAPI)")
            resp = self.get_user(new_user_name)[0]
            if resp == 0:  # user exists
                self.delete_user(new_user_name)
                self.create_user(new_user_name, new_user_pwd)
            else:  #404, user does not exist
                self.create_user(new_user_name, new_user_pwd)
            self.log.info("User: {} is created successfully.".format(new_user_name))
        except Exception as e:
            self.log.exception('FAILED: Unable to create new user, exception: {}'.format(repr(e)))

    def create_new_user(self, new_user_name='user1', new_user_pwd='password1'):
        """
            Create a new user (Selenium API)
            @new_user_name: new user name
            @new_user_pwd: new user's password
        """
        try:
            self.log.info('==> Create new user')
            self.get_to_page('Users')
            self.click_wait_and_check('nav_users_link', 'users_createUser_link', visible=True)
            time.sleep(5)
            # if user1 exists, remove it first then create a new one
            if self.is_element_visible('users_user_{}'.format(new_user_name), 15):
                self.click_wait_and_check('users_user_{}'.format(new_user_name),
                                          'css=#users_editPW_switch+span .checkbox_container')
                self.click_wait_and_check('users_removeUser_link', 'popup_apply_button', visible=True)
                self.click_wait_and_check('popup_apply_button', visible=False)
                self.wait_for_updating_complete(interval=3)
            # create a new user1
            self.click_wait_and_check('users_createUser_link', 'users_addUserSave_button', visible=True)
            self.input_text_check('users_userName_text', new_user_name, False)
            self.input_text_check('users_newPW_password', new_user_pwd, False)
            self.input_text_check('users_comfirmPW_password', new_user_pwd, False)
            # Due to screen resolution, 'save' button can not be seen to click
            time.sleep(2)
            self.click_wait_and_check('users_addUserSave_button', visible=False)
            self.wait_for_updating_complete(interval=3)
            self.log.info('User: {} is created successfully.'.format(new_user_name))
        except Exception as e:
            self.takeScreenShot(prefix='createNewUser')
            self.log.exception('FAILED: Unable to create new user, exception: {}'.format(repr(e)))

    def check_ftp_state(self):
        """
            Verify the ftp setting
        """
        try:
            # Once you switch to Users page, you can not switch to Settings
            # Go to Home page (Workaround: for 2.00.026)
            # Commented out by using new framework fix rev 17377
            # self.log.info("Go to Home before switching to Settings")
            # self.get_to_page("Home")
            # Go to Settings
            self.log.info("Checking FTP status information")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info('==> Checking FTP State')
            ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
            if ftp_status_text == 'ON':
                self.log.info('FTP has been enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.error('FAILED: FTP service is OFF!! It should be ON.')
                self.takeScreenShot(prefix='ftpOff')
        except Exception as e:
            self.takeScreenShot(prefix='checkftpstate')
            self.log.exception('FAILED: Unable to configure ftp!! Exception: {}'.format(repr(e)))
        else:
            self.close_webUI()

    def config_public_share(self, access="Anonymous None"):
        """
            Set access permission to the public share for anonymous user
            
            @access: Public share access permission for anonymous
        """
        try:              
            self.log.info('==> Configure FTP share with {}'.format(access))          
            self.get_to_page('Shares')
            time.sleep(2)
            self.click_wait_and_check('nav_shares_link',
                                      'shares_createShare_button', visible=True, timeout=30)
            self.log.info('Click Public')
            self.wait_until_element_is_visible('shares_share_Public', timeout=30)
            self.click_wait_and_check('shares_share_Public',
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            self.log.info('Switch the ftp to ON')
            self.wait_until_element_is_visible('css=#shares_ftp_switch+span .checkbox_container', 30)
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_wait_and_check('css=#shares_ftp_switch+span .checkbox_container', 'ftp_dev', visible=True)
            self.log.info('ftp service is enabled to ON')
            self.click_link_element('ftp_dev', access)
            time.sleep(1)
            if self.is_element_visible('shares_ftpSave_button', wait_time=30):
                self.click_wait_and_check('shares_ftpSave_button', visible=False)
            self.wait_for_updating_complete(interval=3)
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. Exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            self.takeScreenShot(prefix='configPublicShare')
        else:
            self.log.info("Save {} permission to Anonymous.".format(access))

    def config_user_share(self, new_share='test1', new_share_desc='for test 1 test'):
        """
            Create user share
            
            @new_share: new share folder name
            @new_share_desc: the description of new share folder
        """
        try:
            # self.log.info("==> Add a new share called {}".format(new_share))
            # # RESTful API
            # self.log.info("==> Adding new user share - {} (by RESTAPI)".format(new_share))
            # if self.get_share(new_share)[0] == 0:
            #     self.log.info("{} share exists".format(new_share))
            #     self.delete_share(new_share)
            #     self.create_shares(share_name=new_share, number_of_shares=1, description=new_share_desc)
            #     self.update_share_network_access(share_name=new_share, public_access=True)
            # else:
            #     self.log.info("{} share does not exist".format(new_share))
            #     self.create_shares(share_name=new_share, number_of_shares=1, description=new_share_desc)
            #     self.update_share_network_access(share_name=new_share, public_access=True)
            # time.sleep(5)
            # if self.get_share(new_share)[0] == 0:
            #     self.log.info("Succeed to create {} share".format(new_share))
            #     self.reload_page()
            # else:
            #     self.log.info("Failed to create {} share".format(new_share))
            #     self.log.info("Try to recreate the user share again")
            #     self.create_shares(share_name=new_share, number_of_shares=1, description=new_share_desc)
            #     self.update_share_network_access(share_name=new_share, public_access=True)
            #     if self.get_share(new_share)[0] == 0:
            #         self.log.info("Succeed to create {} share".format(new_share))
            #         self.reload_page()
            #     else:
            #         self.log.error("Failed to create {} share".format(new_share))
            self.log.info("****** Create new share: {} by WebUI ******".format(new_share))
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link',
                                      'shares_createShare_button', visible=True, timeout=30)
            if self.is_element_visible('shares_share_{}'.format(new_share), wait_time=15):
                self.log.info("New share: {} exists".format(new_share))
            else:  # create new share
                self.log.info("Create new share: {}".format(new_share))
                self.click_wait_and_check('shares_createShare_button', 'shares_createSave_button', visible=True)
                self.input_text_check('shares_shareName_text', new_share, False)
                self.input_text_check('shares_shareDesc_text', new_share_desc, False)
                time.sleep(2)
                self.click_wait_and_check('shares_createSave_button', visible=False)
                self.wait_for_updating_complete(interval=3)
                self.click_wait_and_check('nav_shares_link',
                                          'shares_createShare_button', visible=True, timeout=30)
                if self.is_element_visible('shares_share_{}'.format(new_share), wait_time=15):
                    self.log.info("Succeed to create new share: {}".format(new_share))
                else:
                    self.log.error("ERROR: Unable to create new share: {}".format(new_share))
                    self.takeScreenShot(prefix='{}'.format(new_share))
                    raise Exception("ERROR: Fail to create new share, stop the test")
            self.log.info("Select share: {}".format(new_share))
            self.click_wait_and_check('shares_share_{}'.format(new_share),
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            self.log.info('Switch the ftp to ON')
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_element('css=#shares_ftp_switch+span .checkbox_container')
            self.log.info('ftp service is enabled to ON')
            self.click_link_element('ftp_dev', linkvalue='Anonymous None')
            if self.is_element_visible('shares_ftpSave_button', wait_time=30):
                self.click_wait_and_check('shares_ftpSave_button', visible=False)
            self.wait_for_updating_complete(interval=3)
        except Exception as e:
            self.takeScreenShot(prefix='configUserShare')
            self.log.error('FAILED: Unable to config {} share. Exception: {}'.format(new_share, repr(e)))
        else:
            self.log.info("Save Anonymous None permission to {}.".format(new_share))

    def test_anonymous_login(self, FTP_IP=""):
        """
            FW: 1.05.30, Validate anonymous access to Public share folder
            FW: 2.00.026, Validate anonymous has no access to login ftp
            
            @FTP_IP: Target FTP server you'd like to test
        """
        if FTP_IP == "":
            FTP_IP = self.uut[Fields.internal_ip_address]
        self.log.info("==> Test anonymous ftp login to {}".format(FTP_IP))
        time.sleep(2)
        retry = 3
        ftpconn = None
        while retry > 0:
            try:
                ftpconn = FTP(FTP_IP)
            except Exception as e:
                retry -= 1
                self.log.warning('RETRY: {} attempt, Unable to connect to FTP {}, exception: {}'.format(3-retry, FTP_IP, repr(e)))
                if retry == 0:
                    self.log.error("Failed 3 retries to establish FTP connection, exception: {}".format(repr(e)))
            else:
                break                
            
        # Lightning FW: 2.00.026
        try:
            ftpconn.login()
        except error_perm as ex:
            self.log.info("PASS: Anonymous failed to login ftp, exception: {}".format(repr(ex)))            
        except Exception as e:
            self.log.error("FAILED: Anonymous login error, exception: {}".format(repr(e)))
        finally:
            ftpconn.close()
        """
        # Lightning FW: 1.05.30
        try:
            listfolders = ftpconn.nlst()
            if 'Public' in listfolders:
                self.log.error("FAILED: Anonymous user can see Public folder.")
            else:
                self.log.info("PASS: Anonymous user can login but can't see Public folder")
            ftpconn.close()
        except Exception as e:
            self.log.error("FAILED: Public folder access test failed, exception: {}".format(repr(e)))
        """

    def test_user_access(self, new_share="test1", user_name='user1', user_pwd='password1'):
        """
            Validate the r/w ftp access in "test1" share folder for 'user1'
            
            @new_share: New share folder you'd like to test
            @user_name: new user who can access to the new share folder
            @user_pwd: new user's password 
        """
        try:
            ftpconn, ex = self.ftpConnection(new_user_name=user_name, new_user_pwd=user_pwd, anonymous=False)
            self.ftpWriteTest(ftpconn, share_folder=new_share, delete_after_upload=True)
        except Exception as ex:
            self.log.error(repr(ex))
            
        try:
            ftpconn, ex = self.ftpConnection(new_user_name=user_name, new_user_pwd=user_pwd, anonymous=False)
            self.ftpReadTest(ftpconn, share_folder=new_share, delete_after_download=False)
        except Exception as ex:
            self.log.error(repr(ex))

    def test_ftp_setting(self, new_share='test1'):
        """ 
            Verify the settings are still valid
            
            @new_share: new share folder name
        """
        try:
            self.log.info('==> Checking FTP settings for {} again'.format(new_share))        
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link',
                                      'shares_createShare_button', visible=True, timeout=15)
            time.sleep(5)
            if self.is_element_visible("shares_share_{}".format(new_share), 30):
                self.log.info("PASS: test1 share exists")
                # self.click_element("shares_share_{}".format(new_share))
                self.click_wait_and_check('shares_share_{}'.format(new_share),
                                          'css=#shares_public_switch+span .checkbox_container', visible=True)
            else:
                self.log.error("FAILED: test 1 share does not exist")
            time.sleep(2)
            
            self.wait_until_element_is_visible('css=#shares_ftp_switch+span .checkbox_container', 30)
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("PASS: ftp service is ON, status = {}".format(ftp_status))
            else:
                self.log.error("FAILED: ftp service is OFF")

            access_test = self.get_text("ftp_dev")
            if "Anonymous None" == access_test:
                self.log.info("PASS: {} access is Anonymous None".format(new_share))
            else:
                self.log.info("FAILED: {} access is wrong. Now is {}".format(new_share, access_test))
        except Exception as e:
            self.log.error("FAILED: FTP settings are not valid. Exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='testFtpSetting')
        else:
            self.close_webUI()

    def disable_ftp(self):
        """
            Disable FTP service
        """
        retry = 3
        while retry >= 0:
            try:
                self.get_to_page('Settings')
                self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
                self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
                self.log.info('==> Turn off ftp service')
                ftp_status_text = self.get_text('css = #settings_networkFTPAccess_switch+span .checkbox_container')
                if ftp_status_text == 'ON':
                    self.click_element('css = #settings_networkFTPAccess_switch+span .checkbox_container')
                    time.sleep(2)
                    ftp_status_text = self.get_text('css = #settings_networkFTPAccess_switch+span .checkbox_container')
                    if ftp_status_text == 'OFF':
                        self.log.info("FTP is disabled successfully. Status = {}".format(ftp_status_text))
                else:
                    self.log.info('FTP service is disabled, status = {0}'.format(ftp_status_text))
            except StaleElementReferenceException:
                if retry == 0:
                    self.log.error("Reach maximum retry for exception: {}".format(repr(e)))
                    raise
                self.log.info("Going for retry due to exception: {}".format(repr(e)))
                retry -= 1
                continue
            except Exception as e:
                self.takeScreenShot(prefix='disableFtp')
                self.log.exception('FAILED: Unable to configure ftp!! Exception: {}'.format(repr(e)))
            else:
                break
        self.close_webUI()

    def test_anonymous_fail_connect(self, FTP_IP=""):
        """
            Test Anonymous unable to login if FTP service is OFF
            
            @FTP_IP: DUT's IP
        """
        if FTP_IP == "":
            FTP_IP = self.uut[Fields.internal_ip_address]        
        try: 
            ftps = FTP(FTP_IP)
            if ftps:
                raise Exception("FAILED: FTP service is OFF, ftp connection should be refused.")
            ftps.close()
        except IOError:
            self.log.info("PASS: Unable to establish anonymous ftp connection due to ftp service set to OFF")
        except Exception as e:
            self.log.error("FAILED: exception: {}".format(repr(e)))

    def disable_ftp_public_share(self):
        """
            Disable ftp service on public share folder
        """
        try:
            self.log.info('==> Disable FTP for public share')       
            self.get_to_page('Shares')
            self.click_wait_and_check('nav_shares_link',
                                      'shares_createShare_button', visible=True, timeout=15)
            self.log.info('Click Public')
            time.sleep(2)
            self.wait_until_element_is_visible('shares_share_Public', timeout=15)
            self.click_wait_and_check('shares_share_Public',
                                      'css=#shares_ftp_switch+span .checkbox_container', visible=True)
            self.log.info('Switch OFF ftp service')
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.click_element('css=#shares_ftp_switch+span .checkbox_container')
                self.log.info("FTP service is OFF")
            else:
                self.log.info("ftp has been turned off")
            self.wait_for_updating_complete(interval=3)
            # time.sleep(2)
        except Exception as e:
            self.log.error("FAILED: Unable to disable ftp public share, exception:{}".format(repr(e)))
            self.takeScreenShot(prefix='disableFtpPublicShare')

    def test_disable_public_share(self, user_name='user1', user_pwd='password1'):
        """
            Test if Public folder can be seen
            
            @user_name: new user name
            @user_pwd: new user's password
        """
        self.log.info("==> Testing ftp access to disabled public share")
        retry = 3
        while retry >= 0:
            try:
                ftpconn, ex = self.ftpConnection(new_user_name=user_name, new_user_pwd=user_pwd, anonymous=False)
                if ex:
                    raise ex
            except Exception:
                if retry == 0:
                    self.log.error("ERROR: ({}, {}) account fail to connect ftp w/ "
                                       "disabled public share.".format(user_name, user_pwd))
                    raise
                retry -= 1
                self.log.info("Remaining {} retry...".format(retry))
                time.sleep(1)
                continue
            else:
                try:
                    listfolders = ftpconn.nlst()
                    if 'Public' in listfolders:
                        self.log.error("FAILED: {} can see Public folder which is disabled.".format(user_name))
                    else:
                        self.log.info("PASS: Public folder does not display")
                    ftpconn.close()
                except Exception as e:
                    self.log.error("FAILED: disable public share test failed, Exception: {}".format(repr(e)))
                finally:
                    break

    def test_ftp_access_shares(self):
        """
            Test FTP access to public share
        """
        self.log.info("==> Testing ftp access to public share")
        try:
            # self.config_public_share("Anonymous None")
            self.config_public_share("Anonymous Read / Write")
            time.sleep(3)
            self.test_user_access(new_share='Public', user_name='user1', user_pwd='password1')
        except Exception as e:
            self.log.error("FAILED: Public share ftp test failed, exception: {}".format(repr(e))) 

    def test_anonymous_rw_access(self, folder='Public'):
        """
            Validate anonymous r/w access
            
            @folder: the folder name you'd like to test for anonymous
        """
        try:
            ftpconn, ex = self.ftpConnection(anonymous=True)
            self.ftpWriteTest(ftpconn, share_folder=folder, delete_after_upload=True)
        except Exception as e:
            self.log.error(repr(e))
            
        try:
            ftpconn, ex = self.ftpConnection(anonymous=True)
            self.ftpReadTest(ftpconn, share_folder=folder, delete_after_download=True)
        except Exception as e:
            self.log.error(repr(e))        

    def test_anonymous_r_access(self, folder='Public'):
        """
            Validate anonymous read only access
            
            @folder: the folder name you'd like to test for anonymous
        """
        try:
            ftpconn, ex = self.ftpConnection(anonymous=True)
            self.ftpReadTest(ftpconn, share_folder=folder, delete_after_download=True)
        except Exception as e:
            self.log.error(repr(e))          

        try:
            ftpconn, ex = self.ftpConnection(anonymous=True)
            self.ftpWriteTest(ftpconn, share_folder=folder, delete_after_upload=True)
        except error_perm as e:
            self.log.info("PASS: Anonymous can not write files via FTP, error: {}".format(repr(e)))
        except Exception as e:
            self.log.error(repr(e))        
        
    def ftpConnection(self, ftp_ip="", new_user_name='user1', new_user_pwd='password1', anonymous=True):
        """
            Create ftp connection and login by user account or anonymous
            Once login, it will return the ftp socket back and exception
            
            @ftp_ip: '192.168.1.91' or self.uut[Fileds.internal_ip_address]
            @anonymous: True (Anonymous login)
                        User login: new_user_name / new_user_pwd (Set to anonymous=False)
                        If you do not set anonymous to False, it always login as anonymous user
            
            Need to set anonymous=False to switch to user login mode
            @new_user_name: user account
            @new_user_pwd: user password               
            
            return (ftpconn, exception)
            
            Usage:
            ftpconn, ex = self.ftpConnection('192.168.1.91', new_user_name='user1', new_user_pwd='password1', anonymous=False)
            ftpconn, ex = self.ftpConnection('192.168.1.91', anonymous=True)
        """
        ex = None
        if ftp_ip == "":
            ftp_ip = self.uut[Fields.internal_ip_address]
            
        try:
            ftpconn = FTP(ftp_ip)
        except Exception as e:
            self.log.info("Warning: Unable to connect to FTP: {}, exception: {}".format(ftp_ip, repr(e)))
            ftpconn = None
            return (ftpconn, e)

        try:
            if anonymous:
                resp = ftpconn.login()
            else:
                resp = ftpconn.login(new_user_name, new_user_pwd)
        except error_perm as e:
            self.log.info("Warning: Unable to login ftp, exception: {}".format(repr(e)))
            return (ftpconn, e)
        except Exception as e:
            self.log.info("FAILED: ftp login failed, exception: {}".format(repr(e)))
            return (ftpconn, e)
                                
        if '230' in resp:
            if anonymous:
                self.log.info("Anonymous login succeed")
            else:
                self.log.info("{} login succeed".format(new_user_name))
        else:
            if anonymous:
                self.log.error("Anonymous login failed")
            else:
                self.log.error("{} login failed".format(new_user_name))
        
        return (ftpconn, ex)

    def ftpReadTest(self, ftpconn, filesize=10240, share_folder='Public', delete_after_download=True):
        """
            Pre-requiste:
                You need to have temporary file called 'file0.jpg' under the share_folder for test
                Normally, run the upload(write) test first to generate the temporary file there
                Then, execute the download(read) test
             
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_download: True (Delete the temporary file after downloading test)
        """
        time.sleep(2)
        resp = ftpconn.cwd(share_folder)  # switch to 'Public' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))        
        
        # download newfile.jpg as download_newfile.jpg
        tmpfile = 'newfile.jpg'  # temporary file on FTP server
        download_file = "download_newfile.jpg"  # filename which is saved locally

        # if download_file exists, removes it
        if os.path.isfile(download_file):
            os.remove(download_file)
            self.log.info("Cleaning downloaded file: {}".format(download_file))

        # Check if tmpfile exists or not
        listfiles = ftpconn.nlst()
        if tmpfile in listfiles:
            self.log.info("{} exists, read test is ready to go".format(tmpfile))
        else:
            raise Exception("FAILED: {} does not exist for read test, STOP!".format(tmpfile))
    
        self.log.info("*** Going to download file for read access test ***")
        
        try:
            with open(download_file, "wb") as f:
                def callback(data):
                    f.write(data)
                resp = ftpconn.retrbinary("RETR "+tmpfile, callback, blocksize=1048576)
            if '226' in resp:
                self.log.info("{} is saved locally".format(download_file))
                # self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("FAILED: {} download failed".format(download_file))
            
        except Exception as e:
            raise Exception("FAILED: ftpReadTest failed, can not download {}, exception: {}".format(tmpfile, repr(e)))
        ftpconn.close()
        
        if delete_after_download:
            os.remove(download_file)
            self.log.info("Cleaning temporary files: {}".format(os.path.abspath(download_file)))
        
        self.log.info("*** Read access test END ***")                 
    
    def ftpWriteTest(self, ftpconn, filesize=10240, share_folder='Public', delete_after_upload=True):
        """
            Generate temporary file called file0.jpg for uploading test
            
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_upload: True (Delete the temporary file after uploading test)

            Sending void cmd
            http://stackoverflow.com/questions/15170503/checking-a-python-ftp-connection
        """
        resp = ftpconn.cwd(share_folder)  # switch to 'Public' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))   
        
        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        src_file = ''.join(tmp_file)
        tmp_dir = os.path.dirname(src_file)
        tmpfile = os.path.join(tmp_dir, 'newfile.jpg')
        self.log.info("Rename temporary file file0.jpg to newfile.jpg")
        os.rename(src_file, tmpfile)

        self.log.info("Generate temporary file: {}".format(tmpfile))

        try:
            ftpconn.voidcmd("NOOP")  # keep connection alive
        except Exception as e:
            self.log.info("WARN: Connection might be closed, exception: {}".format(repr(e)))

        self.log.info("** Going to upload temporary file for write access test **")
        # upload file0.jpg
        retry = 3
        while retry >= 0:
            try:
                with open(tmpfile, "rb") as f:
                    resp = ftpconn.storbinary("STOR "+os.path.basename(tmpfile), f, 1048576)  # 1048576 = 1MB
                if '226' in resp:  # File successfully transferred
                    self.log.info("{} is uploaded successfully".format(os.path.basename(tmpfile)))
                    # self.log.info("Transferred speed: "+resp.split(',')[1].strip())
                else:
                    self.log.error("Upload {} failed".format(os.path.basename(tmpfile)))
            except error_perm as e:
                self.get_ftp_anonymous_access(sharename=share_folder)
                self.log.info('Available Directories:')
                ftpconn.dir()
                raise e
            except Exception as e:
                if retry == 0:
                    raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. exception: {}".format(os.path.basename(tmpfile), repr(e)))
                retry -= 1
                self.log.info("WARN: Going to retry the ftp write again, exception: {}".format(repr(e)))
            else:
                break
        ftpconn.close()
        
        #os.chdir('..') # switch to parent directory instead of staying in temporary folder
        #self.log.info("FTP Current Directory: {}".format(os.path.abspath('.')))
        if delete_after_upload:
            shutil.rmtree(os.path.dirname(tmpfile))
            self.log.info("Cleaning temporary files: {}".format(tmpfile))
        
        self.log.info("*** Write access test END ***")

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            # visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, timeout=5, retry=3):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def get_ftp_anonymous_access(self, sharename='Public'):
        readlist = None
        writelist = None
        xml_contents = self.execute('cat /etc/NAS_CFG/ftp.xml')[1]
        root = ET.fromstring(xml_contents)
        for item in root.findall('.//item'):
            share = item.find('name').text
            if share == sharename:
                readlist = item.find('read_list').text
                writelist = item.find('write_list').text
                self.log.debug("READ list: {}".format(readlist))
                self.log.debug("WRITE list: {}".format(writelist))
                break
        if writelist and 'ftp' in writelist:
            self.log.info("{} share has [anonymous r/w] access".format(sharename))
        elif readlist and 'ftp' in readlist:
            self.log.info("{} share has [anonymous read only] access".format(sharename))
        else:
            self.log.info("{} share has [anonymous none] access".format(sharename))

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

    def wait_for_updating_complete(self, interval=3):
        """
            Updating dialog wait
        """
        count = 0
        while self.is_element_visible("//div[@class=\'updateStr\']", wait_time=5):
            if count == 10:
                self.log.error("ERROR: Reach the maximum wait for updating")
                self.takeScreenShot(prefix='updating')
                break
            self.log.info("Waiting for updating to finish...")
            time.sleep(interval)  # Waiting for Saving (Updating in LT4A takes longer time than other models)
            count += 1

ftpEnDisable()