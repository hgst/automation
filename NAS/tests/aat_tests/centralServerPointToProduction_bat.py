"""
Create on Mar 30, 2016
@Author: lo_va
Objective:  To check central server point to production server.
Wiki link: http://wiki.wdc.com/wiki/Central_Server_point_to_Production
"""

from testCaseAPI.src.testclient import TestClient
check_path1 = '/var/www/rest-api/config/dynamicconfig.ini'
check_path2 = '/etc/nas/wd2go_setting'
check_url1 = 'https://web.wd2go.com'
check_url2 = 'https://www.wd2go.com'
Production = '-p'
Statging10 = '-0'
Beta = '-b'


class CentralServerProduction(TestClient):

    def run(self):

        server_base_url = self.execute('cat {} | grep SERVER_BASE_URL'.format(check_path1))[1].rsplit('=')[1].strip('"')
        base_url_flag = self.execute('cat {}'.format(check_path2))[1]
        self.log.info('Server base url is : {}'.format(server_base_url))
        self.log.info('wd2go setting is : {}'.format(base_url_flag))
        if server_base_url == check_url1 or server_base_url == check_url2 and base_url_flag == Production:
            self.log.info('Server base url is match, test PASSED!!')
        else:
            self.log.error('Server base url is not match, test FAILED!!')


CentralServerProduction()
