'''
Created on July 14, 2014

@author: Carlos Vasquez
'''
from testCaseAPI.src.testclient import TestClient
import time

invalid_names = {'1', '123456789012345', '.test', '.', 'test device', '@%&%!$!$'}
valid_names = {'wd', 'test1test2test3', 'testuser1', 'WDMyCloudEX4'}
device_desc = 'Device description'

class deviceName(TestClient):
    def run(self):
        try:
            # Store original device name
            originalDeviceName = self.get_xml_tag(self.get_device_description()[1], tag='machine_name')
            self.log.info('######################## INVALID NAME TESTS ##############################')
            self.deviceName(invalid_names)
            self.log.info('######################### VALID NAME TESTS ###############################')
            self.deviceName(valid_names)
        except Exception, ex:
            self.log.exception('Failed to set device name\n' + str(ex))
        finally:
            self.set_device_description(machine_name=originalDeviceName, machine_desc=device_desc)

    def deviceName(self, nameList=[]):
        for names in nameList:
            # Set the device name
            self.set_device_description(machine_name=names, machine_desc=device_desc)
            time.sleep(5)

            # Get the device name
            response = self.get_xml_tag(self.get_device_description()[1], tag='machine_name')
            if nameList == invalid_names:
                if response == names:
                    self.log.error('Invalid device name test: "{0}", FAIL'.format(names))
                else:
                    self.log.info('Invalid device name test: "{0}", SUCCESS'.format(names))
            else:
                if response == names:
                    self.log.info('Valid device name test: "{0}", SUCCESS'.format(names))
                else:
                    self.log.error('Valid device name test: "{0}", FAIL'.format(names))

deviceName()