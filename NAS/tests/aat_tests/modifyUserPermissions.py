"""
Created on Jan 5, 2015
Author: Carlos Vasquez
"""

from testCaseAPI.src.testclient import TestClient
from smb.smb_structs import OperationFailure
import time


private_share_name_A = ['PrivateShareA_1', 'PrivateShareA_2', 'PrivateShareA_3','PrivateShareA_4']

private_share = ['PrivateShare1', 'PrivateShare2', 'PrivateShare3', 'PrivateShare4']
share_desc = 'share volume'
   
class modifyUserPermissions(TestClient):
    
    def run(self):      
        try:
           
           self._myCleanup()
           
           # configure raid (JBOD)
           #self._configureJBOD() No need to configure raid
           
           # Create or modify share folders
           self._create_shares(private_share_name_A, public=False)
                                                  
           # Create or modify users
           self._update_user_shares()         
  
           # Modify permissions
           self._modifyUserPermissions('PrivateShareA_3')
           
        except:
            self.log.exception('An exception has occurred modifying user permissions')
    
    def _myCleanup(self): 
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        self.execute('smbif -b user1')
        
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()
        
        self.log.info('Deleting all groups ...')
        for group in range (1,65):
            group_name = 'group{0}'.format(group)
            self.delete_group(group_name) 
            
         
    def _configureJBOD(self):   
        # configure raid (JBOD) if already JBOD skip configuration
        xml_response = self.get_system_information()
        modelNumber = self.get_xml_tag(xml_response[1], 'model_number')

        if modelNumber in ('BWAZ', 'BBAZ') and (self.get_raid_mode() <> 'JBOD'):  
            self.log.info('Configuring JBOD(2-Bay...')
            self.configure_jbod()   
        elif modelNumber in ('LT4A', 'BNEZ', 'BWZE') and (self.get_raid_mode() <> 'JBOD'):
            self.log.info('Configuring JBOD(4-Bay)...')
            self.configure_jbod()
        else: 
            self.log.info('No raid configuration needed')
             
    def _create_shares(self, share_name = [], public=True):
        self.log.info('######################## Create Shares ########################')
        try:                      
            for i in range (1,5):
                self.log.info('Creating share: {0}'.format(share_name[i-1]))
                self.create_shares(share_name=share_name[i-1], description=share_desc)      
        except Exception,ex:
            self.log.info('Failed to create share\n' + str(ex))
            
    def _update_user_shares(self):
        try:   
            self.log.info('######################## Update User Shares ########################')
            
            # Create user1 to be used to access shares
            createdUser1 = self.create_user(username='user1', fullname='User One', password='fituser', is_admin=False)
            if createdUser1 == 0:
                self.log.info('user1 was created successfully')
            else:
                self.log.error('user1 was not created successfully')
                
            # Create user2 to be used to access shares
            createdUser2 = self.create_user(username='user2', fullname='User One', password='fituser', is_admin=False)
            if createdUser2 == 0:
                self.log.info('user2 was created successfully')
            else:
                self.log.error('user2 was not created successfully') 
                        
            # For user2 deny access for PrivateShareA_3
            self.create_share_access(share_name='PrivateShareA_3', username='user2', access='D')              
        except Exception,ex:
            self.log.exception('Failed to update share\n' + str(ex))      
            
    def _modifyUserPermissions(self, shareName):
        
        self.log.info('######################## Modify Permissions ########################')
        
        self.update_share_network_access(share_name='user2', public_access=False)
        
        self.create_share_access(share_name=shareName, username='user2', access='RW')
        # Clear cache
        self.log.info('Clearing cache ...')
        self.clear_cache()
        
        generated_file = self.generate_test_file(kilobytes=1)
        try:
            self.write_files_to_smb(username='user2', password='fituser', files=generated_file, share_name=shareName)
        except OperationFailure:
            self.log.error('Failed to write file to {0}'.format(shareName))
            
        
        # Check the generated file exists in share 
        try:
            exists = self.list_files_on_smb(username='user2', password='fituser', share_name=shareName)
            if 'file0.jpg' in exists:
                self.log.info('Modify Permissions(write file) test for {0}: SUCCESS'.format(shareName))
            else:
                self.log.error('Modify Permissions(write file) test for {0}: FAILED'.format(shareName))
        except OperationFailure:
            self.log.error('Failed to list files from {0}'.format(shareName))
          
        # Delete file from share
        try:
            self.delete_file_from_smb(filename='file0.jpg', username='user2', password='fituser', share_name=shareName)
        except OperationFailure:
            self.log.error('Failed to delete file from {0}'.format(shareName))
        
        # Check the deleted file does not exist in share 
        try:
            deleted_ = self.list_files_on_smb(username='user2', password='fituser', share_name=shareName)
            if 'file0.jpg' not in deleted_:
                self.log.info('Modify Permissions(delete file) test for {0}: SUCCESS'.format(shareName))
            else:
                self.log.error('Modify Permissions(delete file) test for {0}: FAILED'.format(shareName)) 
        except OperationFailure:
            self.log.error('Failed to list files from {0}'.format(shareName))
           
modifyUserPermissions()