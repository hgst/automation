""" Home page-Users- users can add user from the plus icon

    @Author: Lee_e
    
    Objective: User can be added from the user icon on the home page
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 1.05.30)
                    Lightning: Pass (FW: 2.00.40)
                    KC       : Pass (FW: 2.00.31)
              Jenkins Lightning: Pass (FW: 2.00.141)
                      Yellowstone: Pass (FW:2.00.141)
                      Glacier: Pass (FW:2.00.144)
                      Yosemite: Pass (FW:2.00.144)
                      Sprite: Pass (FW:2.00.144)
                      Kings Canyon (FW:2.00.145)
                      Aurora: Pass (FW:2.00.145)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

username = 'user1'

class UserCanAddUserFromThePlusIcon(TestClient):
    def run(self):

        user_list = self.get_user_list()
        if username in user_list:
            self.log.info('deleting user:{0}'.format(username))
            self.execute('account -d -u {0}'.format(username))

        self.add_user_from_home_page_icon(username)
        self.check_users(username)
        
    def tc_cleanup(self):
        self.delete_all_users(use_rest=False)
    
    def add_user_from_home_page_icon(self, username):
        self.log.info('*** Add User From Home page icon ***')
        retry = 3
        while retry >= 0:
            try:
                self.access_webUI()
                self.get_to_page('Home')
                time.sleep(3)
                self.wait_until_element_is_visible(self.Elements.HOME_USER_ARROW, timeout=10)
                self.click_wait_and_check(self.Elements.HOME_USER_ARROW, self.Elements.USERS_ADD_DIAG, timeout=10)
                self.input_text_check(self.Elements.USERS_CREATE_NAME, username)
                self.click_wait_and_check(self.Elements.USERS_CREATE_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
                break
            except Exception as e:
                if retry == 0:
                    self.log.exception('FAIL: exception log- {0}'.format(repr(e)))
                    self.take_screen_shot('add_user_from_home_page_icon')
                self.close_webUI()
                retry -= 1
        
    def check_users(self, username):
        self.log.info('*** Check user:{0} if be created ***'.format(username))
        user_list = self.run_on_device('account -i user', timeout=5)
        self.log.info('user_list: '+str(user_list))
        if username in user_list:
            self.log.info('{0} create successfully'.format(username))
        else:
            self.log.error('{0} could not be found'.format(username))

    def get_user_list(self):
        command = 'account -i user'
        output = self.run_on_device(command)

        return output

UserCanAddUserFromThePlusIcon()