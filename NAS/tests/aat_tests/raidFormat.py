from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.wd_wizards import RAIDWizard
import global_libraries.wd_exceptions as exception


class SetRaid(TestClient):
    def run(self):
        raid_level = int(float(self.uut.get(self.Fields.raidlevel, None)))

        if raid_level is None:
            raise exception.InvalidCommandLineOption('Must pass --{} to this test'.format(self.Fields.raidlevel))

        downgrade_to_jbod = False if self.uut.get(self.Fields.downgrade_to_jbod, None) is None else True
        extend_capacity = False if self.uut.get(self.Fields.extend_capacity, None) is None else True
        limit_capacity = False if self.uut.get(self.Fields.limit_capacity, None) is None else True
        remainder_as_spanning = False if self.uut.get(self.Fields.remainder_as_spanning, None) is None else True

        raid = RAIDWizard(self, raid_level=raid_level)

        testname = 'Setting RAID level to {}'.format(raid_level)
        self.start_test(testname)
        # Get the current RAID level
        current_raid = self.get_raid_level()

        # Make sure the starting level is correct for the selected options
        if downgrade_to_jbod and current_raid != 1:
            # Can only downgrade to JBOD from RAID 1
            self.log.warning('Current RAID is {}, but downgrade to JBOD requires RAID 1.'.format(current_raid))
            self.switch_raid_level(raid, 1)

        if extend_capacity and current_raid != 1:
            # Can only extend RAID 1
            self.log.warning('Current RAID is {}, but extend capacity requires RAID 1.'.format(current_raid))
            self.switch_raid_level(raid, 1)

        if current_raid == raid_level:
            if raid_level == 11:
                self.log.info('RAID is already formatted to JBOD. Reformatting to Spanning')
                self.switch_raid_level(raid, 12)
            else:
                self.log.info('RAID is already formatted to {}. Reformatting to JBOD'.format(raid_level))
                self.switch_raid_level(raid, 11)

        # Reset back to the expected RAID level
        raid.set_level(raid_level)
        raid.format(downgrade_to_jbod=downgrade_to_jbod,
                    extend_capacity=extend_capacity,
                    limit_capacity=limit_capacity,
                    remainder_as_spanning=remainder_as_spanning)

        current_raid = self.get_raid_level()
        if current_raid != raid_level:
            raise exception.InvalidRAIDLevel('RAID is set to {}, not {}'.format(current_raid, raid_level))

        self.pass_test(testname, 'Successfully changed RAID level')

    def switch_raid_level(self, raid, level):
        self.log.info('Switching to RAID {}'.format(level))
        raid.set_level(level)
        raid.format()
        current_raid = self.get_raid_level()
        if current_raid != level:
            raise exception.InvalidRAIDLevel('RAID is set to {}, not {}'.format(current_raid, level))

        self.log.info('RAID set to {}'.format(level))


SetRaid()