"""
Create on Mar 22, 2016
@Author: lo_va
Objective: Verify OTA firmware auto-update work.
wiki URL: http://wiki.wdc.com/wiki/OTA_auto-update_BAT
"""

import time
import requests
import json
import os
import shutil
import tarfile

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries import CommonTestLibrary as ctl
yocto_download_path = 'http://repo.wdc.com/content/repositories/projects/MyCloudOS/mycloudos'
ota_enable_config = '8a808fe35411186b015413d4504f0008'
ota_disable_config = '8a808b9754111821015413d4bf290013'
updateOk = 'updateOk'
updateReboot = 'updateReboot'
downloading = 'downloading'
updating = 'updating'
downloadFail = 'downloadFail'
updateFail = 'updateFail'
adjustWisb = 'adjustWisb'
adjustWiri = 'adjustWiri'
updateFailAfterReboot = 'updateFailAfterReboot'


class OtaAutoUpdate(TestClient):

    def run(self):
        self.yocto_init()
        self.fw_original_version = self.yocto_check()
        self.fw_update_version = self.execute('cat /Data/devmgr/db/lastpassbuild')[1]

        wddevice1 = self.execute('cat /Data/devmgr/db/wddevice.ini | grep deviceid')[1]
        wddevice2 = self.execute('cat /Data/devmgr/db/wddevice.ini | grep accesstoken')[1]
        deviceid = wddevice1.split('deviceid = ')[1]
        accesstoken = wddevice2.split('accesstoken = ')[1]
        self.url = 'http://qa1-device.remotewd1.com/device/v1/device/{0}?&access_token={1}'.format(deviceid, accesstoken)
        self.log.info('Url = {}'.format(self.url))
        try:
            wisb = self.get_values()['wisb']
            wiri = self.get_values()['wiri']
            self.log.info('Current wisb: {0}, wiri: {1}'.format(wisb, wiri))
            self.update_wisb_firmware()
            self.ota_enable()
            status = self.get_values().get('status')
            loop = 1
            while not status == (downloading or updating or updateReboot) and loop < 35:
                self.log.warning('Current status is: {}, send update wisb request again.. loop {}'.format(status, loop))
                self.update_wisb_firmware()
                time.sleep(10)
                loop += 1
                status = self.get_values().get('status')
                if loop >= 35:
                    raise Exception('OTA not start to auto update after send request {} times, update Failed'.format(loop-1))
            self.check_fail_case()

            self.wait_firmware_downloading()
            self.wait_firmware_updating()
            self.wait_device_back()
        except Exception, ex:
            self.log.exception('OTA auto-update Failed!! Exception: {}'.format(ex))

        status = self.check_fail_case()
        wiri = self.get_values().get('wiri')
        wisb = self.get_values().get('wisb')
        if wisb == wiri and status == updateOk:
            self.log.info('OTA Auto-Update Successful!!')
        else:
            self.log.error('Current update status is : {}'.format(status))
            self.log.error('wisb: {0}, wiri: {1}, wisb and wiri are not same, Update Firmware Failed!!'.format(wisb, wiri))

    def tc_cleanup(self):
        self.ota_disable()
        currentfw = self.yocto_check()
        if currentfw != self.fw_original_version:
            self.swupdateinsidenas(self.fw_original_version)
            os.chdir('..')
            for item in os.listdir('.'):
                if 'mycloudos' in item:
                    shutil.rmtree(item)
            time.sleep(5)

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            boot_finished = self.execute('ls /tmp')[1]
            fw_ver = self.execute('cat /etc/version')[1]
            if 'boot_finished' in boot_finished:
                self.log.info('Device is Ready')
                self.log.info('Firmware Version: {}'.format(fw_ver))
            else:
                raise Exception('boot_finished is not exist, device not ready!')
            return fw_ver
        except Exception as ex:
            self.log.exception('Device check failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

    def wait_firmware_downloading(self):
        max_download_time = 500
        start_time = time.time()
        status = self.check_fail_case()
        self.log.info('Status : {}'.format(status))
        while status == downloading:
            time.sleep(2)
            status = self.check_fail_case()
            self.log.info('Status : {}'.format(status))
            if time.time() - start_time > max_download_time:
                raise Exception('Timed out to waiting firmware download')

    def wait_firmware_updating(self):
        max_updating_time = 180
        start_time = time.time()
        status = self.check_fail_case()
        self.log.info('Status : {}'.format(status))
        while status == updating:
            time.sleep(2)
            status = self.check_fail_case()
            self.log.info('Status : {}'.format(status))
            if time.time() - start_time > max_updating_time:
                raise Exception('Timed out to waiting firmware updating')

    def wait_device_back(self):
        max_boot_time = 300
        start_time = time.time()
        status = self.check_fail_case()
        self.log.info('Status : {}'.format(status))
        while not status == updateOk:
            time.sleep(2)
            status = self.check_fail_case()
            self.log.info('Status : {}'.format(status))
            if time.time() - start_time > max_boot_time:
                raise Exception('Timed out waiting to boot')

    def update_wisb_firmware(self):
        data = {"firmware":{"wisb":self.fw_update_version}}
        self.log.debug('Data: {}'.format(data))
        headers = {"Content-type": "application/json"}
        r = requests.put(self.url, data=json.dumps(data), headers=headers)
        loop = 1
        while not r.json()['data']['firmware']['wisb'] == self.fw_update_version and loop < 35:
            self.log.warning('wisb not updated, send request again.. loop {}'.format(loop))
            r = requests.put(self.url, data=json.dumps(data), headers=headers)
            loop += 1
            if loop >= 35:
                raise Exception('wisb not updated after send request {} times, wisb update Failed'.format(loop-1))
        else:
            self.log.info('wisb: {} update succeed!!'.format(self.fw_update_version))

    def ota_enable(self):
        data = {"configuration": {"wisb":ota_enable_config}}
        headers = {"Content-type": "application/json"}
        requests.put(self.url, data=json.dumps(data), headers=headers)
        time.sleep(10)
        loop = 1
        while not self.get_values().get('wiri_conf') == ota_enable_config and loop < 35:
            self.log.warning('ota not enabled, send request again.. loop {}'.format(loop))
            time.sleep(10)
            r = requests.put(self.url, data=json.dumps(data), headers=headers)
            loop += 1
            if loop >= 35:
                raise Exception('ota not enabled after send request {} times, ota update Failed'.format(loop-1))
        else:
            self.log.info('ota: {} enable succeed!!'.format(ota_enable_config))

    def ota_disable(self):
        data = {"configuration": {"wisb":ota_disable_config}}
        headers = {"Content-type": "application/json"}
        requests.put(self.url, data=json.dumps(data), headers=headers)
        time.sleep(10)
        loop = 1
        while not self.get_values().get('wiri_conf') == ota_disable_config and loop < 35:
            self.log.warning('ota not disabled, send request again.. loop {}'.format(loop))
            time.sleep(10)
            r = requests.put(self.url, data=json.dumps(data), headers=headers)
            loop += 1
            if loop >= 35:
                raise Exception('ota not disabled after send request {} times, update Failed'.format(loop-1))
        else:
            self.log.info('ota: {} disable succeed!!'.format(ota_disable_config))

    def get_values(self):
        response = requests.get(self.url)
        wisb_conf = response.json()['data']['configuration']['wisb']
        wiri_conf = response.json()['data']['configuration']['wiri']
        wisb = response.json()['data']['firmware']['wisb']
        wiri = response.json()['data']['firmware']['wiri']
        status = response.json()['data']['firmware']['status']
        return {'wiri': wiri, 'wisb': wisb, 'status': status, 'wiri_conf': wiri_conf, 'wisb_conf': wisb_conf}

    def check_fail_case(self):
        status = self.get_values().get('status')
        if status == updateFail:
            self.log.warning('Update status is : {}, updated firmware FAILED!!'.format(status))
        if status == downloadFail:
            self.log.warning('Update status is : {}, downloaded firmware FAILED!!'.format(status))
        if status == updateFailAfterReboot:
            self.log.warning('Update status is : {}, updated firmware FAILED!!'.format(status))
        if status == adjustWisb:
            self.log.warning('Update status is : {}, updated firmware FAILED!!'.format(status))
        return status

    def device_reboot(self):
        max_boot_time = 300
        start = time.time()
        self.log.info('Device reboot')
        self.execute('reboot')
        while self.is_device_pingable():
            time.sleep(1)
            if time.time() - start >= max_boot_time:
                raise Exception('Device failed to power off within {} seconds'.format(max_boot_time))
        start_time = time.time()
        while not self.is_device_pingable():
            if time.time() - start_time > max_boot_time:
                raise Exception('Timed out waiting to boot')
        start_time = time.time()
        ssh_connected = False
        boot_finished = ''
        i = 1
        while not ssh_connected or 'boot_finished' not in boot_finished:
            try:
                boot_finished = self.execute('ls /tmp')[1]
                ssh_connected = True
            except Exception as ex:
                print 'ssh failed, try again {0}, {1}'.format(i, ex)
                if time.time() - start_time > 500:
                    raise Exception('Timed out waiting ssh connect.')
                i += 1
            if 'boot_finished' not in boot_finished and time.time() - start_time > 500:
                raise Exception('Time out waiting boot_finished file')
            time.sleep(1)

    def swupdateinsidenas(self, fw_original_version):
        fw_name = 'mycloudos-{0}.tgz'.format(fw_original_version)
        for dir in os.listdir('.'):
            if dir.startswith('mycloudos') and dir.endswith('.tgz'):
                os.remove(dir)
            elif 'mycloudos' in dir:
                shutil.rmtree(dir)
        os.mkdir('mycloudos')
        ctl.run_command_locally('wget -t 10 -c {0}/{1}/{2}'
                                .format(yocto_download_path, fw_original_version, fw_name))
        shutil.move('./{0}'.format(fw_name), './mycloudos/{0}'.format(fw_name))
        os.chdir('mycloudos')
        tar = tarfile.open(fw_name)
        tar.extractall()
        tar.close()
        swu_file = ''
        for file in os.listdir('.'):
            if file.endswith('.swu'):
                swu_file = file
        self.log.info('SCP transfer swupdate image to device')
        try:
            self.open_scp_connection()
            self.scp_file_to_device(files=swu_file, remote_path='/tmp', recursive=False, preserve_times=False)
        except Exception as e:
            self.log.exception('Failed to transfer image. Exception: {}'.format(repr(e)))
        self.log.info('Run update-mycloud command')
        self.execute('update-mycloud /tmp/{}'.format(swu_file))
        time.sleep(3)
        self.device_reboot()
        fw_current_ver = self.execute('cat /etc/version')[1]
        self.log.info('Current Yocto FW version is :{}'.format(fw_current_ver))


OtaAutoUpdate(checkDevice=False)
