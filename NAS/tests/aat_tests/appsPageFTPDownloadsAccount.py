"""
Created on June 16th, 2015

@author: vasquez_c

## @brief User shall able to FTP download via web UI as account
#
#  @detail 
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=32375&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=50&nTP=117472&etab=8

"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

ftp_file_path = '/shares/Public/'
ftp_user = 'ftpuser'
ftp_password = 'fituser'
ftp_filename = 'AFP-74BD.mp4'
file_md5_sum = 'b776a446fa43d7b735854de315cbb840'
rm_mp4 = 'rm /shares/Public/*.mp4'
ftp_file = "FTP_file-01"
ftp_port = '21'
Non_support_FTP_Download = ['GLCR']

class FtpDownloadAccount(TestClient):
    # Create tests as function in this class

    def run(self):
        model = self.get_model_number()
        if model in Non_support_FTP_Download:
            self.log.info('Model:{0} does not support FTP Download'.format(model))
            return

        self.log.info('User page - FTP download as Account started')

        # ensure accept EULA as admin - in case EULA hasn't been accepted
        self.access_webUI()
        self.close_webUI()
        self.ftp_download_account()
       
    def ftp_download_account(self):

        ftp_server_ip = self.uut[self.Fields.ftp_server]
        self.log.info('ftp_server_ip: {}'.format(ftp_server_ip))
        
        self.get_to_page('Apps')
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD, self.Elements.APPS_FTP_DOWNLOAD_CREATE)

        # Select to create FTP job with authentication
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE, self.Elements.APPS_FTP_DOWNLOAD_CREATE_ACCOUNT)
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_ACCOUNT, self.Elements.APPS_FTP_DOWNLOAD_CREATE_ACCOUNT_USERNAME)

        # Create FTP job
        self.log.info("Creating FTP job...")
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_JOB, ftp_file, True)
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_HOST, ftp_server_ip, True)
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_HOST_PORT, ftp_port, True)
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_ACCOUNT_USERNAME, ftp_user, True)
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_ACCOUNT_PASSWORD, ftp_password, True)

        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_SOURCE_FOLDER_PATH, 'link=ftpshare', timeout=120)
        time.sleep(5)
        self.click_wait_and_check('link=ftpshare', 'link=AFP-74BD.mp4', timeout=60)
        time.sleep(1)
    
        self.click_element_at_coordinates("link=AFP-74BD.mp4", -100, 0)

        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_SOURCE_FOLDER_OK)

        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_DESTINATION_FOLDER_PATH, 'link=Public', timeout=120)
        time.sleep(5)
       
        self.click_element_at_coordinates('link=Public', -70, 0)
        time.sleep(5)
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_DESTINATION_FOLDER_OK)

        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_JOB_OK, 'apps_ftpdownloadsGoJob1_button', timeout=120)
        self.click_element('apps_ftpdownloadsGoJob1_button')
        time.sleep(30)
       
        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path, ftp_filename)
        if file_md5_sum == ftpdownloadedfilesum:
            self.log.info('FTP file download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP file download failed, file checksum: {0}'.format(ftpdownloadedfilesum))
        
    def tc_cleanup(self):
        self.log.info('Delete downloaded folder')
        self.execute(rm_mp4)

        self.log.info('Delete FTP job')
        self.execute('ftp_download -a {} -c jobdel'.format(ftp_file))
        self.end()
FtpDownloadAccount()