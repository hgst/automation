""" Test Case Template

This template is used for new test cases. It inherits from TestClient so it does not need to worry about reporting
the test results, configuring the logger, etc.

"""

# Import the test case
import time
from testCaseAPI.src.testclient import TestClient
max_num=10
input_char='a'
invalid_username=["root", "daemon", "bin", "sys", "sync", "games", "man", "lp", "mail", "news", "uucp", "proxy", "backup", "list", "irc", "gnats", "nobody", "libuuid", "sshd", "ntp", "CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"]
section_name=["Home", "Users", "Shares", "Cloud Access", "Backups", "Storage", "Apps", "Settings"]
diag_field=['home_diagnosticsSysTemper_value', 'home_diagnosticsDrive1Temper_value', 'home_diagnosticsDrive2Temper_value', 'home_diagnosticsDrive3Temper_value', 'home_diagnosticsDrive4Temper_value', 'home_diagnosticsFanTemper_value', 'home_diagnosticsDriveStatus_value', 'home_diagnosticsDriveStatus_value']
special_char=['<', '>', ':', '"', '/', '\\', '|', '?','*', '@', '#', '+', '%']
invalid_email=['@a.a', 'a@.a', 'a@a.', 'aa.a', 'a@aa']

class webUI(TestClient):

    def run(self):
        try:        
            self.access_webUI()
            

            #self.test_space_category()
            #self.test_all_sections(section_name)
          
            #self.test_invalid_sharename(special_char)

            #self.test_invalid_email("JohnDoe", invalid_email)
            self.test_sys_uptime()

            #self.test_lang_select()
            #self.test_diag_status(diag_field)
            #self.test_web_access_timeout()
            #self.get_to_page('Users')
            #self.click_element('users_addUserSave_button')
            #self.click_element('users_createUser_link')
            #result=self.test_textbox_max('settings_generalDeviceName_text', max_num)
            #print "AAAAAAA"+str(result)
            #self.test_invliad_username('users_userName_text', 'users_addUserSave_button', invalid_username)
            
        except Exception:
            self.log.error('Test Failed: Unable to run webUI\n')

    def test_sys_uptime(self):
        try:
            self.get_to_page('Settings')
            self.wait_until_element_is_visible('settings_utilities_link')
            self.click_element('settings_utilities_link')
            self.wait_until_element_is_visible('settings_utilitiesUptime_value')
            result=self.get_text("settings_utilitiesUptime_value")
            if result=="":
                raise Exception('Test Failed: no value in system up time')
            else:
                self.log.info('System Up time is checked')

            
        except Exception:
            self.log.exception('Test Failed: Unable to test system uptime\n') 
    
    def test_space_category(self):
        try:

            self.get_to_page('Home')
            self.click_element('vol_capacity')
            time.sleep(6)
            n=1
            while n<6:
                self.wait_until_element_is_visible("home_legendLabel"+str(n)+"_value")
                #text_result=self.get_text("home_legendLabel1_value")
                text_result=self.get_text("home_legendLabel"+str(n)+"_value")
                if text_result=="":
                    raise Exception('Test Failed: Space category breakdown has no value or not exist')
                else:
                    self.log.info('Space category break down has value and exist')
                n=n+1
                    
            self.click_element('home_capacityClose1_button')
            
        except Exception:
            self.log.exception('Test Failed: Failed to test space category breakdpwn\n')  
    
    def test_invalid_email(self, name, email):
        try:
            self.get_to_page('Users')
            self.click_element('users_createUser_link')
            result=self.get_user(name)
            if result[0]==404:
                pass
            else:
                self.delete_user(name)
            for x in email:

                self.input_text("users_userName_text", name, False) 
                self.input_text("users_mail_text", x, False)
                self.click_element('users_addUserSave_button')
                time.sleep(1)
                result=self.is_element_visible('users_addUserSave_button')
                if result==True:
                    pass
                else:
                    raise Exception('Test Failed: invalid email format test failed')  
            self.click_element('users_addUserCancel1_button')
            
        except Exception:
            self.log.exception('Test Failed: Failed to test invalid email format\n')   

    def test_web_access_timeout(self):
        try:
            self.get_to_page('Settings')
            time.sleep(1)
            self.wait_until_element_is_visible('settings_general_link')
            self.click_element('settings_general_link')   
            self.click_element('settings_generalTimeout_select')
            self.click_element('settings_generalTimeoutLi1_select')
            script_timeout=0
            while True:
                    if self.is_element_visible('login_login_button'):
                        self.log.info('Web access timeout and is logout by the system')
                        break
                    elif script_timeout==1:
                        time.sleep(3)
                        raise Exception('Test Failed: Exceed web access timeout, but not logout') 
                    else:
                        self.log.info('Waiting for Web access timeout')
                        time.sleep(300)
                        script_timeout=1
                        
            self.close_webUI()
            self.access_webUI()
            
            self.get_to_page('Settings')
            time.sleep(1)
            self.wait_until_element_is_visible('settings_general_link')
            self.click_element('settings_general_link')   
            self.click_element('settings_generalTimeout_select')
            self.drag_and_drop_by_offset('css=div.jspDrag', 0,50)
            self.click_element('settings_generalTimeoutLi11_select')
            script_timeout=0
            while True:
                    if self.is_element_visible('login_login_button'):
                        self.log.info('Web access timeout and is logout by the system')
                        break
                    
                    elif script_timeout==1:
                        time.sleep(3)
                        raise Exception('Test Failed: Exceed web access timeout, but not logout') 
                    
                    else:
                        self.log.info('Waiting for Web access timeout')
                        time.sleep(900)
                        script_timeout=1
            self.close_webUI()
            self.access_webUI()
            
        except Exception:
            self.log.exception('Test Failed: Failed to test web access timeout\n')             
                          
    
    def test_lang_select(self):
        try:
            n=1
            while (n<19):

                self.get_to_page('Settings')
                time.sleep(1)
                x=(n/5)*50
                self.click_element('settings_generalLanguage_select')
                self.drag_and_drop_by_offset('css=div.jspDrag', 0,x)
                self.click_element('settings_generalLanguageLi'+str(n)+'_select')
                time.sleep(1)
                self.click_element('settings_generalLanguageSave_button')
                time.sleep(4)
                n=n+1
            ''' Change the language back to ENG'''
            self.get_to_page('Settings')
            time.sleep(1)
            self.click_element('settings_generalLanguage_select')
            self.click_element('settings_generalLanguageLi1_select')
            time.sleep(1)
            self.click_element('settings_generalLanguageSave_button')  
            
        except Exception:
            self.log.exception('Test Failed: Failed to select different language\n')            

    
    def test_invalid_sharename(self, name):
        try:
            self.get_to_page('Shares')
            self.click_element('shares_createShare_button')
            time.sleep(1)
            for x in name:
                self.input_text('shares_shareName_text',"abc"+str(x), False)
                self.click_element('shares_createSave_button')
                
                time.sleep(1)
                self.click_element('popup_ok_button')
    
        except Exception:
            self.log.exception('Test Failed: Unable to test invalid share names\n')
    
    
    
    def test_diag_status(self, field):
        try:
            self.get_to_page ('Home')
            self.click_element('smart_info')
            for x in diag_field:
                text_result=self.get_text(x)
                if text_result=="":
                    raise Exception('Test Failed: Diagnostic Health Value is none')
                else:
                    self.drag_and_drop_by_offset('css=div.jspDrag', 0, 10)        
    
        except Exception:
            self.log.exception('Test Failed: Unable to check all diagnostic status\n')
    
    def test_textbox_max(self, locator, max):
        try:
            input_value=""
            for i in range(max+1):
                input_value+=input_char
                
            self.input_text(locator , input_value, False)
            text_result=self.get_value(locator)
   
            if len(text_result)==max:
                return True
                
            else:
                return False
                
        except Exception:
            self.log.exception('Test Failed: fail to test max input character for text box\n')
    
    def test_all_sections(self, section_name):
        try:
            for x in section_name:
                self.get_to_page(x)
                time.sleep(1)
                          
        except Exception:
            self.log.exception('Test Failed: fail to click all sections\n')

    def test_system_health(self):
        try:
            for x in section_name:
                self.get_to_page(x)
                time.sleep(1)
                          
        except Exception:
            self.log.exception('Test Failed: fail to click all sections\n')
    
    
    def test_invliad_username(self, locator, create_ln, name ):
        try:
            self.get_to_page('Users')
            self.click_element('users_createUser_link')
            for x in name:
                 self.input_text(locator, x, False)
                 self.click_element(create_ln, False)
                 time.sleep(1)
                 result=self.is_element_visible(create_ln)
                 if result==True:
                     pass
                 else:
                     raise Exception('Test Failed: Reserved name should not creat successfully')
            self.click_element('users_addUserCancel1_button')   
        except Exception:
            self.log.exception('Test Failed: fail to test invalid creation\n')
   
         
webUI()


