""" BAT- AD Users Add/Remove

    @Author: Lee_e

    Objective: Validate that Active Directory users are instantly updated when added/removed or enabled/disabled
    Automation: Full

    Supported Products:
        All

    Test Status:
              Local Lightning : PASS (FW: 2.10.278)
                    KC        : PASS (FW: 2.10.259)
                    Sprite    : PASS (FW: 2.10.288)

              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.282)
                              : Yosemite     - PASS (FW: 2.10.273)
                              : Kings Canyon - PASS (FW: 2.10.284)
                              : Sprite       - PASS (FW: 2.10.276)
                              : Zion         - non - support
                              : Aurora       - PASS (FW: 2.10.289)
                              : Yellowstone  - PASS (FW: 2.10.281)
                              : Glacier      -  non - support
                              : Grand Teton  -  non - support

"""
from testCaseAPI.src.testclient import TestClient
import time
import requests
import base64

local_user = 'user1'

class ADUsersAddRemove_bat(TestClient):
    def run(self):
        global dns_server_org_ip
        dns_server_org_ip = self.save_dns_server_ip()
        model = self.get_model_number()
        non_support = ['GLCR', 'BZVM', 'BWVZ']
        if model in non_support:
            self.log.info('Model is {0} in not support iSCSI list{1}'.format(model, non_support))
            return
        self.check_update_user()
        self.log.info('Default AD UserName:{0}, Default AD Password:{1}, AD Domain Name:{2}, AD Server:{3}'
                      .format(self.uut[self.Fields.default_ad_username], self.uut[self.Fields.default_ad_password],
                              self.uut[self.Fields.ad_domain_name], self.uut[self.Fields.ad_server]))
        self.ad_configure(status='ON')
        self.check_user_list(user=local_user)
        self.webUI_ad_user_login()

    def tc_cleanup(self):
        global dns_server_org_ip
        self.check_update_user(delete=True)
        self.disable_AD_cgi()
        #self.reset_dns_server_ip(dns_server_ip=dns_server_org_ip)

    def ad_configure(self, status='ON'):
        self.get_to_page('Settings')
        self.click_wait_and_check(self.Elements.NETWORK_BUTTON, self.Elements.SETTINGS_NETWORK_AD_SWITCH)
        self.configure_active_directory(status=status,
                                        AD_USERNAME=self.uut[self.Fields.default_ad_username],
                                        AD_PASSWORD=self.uut[self.Fields.default_ad_password],
                                        AD_DOMAIN=self.uut[self.Fields.ad_domain_name],
                                        AD_DNS_SERVER=self.uut[self.Fields.ad_server])

    def configure_active_directory(self, status=None, AD_USERNAME=None, AD_PASSWORD=None, AD_DOMAIN=None, AD_DNS_SERVER=None):
        status_dict = {'ON': '1', 'OFF': '0'}
        try:
            self.check_attribute('settings_networkADS_switch', expected_value=status_dict[status], attribute_type='value')
        except Exception:
            if status == 'ON':
                self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_SWITCH, self.Elements.SETTINGS_NETWORK_AD_APPLY)
            else:
                self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_SWITCH, self.Elements.SETTINGS_NETWORK_AD_CONFIGURE, visible=False)

        if status == 'ON':
            if not self.is_element_visible(self.Elements.SETTINGS_NETWORK_AD_APPLY):
                self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_CONFIGURE, self.Elements.SETTINGS_NETWORK_AD_APPLY)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_USERNAME, AD_USERNAME)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_PASSWORD, AD_PASSWORD)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_DOMAIN_NAME, AD_DOMAIN)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_DNS_SERVER, AD_DNS_SERVER)
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_APPLY, self.Elements.UPDATING_STRING, timeout=60)
            self.log.info('Message shows: '+str(self.get_text(self.Elements.SETTINGS_NETWORK_AD_RESULT_DIAG)))
            if self.get_text(self.Elements.SETTINGS_NETWORK_AD_RESULT_DIAG) == 'Successful':
                self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_OK, self.Elements.SETTINGS_NETWORK_AD_CONFIGURE)
            else:
                self.log.exception('Config AD with wrong message:{0}'.format(self.get_text(self.Elements.SETTINGS_NETWORK_AD_RESULT_DIAG)))

    def check_user_list(self, user):
        self.get_to_page('Users')
        self.click_wait_and_check(self.Elements.USERS_BUTTON, 'css=div.uName', timeout=10)
        users = self.get_text('css=.LightningSubMenu.userMenu.userMenuList')
        self.log.info('user list: {0}'.format(users))
        ad_username = self.uut[self.Fields.ad_domain_name].replace('.COM', '\\') + self.uut[self.Fields.default_ad_username]

        sub_test_name_1 = 'Check AD users are displayed in the format of Domain\[username]'
        self.start_test(sub_test_name_1)
        if ad_username in users:
            self.pass_test(sub_test_name_1)
        else:
            self.fail_test(sub_test_name_1)

        sub_test_name_2 = 'Check local users are still displayed without a Domain prefix'
        self.start_test(sub_test_name_2)
        if user in users:
            self.pass_test(sub_test_name_2)
        else:
            self.fail_test(sub_test_name_2)
        self.close_webUI()

    def check_update_user(self, delete=False):
        self.log.info('*** get user list ***')
        users = self.execute('account -i user')[1].split()

        if delete:
            self.log.info('Delete user: {0}'.format(local_user))
            self.run_on_device('account -d -u {0}'.format(local_user))
        elif local_user in users:
            self.log.info('Modify user {0}'.format(local_user))
            self.modify_user_account(user=local_user, passwd='welc0me')
        else:
            self.log.info('Add user {0}'.format(local_user))
            add_user_command = 'account -a -u {0} -p {1}'.format(local_user, 'welcome')
            self.run_on_device(add_user_command)

    def webUI_ad_user_login(self):
        """
        :param username: user name you'd like to login
        :param userpwd: user password for user name account
        """
        self.start_test('webUI_ad_user_login')
        ad_username = self.uut[self.Fields.ad_domain_name].replace('.COM', '\\') + self.uut[self.Fields.default_ad_username]
        self.log.info('ad_username: {0}'.format(ad_username))
        try:
            self.access_webUI(do_login=False)
            self.input_text_check(self.Elements.LOGIN_USERNAME, ad_username, do_login=False)
            self.input_text_check(self.Elements.LOGIN_PASSWORD, self.uut[self.Fields.ad_password], do_login=False)
            self.click_wait_and_check(self.Elements.LOGIN_BUTTON, visible=False)
            self._sel.check_for_popups()
            self.pass_test('webUI_ad_user_login as [{}] pass'.format(ad_username))
            self.close_webUI()
        except Exception, ex:
            self.fail_test('webUI_ad_user_login with exception : {0}'.format(repr(ex)))
            self.take_screen_shot('ad_login_fail')

    def save_dns_server_ip(self):
        dns_server_ip = self.run_on_device('cat /etc/resolv.conf').split('\n')
        self.log.info('Original DNS Sever IP: {0}'.format(dns_server_ip))
        return dns_server_ip

    def reset_dns_server_ip(self, dns_server_ip=None):
        dns_server_ip = self.run_on_device('cat /etc/resolv.conf').split('\n')
        self.log.info('Current DNS Sever IP: {0}'.format(dns_server_ip))

        self.log.info('Back to default DNS server')
        waiting_string = '/ #'
        if self.uut[self.Fields.serial_server] is None:
            self.log.warning('Could not set network port. No serial connection.')
            return False

        self.log.info('###### Open serial connection ######')
        cmd = 'setNetworkDhcp.sh ifname=bond0 ; setNetworkDhcp.sh ifname=egiga0'
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))

        # reset uut network config
        self.serial_wait_for_string(waiting_string, 30)
        self.serial_write(cmd)
        time.sleep(10)
        dns_server_current_ip = self.run_on_device('cat /etc/resolv.conf').split('\n')
        self.log.info('dns server reset to ip: {0}'.format(dns_server_current_ip))
        '''
        for i in range(0, len(dns_server_current_ip)):
            self.log.info('dns server current ip: {0}'.format(dns_server_current_ip[i]))
            if
        '''

    def config_active_directory_cgi(self):
        password_encode = base64.b64encode(self.uut[self.Fields.default_ad_password])
        cmd = 'cgi_set_ads&f_enable=1&f_name={0}&f_pw={1}&f_realm_name={2}&f_dns1={3}'\
            .format(self.uut[self.Fields.default_ad_username], password_encode, self.uut[self.Fields.ad_domain_name], self.uut[self.Fields.ad_server])
        head = 'account_mgr.cgi'
        self.send_cgi(cmd, head)

    def disable_AD_cgi(self):
        self.log.info('Disable AD CGI')
        head ='account_mgr.cgi'
        cmd = 'cgi_set_ads&f_enable=0&f_name=&f_pw=&f_realm_name=&f_dns1='
        self.send_cgi(cmd, head)

    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: ' + login_url + 'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))

        return r.text.encode('ascii', 'ignore')

ADUsersAddRemove_bat()
