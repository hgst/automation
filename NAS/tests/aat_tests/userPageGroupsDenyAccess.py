"""
title           :userPageGroupsDenyAccess.py
description     :To verify if a group can be authorized 'Deny Access' access to a given folder
author          :yang_ni
date            :2015/03/30
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time


class userPageGroupsDenyAccess(TestClient):

    def run(self):
        if self.get_model_number() in ('GLCR',):
            self.log.info("=== Glacier de-feature does not support group feature, skip the test ===")
            return

        testname = 'User Page - Groups - Share Access - Deny Access'
        self.start_test(testname)

        try:
            #Delete all the users, groups and shares
            self.setup()

            # Create a user
            #self.create_user(username='user1')

            # Create a group 
            self.create_groups(group_name='group1', number_of_groups=1)
            
            # Create a group and add 'user1' into this group
            #self.create_groups(group_name='group1', number_of_groups=1, memberusers='user1')

            # Create a share.
            self.create_shares(share_name='share', number_of_shares=1)

            # Change share1 to private, share must be set to private instead of public. Toggle the Public switch off
            self.update_share_network_access(share_name='share', public_access=False)

            #Assigns group1 to share1 with the 'Deny' access level
            self.assign_group_to_share(group_name='group1', share_number=1, access_type='d')

            #Check from Web UI if 'Deny access' authorization of share is given to group1
            time.sleep(20)
            self.get_to_page('Users')
            time.sleep(5)
            self.click_button('users_group_button')
            time.sleep(5)
            self.click_button('users_group_group1')
            text = self.get_text("//div[@id=\'group_sharelist\']/ul/li[4]/div[4]")

            if text == 'Deny Access':
                self.pass_test(testname, 'Successfully authorizes deny access to group1')
            else:
                self.fail_test(testname, 'Failed to authorizes deny access to group1')

        except Exception as e:
            self.fail_test(testname, 'Failed to authorizes deny access to group1, error: {}'.format(e))

# # @Clean up all the users/shares/groups before test starts
    def setup(self):
        #Delete all the users first
        self.log.info('Deleting all users ...')
        self.delete_all_users()
        time.sleep(3)
        self.log.info('Deleting all groups ...')
        self.delete_all_groups()
        time.sleep(3)
        self.log.info('Deleting all shares ...')
        self.delete_all_shares()


# This constructs the userPageGroupsDenyAccess() class, which in turn constructs TestClient() which triggers the userPageGroupsDenyAccess.run() function
userPageGroupsDenyAccess()