from testCaseAPI.src.testclient import TestClient
import time


class InvalidVlanIdValues(TestClient):
    # Create tests as function in this class
    def run(self):

        try:
            self.log.info('Invalid VLAN ID started')
            self.invalid_vlan_id()
        except Exception, ex:
            self.log.exception('Failed to complete Invalid VLAN ID \n' + str(ex))
        finally:
            self.log.info('Reset unit back to DHCP')
            self.reset_network_config_dhcp()

    def invalid_vlan_id(self):
        self.access_webUI()
        self.get_to_page('Settings')
        self.wait_until_element_is_clickable('settings_network_link')
        self.click_element('settings_network_link')
        self.wait_until_element_is_clickable('settings_networkLAN0IPv4Static_button')
        self.click_element('settings_networkLAN0IPv4Static_button')
        self.wait_until_element_is_clickable('settings_networkIPv4Next1_button')
        self.click_element('settings_networkIPv4Next1_button')
        self.wait_until_element_is_clickable('settings_networkIPv4Next2_butotn')
        self.click_element('settings_networkIPv4Next2_butotn')
        self.wait_until_element_is_clickable('css = #settings_networkVLAN_switch+span .checkbox_container')
        self.click_element('css = #settings_networkVLAN_switch+span .checkbox_container')
        time.sleep(3)
        self.input_text('settings_networkVLANID_text', '5000', do_login=True)
        self.click_element('settings_networkIPv4Next3_button')
        self.wait_until_element_is_clickable('id=popup_ok_button')
        self.click_element('id=popup_ok_button')
        time.sleep(3)
        self.input_text('settings_networkVLANID_text', 'abc', do_login=True)
        self.click_element('settings_networkIPv4Next3_button')
        self.wait_until_element_is_clickable('id=popup_ok_button')
        self.click_element('id=popup_ok_button')
        self.click_element('settings_networkIPv4Cancel3_button')
        self.log.info('Invalid VLAN ID values were properly rejected. Test has PASSED')


InvalidVlanIdValues()
