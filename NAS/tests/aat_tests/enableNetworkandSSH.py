"""
Create on Jan 8, 2016
@Author: lo_va
Objective:  Verify network is enabled by default - ip address is acquired automatically
            Verify ssh is enabled by default, and user can login to ssh successfully
            Refer detail on KAM-32
wiki URL: 	http://wiki.wdc.com/wiki/Enable_network_and_SSH
"""

import time

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
kamino_waiting_string = '~#'


class EnableNetworkandSSH(TestClient):

    def run(self):
        self.yocto_init()
        fw = self.yocto_check()
        if '4.0.0' in fw:
            self.log.info('Enable Network and SSH test PASSED!!')
        else:
            self.log.error('Enable Network and SSH test FAILED!!')


    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
            return fw_ver
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.open_serial_connection()
            self.serial_wait_for_string(kamino_waiting_string, 30)
            self.serial_write('ifconfig egiga2')
            self.serial_wait_for_string('inet addr:', 20)
            read_result = self.serial_read()
            self.log.info('read result: {0}'.format(read_result))
            return read_result


    def open_serial_connection(self):
        # Open serial connection
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))

EnableNetworkandSSH(checkDevice=False)
