
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import requests
import time

waiting_string = '/ #'

class PowerSchedule(TestClient):

    def run(self):
        self.log.info('******* Test Start ********')
        self.log.info('********* Testing without NTP time **********')
        self.config_ntp(enable=0)
        self.get_mac_address()
        
        self.check_ntp_config_status(status='false')
        fw_version = self.get_fw_version()
        # check system if can on/off corresponding to each day
        self.turn_OnOff_by_schedule(fw_version)
        # check system if can on next day as schedule set
        
        self.day_off(fw_version)
        self.log.info('********* NTP time Enable **********')      
        self.config_ntp(enable=1)
        time.sleep(10)
        self.check_ntp_config_status(status='true')
        self.log.info('****** Test End ******')        
    
    def tc_cleanup(self):
        self.turn_on_by_WOL()
        fw_version = self.get_fw_version()
        self.set_ssh()
        self.reset_schedule(fw_version)
        self.config_power_schedule(fw_version, enable=0)
        self.config_ntp(enable=1)
        
    def config_ntp(self, enable):
        self.log.info('NTP enable: {0}'.format(enable))
        ntp_cmd = 'cgi_ntp_time&f_ntp_enable=%s&f_ntp_server=' % enable
        self.send_cgi(ntp_cmd)
        
    def check_ntp_config_status(self, status):
        result_code, result = self.get_date_time_configuration()
        self.log.debug('result = %s' % result)
        ntpservice_status = self.get_xml_tag(xml_content=result, tag='ntpservice')
        self.log.info('ntpservice = %s' % ntpservice_status)
        if ntpservice_status != status:
            self.log.error('Failed to switch NTP service : %s' % ntpservice_status)  
        else:
            self.log.info('PASS: NTP service is : %s' % ntpservice_status)

    def config_power_schedule(self, fw_version, enable):
        self.log.info('power schedule enable:{0}'.format(enable))
        if int(fw_version) == 1:
            cmd = 'cgi_power_off_sch&f_power_off_enable='
        else:
            cmd = 'cgi_power_sch_enable&enable='
        power_sch_config_cmd = cmd + str(enable)
        self.send_cgi(power_sch_config_cmd)

    def power_sch_cgi(self, on, off, set, fw_version):
        if int(fw_version) == 1:
            power_sch_cgi_cmd = 'cgi_power_off_sch&f_power_off_enable=1&schedule=' + ','.join(on) + '&off_schedule=' + ','.join(off)
        else:
            power_sch_cgi_cmd = 'cgi_power_sch&enable=1&schedule=' + ','.join(set)
        self.send_cgi(power_sch_cgi_cmd)        

    def reset_schedule(self, fw_version):
        self.log.info('Reset power schedule')
        if int(fw_version) == 1:
            on = ['0 0 0']*7
            off = ['0 23 59']*7
            reset_schedule_cmd = 'cgi_power_off_sch&f_power_off_enable=1&schedule=' + ','.join(on) + '&off_schedule=' + ','.join(off)
        else:
            set = ['1,1,1,1,1,1,1,1,1,1,1,1']*7
            reset_schedule_cmd = 'cgi_power_sch&enable=1&schedule=' + ','.join(set) 
        self.send_cgi(reset_schedule_cmd)
        
    def send_cgi(self, cmd):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/system_mgr.cgi?cmd='.format(self.uut[Fields.internal_ip_address])
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))
        return r.text.encode('ascii', 'ignore')

    def get_fw_version(self):
        result = self.get_firmware_version_number()
        version = result.split('.')[0]
        self.log.info('Head of firmware is :' + version)
        
        return version

    def datetime_update(self, days, hour, on=False):
        self.log.debug('********** always testing 20150111-20150117 and set min = 56 **********')
        year = '2015'
        month = '1'
        day = '1'+str(days)
        min = '56'
        if on:
            min = '54'
        self.log.info('*** testing {0}0{1}{2} and set min: {3} ***'.format(year, month, day, min))
        datetime_update_cmd = 'cgi_manual_time&f_year=%s&f_month=%s&f_day=%s&f_hour=%s&f_min=%s&f_sec=0' % (year, month, day, hour, min)
        self.send_cgi(datetime_update_cmd)

    def turn_on_by_WOL(self):
        self.log.info('********** state = turn_on_by_WOL *********')
        retry = 2
        while retry > 0:
            try:
                time.sleep(5)
                self.send_magic_packet()
                time.sleep(3)
                start_time = time.time()
      
                while not self.is_ready():
                    self.log.info('ping system')
                    time.sleep(20)
                    if time.time() - start_time > 380:
                        raise Exception('Timed out for waiting to boot by WoL:{0} seconds '.format(time.time()-start_time))  
                if self.is_ready():
                    self.log.info(' WoL spend time: {0} seconds'.format(time.time()-start_time))  
                    self.log.info('PASS: Wake on Lan test Success')   
                    break
            except Exception as e:
                self.log.debug('Retry {0} times to WoL'.format(2-retry))
                retry -= 1

    def turn_OnOff_by_schedule(self, fw_version):
        self.log.info('******** On and Off accordingly on each day **********')
        on_set = []
        off_set = []
        set = []
        if int(fw_version) == 1:
            on_set = ['1 10 0']*7
            off_set = ['1 12 0']*7
        else:
            "AM 12-2, AM 2-4, AM 4-6, AM 6-8, AM 8-10, AM 10-12, PM 12-2, PM 2-4, PM 4-6, PM 6-8, PM 8-10, PM 10-12"
            set = ['1,1,1,1,0,1,0,1,1,1,1,1']*7
        self.power_sch_cgi(on_set, off_set, set, fw_version)
        on = 9
        off = 11
        for i in range(1, 8):
            map = {1: 'Sun', 2: 'Mon', 3: 'Tue', 4: 'Wed', 5: 'Thu', 6: 'Fri', 7: 'Sat'}
            self.log.info('Testing - {0} : {1}'.format(i, map[i]))
            self.datetime_update(days=i, hour=on, on=True)

            time.sleep(1)
            self.log.info('shutdown UUT')
            self.serial_shutdown(timeout=self.uut[self.Fields.shutdown_time])
            start_time = time.time()
            while not self.is_ready():
                self.log.info('off---> power on to ping system')
                time.sleep(20)
                if time.time() - start_time >= (420+self.uut[self.Fields.reboot_time]):
                    raise Exception('Timed out waiting to boot: '+str(time.time()-start_time))
            
            if self.is_ready():    
                self.log.info('PASS: power schedule to on')
            
            self.datetime_update(days=i, hour=off)
            start_time = time.time()
            while self.is_device_pingable():
                self.log.info('on --> power off to ping system')
                time.sleep(20)
                if time.time() - start_time >= (300+self.uut[self.Fields.shutdown_time]):
                    raise Exception('Device failed to power off: '+str(time.time()-start_time))
            #time.sleep(5)
            if not self.is_device_pingable():
                self.log.info('PASS: power schedule to off')
                self.turn_on_by_WOL()
        
    def day_off(self, fw_version):
        self.log.info('******** state: test day off ********')
        on_set = []
        off_set = []
        set = []
        if int(fw_version) == 1:
            on_set = ['0 0 0']*7
            off_set = ['0 23 59']*7
            on_set[1] = '1 10 0'
            off_set[0] = '1 10 0'
        else:
            "AM 12-2, AM 2-4, AM 4-6, AM 6-8, AM 8-10, AM 10-12, PM 12-2, PM 2-4, PM 4-6, PM 6-8, PM 8-10, PM 10-12"
            set = ['1,1,1,1,1,1,1,1,1,1,1,1']*7
            set[0] = '1,1,1,1,1,0,0,0,0,0,0,0'
            set[1] = '0,0,0,0,0,1,1,1,1,1,1,1'   
        self.power_sch_cgi(on_set, off_set, set, fw_version)
        off = 9
        on = 9       
        self.datetime_update(days=1, hour=off)
     
        self.log.info('Loop until the pings stop')
        start_time = time.time() 
        while self.is_device_pingable():
            self.log.info('****** system prepare to off ******')
            time.sleep(20)
            if time.time() - start_time >= (300+self.uut[self.Fields.shutdown_time]):
                raise Exception('Device failed to power off:' + str(time.time()-start_time))
 
        self.log.info('******** Now, wait for the system to come back up. testing by auto set to next day ********')
        self.turn_on_by_WOL()

        self.datetime_update(days=2, hour=on, on=True)
        self.log.debug('shutdown UUT')
        self.serial_shutdown(timeout=self.uut[self.Fields.shutdown_time])
        start_time = time.time() 
        while not self.is_ready():
            self.log.info('****** system prepare to on ******')
            time.sleep(20)
            if time.time() - start_time >= (420+self.uut[self.Fields.reboot_time]):
                raise Exception('Device failed to power on')
            
        if self.is_ready():
            self.log.info('PASS :day off testing ')

    def get_mac_address(self):
        self.log.info('mac_address:{0} ip:{1}'.format(self.uut[self.Fields.mac_address], self.uut[self.Fields.internal_ip_address]))
        mac_address = ''.join(self.get_xml_tags(self.get_system_information(), 'mac_address'))
        self.log.info('mac_address rest:{0}'.format(mac_address))
        if mac_address != self.uut[self.Fields.mac_address]:
            self.uut[self.Fields.mac_address] = mac_address
        self.log.info('mac_address :{0} ip:{1}'.format(self.uut[self.Fields.mac_address], self.uut[self.Fields.internal_ip_address]))

    def serial_shutdown(self, timeout=120):
        # before really do shutdown command, total need 120 seconds: update set min from 56 to 54
        # Open serial connection
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.log.info('read result: {0}'.format(read_result))

        self.serial_wait_for_string(waiting_string, 30)
        start = time.time()
        self.serial_write('/usr/sbin/shutdown.sh')
        self.log.info('do command: /usr/sbin/shutdown.sh')

        # Loop until the pings stop
        while self.is_device_pingable():
            time.sleep(0.5)
            if time.time() - start >= timeout:
                raise Exception('Device failed to power off within {0} seconds'.format(timeout))

PowerSchedule()