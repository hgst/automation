"""
Created on July 14, 2014

@author: tran_jas

"""

from testCaseAPI.src.testclient import TestClient


class DeviceID(TestClient):
    def run(self):
        # noinspection PyPep8
        media_server_command = 'cat /usr/local/twonky/resources/devicedescription-custom-settings.txt | grep TMS_PNPX_X_HARDWAREWID'
        hardware_id_command = 'grep -e "X_hardwareId" /etc/upnpNas_conf.xml'
        model_id_command = 'grep -e "X_modelId" /etc/upnpNas_conf.xml'
        container_id_command = 'grep -e "X_containerId" /etc/upnpNas_conf.xml'

        vendor_id = 'VEN_011A'

        # Get device's model then set ID values accordingly
        model_number = self.get_model_number(use_rest=True)
        self.log.info('device model: {0}'.format(model_number))

        self.log.info('Verifying ID for %s' % str(model_number))

        default_media_server_id = self.uut[self.Fields.default_media_server_id]
        x_hardware_id = self.uut[self.Fields.x_hardware_id]
        x_model_id = self.uut[self.Fields.x_model_id]
        x_container_id = self.uut[self.Fields.x_container_id]

        # Verify Media Server ID
        returnvalue = self.execute(media_server_command)

        a, b, c = returnvalue[1].split("#")

        testname = 'Media Server ID'
        self.start_test(testname)
        if c == default_media_server_id:
            # print 'Correct Media Server ID: ', c
            self.pass_test(testname, 'Correct Media Server ID: {}'.format(c))
        else:
            # print 'Media Server ID does not match: ', c
            self.fail_test(testname, 'Media Server ID does not match: {}'.format(c))

        # Verify Hardware ID and Vendor ID
        returnvalue = self.execute(hardware_id_command)

        a, b, c = returnvalue[1].split('>')
        d, e = b.split('<')
        f, g, h = d.split('&')

        testname = 'Hardware ID'
        self.start_test(testname)
        if d == x_hardware_id:
            self.pass_test(testname, 'Correct Hardware ID: {}'.format(d))
        else:
            self.fail_test(testname, 'Hardware ID does not match: {}'.format(d))

        testname = 'Vendor ID'
        self.start_test(testname)
        if f == vendor_id:
            self.pass_test(testname, 'Correct Vendor ID: {}'.format(f))
        else:
            self.fail_test(testname, 'Vendor ID does not match: {}'.format(f))

        # Verify Model ID
        testname = 'Model ID'
        self.start_test(testname)
        returnvalue = self.execute(model_id_command)

        a, b = returnvalue[1].split('{')
        c, d = b.split('}')
        if c == x_model_id:
            self.pass_test(testname, 'Correct Model ID: {}'.format(c))
        else:
            self.fail_test(testname, 'Model ID does not match: {}'.format(c))

        # Verify Container ID
        testname = 'Container ID'
        self.start_test(testname)
        returnvalue = self.execute(container_id_command)

        a, b = returnvalue[1].split('{')
        c, d = b.split('}')
        if c == x_container_id:
            self.pass_test(testname, 'Correct Container ID: {}'.format(c))
        else:
            self.fail_test(testname, 'Container ID does not match: {}'.format(c))

DeviceID()
