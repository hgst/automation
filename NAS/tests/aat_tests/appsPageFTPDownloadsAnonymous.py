"""
Created on June 16th, 2015

@author: vasquez_c

## @brief User shall able to FTP download via web UI as anonymous
#
#  @detail 
#     http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=32375&execView=execDetails&view=details&tdetab=3&pltab=steps&pId=50&nTP=117472&etab=1

"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

ftp_file_path_20 = '/shares/Public/FTPTest/'
ftp_filename_2 = 'diaryofacamper_raw.mp4'
file_md5_sum_2 = '185236549de96b693b6cd7b2afb25427'
rm_ftp_test = 'rm -r /shares/Public/FTPTest'
ftp_folder = "FTP_folder-345"
Non_support_FTP_Download = ['GLCR']

class FtpDownload(TestClient):

    def run(self):
        model = self.get_model_number()
        if model in Non_support_FTP_Download:
            self.log.info('Model:{0} does not support FTP Download'.format(model))
            return

        self.log.info('User page - FTP download as anonymous')

        # ensure accept EULA as admin - in case EULA hasn't been accepted
        self.access_webUI()
        self.close_webUI()
        self.ftp_download_anonymous()

    def ftp_download_anonymous(self):

        ftp_server_ip = self.uut[self.Fields.ftp_server]
        self.log.info('ftp_server_ip: {}'.format(ftp_server_ip))

        self.access_webUI()

        self.get_to_page('Apps')
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD, self.Elements.APPS_FTP_DOWNLOAD_CREATE)

        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE, self.Elements.APPS_FTP_DOWNLOAD_CREATE_ACCOUNT)

        self.log.info('FTP download a folder')
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_JOB, ftp_folder, True)
        self.input_text(self.Elements.APPS_FTP_DOWNLOAD_CREATE_HOST, ftp_server_ip, True)
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_SOURCE_FOLDER_PATH, 'link=Public', 120)
        time.sleep(5)
        self.click_wait_and_check('link=Public', 'link=FTPTest', timeout=120)
        time.sleep(5)
        
        self.click_element_at_coordinates('link=FTPTest', -70, 0)
        time.sleep(5)

        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_SOURCE_FOLDER_OK)

        self.click_element(self.Elements.APPS_FTP_DOWNLOAD_CREATE_DESTINATION_FOLDER_PATH, 120)
        time.sleep(5)
        
        self.click_element_at_coordinates('link=Public', -70, 0)
        self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_DESTINATION_FOLDER_OK)
        
        # Switch recurring backup on if it's off
        if self.is_element_visible(self.Elements.APPS_FTP_DOWNLOAD_CREATE_JOB_RECURRENCE_WEEKLY):
            self.log.info("Recurring backup is already on")
        else:
            self.log.info("Turn on recurring backup")
            self.click_wait_and_check(self.Elements.APPS_FTP_DOWNLOAD_CREATE_JOB_RECURRENCE, self.Elements.APPS_FTP_DOWNLOAD_CREATE_JOB_RECURRENCE_WEEKLY)
        self.log.info('Schedule at 2AM, daily')
        self.click_wait_and_check('id_sch_hour', 'link=2:00')
        time.sleep(3)
        self.click_element('link=2:00')
        time.sleep(3)
        self.click_wait_and_check('apps_ftpdownloadsCreate3_button', timeout=120)

        time.sleep(30)
        
        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path_20, ftp_filename_2)
        if file_md5_sum_2 == ftpdownloadedfilesum:
            self.log.info('FTP folder download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP folder download failed, file checksum: {0}'.format(ftpdownloadedfilesum))
    
    def tc_cleanup(self):
        self.log.info('Delete downloaded folder')
        self.execute(rm_ftp_test)

        self.log.info('Delete FTP job')
        self.execute('ftp_download -a {} -c jobdel'.format(ftp_folder))
        self.end()
FtpDownload()