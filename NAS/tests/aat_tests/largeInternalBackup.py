from testCaseAPI.src.testclient import TestClient
import time

class LargeInternalBackup(TestClient):
    # Create tests as function in this class
    

    def run(self):
        time1 = self.execute('date')
        print time1
        file_size = 100240000
        #self.execute('rm /shares/Public/file*')
        self.execute('rm /shares/SmartWare/file*')
        self.execute('rm -rf /shares/SmartWare/test_1*')
        self.execute('rm /shares/TimeMachineBackup/file*')
        
           
        '''self.execute('rm /shares/Public/file*')
        self.execute('rm /shares/SmartWare/file*')
        self.execute('rm -rf /shares/SmartWare/test_1*')
        self.execute('rm /shares/TimeMachineBackup/file*')
        file = self.create_random_file('/shares/Public/', filename='file0.jpg', blocksize=1024, count=file_size)
        '''
        #file = self.create_random_file('/shares/Public/', filename='file0.jpg', blocksize=file_size, count=1024)
        #self.create_file(filesize=file_size, units='MB', no_repeat=False, file_count=1, 
         #                dest_share_name='Public', dest_dir_path='/', run_on_uut=False)
        hash1 = self.md5_checksum('/shares/Public/', 'file0.jpg')
        print hash1
        self.get_to_page('Backups')
        self.wait_until_element_is_clickable('backups_internal_link')
        self.click_element('backups_internal_link')
        self.wait_until_element_is_clickable('backups_InternalBackupsCreate_button')
        self.click_element('backups_InternalBackupsCreate_button')
           
        self.wait_until_element_is_clickable('backups_InternalBackupsSourcepath_button')
        self.click_element('backups_InternalBackupsSourcepath_button')
        time.sleep(15)
        self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[1]/label/span')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span')           
        self.click_element('home_treeOk_button')
            
        self.wait_until_element_is_clickable('backups_InternalBackupsDestpath_button')
        self.click_element('backups_InternalBackupsDestpath_button')
        time.sleep(15)
        self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/label/span')
        self.click_element('home_treeOk_button')

        self.input_text('backups_InternalBackupsTaskName_text', 'test_1-4', '')
        self.click_element('backups_InternalBackupsCreate2_button')
        time.sleep(1)
        self.click_element('backups_internal_link')
        #self.wait_until_element_is_clickable('backups_InternalBackupsGoJob1_button')
        exist = self.execute('ls /shares/SmartWare/test_1-4/Public/')
        time.sleep(43200)      
            
        hash3 = self.md5_checksum('/shares/SmartWare/test_1-4/Public/', 'file0.jpg')

        print hash1
        print hash3

        if hash1 == hash3:
            print 'Large Backup Checksum is successful'
        else:
            self.log.error('Checksum was not successful. File corrupted')
            
        time1 = self.execute('date')
        print time1
        self.execute('rm -rf /shares/SmartWare/test*')
        self.get_to_page('Backups')
        self.click_element('backups_internal_link')
        self.click_element('backups_InternalBackupsDelJob1_button')
        self.click_element('popup_apply_button')
LargeInternalBackup()