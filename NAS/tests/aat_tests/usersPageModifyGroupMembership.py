""" Users page-Modify Group Membership
    @Author: Lee_e
    
    Objective: Check if membership can be add/remove to/from group
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lghtning : Pass (FW: 2.00.201)
                    KC       : Pass (FW: 2.00.201)
                    Zion     : Pass (FW: 2.00.205)
              Jenkins Taiwan : Lightning    - PASS (FW: 2.00.205)
                             : Yosemite     - PASS (FW: 2.00.205)
                             : Kings Canyon - PASS (FW: 2.00.205)
                             : Sprite       - PASS (FW: 2.00.205)
              Jenkins Irvine : Zion         - PASS (FW: 2.00.205)
                             : Aurora       - PASS (FW: 2.00.205)
                             : Glacier      - PASS (FW: 2.00.200)
                             : Yellowstone  - PASS (FW: 2.00.205)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

user_list = ['user1','user2']
groupname = 'a1'
user_password = 'welc0me'
picture_counts = 0

class ModifyGroupMembership(TestClient):
    def run(self):
        self.delete_all_users(use_rest=False)
        self.delete_all_groups()
        self.add_user(user_list, user_password)
        self.creat_group_command(group_name=groupname, memberusers=user_list[0])
        self.update_membership_of_group_from_UI(group_name=groupname)
        result = self.check_membership_of_group(group_name=groupname, member=user_list[1])
        if not result:
            self.log.info("PASS: Group membership can be changed successfully")
        else:
            self.log.error("FAIL: Group membership updated failed")          
     
    def tc_cleanup(self):
        self.log.info('delete user')
        self.delete_all_users(use_rest=False)
        self.log.info('delete group')
        self.execute("account -d -g '{0}'".format('a1'))
    
    def add_user(self, user_list=None, password=None):
        self.log.info('*** Testing status: Add User ***')
        for username in user_list:
            command = "account -a -u '{0}' -p '{1}'".format(username, password)
            self.log.debug(command)
            self.execute(command)
    
    def creat_group_command(self, numberOfgroups=1, group_name=None, memberusers=None):
        self.log.info('*** Testing status: Create Group & assing Quota ***')
        self.create_groups(group_name, numberOfgroups, memberusers)

    def update_membership_of_group_from_UI(self, group_name):
        self.log.info('*** Testing status: Update membership of Group From UI ***')
        try:
            self.get_to_page('Users')
            self.wait_until_element_is_visible(self.Elements.GROUPS_BUTTON, timeout=10)
            self.click_wait_and_check(self.Elements.GROUPS_BUTTON, 'users_group_'+str(group_name), timeout=10)
            self.click_wait_and_check('users_group_'+str(group_name), self.Elements.GROUPS_GROUP_MEMBERSHIP, timeout=10)
            self.click_wait_and_check(self.Elements.GROUPS_GROUP_MEMBERSHIP, self.Elements.GROUPS_EDIT_GROUP_DIAG, timeout=10)
            if self.is_checkbox_selected('css=.LightningCheckbox,users_editGroupMember1_chkbox'):
                self.unselect_checkbox('css=.LightningCheckbox,users_editGroupMember1_chkbox')
            else:
                self.log.error('User1 not be added to group at beginning')
            if not self.is_checkbox_selected('css=.LightningCheckbox,users_editGroupMember2_chkbox'):
                self.select_checkbox('css=.LightningCheckbox,users_editGroupMember2_chkbox')
            else:
                self.log.error('User2 should not added to group at begining')
            self.click_wait_and_check(self.Elements.GROUPS_EDIT_GROUP_SAVE, self.Elements.UPDATING_STRING, visible=False, timeout=60)
        except Exception as e:
            self.log.error('Exception: {0}'.format(repr(e)))
            self.screenshot('update_membership_of_group_from_UI')
                               
    def check_membership_of_group(self,group_name,member):
        self.log.info('*** Testing status: Check Group Membership Info ***')       
        result = 0
        output = self.run_on_device('cat /etc/group')
        self.log.debug('/etc/group : {0}'.format(output))
        for output_list in output.split('\n'):
            self.log.debug('output_list: {0}'.format(output_list))
            if 'a1:!:' in output_list:
                if not output_list.find(member):
                    result = 1
                else:
                    result = 0
            else:
                result = 1
        return result

    def screenshot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picture_counts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picture_counts)
        output_dir = get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, output_dir))
        picture_counts += 1
        
ModifyGroupMembership()