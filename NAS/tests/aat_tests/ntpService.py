"""
Create on Apr 21, 2016
@Author: lo_va
Objective:  Verify NTP service in kamino firmware build
            KAM-162 (M2 feature, 12/2015) - only test with US NTP server
wiki URL: 	http://wiki.wdc.com/wiki/NTP_Service
"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries import CommonTestLibrary as ctl
import time
import signal

date_set = 'date -s "2009-12-20 01:01:01"'
ntp_client = 'ntpclient -h pool.ntp.org -s'
ntp_timeout = 150

class NTPService(TestClient):

    def run(self):

        self.yocto_init()
        self.yocto_check()
        # ntp_daemon = self.execute('/etc/init.d/ntpd status')[1]
        # self.log.info('ntp daemon status : {}'.format(ntp_daemon))
        self.check_time(date=True)
        # Change date time to 2009-12-20 01:01:01
        signal.signal(signal.SIGALRM, self.signal_handler)
        signal.alarm(ntp_timeout)
        try:
            self.execute(date_set)
            self.check_time(date=True)
            self.execute('hwclock -w')
            self.check_time(hwclock=True)
            time.sleep(3)
            self.execute(ntp_client)
            current_time = self.check_time(date=True)
            self.check_time(hwclock=True)
            self.execute('hwclock -w')
            self.check_time(hwclock=True)
            local_time = ctl.run_command_locally('env TZ=UTC date')[1]
            self.log.info('Local time is: {}'.format(local_time))
            self.log.info('Device time is: {}'.format(current_time))
            if local_time.split()[0] == current_time.split()[0] and \
                            local_time.split()[1] == current_time.split()[1] and \
                            local_time.split()[2] == current_time.split()[2] and \
                            local_time.split()[5] == current_time.split()[5]:
                self.log.info('NTP Service test PASSED!!')
            else:
                self.log.error('NTP Service test FAILED!!')
        except Exception as ex:
            self.log.warning('Executed ntpService.py Timeout!! ErrMsg: {}'.format(ex))


    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            fw_ver = self.execute('cat /etc/version')[1]
            self.log.info('Firmware Version: {}'.format(fw_ver))
        except Exception as ex:
            self.log.exception('Executed SSH command failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

    def check_time(self, hwclock=None, date=None):
        if date:
            current_time = self.execute('date')[1]
            self.log.info('Current Time: {}'.format(current_time))
            return current_time
        if hwclock:
            hwclock = self.execute('hwclock -r')[1]
            self.log.info('hwclock -r: {}'.format(hwclock))
            return hwclock

    @staticmethod
    def signal_handler(signum, frame):
        raise Exception('Timeout on running command locally')

NTPService(checkDevice=False)
