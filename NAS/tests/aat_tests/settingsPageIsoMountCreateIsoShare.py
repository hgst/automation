""" Settings page - ISO Mount - Create ISO Share (MA-84)

    @Author: lin_ri
    Procedure @ http://silk.sc.wdc.com/silk/DEF/TM/Test+Plan?nEx=20018&execView=execDetails&view=details&pltab=steps&fiTpId=13015&pId=50&nTP=117529&etab=8
    1) Open WebUI and login as Admin
    2) Click on Settings
    3) Select ISO mount
    4) Click on Create ISO Share (An .iso file must be selected before you can create the share)
    5) Provide the share location on the NAS
    6) Verify that the share is created


    Automation: Full

    Not supported product:
        N/A

    Support Product:
        Lightning
        YellowStone
        Glacier
        Yosemite
        Sprite
        Aurora
        Kings Canyon


    Test Status:
        Local Lightning: N/A (FW: 2.10.144)
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from seleniumAPI.src.seleniumclient import ElementNotReady
from selenium.common.exceptions import StaleElementReferenceException
import time
import os




class settingsPageIsoMountCreateIsoShare(TestClient):
    def run(self):
        self.log.info('############### (MA-84) Settings page/ISO Mount/Create ISO Share TESTS START ###############')
        share_name = 'isoshare'
        iso_file_name = 'newImage.iso'
        if self.uut[self.Fields.product] in ('Glacier',):
            self.log.info("**** Glacier does not support ISO utility, skip the test!! ****")
            return
        try:
            self.delete_all_shares()
            self.delete_iso_share_by_ui()
            self.run_on_device('rm -rf /shares/{}/*.iso'.format(share_name))
            self.log.info("Create new share: {}".format(share_name))
            self.create_shares(share_name=share_name, force_webui=True)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            self.log.info("Generate random file on /shares/{}/".format(share_name))
            self.create_random_file('/shares/{}/'.format(share_name), filename='testfile.jpg',
                                    blocksize=1024, count=10240)
            iso_cmd = 'cd /mnt/HD/HD_a2/{0};'.format(share_name)
            iso_cmd += 'genisoimage -r -f -udf -joliet-long -allow-limited-size ' \
                       '-jcharset UTF-8 -o {0} .'.format(iso_file_name)
            self.run_on_device(iso_cmd)
            self.file_check(share_name=share_name, filename=iso_file_name)
            self.create_iso_share_by_ui(share_name=share_name, file_name=iso_file_name)
            self.verify_iso_share_by_ui(iso_name=iso_file_name)
            # self.create_iso_share(share_name=share_name, iso_file=iso_file_name)
            # self.delete_iso_share(iso_file=iso_file_name)
            time.sleep(10)
        except Exception as e:
            self.log.error("Test Failed: (MA-84) Create ISO Share test, exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            output = self.run_on_device('ls -la /shares/')
            self.log.info("Current shares: \n {}".format(output))
        finally:
            self.log.info("====== Clean Up section ======")
            self.delete_iso_share_by_ui()
            self.run_on_device('rm -rf /shares/{}/*.iso'.format(share_name))
            self.log.info('############### (MA-84) Settings page/ISO Mount/Create ISO Share TESTS END ###############')

    def file_check(self, share_name, filename):
        cmd = 'test -f /shares/{}/{}; echo $?'.format(share_name, filename)
        result = self.execute(cmd)[1]
        self.log.info('file check: {}'.format(result))
        if result == '0':  # Success: the file exists
            self.log.info("PASS: Make iso image file: {}".format(filename))
        else:
            self.log.error("ERROR: ISO image file: {} is not found!!".format(filename))

    def create_iso_share_by_ui(self, share_name, file_name):
        try:
            self.log.info("=== Create ISO Share by UI ===")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('settings_utilities_link', "settings_utilitiesLogs_button", visible=True)
            # Scroll the page to the bottom
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            self.click_wait_and_check('settings_utilitiesISOShare_button', 'isoDiag_title', visible=True, timeout=30)
            self.click_wait_and_check("//a[@rel=\'/mnt/HD/HD_a2/{}/\']".format(share_name),
                                      "//a[@rel=\'/mnt/HD/HD_a2/{}/new/\']".format(share_name), visible=True)
            iso_locator = "//a[@rel=\'/mnt/HD/HD_a2/{0}/\']/following-sibling::ul/li/label/span".format(share_name)
            iso_checkbox = "//a[@rel=\'/mnt/HD/HD_a2/{0}/\']/following-sibling::ul/li/label//input".format(share_name)
            check_status = self.element_find(iso_checkbox).get_attribute('checked')
            while check_status != 'true':
                self.click_element(iso_locator)
                check_status = self.element_find(iso_checkbox).get_attribute('checked')
            self.log.info("{} is selected: {}".format(file_name, check_status))
            self.click_wait_and_check('settings_utilitiesISOShareNext_button',
                                      'settings_utilitiesISOShareNext1_button', visible=True)
            self.click_wait_and_check('settings_utilitiesISOShareNext1_button',
                                      'settings_utilitiesISOShareNext2_button', visible=True)
            if self.uut[self.Fields.product] in ('Lightning', ):
                self.click_wait_and_check('settings_utilitiesISOShareNext2_button', visible=False)
            else:
                self.click_wait_and_check('settings_utilitiesISOShareNext2_button',
                                          'settings_utilitiesISOShareSave_button', visible=True)
                self.click_wait_and_check('settings_utilitiesISOShareSave_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
            time.sleep(5)  # Wait for updating
        except Exception as e:
            self.log.exception("ERROR: Unable to create ISO share by Web UI, exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            output = self.run_on_device('ls -la /shares/')
            self.log.info("Current shares: \n {}".format(output))
        else:
            self.log.info("=== Succeed to create the ISO share: {} ===".format(file_name))
        finally:
            self.close_webUI()

    def delete_iso_share_by_ui(self):
        try:
            self.log.info("=== Clean ISO Share by UI ===")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('settings_utilities_link', "settings_utilitiesLogs_button", visible=True)
            # Scroll the page to the bottom
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            row_elem = self.element_find("//div[@id=\'iso_list\']")
            rows = len(row_elem.find_elements_by_xpath("//div[@id=\'iso_list\']/ul/li"))
            self.log.info("ROWS: {}".format(rows))
            if rows == 0:
                self.log.info("No ISO mount share is created, skip the deletion!")
                return
            for i in range(rows):
                self.click_wait_and_check('settings_utilitiesISODel0_link',
                                          'popup_apply_button', visible=True)
                self.click_wait_and_check('popup_apply_button', visible=False)
                self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)
                time.sleep(5)   # Waiting for updating
            self.log.info("=== All ISO shares are clean! ===")
        except Exception as e:
            self.log.exception("ERROR: Fail to delete ISO share by Web UI, exception: {}".format(repr(e)))
        finally:
            self.close_webUI()

    def verify_iso_share_by_ui(self, iso_name):
        try:
            self.log.info("=== Verify ISO Share by UI ===")
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('settings_utilities_link', "settings_utilitiesLogs_button", visible=True)
            # Scroll the page to the bottom
            self.execute_javascript("window.scrollTo(0, document.body.scrollHeight);")
            row_elem = self.element_find("//div[@id=\'iso_list\']")
            rows = len(row_elem.find_elements_by_xpath("//div[@id=\'iso_list\']/ul/li"))
            self.log.info("ROWS: {}".format(rows))
            iso_name = iso_name.rsplit('.')[0]  # exclude .iso
            if rows > 1:
                self.log.info("FOUND more than one ISO share mount")
            for i in range(rows):
                iso_share_name = self.get_text('settings_utilitiesISOName{}_value'.format(i))
                self.log.info("NAME: {}".format(iso_share_name))
                if iso_share_name == iso_name:
                    self.log.info("=== PASS: {} is mounted as ISO share. ===".format(iso_name))
                    return
        except Exception as e:
            self.log.exception("ERROR: Unable to verify ISO share: {}, exception: {}".format(iso_name, repr(e)))
        else:
            self.log.exception("ERROR: ISO share:{} is not found.".format(iso_name))

settingsPageIsoMountCreateIsoShare()