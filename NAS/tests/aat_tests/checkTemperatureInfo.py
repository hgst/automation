"""
Created on July 27th, 2015

@author: tran_jas

# procedure for manually testing temperature status is at http://wiki.wdc.com/wiki/LED_Test

## @brief Verify device will display 'warn' message if temperature is changed

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient



class CheckTemperatureInfo(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            result = self.get_system_state()
            temperature = self.get_xml_tag(result[1], 'temperature')
            self.log.info('Current temperature status: {}'.format(temperature))
            self.execute('echo "1" > /tmp/temp_state')
            result = self.get_system_state()
            temperature2 = self.get_xml_tag(result[1], 'temperature')
            self.log.info('Current temperature status: {}'.format(temperature2))
            if temperature2 == 'warn':
                self.log.info("Check temperature test passed")
            else:
                self.log.error("Check temperature test failed")

        except Exception, ex:
            self.log.exception('Failed to complete check temperature test case \n' + str(ex))
        finally:
            self.execute('echo "0" > /tmp/temp_state')
            result = self.get_system_state()
            temperature2 = self.get_xml_tag(result[1], 'temperature')
            self.log.info('Current temperature status: {}'.format(temperature2))


CheckTemperatureInfo()