
"""
    Script to install/update package(s) on all exec servers

"""


from testCaseAPI.src.testclient import TestClient

exec_servers = ['10.6.160.140', '10.6.160.223', '10.6.160.92', '10.6.160.51', '10.6.160.103',
                '10.6.160.127', '10.6.161.14', '10.136.139.17', '10.136.139.18', '10.136.139.19',
                '10.136.139.20', '10.136.139.21', '10.136.139.22', '10.136.139.23', '10.136.139.24',
                '10.136.139.25', '10.136.139.26']

# Update this dictionary with packages and expected versions. Only include the version number, not the whole version
# returned from yum's list option (such as 0.8.1-13.fc20).
yum_expected = {'fuse-afp': '0.8.1-13',
                'afpfs-ng': '0.8.1-13'}


class ExecServerUpdate(TestClient):

    def run(self):

        try:
            for exec_server in exec_servers:
                # ssh_command = 'ssh fitbranded@' + exec_servers[0]
                self.ssh_connect('fitbranded', 'Branded1', port=None, server_ip=exec_server)
                yum_installed = self.execute('yum list installed')[1]
                # self.log.info(yum_installed)

                for package in yum_expected:
                    package_location = yum_installed.find(package)
                    expected_version = yum_expected[package]

                    if package_location > 0:
                        package_info = yum_installed[package_location:].splitlines()[0]
                        installed_version = package_info.split()[1]

                        if expected_version in installed_version:
                            self.log.info('Package {} is installed on {} and the expected version of '
                                          '{}'.format(package, exec_server, expected_version))
                        else:
                            self.log.info('Updating {} from {} to {}'.format(package, installed_version,
                                                                             expected_version))
                            # First, remove the old version
                            result, output = self.execute('sudo yum -y remove {}'.format(package))
                            if result != 0:
                                raise Exception('Failed to remove {} - Output: {}'.format(package, output))

                            self.log.info('Removed old version')

                            # Install the new version
                            res, output = self.execute('sudo yum -y install {}-{}*'.format(package, expected_version))
                            if res != 0:
                                raise Exception('Failed to install {}-{} Output: {}'.format(package, expected_version,
                                                                                            output))

                            self.log.info('Successfully installed {}-{}'.format(package, expected_version))
                    else:
                        self.log.info('Package {} is not installed on {}'.format(package, exec_server))
                        # Install the new version
                        res, output = self.execute('sudo yum -y install {}-{}*'.format(package, expected_version))
                        if res != 0:
                            raise Exception('Failed to install {}-{} Output: {} on {}'.format(package, expected_version,
                                                                                              output, exec_server))

                        self.log.info('Successfully installed {}-{} on {}'.format(package, expected_version,
                                                                                  exec_server))

        except Exception, ex:
            self.log.info(ex)

ExecServerUpdate()