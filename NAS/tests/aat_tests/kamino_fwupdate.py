"""
Create on Jan 20, 2016
@Author: lo_va
Objective: For Kamino project platform(Yocto) firmware update used.
"""

import os
import time
import shutil
import tarfile
import signal

from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
from global_libraries import CommonTestLibrary as ctl
fw_update_version = os.environ["Kamino_FW"]
yocto_download_path = os.environ["NEXUS_BASE_URL"]
run_ctl_timeout = 600


class FWupdate(TestClient):

    def run(self):
        self.yocto_init()
        self.yocto_check()
        fw_name = 'mycloudos-{0}.tgz'.format(fw_update_version)
        for dir in os.listdir('.'):
            if dir.startswith('mycloudos') and dir.endswith('.tgz'):
                os.remove(dir)
            elif 'mycloudos' in dir:
                shutil.rmtree(dir)
        os.mkdir('mycloudos')
        try:
            ctl.run_command_locally('wget -nv -t 10 -c {0}/{1}/{2}'
                                    .format(yocto_download_path, fw_update_version, fw_name))
        except Exception as ex:
            self.log.exception('Download Firmware image failed! ErrMsg: {}'.format(ex))
            with open("DownloadFailed.txt", "w") as f:
                f.write('Download Firmware Failed\n')
            shutil.copy('DownloadFailed.txt', '{}/DownloadFailed.txt'.format(os.environ["WORKSPACE"]))
            exit(1)
        shutil.move('./{}'.format(fw_name), './mycloudos/{0}'.format(fw_name))
        os.chdir('mycloudos')
        tar = tarfile.open(fw_name)
        tar.extractall()
        tar.close()
        ip_addr = self.uut[Fields.internal_ip_address]
        swu_file = ''
        for file in os.listdir('.'):
            if file.endswith('.swu'):
                swu_file = file

        # Remove boot_finished
        self.execute('rm /tmp/boot_finished')

        signal.signal(signal.SIGALRM, self.signal_handler)
        signal.alarm(run_ctl_timeout)
        try:
            run = ctl.run_command_locally('python firmware-update.py -b {0} -n ./{1}'.format(ip_addr, swu_file))
            self.log.debug('Run Result:{}'.format(run))
            self.wait_device_back()
        except Exception as ex:
            self.log.exception('Executed firmware-update.py FAILED! ErrMsg: {}'.format(ex))
            with open("UpdateFailed.txt", "w") as f:
                f.write('Updated Firmware Failed\n')
            shutil.copy('UpdateFailed.txt', '{}/UpdateFailed.txt'.format(os.environ["WORKSPACE"]))
        fw_updated_ver = self.yocto_check()
        if fw_updated_ver == fw_update_version:
            self.log.info('Updated Firmware by using firmware.py Succeed!!')
            with open("UpdateSuccess.txt", "w") as f:
                f.write('Updated Firmware Success\n')
            shutil.copy('UpdateSuccess.txt', '{}/UpdateSuccess.txt'.format(os.environ["WORKSPACE"]))
        else:
            self.log.error('Updated Firmware by using firmware.py FAILED!!')
            with open("UpdateFailed.txt", "w") as f:
                f.write('Updated Firmware Failed\n')
            shutil.copy('UpdateFailed.txt', '{}/UpdateFailed.txt'.format(os.environ["WORKSPACE"]))

    def tc_cleanup(self):
        os.chdir('..')
        for item in os.listdir('.'):
            if 'mycloudos' in item:
                shutil.rmtree(item)
        os.chdir(os.environ["WORKSPACE"])
        try:
            self.log.info('Start to SCP lastpassbuild file to the NAS')
            self.open_scp_connection()
            self.scp_file_to_device(files='lastpassbuild', remote_path='/Data/devmgr/db', recursive=False, preserve_times=False)
        except Exception as e:
            self.log.exception('Failed to transfer lastpassbuild file. Exception: {}'.format(repr(e)))

    def yocto_init(self):
        self.skip_cleanup = True
        self.uut[Fields.ssh_username] = 'root'
        self.uut[Fields.ssh_password] = ''
        self.uut[Fields.serial_username] = 'root'
        self.uut[Fields.serial_password] = ''

    def yocto_check(self):
        try:
            boot_finished = self.execute('ls /tmp')[1]
            fw_ver = self.execute('cat /etc/version')[1]
            if 'boot_finished' in boot_finished:
                self.log.info('Device is Ready')
                self.log.info('Firmware Version: {}'.format(fw_ver))
            else:
                raise Exception('boot_finished is not exist, device not ready!')
            return fw_ver
        except Exception as ex:
            self.log.exception('Device check failed! [ErrMsg: {}]'.format(ex))
            self.log.warning('Break Test')
            exit(1)

    def wait_device_back(self):
        max_boot_time = 60
        start_time = time.time()
        while not self.is_device_pingable():
            if time.time() - start_time > max_boot_time:
                raise Exception('Timed out waiting to boot')
        start_time = time.time()
        ssh_connected = False
        boot_finished = ''
        i = 1
        while not ssh_connected or 'boot_finished' not in boot_finished:
            try:
                boot_finished = self.execute('ls /tmp')[1]
                ssh_connected = True
            except Exception as ex:
                print 'ssh failed, try again {0}, {1}'.format(i, ex)
                if time.time() - start_time > 500:
                    raise Exception('Timed out waiting ssh connect.')
                i += 1
            if 'boot_finished' not in boot_finished and time.time() - start_time > 500:
                raise Exception('Time out waiting boot_finished file')
            time.sleep(1)

    @staticmethod
    def signal_handler(signum, frame):
        raise Exception('Timeout on running command locally')

FWupdate(checkDevice=False)
