""" My Cloud Other (User) Login

    @Author: Lee_e

    Objective: check if other user can log into webUI

    Automation: Full

    Supported Products:
        All (exclude Glacier due to de-feature that only admin can login web)

    Test Status:
              Local Lightning : Pass (FW: 2.10.159)
                    KC        : Pass (FW: 2.10.159)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.159)
                              : Yosemite     - PASS (FW: 2.10.159)
                              : Kings Canyon - PASS (FW: 2.10.158)
                              : Sprite       - PASS (FW: 2.10.159)
              Jenkins Irvine  : Zion         - PASS (FW: 2.10.157)
                              : Aurora       - PASS (FW: 2.10.157)
                              : Yellowstone  - PASS (FW: 2.10.157)
                              : Glacier      - De-feature

"""

from testCaseAPI.src.testclient import TestClient

user_list = ['user1']
user_password = '12345'
non_support_list = ['GLCR']

class MyCloudOtherUserLogin(TestClient):
    def run(self):
        model = self.get_model_number()
        if model in non_support_list:
            self.log.info('Model is {0} in not support iSCSI list{1}'.format(model, non_support_list))
            return

        self.check_update_user()
        self.webUI_user_login(username=user_list[0], userpwd=user_password)
        self.webUI_user_logout()

    def tc_cleanup(self):
        self.check_update_user(delete=True)

    def check_update_user(self, delete=False):
        users = self.get_user_list()
        for i in range(0, len(user_list)):
            username = user_list[i]
            if username in users:
                self.log.info('Modify user {0}'.format(username))
                self.modify_user_account(user=username, passwd=user_password)
            else:
                self.log.info('Add user {0}'.format(username))
                add_user_command = 'account -a -u {0} -p {1}'.format(username, user_password)
                self.run_on_device(add_user_command)
            if delete:
                self.log.info('Delete user: {0}'.format(username))
                self.run_on_device('account -d -u {0}'.format(username))

    def webUI_user_login(self, username, userpwd):
        """
        :param username: user name you'd like to login
        :param userpwd: user password for user name account
        """
        self.start_test('webUI_user_login')
        try:
            self.access_webUI(do_login=False)
            self.input_text_check('login_userName_text', username, do_login=False)
            self.input_text_check('login_pw_password', userpwd, do_login=False)
            self.click_wait_and_check('login_login_button', visible=False)
            self._sel.check_for_popups()
            self.pass_test('webUI_user_login as [{}] pass'.format(username))
        except Exception, ex:
            self.fail_test('webUI_user_login with exception : {0}'.format(repr(ex)))
            self.take_screen_shot('login_fail')

    def webUI_user_logout(self):
        self.start_test('webUI_user_logout')
        try:
            self.click_wait_and_check(self.Elements.NON_ADMIN_TOOLBAR_LOGOUT, self.Elements.NON_ADMIN_TOOLBAR_LOGOUT_LOGOUT)
            self.click_wait_and_check(self.Elements.NON_ADMIN_TOOLBAR_LOGOUT_LOGOUT, 'login_login_button', timeout=10)
            self.pass_test('webUI_user_logout')
        except Exception, ex:
            self.fail_test('webUI_user_logout with exception : {0}'.format(repr(ex)))
            self.take_screen_shot('logout_fail')

    def get_user_list(self):
        self.log.info('*** get user list ***')
        users = self.execute('account -i user')[1].split()

        return users

MyCloudOtherUserLogin()