"""
Created on Jan 16th, 2015

@author: tran_jas

## @brief User shall able to create HTTP and FTP downloads via web UI
#
#  @detail 
#     HTTP case 1: schedule http download job to start in 15 minutes, then click Start button
#     HTTP case 2: schedule http download job to start in 1 hour, modify time and location
#        Verify daily, weekly and monthly backup  
#     FTP case 1: Download file with authentication, no schedule
#     FTP case 2: Download folder with no authentication, scheduled

"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

httpFile = 'http://go.microsoft.com/fwlink/?LinkId=324638'
httpFileSum = '1eda16a52b3c855cb80e9de4b0a78eac'
httpFileName = 'EIE11_EN-US_MCM_WIN764.EXE'
httpFile2 = 'http://go.microsoft.com/fwlink/p/?LinkId=522100'
httpFileSum2 = '8d0e461198854496502dc0293ea13d7a'
httpFileName2 = 'Windows10_TechnicalPreview_x64_EN-US_9926.iso'

ftp_file_path = '/shares/Public/'
ftp_file_path_20 = '/shares/Public/FTPTest/'
http_file_path = '/shares/SmartWare/'
ftp_user = 'ftpuser'
my_ftp_user = 'jtran'
ftp_password = 'fituser'
ftp_filename = 'AFP-74BD.mp4'
file_md5_sum = 'b776a446fa43d7b735854de315cbb840'
ftp_filename_2 = 'diaryofacamper_raw.mp4'
file_md5_sum_2 = '185236549de96b693b6cd7b2afb25427'
successful_text = 'Successful'
rm_exe = 'rm /shares/SmartWare/*.EXE'
rm_exe_pub = 'rm /shares/Public/*.EXE'
rm_iso = 'rm /shares/Public/*.iso'
rm_jpg = 'rm /shares/Public/*.jpg'
rm_mp4 = 'rm /shares/Public/*.mp4'
rm_ftp_test = 'rm -r /shares/Public/FTPTest'
ftp_folder = "FTP_folder-345"
ftp_file = "FTP_file-01"
ftp_port = '21'

wrong_IP = '192.168.0.001'
wrong_ftp_password = 'passwordno'
wrong_port = '25'
credential_error = 'Download failed due to failed authentication. Please re-enter the correct login credentials.'
port_error = 'Download failed due to incorrect port number. Please check with the FTP server and enter the correct ' \
             'port number.'
host_error = 'Download failed due to invalid host. Please re-enter the correct host name.'
max_file_folder_error = 'The maximum number of folders and/or files has been reached'


class HttpFtpDownloader(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            ftp_server_ip = self.uut[self.Fields.ftp_server]

            self.log.info('httpFtpDownloader started')
            self.set_web_access_timeout('30')
            self.http_downloader()
            self.http_downloader_recurring()
            self.ftp_downloader_2_0(ftp_server_ip)
            self.disable_ntp()
            self.ftp_downloader_2_0_scheduled(ftp_server_ip)
            self.close_webUI()
            self.create_user(my_ftp_user, password=ftp_password)
            self.uut[self.Fields.web_username] = my_ftp_user
            self.uut[self.Fields.web_password] = ftp_password
            self.ftp_downloader_2_0(ftp_server_ip)
            self.ftp_downloader_2_0_scheduled(ftp_server_ip)

        except Exception, ex:
            self.log.exception('Failed to complete HttpFtpDownloader \n' + str(ex))
        finally:
            self.set_web_access_timeout('5')
            self.log.info('Delete downloaded file')
            self.execute(rm_iso)
            self.execute(rm_exe)
            self.execute(rm_mp4)
            self.execute(rm_ftp_test)
            self.log.info('Delete created user and reset to admin')
            self.delete_user(my_ftp_user)
            self.uut[self.Fields.web_username] = 'admin'
            self.uut[self.Fields.web_password] = ''
            self.enable_ntp()
            self.close_webUI()

    # HTTP Downloader
    # Test case 1: schedule http download job to start in 15 minutes, then click Start BUTTON
    def http_downloader(self):
        self.get_to_page('Apps')
        self.click_element('apps_httpdownloads_link')
        self.click_element('apps_httpdownloadsLoginMethodAnonymous_button')
        time.sleep(3)
        self.input_text('apps_httpdownloadsURL_text', httpFile2, True)
        time.sleep(3)
        self.click_element('apps_httpdownloadsTest_button')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('apps_httpdownloadsTestClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successful_text:
            self.log.info('Valid URL was entered: {0}'.format(httpFile2))
            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            # time.sleep(5)
            self.click_element('home_treeOk_button')
            # time.sleep(5)
            # if self.is_element_visible('home_treeOk_button'):
            #     self.click_element('home_treeOk_button')

            # Disable NTP and set date time to 1:45AM
            self.disable_ntp()
            self.execute('date -s 2015.01.05-01:45:00')

            # Schedule download at 2AM (15 minutes schedule)
            self.click_element('f_hour')
            self.click_element('link=2AM')
            self.click_element('f_min')
            self.click_element('link=00')
            self.click_element('apps_httpdownloadsCreate_button')
            time.sleep(3)

            # click Start button
            while self.is_element_visible('css=div.tip.tip_backup_wait > img'):
                self.log.info('Start http download job...')
                self.click_element('//tr[@id=\'row1\']/td[7]/div/a/img')
                time.sleep(10)

            self.log.info("4 minutes wait for downloading...")
            time.sleep(240)

            # refresh UI
            self.click_element('apps_httpdownloads_link')

            loop_count = 0
            while (self.is_element_visible('css=div.tip.tip_backup_success > img') is False) and (loop_count < 30):
                self.log.info("Waiting for successful icon...")
                loop_count += 1
                time.sleep(60)
                self.click_element('apps_httpdownloads_link')
                time.sleep(10)

            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('http download successful image is displayed')

            time.sleep(30)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(ftp_file_path, httpFileName2)

            if httpFileSum2 == httpdownloadedfilesum:
                self.log.info('HTTP download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            self.log.info('Delete downloaded file')
            self.execute(rm_iso)

            self.log.info('Delete http job')
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.log.info('job to be deleted')

                # work around until ITR # 104595 is resolved
                # click Modify button, close error then continue
                if self.is_element_visible('apps_httpdownloadsModify_button'):
                    self.click_element('apps_httpdownloadsModify_button')
                    while self.is_element_visible('popup_ok_button'):
                        self.click_button('popup_ok_button')

                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsDel_button')

                while self.is_element_visible('popup_ok_button'):
                    self.log.info('Failed to select http job, close error message then try again')
                    self.click_button('popup_ok_button')

                    # work around until ITR # 104595 is resolved
                    # click Modify button, close error then continue
                    if self.is_element_visible('apps_httpdownloadsModify_button'):
                        self.click_element('apps_httpdownloadsModify_button')
                        while self.is_element_visible('popup_ok_button'):
                            self.click_button('popup_ok_button')

                    self.click_element('//tr[@id=\'row1\']/td[2]/div')
                    self.click_element('apps_httpdownloadsDel_button')
                    time.sleep(3)

                while self.is_element_visible('popup_apply_button'):
                    self.click_element('popup_apply_button')
                    time.sleep(3)
                # refresh UI
                self.click_element('apps_httpdownloads_link')
        else:
            self.log.error('Invalid URL was entered: {0}'.format(httpFile2))

    # HTTP Downloader        
    # Test case 2: schedule http download job to start in 1 hour, modify time and location
    # Verify daily, weekly and monthly backup
    def http_downloader_recurring(self):

        # Disable NTP and set date time to 1:00AM
        self.log.info('Disable NTP and set date')
        self.disable_ntp()

        self.execute('date -s 2015.01.05-01:00:00')
        self.log.info('Current date time - before change schedule: {0}'.format(self.execute('date')))

        self.get_to_page('Apps')
        self.click_element('apps_httpdownloads_link')
        self.click_element('apps_httpdownloadsLoginMethodAnonymous_button')

        self.input_text('apps_httpdownloadsURL_text', httpFile, True)
        self.click_element('apps_httpdownloadsTest_button')
        popuptext = self.get_text('//td/div/table/tbody/tr[2]/td[2]')
        newpopuptext = popuptext.strip()
        self.log.info('Popup text message: {0}'.format(newpopuptext))
        self.click_element('apps_httpdownloadsTestClose2_button')

        # If valid URL was entered, continue
        if newpopuptext == successful_text:
            self.log.info('Valid URL was entered: {0}'.format(httpFile))
            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[1]/label/span', 120)
            time.sleep(5)
            self.click_element('home_treeOk_button')
            time.sleep(5)
            if self.is_element_visible('home_treeOk_button'):
                self.click_element('home_treeOk_button')

            # Schedule download at 2AM (1 hour schedule)
            self.click_element('f_hour')
            self.click_element('link=2AM')
            self.click_element('f_min')
            self.click_element('link=00')
            self.click_element('apps_httpdownloadsCreate_button')
            time.sleep(3)

            # Modify time and location (SmartWare folder)
            if self.is_element_visible('css=div.tip.tip_backup_wait > img'):
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')

            self.click_element('apps_httpdownloadsBrowse_button')
            self.click_element('//div[@id=\'folder_selector\']/ul/li[2]/label/span', 120)
            time.sleep(5)
            self.click_element('home_treeOk_button')

            self.click_element('f_hour')
            self.click_element('link=1AM')
            self.click_element('f_min')
            self.click_element('link=05')
            self.click_element('apps_httpdownloadsSave_button')

            time.sleep(120)
            # refresh UI
            self.click_element('apps_httpdownloads_link')

            # wait for successful image
            self.wait_until_element_is_visible('css=div.tip.tip_backup_success > img', 180)
            if self.is_element_visible('css=div.tip.tip_backup_success > img'):
                self.log.info('http download successful image is displayed')

            time.sleep(15)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(http_file_path, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP scheduled download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP scheduled download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rm_exe)

            # Modify to daily back up
            self.log.info("Daily backup, select row then modify button")
            self.click_element('apps_httpdownloads_link')
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')
            time.sleep(3)

            # Turn on Recurring Backup
            if not self.is_element_visible('apps_httpdownloadsPeriodDay_button'):
                self.log.info('Recurring switch is not turn on, turn it on')
                self.click_element('css=#apps_httpdownloadsPeriod_switch+span .checkbox_container')

            self.click_element('apps_httpdownloadsPeriodDay_button')
            # self.click_element('f_period_hour')
            # self.click_element('link=1AM')
            self.click_element('f_period_min')
            self.click_element('link=05')
            self.click_element('apps_httpdownloadsSave_button')
            time.sleep(5)

            self.log.info('time before set: {0}'.format(self.execute('date')))
            self.execute('date -s 2015.01.06-12:03:00')
            self.log.info('time after set: {0}'.format(self.execute('date')))

            # will not able ot use successful image, as it will disappear quickly after download
            self.log.info('Waiting for timer to kick off backup...')
            time.sleep(300)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(http_file_path, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP daily download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP daily download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rm_exe)

            # Weekly backup
            self.log.info("Weekly backup, select row then modify button")
            self.click_element('apps_httpdownloads_link')
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')
            time.sleep(3)

            # Select weekly
            self.click_element('apps_httpdownloadsPeriodWeek_button')
            # self.click_element('f_period_hour')
            # self.click_element('link=1AM')
            self.click_element('f_period_min')
            self.click_element('link=05')
            self.click_element('f_period_week')
            self.click_element('link=Mon')
            self.click_element('apps_httpdownloadsSave_button')
            time.sleep(3)

            self.log.info('time before set: {0}'.format(self.execute('date')))
            self.execute('date -s 2015.01.12-12:03:00')
            self.log.info('time after set: {0}'.format(self.execute('date')))

            # will not able ot use successful image, as it will disappear quickly after download
            self.log.info('Waiting for timer to kick off backup...')
            time.sleep(300)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(http_file_path, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP weekly download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP weekly download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rm_exe)

            # Monthly backup
            self.log.info("Monthly backup, select row then modify button")
            self.click_element('apps_httpdownloads_link')
            self.click_element('//tr[@id=\'row1\']/td[2]/div')
            self.click_element('apps_httpdownloadsModify_button')

            while self.is_element_visible('popup_ok_button'):
                self.click_button('popup_ok_button')
                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsModify_button')
            time.sleep(3)

            # Select monthly
            self.click_element('apps_httpdownloadsPeriodMonth_button')
            # self.click_element('f_period_hour')
            # self.click_element('link=1AM')
            self.click_element('f_period_min')
            self.click_element('link=05')
            self.click_element('f_period_month')
            self.click_element('link=01')
            self.click_element('apps_httpdownloadsSave_button')
            time.sleep(3)

            self.log.info('time before set: {0}'.format(self.execute('date')))
            self.execute('date -s 2015.02.01-12:03:00')
            self.log.info('time after set: {0}'.format(self.execute('date')))

            # will not able ot use successful image, as it will disappear quickly after download
            self.log.info('Waiting for timer to kick off backup...')
            time.sleep(300)

            # Verify download successfully via checksum comparison
            httpdownloadedfilesum = self.md5_checksum(http_file_path, httpFileName)

            if httpFileSum == httpdownloadedfilesum:
                self.log.info('HTTP monthly download successfully, file checksum: {0}'.format(httpdownloadedfilesum))
            else:
                self.log.error('HTTP monthly download failed, file checksum: {0}'.format(httpdownloadedfilesum))

            # delete downloaded file
            self.execute(rm_exe)

            # delete http job
            self.log.info('Delete http job')
            while self.is_element_visible('//tr[@id=\'row1\']/td[2]/div'):
                self.log.info('job to be deleted')

                # work around until ITR # 104595 is resolved
                # click Modify button, close error then continue
                if self.is_element_visible('apps_httpdownloadsModify_button'):
                    self.click_element('apps_httpdownloadsModify_button')
                    while self.is_element_visible('popup_ok_button'):
                        self.click_button('popup_ok_button')

                self.click_element('//tr[@id=\'row1\']/td[2]/div')
                self.click_element('apps_httpdownloadsDel_button')

                while self.is_element_visible('popup_ok_button'):
                    self.log.info('Failed to select http job, close error message then try again')
                    self.click_button('popup_ok_button')

                    # work around until ITR # 104595 is resolved
                    # click Modify button, close error then continue
                    if self.is_element_visible('apps_httpdownloadsModify_button'):
                        self.click_element('apps_httpdownloadsModify_button')
                        while self.is_element_visible('popup_ok_button'):
                            self.click_button('popup_ok_button')

                    self.click_element('//tr[@id=\'row1\']/td[2]/div')
                    self.click_element('apps_httpdownloadsDel_button')
                    time.sleep(3)

                while self.is_element_visible('popup_apply_button'):
                    self.click_element('popup_apply_button')
                    time.sleep(3)

                # refresh UI
                self.click_element('apps_httpdownloads_link')
        else:
            self.log.error('Invalid URL was entered: {0}'.format(httpFile))

        # Enable NTP
        self.enable_ntp()

    # FTP downloader 2.0
    # Test case 1: Download file with authentication, no schedule
    def ftp_downloader_2_0(self, ftp_server_ip):

        self.close_webUI()
        self.access_webUI()

        if self.is_element_visible('nav_downloads_link'):
            self.log.info('Testing as regular user')
            self.click_element('nav_downloads_link')
            self.click_element('//li[@id=\'ftp_downloads\']/span')
        else:
            self.get_to_page('Apps')
            self.click_element('apps_ftpdownloads_link')

        # Before a backup job is created, verify that the "FTP Downloads Jobs" is not displayed
        if self.is_element_visible('apps_ftpdownloadsDelJob1_button'):
            self.log.error('No FTP job should be displayed')
        else:
            self.log.info('No FTP job is displayed as expected')

        # Select to create FTP job with authentication
        self.click_element('apps_ftpdownloadsCreate_button')
        self.click_element('apps_ftpdownlaodsAccount_button')

        """
        # Verifying max characters (ITR 103880)
        self.log.info("Verify max characters for username textbox")
        self.input_text('apps_ftpdownloadsUsername_text', "01234567890123456789012345678901234", True)
        return_value = self.get_value("apps_ftpdownloadsUsername_text")
        if return_value == '01234567890123456789012345678901':
            self.log.info("Only 32 characters are accepted")
        else:
            self.log.error("More than 32 characters are accepted")

        self.log.info("Verify max characters for password textbox")
        self.input_text('apps_ftpdownloadsPassword_text', "01234567890123456789012345678901234", True)
        return_value = self.get_value("apps_ftpdownloadsPassword_text")
        if return_value == '01234567890123456789012345678901':
            self.log.info("Only 32 characters are accepted")
        else:
            self.log.error("More than 32 characters are accepted")

        # Verifying max number of selected files/folders
        self.log.info("Verifying max number of selected files/folders")
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.input_text('apps_ftpdownloadsUsername_text', ftp_user, True)
        self.input_text('apps_ftpdownloadsPassword_text', ftp_password, True)
        self.click_element('apps_ftpdownloadsSourcepath_button', 120)
        time.sleep(5)
        self.click_element('link=ftpshare')
        time.sleep(5)
        item_count = 1

        visible = False

        self.log.info(visible)
        while visible is False:
            self.log.info("Clicking item: {0}".format(item_count))
            item_count += 1

            self.click_element("//div[@id='folder_selector']/ul/li[3]/ul/li[{0}]/label/span".format(item_count))
            self.drag_and_drop_by_offset('css=div.jspDrag', 0, 7)
            visible = self.is_element_visible('popup_ok_button')
            self.log.info(visible)

        if self.is_element_visible('popup_ok_button'):
            error_message = self.get_text('//div[6]/div[3]/div/ul/li')
            self.log.info(error_message)
            self.click_element('popup_ok_button')

        self.log.info("Loop count: {0}".format(item_count))
        if item_count == 27:
            self.log.info("Error message displayed as expected: {0}".format(error_message))
        else:
            self.log.error("Error message did not display as expected: {0}".format(error_message))

        time.sleep(5)
        self.click_element('home_treeCancel_button')


        # Verify incorrect username/password
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.input_text('apps_ftpdownloadsUsername_text', ftp_user, True)
        self.input_text('apps_ftpdownloadsPassword_text', wrong_ftp_password, True)

        self.click_element('apps_ftpdownloadsSourcepath_button', 180)
        time.sleep(10)
        my_text = self.get_text('//div[3]/div/div[2]/div/div/div')

        if my_text == credential_error:
            self.log.info('Error message displayed as expected: {0}'.format(my_text))
        else:
            self.log.error('Wrong password error message did not display as expected: {0}'.format(my_text))

        if self.is_element_visible('home_treeCancel_button'):
            self.click_element('home_treeCancel_button')

        # Verify wrong host IP
        self.input_text('apps_ftpdownloadsHost_text', wrong_IP, True)
        self.input_text('apps_ftpdownloadsUsername_text', ftp_user, True)
        self.input_text('apps_ftpdownloadsPassword_text', ftp_password, True)

        self.click_element('apps_ftpdownloadsSourcepath_button', 180)
        time.sleep(10)
        my_text = self.get_text('//div[3]/div/div[2]/div/div/div')

        if my_text == host_error:
            self.log.info('Error message displayed as expected: {0}'.format(my_text))
        else:
            self.log.error('Wrong host error message did not display as expected: {0}'.format(my_text))

        if self.is_element_visible('home_treeCancel_button'):
            self.click_element('home_treeCancel_button')

        # Verify wrong port
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.input_text('apps_ftpdownloadsHostPort_text', wrong_port, True)
        self.input_text('apps_ftpdownloadsUsername_text', ftp_user, True)
        self.input_text('apps_ftpdownloadsPassword_text', ftp_password, True)

        self.click_element('apps_ftpdownloadsSourcepath_button', 180)
        time.sleep(10)
        my_text = self.get_text('//div[3]/div/div[2]/div/div/div')

        if my_text == port_error:
            self.log.info('Error message displayed as expected: {0}'.format(my_text))
        else:
            self.log.error('Wrong port error message did not display as expected: {0}'.format(my_text))

        if self.is_element_visible('home_treeCancel_button'):
            self.click_element('home_treeCancel_button')

        time.sleep(10)
        """

        # Create FTP job
        self.log.info("Creating FTP job...")
        self.input_text('apps_ftpdownloadsTaskName_text', ftp_file, True)
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.input_text('apps_ftpdownloadsHostPort_text', ftp_port, True)
        self.input_text('apps_ftpdownloadsUsername_text', ftp_user, True)
        self.input_text('apps_ftpdownloadsPassword_text', ftp_password, True)

        self.click_element('apps_ftpdownloadsSourcepath_button', 120)
        time.sleep(5)
        self.click_element('link=ftpshare')
        time.sleep(1)
        # self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li[3]/ul/li[2]/label/span')
        # self.click_element('//div[@id=\'folder_selector\']/ul/li[3]/ul/li[2]/label/span')
        self.click_element_at_coordinates("link=AFP-74BD.mp4", -100, 0)

        self.click_element('home_treeOk_button')

        self.click_element('apps_ftpdownloadsDestpath_button', 120)
        time.sleep(5)
        # self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li/label/span')
        # self.click_element('//div[@id=\'folder_selector\']/ul/li/label/span')
        self.click_element_at_coordinates('link=Public', -70, 0)
        self.click_element('home_treeOk_button')

        self.click_element('apps_ftpdownloadsCreate3_button')
        self.click_element('apps_ftpdownloadsGoJob1_button')
        time.sleep(10)

        # refresh page
        if self.is_element_visible('apps_ftpdownloads_link'):
            self.click_element('apps_ftpdownloads_link')
        elif self.is_element_visible('//li[@id=\'ftp_downloads\']/span'):
            self.click_element('//li[@id=\'ftp_downloads\']/span')

        my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
        self.log.info('Current ftp downloading status: {0}'.format(my_text))
        max_wait = 0
        while my_text != "Download Completed" and max_wait < 15:
            time.sleep(60)
            max_wait += 1
            self.log.info('Waiting for ftp download, loop count: {0}'.format(max_wait))
            self.click_element('//li[@id=\'ftp_downloads\']/span')
            my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
            self.log.info('Current ftp downloading status: {0}'.format(my_text))

        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path, ftp_filename)
        if file_md5_sum == ftpdownloadedfilesum:
            self.log.info('FTP file download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP file download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

        self.log.info('Delete downloaded file')
        self.execute(rm_mp4)

        # refresh page
        if self.is_element_visible('apps_ftpdownloads_link'):
            self.click_element('apps_ftpdownloads_link')
        elif self.is_element_visible('//li[@id=\'ftp_downloads\']/span'):
            self.click_element('//li[@id=\'ftp_downloads\']/span')

        self.click_element('apps_ftpdownloadsDelJob1_button')
        while self.is_element_visible('popup_apply_button') is False:
            time.sleep(5)
            self.click_element('apps_ftpdownloadsDelJob1_button')

        self.click_element('popup_apply_button')

    # FTP downloader 2.0
    # Test case 2: Download folder with no authentication, scheduled
    def ftp_downloader_2_0_scheduled(self, ftp_server_ip):
        # Change time
        self.execute('date -s 2015.01.05-01:57:00')

        self.close_webUI()
        self.access_webUI()

        if self.is_element_visible('nav_downloads_link'):
            self.log.info('Testing as regular user')
            self.click_element('nav_downloads_link')
            self.click_element('//li[@id=\'ftp_downloads\']/span')
            # self.click_element('ftp_downloads')
        else:
            self.get_to_page('Apps')
            self.click_element('apps_ftpdownloads_link')

        self.click_element('apps_ftpdownloadsCreate_button')

        """
        self.log.info("Verify default FTP port")
        return_value = self.get_value("apps_ftpdownloadsHostPort_text")
        if return_value == '21':
            self.log.info("Default FTP port value: {0}".format(return_value))
        else:
            self.log.error("Error, default FTP port value: {0}".format(return_value))

        self.input_text('apps_ftpdownloadsHostPort_text', "22", True)
        return_value = self.get_value("apps_ftpdownloadsHostPort_text")
        if return_value == '22':
            self.log.info("FTP port can be modified, value: {0}".format(return_value))
        else:
            self.log.error("Error, FTP port can not be modified, value: {0}".format(return_value))
        self.input_text('apps_ftpdownloadsHostPort_text', "21", True)

        self.log.info("Verify FTP host max number of characters")
        self.input_text('apps_ftpdownloadsHost_text', "01234567890123456789012345678901234", True)
        return_value = self.get_value("apps_ftpdownloadsHostPort_text")
        if return_value == '01234567890123456789012345678901':
            self.log.info("Only 32 characters are accepted")
        else:
            self.log.error("More than 32 characters are accepted")

        self.log.info("Verify un/acceptable characters for Job Name")
        unacceptable_characters = ['#', '>', '.', ' ', '?', '"', '$', '%', '/', '*', '(', ')', '[', '!', '@', '&', '^']

        for character in unacceptable_characters:
            self.log.info("Checking character: {0}".format(character))
            self.input_text('apps_ftpdownloadsTaskName_text', character, True)
            self.click_element('apps_ftpdownloadsCreate3_button')
            # add checking for error message
            self.click_element('popup_ok_button')
            time.sleep(1)

        self.log.info("Checking maximum number of characters - 16")
        self.input_text('apps_ftpdownloadsTaskName_text', "012345678901234567", True)
        self.click_element('apps_ftpdownloadsCreate3_button')
        self.click_element('popup_ok_button')
        time.sleep(1)
        return_value = self.get_value("apps_ftpdownloadsTaskName_text")
        self.log.info(return_value)
        if return_value == "0123456789012345":
            self.log.info("Maximum number of characters for job name is 16")
        else:
            self.log.error("Maximum number of characters for job name is not 16")

        self.log.info("Checking no character")
        self.input_text('apps_ftpdownloadsTaskName_text', "", True)
        self.click_element('apps_ftpdownloadsCreate3_button')
        error_message = self.get_text("//ul[@id='error_dialog_message_list']/li")
        self.log.info(error_message)
        self.click_element('popup_ok_button')
        if error_message == "Enter the Job Name.":
            self.log.info("No character is not acceptable")
        else:
            self.log.error("Failed to check for no character")
        """

        # Switch recurring backup on if it's off
        if self.is_element_visible('backups_InternalBackupWeekly_button'):
            self.log.info("Recurring backup is already on")
        else:
            self.log.info("Turn on recurring backup")
            self.click_element('css = #apps_ftpdownloadsRecurring_switch+span .checkbox_container')

        """
        # Verify options are available for each schedule type
        # Monthly
        self.log.info('Verifying monthly page')
        self.click_element('backups_InternalBackup Monthly _button')
        time.sleep(3)

        self.log.info('Verifying monthly - AM/PM drop down menu')
        dropdown_menu = 'id_pm'
        dropdown_menu_items = ['link=AM', 'link=PM']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)
        """

        """ skip for now
        self.log.info('Verifying monthly - hour drop down menu')
        dropdown_menu = 'id_sch_hour'
        dropdown_menu_items = ['link=1:00', 'link=2:00', 'link=3:00', 'link=4:00', 'link=5:00', 'link=6:00',
                               'link=7:00', 'link=8:00', 'link=10:00', 'link=11:00', 'link=12:00', 'link=9:00']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)


        self.log.info('Verifying monthly - date drop down menu')
        dropdown_menu = 'id_sch_day'
        dropdown_menu_items = ['link=01', 'link=02', 'link=03', 'link=04', 'link=05', 'link=06', 'link=12', 'link=08',
                               'link=09', 'link=10', 'link=07', 'link=11', 'link=17', 'link=14', 'link=13', 'link=16',
                               'link=15', 'link=24', 'link=19', 'link=23', 'link=21', 'link=18', 'link=20', 'link=22',
                               'link=28', 'link=26', 'link=27', 'link=25']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)
        """

        # Weekly
        self.log.info('Verifying weekly window')
        self.click_element('backups_InternalBackupWeekly_button')
        time.sleep(3)

        self.log.info('Verifying weekly - AM/PM drop down menu')
        dropdown_menu = 'id_pm'
        dropdown_menu_items = ['link=AM', 'link=PM']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)

        """
        self.log.info('Verifying weekly - hour drop down menu')

        dropdown_menu = 'id_sch_hour'
        dropdown_menu_items = ['link=1:00', 'link=2:00', 'link=3:00', 'link=4:00', 'link=5:00', 'link=6:00',
                               'link=7:00', 'link=8:00', 'link=10:00', 'link=11:00', 'link=12:00', 'link=9:00']

        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)


        self.log.info('Verifying weekly - day drop down menu')
        dropdown_menu = 'id_sch_week'
        dropdown_menu_items = ['link=MON', 'link=TUE', 'link=WED', 'link=THU', 'link=FRI', 'link=SAT', 'link=SUN']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item, scroll_value=7)
        """

        # Daily
        self.log.info('Verifying daily window')
        self.click_element('backups_InternalBackupDaily_button')
        time.sleep(3)

        self.log.info('Verifying daily - AM/PM drop down menu')
        dropdown_menu = 'id_pm'
        dropdown_menu_items = ['link=AM', 'link=PM']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)

        """
        self.log.info('Verifying daily - hour drop down menu')
        dropdown_menu = 'id_sch_hour'
        dropdown_menu_items = ['link=1:00', 'link=2:00', 'link=3:00', 'link=4:00', 'link=5:00', 'link=6:00',
                               'link=7:00', 'link=8:00', 'link=10:00', 'link=11:00', 'link=12:00', 'link=9:00']
        for dropdown_item in dropdown_menu_items:
            self.select_item_drop_down_menu(dropdown_menu, dropdown_item)
        """

        """
        # Verify only 25 jobs are allowed
        loop_count = 0
        visible = False
        while visible is False:
            self.click_element('apps_ftpdownloadsCreate_button')
            self.input_text('apps_ftpdownloadsTaskName_text', loop_count, True)
            self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
            self.click_element('apps_ftpdownloadsSourcepath_button', 120)
            time.sleep(5)
            self.click_element('link=ftpshare')
            time.sleep(3)
            self.wait_until_element_is_clickable("//div[@id='folder_selector']/ul/li[3]/ul/li[2]/label/span")
            self.click_element("//div[@id='folder_selector']/ul/li[3]/ul/li[2]/label/span")
            self.click_element('home_treeOk_button')

            self.click_element('apps_ftpdownloadsDestpath_button', 120)
            time.sleep(5)
            self.wait_until_element_is_clickable('//div[@id=\'folder_selector\']/ul/li/label/span')
            self.click_element('//div[@id=\'folder_selector\']/ul/li/label/span')
            self.click_element('home_treeOk_button')

            self.click_element('apps_ftpdownloadsCreate3_button')
            loop_count += 1
            time.sleep(5)

        if self.is_element_visible('popup_ok_button'):
            error_message = self.get_text('//div[6]/div[3]/div/ul/li')
            self.log.info(error_message)
            self.click_element('popup_ok_button')
            visible = True

        if loop_count == 25:
            self.log.info("Error message displayed as expected: {0}".format(error_message))
        else:
            self.log.error("Error message did not display as expected: {0}".format(error_message))

        time.sleep(5)
        if self.is_element_visible('home_treeCancel_button'):
            self.click_element('home_treeCancel_button')
        """

        self.log.info('FTP download a folder')
        self.input_text('apps_ftpdownloadsTaskName_text', ftp_folder, True)
        self.input_text('apps_ftpdownloadsHost_text', ftp_server_ip, True)
        self.click_element('apps_ftpdownloadsSourcepath_button', 120)
        time.sleep(5)
        self.click_element('link=Public', 120)
        time.sleep(5)
        self.click_element_at_coordinates('link=FTPTest', -70, 0)
        time.sleep(5)
        self.click_element('home_treeOk_button')
        self.click_element('apps_ftpdownloadsDestpath_button', 120)
        time.sleep(5)
        self.click_element_at_coordinates('link=Public', -70, 0)
        self.click_element('home_treeOk_button')

        self.log.info('Schedule at 2AM, daily')
        self.click_element('id_sch_hour')
        time.sleep(3)
        self.click_element('link=2:00')
        time.sleep(3)
        self.click_element('apps_ftpdownloadsCreate3_button')

        self.log.info("Waiting 3 minutes for timer to start")
        time.sleep(180)
        my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
        self.log.info('Current ftp downloading status: {0}'.format(my_text))
        max_wait = 0
        while my_text != "Download Completed" and max_wait < 15:
            time.sleep(60)
            max_wait += 1
            self.log.info('Waiting for ftp download, loop count: {0}'.format(max_wait))
            my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
            self.log.info('Current ftp downloading status: {0}'.format(my_text))

        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path_20, ftp_filename_2)
        if file_md5_sum_2 == ftpdownloadedfilesum:
            self.log.info('FTP folder download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP folder download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

        self.log.info('Delete downloaded folder')
        self.execute(rm_ftp_test)

        self.execute('date -s 2015.01.05-12:57:00')

        self.log.info('Modify to schedule at 1AM')
        # Refresh page
        if self.is_element_visible('apps_ftpdownloads_link'):
            self.click_element('apps_ftpdownloads_link')
        elif self.is_element_visible('//li[@id=\'ftp_downloads\']/span'):
            self.click_element('//li[@id=\'ftp_downloads\']/span')

        self.click_element('apps_ftpdownloadsModifyJob1_button')
        time.sleep(3)
        self.click_element('id_sch_hour')
        time.sleep(3)
        self.click_element('link=1:00')
        # dropdown_menu = 'id_sch_hour'
        # dropdown_item = 'link=1:00'
        # self.select_item_drop_down_menu(dropdown_menu, dropdown_item)
        time.sleep(3)
        self.click_element('apps_ftpdownloadsSave3_button')

        self.log.info("Waiting 3 minutes for timer to start")
        time.sleep(180)
        my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
        self.log.info('Current ftp downloading status: {0}'.format(my_text))
        max_wait = 0
        while my_text != "Download Completed" and max_wait < 15:
            time.sleep(60)
            max_wait += 1
            self.log.info('Waiting for ftp download, loop count: {0}'.format(max_wait))
            my_text = self.get_text('//table[@id=\'jobs_list\']/tbody/tr/td[6]/div')
            self.log.info('Current ftp downloading status: {0}'.format(my_text))

        # Verify download successfully via checksum comparison
        ftpdownloadedfilesum = self.md5_checksum(ftp_file_path_20, ftp_filename_2)
        if file_md5_sum_2 == ftpdownloadedfilesum:
            self.log.info('FTP folder download successfully, file checksum: {0}'.format(ftpdownloadedfilesum))
        else:
            self.log.error('FTP folder download failed, file checksum: {0}'.format(ftpdownloadedfilesum))

        self.log.info('Delete downloaded folder')
        self.execute(rm_ftp_test)

        self.log.info('Delete FTP job')

        if self.is_element_visible('apps_ftpdownloads_link'):
            self.click_element('apps_ftpdownloads_link')
        elif self.is_element_visible('//li[@id=\'ftp_downloads\']/span'):
            self.click_element('//li[@id=\'ftp_downloads\']/span')

        self.click_element('apps_ftpdownloadsDelJob1_button')
        while self.is_element_visible('popup_apply_button') is False:
            time.sleep(5)
            self.click_element('apps_ftpdownloadsDelJob1_button')

        self.click_element('popup_apply_button')

    def disable_ntp(self):
        self.log.info('Disable NTP')
        self.set_date_time_configuration(datetime='', ntpservice=False, ntpsrv0='', ntpsrv1='', ntpsrv_user='',
                                         time_zone_name='US/Pacific')

    def enable_ntp(self):
        self.log.info('Enable NTP')
        self.set_date_time_configuration(datetime='1421057768', ntpservice=True, ntpsrv0='time.windows.com',
                                         ntpsrv1='pool.ntp.org', ntpsrv_user='', time_zone_name='US/Pacific')

HttpFtpDownloader()