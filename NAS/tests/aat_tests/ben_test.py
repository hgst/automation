import signal
import time
import sys

from testCaseAPI.src.testclient import TestClient

class signalTest(TestClient):
    original_sigint = None
    
    def run(self):
        self.set_signals()
        timer = 0
        while True:
            if timer > 300:
                break
            time.sleep(1)
            timer += 1
            self.log.info("a")
    
    def exit_gracefully(self, signum, frame):
        # restore the original signal handler as otherwise evil things will happen
        # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
        global original_sigint
        signal.signal(signal.SIGINT, original_sigint)
        self.log.info("signum:", signum)
        sys.exit(1)
    
        # restore the exit gracefully handler here
        signal.signal(signal.SIGINT, exit_gracefully)
    
    def set_signals(self):
        global original_sigint
        original_sigint = signal.getsignal(signal.SIGINT)
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        signal.signal(signal.SIGALRM, self.exit_gracefully)
    
signalTest()