from testCaseAPI.src.testclient import TestClient
# create a TESTClient object
#test = TestClient()
import os
from xml.etree import ElementTree

from global_libraries import CommonTestLibrary as ctl



class MyClass(TestClient):
    def run(self):
        self.log.error('This is an error')
        self.log.warning('This is a warning')
        self.log.warning('This is another warning')
        self.log.exception('I had an exception')
        self.log.exception('I had another exception')
        self.log.exception('I had yet another exception')

    def get_firmware_version_number(self):
        return '1.00.50'

    def _build_outputxml(self, errors, warnings, exceptions):
        print 'errors, warnings, exceptions:'
        print errors, warnings, exceptions
        directory = ctl.get_silk_results_dir()
        outputfile = os.path.join(directory, 'output.xml')
        self.log.debug(outputfile)
        testname = ctl.get_silk_test_name()

        test = ElementTree.Element('Test')
        test.attrib['TestItem'] = testname

        run_info = ElementTree.SubElement(test, 'RunInfo')
        run_info.attrib['Version'] = str(self.get_firmware_version_number())

        # WasSuccess
        wassuccess = ElementTree.SubElement(test, 'WasSuccess')
        if errors == 0:
            wassuccess.text = 'True'
        else:
            wassuccess.text = 'False'

        # Build
        build = ElementTree.SubElement(test, 'Version')
        build.text = str(self.get_firmware_version_number())

        # RunComment
        build = ElementTree.SubElement(test, 'RunComment')
        build.text = 'My Comment'

        # RunCount
        runcount = ElementTree.SubElement(test, 'RunCount')
        runcount.text = str('1')

        # FailureCount
        failurecount = ElementTree.SubElement(test, 'FailureCount')
        if errors > 0:
            failurecount.text = '1'
        else:
            failurecount.text = '0'

        # ErrorCount
        errorcount = ElementTree.SubElement(test, 'ErrorCount')
        errorcount.text = str(errors)

        # WarningCount
        warningcount = ElementTree.SubElement(test, 'WarningCount')
        warningcount.text = str(warnings)

        # Asserts
        asserts = ElementTree.SubElement(test, 'Asserts')
        asserts.text = str(exceptions)

        xmlstring = ElementTree.tostring(test)

        self.log.debug('Output.xml is ' + str(outputfile))
        outfile = open(outputfile, 'w')
        outfile.write(xmlstring)
        outfile.close()



MyClass()










