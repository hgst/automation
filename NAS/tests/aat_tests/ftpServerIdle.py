""" 
    FTP Server Idle Time
    - Check default idle time
    - Check custom idle time

    @Author: lin_ri
    wiki URL: http://wiki.wdc.com/wiki/FTP
    
    Automation: Full
    
    Tested Product:
        Lightning
    
    Test Status:
              Local Ligtning: Pass (FW: 1.05.30)
        TW Jenkins Lightning: Pass (FW: 1.05.30)
""" 
from testCaseAPI.src.testclient import TestClient
from global_libraries.testdevice import Fields
import xml.etree.ElementTree as ET
from ftplib import FTP
from ftplib import error_perm
import time
import os
import shutil

picturecounts = 0

class ftpServerIdle(TestClient):
    
    def run(self):
        self.log.info('######################## FTP Server Idle TESTS START ##############################')
        try:
            self.delete_all_shares()  # Clean all shares except Public, SmartWare and TimeMachineBackup
            self.access_webUI()
            self.configure_ftp()
            self.config_public_share(access="Anonymous Read / Write")
            # check default idle time
            self.log.info("=== Check Default Idle Time ===")
            self.config_idle_time(idle=10)
            self.test_ftp_idle_connection(waitmin=10)
            self.test_anonymous_rw_access(folder='Public')
            self.close_webUI()
            # Check custom idle time
            self.log.info("=== Check Custom Idle time ===")
            self.access_webUI()
            self.config_idle_time(idle=2)
            self.close_webUI()
            self.test_ftp_idle_connection(waitmin=2)
            self.test_anonymous_rw_access(folder='Public')
        except Exception as e:
            self.log.error("ERROR: ftpServerIdle exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='run')
        finally:
            self.log.info('######################## FTP Server Idle TESTS END ##############################')

    def configure_ftp(self):
        """
            Configure FTP
        """
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            self.log.info('==> Configure ftp')
            self.wait_until_element_is_visible('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                               timeout=15)
            ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
            if ftp_status_text == 'ON':
                self.log.info('FTP is enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.info('Turn ON FTP service')
                self.click_wait_and_check('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                          'popup_ok_button', visible=True)
                self.click_wait_and_check('popup_ok_button', visible=False)
                ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
        except Exception as e:
            self.log.exception('Failed: Unable to configure ftp!! Exception: {}'.format(e))
            self.takeScreenShot(prefix='enable_ftp_service')
        else:
            if ftp_status_text == 'ON':
                self.log.info("FTP is enabled successfully. Status = {}".format(ftp_status_text))
            else:
                self.log.error("Unable to turn on FTP service, now is set to {}".format(ftp_status_text))
            self.close_webUI()
    
    def config_public_share(self, access="Anonymous None"):
        """
            Configure public share to the access value passed by the argument
            
            @access: The access permission of public share for Anonymous user
        """
        try:              
            self.log.info('==> Configure FTP share with {}'.format(access))
            # Add public share if it does not exist
            self.log.info("[Create Public share if not exists]")
            self.create_shares(number_of_shares=1, share_name='Public', force_webui=True)
            self.update_share_network_access(share_name='Public', public_access=True)
            self.reload_page()
            # end of public share addition
            self.get_to_page('Shares')
            time.sleep(2)
            self.click_wait_and_check('nav_shares_link', 'shares_createShare_button', visible=True, timeout=15)
            self.log.info('Click Public')
            self.wait_until_element_is_visible('shares_share_Public', timeout=30)
            self.click_wait_and_check('shares_share_Public',
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            time.sleep(2)
            self.log.info('Switch the ftp to ON')
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_wait_and_check('css=#shares_ftp_switch+span .checkbox_container', 'ftp_dev', visible=True)
            self.log.info('ftp service is enabled to ON')
            self.click_link_element('ftp_dev', linkvalue=access)
            self.click_wait_and_check('shares_ftpSave_button', visible=False)
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)  # Waiting for Saving
            time.sleep(3)
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. exception: {}".format(repr(e)))
            self.log.info("Available volumes: {0}".format(self.get_xml_tags(self.get_volumes()[1], 'base_path')))
            self.takeScreenShot(prefix='ConfigPublicShare')

    def config_idle_time(self, idle=5):
        """ 
            Config default idle time
            @idle: Unit (minutes)
        """
#         self.log.info("==> Configure default idle time to {}".format(local_ip))
        try:
            self.get_to_page('Settings')
            self.click_wait_and_check('nav_settings_link', 'settings_general_link', visible=True)
            self.click_wait_and_check('network', 'css=#network.LightningSubMenuOn', visible=True)
            time.sleep(3)
            self.log.info("==> Click configure")
            self.click_wait_and_check('css=#settings_networkFTPAccessConfig_link > span:nth-child(1)',
                                      'css=#settings_networkFTPAccessNext1_button', visible=True, timeout=5)
            self.log.info("Set Idle Time to {}".format(idle))
            self.input_text_check('settings_networkFTPAccessIdleTime_text', str(idle), False)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext1_button',
                                      'css=#settings_networkFTPAccessNext2_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext2_button',
                                      'css=#settings_networkFTPAccessNext3_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext3_button',
                                      'css=#settings_networkFTPAccessNext4_button', visible=True, timeout=5)
            self.click_wait_and_check('css=#settings_networkFTPAccessNext4_button', visible=False, timeout=30)
            self.log.info("FTP Idle time is set to {} minutes.".format(idle))
            self.wait_until_element_is_not_visible(self.Elements.UPDATING_STRING)  # Waiting for updating
            time.sleep(3)
        except Exception as e:
            self.log.error("ERROR: Failed to configure idle time, exception: {}".format(repr(e)))
            self.takeScreenShot(prefix='configIdleTime')

    def test_ftp_idle_connection(self, waitmin=10):
        """
            Verify ftp connection alive after 10 or specified minutes wait
            
            @waitmin: how long the ftp socket stays idle
        """
        import socket
        FTP_IP = self.uut[Fields.internal_ip_address]
        retry = 3
        ftpconn = None
        while retry > 0:
            try:
                ftpconn, ex = self.ftpConnection(anonymous=True)
                if ex:
                    raise ex
            except Exception as e:
                retry = retry - 1
                self.log.info("FTP: Retrying {} time".format(3-retry))
                if retry == 0:
                    self.log.error("Failed 3 retries to establish FTP connection to {}, exception: {}".format(FTP_IP, repr(e)))
                    raise
                time.sleep(1) # Wait 1 sec before next retry
                continue
            else:
                break
        self.log.info("FTP socket is open but waits for {} minutes to check if connection is still there".format(waitmin))        
        time.sleep(waitmin*60 + 10)  # 10min
        try:
            ftpconn.nlst()
        except (EOFError,  socket.error, IOError) as e:
            self.log.info("PASS: FTP service is unavailable due to idle {} mins, exception: {}".format(waitmin, repr(e)))
        except Exception as e:
            self.log.error("FAILED: exception: {}".format(repr(e)))
        else:
            self.log.info(ftpconn.nlst())
            self.log.error("FAILED: No exception, FTP Idle connection test failed.")
        ftpconn.close()
        
    def test_anonymous_rw_access(self, folder='Public', retry=3):
        """
            Validate anonymous r/w access
            
            @folder: The folder name anonymous user will browse            
        """
        self.log.info("-- write test --")
        while retry >= 0:
            try:
                ftpconn, ex = self.ftpConnection(anonymous=True)
                self.ftpWriteTest(ftpconn, share_folder=folder, delete_after_upload=True)
            except Exception as e:
                if retry == 0:
                    self.log.error('FTP Write ERROR: Reach maximum retries due to exception {}'.format(repr(e)))
                    raise
                self.log.info("FTP Write exception: {}, remaining {} retries...".format(repr(e), retry))
                retry -= 1
                time.sleep(1)
            else:
                break
        self.log.info("-- read test --")
        try:
            ftpconn, ex = self.ftpConnection(anonymous=True)
            self.ftpReadTest(ftpconn, share_folder=folder, delete_after_download=False)
        except Exception as e:
            self.log.error(repr(e))

    def ftpConnection(self, ftp_ip="", new_user_name='user1', new_user_pwd='password1', anonymous=True):
        """
            Create ftp connection and login by user account or anonymous
            Once login, it will return the ftp socket back and exception
            
            @ftp_ip: '192.168.1.91' or self.uut[Fileds.internal_ip_address]
            @anonymous: True (Anonymous login)
                        User login: new_user_name / new_user_pwd (Set to anonymous=False)
                        If you do not set anonymous to False, it always login as anonymous user
            
            Need to set anonymous=False to switch to user login mode
            @new_user_name: user account
            @new_user_pwd: user password               
            
            return (ftpconn, exception)
            
            Usage:
            ftpconn, ex = self.ftpConnection('192.168.1.91', new_user_name='user1', new_user_pwd='password1', anonymous=False)
            ftpconn, ex = self.ftpConnection('192.168.1.91', anonymous=True)
        """
        ex = None
        if ftp_ip == "":
            ftp_ip = self.uut[Fields.internal_ip_address]
            
        try:
            ftpconn = FTP(ftp_ip)
        except Exception as ex:
            self.log.info("Warning: Unable to connect to FTP: {}, exception: {}".format(ftp_ip, repr(ex)))
            ftpconn = None
            return (ftpconn, ex)

        try:
            if anonymous:
                resp = ftpconn.login()
            else:
                resp = ftpconn.login(new_user_name, new_user_pwd)
        except error_perm as ex:
            self.log.info("Warning: Unable to login ftp, exception: {}".format(repr(ex)))
            return (ftpconn, ex)
        except Exception as ex:
            self.log.info("FAILED: ftp login failed, exception: {}".format(repr(ex)))
            return (ftpconn, ex)
                                
        if '230' in resp:
            if anonymous:
                self.log.info("Anonymous login succeed")
            else:
                self.log.info("{} login succeed".format(new_user_name))
        else:
            if anonymous:
                self.log.error("Anonymous login failed")
            else:
                self.log.error("{} login failed".format(new_user_name))
        
        return (ftpconn, ex)

    def ftpReadTest(self, ftpconn, filesize=10240, share_folder='Public', delete_after_download=False):
        """
            Pre-requiste:
                You need to have temporary file called 'file0.jpg' under the share_folder for test
                Normally, run the upload(write) test first to generate the temporary file there
                Then, execute the download(read) test
             
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_download: True (Delete the temporary file after downloading test)
        """
        resp = ftpconn.cwd(share_folder) # switch to 'Public' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))        
        
        # download file0.jpg as file_check.jpg
        tmpfile = 'file0.jpg' # temporary file on FTP server       
        download_file = "download_file0.jpg" # filename which is saved locally

        # if download_file exists, removes it
        if os.path.isfile(download_file):
            os.remove(download_file)

        # Check if tmpfile exists or not
        listfiles = ftpconn.nlst()
        if tmpfile in listfiles:
            self.log.info("{} exists, read test is ready to go".format(tmpfile))
        else:
            raise Exception("FAILED: {} does not exist for read test, STOP!".format(tmpfile))
    
        self.log.info("*** Going to download file for read access test ***")
        
        try:
            with open(download_file, "wb") as f:
                def callback(data):
                    f.write(data)
                resp = ftpconn.retrbinary("RETR "+tmpfile, callback, blocksize=1048576)
            if '226' in resp:
                self.log.info("{} is saved locally".format(download_file))
                self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("FAILED: {} download failed".format(download_file))
            
        except Exception as e:
            raise Exception("FAILED: ftpReadTest failed, can not download {}, exception: {}".format(tmpfile, repr(e)))
        ftpconn.close()
        
        if delete_after_download:
            os.remove(download_file)
            self.log.info("Cleaning temporary files: {}".format(os.path.abspath(download_file)))
        
        self.log.info("*** Read access test END ***")                 
    
    def ftpWriteTest(self, ftpconn, filesize=10240, share_folder='Public', delete_after_upload=True):
        """
            Generate temporary file called file0.jpg for uploading test
            
            @ftpconn: ftp connection socket
            @filesize: 10240 KB (10MB)
            @share_folder: The foldername you want to download the file
            @delete_after_upload: True (Delete the temporary file after uploading test)
        """
        resp = ftpconn.cwd(share_folder) # switch to 'Public' folder
        if '250' in resp:
            self.log.info("Switch to {} folder".format(share_folder))
            self.log.info("Current Working directory: {}".format(ftpconn.pwd()))
            self.log.info('Available Directories: ')
            ftpconn.dir()
        else:
            self.log.error("Unable to switch to {} folder".format(share_folder))   
        
        tmp_file = os.path.abspath(self.generate_test_file(filesize))
        tmpfile = ''.join(tmp_file)
        self.log.info("Generate temporary file: {}".format(tmpfile))
        time.sleep(5)  # Wait for temporary file generation
                    
        self.log.info("** Going to upload temporary file for write access test **")
        # upload file0.jpg
        try:
            with open(tmpfile, "rb") as f:
                self.log.info("Write {} to ftp".format(os.path.basename(tmpfile)))
                resp = ftpconn.storbinary("STOR "+os.path.basename(tmpfile), f, 1048576) # 1048576 = 1MB
        except error_perm:
            self.get_ftp_anonymous_access(sharename=share_folder)
            self.log.info("Current Working directory: {}".format(ftpconn.pwd()))
            self.log.info('Available Directories:')
            ftpconn.dir()
        except Exception as e:
            raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. exception: {}".format(os.path.basename(tmpfile), repr(e)))
        else:
            if '226' in resp: # File successfully transferred
                self.log.info("{} is uploaded successfully".format(os.path.basename(tmpfile)))
                self.log.info("Transferred speed: "+resp.split(',')[1].strip())
            else:
                self.log.error("Upload {} failed".format(os.path.basename(tmpfile)))
            ftpconn.close()
        
        #os.chdir('..') # switch to parent directory instead of staying in temporary folder
        if delete_after_upload:
            shutil.rmtree(os.path.dirname(tmpfile))
            self.log.info("Cleaning temporary files: {}".format(tmpfile))
        
        self.log.info("*** Write access test END ***")

    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait
        """
        locator = self._sel._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            # visible = False # Target locator is invisible after being clicked by default behavior
        else:
            check_locator = self._sel._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if {}".format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    def click_link_element(self, locator, linkvalue, timeout=5, retry=3):
        """
        :param locator: the link locator
        :param linkvalue: the locator link value you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception as e:
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def get_ftp_anonymous_access(self, sharename='Public'):
        readlist = None
        writelist = None
        xml_contents = self.execute('cat /etc/NAS_CFG/ftp.xml')[1]
        root = ET.fromstring(xml_contents)
        for item in root.findall('.//item'):
            share = item.find('name').text
            if share == sharename:
                readlist = item.find('read_list').text
                writelist = item.find('write_list').text
                self.log.debug("READ list: {}".format(readlist))
                self.log.debug("WRITE list: {}".format(writelist))
                break
        if writelist and 'ftp' in writelist:
            self.log.info("{} share has [anonymous r/w] access".format(sharename))
        elif readlist and 'ftp' in readlist:
            self.log.info("{} share has [anonymous read only] access".format(sharename))
        else:
            self.log.info("{} share has [anonymous none] access".format(sharename))

    def takeScreenShot(self, prefix=None):
        """
            Take Screen Shot
        """
        from global_libraries.CommonTestLibrary import get_silk_results_dir
        global picturecounts
        if not prefix:
            prefix = os.path.splitext(__file__)[0]
        filename = '{}_error_{}.png'.format(prefix, picturecounts)
        outputdir = get_silk_results_dir()
        path = os.path.join(outputdir, filename)
        self._sel.capture_browser_screenshot(path)
        self.log.info("Take screenshot: {} @ {} ".format(filename, outputdir))
        picturecounts += 1

ftpServerIdle()