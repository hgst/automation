""" Home Page- Quick Links

    @Author: Lee_e

    Objective: verify that button on quick link is work
    Automation: Full

    Supported Products:
        All

    Test Status:
              Local Lightning : PASS (FW: 2.00.223)
                    Lightning : PASS (FW: 2.10.120)
                    KC        : PASS (FW: 2.00.224)
                    Glacier   : PASS (FW: 2.10.116)
              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.130 & 2.00.224)
                              : Yosemite     - PASS (FW: 2.10.130 & 2.00.224)
                              : Kings Canyon - PASS (FW: 2.10.128 & 2.00.224)
                              : Sprite       - PASS (FW: 2.10.130 & 2.00.224)
                              : Zion         - PASS (FW: 2.10.130 & 2.00.224)
                              : Aurora       - PASS (FW: 2.10.130 & 2.00.224)
                              : Yellowstone  - PASS (FW: 2.10.130 & 2.00.224)
                              : Glacier      - PASS (FW: 2.10.127 & 2.00.224)

"""

from testCaseAPI.src.testclient import TestClient
import time


shutdown_warning_message = 'The system will shut down.'
reboot_warning_message = 'reboot the My Cloud system.'
power_switch_model = ['GLCR']
model = ''

class HomePageQuickLinks(TestClient):

    def run(self):
        global model
        model = self.get_model_number_check()
        self.get_mac_address()
        self.log.info('model: {0}'.format(model))
        self.quick_links_dropdown_verify()
        self.quick_links_logout_verify()
        self.quick_links_reboot_verify()
        self.quick_links_reboot_verify(cancel=False, message_check=False)
        if model not in power_switch_model:
            self.quick_links_shutdown_verify()
            self.quick_links_shutdown_verify(cancel=False, message_check=False)

    def tc_cleanup(self):
        self.skip_cleanup = True
        global model
        self.log.info('model: {0}'.format(model))
        if model in power_switch_model:
            self.power_off()
            time.sleep(2)
            self.power_on()
            start_time = time.time()
            while not self.is_ready():
                self.log.debug('Web is not ready')
                if time.time() - start_time > self.uut[self.Fields.boot_time]:
                    raise Exception('Timed out waiting to boot')
        else:
            time.sleep(5)
            self.turn_on_by_wol()
        self.ssh_connect()

    def quick_links_dropdown_verify(self):
        sub_test_name = 'Home page - Quick Links- Dropdown'
        try:
            self.start_test(sub_test_name)
            self.webUI_admin_login()
            self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT, self.Elements.TOOLBAR_LOGOUT_LOGOUT)
            drop_down_text = "Welcome " + self.uut[self.Fields.web_username]
            self.log.info('text: {0}'.format(drop_down_text))
            self.pass_test(sub_test_name, 'message shows on Dropdown is correctly')
            self.close_webUI()
        except Exception as e:
            self.fail_test(sub_test_name, 'message shows on Dropdown is incorrectly: {0}'.format(e))

    def quick_links_shutdown_verify(self, cancel=True, message_check=True):
        self.log.info('Home page - Quick Links- shutdown , cancel: {0}, message_check: {1}'
                      .format(cancel, message_check))
        if cancel:
            self.webUI_admin_login()
        self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT, self.Elements.TOOLBAR_LOGOUT_SHUTDOWN)
        self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_SHUTDOWN, self.Elements.TOOLBAR_LOGOUT_SHUTDOWN_WARNING_DIAG)
        if message_check:
            sub_test_name = 'Home page - Quick Links- shutdown, message_check: {0}'.format(message_check)
            self.start_test(sub_test_name)
            message = self.get_text(self.Elements.TOOLBAR_LOGOUT_SHUTDOWN_WARNING_DIAG)
            if shutdown_warning_message in message:
                self.pass_test(sub_test_name, 'shutdown warning message appear')
            else:
                self.fail_test(sub_test_name, 'shutdown warning message appear incorrectly')
        if cancel:
            sub_test_name = 'Home page - Quick Links- cancel shutdown: {0}'.format(cancel)
            self.start_test(sub_test_name)
            try:
                self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_SHUTDOWN_CANCEL, visible=False)
                self.pass_test(sub_test_name, 'cancel shutdown successfully')
            except Exception as e:
                self.fail_test(sub_test_name, 'Fail to cancel shutdown by UI, exception:{0}'.format(repr(e)))
                self.take_screen_shot('cancel_shutdown')
        else:
            sub_test_name = 'Home page - Quick Links- cancel shutdown: {0}'.format(cancel)
            self.start_test(sub_test_name)
            try:
                self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_SHUTDOWN_OK, self.Elements.TOOLBAR_LOGOUT_SHUTDOWN_DIAG)
                self.pass_test(sub_test_name, 'shutdown by UI successfully')
            except Exception as e:
                self.fail_test(sub_test_name, 'Fail to shutdown by UI with exception {0}'.format(repr(e)))
                self.take_screen_shot('do_shutdown')
            # Loop until the pings stop
            start = time.time()
            while self.is_device_pingable():
                time.sleep(0.5)
                try:
                    if time.time() - start >= self.uut[self.Fields.shutdown_time]:
                        raise Exception('Device failed to power off within {0} seconds'
                                        .format(self.uut[self.Fields.shutdown_time]))
                except Exception as e:
                    self.log.exception(sub_test_name, 'Fail to shutdown within {0} seconds by UI'.format(repr(e)))

    def quick_links_reboot_verify(self, cancel=True, message_check=True):
        self.log.info('Home page - Quick Links- Reboot , cancel: {0}, message_check: {1}'.format(cancel, message_check))
        if cancel:
            self.webUI_admin_login()
        self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT, self.Elements.TOOLBAR_LOGOUT_REBOOT)
        self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_REBOOT, self.Elements.TOOLBAR_LOGOUT_REBOOT_WARNING_DIAG)

        if message_check:
            sub_test_name = 'Home page - Quick Links- reboot, message_check: {0}'.format(message_check)
            self.start_test(sub_test_name)
            message = self.get_text(self.Elements.TOOLBAR_LOGOUT_REBOOT_WARNING_DIAG)
            if reboot_warning_message in message:
                self.pass_test(sub_test_name, 'reboot warning message appear')
            else:
                self.fail_test(sub_test_name, 'reboot warning message appear incorrectly')
                self.take_screen_shot(prefix='reboot_warning_massage')
        if cancel:
            sub_test_name = 'Home page - Quick Links- cancel reboot: {0}'.format(cancel)
            self.start_test(sub_test_name)
            try:
                self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_REBOOT_CANCEL, visible=False)
                self.pass_test(sub_test_name, 'cancel reboot successfully')
            except Exception as e:
                self.fail_test(sub_test_name, 'Fail to cancel reboot by UI, exception:{0}'.format(repr(e)))
                self.take_screen_shot(prefix='cancel_reboot')
        else:
            sub_test_name = 'Home page - Quick Links- cancel reboot: {0}'.format(cancel)
            self.start_test(sub_test_name)
            try:
                self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_REBOOT_OK, self.Elements.TOOLBAR_LOGOUT_REBOOT_DIAG)
                self.pass_test(sub_test_name, 'reboot by UI successfully')
            except Exception as e:
                self.fail_test(sub_test_name, 'unit does not reboot with exception{0} by UI.'.format(repr(e)))
                self.take_screen_shot(prefix='do_reboot')

            # Loop until the pings stop
            start = time.time()
            while self.is_ready():
                time.sleep(0.5)
                try:
                    if time.time() - start >= self.uut[self.Fields.reboot_time]:
                        raise Exception('Device failed to power off within {0} seconds'
                                        .format(self.uut[self.Fields.reboot_time]))
                except Exception as e:
                    self.log.exception('Fail to power down within {0} seconds by UI'.format(repr(e)))
            time.sleep(5)

            start_time = time.time()
            while not self.is_ready():
                self.log.debug('Web is not ready')
                try:
                    if time.time() - start_time > self.uut[self.Fields.reboot_time]:
                        raise Exception('Timed out for waiting {0} seconds to boot'
                                        .format(self.uut[self.Fields.reboot_time]))
                except Exception as e:
                    self.log.exception('Fail to boot up within {0} seconds by UI'.format(repr(e)))
            self._sel.close_all_browsers()
            self.disconnect_user_sessions()
            self.ssh_connect()

    def quick_links_logout_verify(self):
        sub_test_name = 'Home page - Quick Links- Logout'
        try:
            self.start_test(sub_test_name)
            self.webUI_admin_login()
            self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT, self.Elements.TOOLBAR_LOGOUT_LOGOUT)
            self.click_wait_and_check(self.Elements.TOOLBAR_LOGOUT_LOGOUT, self.Elements.LOGIN_BUTTON)
            self.pass_test(sub_test_name, 'logout successfully')
        except Exception as e:
            self.fail_test(sub_test_name, 'System admin fails to log out: {0}'.format(e))
            self.take_screen_shot(prefix='quick_links_logout')

    def turn_on_by_wol(self):
        self.log.info('********** state = turn_on_by_WOL *********')
        retry = 2
        while retry > 0:
            try:
                self.send_magic_packet()
                time.sleep(3)
                start_time = time.time()

                while not self.is_ready():
                    self.log.info('waiting for web ready')
                    time.sleep(20)
                    if time.time() - start_time > self.uut[self.Fields.boot_time]:
                        raise Exception('Timed out for waiting to boot by WoL:{0} seconds'
                                        .format(time.time()-start_time))
                if self.is_ready():
                    self.log.info(' PASS: Wake on Lan test Success and spend time: {0} seconds'
                                  .format(time.time()-start_time))
                    break
            except Exception as e:
                if retry == 0:
                    self.log.exception("Failed to wake on LAN ")
                self.log.info('Retry {0} times to WoL'.format(2-retry))
                retry -= 1
                continue
            else:
                break

    def webUI_admin_login(self):
        """
        :param username: user name you'd like to login
        :param userpwd: user password for user name account
        """
        self.log.info('WebUI Login')
        self.log.info('username:{0}, password:{1}'.format(self.uut[self.Fields.web_username], self.uut[self.Fields.web_password]))
        password = self.uut[self.Fields.web_password]
        self.access_webUI(do_login=False)
        self.input_text_check(self.Elements.LOGIN_USERNAME, self.uut[self.Fields.web_username], do_login=False)
        if password != ('' and None):
            self.input_text_check(self.Elements.LOGIN_PASSWORD, self.uut[self.Fields.web_password], do_login=False)
        self.click_wait_and_check(self.Elements.LOGIN_BUTTON, visible=False)
        self._sel.check_for_popups()

    def get_model_number_check(self, retry=3, attempt_delay=3):
        model_number = ''
        while retry >= 0:
            try:
                model_number = self.get_model_number()
                return model_number
            except Exception as e:
                if retry == 0:
                    self.log.exception('Exception: {0}'.format(repr(e)))
                    self.log.debug('result : {0}'.format(model_number))

                self.log.info('Exception: {0} ,Wait {1} seconds and retry {2} for get model number. '.format(repr(e), attempt_delay, (3-retry)))
                time.sleep(attempt_delay)
                retry -= 1

    def get_mac_address(self):
        self.log.info('mac_address:{0} ip:{1}'.format(self.uut[self.Fields.mac_address], self.uut[self.Fields.internal_ip_address]))
        mac_address = ''.join(self.get_xml_tags(self.get_system_information(), 'mac_address'))
        self.log.info('mac_address rest:{0}'.format(mac_address))
        if mac_address != self.uut[self.Fields.mac_address]:
            self.uut[self.Fields.mac_address] = mac_address
        self.log.info('mac_address :{0} ip:{1}'.format(self.uut[self.Fields.mac_address], self.uut[self.Fields.internal_ip_address]))

HomePageQuickLinks()