"""
title           :usersPageAUserCanBeAdded.py
description     :To verify if a new user can be created
author          :yang_ni
date            :2015/05/13
notes           :
"""

from testCaseAPI.src.testclient import TestClient
import time

class AUserCanBeAdded(TestClient):

    def run(self):

        try:
            # Delete all the users
            self.delete_all_users()

            # Create a user
            self.log.info('Creating a user ...')
            self.create_user(username='user1')

            # Check if user1 is successfully added (from backend)
            user_list = self.get_xml_tags(self.get_all_users()[1], 'username')
            self.log.info('user list: {0}'.format(user_list))
            if 'user1' in user_list:
                self.log.info('User1 is created successfully in backend')
            else:
                self.log.error('User1 is not created successfully in backend')

            # Check if user1 is successfully added (from UI)
            self.get_to_page('Users')
            time.sleep(7)
            self.is_element_visible('users_user_user1',wait_time=30)
            self.log.info('User1 is shown on UI')

        except Exception as e:
            self.log.error('User1 is not created successfully: {}'.format(repr(e)))

    def tc_cleanup(self):
        self.log.info('Deleting all users ...')
        self.delete_all_users()

AUserCanBeAdded()