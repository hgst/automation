__author__ = 'fitbranded'
from paramiko import BadHostKeyException

from testCaseAPI.src.testclient import TestClient

class SSH_Host_Key_Reset(TestClient):
    def run(self):
        testname = 'SSH Host Key Reset'
        self.start_test(testname)
        self.log.debug(self.execute('ls /'))
        self.log.info('SSH connected succssfully. Performing factory restore.')
        self.perform_factory_restore(erase='systemOnly', wait_after_finish=0)
        self.set_ssh(enable=True, reset_ssh_client=False)
        try:
            self.log.debug(self.execute('ls /'))
        except BadHostKeyException:
            self.fail_test(testname, 'Got a BadHostKeyException. Host key was changed after a factory reset')
        else:
            self.pass_test(testname, 'Able to perform SSH command after factory reset. Host key did not change')

SSH_Host_Key_Reset()