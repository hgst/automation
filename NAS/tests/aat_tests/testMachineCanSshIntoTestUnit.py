"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify SSH works on test device

"""

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

class SshTest(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            self.set_ssh()
            result = self.execute('ifconfig')
            if result:
                self.log.info("SSH to device successfully, result : {}".format(result))
            else:
                self.log.error("Failed to SSH to device")
        except Exception, ex:
            self.log.exception('Failed to complete SSH test case \n' + str(ex))


SshTest()