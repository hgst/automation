""" Users page-Modify Group Quotas
    @Author: Lee_e
    
    Objective: Setup group and check quota of group should not smaller than user
    
    Automation: Full
    
    Supported Products:
        All
    
    Test Status: 
              Local Lightning: Pass (FW: 2.00.176)
                    KC       : Pass (FW: 2.00.155)
                    Zion     : Pass (FW: 2.00.205)
              Jenkins Taiwan : Lightning    - pass (FW: 2.00.205)
                             : Yosemite     - pass (FW: 2.00.205)
                             : Kings Canyon - pass (FW: 2.00.205)
                             : Sprite       - pass (FW: 2.00.205)
              Jenkins Irvine : Zion         - pass (FW: 2.00.205)
                             : Aurora       - pass (FW: 2.00.205)
                             : Glacier      - pass (FW: 2.00.200)
                             : Yellowstone  - pass (FW: 2.00.205)
     
""" 
from testCaseAPI.src.testclient import TestClient
import sys
import time
import os

groupname = 'a1'
group_amount = '5'

class ModifyGroupQuotas(TestClient):
    def run(self):
        self.delete_all_groups()
        self.creat_group_quota(group_name=groupname)
        result = self.update_group_quota_from_UI(groupname, group_amount, capacity='GB')

        if not result:
            self.log.info("PASS: Group amount updated successfully by UI")
        else:
            self.log.error("FAIL: Unable to update group amount by UI")          
     
    def tc_cleanup(self):
        self.log.info('delete group')
        self.execute("account -d -g '{0}'".format('a1'))
        
    def creat_group_quota(self, numberOfgroups=1, group_name=None, memberusers=None):
        self.log.info('*** Testing status: Create Group & assign Quota ***')
        self.create_groups(group_name, numberOfgroups, memberusers)
        self.execute("/usr/sbin/quota_set -g '{0}' -s '{1}' -a".format(group_name, 20480))
        self.log.info("command :/usr/sbin/quota_set -g '{0}' -s {1} -a".format(group_name, 20480))
       
    def update_group_quota_from_UI(self, group_name=None, group_quota=None, capacity=None):
        self.log.info('*** Testing status: Update Group Quota from UI ***')
        result = 0
        self.get_to_page('Users')
        self.wait_until_element_is_visible(self.Elements.GROUPS_BUTTON, timeout=5)
        self.click_wait_and_check(self.Elements.GROUPS_BUTTON, 'users_group_'+str(group_name))
        self.click_wait_and_check('users_group_'+str(group_name), 'css=#users_editGroupQuota_link > span._text')
        self.click_wait_and_check('css=#users_editGroupQuota_link > span._text', 'users_v1Size_text')
        try:
            self.input_text_check('users_v1Size_text', str(group_quota))
            self.input_text_check('users_v1Size_text', str(group_quota))
            self.click_wait_and_check('quota_unit_1', 'link='+str(capacity))
            self.click_wait_and_check('link='+str(capacity), 'users_editQuotaSave_button')
        except Exception as e:
            self.log.error('Exception error:{0}'.format(repr(e)))
            result = 1            
        self.click_wait_and_check('users_editQuotaSave_button', self.Elements.UPDATING_STRING, visible=False, timeout=60)
        
        return result

ModifyGroupQuotas()