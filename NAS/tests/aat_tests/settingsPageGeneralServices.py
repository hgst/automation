""" Settings Page-General-Services

    @Author: Lee_e

    Objective: verify general services if can be modified
    Automation: Full

    Supported Products:
        All

    Test Status:
              Local Lightning : PASS (FW: 2.10.228)
                    Lightning : PASS (FW: 2.10.209)
                    Lightning : PASS (FW: 2.10.278)
                    KC        : PASS (FW: 2.10.228)

              Jenkins Taiwan  : Lightning    - PASS (FW: 2.10.239)
                              : Yosemite     - PASS (FW: 2.10.239)
                              : Kings Canyon - PASS (FW: 2.10.239)
                              : Sprite       - PASS (FW: 2.10.239)
                              : Zion         - PASS (FW: 2.10.238)
                              : Aurora       - PASS (FW: 2.10.239)
                              : Yellowstone  - PASS (FW: 2.10.237)
                              : Glacier      -  non - support
                              : Grand Teton  - PASS (FW: 2.10.239)

"""
from testCaseAPI.src.testclient import TestClient
import time
import requests

DFS_Root_folder_name = 'Public'
DFS_Local_folder_name = 'Public-A'
DFS_Remote_share_name = 'Public-B'
DFS_exclude_list = ['BZVM', 'GLCR', 'BWVZ']
AD_exclude_list = ['GLCR', 'BZVM', 'BWVZ']

class SettingsPageGeneralServices(TestClient):
    def run(self):
        model_number = self.get_model_number()

        if model_number not in DFS_exclude_list:
            self.disable_dfs_cgi()
            self.delete_dfs_cgi(root_folder_name=DFS_Root_folder_name, local_folder_name=DFS_Local_folder_name)
            self.dfs_test()

        if model_number not in AD_exclude_list:
            self.disable_AD_cgi()
            self.log.info('Default AD UserName:{0}, Default AD Password:{1}, AD Domain Name:{2}, AD Server:{3}'
                          .format(self.uut[self.Fields.default_ad_username], self.uut[self.Fields.default_ad_password],
                                  self.uut[self.Fields.ad_domain_name], self.uut[self.Fields.ad_server]))
            self.ad_test()

        self.recycle_test()
        self.close_webUI()

    def dfs_test(self):
        self.get_to_page('Settings')
        self.click_wait_and_check(self.Elements.NETWORK_BUTTON, self.Elements.SETTINGS_NETWORK_DFS_SWITCH)
        self.check_share_aggregation_config(status='OFF')
        self.check_share_aggregation_config(status='ON')
        self.config_root_folder_name(root_folder_name=DFS_Root_folder_name)
        self.config_DFS_Link(local_folder_name=DFS_Local_folder_name, remote_share_name=DFS_Remote_share_name, host=self.uut[self.Fields.ftp_server])
        self.check_dfs_list(local_folder_name=DFS_Local_folder_name, host_ip=self.uut[self.Fields.ftp_server], remote_share=DFS_Remote_share_name)
        self.close_webUI()

    def ad_test(self):
        self.get_to_page('Settings')
        self.click_wait_and_check(self.Elements.NETWORK_BUTTON, self.Elements.SETTINGS_NETWORK_AD_SWITCH)
        self.check_active_directory_config(status='OFF')
        self.check_active_directory_config(status='ON')
        self.configure_active_directory(AD_USERNAME=self.uut[self.Fields.default_ad_username], AD_PASSWORD=self.uut[self.Fields.default_ad_password],
                                        AD_DOMAIN=self.uut[self.Fields.ad_domain_name], AD_DNS_SERVER=self.uut[self.Fields.ad_server])
        self.check_ads_info(AD_USERNAME=self.uut[self.Fields.default_ad_username], AD_DOMAIN_NAME=self.uut[self.Fields.ad_domain_name], AD_DNS_SERVER=self.uut[self.Fields.ad_server])

    def recycle_test(self):
        self.get_to_page('Settings')
        self.config_recycle_bin()

    def tc_cleanup(self):
        model_number = self.get_model_number()

        if model_number not in DFS_exclude_list:
            self.delete_dfs_cgi(root_folder_name=DFS_Root_folder_name, local_folder_name=DFS_Local_folder_name)
            self.disable_dfs_cgi()
        if model_number not in AD_exclude_list:
            self.disable_AD_cgi()
            cmd = 'setNetworkDhcp.sh ifname=bond0 ; setNetworkDhcp.sh ifname=egiga0'
            self.execute(cmd)
            time.sleep(10)

    # Network --> Windows Services
    def check_share_aggregation_config(self, status=None):
        status_dict = {'ON': '1', 'OFF': '0'}
        test_name = 'Check DFS status: '+status
        if status == 'ON':
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_DFS_SWITCH, self.Elements.SETTINGS_NETWORK_DFS_CONFIGURE)
        try:
            self.start_test(test_name)
            self.check_attribute('settings_networkDFS_switch', expected_value=status_dict[status], attribute_type='value')
            self.pass_test(test_name)
        except Exception:
            self.fail_test(test_name)
            self.take_screen_shot(test_name)
            raise Exception

    # Need to configure when share aggregation switch to ON
    def config_root_folder_name(self, root_folder_name=None):
        sub_test_name = 'Config root folder name'
        self.start_test(sub_test_name)

        if not self.is_element_visible(self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_APPLY_BUTTON, wait_time=5):
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_DFS_CONFIGURE, self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_APPLY_BUTTON)

        self.log.info('Root Folder Name: ' + str(self.get_value(self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_NAME)))

        if self.get_value(self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_NAME) != root_folder_name:
            self.input_text_check(self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_NAME, root_folder_name)
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_APPLY_BUTTON, self.Elements.SETTINGS_NETWORK_DFS_ADD_LINK)

        try:
            self.check_attribute(self.Elements.SETTINGS_NETWORK_DFS_ROOT_FOLDER_NAME, expected_value=root_folder_name, attribute_type='value')
            self.pass_test(sub_test_name)
        except Exception as e:
            self.fail_test(sub_test_name+' with Exception: {0}'.format(repr(e)))

    def config_DFS_Link(self, local_folder_name=None, remote_share_name=None, host='192.168.1.116'):
        sub_test_name = 'Config DFS Link'
        self.start_test(sub_test_name)
        try:
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_DFS_ADD_LINK, self.Elements.SETTINGS_NETWORK_DFS_ADD_LINK_APPLY, timeout=60)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_DFS_LOCAL_FOLDER_NAME, local_folder_name)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_DFS_REMOTE_HOST, host)
            self.input_text_check(self.Elements.SETTINGS_NETWORK_DFS_REMOTE_SHARE, remote_share_name)
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_DFS_ADD_LINK_APPLY, self.Elements.SETTINGS_NETWORK_DFS_ADD_LINK_CANCEL, timeout=60)
            self.pass_test(sub_test_name)
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_DFS_ADD_LINK_CANCEL, self.Elements.SETTINGS_NETWORK_DFS_CONFIGURE)

        except Exception as e:
            self.fail_test(sub_test_name+' with Exception: {0}'.format(repr(e)))
            self.take_screen_shot('Config_DFS_Link')
            raise Exception

    def check_active_directory_config(self, status=None):
        status_dict = {'ON': '1', 'OFF': '0'}
        test_name = 'Check AD status: '+status
        if status == 'ON':
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_SWITCH, self.Elements.SETTINGS_NETWORK_AD_APPLY)
        try:
            self.start_test(test_name)
            self.check_attribute('settings_networkADS_switch', expected_value=status_dict[status], attribute_type='value')
            self.pass_test(test_name)
        except Exception:
            self.fail_test(test_name)
            self.take_screen_shot(test_name)
            raise Exception

    def configure_active_directory(self, AD_USERNAME=None, AD_PASSWORD=None, AD_DOMAIN=None, AD_DNS_SERVER=None):
        self.start_test('Config AD')
        if not self.is_element_visible(self.Elements.SETTINGS_NETWORK_AD_APPLY):
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_CONFIGURE, self.Elements.SETTINGS_NETWORK_AD_APPLY)
        self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_USERNAME, AD_USERNAME)
        self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_PASSWORD, AD_PASSWORD)
        self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_DOMAIN_NAME, AD_DOMAIN)
        self.input_text_check(self.Elements.SETTINGS_NETWORK_AD_DNS_SERVER, AD_DNS_SERVER)
        self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_APPLY, self.Elements.UPDATING_STRING, timeout=60)
        self.log.info('Message shows: '+str(self.get_text(self.Elements.SETTINGS_NETWORK_AD_RESULT_DIAG)))
        if self.get_text(self.Elements.SETTINGS_NETWORK_AD_RESULT_DIAG) == 'Successful':
            self.pass_test('Config AD')
            self.click_wait_and_check(self.Elements.SETTINGS_NETWORK_AD_OK, self.Elements.SETTINGS_NETWORK_AD_CONFIGURE)
        else:
            self.fail_test('Config AD with wrong message:{0}'.format(self.get_text(self.Elements.SETTINGS_NETWORK_AD_RESULT_DIAG)))

    # General --> Services
    def config_recycle_bin(self):
        self.start_test('Recycle-Bin')
        try:
            self.click_wait_and_check(self.Elements.GENERAL_BUTTON, self.Elements.SETTINGS_GENERAL_RECYCLEBIN_LINK)
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_RECYCLEBIN_OK, self.Elements.SETTINGS_GENERAL_RECYCLEBIN_APPLY)
            self.click_wait_and_check(self.Elements.SETTINGS_GENERAL_RECYCLEBIN_APPLY, self.Elements.UPDATING_STRING, visible=False, timeout=60)
            self.pass_test('Recycle-Bin')
        except Exception as e:
            self.fail_test('Recycle-Bin')
            raise Exception("Recycle-Bin with Exception {}".format(repr(e)))

    def disable_dfs_cgi(self):
        self.log.info('Disable DFS CGI')
        head = 'account_mgr.cgi'
        cmd = 'cgi_set_dfs_enable&dfs=0'
        self.send_cgi(cmd=cmd, url2_cgi_head=head)

    def delete_dfs_cgi(self, root_folder_name=None, local_folder_name=None):
        self.log.info('Delete DFS CGI')
        head = 'account_mgr.cgi'
        #'cgi_del_dfs&group=Public&local=192.168.11.39'
        #'cgi_del_dfs&group=Public&local=Public-A'
        cmd = 'cgi_del_dfs&group={0}&local={1}'.format(root_folder_name, local_folder_name)
        self.send_cgi(cmd=cmd, url2_cgi_head=head)

    def set_dfs_cgi(self):
        ## sharename = localname
        ## path = remote share
        ## group = root folder name
        cmd = 'cmd=cgi_set_dfs&host=192.168.1.116&sharename=A&path=B&group=Public'

    def check_ads_info(self, AD_USERNAME=None, AD_DOMAIN_NAME=None, AD_DNS_SERVER=None):
        self.log.info('get AD info')
        cmd = 'cgi_get_ads_info'
        head = 'account_mgr.cgi'
        result = self.send_cgi(cmd, head)
        AD_USERNAME_info = self.get_xml_tags(result, 'u_name')
        AD_DOMAIN_NAME_info = self.get_xml_tags(result, 'realm')
        AD_DNS_SERVER_info = self.get_xml_tags(result, 'dns1')
        print AD_USERNAME_info, AD_DOMAIN_NAME_info, AD_DNS_SERVER_info
        self.start_test('Check AD_USERNAME')
        if AD_USERNAME in AD_USERNAME_info:
            self.pass_test('Check AD_USERNAME:{0}'.format(AD_USERNAME_info))
        else:
            self.fail_test('Check AD_USERNAME: {0}'.format(AD_USERNAME_info))

        self.start_test('Check AD_DOMAIN_NAME')
        if AD_DOMAIN_NAME in AD_DOMAIN_NAME_info:
            self.pass_test('Check AD_DOMAIN_NAME: {0}'.format(AD_DOMAIN_NAME_info))
        else:
            self.fail_test('Check AD_DOMAIN_NAME: {0}'.format(AD_DOMAIN_NAME_info))

        self.start_test('Check AD_DNS_SERVER')
        if AD_DNS_SERVER in AD_DNS_SERVER_info:
            self.pass_test('Check AD_DNS_SERVER: {0}'.format(AD_DNS_SERVER_info))
        else:
            self.fail_test('Check AD_DNS_SERVER: {0}'.format(AD_DNS_SERVER_info))

    def disable_AD_cgi(self):
        self.log.info('Disable AD CGI')
        head ='account_mgr.cgi'
        cmd = 'cgi_set_ads&f_enable=0&f_name=&f_pw=&f_realm_name=&f_dns1='
        self.send_cgi(cmd, head)

    def check_dfs_list(self, local_folder_name=None, host_ip=None, remote_share=None):
        local_name, host, remote_share_name = self.get_dfs_list_info()
        self.start_test('Check DFS Local folder Name')
        if local_name == local_folder_name:
            self.pass_test('Check DFS Local folder Name:{0}'.format(local_name))
        else:
            self.fail_test('Check DFS Local folder Name: {0}'.format(local_name))

        self.start_test('Check DFS Host IP')
        if host == host_ip:
            self.pass_test('Check DFS Host IP:{0}'.format(host))
        else:
            self.fail_test('Check DFS Host IP: {0}'.format(host))

        self.start_test('Check DFS Remote Share Name')
        if remote_share_name == remote_share:
            self.pass_test('Check DFS Remote Share Name:{0}'.format(remote_share_name))
        else:
            self.fail_test('Check DFS Remote Share Name: {0}'.format(remote_share_name))

        '''
        for i in range(0, len(dfs_list)/3):
           local_name, host, remote_share_name = dfs_list[i:i+2]
        '''

    def get_dfs_list_info(self, root_folder_name='Public'):
        self.log.info('Get DFS info')
        head = 'account_mgr.cgi'
        cmd = 'cgi_get_dfs_list&page=1&rp=100&f_field='+root_folder_name
        result = self.send_cgi(cmd=cmd, url2_cgi_head=head)
        local_name, host, remote_share_name = self.get_xml_tags(result, 'cell')

        return local_name, host, remote_share_name

    def send_cgi(self, cmd, url2_cgi_head):
        login_url = "http://{0}/cgi-bin/login_mgr.cgi?".format(self.uut[self.Fields.internal_ip_address])
        user_data = {'cmd': 'wd_login', 'username': 'admin', 'pwd': ''}
        head = 'http://{0}/cgi-bin/{1}?cmd='.format(self.uut[self.Fields.internal_ip_address], url2_cgi_head)
        cmd_url = head + cmd
        self.log.debug('login cmd: '+login_url +'cmd=wd_login&username=admin&pwd=')
        self.log.debug('setting cmd: '+cmd_url)
        s = requests.session()
        r = s.post(login_url, data=user_data)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))

        r = s.get(cmd_url)
        time.sleep(20)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))

        self.log.debug("Successful CGI CMD: {}".format(cmd_url))

        return r.text.encode('ascii', 'ignore')

SettingsPageGeneralServices()