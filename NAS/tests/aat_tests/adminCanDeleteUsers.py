"""
Created on July 27th, 2015

@author: tran_jas

## @brief FW-0620 User management.Admin can delete users
#   Create new user
#   Get list of current users
#   Delete newly created user
#   Get list of current users
#   Verify newly created user is not listed

"""


# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

username = 'elijah'

class AdminCanDeleteUser(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            self.create_user(username)
            users_before = self.get_all_users()
            list_users_before = self.get_xml_tags(users_before, 'username')
            self.log.info('list_users_before: {}'.format(list_users_before))

            self.log.info("Deleting user {}".format(username))
            self.delete_user(username)

            users_after = self.get_all_users()
            list_users_after = self.get_xml_tags(users_after, 'username')
            self.log.info('list_users_after: {}'.format(list_users_after))

            if username not in users_after:
                self.log.info("Delete user {} successfully".format(username))
            else:
                self.log.error("Failed to delete {}".format(username))

        except Exception, ex:
            self.log.exception('Failed to complete delete user test case \n' + str(ex))

        finally:
            self.log.info("Delete user {} in case test failed after create user".format(username))
            self.delete_user(username)

AdminCanDeleteUser()