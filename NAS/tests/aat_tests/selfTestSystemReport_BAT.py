# title           :selfTestSystemReport_BAT.py
# description     :To verify that the quick test works successfully and display the correct result (For BAT Test)
# author          :yang_ni
# date            :2015/01/23
# notes           :http://wiki.wdc.com/wiki/Self-test_and_System_Report
#
#                 Step 1: Create a Raid 1 or Raid 5 (If 2 Bay, create Raid1, If 4 Bay, create Raid5)
#                 Step 2: Perform the quicktest, and verify if the result shows correctly.
#==============================================================================

from testCaseAPI.src.testclient import TestClient
import time,os

class selfTestSystemReport(TestClient):
    def run(self):
        try:
            self.log.info('######################## Quick Test START ##############################')         
            
            model_number = self.get_model_number()
            raid_mode = self.get_raid_mode()
            
            bay2_model =  ['BWAZ', 'BBAZ', 'KC2A', 'BZVM']
            bay4_model =  ['LT4A', 'BNEZ', 'BWZE']
            
            #1. Create a Raid 1 or Raid 5 depending on NAS device (If 2 Bay : create Raid1, If 4 Bay: create Raid5):
            if (model_number in bay2_model) and (raid_mode != "RAID1"):
                self.log.info('Raid mode is not in Raid1, configuring to Raid1 now')
                self.configure_raid(raidtype='RAID1')
                
            elif (model_number in bay4_model) and (raid_mode != "RAID5"):
                self.log.info('Raid mode is not in Raid5, configuring to Raid5 now')
                self.configure_raid(raidtype='RAID5',volume_size=200)

            #2. Settings -> Utilities -> QuickTest :
            self.quickTest()
            
            self.close_webUI()
            self.log.info('######################## Quick Test END ##############################')         

        except Exception as e:
            self.log.error("Quick Test FAILED: exception: {}".format(repr(e)))

    # # @Perform the Quick test on the 'setting' page
    def quickTest(self):
         
         self.access_webUI()
         time.sleep(7)
         self.get_to_page('Settings')
         time.sleep(7)

         self.click_wait_and_check('settings_utilities_link','settings_utilitiesConfig_button',visible=True)
         self.click_wait_and_check('settings_utilitiesQuickTest_button','settings_utilitiesDiskTestCancel_button',visible=True)

         time.sleep(165)
               
         self.wait_until_element_is_clickable('settings_utilitiesDiskTestClose_button',timeout=20)
         result = str(self.get_text('DIV_SMART_RES')).splitlines()
                  
         self.click_button('settings_utilitiesDiskTestClose_button')
         
         #Check if return message of each disk contains word 'Pass'
         for i in range(0,len(result)):
            if("Pass" in result[i]):
                self.log.info(result[i])
            else:
                self.log.info(result[i])

# This constructs the selfTestSystemReport() class, which in turn constructs TestClient() which triggers the selfTestSystemReport.run() function
selfTestSystemReport()