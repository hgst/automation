"""
Created on July 27th, 2015

@author: tran_jas

## @brief Verify device serial port is connected and active
"""

import time

# Import the test case API and base class
from testCaseAPI.src.testclient import TestClient

class SerialConnect(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            result = self.serial_read_all()
            self.log.info("first read result : {}".format(result))
            self.serial_write('\n\n\n')
            time.sleep(5)
            result = self.serial_read_all()
            if result:
                self.log.info("Connect via serial to device successfully, result : {}".format(result))
            else:
                self.log.error("Failed to connect via serial to device")
        except Exception, ex:
            self.log.exception('Failed to complete serial connection test case \n' + str(ex))


SerialConnect()