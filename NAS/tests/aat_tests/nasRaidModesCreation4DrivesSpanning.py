from testCaseAPI.src.testclient import TestClient
import time
class MyClass(TestClient):
    # Create tests as function in this class
    def run(self):
        try:
            self.access_webUI()
            time.sleep(5)
            self.configure_spanning()
            generated_file = self.generate_test_file(kilobytes=10240) 
            self.write_files_to_smb(files=generated_file)
            self.read_files_from_smb(files='file0.jpg')
            self.close_webUI()
        except Exception, ex: 
            print ('Converting RAID mode was not successful. Check RAID settings')      
            self.log.error('Failed to Configure RAID\n' + str(ex)) 

        # Call all tests from this function

# This constructs the Tests() class, which in turn constructs TestClient() which triggers the Tests.run() function
MyClass()
