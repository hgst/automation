"""
## @author: lo_va
# Objective: To exercise 3 drives configuration for JBOD creation scenarios for multi-bay Consumer NAS.
# wiki URL: http://wiki.wdc.com/wiki/NAS_RAID_Modes_Creation
"""
from testCaseAPI.src.testclient import TestClient
from global_libraries import wd_exceptions
import time
import random

class jbodDrives3(TestClient):

    def run(self):
        drives_number = self.get_drive_count()
        if drives_number < 3:    # Check Drives counts
            raise wd_exceptions.InvalidDeviceState('Device was less than 3 drives, cannot test Creation Test!!!')
        file_prefix = 'rwfile{}'.format(random.randint(1, 100))
        generate_size = 102400

        # Cleanup Drives first
        self.log.info('Clean up drive {}'.format(4))
        self.clean_up_drive(4)

        # Remove Drives
        self.log.info('Remove drive {}'.format(4))
        self.remove_drives(4)

        generated_file = ''
        try:
            self.configure_raid(raidtype='JBOD', drive_bays=3, force_rebuild=True)
            generated_file = self.generate_test_file(generate_size, prefix=file_prefix)
        except Exception, ex:
            self.log.exception('Failed to Configured JBOD with 3 Drive, Exception: {}'.format(repr(ex)))

        # Insert Drives Back
        self.log.info('Insert drive {}'.format(4))
        self.insert_drives(4)

        # Wait for system protocol service ready
        time.sleep(10)

        # Read/Write Access test for JBOD Volume 1
        sharename = 'Public'
        try:
            self.write_files_to_smb(files=generated_file, share_name=sharename, delete_after_copy=False)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(files=file_prefix+'0.jpg', share_name=sharename, delete_after_copy=True)
            self.log.info('Read/Write Access for JBOD1 with 3 Drive, test PASS!!')
        except Exception, ex:
            self.log.exception('Read/Write Access for JBOD1 with 3 Drive, test Failed!!, Exception: {}'.format(repr(ex)))

        # Read/Write Access test for JBOD Volume 2
        sharename2 = 'JBOD2Folder'
        self.create_shares(number_of_shares=1, share_name=sharename2, volume_name='Volume_2')
        try:
            self.write_files_to_smb(files=generated_file, share_name=sharename2, delete_after_copy=False)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename2+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(files=file_prefix+'0.jpg', share_name=sharename2, delete_after_copy=True)
            self.log.info('Read/Write Access for JBOD2 with 3 Drive, test PASS!!')
        except Exception, ex:
            self.log.exception('Read/Write Access for JBOD2 with 3 Drive, test Failed!!, Exception: {}'.format(repr(ex)))

        # Read/Write Access test for JBOD Volume 3
        sharename3 = 'JBOD3Folder'
        self.create_shares(number_of_shares=1, share_name=sharename3, volume_name='Volume_3')
        try:
            self.write_files_to_smb(files=generated_file, share_name=sharename3, delete_after_copy=False)
            self.hash_check(localFilePath=generated_file, uutFilePath='/shares/'+sharename3+'/', fileName=file_prefix+'0.jpg')
            self.read_files_from_smb(files=file_prefix+'0.jpg', share_name=sharename3, delete_after_copy=True)
            self.log.info('Read/Write Access for JBOD3 with 3 Drive, test PASS!!')
        except Exception, ex:
            self.log.exception('Read/Write Access for JBOD3 with 3 Drive, test Failed!!, Exception: {}'.format(repr(ex)))

    def hash_check(self, localFilePath, uutFilePath, fileName):
        hashLocal = self.checksum_files_on_workspace(dir_path=localFilePath, method='md5')
        hashUUT = self.md5_checksum(uutFilePath,fileName)
        if hashLocal == hashUUT:
            self.log.info('***** Hash check SUCCEEDED!! *****')
        else:
            self.log.error('***** Hash check FAILED!! *****')

    def clean_up_drive(self, drive_number):
        drive_mapping = self.get_drive_mappings()
        clean_up_cmd = 'echo -e "o\ny\nw\ny\n" | gdisk /dev/{}'.format(drive_mapping.get(drive_number))
        self.run_on_device(clean_up_cmd)

# This constructs the Tests() class, which in turn constructs TestClient() which triggers the Tests.run() function
jbodDrives3()
