'''
Created on September 5th, 2018
@Author: Philip Yang

Full Title: GRACK System: System Information

page: Overview, Program, Performance, Report, Log and Support.

'''
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


_functionbtn ={
    'overview' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'program' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
    'performance' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(3) span',
    'report' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(4) span',
    'logs' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(5) span',
    'support' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(6) span'
}

_performance ={
    'cpu' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="perfstats"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id$="-innerCt"] div a:nth-child(1) span',
    'disk' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="perfstats"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id$="-innerCt"] div a:nth-child(2) span',
    'loadaverage' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="perfstats"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id$="-innerCt"] div a:nth-child(3) span',
    'memory' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="perfstats"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id$="-innerCt"] div a:nth-child(4) span',
    'network' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="perfstats"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id$="-innerCt"] div a:nth-child(5) span'
}

_logs_type ={
    'Auth' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ",
    'boot' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ",
    'btrfs' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[3] ",
    'daemon' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[4] ",
    'ftp' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[5] ",
    'message' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[6] ",
    'rsync' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[7] ",
    'smart' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[8] ",
    'smb_cifs' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[9] ",
    'syslog' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[10] ",
}

_support_button ={
    'downloadlogfiles' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="-support"] div[id$="-support-body"] div div fieldset:nth-child(2) div div div div:nth-child(1) div div div div a span',
    'sendbyemail' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id$="-support"] div[id$="-support-body"] div div fieldset:nth-child(2) div div div div:nth-child(2) div div div div a span'
}

class sys_sysinfo():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'System SysInfo'
        self.messageCheck = False

    def _loginTab(self, tab):
        # Click tab of System Information
        try:
            self.utility.infoMsg('Click Tab of System Information')
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"][class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id^="tabbar"] div a:nth-child(5) span').click()
            time.sleep(2)
            self.utility.infoMsg('Click Tab of %s' % tab)
            self.Browser.find_element_by_css_selector(_functionbtn[tab]).click()
            time.sleep(2)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='System Monitoring tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='System Monitoring tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('System Monitoring tab button cannot be clicked.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def overview(self):
        self.itemname='SysInfo: Overview'
        self.utility.unittest(test_item_name=self.itemname)

        try:
            self._loginTab(tab='overview')
            time.sleep(5)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Overview: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Overview: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Overview: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Overview: Exception just happened.')
            pass

    def processes(self):
        self._loginTab(tab='program')
        self.itemname = 'SysInfo: Processes'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Refresh...')
            self.Browser.find_element_by_css_selector('span[id$="-top-refresh-btnWrap"]').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Processes: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Processes: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Processes: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Processes: Exception just happened.')
            pass

    def _cpu(self):
        self.itemname = 'SysInfo: Performance CPU usage'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Click tab of CPU')
            self.Browser.find_element_by_css_selector(_performance['cpu']).click()
            time.sleep(8)
            self.utility.infoMsg('Refresh...')
            self.Browser.find_element_by_css_selector('span[id$="perfstats-cpu-refresh-btnWrap"]').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Performance CPU usage: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Performance CPU usage: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Performance CPU usage: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Performance CPU usage: Exception just happened.')
            pass

    def _disk(self):
        self.itemname = 'SysInfo: Performance Disk usage'
        self.utility.unittest(test_item_name=self.itemname)
        _disk_exist = True
        _disk_checked = True
        _diskmember = 1
        while _disk_exist:
            try:
                self.Browser.find_element_by_css_selector(_performance['disk']).click()
                time.sleep(2)
                self.utility.infoMsg('Pick disk member %d' %_diskmember)
                self.Browser.find_element_by_css_selector(
                    'div[id$="-perfstats-body"] div[id$="perfstats-diskusage"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(%d) span' % _diskmember).click()
                time.sleep(2)
                self.utility.infoMsg('Refresh...')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-perfstats-diskusage-body"] div:nth-child(%d) div[class*="x-toolbar"] div div a span' % _diskmember).click()
                time.sleep(15)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname,
                                       error='Performance Disk usage: Unselectable elemnt ' + e0.msg)
                _disk_checked = False
                pass
            except NoSuchElementException:
                _disk_exist = False
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname,
                                       error='Performance Disk usage: Element is invisibled ' + e2.msg)
                _disk_checked = False
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Performance Disk usage: Exception just happened.')
                _disk_checked = False
                pass
            _diskmember += 1
        if _disk_checked:
            self.utility.pass_test(testname=self.itemname)

    def _loadaverage(self):
        self.itemname = 'SysInfo: Performance loading'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Click tab of load average')
            self.Browser.find_element_by_css_selector(_performance['loadaverage']).click()
            time.sleep(8)
            self.utility.infoMsg('Refresh...')
            self.Browser.find_element_by_css_selector('span[id$="perfstats-load-refresh-btnWrap"]').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Performance loading: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Performance loading: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Performance loading: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Performance loading: Exception just happened.')
            pass

    def _memory(self):
        self.itemname = 'SysInfo: Performance Memory usage'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Click tab of memory')
            self.Browser.find_element_by_css_selector(_performance['memory']).click()
            time.sleep(8)
            self.utility.infoMsg('Refresh...')
            self.Browser.find_element_by_css_selector('span[id$="perfstats-memory-refresh-btnWrap"]').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Performance Memory usage: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Performance Memory usage: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Performance Memory usage: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Performance Memory usage: Exception just happened.')
            pass

    def _network(self):
        self.itemname = 'SysInfo: Performance Network Interface'
        self.utility.unittest(test_item_name=self.itemname)
        _eth_exist = True
        _eth_checked = True
        _ethmember = 1
        while _eth_exist:
            try:
                self.Browser.find_element_by_css_selector(_performance['network']).click()
                time.sleep(8)
                self.utility.infoMsg('Pick eth member %d' %_ethmember)
                self.Browser.find_element_by_css_selector('div[id$="-perfstats-body"] div[id$="perfstats-networkinterfaces"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(%d) span' % _ethmember ).click()
                time.sleep(2)
                self.utility.infoMsg('Refresh...')
                self.Browser.find_element_by_css_selector('div[id$="-perfstats-networkinterfaces-body"] div:nth-child(%d) div[class*="x-toolbar"] div div a span'% _ethmember).click()
                time.sleep(15)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Performance Network: Unselectable elemnt ' + e0.msg)
                _eth_checked = False
                pass
            except NoSuchElementException:
                _eth_exist = False
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname,
                                       error='Performance Network: Element is invisibled ' + e2.msg)
                _eth_checked = False
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Performance Network: Exception just happened.')
                _eth_checked = False
                pass
            _ethmember+=1
        if _eth_checked:
            self.utility.pass_test(testname=self.itemname)

    def performance(self):
        self._loginTab(tab='performance')
        self._cpu()
        self._disk()
        self._loadaverage()
        self._memory()
        self._network()

    def report(self):
        # self._loginTab(tab='report')
        self.itemname = 'SysInfo: Report'
        self.utility.unittest(test_item_name=self.itemname)
        self._web_login(tab='report')
        try:
            self.utility.infoMsg('Refresh...')
            self.newBrowser.find_element_by_css_selector('span[id$="-report-refresh-btnWrap"]').click()
            time.sleep(10)

            self.utility.infoMsg('Click download button')
            self.newBrowser.find_element_by_css_selector('span[id$="-report-download-btnWrap"]').click()
            time.sleep(10)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Report: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Report: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Report: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Report: Exception just happened.')
            pass
        finally:
            self._web_logout()

    def _web_login(self,tab):
        """ Add set_profile for download log testing"""
        ffpf = webdriver.FirefoxProfile()
        ffpf.set_preference("browser.download.folderList", 2)
        ffpf.set_preference("browser.download.dir", "~/Downloads")
        ffpf.set_preference("browser.download.manager.showWhenStarting", False)
        ffpf.set_preference("browser.download.manager.useWindow", False)
        ffpf.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/zip, application/octet-stream, text/plain, application/x-gzip, application/a-gzip, application/x-gtar, application/x-compressed, application/x-tar+x-gzip")
        self.newBrowser = webdriver.Firefox(firefox_profile=ffpf)
        self.newBrowser.maximize_window()
        self.newBrowser.get(self.utility.deviceIP)
        time.sleep(2)

        try:
            username = self.newBrowser.find_element_by_id("username-inputEl")
            username.send_keys(self.utility.grack_username)
            password = self.newBrowser.find_element_by_id("password-inputEl")
            password.send_keys(self.utility.grack_password)

            self.newBrowser.find_element_by_id("login-panel-login").click()
            time.sleep(0.5)
            try:
                self.newBrowser.find_element_by_css_selector('div[id^="messagebox-"]').is_displayed()
                self.utility.infoMsg('You have logged in another browser or you didn\'t logout correctly before.')
                self.newBrowser.find_element_by_css_selector(
                    'div[id$="-toolbar-targetEl"] > a[id^="button"]:nth-child(2) > span[id$="-btnWrap"]').click()
            except NoSuchElementException:
                pass
            except ElementNotVisibleException:
                pass
        except NoSuchElementException as e:
            self.utility.errorMsg('Fail to login. Cannot find the element.')
        else:
            time.sleep(8)
            self.utility.infoMsg('Login finished!')

        try:
            self.newBrowser.find_element_by_css_selector('a[class*="x-tab"]:nth-child(6)').click()
            self.utility.infoMsg('Goto Tab: System')

            self.utility.infoMsg('Click Tab of System Information')
            self.newBrowser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"][class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id^="tabbar"] div a:nth-child(5) span').click()
            time.sleep(2)
            self.utility.infoMsg('Click Tab of %s' % tab)
            self.newBrowser.find_element_by_css_selector(_functionbtn[tab]).click()
            time.sleep(2)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='System Monitoring tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='System Monitoring tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('System Monitoring tab button cannot be clicked.')
            pass

    def _web_logout(self):
        try:
            # Click tab and logout of device
            self.newBrowser.find_element_by_id('splitbutton-1022-btnIconEl').click()
            time.sleep(2)

            try:
                self.newBrowser.find_element_by_id('menuitem-1027-textEl').click()  # Apply for FW under v1.1.0
            except NoSuchElementException:
                # Handle UI changing for FW v1.1.0
                self.newBrowser.find_element_by_css_selector(
                    'div[id^="workspace-menu-"] div[class*="x-menu-body"] div[class*="x-box-inner"] div[class*="x-box-target"] div:nth-child(5) a span').click()
                pass
            time.sleep(2)
            self.newBrowser.find_element_by_id('button-1006-btnInnerEl').click()
        except NoSuchElementException as e:
            self.utility.errorMsg('Fail to logout: ' + e.msg)
        except Exception as e2:
            self.utility.errorMsg(e2.message)
        finally:
            self.utility.infoMsg("Logout finished!")
            self.newBrowser.close()

    def logs(self,pickuptype='Auth'):
        # self._loginTab(tab='log')
        self.itemname = 'SysInfo: Logs'
        self.utility.unittest(test_item_name=self.itemname)
        self._web_login(tab='logs')
        try:

            self.utility.infoMsg('Pick up type: %s' %pickuptype)
            self.newBrowser.find_element_by_css_selector('div[id$="-logs-type-trigger-picker"]').click()
            time.sleep(3)
            self.newBrowser.find_element_by_xpath(_logs_type[pickuptype]).click()
            time.sleep(5)

            self.utility.infoMsg('Download...')
            self.newBrowser.find_element_by_css_selector('span[id$="-logs-download-btnWrap"]').click()
            time.sleep(5)

            self.utility.infoMsg('Clear...')
            self.newBrowser.find_element_by_css_selector('span[id$="-logs-clear-btnWrap"]').click()
            time.sleep(5)
            self.newBrowser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(5)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Log: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Log: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Log: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Log: Exception just happened.')
            pass
        finally:
            self._web_logout()

    def _catchMessageBox(self):
        try:
            self.newBrowser.find_element_by_css_selector('div[class*="x-message-box"] div[id$="toolbar"] div div a:nth-child(1) span').click()
            # self.newBrowser.find_element_by_xpath("//div[contains(@class, 'x-message-box')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span").click()
            time.sleep(5)
            self.messageCheck = True
            self.utility.infoMsg('Catching success message.')
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Support: Unselectable elemnt in MessageBox ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.infoMsg('Try to pick up error messagebox.')
            pass
        except ElementNotVisibleException as e2:
            self.utility.infoMsg('Try to pick up error messagebox.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname,
                                   error='Support: Exception just happened in MessageBox check.')
            pass

    def _catchFailMessageBox(self):
        try:
            # self.newBrowser.find_element_by_css_selector('div[class*="x-message-box"] div[id$="toolbar"] div div a:nth-child(1) span').click()
            self.newBrowser.find_element_by_xpath("//div[contains(@class, 'x-message-box')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span").click()
            time.sleep(5)
            self.messageCheck = True
            self.utility.infoMsg('Catching fail message.')
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Support: Unselectable elemnt in MessageBox ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Support: No such element '+ e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Support: Element is invisibled in MessageBox' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Support: Exception just happened in MessageBox check.')
            pass

    def support(self):
        # self._loginTab(tab='support')
        self.itemname = 'SysInfo: Support'
        self.utility.unittest(test_item_name=self.itemname)
        self._web_login(tab='support')

        if 1:
            try:
                '' 'Download log files '''
                self.utility.infoMsg('Click download log files')
                self.newBrowser.find_element_by_css_selector(_support_button['downloadlogfiles']).click()
                time.sleep(15)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Support: DL log files has an unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='Support: No such element in DL log files ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname,
                                       error='Support: Element is invisibled in DL log files' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Support: Exception just happened in DL log files.')
                pass


        try:
            ''' Send log by email '''
            self.utility.infoMsg('Send log file by email...')
            self.newBrowser.find_element_by_css_selector(_support_button['sendbyemail']).click()
            time.sleep(10)

            self.utility.infoMsg('Check message box popup or not')
            self._catchMessageBox()
            if self.messageCheck is False:
                self._catchFailMessageBox()
            if self.messageCheck:
                self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Support: sending log has an unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Support: No such element in sending log ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Support: Element is invisibled in sending log ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Support: Exception just happened in sending log.')
            pass
        finally:
            self._web_logout()

if __name__ == "__main__":
    testing = sys_sysinfo()
    testing.overview()
    testing.processes()
    testing.performance()
    testing.report()
    testing.logs()
    testing.support()
    testing._logout()

