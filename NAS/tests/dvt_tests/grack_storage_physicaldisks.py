'''
Created on June 22, 2018
@Author: Philip Yang

Full Title: GRACK physical disk testing
Features including edit, wipe, scan and locate.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


spindown_selector = {
    'Disabled': 'div[id$="listWrap"] ul li:nth-child(1)',
    '5': 'div[id$="-listWrap"] ul li:nth-child(2)',
    '10': 'div[class*="x-boundlist"] div ul li:nth-child(3)',
    '20': 'div[class*="x-boundlist"] div ul li:nth-child(4)',
    '30': 'div[class*="x-boundlist"] div ul li:nth-child(5)',
    '60': 'div[class*="x-boundlist"] div ul li:nth-child(6)',
    '120': 'div[class*="x-boundlist"] div ul li:nth-child(7)',
    '240': 'div[class*="x-boundlist"] div ul li:nth-child(8)',
    '300': 'div[class*="x-boundlist"] div ul li:nth-child(9)',
    '360': 'div[class*="x-boundlist"] div ul li:nth-child(10)'
}

wipe_mode = {
    'secure':'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span',
    'quick' : 'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(3) span',
    'cancel' : 'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(4) span'
}
wiping_button = {
    'stop' : 'div[class*="x-window-modal"] div[class*="x-toolbar"] div div a:nth-child(2) span',
    'close' : 'div[class*="x-window-modal"] div[class*="x-toolbar"] div div a:nth-child(3) span'

}

class grack_storage_physicaldisks():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.itemname = 'storage testing'
        self.wiping = False
        self.scanning = False
        self.memo = ''
        self.counter = 0

    def edit(self, disk='/dev/sdc', spindown='5'):
        ''' edit storage
        :param disk: choose a disk.
        :param spindown: set a time limit or disabled the disk.
        '''
        self.itemname = 'Edit physical disk'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Disk spindown testing ***')
        try:
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % disk).click()
            time.sleep(1)

            # Click edit button
            self.Browser.find_element_by_css_selector('span[id$="-edit-btnWrap"]').click()
            time.sleep(0.5)
            self.utility.infoMsg('Edit...')
            time.sleep(3)
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not found: please make sure your RAID has been created.' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the popup window.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: disk or edit button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have provide other disk for edit:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Exception has happened. ' + e.message)


        # Pcik spindown button
        try:
            self.Browser.find_element_by_css_selector('div[class^="x-autocontainer-innerCt"][id^="ext-comp"] div:nth-child(3) div div div').click()
            self.utility.infoMsg(msg='Picking...')
            time.sleep(2)

            self.Browser.find_element_by_css_selector('%s' %spindown_selector[spindown]).click()
            self.utility.infoMsg('Pick: %s' %spindown)
            time.sleep(5)

            self.Browser.find_element_by_css_selector('div[class*="x-toolbar"] div div a:nth-child(1) span[id$="ok-btnWrap"]').click()
            self.utility.infoMsg('Saving')
            time.sleep(2)
            self.utility.pass_test(testname=self.itemname)
            time.sleep(2)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not found. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the popup window.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: time limit or save button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have choosen an exist time limit:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Exception has happened. ' + e.message)

    def wipe(self, disk = '/dev/sde', mode = 'secure', forcedInterruption=False, quitWiping=False ):
        ''' wipe storage
        :param disk: choose a disk.
        :param spindown: set a time limit or disabled the disk.
        :param forcedInterruption: if wiping is using too much time which flag would click the stop button.
        :param quitWiping: design for testing the stop feature.
        '''
        self.itemname = 'Wipe physical disk'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Disk wipe testing ***')
        self.utility.infoMsg('Wiping mode: %s' % mode)
        self.utility.infoMsg('Forced interruption: %s' % forcedInterruption)
        try:
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % disk).click()
            time.sleep(1)

            # Click edit button
            self.Browser.find_element_by_css_selector('span[id$="-wipe-btnWrap"]').click()
            time.sleep(5)
            self.utility.infoMsg('Click wipe button.')
            time.sleep(8)
            #catch confirm dialogue
            self.detectDialogue()
            time.sleep(8)
            self.Browser.find_element_by_css_selector( '%s'  % wipe_mode[mode]).click()
            time.sleep(8)

            # Determine the stop/close button status
            self.detectWiping()
            while self.wiping == False:
                self.counter +=1
                self.detectWiping()

                # Testing the stop button
                if quitWiping:
                    self.StopWiping()

                # If wiping wastes a long time, I provided the flag for interrupt the testing.
                if self.counter >= 15 and forcedInterruption:
                    self.utility.infoMsg('Wiping wastes a long time. Trying to stop the progress.')
                    self.StopWiping()
                time.sleep(3)

            self.Browser.find_element_by_css_selector(wiping_button["close"]).click()
            self.utility.infoMsg('Close window.')
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname, result='Wiping is finished.')

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: Wipe button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Wipe testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Wipe button is not found:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Wipe testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Wipe testing: Exception has happened. ' + e.message)


    def scan(self):
        ''' Scan storage change '''
        self.itemname = 'Scan physical disk'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Disk scaning testing ***')
        try:
            self.Browser.find_element_by_css_selector('span[id$="scan-btnWrap"]').click()
            time.sleep(30)
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not found. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Scan testing: Stuck in the popup window.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: scan button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Scan testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have choosen a exist time limit:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Scan testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Scan testing: Exception has happened. ' + e.message)

        self.scanning = True
        #time.sleep(5)
        self.detectmsgbox()
        while self.scanning:
            self.detectmsgbox()
        time.sleep(0.5)
        self.utility.pass_test(testname=self.itemname)


    def locate(self, disk = '/dev/sdj', timeup=5):
        ''' LED blinking timing '''
        self.itemname = 'Locate physical disk'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('Disk LED blinking')
        try:
            # Find the target of disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % disk).click()
            time.sleep(8)
            # Click locate button
            self.Browser.find_element_by_css_selector('span[id$="locate-btnWrap"]').click()

        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have provided an exist disk:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='locate testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Exception has happened. ' + e.message)

        try:
            # Change blink timeing
            if (timeup == 0):
                self.timeup0()
            else:
                timeup_setting = self.Browser.find_element_by_name('seconds')
                timeup_setting.clear()
                timeup_setting.send_keys(timeup)
                time.sleep(0.5)

            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            time.sleep(0.5)

            # Checking error message popup or not
            self.catchlocateError()

            self.utility.pass_test(result=self.memo)
            time.sleep(60)
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not found. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Stuck in the popup window.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: time limit or save button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have choosen an exist time limit:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='locate testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Exception has happened. ' + e.message)

    ##### it LED blinking timing got 0seconds.
    def timeup0(self):
        try:
            self.utility.infoMsg('Set 0 seconds for Blinking timeup.')
            timeup_setting = self.Browser.find_element_by_name('seconds')
            timeup_setting.clear()
            timeup_setting.send_keys('0')
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            time.sleep(5)

            # Popup error message
            self.catchlocateError()
            self.utility.infoMsg('Catching alert message')
            time.sleep(5)

            # Reset value
            timeup_setting.clear()
            self.utility.infoMsg('Reset time up value.')
            time.sleep(0.5)
            timeup_setting.send_keys('30')
            time.sleep(8)


        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not found. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Stuck in the popup window.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: time limit or save button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have choosen an exist time limit:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='locate testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='locate testing: Exception has happened. ' + e.message)

    ##### Catch error when we save the settings.
    def catchlocateError(self):
        try:
            self.memo = self.Browser.find_element_by_css_selector('div[id^="component-"][class*="x-window-text"]').text
            self.utility.warnMsg('Catching error msg: ' + self.memo)
            time.sleep(15)
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            #text = self.Browser.find_element_by_xpath('//body//div[contains(@class, "x-message-box")][2]//div[contains(@class, "x-toolbar")]//div//div//a[1]//span').txt
            time.sleep(8)

            return
        except Exception as e:
            print(e.message)
            pass

    def detectWiping(self):
        ''' Detemine the close status is depending on the a href class parameter: x-btn-disabled '''
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-modal"] div[class*="x-toolbar"] div div a[class*="x-btn-disabled"]:nth-child(3)').click()
            self.utility.infoMsg( 'Close button is disabled, you can wait for wiping down or discard the behavior.')

        except ElementNotVisibleException as e4:
            self.utility.infoMsg('Element is invisible. ' + e4.msg)
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: Close button cannot be clicked.' + e3.msg)
            pass
        except NoSuchElementException:
            self.wiping = True
            pass
        except Exception as e:
            self.utility.infoMsg('Wipe detecting: loading...' + e.message)
            pass

    def StopWiping(self):
        ''' Stop the wiping, if stop button is enabled '''
        try:
            self.Browser.find_element_by_css_selector(
                wiping_button['stop']).click()
            self.utility.infoMsg('Stop the wiping...')
            self.wiping=True
            time.sleep(10)
        except ElementNotVisibleException as e4:
            self.utility.infoMsg('Element is invisible. ' + e4.msg)
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: Stop button cannot be clicked.' + e3.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Stop button is not found:' + e2.msg)
            pass
        except Exception as e:
            self.utility.infoMsg('Wipe detecting: loading...' + e.message)
            pass

    def detectmsgbox(self):
        ''' Stop the wiping, if stop button is enabled '''
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"][class*="x-no-progressbar"]')
            msgtext  = self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"]').text
            self.utility.infoMsg('Catch scanning status: ' + msgtext)
            self.scanning = True
            time.sleep(2)
        except ElementNotVisibleException as e4:
            self.utility.infoMsg('Element is invisible. ' + e4.msg)
            self.scanning = False
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: Stop button cannot be clicked. ' + e3.msg)
            self.scanning = False
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Scanning is stop: ' + e2.msg)
            self.scanning = False
            pass
        except Exception as e:
            self.utility.infoMsg('Exception has happened. ' + e.message)
            self.scanning = False
            pass

    def detectDialogue(self):
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a span').is_displayed()
            msgtext = self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-window-body"]').text
            self.utility.infoMsg('Catch message: ' + msgtext)
            time.sleep(3)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a span').click()
            time.sleep(3)
        except NoSuchElementException as e2:
            pass



    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()


if __name__ == "__main__":
    testing = grack_storage_physicaldisks()
    time.sleep(30)
    testing.detectDialogue()
    testing.edit()
    testing.locate()
    # testing.wipe(forcedInterruption=True)
    testing.scan()
    testing.logout()
