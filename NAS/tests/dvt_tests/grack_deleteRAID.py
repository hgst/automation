'''
    Created on May 7th, 2018
    @Author: Philip.Yang

    Full Title: Delete RAID
    Open broswer and goto connect of tab, Click Storage >> RAID
    Determine SSH/TCP tunnel switch status then do the setup behavior.

    For modulized, you can set DoNotDelete to determine the RAID exist or not after creating.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

class deleteRAID():
    def __init__(self):
        self.itemname = 'Delete RAID'
    def delete(self, raidname=None):
        utility = device_info.device_info()
        utility.unittest(self.itemname)
        Browser = utility.browser
        utility.infoMsg('Prepare to remove: %s' %raidname)
        utility.web_login(tabname='storage')
        Browser.find_element_by_css_selector('div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
        time.sleep(8)
        utility.infoMsg('Get into RAID page.')
        remount = True
        if(raidname is None):
            remount = False
            utility.warn_test('RAID Name cannot be null.')
        if (remount):
            try:
                # Click RAID5 and remove
                Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidname).click()
                time.sleep(1)
                # Unmount
                Browser.find_element_by_css_selector('span[id$="-unmount-btnWrap"]').click()
                time.sleep(20)
                utility.infoMsg('Unmount has finished.')
                remount = True
            except ElementNotVisibleException as e2:
                utility.errorMsg('Unmount Error ' + e2.msg)
                utility.fail_test(self.itemname, e2.msg)
                pass
            except NoSuchElementException as e3:
                utility.errorMsg('Error, Element is not exist.' + e3.msg)
                utility.fail_test(self.itemname, e3.msg)
                pass
            except InvalidSelectorException as e4:
                utility.errorMsg('Error, Element cannot be selected.' + e4.msg)
                utility.fail_test(self.itemname, e4.msg)
                pass
            except Exception as e:
                utility.errorMsg('Unmount Error ' + e.message)
                utility.fail_test(self.itemname, e.message)
                pass

        else:
            utility.infoMsg('Unmount is canceled.')

        if (remount):
            try:
                utility.infoMsg('Prepare to delete RAID Volume.')
                Browser.find_element_by_css_selector('span[id$="-delete-btnWrap"]').click()
                time.sleep(8)
                Browser.find_element_by_css_selector(
                    'div[id$="toolbar-targetEl"] > a[class*="x-btn"]:nth-child(2)').click()
                time.sleep(20)
                utility.infoMsg('Delete devices has finished')
                utility.pass_test(self.itemname)

            except ElementNotVisibleException as e2:
                utility.errorMsg('Delete Error ' + e2.msg)
                utility.fail_test(self.itemname, e2.msg)
                pass
            except NoSuchElementException as e3:
                utility.errorMsg('Error, Element is not exist.' + e3.msg)
                utility.fail_test(self.itemname, e3.msg)
                pass
            except InvalidSelectorException as e4:
                utility.errorMsg('Error, Element cannot be selected.' + e4.msg)
                utility.fail_test(self.itemname, e4.msg)
                pass
            except Exception as e:
                utility.errorMsg('Delete Error ' + e.message)
                utility.fail_test(self.itemname, e.message)
                pass

        else:
            utility.infoMsg('Delete is canceled.')

        utility.web_logout()
        utility.get_test_results()

if __name__ == "__main__":
    deleteRAID().delete('sample')
