'''
Created on April 3, 2018
@Author: Philip.Yang

Full Title: GRACK Device Info
# ===================================================================================
# Class device_info
# ===================================================================================
    @device_info
    unittest() To register and start test case.
    weblogin() for login GRACK webpage automatically
    weblogout() for logout GRACK webpage automatically

    infoMsg(): log message with level INFO on the logger
    warnMsg(): log message with level WARNING on the logger
    errorMsg(): log message with level ERROR on the logger

    ssh_execute(): using CLI thourgh ssh connecting
    sftp_connect(): upload file through sftp
    scp_connect(): upload file through scp

'''
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import WebDriverException
import paramiko
import datetime
import pysftp
import time
import sys
import os

sys.path.append("../../../AAT_framework") # for shell script working correctly
from socket import error as SocketError
from global_libraries import CommonTestLibrary as ctl
from global_libraries import scp

tab = {
    'dashboard': 'a[class*="x-tab"]:nth-child(1)',
    'workspaces': 'a[class*="x-tab"]:nth-child(2)',
    'storage': 'a[class*="x-tab"]:nth-child(3)',
    'connect': 'a[class*="x-tab"]:nth-child(4)',
    'accessControl': 'a[class*="x-tab"]:nth-child(5)',
    'system': 'a[class*="x-tab"]:nth-child(6)'
}

class device_info(object):
        """
        device information descritpion.
        """
        _tc_log = ctl.getLogger('AAT.testclient')
        log = ctl.getLogger('DVT.{}'.format(ctl.get_silk_test_name()))
        #ExceptionHandler = ctl.HandleExceptions(log)

        def __init__(self,execute_test=True):
            #self._tc_log = ctl.getLogger('DVT.device_info') # Logger for the test case
            #self.log = ctl.getLogger('DVT.{}'.format(ctl.get_silk_test_name()))
            self.tests = {}
            self._test_list = []
            self.TEST_STARTED = 'in progress'
            self.TEST_PASSED = 'passed'
            self.TEST_WARNED = 'passed with warning'
            self.TEST_FAILED = 'FAILED'
            self.NETWORK_ACTIVE = 'ACTIVE'
            self.NETWORK_ERROR = 'ERROR'
            self.browser = webdriver.Firefox()
            self.grack_username = 'admin'
            self.grack_password = 'gtech'
            self.grack_rootpwd = 'gtech'
            self.deviceIP = 'http://192.168.11.64'
            self.deviceName = 'GRACK'
            self.username = 'test99'
            self.userpwd = 'welc0me'
            self.ssh = paramiko.SSHClient()
            self.scp_client = None
            #if execute_test:
            #    self.get_test_results()

        def start_test(self, testname=None, message=None):
            """ Start a test.

            :param testname: If present, use the specified string as the unique identifier for this test. If not present,
                                     generate a unique ID.
            :return: The testname
            """
            if testname is None:
                # Generate a unique testname
                test_number = 1
                while True:
                    if str(test_number) in self.tests:
                        test_number += 1
                    else:
                        testname = str(test_number)
                        break

            if message is None:
                message = testname

            self.log.info('*** Test started: {} ***'.format(message))
            if testname in self.tests:
                if self.tests[testname] == self.TEST_STARTED:
                    self.log.warning('Duplicate test {} added. Setting to warning.'.format(testname))
                    self.tests[testname] = self.TEST_WARNED
                else:
                    self.log.warning('Duplicate test {} added. '.format(testname) +
                                     'Leaving status as {}'.format(self.tests[testname]))
            else:
                self.tests[testname] = self.TEST_STARTED
                self._test_list.append(testname)

            return testname

        def unittest(self, test_item_name='undefined', msg=None):
                ''' unittest method
                :param register the unit test name to record the test message.
                :param message to log the comment of testcase.
                # To register and start test case
                '''
                self.infoMsg("GRACK UNIT TEST: " + test_item_name)
                self.infoMsg("Device URL: " + self.deviceIP)
                today =  datetime.datetime.today()
                self.infoMsg(today)

                # To register the test case
                self.start_test(test_item_name, msg)
                self.tests[test_item_name] = self.TEST_STARTED

        def ssh_execute(self, login_id=None, login_pwd=None, port=22, msg='busybox ifconfig'):
            ''' ssh linking
            :param port: default port is 22.
            :param msg; default setting would query the interface config
            '''

            #Ping IP
            exist = self.pingDevice()

            #ssh login
            if(exist):
                address = self.deviceIP.split('http://')
                self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                response = ""
                try:
                    """ self.ssh.connect(hostname=ip_address, username=username, password=password, port=port) """
                    if login_id is None:
                        login_id = self.grack_username
                    if login_pwd is None:
                        login_pwd = self.grack_password
                    self.ssh.connect(address[1], port, username=login_id, password=login_pwd)
                    stdin, stdout, stderr = self.ssh.exec_command(msg, get_pty=True)
                    for line in stdout:
                        if 0:
                            print('Output: ' + line.strip('\n'))
                        response = line.strip('\n')
                    self.ssh.close()
                    self.infoMsg('SSH client is working.')
                    return response
                except SocketError as e_msg:
                    self.errorMsg(e_msg.args[1])
                except paramiko.AuthenticationException:
                    self.errorMsg('Authentication failed')
            else:
                self.warnMsg('Device has no response. Please check the device is online.')

        def sftp_connect(self, uname, upwd, sourcepath='./logo.gif', msg='date' ):
            ''' sftp connecting
            :param uname: username
            :param upwd: userpassword
            :param msg: CLI command

            '''
            address = self.deviceIP.split('http://')
            try:
                sftp = pysftp.Connection(host=address[1], username= uname, password=upwd)

                # execute CLI
                respo = sftp.execute(msg)
                self.infoMsg('CLI response: ' +  respo[0])

                # upload image
                with sftp.cd('/tmp/'):
                    sftp.put(sourcepath)
                    # print file under tmp folder
                    for attr in sftp.listdir_attr():
                        print attr

                return True
            except pysftp.AuthenticationException as sftpAuthError:
                self.errorMsg(sftpAuthError.message)
                return False
            except pysftp.ConnectionException as sftpCntError:
                self.errorMsg('Could not connect to %s :  %s' %( sftpCntError.args[0], sftpCntError.args[1]))
                return  False
            except Exception as e:
                self.errorMsg(e.message)
                return False

        # ===========================================================================================================#
        #                                         SCP                                                               #
        # ===========================================================================================================#
        # @utilities.decode_unicode_args
        def scp_connect(self):
            """Open a new SCP connection to the test unit."""
            address = self.deviceIP.split('http://')
            try:
                self.debugMsg('Opening SCP connection for {0}:{1}'.format(address[1], 22))
                transport = paramiko.Transport((address[1], 22))
                transport.connect(username=self.grack_username, password=self.grack_password)
                self.infoMsg('SSH client transport: ' + str(transport))
                self.scp_client = scp.SCPClient(transport)
                self.infoMsg('SCP client: ' + str(self.scp_client))
            except Exception:
                print()
            return self.scp_client

        def debugMsg(self, msg):
            ''' log level: debug '''
            self._tc_log.debug(msg)

        def infoMsg(self, msg):
            ''' log level: Info '''
            self._tc_log.info(msg)

        def warnMsg(self, msg):
            ''' log level: warning '''
            self._tc_log.warning(msg)

        def errorMsg(self, msg):
            ''' log level: error '''
            self._tc_log.error(msg)

        def pingDevice(self):
            ''' ping device '''
            address = self.deviceIP.split('http://')
            response = os.system("ping -c 1 %s" %address[1])
            if response == 0:
                self.infoMsg('Network Active')
                return self.NETWORK_ACTIVE
            else:
                self.infoMsg('Network Error, device no response.')
                return  self.NETWORK_ERROR

        def get_test_results(self):
                """ Get the final test results

                    Log the number of errors and warnings, build the output.xml file, and return the final result
                :return: Number of errors (0 is pass, with or without warnings)
                """
                self._tc_log.info('')
                self._tc_log.info('*** Test case complete ***')

                test_count = len(self.tests)
                if test_count > 0:
                        # Use self.tests for results
                        passed = 0
                        failed = 0
                        warned = 0

                        # Store the results in a list that will later be iterated. We don't want to output the results yet because
                        # we want the total number of pass/failed/warned output first, so this is going to take two loops.
                        test_results = []

                        for test in self._test_list:
                                if self.tests[test] == self.TEST_PASSED:
                                        passed += 1
                                elif self.tests[test] == self.TEST_WARNED:
                                        warned += 1
                                elif self.tests[test] == self.TEST_FAILED:
                                        failed += 1
                                else:
                                        self._tc_log.error('Test: {} has no result. Assuming it failed.'.format(test))
                                        failed += 1
                                        self.tests[test] = self.TEST_FAILED

                                test_results.append('   {}: {}'.format(test, self.tests[test]))

                        if ctl.get_count(ctl.EXCEPTION) > 0:
                                self._tc_log.info('*** Exceptions encountered. Test case FAILED ***')
                                failed = 1 if failed == 0 else failed
                        elif failed > 0:
                                self._tc_log.info('*** Test case FAILED ***')
                        elif warned > 0:
                                self._tc_log.info('*** Test case passed with warnings ***')
                        else:
                                self._tc_log.info('*** Test case passed.')

                        self._tc_log.info('')
                        self._tc_log.info('*** Results summary ***')
                        self._tc_log.info('    {} total tests'.format(test_count))
                        self._tc_log.info('    {} failed'.format(failed))
                        self._tc_log.info('    {} passed ({} with a warning)'.format(passed + warned, warned))
                        self._tc_log.info('')
                        self._tc_log.info('*** Individual test results ***')
                        for result in test_results:
                                self._tc_log.info('    {}'.format(result))

                        #self._build_outputxml(failed, warned, 0)

        def pass_test(self, testname=None, result=None):
                """ Passes the specified test

                :param testname: If present, the test to pass. If not, it will pass the test that is currently
                                 in the state self.TEST_STARTED.
                :param result: Optional string to include in the log

                NOTE: If more than one test is in the state of self.TEST_STARTED, then testname MUST be passed. Otherwise,
                      an exception will be thrown, and the entire test case will fail. This is because it is unclear which
                      test is being passed, so the overall test results will be ambiguous.
                """
                if result is None:
                        # Only one parameter was passed, so first see if the test exists
                        if testname not in self.tests:
                                # Assume it's a result, not a testname
                                result = testname
                                testname = None

                if testname is None:
                        testname = self._find_running_test()

                if testname in self.tests:
                        if self.tests[testname] == self.TEST_STARTED:
                                self.tests[testname] = self.TEST_PASSED
                                self.log.info('*** Test: {} - passed ***'.format(testname))
                        else:
                                self.log.warning(
                                        '*** Test: {} - marked as passed, but status is {} ***'.format(testname,
                                                                                                       self.tests[
                                                                                                               testname]))
                else:
                        self.log.warning(
                                'Test: {} - marked as passed, but not started. Passing with a warning. ***'.format(
                                        testname))
                        self.tests[testname] = self.TEST_WARNED
                        self._test_list.append(testname)

                if result:
                        self.log.info('*** Test result: {} ***'.format(result))

        def fail_test(self, testname=None, error=None, filename=None):
                """ Fail the test

                :param testname: The test to fail. If None, fail the last started test
                :param error: An error message. If None, use the test name
                :param filename: An optional file to store in the Silk results

                """
                if error is None:
                        # Only one parameter was passed, so first see if the test exists
                        if testname not in self.tests:
                                # Assume it's an error, not a testname
                                error = testname
                                testname = None

                if testname is None:
                        testname = self._find_running_test()

                if testname in self.tests:
                        if self.tests[testname] == self.TEST_STARTED:
                                self.tests[testname] = self.TEST_FAILED
                                self.log.error('*** Test: {} - FAILED: {}***'.format(testname, error))
                        else:
                                self.log.error('Test: {} - marked as failed, but status is already {} '.format(testname,
                                                                                                               self.tests[
                                                                                                                       testname]) +
                                               ' - Marking as failed: {} ***'.format(error))
                                self.tests[testname] = self.TEST_FAILED
                else:
                        self.log.error('*** Test: {} - marked as failed, but it was never started. '.format(testname) +
                                       'Marking as failed: {} ***'.format(error))
                        self.tests[testname] = self.TEST_FAILED
                        self._test_list.append(testname)

                if filename is not None:
                        self.log.info('{} stored as part of this error'.format(filename))
                        self.store_file_for_silk_result(filename)

        def warn_test(self, testname=None, warning=None):
                """ Pass the test with a warning

                :param testname: The test to pass. If None, pass the last started test
                :param warning: A warning message. If None, use the test name
                """
                if warning is None:
                        # Only one parameter was passed, so first see if the test exists
                        if testname not in self.tests:
                                # Assume it's a warning, not a testname
                                warning = testname
                                testname = None

                if testname is None:
                        testname = self._find_running_test()

                if testname in self.tests:
                        if self.tests[testname] == self.TEST_STARTED:
                                self.tests[testname] = self.TEST_WARNED
                                self.log.warning('*** Test: {} - passed with WARNING: {} ***'.format(testname, warning))
                        else:
                                if self.tests[testname] == self.TEST_FAILED:
                                        self.log.warning('Test: {} - marked as passed with warning, '.format(testname) +
                                                         'but status is already {}'.format(self.tests[testname]) +
                                                         ' - Leaving status. ***')
                                else:
                                        self.log.warning('Test: {} - marked as passed with warning, '.format(testname) +
                                                         ' but status is already {}'.format(self.tests[testname]) +
                                                         ' - Marking as passed with warning: ***'.format(warning))
                                self.tests[testname] = self.TEST_WARNED
                else:
                        self.log.warning('Test: {} - marked as passed with warning, but it was never started. '.format(
                                testname) +
                                         'Marking as warning: {} ***'.format(warning))
                        self.tests[testname] = self.TEST_WARNED
                        self._test_list.append(testname)

        def _find_running_test(self):
                # Return the test that is currently running. Throw an exception if the number of tests at self.TEST_STARTED
                # is not exactly one.
                started_tests = []
                for test in self.tests:
                        if self.tests[test] == self.TEST_STARTED:
                                started_tests.append(test)
                if len(started_tests) == 0:
                    #raise exception.NoRunningTests('No tests are currently running. Cannot log test result.')
                    self.errorMsg('No tests are currently running. Cannot log test result.')

                if len(started_tests) > 1:
                    #raise exception.MultipleRunningTests('Must pass testname when mutliple tests are runnning. ' +
                    #                                     'Cannot determine test for result.')
                    self.errorMsg('Must pass testname when mutliple tests are runnning. ' + 'Cannot determine test for result.')

                return started_tests[0]

        def web_login(self, tabname=None):
                '''GRACK webpage login automatically.
                :param tabname
                    Accept GRACK tabname to speed up.
                    tabname including: 'dashboard', 'workspaces', 'storage', 'connect', 'accessControl', 'system'
                :raise exception
                    when selenium cannot send username or password that would trigger NoSuchElementException.
                    Checked broswer messagebox and trigger excepetion. If happened
                '''
                self.browser.maximize_window()
                self.browser.get(self.deviceIP)
                time.sleep(2)
                # Keyin username and password and warp to dashboard
                try:
                        username = self.browser.find_element_by_id("username-inputEl")
                        username.send_keys(self.grack_username)
                        password = self.browser.find_element_by_id("password-inputEl")
                        password.send_keys(self.grack_password)

                        self.browser.find_element_by_id("login-panel-login").click()
                        time.sleep(0.5)
                        try:
                                catch_warning = self.browser.find_element_by_css_selector('div[id^="messagebox-"]').is_displayed()
                                self.infoMsg('You have logged in another browser or you didn\'t logout correctly before.')
                                self.browser.find_element_by_css_selector(
                                        'div[id$="-toolbar-targetEl"] > a[id^="button"]:nth-child(2) > span[id$="-btnWrap"]').click()
                        except NoSuchElementException:
                                pass
                        except ElementNotVisibleException :
                                pass


                except NoSuchElementException as e:
                        self.errorMsg('Fail to login. Cannot find the element.')

                else:
                        time.sleep(8)
                        self.infoMsg('Login finished!')

                if (tab != None):
                        try:
                                self.browser.find_element_by_css_selector(tab[tabname]).click()
                                self.infoMsg('Goto Tab: ' + tab + " .")
                        except:
                                pass




        def web_logout(self):
                '''GRACK webpage logout automatically'''
                try:
                        # Click tab and logout of device
                        self.browser.find_element_by_id('splitbutton-1022-btnIconEl').click()
                        time.sleep(2)

                        try:
                            self.browser.find_element_by_id('menuitem-1027-textEl').click() # Apply for FW under v1.1.0
                        except NoSuchElementException:
                            # Handle UI changing for FW v1.1.0
                            self.browser.find_element_by_css_selector('div[id^="workspace-menu-"] div[class*="x-menu-body"] div[class*="x-box-inner"] div[class*="x-box-target"] div:nth-child(5) a span').click()
                            pass
                        time.sleep(2)
                        self.browser.find_element_by_id('button-1006-btnInnerEl').click()

                except WebDriverException as e1:
                        self.errorMsg('Fail to logout, logout button is not clickable at point: ' + e1.msg)
                except NoSuchElementException as e:
                        self.errorMsg('Fail to logout: ' + e.msg)
                except Exception as e2:
                        self.errorMsg(e2.message)
                finally:
                        self.infoMsg("Logout finished!")
                self.browser.close()

        def factory_reset(self):
            ''' factory reset '''
            try:
                # Click tab and reset device
                self.browser.find_element_by_id('splitbutton-1022-btnIconEl').click()
                time.sleep(3)
                self.browser.find_element_by_id('menuitem-1025-textEl').click()
                time.sleep(3)
                spell = self.browser.find_element_by_css_selector('input[id^="messagebox"]')
                spell.send_keys('DELETE')
                time.sleep(3)
                self.browser.find_element_by_id('button-1013-btnInnerEl').click()
                time.sleep(30)
                self.browser.close()
            except WebDriverException as e1:
                self.errorMsg('FACTORY RESET: Fail to execute Factory reset, reset button is not clickable at point: ' + e1.msg)
            except NoSuchElementException as e:
                self.errorMsg('FACTORY RESET: Fail to execute Factory reset: ' + e.msg)
            except Exception as e2:
                self.errorMsg(e2.message)
            finally:
                self.infoMsg("FACTORY RESET: System reset is starting!")

        def _login_init(self):
            try:
                # if web address is not changed.
                self.browser.get(self.deviceIP)
                time.sleep(15)
                username = self.browser.find_element_by_id("username-inputEl")
                username.send_keys(self.grack_username)
                password = self.browser.find_element_by_id("password-inputEl")
                password.send_keys(self.grack_password)
                self.browser.find_element_by_id("login-panel-login").click()
                time.sleep(0.5)
                try:
                    self.browser.find_element_by_css_selector('div[id^="messagebox-"]').is_displayed()
                    self.infoMsg('Init: You have logged in another browser or you didn\'t logout correctly before.')
                    self.browser.find_element_by_css_selector(
                        'div[id$="-toolbar-targetEl"] > a[id^="button"]:nth-child(2) > span[id$="-btnWrap"]').click()
                except NoSuchElementException:
                    self.warnMsg('Init: cannot login the webpage, element is not found.')
                    return
                except ElementNotVisibleException:
                    self.warnMsg('Init: cannot login the webpage, element has been hided.')
                    return

                time.sleep(5)
                self.browser.find_element_by_css_selector(
                    'div[class*="setup-window"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
                time.sleep(1)
                self.browser.find_element_by_css_selector(
                    'div[id*="theme_selector-body"] div div div div a:nth-child(2) span').click()
                time.sleep(2)
                self.browser.find_element_by_css_selector(
                    'div[id*="setup_webadmin"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
                time.sleep(3)
                self.browser.find_element_by_xpath(
                    "//body//div[contains(@class, 'x-message-box')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[2]//span").click()
                time.sleep(10)
                self.web_logout()
                self.infoMsg('Init: The initial is finished.')

            except NoSuchElementException as e:
                self.warnMsg('Init: Fail to login. Maybe the IP address is gone or .')
            except Exception:
                self.warnMsg('Init: Oops!')




if __name__ == "__main__" :
        test_device_info = device_info()
        #################
        test_device_info.web_login()
        time.sleep(3)
        test_device_info.factory_reset()
        #test_device_info._login_init()
        #################
        #test_device_info.pingDevice()
        #test_device_info.unittest('unit test', 'for comment')
        #test_device_info.pass_test()
        #test_device_info.browser.close()

