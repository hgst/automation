'''
    Created on May 16th, 2018
    @Author: Philip.Yang

    Full Title: Access Active Directory configure
    Help to setup Active Directory configure.

'''
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

class ADconfigure():
    def __init__(self):
        self.itemname = 'Access Active Directory Configure'
        self.ad_switch = False
        self.check_insert = False
        self.utility = device_info.device_info()
        self.browser = self.utility.browser


    def configure(self, AD_switch=False, full_domain_name=None, server_IP=None, domain_name=None, kerberos_realm=None, domain_netBIOS_name=None, admin_name=None, admin_pwd=None ):

        self.utility.unittest(self.itemname)

        self.utility.web_login(tabname='accessControl')
        # To detect AD switch status
        try:
            # Click tab of ActiveDirectory
            self.browser.find_element_by_css_selector('div[id^="workspace-node-tab"][class*="x-panel-body-default"] div:nth-child(2) div[class*="x-tab-bar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(8)
            # Detect the switch status
            self.browser.find_element_by_css_selector('div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div div fieldset div div div div[class*="x-form-cb-checked"]:nth-child(1)').is_displayed()
            self.utility.infoMsg('AD switch has been opened.')
            self.ad_switch = True
        except InvalidSelectorException as e2:
            self.utility.errorMsg(e2)
        except Exception as e:
            self.utility.infoMsg('AD switch has been closed.')
            self.ad_switch = False

        # Click button: ActiveDirectory
        if(self.ad_switch != AD_switch):
            try:
                txt = self.browser.find_element_by_css_selector(
                    'fieldset[class*="x-fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="checkbox"] > div[id^="checkbox"] > div[id^="checkbox"] >input[id^="checkbox"]  ')
                txt.click()
                time.sleep(0.5)
                self.utility.infoMsg('AD switch status change.')
            except InvalidSelectorException as e2:
                self.utility.errorMsg(e2)
                pass
            except NoSuchElementException:
                self.utility.errorMsg('AD switch checkbox is not exist.')
                pass
            except Exception as e:
                self.utility.infoMsg('AD switch has been closed.')
                self.ad_switch = False

        if(AD_switch):
            if(full_domain_name is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="domainserverhostname"]')
                insert.clear()
                insert.send_keys(full_domain_name)
            else:
                self.utility.warnMsg('Full domain name is required.')

            if(server_IP is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="domainserveripaddress"]')
                insert.clear()
                insert.send_keys(server_IP)
            else:
                self.utility.warnMsg('Server IP is required.')

            if(domain_name is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="domainname"]')
                insert.clear()
                insert.send_keys(domain_name)
            else:
                self.utility.warnMsg('Domain name is required.')

            if(kerberos_realm is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="realm"]')
                insert.clear()
                insert.send_keys(kerberos_realm)
            else:
                self.utility.warnMsg('Realm is required.')

            if(domain_netBIOS_name is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="netbiosname"]')
                insert.clear()
                insert.send_keys(domain_netBIOS_name)
            else:
                self.utility.warnMsg('NetBIOS username is required.')

            if(admin_name is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="username"]')
                insert.clear()
                insert.send_keys(admin_name)
            else:
                self.utility.warnMsg('Admin username is required.')

            if(admin_pwd is not None):
                insert = self.browser.find_element_by_css_selector('input[name*="userpassword"]')
                insert.clear()
                insert.send_keys(admin_pwd)
            else:
                self.utility.warnMsg('Admin password is required.')
            time.sleep(0.5)

            # Click Save button
            self.clickSavebutton()

            # Check error
            self.catchPopupWindow(5)
            if (self.check_insert):
                try:
                    self.browser.find_element_by_css_selector(
                        'div[id^="messagebox"] > div[id^="messagebox"] > a[id^="button"]').click()
                    self.utility.errorMsg('Please double check your parameter for ADS.')
                except InvalidSelectorException as e2:
                    self.utility.errorMsg(e2.msg)
                    pass
                except NoSuchElementException as e1:
                    self.utility.errorMsg(e1.msg)
                    pass
                except Exception as e:
                    self.utility.errorMsg(e.message)
                    pass
            else:
                # Click messagebox Yes button, Apply the change.
                self.clickYesbutton()

                # Check loading
                _loadingcheck = True
                while _loadingcheck:
                    if self.browser.find_element_by_css_selector('div[id^="loadmask-"][class*="x-mask-msg-text"]').is_displayed():
                        self.utility.infoMsg('Joining domain, please wait...')
                        time.sleep(5)
                    else:
                        _loadingcheck = False

                # Check confirm window
                self.catchPopupWindow(5)
                if(self.check_insert):
                    message = False
                    try:
                        self.browser.find_element_by_css_selector('div[id^="messagebox"][class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').is_displayed()
                        message = True
                    except Exception as e:
                        pass

                    try:
                        if message:
                            comment = self.browser.find_element_by_css_selector(
                                'div[id^="messagebox"][class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').text
                            self.utility.infoMsg(msg=comment)
                            self.browser.find_element_by_css_selector(
                                'div[id^="messagebox"][class*="x-box-inner"] > div[id^="messagebox"][class*="x-box-target"] > a:nth-child(1) > span').click()
                            self.utility.pass_test(testname=self.itemname)
                        else:
                            self.utility.warn_test(testname=self.itemname, warning='The confirm window is invisible.')
                    except InvalidSelectorException as e2:
                        self.utility.errorMsg(e2.msg)
                    except NoSuchElementException as e1:
                        self.utility.errorMsg(e1.msg)
                    except Exception as e:
                        self.utility.errorMsg(e.message)

        else:
            # To click Save button to save AD off.
            self.clickSavebutton()
            # Click messagebox Yes button
            self.clickYesbutton()
            # Final accept button
            self.catchPopupWindow(30)
            if (self.check_insert):
                try:
                    self.browser.find_element_by_css_selector(
                        'div[id^="messagebox"] > div[id^="messagebox"] > a[id^="button"]').click()
                    time.sleep(30)
                    self.utility.pass_test('Switched to workgroup mode successfully')
                except InvalidSelectorException as e2:
                    self.utility.errorMsg(e2.msg)
                except NoSuchElementException as e1:
                    self.utility.errorMsg(e1.msg)
                except Exception as e:
                    self.utility.errorMsg(e.message)

        self.utility.web_logout()
        self.utility.get_test_results()

    def catchPopupWindow(self, waiting=0):
        # Catch Popup window.
        time.sleep(waiting)
        try:
            self.browser.find_element_by_css_selector('div[id^="messagebox"]').is_displayed()
            self.check_insert = True
            return
        except InvalidSelectorException:
            # self.utility.errorMsg()
            self.check_insert =True
            pass
        except NoSuchElementException:
            self.utility.infoMsg('Walk through error detecter.')
            self.check_insert =False
            pass
        except Exception as e:
            self.utility.errorMsg(e.message)
            self.check_insert = True
            pass


    def clickSavebutton(self):
        # Click Save button
        try:
            self.utility.infoMsg('Click Save button.')
            time.sleep(8)
            self.browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
        except InvalidSelectorException:
            self.utility.errorMsg('OK button is not clickable')
            pass
        except NoSuchElementException:
            self.utility.errorMsg('OK button is not exist.')
            pass
        except Exception as e:
            self.utility.errorMsg(e.message)
            pass

    def clickYesbutton(self):
        try:
            self.browser.find_element_by_css_selector('a[class*="x-btn-focus"]').click()
            self.utility.infoMsg('Accept AD configure.')
        except InvalidSelectorException as e2:
            self.utility.errorMsg(e2.msg)
        except NoSuchElementException as e1:
            self.utility.errorMsg(e1.msg)
        except Exception as e:
            self.utility.errorMsg(e.message)


if __name__ == "__main__" :
    # ADconfigure().configure(AD_switch=True, full_domain_name='svr08r2.server08r2.ad', server_IP='192.168.11.241', domain_name='server08r2.ad', kerberos_realm='SERVER08R2.AD', domain_netBIOS_name='SERVER08R2', admin_name='administrator', admin_pwd='Qqqq1111' )
    ADconfigure().configure(AD_switch=False, full_domain_name='', server_IP='', domain_name='', kerberos_realm='', domain_netBIOS_name='', admin_name='', admin_pwd='' )