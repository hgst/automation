'''
Created on Auguest 13, 2018
@Author: Philip Yang

Full Title: GRACK Connect iSCSI_SCST

iSCSI setting, Add/Edit/Delete iSCSI target(including add, edit and delete lun)

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

_iSCSI_tab ={
    'setting' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'target' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
}

_iSCSI_window ={
    'findRAID' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(2) div div[id^="combobox"] div[id$="trigger-picker"]',
    'R2T' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(7) div div[id^="checkbox"] input',
    'immediateData' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(8) div div[id^="checkbox"] input',
    'headerdigest' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(9) div div div[id$="trigger-picker"]',
    'datadigest' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(10) div div div[id$="trigger-picker"]',
    'DataPDUInOrder' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(19) div div[id^="checkbox"] input',
    'DataSequenceInOrder' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(20) div div[id^="checkbox"] input',
    'OFMaker' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(22) div div[id^="checkbox"] input',
    'IFMaker' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(23) div div[id^="checkbox"] input',
    'ok' : 'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span',
    'ok_ver2' : "//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span",
    'popupOK' : 'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span',
    'progressing' : ' div[class*="x-message-box"][class*="x-no-progressbar"] ',
    'list_1' : ' div[id$="-targets-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1)',
    'nv_cache' : "//div[contains(@class, 'x-window-container')]//div[contains(@class, 'x-window-body')]//div//div//div[5]//div[contains(@class, 'x-form-cb-wrap-inner')]//input "
}

_manage_lun = {
    'new' : "a[1]",
    'edit' : "a[2]",
    'delete' : "a[3]",
}

class iSCSI():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'connect iSCSI SCST'
        self.common_switch_status = False
        self.login = False
        self.common_waiting = 15

    def _login(self, tab):
        try:
            self._iSCSCI_tab()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(_iSCSI_tab[tab]).click()
            time.sleep(2)
            self.login = True
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error=e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error=e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error=e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
            pass

    def _iSCSCI_tab(self):
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(9)').click()
            time.sleep(0.5)
            self.utility.infoMsg('Get into the iSCSI page.')
        except ElementNotVisibleException as e:
            self.utility.infoMsg('iSCSI tab button is invisibled.')
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(self.itemname, error='Failed to click \'iSCSI\' button.')
            pass
        except:
            self.utility.fail_test(self.itemname, error='Exception just happened.')
            pass

    def _catchProgress_window(self):
        try:
            # To make sure the progressing is finshed.
            self.Browser.find_element_by_css_selector(_iSCSI_window['progressing']).is_displayed()
            self.utility.infoMsg('Progressing...')
            return True
        except:
            return False

    def _catchOKbtn(self):
        _flag = False
        _messagebox = 1
        while _flag is False:
            try:
                self.Browser.find_element_by_xpath(
                    "//div[contains(@class, 'x-window-container')][%d]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span" % _messagebox).click()
                time.sleep(5)
                _flag = True
                return
            except NoSuchElementException as e1:
                _messagebox += 1
                pass
            except ElementNotVisibleException as e2:
                _messagebox += 1
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass
            if _messagebox > 9:
                self.utility.fail_test(testname=self.itemname, error='Failed to click the ok button.')
                _flag = True
                pass
        return

    def _catchError(self):
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            self.utility.infoMsg('Catching a pop messagebox.')
            time.sleep(0.5)
        except:
            pass

    def _luntab(self, tabname):
        _flag = False
        _lun_window_level = 1
        while _flag is False:
            try:
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-window')][%d]//div[contains(@class, 'x-window-body')]//div//div[contains(@class, 'x-toolbar')]//div//div//%s//span " % (_lun_window_level , tabname)).click()
                _flag = True
            except NoSuchElementException as e1:
                _lun_window_level += 1
                pass
            except ElementNotVisibleException as e2:
                _lun_window_level += 1
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass
            if _lun_window_level > 9:
                self.utility.fail_test(testname=self.itemname, error='ADD LUN: Add button is not found.')
                _flag = True
                pass
        return

    def _clickClosebtn(self):
        _flag = False
        _lun_window_level = 1
        while _flag is False:
            try:
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-window')][%d]//div[contains(@class, 'x-toolbar-footer')]//div//div//a[1]//span " % (_lun_window_level)).click()
                _flag = True
                return
            except NoSuchElementException as e1:
                _lun_window_level += 1
                pass
            except ElementNotVisibleException as e2:
                _lun_window_level += 1
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass
            if _lun_window_level > 9:
                self.utility.fail_test(testname=self.itemname, error='ADD LUN: Close button is not found.')
                _flag = True
                pass
        return

    def _popupOKbtn(self):
        _flag = False
        _messagebox = 1
        while _flag is False:
            try:
                self.Browser.find_element_by_xpath(
                    "//div[contains(@class, 'x-message-box')][%d]//div[contains(@class, 'x-toolbar')]//div//div//a[2]//span" % _messagebox).click()
                _flag = True
                return
            except NoSuchElementException as e1:
                _messagebox += 1
                pass
            except ElementNotVisibleException as e2:
                _messagebox += 1
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass
            if _messagebox > 9:
                self.utility.fail_test(testname=self.itemname, error='Failed to click the popup ok button.')
                _flag = True
                pass
        return

    def _addlun(self):
        try:
            self.itemname = 'Target: Add lun'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Create a lun...')
            self._luntab(tabname=_manage_lun['new'])
            time.sleep(2)

            self.utility.infoMsg('Replace lun number.')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="id"]')
            _tmp.clear()
            _tmp.send_keys('1023')
            time.sleep(0.5)

            self.utility.infoMsg('Replace lun number.')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="filesize"]')
            _tmp.clear()
            _tmp.send_keys('0.01')
            time.sleep(0.5)

            self.Browser.find_element_by_xpath(_iSCSI_window['nv_cache']).click()
            time.sleep(5)

            self._catchOKbtn()
            time.sleep(self.common_switch_status)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD LUN: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD LUN: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD LUN: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD LUN: Exception just happened.')
            pass

    def _editlun(self, lunName='sampleiSCSI-1023'):
        try:
            self.itemname = 'Target: Edit lun'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Edit a lun...')
            time.sleep(2)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % lunName ).click()
            time.sleep(2)
            self._luntab(tabname=_manage_lun['edit'])
            time.sleep(2)

            self.Browser.find_element_by_xpath(_iSCSI_window['nv_cache']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Saving...')
            self._catchOKbtn()
            time.sleep(5)
            self.utility.infoMsg('Apply...')
            self._popupOKbtn()
            time.sleep(self.common_switch_status)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='EDIT LUN: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='EDIT LUN: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='EDIT LUN: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='EDIT LUN: Exception just happened.')
            pass

    def _deletelun(self, lunName='sampleiSCSI-1023'):
        try:
            self.itemname = 'Target: Delete lun'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Delete a lun...')
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % lunName).click()
            self._luntab(tabname=_manage_lun['delete'])
            time.sleep(2)
            #self.Browser.find_element_by_css_selector(_iSCSI_window['popupOK']).click()
            self._popupOKbtn()
            time.sleep(self.common_switch_status)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='DELETE LUN: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='DELETE LUN: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='DELETE LUN: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='DELETE LUN: Exception just happened.')
            pass


    def _pickRAID(self):
        self.common_switch_status=False
        try:
            self.Browser.find_element_by_css_selector(_iSCSI_window['findRAID']).click()
            time.sleep(15)
            self.utility.infoMsg('To pick up a RAID.')
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click()
            time.sleep(15)
            self.common_switch_status = True
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Failed to pick up a RAID  ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Exception just happened.')
            pass

    def _addtarget(self):
        try:
            self.itemname = 'Target: Add'
            self.utility.unittest(test_item_name=self.itemname)

            self.Browser.find_element_by_css_selector('span[id$="-targets-add-btnWrap"]').click()
            time.sleep(2)

            self.utility.infoMsg('Replace name')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="name"]')
            _tmp.clear()
            _tmp.send_keys('sampleiSCSI')
            time.sleep(0.5)

            self._pickRAID()
            if self.common_switch_status is not True:
                return

            self.utility.infoMsg('Replace file size')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="filesize"]')
            _tmp.clear()
            _tmp.send_keys('0.01')
            time.sleep(0.5)

            self.utility.infoMsg('Replace username')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="username"]')
            _tmp.clear()
            _tmp.send_keys('grack110')
            time.sleep(0.5)

            self.utility.infoMsg('Replace password')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="password"]')
            _tmp.clear()
            _tmp.send_keys('111111111110')
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['immediateData']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['R2T']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['headerdigest']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][2]//div//ul/li[2]").click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['datadigest']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][3]//div//ul/li[2]").click()
            time.sleep(0.5)

            # self.utility.infoMsg('Replace max connections')
            # note = self.Browser.find_element_by_css_selector('input[name*="maxconnections"]')
            # note.clear()
            # note.send_keys('8')
            # time.sleep(0.5)

            self.utility.infoMsg('Replace max recv data segment length')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="maxrecvdatasegmentlength"]')
            _tmp.clear()
            _tmp.send_keys('1048575')
            time.sleep(0.5)

            self.utility.infoMsg('Replace max xmit data segment length')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="maxxmitdatasegmentlength"]')
            _tmp.clear()
            _tmp.send_keys('65534')
            time.sleep(0.5)

            self.utility.infoMsg('Replace max burst length')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="maxburstlength"]')
            _tmp.clear()
            _tmp.send_keys('524280')
            time.sleep(0.5)

            self.utility.infoMsg('Replace first burst length')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="firstburstlength"]')
            _tmp.clear()
            _tmp.send_keys('65534')
            time.sleep(0.5)

            self.utility.infoMsg('Replace default time2 wait')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="defaulttime2wait"]')
            _tmp.clear()
            _tmp.send_keys('1')
            time.sleep(0.5)

            self.utility.infoMsg('Replace default time2 retain')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="defaulttime2retain"]')
            _tmp.clear()
            _tmp.send_keys('1')
            time.sleep(0.5)

            self.utility.infoMsg('Replace max outstanding R2T')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="maxoutstandingr2t"]')
            _tmp.clear()
            _tmp.send_keys('1')
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['DataPDUInOrder']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['DataSequenceInOrder']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Replace Error Recovery Level')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="errorrecoverylevel"]')
            _tmp.clear()
            _tmp.send_keys('1')
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['OFMaker']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_iSCSI_window['IFMaker']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Replace OFMark Int')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="ofmarkint"]')
            _tmp.clear()
            _tmp.send_keys('4096')
            time.sleep(0.5)

            self.utility.infoMsg('Replace IFMark Int')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="ifmarkint"]')
            _tmp.clear()
            _tmp.send_keys('4096')
            time.sleep(0.5)

            self.utility.infoMsg('Replace Rsp Timeout')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="rsptimeout"]')
            _tmp.clear()
            _tmp.send_keys('99')
            time.sleep(0.5)

            self.utility.infoMsg('Replace No Pin Interval')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="nopininterval"]')
            _tmp.clear()
            _tmp.send_keys('40')
            time.sleep(0.5)

            self.utility.infoMsg('Replace No Pin Timeout')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="nopintimeout"]')
            _tmp.clear()
            _tmp.send_keys('40')
            time.sleep(0.5)

            self._catchOKbtn()
            time.sleep(5)
            self._catchError()
            time.sleep(2)
            progressing = self._catchProgress_window()
            while progressing:
                time.sleep(self.common_waiting)
                progressing = self._catchProgress_window()
            time.sleep(30)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Exception just happened.')
            pass

    def _edittarget(self):
        try:
            self.Browser.find_element_by_css_selector(_iSCSI_window['list_1']).click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('span[id$="-targets-edit-btnWrap"]').click()
            time.sleep(5)

            self._addlun()
            time.sleep(5)
            self._editlun()
            time.sleep(5)
            self._deletelun()
            time.sleep(5)
            self._clickClosebtn()
            time.sleep(5)

        except Exception:
            self.utility.errorMsg('Failed to locate Target item or Edit button. To ignore the testing.')
            pass


    def _deletetarget(self,):
        try:
            self.itemname = 'Target: Delete'
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_css_selector(_iSCSI_window['list_1']).click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('span[id$="-targets-delete-btnWrap"]').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(_iSCSI_window['popupOK']).click()
            time.sleep(self.common_waiting)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: Exception just happened.')
            pass

    def _Switchstatus(self):
        """ check iSCSI switch status in settings. """
        try:
            self.Browser.find_element_by_css_selector('div[id$="-settings-innerCt"][class*="x-autocontainer-innerCt"] fieldset div div div div[class*="x-form-cb-checked"]').is_displayed()
            return True
        except:
            return False

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def iSCSI_setting(self,switch=True):
        self.itemname = 'iSCSI setting'
        self.utility.unittest(test_item_name=self.itemname)
        self._login(tab='setting')

        if self.login:
            try:
                iscsi_switch = self._Switchstatus()
                if switch == iscsi_switch:
                    pass
                else:
                    self.Browser.find_element_by_css_selector('input[id^="checkbox"]').click()
                    time.sleep(2)
                    self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
                    time.sleep(8)
                self.utility.pass_test(testname=self.itemname)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='No such element '+e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname, error='Element is invisibled '+e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass

    def iSCSI_target(self, switch=True):
        self._login(tab='target')
        if self.login:
            self._addtarget()
            self._edittarget()
            self._deletetarget()



if __name__ == "__main__":
    testing = iSCSI()
    testing.iSCSI_setting(switch=True)
    testing.iSCSI_target()
    time.sleep(30)
    testing._logout()


