'''
    Created on May 22, 2018
    Updated on July 12, 2018
    @Author: Philip.Yang

    Full Title: To add a workspace for SMB share.
    Provide features to setup and edit share config.
    @grack_setupSMB
    setupSMB_simple() help to create a share faster which feature would auto choose the 1st workspace.
    editSMB() can help you edit the share config.
    deleteSMB() can delete the SMB item

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info
import grack_createworkspace

smbPublic = {
    'No': 'a[class*="x-tab"]:nth-child(1)',
    'GuestsAllowed': 'a[class*="x-tab"]:nth-child(2)',
    'OnlyGuests': 'a[class*="x-tab"]:nth-child(3)'
}

buttonsStatus = {
    'readOnly' : 'div[id$="outerCt"] div[class*="x-form-cb-checked"][class*=x-field]:nth-child(6)',
    'browseable' : 'div[id$="outerCt"] div[class*="x-form-cb-checked"][class*=x-field]:nth-child(7)',
    'iACLs' : 'div[id$="outerCt"] div[class*="x-form-cb-checked"][class*=x-field]:nth-child(8)',
    'iPermission' : 'div[id$="outerCt"] div[class*="x-form-cb-checked"][class*=x-field]:nth-child(9)',
    'recycleBin' : 'div[id$="outerCt"] div[class*="x-container"] div div div div div[class*="x-form-cb-checked"]',
    'hidDotFile': 'div[id$="outerCt"] div[class*="x-form-cb-checked"]:nth-child(11)',
    'audit' : 'div[id$="outerCt"] div[class*="x-form-cb-checked"]:nth-child(16)',
}

bt_status = {
    'read_only' : False,
    'browseable' : True,
    'iACLS' : True,
    'iPermission' : True,
    'recycleBin' : False,
    'hidDotFile' : False,
    'audit' : False,
}


class setupSMB():
    def __init__(self):
        self.itemname = 'Setup SMB/CIFS'

        self.utility = device_info.device_info()
        self.browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.workspace_exist=False

        # SMB page
        self.browser.find_element_by_css_selector('div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(4)').click()
        time.sleep(3)


    def setupSMB_simple(self, enableBtn=True, comment="General", Public='No'):
        ''' setup SMB with simple config
        :param enable: to enbal the smb feature
        :param comment: for MB note
        :param Public: including 3 status: No, GuestsAllowed and OnlyGuests.
        '''
        self.itemname = 'SMB Add Item Testing'
        self.utility.unittest(self.itemname)
        # Select 1st workspace
        # To click Add button
        self.workspace_exist =False
        try:
            self.browser.find_element_by_css_selector('span[id$="shares-add-btnWrap"]').click()
            time.sleep(5)
            self.browser.find_element_by_css_selector('div[id^="sharedfoldercombo"] div div div[id$="-trigger-picker"]').click()
            time.sleep(10)
            self.browser.find_element_by_css_selector('div ul li:nth-child(1)').click()
            time.sleep(5)
            self.workspace_exist = True
        except NoSuchElementException:
            self.utility.infoMsg('Cannot find out any workspace.')
            time.sleep(0.5)
            self.newWorkspace()
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname, error=e.message)
            pass

        if(self.workspace_exist):
            try:
                # To determine enable button status
                if (enableBtn):
                    pass
                else:
                    self.browser.find_element_by_css_selector('div[class*="x-window-container"] div[class*="x-window-body"] div div div[id$="outerCt"] div[class*="x-field"]:nth-child(1) div div input[id^="checkbox"]').click()
                    time.sleep(5)

                if (comment):
                    commentbox = self.browser.find_element_by_css_selector('div[id$="outerCt"] div[class*="x-field"]:nth-child(4) div div div input[id^="textfield"] ')
                    time.sleep(5)
                    commentbox.send_keys(comment)
                    time.sleep(5)
                else:
                    pass

                # Click Save button
                self.browser.find_element_by_css_selector('div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
                time.sleep(8)
                self.utility.pass_test(testname=self.itemname)

            except NoSuchElementException as e1:
                self.utility.errorMsg(e1.msg)
                self.utility.fail_test(testname=self.itemname, error=e1.msg)
            except ElementNotVisibleException as e2:
                self.utility.errorMsg(e2.msg)
                self.utility.fail_test(testname=self.itemname, error=e2.msg)
            except Exception as e:
                self.utility.errorMsg(e.message)
                self.utility.fail_test(testname=self.itemname, error=e.message)
        else:
            self.utility.infoMsg('You have to choose an exist workspace.')

    def editSMB(self, workSpace, custom_readOnly=False, custom_browseable=True, custom_ACLs=True, custom_permission=True, custom_recycleBin=False, custom_recycleSizeLimit=0, custom_recycleAutoCleanDays=0, custom_hideDotFile=True, custom_hostAllowed="", custom_hostdeny="", custom_audit=False):
        self.itemname = 'SMB Edit Item Testing'
        self.utility.unittest(self.itemname)
        workSpace_exist = False
        try:
            # Find workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(0.5)
            workSpace_exist = True
        except Exception:
            self.utility.errorMsg('Cannot find the workspace: %s, testing has been interrupted. ' % workSpace)
            self.utility.fail_test(testname=self.itemname, error='You have to choose an exist workspace.')


        if(workSpace_exist):

            # Click edit button
            self.browser.find_element_by_css_selector('a[id$="shares-edit"]').click()
            time.sleep(0.5)

            # Update latest status of button into array.
            for counter in buttonsStatus:
                try:
                    self.browser.find_element_by_css_selector(buttonsStatus[counter])
                    bt_status[counter] = True
                except NoSuchElementException as e1:
                    bt_status[counter] = False
                    pass
                except InvalidSelectorException as e2:
                    self.utility.errorMsg(e2.msg)
                except Exception as e3:
                    self.utility.errorMsg(e3.message)
                finally:
                    print('%s latest status: %s ' % (counter, bt_status[counter]))

            # Compare and change the button status.
            try:
                # read only
                if(custom_readOnly == bt_status['readOnly']):
                    pass
                else:
                    self.browser.find_element_by_css_selector('div[id$="outerCt"] div:nth-child(6) div div input').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** Readonly setup finished. ***')

                # broswerable
                if (custom_browseable == bt_status['browseable']):
                    pass
                else:
                    self.browser.find_element_by_css_selector('div[id$="outerCt"] div:nth-child(7) div div input').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** Browserable setup finished. ***')

                # inherited ACLs
                if (custom_ACLs == bt_status['iACLS']):
                    pass
                else:
                    self.browser.find_element_by_css_selector(
                        'div[id$="outerCt"] div:nth-child(8) div div input').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** iACLs setup finished. ***')

                # inherited permission
                if (custom_permission == bt_status['iPermission']):
                    pass
                else:
                    self.browser.find_element_by_css_selector(
                        'div[id$="outerCt"] div:nth-child(9) div div input').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** iPermission setup finished. ***')

                # Recycle Bin
                if (custom_recycleBin == bt_status['recycleBin']):
                    pass
                else:
                    self.browser.find_element_by_css_selector(
                        'div[id$="outerCt"] div[class*="x-container"] div div div div div[class*="x-field"]:nth-child(1) div div input ').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** RecycleBin setup finished. ***')

                # Recycle size limit
                sizeLimit = self.browser.find_element_by_css_selector(
                    'div[id$="outerCt"] div[class*="x-container"] div div div div div[class*="x-field"]:nth-child(2) div div div input')
                sizeLimit.clear()
                sizeLimit.send_keys(custom_recycleSizeLimit)
                self.utility.infoMsg('*** RecycleSizeLimit setup finished. ***')

                # Recycle auto clan days
                autoClean = self.browser.find_element_by_css_selector(
                    'div[id$="outerCt"] div[class*="x-container"] div div div div div[class*="x-field"]:nth-child(3) div div div input')
                autoClean.clear()
                autoClean.send_keys(custom_recycleAutoCleanDays)
                self.utility.infoMsg('*** RecycleAutoCleanDays setup finished. ***')

                # Hide dot files
                if (custom_hideDotFile == bt_status['hidDotFile']):
                    pass
                else:
                    self.browser.find_element_by_css_selector(
                        'div[id$="outerCt"] div:nth-child(11) div div input').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** HideDotFiles setup finished. ***')

                # Hosts allowed
                hallow = self.browser.find_element_by_css_selector(
                    'div[id$="outerCt"] div:nth-child(14) div div div input')
                hallow.clear()
                hallow.send_keys(custom_hostAllowed)
                self.utility.infoMsg('*** Hosts allowed setup finished. ***')

                # Hosts deny
                hdeny = self.browser.find_element_by_css_selector(
                    'div[id$="outerCt"] div:nth-child(15) div div div input')
                hdeny.clear()
                hdeny.send_keys(custom_hostdeny)
                self.utility.infoMsg('*** Hosts deny setup finished. ***')

                # Audit
                if (custom_audit == bt_status['hidDotFile']):
                    pass
                else:
                    self.browser.find_element_by_css_selector(
                        'div[id$="outerCt"] div:nth-child(16) div div input').click()
                    time.sleep(0.5)
                self.utility.infoMsg('*** Audit setup finished. ***')

                # Click Save button
                self.browser.find_element_by_css_selector(
                    'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
                time.sleep(8)
                self.utility.pass_test(testname=self.itemname, result='Edit workspace has finished.')

            except NoSuchElementException as e1:
                self.utility.errorMsg(e1.msg)
                self.utility.fail_test(testname=self.itemname, error=e1.msg)
            except ElementNotVisibleException as e2:
                self.utility.errorMsg(e2.msg)
                self.utility.fail_test(testname=self.itemname, error=e2.msg)
            except Exception as e:
                self.utility.errorMsg(e.message)
                self.utility.fail_test(testname=self.itemname, error=e.message)

    def deleteSMB(self, workSpace):
        self.itemname = 'SMB delete Item Testing'
        self.utility.unittest(self.itemname)
        workSpace_exist=False
        try:
            # Find workspace
            self.utility.infoMsg('Pick up a workspace.')
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(0.5)
            workSpace_exist = True
        except Exception:
            self.utility.infoMsg('Cannot find the workspace: %s, testing has been interrupted. ' % workSpace)
            self.utility.fail_test(testname=self.itemname, error='You have to choose an exist workspace.')

        if (workSpace_exist):
            self.utility.infoMsg('Delete SMB item.')
            try:
                # Click delete btn
                self.browser.find_element_by_css_selector(' span[id$="-shares-delete-btnWrap"]').click()
                time.sleep(3)

                # Apply
                self.utility.infoMsg('Delete the NFS item.')
                self.browser.find_element_by_css_selector(
                    'div[class*="x-message-box"] div[id$="-toolbar"] div div a:nth-child(2) span').click()
                time.sleep(15)
                # pick error msg
                self.pickErrorMsg()
                time.sleep(0.5)
                self.utility.pass_test(testname=self.itemname, result='Delete SMB item is finished.')
            except NoSuchElementException:
                self.utility.fail_test(testname=self.itemname, error='Delete workflow has an error.')
        else:
            self.utility.warnMsg(testname=self.itemname, msg='The delete workflow is ignoring.')


    def newWorkspace(self):

        try:
            self.utility.infoMsg('Create workspace.')
            self.browser.find_element_by_css_selector('div[id$="-trigger-add"]').click()
            time.sleep(5)

            self.utility.infoMsg('Make a name for workspace.')
            workspaceName = self.browser.find_element_by_css_selector('input[id*="textfield"][name="name"]')
            workspaceName.clear()
            workspaceName.send_keys('General')
            time.sleep(2)

            # Click trigger btn
            self.utility.infoMsg('Select a RAID item for workspace.')
            self.browser.find_element_by_css_selector('input[name="devicefile"').click()
            time.sleep(15)
            self.browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click()
            time.sleep(3)

            # Save
            self.utility.infoMsg(msg='Save the new workspace.')
            self.browser.find_element_by_xpath(
                "//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span ").click()
            time.sleep(30)

            self.browser.find_element_by_css_selector('div[id$="-trigger-picker"]').click()
            time.sleep(0.5)
            self.browser.find_element_by_css_selector('div ul li:nth-child(1)').click()
            time.sleep(0.5)
            self.workspace_exist=True
            return

        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='There has no RAID to create workspace, please create RAID at first.')
            print (e1.msg)
            pass
        except Exception as e:
            self.utility.warnMsg(testname=self.itemname, msg='Exception is happened.')
            print (e.message)
            pass

    def pickErrorMsg(self):
        try:
            self.utility.infoMsg('Trying to pick error message, if it pop-up.')
            # Parser the message
            error_msg = self.browser.find_element_by_css_selector('div[class*="x-plain"] div[id$="-body"] div div div[id^="container-"] div').text
            self.utility.warnMsg(msg=error_msg)
            time.sleep(0.5)
            # Click ok btn
            self.browser.find_element_by_css_selector('div[class*="x-message-box"] div[id^="toolbar-"] div div a:nth-child(1)').click()
        except NoSuchElementException:
            self.utility.infoMsg(msg='Cannot detect any error message.')
            pass



    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__" :
    testing = setupSMB()
    testing.setupSMB_simple(enableBtn=False)
    testing.editSMB(workSpace='General', custom_audit=False, custom_hideDotFile=True)
    testing.deleteSMB(workSpace='General')
    testing.logout()
