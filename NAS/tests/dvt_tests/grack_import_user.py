'''
    Created on April 19, 2018
    @Author: Philip.Yang

    Full Title: Import user and delete user.
    Open browser and import user.

'''

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time

import device_info

class importUser():

    def __init__(self):
        self.testname = 'Import user'
        self.commonFeatures = device_info.device_info()
        self.commonFeatures.web_login(tabname='accessControl')
        self.commonFeatures.unittest(test_item_name=self.testname)
        time.sleep(5)


    def gotest(self,remove=True):

        try:
            # Click Plus button
            self.commonFeatures.browser.find_element_by_css_selector('span[id$="-add-btnEl"]').click()
            time.sleep(2)
            # Click Import in the menu
            self.commonFeatures.browser.find_element_by_css_selector(
                'div[class*="x-box-target"] > div[class*="x-menu-item"]:nth-child(2) > a[class*="x-menu-item-link"]:nth-child(1)').click()
            time.sleep(2)

            # Insert dialogue.
            dialogue  = self.commonFeatures.browser.find_element_by_css_selector('textarea[id$="-inputEl"]')
            dialogue.send_keys("\n uc0079;;uc0079;;grack12;;false")
            self.commonFeatures.browser.find_element_by_css_selector('a[id$="-ok"').click()
            time.sleep(20)
            self.commonFeatures.infoMsg('Import User success.')
            self.commonFeatures.pass_test(testname=self.testname, result='Import user is done.')
        except NameError as e2:
            self.commonFeatures.errorMsg('Error, cannot find the element: ' + e2)
            self.commonFeatures.fail_test(self.testname)
            pass
        except NoSuchElementException:
            self.commonFeatures.errorMsg('Error, cannot find the element in the group list.')
            self.commonFeatures.fail_test(self.testname)
            pass

        self.editUser()

        self.commonFeatures.infoMsg('Remove testing: %s' %remove )
        if(remove):
            self.remove()
        time.sleep(15)

        self.commonFeatures.web_logout()
        self.commonFeatures.get_test_results()
        self.commonFeatures.infoMsg("*** Finish Testing ***")

    def remove(self):
        self.testname = 'Delete user'
        self.commonFeatures.unittest(test_item_name=self.testname)
        # Remove User Account
        try:
            self.commonFeatures.browser.find_element_by_xpath("//*[contains(text(), 'uc0079')]").click()
            self.commonFeatures.infoMsg("Found account: \' uc0079 \', prepare to remove.")
            time.sleep(2)
            # Remove User
            self.commonFeatures.browser.find_element_by_css_selector('span[id$="-delete-btnWrap"').click()
            time.sleep(8)
            self.commonFeatures.browser.find_element_by_css_selector('div[class*="x-message-box"] div[id$="toolbar"] div div a:nth-child(2) span').click()
            time.sleep(8)
            self.commonFeatures.pass_test(self.testname, result='Remove user account success.')
            return

        except NoSuchElementException as e:
            self.commonFeatures.warnMsg('Remove user account fail')
            time.sleep(5)
            self.commonFeatures.warn_test(self.testname, e.msg)
            pass
        except:
            self.commonFeatures.warnMsg('Remove user account fail')
            time.sleep(5)
            self.commonFeatures.warn_test(self.testname, 'There has some problem when we remove user account.')
            pass

    def editUser(self):
        self.testname = 'Edit user'
        self.commonFeatures.unittest(test_item_name=self.testname)
        # Edit user account
        try:
            self.commonFeatures.browser.find_element_by_xpath("//*[contains(text(), 'uc0079')]").click()
            time.sleep(0.5)
            # Click edit button
            self.commonFeatures.browser.find_element_by_css_selector(' span[id$="-edit-btnWrap"] ').click()
            time.sleep(2)

            self.commonFeatures.infoMsg('Replace comment...')
            _comment = self.commonFeatures.browser.find_element_by_name('comment')
            _comment.clear()
            _comment.send_keys('New Comment')
            time.sleep(0.5)

            self.commonFeatures.infoMsg('Replace email...')
            _mailaddress = self.commonFeatures.browser.find_element_by_name('email')
            _mailaddress.clear()
            _mailaddress.send_keys('fakemail@wdc.com')
            time.sleep(0.5)

            self.commonFeatures.infoMsg('Replace password...')
            _password = self.commonFeatures.browser.find_element_by_name('password')
            _password.clear()
            _password.send_keys('111111')
            time.sleep(0.5)

            self.commonFeatures.infoMsg('Replace password confirm...')
            _confirmpassword = self.commonFeatures.browser.find_element_by_name('passwordconf')
            _confirmpassword.clear()
            _confirmpassword.send_keys('111111')
            time.sleep(0.5)

            self.commonFeatures.infoMsg('Saving...')
            self.commonFeatures.browser.find_element_by_css_selector(' span[id$="-ok-btnWrap"]').click()
            time.sleep(5)

            self.commonFeatures.pass_test(testname=self.testname, result='Import and edit user is done.')

        except NoSuchElementException as e:
            self.commonFeatures.warnMsg('Edit user account fail: element is missing.')
            time.sleep(0.5)
            self.commonFeatures.warn_test(self.testname, e.msg)
            pass
        except :
            self.commonFeatures.warnMsg('Edit user account fail')
            time.sleep(0.5)
            self.commonFeatures.warn_test(self.testname, 'There has some problem when we remove user account.')
            pass

if __name__ == "__main__" :
    importUser = importUser()
    importUser.gotest(remove=False)


