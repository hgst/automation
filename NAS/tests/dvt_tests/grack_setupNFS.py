'''
Created on July 5, 2018
@Author: Philip Yang

Full Title: GRACK Connect NFS

NFS service: add, edit and delte item.
NFS settings: function on/off and number of servers

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


privilege_permission ={
    'read_only':"//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    'read_write':"//div[contains(@class, 'x-boundlist')][2]//div//ul/li[2] "
}

user_access = {
    'enabled':"//div[contains(@class, 'x-boundlist')][3]//div//ul/li[1] ",
    'disabled':"//div[contains(@class, 'x-boundlist')][3]//div//ul/li[2] "
}



class grack_setupNFS():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'storage setupNFS'
        self.pickup=False
        self.nfs_switch=False
        self.numproc_check=False
        # Click NFS tab
        #self.Browser.find_element_by_css_selector('span[id*="tab-1060-btnWrap"]').click()
        self.Browser.find_element_by_css_selector(
            'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(3)').click()
        time.sleep(0.5)


    def settings(self, function_switch=True, servers='28'):
        ''' NFS settings
        :param function _switch on/off
        :param servers: number of servers
        '''

        self.itemname = 'NFS settings Testing'
        self.utility.unittest(self.itemname)

        try:
            int(servers)
            self.utility.infoMsg('Check numbers of servers: %s'%servers)
            self.numproc_check=True
        except:
            self.utility.fail_test(testname=self.itemname, error='You cannot set a string into numbers of server.')
            self.numproc_check=False

        try:
            self.Browser.find_element_by_css_selector('div[id$="-center-body"] div div div div[id$="-innerCt"] div a:nth-child(2) span').click()
            time.sleep(3)

            self.utility.infoMsg('Catching switch status...')
            self.switchStatus()
            time.sleep(3)

            if(self.nfs_switch == function_switch):
                pass
            else:
                self.utility.infoMsg('Change swithc status...')
                self.Browser.find_element_by_css_selector('div[class*="x-autocontainer-innerCt"][id^="fieldset-"] div[id^="checkbox"] div div input').click()
                time.sleep(0.5)

            # Modify numbers of server
            if self.numproc_check:
                numproc = self.Browser.find_element_by_name('numproc')
                numproc.clear()
                self.utility.infoMsg('Replacing numbers...')
                numproc.send_keys(servers)
                time.sleep(0.5)

            # Save
            self.utility.infoMsg('Saving the settings.')
            self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Settings testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings testing: Exception has happened. ' + e.message)
            pass

    def delete(self, note='NFS testing'):
        ''' NFS delete
        :param note: depending on the comment to find the target.
        '''
        self.itemname = 'NFS Delete Item Testing'
        self.utility.unittest(self.itemname)
        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click delete btn
            self.Browser.find_element_by_css_selector(' span[id$="-shares-delete-btnWrap"]').click()
            time.sleep(3)

            # Apply
            self.utility.infoMsg('Delete the NFS item.')
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[id$="-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(15)

            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Delete testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Exception has happened. ' + e.message)
            pass

    def edit(self, access_IP='192.168.11.12/108', note='NFS testing'):
        ''' NFS edit
        :param access_IP: modify the access ip range.
        :param note: depending on the comment to find the target.
        '''
        self.itemname = 'NFS Edit Item Testing'
        self.utility.unittest(self.itemname)
        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click Edit btn
            self.Browser.find_element_by_css_selector(' span[id$="-shares-edit-btnWrap"]').click()
            time.sleep(3)

            # Replace IP
            self.utility.infoMsg('Set access IP: %s' % access_IP)
            clientIP = self.Browser.find_element_by_name('client')
            clientIP.clear()
            clientIP.send_keys(access_IP)
            time.sleep(0.5)

            # Save
            self.utility.infoMsg('Save item')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(20)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Exception has happened. ' + e.message)
            pass

    def add(self, access_IP='192.168.11.8/254', permission='read_write', extraOption='no_subtree_check', userAccess='disabled', note='NFS testing'):
        ''' NFS Add item
        :param access_IP : access IP range
        :param permission : Privilege permission
        :param extraOption: please refer to the NFS tech docs
        :param userAccess: user access control
        :param note: comment
        '''
        self.itemname='NFS Add Item Testing'
        self.utility.unittest(self.itemname)

        try:
            # Click add btn
            self.Browser.find_element_by_css_selector(' span[id$="-shares-add-btnWrap"]').click()
            time.sleep(0.5)

            # TRY TO PICK A WORKSPACE
            self.pickWorkspace()

            if self.pickup == False:
                # TRY TO CREATE A WORKSPACE
                self.utility.infoMsg('Create a workspace first.')
                self.createWorkspace()
                self.pickWorkspace()

            # Set IP
            self.utility.infoMsg('Set access IP: %s' %access_IP)
            clientIP = self.Browser.find_element_by_name('client')
            clientIP.clear()
            clientIP.send_keys(access_IP)
            time.sleep(0.5)

            # Privilege_Permission
            self.utility.infoMsg('Set permission...')
            self.Browser.find_element_by_css_selector('div[id$="-innerCt"][class*="x-autocontainer-innerCt"] div:nth-child(3) div div div[id$="trigger-picker"]').click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(privilege_permission[permission]).click()
            time.sleep(0.5)

            # Advanced option
            self.utility.infoMsg('Replace extra option...')
            option = self.Browser.find_element_by_name('extraoptions')
            option.clear()
            option.send_keys(extraOption)
            time.sleep(0.5)

            # Allow all users file access
            self.utility.infoMsg('Set user access permission...')
            self.Browser.find_element_by_css_selector(
                'div[id$="-innerCt"][class*="x-autocontainer-innerCt"] div:nth-child(5) div div div[id$="trigger-picker"]').click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(user_access[userAccess]).click()
            time.sleep(0.5)

            # Add note
            self.utility.infoMsg('Replace comment...')
            option = self.Browser.find_element_by_name('comment')
            option.clear()
            option.send_keys(note)
            time.sleep(0.5)

            # Save
            self.utility.infoMsg('Save item')
            time.sleep(5)
            self.Browser.find_element_by_css_selector('div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(20)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Add testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Exception has happened. ' + e.message)
            pass

    def switchStatus(self):
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-autocontainer-innerCt"][id^="fieldset-"] div[class*="x-form-cb-checked"][id^="checkbox"]').is_displayed()
            self.nfs_switch=True
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the class:"x-form-cb-checked". Maker the switch status.')
            self.nfs_switch=False
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Catching switch status: Exception has happened. ' + e.message)
            pass

    def pickWorkspace(self):
        try:
            self.utility.infoMsg('Pick up the 1st workspace.')
            self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            time.sleep(5)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            time.sleep(3)
            self.pickup=True
        except NoSuchElementException:
            self.utility.warnMsg('There has no workspace to pick up.')
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def createWorkspace(self):
        ''' createWorkspace
        Casue the UI cannot detect the workspace so that we trigger a new one for the testing.
        '''
        try:
            self.utility.infoMsg('Create workspace.')
            self.Browser.find_element_by_css_selector('div[id$="trigger-add"]').click()
            time.sleep(5)

            self.utility.infoMsg('Make a name for workspace.')
            workspaceName = self.Browser.find_element_by_name('name')
            workspaceName.send_keys('sampleWorkspace')
            time.sleep(2)

            # Click trigger btn
            self.utility.infoMsg('Select a RAID item for workspace.')
            self.Browser.find_element_by_css_selector('input[name="devicefile"').click()
            time.sleep(8)
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click()
            time.sleep(3)

            # Save
            self.utility.infoMsg('Save the new workspace.')
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span ").click()
            time.sleep(30)
            return

        except NoSuchElementException as e1:
            self.utility.fail_test('There has no RAID to create workspace, please create RAID at first.')
            print (e1.msg)
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()


if __name__ == "__main__":
    testing = grack_setupNFS()
    testing.add()
    testing.edit()
    testing.delete()
    testing.settings()
    testing.logout()
