'''
Created on July 18, 2018
@Author: Philip Yang

Full Title: GRACK Connect Rsync: Job

Rsync service: add_local, add_remote, edit and delete item.


'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

Rsync_option = {
    'executionMode' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(3) div div div[id$="trigger-picker"] ',
    'source' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(4) div div div[id$="trigger-picker"] ',
    'remote_source' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(4) div div div[id$="trigger-picker"] ',
    'backup' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(6) div div div[id$="trigger-picker"] ',
    'remote_backup' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(6) div div div[id$="trigger-picker"] ',
    'minutes' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(9) div div div div[id^="combobox"] div div div[id$="trigger-picker"] ',
    'hours' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(10) div div div div[id^="combobox"] div div div[id$="trigger-picker"] ',
    'days' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(11) div div div div[id^="combobox"] div div div[id$="trigger-picker"] ',
    'months' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(12) div div div[id$="trigger-picker"] ',
    'weeks' : 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(13) div div div[id$="trigger-picker"] ',
}

sync_mode = {
    'push' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    'pull' :  "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[2] "
}

sync_resource = {
    'source' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    'backup' : "//div[contains(@class, 'x-boundlist')][3]//div//ul/li[2] ",
    'remote_source' : "//div[contains(@class, 'x-boundlist')][3]//div//ul/li[1] ",
    'remote_backup' : "//div[contains(@class, 'x-boundlist')][3]//div//ul/li[2] ",
}

timer ={
    'minutes' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[9] ",
    'hours' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[8] ",
    'days'  : "//div[contains(@class, 'x-boundlist')][3]//div//ul/li[7] ",
    'months'  : "//div[contains(@class, 'x-boundlist')][4]//div//ul/li[6] ",
    'weeks'  : "//div[contains(@class, 'x-boundlist')][5]//div//ul/li[5] ",
}

switchMembers = {
    #'switch': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(1) div div input[id^="checkbox"] ',
    'nMins':  'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(9) div div div div[id^="checkbox"] div div input ',
    'nHours': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(10) div div div div[id^="checkbox"] div div input ',
    'nDays': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(11) div div div div[id^="checkbox"] div div input ',
    'trialRun': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(14) div div input ',
    'recursive': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(15) div div input ',
    'times': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(16) div div input ',
    'compress': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(17) div div input ',
    'archive': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(18) div div input ',
    'delete': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(19) div div input ',
    'quiet': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(20) div div input ',
    'pPermission': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(21) div div input ',
    'pACLs': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(22) div div input ',
    'pExtenedAttributes': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(23) div div input ',
    'kptf': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(24) div div input ',
    'sendEmail': 'div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(25) div div input ',
}


class setupRsyncJob():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'connect setupRsync job'
        self.pickup = False
        self.login = False
        self.progress = True
        self._gotoRsync() # make sure login behavior


    def _gotoRsync(self):
        # Click Rsync tab button
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(7)').click()
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Rsync button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Rsync Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Rsync Button cannot be clicked.')
            self._gotoRsync_rev()
            pass

    def _gotoRsync_rev(self):
        # Click more and Rsync tab button
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(10)').click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(7)').click()
            time.sleep(1)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Rsync button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Rsync Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Rsync Button cannot be clicked.')
            pass


    def _pickType(self, type):
        try:
            self.utility.infoMsg('Pick up the Rsync type.')
            self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            time.sleep(20)
            if type=='local':
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            elif type=='remote':
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ").click()
            else:
                self.utility.warnMsg(msg='You send a wrong type for the testing, we would replace your request to local type to pretend the testing still working.')
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            time.sleep(3)
        except NoSuchElementException:
            self.utility.warnMsg('There has no workspace to pick up.')
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def _pickWorkspace(self, select):
        try:
            self.utility.infoMsg('Trying to pick up workspace.')
            self.Browser.find_element_by_css_selector(Rsync_option[select]).click()
            time.sleep(5)
            self.Browser.find_element_by_xpath(sync_resource[select]).click()
            self.pickup = True
        except NoSuchElementException:
            self.utility.warnMsg('There has no workspace to pick up.')
            self.pickup = False
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            self.pickup = False
            pass

    def _pickExecutionmode(self, mode):
        try:
            self.utility.infoMsg('Trying to pick up sync mode.')
            self.Browser.find_element_by_css_selector(Rsync_option['executionMode']).click()
            time.sleep(5)
            self.Browser.find_element_by_xpath(sync_mode[mode]).click()
            self.pickup = True
        except NoSuchElementException:
            self.utility.warnMsg('There has no workspace to pick up.')
            self.pickup = False
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            self.pickup = False
            pass

    def _modifyOptions(self):
        # Modify the options.
        ''''''
        try:
            self.utility.infoMsg('Modify the timer.')

            self.Browser.find_element_by_css_selector(Rsync_option['minutes']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(timer['minutes']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(Rsync_option['hours']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(timer['hours']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(Rsync_option['days']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(timer['days']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(Rsync_option['months']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(timer['months']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(Rsync_option['weeks']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(timer['weeks']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Click all switch.')

            for members in switchMembers:
                self.utility.infoMsg("Touch: " + members)
                self.Browser.find_element_by_css_selector(switchMembers[members]).click()
                time.sleep(0.5)
            return

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: Stuck in the page.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname, error='Edit testing: Exception has happened. ' + e.message)
            pass

    def _checkingProgress(self):
        ''' start Rsync progress'''
        try:
            self.Browser.find_element_by_css_selector('a[id$="-stop"][class*="x-btn-disabled"]').is_displayed()
            self.utility.infoMsg('Progress is stop.')
            self.progress = False
            return
        except NoSuchElementException:
            self.utility.infoMsg('Cannot catch class "x-btn-disabled" so we pretend the progress still running.')
            self.progress = True
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Execution testing: Exception has happened. ' + e.message)
            self.progress = False
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def add_remote(self, remote="rsync://192.168.11.94/AFPsampleWorkspace", sync_mode='pull', note='samplestring2'):
        ''' add_remote , TO ADD A REMOTE SOURCE OR DESTINATION INTO A RSYNC JOB
        :param remote : rsync address for source or destinaion.
        :param sync_mode : push or pull the data.
        :param note : for comment string
        '''
        self.itemname = 'Add Rsync Remote Item Testing'
        self.utility.unittest(self.itemname)
        Type = 'remote'
        if self.login:
            try:
                # Click add btn
                self.Browser.find_element_by_css_selector(' span[id$="-jobs-add-btnWrap"]').click()
                time.sleep(2)

                # 1, TRY TO PICK A TYPE
                self._pickType(type=Type)
                time.sleep(0.5)

                # 2, PICK A EXECUTION MODE
                self._pickExecutionmode(mode=sync_mode)


                if sync_mode == 'pull':
                    # 3, PICK A SOURCE WORKSPACE
                    self._pickWorkspace(select='remote_backup')

                    # 4, destination
                    self.utility.infoMsg('Fill in the remote source server.')
                    addr = self.Browser.find_element_by_css_selector('input[class*="x-form-text"][name*="srcuri"]')
                    # addr = self.Browser.find_element_by_css_selector('div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(7) div div div[id$="-inputWrap"] input')
                    addr.send_keys(remote)
                    time.sleep(0.5)
                elif sync_mode == 'push':
                    # 3, PICK A DESTINATION WORKSPACE
                    self._pickWorkspace(select='remote_source')

                    # 4, destination
                    self.utility.infoMsg('Fill in the remote source server.')
                    addr = self.Browser.find_element_by_css_selector('input[class*="x-form-text"][name*="desturi"]')
                    # addr = self.Browser.find_element_by_css_selector('div[id^="ext-comp"][class*="x-autocontainer-innerCt"] div:nth-child(7) div div div[id$="-inputWrap"] input')
                    addr.send_keys(remote)
                    time.sleep(0.5)
                else:
                    self.pickup = False
                    self.utility.warnMsg(msg='You have chosen an unsupported option.')

                if self.pickup:
                    # 5, comment
                    self.utility.infoMsg('Write down the comment.')
                    txt = self.Browser.find_element_by_css_selector('input[name*="comment"]')
                    txt.send_keys(note)
                    time.sleep(0.5)

                    self.Browser.find_element_by_css_selector(
                        'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
                    time.sleep(8)

                    self.utility.pass_test(testname=self.itemname)
                else:
                    self.utility.infoMsg(
                        "Cause the problem of x-boundlist numbers, we do not support create workspace in this testing.")
                    self.utility.warn_test(testname=self.itemname,
                                           warning="Testing is not finished. Please prepare a least 2 workspace for testing.")


            except ElementNotVisibleException as e1:
                self.utility.infoMsg('Element is not visible. ' + e1.msg)
                self.utility.fail_test(testname=self.itemname,
                                       error='Add testing: Stuck in the page.')
                pass
            except InvalidSelectorException as e3:
                self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
                self.utility.fail_test(testname=self.itemname,
                                       error='Add testing: Stuck in the page.')
                pass
            except NoSuchElementException as e2:
                self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
                self.utility.fail_test(testname=self.itemname, error='Add testing: ' + e2.msg)
                pass
            except Exception as e:
                self.utility.fail_test(testname=self.itemname,
                                       error='Add testing: Exception has happened. ' + e.message)
                pass

    def add_local(self, note='samplestring'):
        ''' add_local  , TO PAIR 2 LOCAL WORKSPACE INTO A RSYNC JOB
        :param note : for comment
        '''
        self.itemname = 'Add Rsync Local Item Testing'
        self.utility.unittest(self.itemname)
        Type = 'local'
        if self.login:
            try:
                # Click add btn
                self.Browser.find_element_by_css_selector(' span[id$="-jobs-add-btnWrap"]').click()
                time.sleep(2)

                # 1, TRY TO PICK A TYPE
                self._pickType(type=Type)
                time.sleep(0.5)

                # 2, PICK A SOURCE WORKSPACE
                self._pickWorkspace(select='source')

                # 3, PICK A BACKUP WORKSAPCE
                self._pickWorkspace(select='backup')

                if self.pickup :
                    # 4, comment
                    txt = self.Browser.find_element_by_css_selector('input[name*="comment"]')
                    txt.send_keys(note)
                    time.sleep(0.5)

                    self.Browser.find_element_by_css_selector(
                        'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
                    time.sleep(8)

                    self.utility.pass_test(testname=self.itemname)
                else:
                    self.utility.infoMsg("Cause the problem of x-boundlist numbers, we do not support create workspace in this testing.")
                    self.utility.warn_test(testname=self.itemname, warning="Testing is not finished. Please prepare a least 2 workspace for testing.")


            except ElementNotVisibleException as e1:
                self.utility.infoMsg('Element is not visible. ' + e1.msg)
                self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
                pass
            except InvalidSelectorException as e3:
                self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
                self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
                pass
            except NoSuchElementException as e2:
                self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
                self.utility.fail_test(testname=self.itemname, error='Add testing: ' + e2.msg)
                pass
            except Exception as e:
                self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Exception has happened. ' + e.message)
                pass

    def edit(self, note='samplestring' ):
        ''' edit ,  Edit Rsync item
            :param note: Rsync comment which help to locate the target.
        '''
        self.itemname = 'Edit Rsync Item Testing'
        self.utility.unittest(self.itemname)

        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//div[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click Edit btn
            self.utility.infoMsg('Preparing to Edit the item.')
            self.Browser.find_element_by_css_selector(' span[id$="-jobs-edit-btnWrap"]').click()
            time.sleep(3)

            # Modify options
            self._modifyOptions()

            # Apply
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
            time.sleep(8)

            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: Stuck in the page.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname, error='Edit testing: Exception has happened. ' + e.message)
            pass

    def execute(self, note='samplestring'):
        ''' execute , Execute Rsync item
            :param note: Rsync comment which help to locate the target.
        '''
        self.itemname = 'Execution Rsync Item Testing'
        self.utility.unittest(self.itemname)
        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//div[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click Delete btn
            self.utility.infoMsg('Preparing to execute the item.')
            self.Browser.find_element_by_css_selector(' span[id$="-jobs-run-btnWrap"]').click()
            time.sleep(3)

            # Click start
            self.Browser.find_element_by_css_selector('span[id$="start-btnWrap"]').click()
            time.sleep(5)
            self._checkingProgress()
            while self.progress:
                time.sleep(5)
                self._checkingProgress()

            self.utility.infoMsg('Close the executing dialogue.')
            self.Browser.find_element_by_css_selector('span[id$="-close-btnWrap"]').click()
            time.sleep(0.5)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname, error='Execution testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname, error='Execution testing: Stuck in the page.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Execution testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname, error='Execution testing: Exception has happened. ' + e.message)
            pass

    def delete(self, note='samplestring'):
        ''' delete , Delete Rsync item
            :param note: Rsync comment which help to locate the target.
        '''

        self.itemname = 'Delete Rsync Item Testing'
        self.utility.unittest(self.itemname)
        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//div[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click Delete btn
            self.utility.infoMsg('Preparing to delete the item.')
            self.Browser.find_element_by_css_selector(' span[id$="-jobs-delete-btnWrap"]').click()
            time.sleep(3)

            # Apply
            self.utility.infoMsg('Delete the Rsync item.')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[id$="-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname, error='Delete testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname, error='Delete testing: Stuck in the page.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Delete testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname, error='Delete testing: Exception has happened. ' + e.message)
            pass

if __name__ == "__main__":
    testing = setupRsyncJob()
    time.sleep(3)
    testing.add_local()
    #testing.add_remote()
    testing.edit()
    testing.execute()
    testing.delete()
    #testing.delete(note='samplestring2')
    testing.logout()
