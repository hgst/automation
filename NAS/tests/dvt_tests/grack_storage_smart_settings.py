'''
Created on July 4, 2018
@Author: Philip Yang

Full Title: GRACK S.M.A.R.T. settings

S.M.A.R.T.: Self-monitoring, Analysis and Reporting Technology.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

smart_device_tab = {
    'device' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(1) span',
    'schedule' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(2) span',
    'settings' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(3) span'
}

power_mode = {
    'nerer' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ",
    'sleep' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ",
    'standby' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[3] ",
    'idle' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[4] "
}


class grack_storage_smart_schedultedtest():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.itemname = 'storage smart scheduled settings'
        # goto the item
        self.clicktab(tab=smart_device_tab['settings'])
        time.sleep(0.5)

    def run_testing(self, setuptimer='3600', mode='idle', report_diff='20', report_informal='55', report_critical='60'):
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Change swithc status...')
            self.Browser.find_element_by_css_selector(' div[id$="settings-innerCt"] fieldset:nth-child(1) div[id^="fieldset"] div div div div div input').click()
            time.sleep(0.5)

            self.utility.infoMsg('Change timer...')
            timer = self.Browser.find_element_by_name('interval')
            timer.clear()
            timer.send_keys('%s' %setuptimer)
            time.sleep(0.5)

            self.utility.infoMsg('Change power mode...')
            self.Browser.find_element_by_css_selector(' div[id$="trigger-picker"]').click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(power_mode[mode]).click()
            time.sleep(0.5)

            self.utility.infoMsg('Change differene report setting...')
            diff = self.Browser.find_element_by_name('tempdiff')
            diff.clear()
            diff.send_keys('%s' % report_diff)
            time.sleep(0.5)

            self.utility.infoMsg('Change informal report setting...')
            informal = self.Browser.find_element_by_name('tempinfo')
            informal.clear()
            informal.send_keys('%s' % report_informal)
            time.sleep(0.5)

            self.utility.infoMsg('Change critical report setting...')
            crit = self.Browser.find_element_by_name('tempcrit')
            crit.clear()
            crit.send_keys('%s' % report_critical)
            time.sleep(0.5)

            self.utility.infoMsg('Save change...')
            self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"').click()
            time.sleep(30)
            self.utility.pass_test(testname=self.itemname)
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='SMART settings testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: button or textarea cannot be selected.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='SMART settings testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the button or textarea. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='SMART settings testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='SMART settings testing: Exception has happened. ' + e.message)



    def clicktab(self, tab):
        try:
            # Click S.M.A.R.T.  and Device tab
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(tab).click()
            time.sleep(0.5)
            return

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Clicktab testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Exception has happened. ' + e.message)

    def logout(self):
        self.utility.get_test_results()
        self.utility.web_logout()

if __name__ == "__main__":
    testing = grack_storage_smart_schedultedtest()
    testing.run_testing()
    time.sleep(60)
    testing.logout()

