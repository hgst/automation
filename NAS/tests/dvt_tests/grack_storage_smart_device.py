'''
Created on June 28, 2018
@Author: Philip Yang

Full Title: GRACK S.M.A.R.T. device
Features including device edit and information setup

S.M.A.R.T.: Self-monitoring, Analysis and Reporting Technology.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

smart_device_tab = {
    'device' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(1) span',
    'schedule' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(2) span',
    'settings' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(3) span'
}

device_btn = {
    'edit' : 'span[id$="-devices-edit-btnWrap"]',
    'information' : 'span[id$="-devices-information-btnWrap"]'
}

info_subtab = {
    'device' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div[id$="-innerCt"] div a:nth-child(1) span',
    'attribute' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div[id$="-innerCt"] div a:nth-child(2) span',
    'log' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div[id$="-innerCt"] div a:nth-child(3) span',
    'extended_info' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div[id$="-innerCt"] div a:nth-child(4) span',
    'history' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div[id$="-innerCt"] div a:nth-child(5) span'
}


class grack_storage_smart_device():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.itemname = 'storage smart device'
        self.switch_status = True

    def smart_device_edit(self, disk='/dev/sdc', smartSwitch = True):
        ''' device edit '''
        self.itemname = 'S.M.A.R.T. device Edit'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Device Edit ***')

        # goto the item
        self.clicktab(tab=smart_device_tab['device'])
        time.sleep(15)
        try:
            # Select disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % disk).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(device_btn['edit']).click()
            time.sleep(0.5)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Exception has happened. ' + e.message)
        # Checking status
        self.smart_edit_switch_status()
        try:
            if self.switch_status != smartSwitch:
                self.Browser.find_element_by_css_selector(' div[id^="checkbox-"] div div input').click()
                time.sleep(3)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
            time.sleep(20)
            self.utility.pass_test(testname=self.itemname,result='S.M.A.R.T. switch testing has finished.')
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to click the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Exception has happened. ' + e.message)

    def smart_device_information(self, disk='/dev/sdc'):
        ''' device info '''
        self.itemname = 'S.M.A.R.T. device Info'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Device Information ***')

        # goto the item
        self.clicktab(tab=smart_device_tab['device'])
        time.sleep(0.5)

        try:
            # Select disk and click info button
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % disk).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(device_btn['information']).click()
            time.sleep(3)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Info testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Info testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to click the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Info testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Info testing: Exception has happened. ' + e.message)

        try:
            # Change tab.
            self.Browser.find_element_by_css_selector(info_subtab['attribute']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(info_subtab['log']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(info_subtab['extended_info']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(info_subtab['history']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(info_subtab['device']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector('span[id$="-close-btnWrap"]').click()
            time.sleep(3)
            self.utility.pass_test(testname=self.itemname, result='information viewing is done.')
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Info tab testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Info tab testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to click the tab. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Info tab testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Info tab testing: Exception has happened. ' + e.message)



    def smart_edit_switch_status(self):
        try:
            # S.M.A.R.T. switch status
            self.Browser.find_element_by_css_selector(' div[id^="checkbox-"][class*="x-form-cb-checked"]  ')
            time.sleep(0.5)
            self.switch_status = True
            return
        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error="SMART switch button isn/'t visible. It's stuck in the popup window.")
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error="SMART switch button is unclickable. It's stuck in the popup window.")
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg("Failed to find the 'x-form-cb-checked' in button class. :" + e2.msg)
            self.switch_status =False
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='SMART ON-OFF check: Exception has happened. ' + e.message)

    def clicktab(self, tab):
        try:
            # Click S.M.A.R.T.  and Device tab
            self.Browser.find_element_by_css_selector('div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(tab).click()
            time.sleep(0.5)
            return

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Clicktab testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Exception has happened. ' + e.message)

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == '__main__':
    testing = grack_storage_smart_device()
    testing.smart_device_edit(disk='/dev/sde')
    testing.smart_device_information(disk='/dev/sde')
    testing.logout()
