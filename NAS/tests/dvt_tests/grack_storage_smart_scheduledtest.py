'''
Created on July 2, 2018
@Author: Philip Yang

Full Title: GRACK S.M.A.R.T. scheduled test
Features including to add, edit, execute and delte HDD in testing flow.

S.M.A.R.T.: Self-monitoring, Analysis and Reporting Technology.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

smart_device_tab = {
    'device' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(1) span',
    'schedule' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(2) span',
    'settings' : 'div[id$="-center-body"] div[class*="x-panel"] div:nth-child(2) div a:nth-child(3) span'
}

schedule_job = {
    'add' : 'span[id$="-scheduledjobs-add-btnWrap"]',
    'edit' : 'span[id$="-scheduledjobs-edit-btnWrap"]',
    'run' : 'span[id$="-scheduledjobs-run-btnWrap"]',
    'delete' : 'span[id$="-scheduledjobs-delete-btnWrap"]'
}

schedule_options = {
    'function_switch' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(1) div div input',
    'device' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(2) div div[id$="triggerWrap"] div[id$="trigger-picker"] ',
    'type' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(3) div div[id$="triggerWrap"] div[id$="trigger-picker"] ',
    'hour' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(4) div div[id$="triggerWrap"] div[id$="trigger-picker"] ',
    'dayofmonth' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(5) div div[id$="triggerWrap"] div[id$="trigger-picker"] ',
    'month' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(6) div div[id$="triggerWrap"] div[id$="trigger-picker"] ',
    'dayofweek' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div div:nth-child(7) div div[id$="triggerWrap"] div[id$="trigger-picker"] ',
    'comment' : ' div[class*="x-window-container"][id^="ext-comp"] div[id$="-body"] div div div div[id$="-innerCt"] div a:nth-child(5) span'
}

self_test_plan = {
    'short' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    'long' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[2] ",
    'conveyance' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[3] ",
    'offline_immediate' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[4] "
}

self_test_behavior = {
    'start' : 'div[class*="x-window-modal"] div[class*="x-toolbar"] div div a:nth-child(1) span',
    'stop':  'div[class*="x-window-modal"] div[class*="x-toolbar"] div div a:nth-child(2) span',
    'close' :  'div[class*="x-window-modal"] div[class*="x-toolbar"] div div a:nth-child(3) span'
}

class grack_storage_smart_schedultedtest():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.itemname = 'storage smart scheduled test'
        self.progressing = False
        # goto the item
        self.clicktab(tab=smart_device_tab['schedule'])
        time.sleep(0.5)

    def schedultedtest_add(self, disk='/dev/sde', plan_choice='long', comment='create testing'):
        ''' Add schedule '''
        self.itemname = 'S.M.A.R.T. add schedule'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Schedule Add ***')

        try:
            # Click add button
            self.Browser.find_element_by_css_selector(schedule_job['add']).click()
            time.sleep(3)

            # Click switch button
            self.Browser.find_element_by_css_selector(schedule_options['function_switch']).click()
            time.sleep(10)

            # Select a disk
            self.Browser.find_element_by_css_selector(schedule_options['device']).click()
            time.sleep(10)
            self.utility.infoMsg(msg='select a disk: %s' % disk)
            self.Browser.find_element_by_xpath(" //ul//li[contains(text(), '%s')] " %disk).click() #solution-1
            #self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click() #solution-2
            time.sleep(10)

            # Select test plan
            self.Browser.find_element_by_css_selector(schedule_options['type']).click()
            time.sleep(3)
            self.utility.infoMsg(msg='select a test plan: %s' % plan_choice)
            self.Browser.find_element_by_xpath(self_test_plan[plan_choice]).click()
            time.sleep(3)

            # Comment
            txt = self.Browser.find_element_by_css_selector(' textarea[id^="textarea"]')
            txt.send_keys(comment)
            time.sleep(3)

            # Click Save
            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"][class*="x-btn-wrap-default-small"]').click()
            time.sleep(30)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Add testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Exception has happened. ' + e.message)

    def schedultedtest_edit(self, disk='/dev/sdd', plan_choice='short', comment='replace testing'):
        ''' Edit schedule '''
        self.itemname = 'S.M.A.R.T. edit schedule'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Schedule Edit ***')

        try:
            # Select target disk
            #self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % disk).click()
            self.Browser.find_element_by_xpath("//*[contains(text(), 'create testing')]").click()
            time.sleep(3)

            # Click edit button
            self.Browser.find_element_by_css_selector(schedule_job['edit']).click()
            time.sleep(10)

            # Click switch button
            self.Browser.find_element_by_css_selector(schedule_options['function_switch']).click()
            time.sleep(10)

            # Select a disk
            self.Browser.find_element_by_css_selector(schedule_options['device']).click()
            time.sleep(10)
            self.utility.infoMsg(msg='Change disk: %s' % disk)
            self.Browser.find_element_by_xpath(" //ul//li[contains(text(), '%s')] " % disk).click() #solution-1
            #self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(2)').click() #solution-2
            time.sleep(3)

            # Select test plan
            self.Browser.find_element_by_css_selector(schedule_options['type']).click()
            time.sleep(3)
            self.utility.infoMsg(msg='select a test plan: %s' % plan_choice)
            self.Browser.find_element_by_xpath(self_test_plan[plan_choice]).click()
            time.sleep(3)

            # Select execution time
            self.utility.infoMsg(msg='manage the execution time...')

            self.Browser.find_element_by_css_selector(schedule_options['hour']).click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][3]//div//ul/li[3] ").click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(schedule_options['dayofmonth']).click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][4]//div//ul/li[3] ").click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(schedule_options['month']).click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][5]//div//ul/li[3] ").click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(schedule_options['dayofweek']).click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][6]//div//ul/li[3] ").click()
            time.sleep(3)

            # Comment
            txt = self.Browser.find_element_by_css_selector(' textarea[id^="textarea"]')
            txt.clear()
            txt.send_keys(comment)
            time.sleep(3)

            # Click Save
            self.Browser.find_element_by_css_selector(' span[id$="ok-btnWrap"][class*="x-btn-wrap-default-small"]').click()
            time.sleep(30)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Edit testing: Exception has happened. ' + e.message)

    def schedultedtest_run(self, comment='replace testing'):
        ''' Run schedule
        :param comment: be used to catch the test item.
        '''

        self.itemname = 'S.M.A.R.T. run schedule'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Schedule Run ***')

        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" %comment).click()
            time.sleep(3)

            # Click edit button
            self.Browser.find_element_by_css_selector(schedule_job['run']).click()
            time.sleep(3)

            # Click start
            self.utility.infoMsg('start testing')
            self.Browser.find_element_by_css_selector(self_test_behavior['start']).click()
            time.sleep(8)

            # Check status
            self.chk_progressing()
            while self.progressing:
                time.sleep(8)
                self.chk_progressing()

            self.utility.infoMsg('close testing')
            self.Browser.find_element_by_css_selector(self_test_behavior['close']).click()
            time.sleep(0.5)

            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Run testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Run testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Run testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Run testing: Exception has happened. ' + e.message)

    def schedultedtest_delete(self, comment='replace testing'):
        ''' Delete schedule
        :param comment: be used to catch the test item.
        '''
        self.itemname = 'S.M.A.R.T. delete schedule'
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('*** Schedule Delete ***')

        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(3)

            # Click delete button
            self.Browser.find_element_by_css_selector(schedule_job['delete']).click()
            time.sleep(3)

            # Click Save
            self.Browser.find_element_by_css_selector(' div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(60)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Delete testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Delete testing: Exception has happened. ' + e.message)

    def clicktab(self, tab):
        try:
            # Click S.M.A.R.T.  and Device tab
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(tab).click()
            time.sleep(0.5)
            return

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Clicktab testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Clicktab testing: Exception has happened. ' + e.message)

    def chk_progressing(self):
        ''' Stop the wiping, if stop button is enabled '''
        try:
            # If Stop has a class of disable which is meaning the progress is done.
            self.Browser.find_element_by_css_selector(' div[class*="x-window-modal"] div[class*="x-toolbar"] div div a[class*="x-item-disable"]:nth-child(2) ').is_displayed()
            self.progressing = False
            time.sleep(2)
        except ElementNotVisibleException as e4:
            self.utility.infoMsg('Element is invisible.  ' + e4.msg)
            self.progressing = True
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Progress is ongoing, please wait a while. : ' + e2.msg)
            self.progressing = True
            pass
        except Exception as e:
            self.utility.infoMsg('Exception has happened. ' + e.message)
            self.progressing = True
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__":
    testing = grack_storage_smart_schedultedtest()
    testing.schedultedtest_add()
    testing.schedultedtest_edit(disk='/dev/sdf')
    testing.schedultedtest_run()
    testing.schedultedtest_delete()
    testing.logout()
