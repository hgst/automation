'''
Created on May 12, 2018
@Author: Philip Yang

Full Title: GRACK RAID mount and unmount.

'''
from selenium import webdriver
import time
import device_info
import grack_create_raid5
import grack_deleteRAID
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException


class raid_unmount_mount():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')

    def unmount(self, raidname='testraid', itemname='unmount testing'):
        try:
            self.testitem = itemname
            self.utility.unittest(self.testitem)
            self.utility.infoMsg('*** RAID unmount testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(5)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidname).click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector('span[id$="-unmount-btnWrap"]').click()
            time.sleep(30)
            self.utility.infoMsg('*** RAID unmount testing is done ***')
            time.sleep(0.5)
            self.utility.pass_test(testname= self.testitem)
        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname= self.testitem,
                                         error='RAID unmount testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname= self.testitem, error='Span testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname= self.testitem,
                                         error='RAID unmount testing: Exception has happened. ' + e.message)

    def mount(self, raidname='testraid', itemname='mount testing'):
        try:
            self.testitem = itemname
            self.utility.unittest(self.testitem)
            self.utility.infoMsg('*** RAID mount testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(5)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidname).click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector('span[id$="-mount-btnWrap"]').click()
            time.sleep(20)
            self.utility.infoMsg('*** RAID mount testing is done ***')
            time.sleep(0.5)
            self.utility.pass_test(testname=self.testitem)
        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname= self.testitem,
                                         error='RAID mount testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname= self.testitem, error='Span testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname= self.testitem,
                                         error='RAID mount testing: Exception has happened. ' + e.message)


    def logout(self):
        self.utility.infoMsg('Preparing to logout.')
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__" :
    #testRAID = grack_create_raid5.createRAID5()
    #testRAID.create_volume(raidname='sample', DoNotDelete=True)

    raidControl = raid_unmount_mount()
    raidControl.unmount(raidname='sample')
    raidControl.mount(raidname='sample')
    raidControl.unmount(raidname='sample',itemname='for delete')
    raidControl.utility.get_test_results()
    time.sleep(5)
    raidControl.logout()

    deltestRAID = grack_deleteRAID.deleteRAID()
    deltestRAID.delete(raidname='sample')