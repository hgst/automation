'''
    Created on May 28, 2018
    @Author: Philip.Yang

    Full Title: SMB/CIFS settings.


'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException

import time
import device_info


logLevel = {
    'None' : 'div[class*="x-boundlist"] div ul li:nth-child(1)',
    'Minimum' : 'div[class*="x-boundlist"] div ul li:nth-child(2)',
    'Normal' : 'div[class*="x-boundlist"] div ul li:nth-child(3)',
    'Full' : 'div[class*="x-boundlist"] div ul li:nth-child(4)',
    'Debug' : 'div[class*="x-boundlist"] div ul li:nth-child(5)'
}

switch_checked_status = {
    'switch' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(1)',
    'lmb' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(4)',
    'ts' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(5)',
    'ws' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(1)'
}

switchbutton = {
    'switch' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(1) div div input',
    'lmb' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(4) div div input',
    'timeSever' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(5) div div input',
    'ws' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(1) div div input'
}

switchstatus = {
    'switch' : False,
    'lmb' : False,
    'ts' : False,
    'ws' : False
}

testfield = {
    'wg' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div:nth-child(2) div div div input',
    'des' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div:nth-child(3) div div div input',
    'wss' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div:nth-child(2) div div div input'
}

class smb_cifs_config():

    def __init__(self):
        self.unittest = 'Configure SMB/CIFS'
        # weblogin
        self.utility = device_info.device_info()
        self.browser = self.utility.browser
        # register
        self.utility.unittest(self.unittest)
        # login webpage
        self.utility.web_login(tabname='connect')
        # SMB_CIFS page
        self.browser.find_element_by_css_selector(
            'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(4)').click()
        time.sleep(0.5)
        # SMB_CIFS page settings
        self.browser.find_element_by_css_selector('div[id$="-center-body"] div div div div div a:nth-child(2)').click()
        time.sleep(0.5)

    def settings(self, switch=False, workgroup="WORKGROUP", description="%h server", lmb=True, timeSever=False, winsSupport=False, winsServer="", selected_logLevel='Normal'):
        ''' configure '''

        # Detect button status

        for detector in switch_checked_status:
            try:
                self.browser.find_element_by_css_selector(switch_checked_status[detector])
                self.utility.infoMsg('%s on' % detector)
                switchstatus[detector] = True
            except InvalidSelectorException:
                self.utility.infoMsg('%s cannot be selected.' % detector)
                self.utility.fail_test(testname=self.unittest, error='switch parser has failed.')
            except NoSuchElementException:
                self.utility.infoMsg('%s is off.' % detector)


        # Fill out the form

        if (switch == switchstatus['switch']):
            pass
        else:
            self.browser.find_element_by_css_selector(switchbutton['switch']).click()
        time.sleep(0.5)

        try:
            wg = self.browser.find_element_by_css_selector(testfield['wg'])
            wg.clear()
            wg.send_keys(workgroup)
        except InvalidSelectorException:
            self.utility.infoMsg('Workgroup cannot be selected.')
            self.utility.fail_test(testname=self.unittest, error='Cannot fill in the workgroup.')
        except NoSuchElementException:
            self.utility.infoMsg('Workgroup is not found.')
        time.sleep(0.5)

        try:
            des = self.browser.find_element_by_css_selector(testfield['des'])
            des.clear()
            des.send_keys(description)
        except InvalidSelectorException:
            self.utility.infoMsg('Description cannot be selected.')
            self.utility.fail_test(testname=self.unittest, error='Cannot fill in the description.')
        except NoSuchElementException:
            self.utility.infoMsg('Description is not found.')
        time.sleep(0.5)

        if (lmb == switchstatus['lmb']):
            pass
        else:
            self.browser.find_element_by_css_selector(switchbutton['lmb']).click()
        time.sleep(0.5)
        if (timeSever == switchstatus['ts']):
            pass
        else:
            self.browser.find_element_by_css_selector(switchbutton['ts']).click()
        time.sleep(0.5)
        if (winsSupport == switchstatus['ws']):
            pass
        else:
            self.browser.find_element_by_css_selector(switchbutton['ws']).click()
        time.sleep(0.5)

        try:
            wss = self.browser.find_element_by_css_selector(testfield['wss'])
            wss.clear()
            wss.send_keys(winsServer)
        except InvalidSelectorException:
            self.utility.infoMsg('WINS server cannot be selected.')
            self.utility.fail_test(testname=self.unittest, error='Cannot fill in the WINS server.')
        except NoSuchElementException:
            self.utility.infoMsg('WINS server is not found.')
        time.sleep(0.5)

        try:
            self.browser.find_element_by_css_selector('div[id$="-settings-innerCt"] fieldset:nth-child(3) div div div div:nth-child(1) div div').click()
            time.sleep(0.5)
            self.browser.find_element_by_css_selector(logLevel[selected_logLevel]).click()
        except InvalidSelectorException:
            self.utility.infoMsg('Log level cannot be selected.')
            self.utility.fail_test(testname=self.unittest, error='Cannot choose the log level.')
        except NoSuchElementException:
            self.utility.infoMsg('Log level is not found.')
        time.sleep(0.5)

        # Save the settings.

        try:
            self.browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
        except InvalidSelectorException:
            self.utility.infoMsg('Save button is unavailable.')
            self.utility.fail_test(testname=self.unittest, error='Cannot save the settings.')
        except NoSuchElementException:
            self.utility.infoMsg('Save button is not found.')
        time.sleep(8)
        self.utility.pass_test(testname=self.unittest, result='unittest %s is passed.' % self.unittest)
        time.sleep(0.5)
        self.utility.get_test_results()
        time.sleep(0.5)
        self.utility.web_logout()


if __name__ == "__main__" :
    smb_cifs_config().settings()