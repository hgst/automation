'''
    Created on April 9, 2018
    @Author: Philip.Yang

    Full Title: Add user and delete user.
    Open browser and then create a user account and delete it.

'''
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


class create_delete_user():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.loginBehavior = True

    def _pickAddbtn(self):
        get_add_btn = False
        try:
            # Click Add button
            self.utility.infoMsg('Click Add button')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-box-target"] > div[class*="x-menu-item"]:nth-child(1) > a[class*="x-menu-item-link"]:nth-child(1)').click()
            get_add_btn = True
        except:
            self.utility.infoMsg('Add button is unclickable.')
            pass
        try:
            if get_add_btn:
                pass
            else:
                # Click Plus button
                self.utility.infoMsg('Click Plus button')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span').click()
                time.sleep(2)
                self.utility.infoMsg('Click Add button')
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-box-target"] > div[class*="x-menu-item"]:nth-child(1) > a[class*="x-menu-item-link"]:nth-child(1)').click()
            time.sleep(2)
            return
        except Exception:
            self.utility.infoMsg('Cannot pick up Plus or Add button.')
            pass

    def _pickAddwindow(self):
        try:
            self.utility.infoMsg('...Pick Add window')
            self.Browser.find_element_by_css_selector('div[class*="x-window-container"]').is_displayed()
            self.loginBehavior = False  # Pre-steps is done.
        except Exception:
            self.utility.infoMsg('Cannot pick up Add window.')
            pass

    def gotest(self, account_numbers=5, username='sample', string_plus_number=True):
        self.itemname = 'Add and Delete User'
        self.utility.unittest(self.itemname)
        self.loginBehavior = True

        adduserid = username
        adduserpwd = 'archangel'
        step1=False

        self.utility.web_login(tabname='accessControl')


        for counter in range(0, account_numbers, 1):
            if string_plus_number:
                number = str(counter)
                adduserid = username + number
            self.utility.infoMsg('username: %s' % adduserid)
            self.loginBehavior =True
            counter=0
            while self.loginBehavior:
                self._pickAddbtn()
                self._pickAddwindow()
                counter += 1
                if counter>5:
                    self.utility.fail_test(testname=self.itemname, error='Cannot open Add window correctly, Please check the device status.')
                    return


            try:
                # Add user
                self.utility.infoMsg('...Insert username')
                addusername = self.Browser.find_element_by_name('name')
                addusername.send_keys(adduserid)
                time.sleep(2)
                self.utility.infoMsg('...Insert userpassword')
                adduserpassword1 = self.Browser.find_element_by_name('password')
                adduserpassword1.send_keys(adduserpwd)
                time.sleep(2)
                self.utility.infoMsg('...Confirm userpassword')
                adduserpassword2 = self.Browser.find_element_by_name('passwordconf')
                adduserpassword2.send_keys(adduserpwd)
                time.sleep(2)
                self.utility.infoMsg('...Click OK button')
                self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
                time.sleep(20)
                self.utility.infoMsg('...Create user account success.')
                step1 = True
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname, error='Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass

        if(step1):
            try:
                # Check user account has already exist.
                self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % adduserid).click()
                self.utility.infoMsg('Found account: \'%s\' , prepare to remove.' %adduserid)
                # Remove User
                self.Browser.find_element_by_css_selector('span[id$="-delete-btnWrap"').click()
                time.sleep(8)
                self.Browser.find_element_by_id('button-1006-btnInnerEl').click()
                time.sleep(15)

                self.utility.infoMsg('Remove user account success.')
                self.utility.pass_test(testname=self.itemname)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname, error='Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass
        else:
            self.utility.fail_test(testname=self.itemname, error="Failed to create use account so we've to interrupt the testing.")

        self._logout()
        self.utility.infoMsg("*** Finish Testing ***")

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__":
    mytest = create_delete_user()
    mytest.gotest(username="Fukuyama", account_numbers=10)
