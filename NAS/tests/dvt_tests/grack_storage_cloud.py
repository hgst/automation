'''
Created on September 27th, 2018
@Author: Philip Yang

Full Title: GRACK Storage: Cloud

Workflow: Edit a S3 account, create and mount the item. Unmount, mount and delete the item.

Note: This testing do not suport activescale.
'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
from selenium.common.exceptions import ElementNotVisibleException
import time
import device_info

cloud_tab ={
    'create' : 'span[id$="-create-btnWrap"]',
    'settings' : 'span[id$="-settings-btnWrap"]',
    'mount' : 'span[id$="-mount-btnWrap"]',
    'unmount' : 'span[id$="-unmount-btnWrap"]',
    'delete' : 'span[id$="-delete-btnWrap"]',
}

class grack_storage_cloud():
    def __init__(self):
        self.itemname = 'Storage Cloud'
        self.ad_switch = False
        self.check_insert = False
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.login = False
        self.status = False
        self.accesskey = "AKIAJOJA2UFL6PVA7LTA"
        self.secretaccesskey = "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"
        self.bucketname = "kamenrider"
        self.as_accesskey = "AKIAJMADPJKMEUS3BGIA"
        self.as_secretaccesskey = "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"
        self.as_bucketname = "grack12test"
        self.as_serveraddress = "s3-us-west-1.amazonaws.com"

    def _login(self):
        self.utility.web_login(tabname='storage')
        # Click tab of Cloud
        try:
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(5) span').click()
            time.sleep(5)
            self.login = True
            return
        except Exception:
            self.utility.infoMsg('Failed to login the page of Cloud.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(15)
        self.utility.web_logout()

    def _checkmount(self):
        for count in range(1,4,1):
            try:
                self.Browser.find_element_by_xpath("//body//div[contains(@class,'x-message-box')][%d]//div[contains(@class,'x-toolbar')]//div//div//a[1]//span" %count).click()
                self.status = True
                return
            except:
                self.utility.infoMsg('Failed to catch the error message, try again...')
                self.status = False
                pass


    def _settings_s3(self, accesskey="", secrectkey=""):
        self.itemname = 'Cloud settings os S3 AWS'
        self.utility.unittest(test_item_name=self.itemname)

        if accesskey == "":
            accesskey = self.accesskey
        if secrectkey == "":
            secrectkey = self.secretaccesskey

        try:
            self.utility.infoMsg('Click settings')
            self.Browser.find_element_by_css_selector(cloud_tab['settings']).click()
            time.sleep(3)

            self.utility.infoMsg('Pick S3 AWS')
            self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ").click()
            time.sleep(3)

            self.utility.infoMsg('Replace the Access Key')
            _ak = self.Browser.find_element_by_css_selector('input[name*="key"]')
            _ak.clear()
            _ak.send_keys(accesskey)
            time.sleep(3)

            self.utility.infoMsg('Replace the Secret Access Key')
            _sak = self.Browser.find_element_by_css_selector('input[name*="secret"]')
            _sak.clear()
            _sak.send_keys(secrectkey)
            time.sleep(3)

            self.utility.infoMsg('Save')
            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Cloud S3 settings tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud S3 settings tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud S3 settings tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Cloud settings of S3: Exception just happened.' + e4.message)
            pass

    def _settings_ac(self, accesskey="", secrectkey=""):
        self.itemname = 'Cloud settings of ActiveScale'
        self.utility.unittest(test_item_name=self.itemname)

        if accesskey == "":
            accesskey = self.as_accesskey
        if secrectkey == "":
            secrectkey = self.as_secretaccesskey

        try:
            self.utility.infoMsg('Click settings')
            self.Browser.find_element_by_css_selector(cloud_tab['settings']).click()
            time.sleep(3)

            # self.utility.infoMsg('Pick ActiveScale')
            # self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            # time.sleep(3)
            # self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            # time.sleep(3)

            self.utility.infoMsg('Replace the Access Key')
            _ak = self.Browser.find_element_by_css_selector('input[name*="key"]')
            _ak.clear()
            _ak.send_keys(accesskey)
            time.sleep(3)

            self.utility.infoMsg('Replace the Secret Access Key')
            _sak = self.Browser.find_element_by_css_selector('input[name*="secret"]')
            _sak.clear()
            _sak.send_keys(secrectkey)
            time.sleep(3)

            self.utility.infoMsg('Save')
            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud ActiveScale settings tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud ActiveScale settings tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud ActiveScale settings tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud settings of ActiveScale: Exception just happened.' + e4.message)
            pass

    def _create_s3(self, bucketname=""):

        self.itemname = 'Cloud Create S3 AWS'
        self.utility.unittest(test_item_name=self.itemname)

        try:
            self.utility.infoMsg('Click Create')
            self.Browser.find_element_by_css_selector(cloud_tab['create']).click()
            time.sleep(3)

            self.utility.infoMsg('Pick S3 AWS')
            self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ").click()
            time.sleep(3)

            if bucketname == "":
                bucketname = self.bucketname

            self.utility.infoMsg('Replace bucket name')
            _ak = self.Browser.find_element_by_css_selector('input[name*="bucket"]')
            _ak.clear()
            _ak.send_keys(bucketname)
            time.sleep(3)

            self.utility.infoMsg('Save')
            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            time.sleep(30)

            if self.status:
                self.utility.fail_test(testname=self.itemname)
            else:
                self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Create S3 tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud Create S3 tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud Create S3 tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Create S3: Exception just happened.' + e4.message)
            pass

    def _create_as(self, bucketname="", hostname=""):

        self.itemname = 'Cloud Create'
        self.utility.unittest(test_item_name=self.itemname)

        try:
            self.utility.infoMsg('Click Create')
            self.Browser.find_element_by_css_selector(cloud_tab['create']).click()
            time.sleep(3)

            # self.utility.infoMsg('Pick ActiveScale')
            # self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            # time.sleep(3)
            # self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            # time.sleep(3)

            if bucketname == "":
                bucketname = self.as_bucketname

            if hostname == "":
                hostname = self.as_serveraddress

            self.utility.infoMsg('Replace bucket name')
            _ak = self.Browser.find_element_by_css_selector('input[name*="bucket"]')
            _ak.clear()
            _ak.send_keys(bucketname)
            time.sleep(3)

            self.utility.infoMsg('Replace server address')
            _ak = self.Browser.find_element_by_css_selector('input[name*="hostname"]')
            _ak.clear()
            _ak.send_keys(hostname)
            time.sleep(3)

            self.utility.infoMsg('Save')
            self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            time.sleep(30)

            self._checkmount()

            if self.status:
                self.utility.fail_test(testname=self.itemname)
            else:
                self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Create S3 tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud Create S3 tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud Create S3 tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Create S3: Exception just happened.' + e4.message)
            pass

    def _mount(self, item_number='1'):
        self.itemname = 'Cloud Mount'
        self.utility.unittest(test_item_name=self.itemname)

        try:

            _pickitem = int(item_number)
            self.utility.infoMsg('Pick item: %d' %_pickitem)
            self.Browser.find_element_by_css_selector('div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(%d)' % _pickitem).click()
            time.sleep(3)

            self.utility.infoMsg('Click Mount')
            self.Browser.find_element_by_css_selector(cloud_tab['mount']).click()
            time.sleep(30)

            self._checkmount()
            '''if status is true which means we catch the error message.'''
            if self.status:
                return
            else:
                self.utility.pass_test(testname=self.itemname)
                return
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Mount tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud Mount tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud Mount tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Mount: Exception just happened.' + e4.message)
            pass

    def _unmount(self, item_number='1'):
        self.itemname = 'Cloud Unmount'
        self.utility.unittest(test_item_name=self.itemname)

        try:

            _pickitem = int(item_number)
            self.utility.infoMsg('Pick item: %d' % _pickitem)
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(%d)' % _pickitem).click()
            time.sleep(3)

            self.utility.infoMsg('Click Unmount')
            self.Browser.find_element_by_css_selector(cloud_tab['unmount']).click()
            time.sleep(30)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Unmount tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud Unmount tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud Unmount tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Unmount: Exception just happened.' + e4.message)
            pass

    def _delete(self, item_number='1'):
        self.itemname = 'Cloud Delete'
        self.utility.unittest(test_item_name=self.itemname)

        try:

            _pickitem = int(item_number)
            self.utility.infoMsg('Pick item: %d' % _pickitem)
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(%d)' % _pickitem).click()
            time.sleep(3)

            self.utility.infoMsg('Click Delete')
            self.Browser.find_element_by_css_selector(cloud_tab['delete']).click()
            time.sleep(3)

            self.utility.infoMsg('Accept')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(3)

            self.utility.infoMsg('Confirm')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(3) span').click()
            time.sleep(30)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Delete tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Cloud Delete tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Cloud Delete tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Cloud Delete: Exception just happened.' + e4.message)
            pass

    def cloud(self):
        self._login()
        if self.login:
            self._settings_ac()
            self._create_as()
            self._settings_s3()
            self._create_s3()
            self._unmount()
            self._mount()
            self._delete()

if __name__ == "__main__":
    testing = grack_storage_cloud()
    testing.cloud()
    testing._logout()
