'''
Created on Auguest 8, 2018
@Author: Philip Yang

Full Title: GRACK Connect Services

Progress report of Total, SSH, SMB/CIFS and FTP.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


services_Tab ={
    'overview': 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span',
    'ssh': 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span',
    'smb_cifs': 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(3) span',
    'ftp': 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(4) span'
}

refresh_btn = {
    'overview' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id^="ext-comp"]:nth-child(1) div[class*="x-toolbar"] div div[class*="x-box-target"] a:nth-child(7) span',
    'ssh' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id^="ext-comp"]:nth-child(2) div[class*="x-toolbar"] div div[class*="x-box-target"] a:nth-child(1) span',
    'smb_cifs' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id^="ext-comp"]:nth-child(3) div[class*="x-toolbar"] div div[class*="x-box-target"] a:nth-child(1) span',
    'ftp' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[id^="ext-comp"]:nth-child(4) div[class*="x-toolbar"] div div[class*="x-box-target"] a:nth-child(1) span',
}


class checkServices():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'connect Services'
        self.common_switch_status = False
        self.login = False
        self.progress = False
        self._gotoServices()  # make sure login behavior

    def _gotoServices(self):
        # Click Services tab button
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(2)').click()
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Services button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Services Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname, error='Services Button is invisibled.')
            pass

    def serviceCheck(self):
        if self.login:
            time.sleep(2)
            self._refresh('overview')
            self._refresh('ssh')
            self._refresh('smb_cifs')
            self._refresh('ftp')
        self._logout()

    def _refresh(self, button):
        self.itemname = button
        self.utility.unittest(self.itemname)
        try:
            self.Browser.find_element_by_css_selector(services_Tab[button]).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(refresh_btn[button]).click()
            time.sleep(2)
            self.utility.pass_test(testname=self.itemname)
            return
        except NoSuchElementException as e1:
            self.utility.errorMsg(e1.msg)
            self.utility.fail_test(testname=self.itemname, error=e1.msg)
        except ElementNotVisibleException as e2:
            self.utility.errorMsg(e2.msg)
            self.utility.fail_test(testname=self.itemname, error=e2.msg)
        except Exception as e:
            self.utility.errorMsg(e.message)
            self.utility.fail_test(testname=self.itemname, error=e.message)

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()


checkServices().serviceCheck()