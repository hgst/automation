'''
    Created on March 14th, 2018
    @Author: Philip.Yang

    Full Title: Import Group and delete it.
    Open browser and import group.

'''

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time

import device_info


accountControl = {
    'user' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(1) span',
    'group' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span',
    'activedirectory' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span',
    'directoryservice' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(4) span'
}

class importUser():

    def __init__(self):
        self.testname = 'Import Group'

    def gotest(self):
        commonFeatures = device_info.device_info()
        commonFeatures.unittest(self.testname)
        commonFeatures.web_login(tabname='accessControl')


        try:
            commonFeatures.browser.find_element_by_css_selector(accountControl['group']).click()
            time.sleep(0.5)
            # Click Plus button
            commonFeatures.browser.find_element_by_css_selector('span[id$="-add-btnEl"]').click()
            time.sleep(2)
            # Click Import in the menu
            commonFeatures.browser.find_element_by_css_selector(
                'div[class*="x-box-target"] > div[class*="x-menu-item"]:nth-child(2) > a[class*="x-menu-item-link"]:nth-child(1)').click()
            time.sleep(2)

            # Insert dialogue.
            dialogue  = commonFeatures.browser.find_element_by_css_selector('textarea[id$="-inputEl"]')
            dialogue.send_keys("\n zakuwarrior;1000;ZGMF")
            commonFeatures.browser.find_element_by_css_selector('a[id$="-ok"').click()
            time.sleep(15)
            commonFeatures.infoMsg('Import Group member success.')
        except NameError as e2:
            commonFeatures.errorMsg('Error, cannot find the element: ' + e2)
            commonFeatures.fail_test(self.testname)
            pass
        except NoSuchElementException:
            commonFeatures.errorMsg('Error, cannot find the element in the group list.')
            commonFeatures.fail_test(self.testname)
            pass


        # Remove User Account
        try:
            commonFeatures.browser.find_element_by_xpath("//*[contains(text(), 'zakuwarrior')]").click()
            commonFeatures.infoMsg("Found account: \' zakuwarrior \', prepare to remove.")
            time.sleep(2)
            # Remove User
            commonFeatures.browser.find_element_by_css_selector('span[id$="-delete-btnWrap"').click()
            time.sleep(8)
            commonFeatures.browser.find_element_by_id('button-1006-btnInnerEl').click()
            time.sleep(15)
            commonFeatures.infoMsg('Remove user account success.')
            commonFeatures.pass_test(self.testname)

        except NoSuchElementException as e:
            commonFeatures.warnMsg('Remove user account fail')
            time.sleep(5)
            commonFeatures.warn_test(self.testname, e.msg)
            pass
        except:
            commonFeatures.warnMsg('Remove user account fail')
            time.sleep(5)
            commonFeatures.warn_test(self.testname, 'There has some problem when we remove user account.')
            pass
        commonFeatures.web_logout()
        commonFeatures.get_test_results()
        commonFeatures.infoMsg("*** Finish Testing ***")

importUser = importUser()
importUser.gotest()

