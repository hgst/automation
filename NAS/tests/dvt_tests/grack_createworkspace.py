'''
Created on April 10, 2018
@Author: Andrew.Tsai

Full Title: GRACK Create Workspaces.

'''
from selenium import webdriver
import time
import device_info
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException

test_item_name = 'Create Workspaces'

# For single user
user_permission = {
    'read_write' : 'div[id^="privilegesgrid"] div[id^="privilegesgrid"]:nth-child(3) div div[class*="x-grid-item-container"] table tbody td:nth-child(3)',
    'read_only' : 'div[id^="privilegesgrid"] div[id^="privilegesgrid"]:nth-child(3) div div[class*="x-grid-item-container"] table tbody td:nth-child(4)',
    'no_access' : 'div[id^="privilegesgrid"] div[id^="privilegesgrid"]:nth-child(3) div div[class*="x-grid-item-container"] table tbody td:nth-child(5)'
}

# For owner, group and others
permission = {
    'no' : ' body[id*=workspace-page] div[class*="x-boundlist"] div ul li:nth-child(1)',
    'read_execute' : ' body[id*=workspace-page] div[class*="x-boundlist"] div ul li:nth-child(2)',
    'read_write_execute' : ' body[id*=workspace-page] div[class*="x-boundlist"] div ul li:nth-child(3)'

}


class createworkspace():


    def unit_testname(self, testname):
        print("GRACK UNIT TEST: " + testname)


    def __init__(self):
        self.commonFeature = device_info.device_info()

        self.commonFeature.web_login('workspaces')
        self.browser = self.commonFeature.browser
        time.sleep(30)


    def create(self, workSpace='test-workspace', raid=1):
        self.commonFeature.unittest(test_item_name='Create Workspace')
        try:
            # Click Add button
            self.commonFeature.infoMsg('Click add button')
            self.browser.find_element_by_css_selector('span[id$="-add-btnWrap"]').click()
            time.sleep(5)
            self.commonFeature.infoMsg('Insert workspace name')
            workspacesname = self.browser.find_element_by_css_selector('input[name="name"]')
            workspacesname.send_keys(workSpace)
            time.sleep(5)
            # RAID Select
            self.commonFeature.infoMsg('Click add RAID button')
            self.browser.find_element_by_css_selector('input[name="devicefile"]').click()
            time.sleep(30)
            self.commonFeature.infoMsg('Pick up a RAID')
            # self.browser.find_element_by_css_selector('div ul[class*="x-list-plain"]').click()
            self.browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[%d]" % raid).click()
            time.sleep(30)
            self.commonFeature.infoMsg('Insert comment')
            self.browser.find_element_by_css_selector('input[type="button"]').click()
            time.sleep(5)
            comment = self.browser.find_element_by_css_selector('textarea[name="comment"]')
            comment.send_keys('This is test.')

            self.commonFeature.infoMsg('Click OK button')
            self.browser.find_element_by_css_selector('span[id*="ok"]').click()
            time.sleep(60)
            self.commonFeature.pass_test(testname='Create Workspace')
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='CreateWorkspace', error=e2.msg)
            pass
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='CreateWorkspace', error='Stuck in the popup window.')
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='CreateWorkspace', error= "Exception has happened. " +e.message)

        finally:
            time.sleep(30)

            self.commonFeature.infoMsg("*** Create Worksapce has finished ***")


    def edit(self,workSpace='test-workspace', comment = 'replace comment'):
        try:
            self.commonFeature.unittest(test_item_name='Edit Workspace')
            #Click target workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(0.5)

            #Click edit button
            self.browser.find_element_by_css_selector('span[id$="-edit-btnWrap"] ').click()
            time.sleep(15)

            #Edit comment
            commentBox = self.browser.find_element_by_css_selector('textarea[name="comment"]')
            commentBox.clear()
            commentBox.send_keys(comment)
            time.sleep(0.5)

            self.commonFeature.infoMsg(msg='Saving...')
            self.browser.find_element_by_css_selector('span[id*="ok"]').click()
            time.sleep(20)

            self.commonFeature.infoMsg(msg='*** Edit Workspace Testing has finished ***')
            time.sleep(0.5)
            self.commonFeature.pass_test(testname='Edit Workspace')
            time.sleep(0.5)
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Edit Workspace', error='Edit Workspace: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Edit Workspace', error='Edit Workspace: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Edit Workspace', error='Edit Workspace Exception has happened. ' + e.message)


    def access_permission(self,workSpace='test-workspace', Recurise=True, permissionTest= 'owner', permission_status='read_execute'):
        ''' access permission testing
        :param workspace should insert an exixt item
        :param permissionTest only accept "owner", "group" and "others"
        :param permission_status is releated to the permissionTest and accept "no", "read_execute" and "read_write_execute" only.
        '''
        try:
            self.commonFeature.unittest(test_item_name='Access Permission')
            # Click target workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(0.5)

            # Click permission button
            self.browser.find_element_by_css_selector('span[id$="-acl-btnWrap"] ').click()
            time.sleep(0.5)

            # Change user permission
            self.browser.find_element_by_css_selector(user_permission['no_access']).click()
            time.sleep(0.5)
            self.browser.find_element_by_css_selector(user_permission['read_only']).click()
            time.sleep(0.5)
            self.browser.find_element_by_css_selector(user_permission['read_write']).click()
            time.sleep(0.5)
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Access Permission',
                                         error='Access Permission: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('user account is not exist so that we cannot mantain the single account permission:' + e2.msg)
            self.commonFeature.warn_test(testname='Access Permission', warning='Access Permission: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Access Permission',
                                         error='Access Permission Exception has happened. ' + e.message)



        try:
            # Click Advanced button
            self.browser.find_element_by_css_selector('div[id$="placeholder-innerCt"]   div div:nth-child(2)  ').click()
            time.sleep(3)

            # Owner/Group/Others Permission
            if 1:
                self.commonFeature.infoMsg(msg='Prparing to edit %s Permission.' % permissionTest)
                if(permissionTest == 'others'):
                    self.othersPermission(change_permission = permission_status)
                elif(permissionTest == 'group'):
                    self.groupPermission(change_permission = permission_status)
                else:
                    self.ownerPermission(change_permission = permission_status)

            # Recursice Permission

            _recursive = True
            try:
                self.browser.find_element_by_css_selector(
                    ' div[id^="ext-comp"][class*="x-border-layout-ct"] div[class*="x-panel"]:nth-child(2) div[id^="ext-comp"] div div[class*="x-form-cb-checked"]:nth-child(5)')
                time.sleep(3)
            except Exception:
                _recursive = False
                time.sleep(0.5)
                pass

            if(Recurise == _recursive):
                pass
            else:
                self.commonFeature.infoMsg(msg='Prparing to change Recursive Permission.' )
                time.sleep(0.5)
                self.browser.find_element_by_css_selector(
                    ' div[id^="ext-comp"][class*="x-border-layout-ct"] div[class*="x-panel"]:nth-child(2) div[id^="ext-comp"] div div:nth-child(5) div div input').click()
                time.sleep(3)

            # Close
            self.commonFeature.infoMsg(msg='Closing...')
            #self.browser.find_element_by_css_selector('div[id$="header-targetEl"] div[id^="tool-"] ').click()
            self.browser.find_element_by_css_selector('div[class*="x-window-closable"][class*="x-window-default-resizable"] div div div div[class*="x-tool"]').click()
            time.sleep(15)

            # Accept
            self.commonFeature.infoMsg(msg='Saving...')
            self.browser.find_element_by_css_selector('div[id^="messagebox"] div a:nth-child(2) span').click()
            time.sleep(30)

            self.commonFeature.infoMsg(msg='*** Access Permission Testing has finished ***')
            self.commonFeature.pass_test(testname='Access Permission')
            time.sleep(5)
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='Access Permission: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='Access Permission: ' +e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Access Permission', error='Access Permission Exception has happened. ' +e.message)

    def ownerPermission(self, change_permission):
        try:
            # Advanced Options: change Owner permission 'div[id^="ext-comp"][class*="x-border-layout-ct"] div[class*="x-panel"]:nth-child(2)'
            self.browser.find_element_by_css_selector(
                ' div[id$="-body"][class*="x-border-layout-ct"] div[class*="x-panel"]:nth-child(2) div[id^="ext-comp"] div div div:nth-child(1) div div div div div:nth-child(2) ').click()
            time.sleep(5)
            self.browser.find_element_by_css_selector(
                permission[change_permission]).click()
            time.sleep(5)
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Owner permission item is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='OwnerPermission: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Owner permission item is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='OwnerPermission: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Access Permission', error='OwnerPermission Exception has happened. ' + e.message)
            pass

    def groupPermission(self, change_permission):
        try:
            # Advanced Options: change group permission
            self.browser.find_element_by_css_selector(
                ' div[id$="-body"][class*="x-border-layout-ct"] div[class*="x-panel"]:nth-child(2) div[id^="ext-comp"] div div[class*="x-container"]:nth-child(2) div[id^="compositefield"] div[id^="compositefield"] div div div[id^="unixfilepermcombo"] ').click()
            time.sleep(5)
            self.browser.find_element_by_css_selector(
                permission[change_permission]).click()
            time.sleep(5)
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Group permission item is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='GroupPermission: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Group permission item is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='GroupPermission: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Access Permission', error='GroupPermission Exception has happened. ' + e.message)
            pass

    def othersPermission(self, change_permission):
        try:
            # Advanced Options: change others permission
            self.browser.find_element_by_css_selector(
                ' div[id$="-body"][class*="x-border-layout-ct"] div[class*="x-panel"]:nth-child(2) div[id^="ext-comp"] div div:nth-child(3)').click()
            time.sleep(5)
            self.browser.find_element_by_css_selector(permission[change_permission]).click()
            time.sleep(5)
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Others permission item is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='OthersPermission: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Others permission item is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Access Permission', error='OthersPermission: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Access Permission', error='OthersPermission Exception has happened. ' + e.message)
            pass

    def makeSnapshot(self, workSpace='test-workspace', destination_path='workspace_backeup', readonlybtn=False):
        try:
            self.commonFeature.unittest(test_item_name='Make Snapshot')
            # Click target workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(5)

            #Click makesnapshot
            self.browser.find_element_by_css_selector('span[id$="-snapshot-btnWrap"] ').click()
            time.sleep(5)

            # Catch pop-up windows
            #txt = self.browser.find_element_by_css_selector(' div[class*="x-window-container"] ').text

            # Edit Snapshot's path
            pathBox = self.browser.find_element_by_css_selector('div[class*="x-window-container"] div[id^="ext-comp"] div div div div div:nth-child(4) div div div input[name="destinationPath"]')
            pathBox.clear()
            pathBox.send_keys(destination_path)
            time.sleep(5)

            # Detecting read-only button status, _readOnly defaut value is True
            _readOnly = True
            _readOnly = self.detectReadOnly()
            if(_readOnly == readonlybtn):
                pass
            else:
                self.browser.find_element_by_css_selector('div[class*="x-window-container"] div[id^="ext-comp"] div div div div div:nth-child(5) div div input').click()
                time.sleep(3)
            time.sleep(5)

            self.browser.find_element_by_css_selector(' span[id$="-ok-btnWrap"]').click()
            self.commonFeature.infoMsg(msg='Saving...')
            time.sleep(30)
            self.commonFeature.pass_test(testname='Make Snapshot')
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Make Snapshot',
                                         error='Make Snapshot: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Make Snapshot', error='Make Snapshot: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Make Snapshot', error='Make Snapshot: Exception has happened. ' + e.message)

    def quota(self, workSpace='test-workspace', quotaSwitch=True):
        try:
            self.commonFeature.unittest(test_item_name='Quota Control')
            # Click target workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(5)

            # Click permission button
            self.browser.find_element_by_css_selector('span[id$="-quota-btnWrap"] ').click()
            time.sleep(5)

            # Detect quota switch status, _quota defaut value is False
            self.commonFeature.infoMsg(msg='Detecting quota switch status...')
            _quota = False
            _quota = self.detectQuotaSwitch()
            if (_quota == quotaSwitch):
                pass
            else:
                self.browser.find_element_by_css_selector(
                    'div[class*="grack-panel"] div[id^="ext-comp"] div div fieldset div[id^="fieldset"] div div div div div input').click()
                time.sleep(5)

            # Save
            #self.browser.find_element_by_css_selector(' div[class*="grack-panel"] div[class*="x-toolbar"] div div a:nth-child(1) span ').click()
            self.browser.find_element_by_css_selector(' span[id$="-ok-btnWrap"]').click()
            self.commonFeature.infoMsg(msg='Saving...')
            time.sleep(8)

            # Click quotaInfo Tab button
            self.browser.find_element_by_css_selector(
                'div[class*="x-window-body-closable"][id^="ext-comp"] div div[class*="x-tab-bar"] div div[id^="tabbar"] div a:nth-child(2) span').click()
            self.commonFeature.infoMsg(msg='Changing page...')
            time.sleep(5)

            # Click target workspace
            self.browser.find_element_by_xpath(
                "//div[contains(@class, 'x-window-container')]//*[contains(text(), '%s')]" % workSpace).click()
            # txt = self.browser.find_element_by_css_selector('div[class="x-window-container"] div[id^="ext-comp"] div div[id^="ext-comp"] div:nth-child(2) div[id^="ext-comp"] ' )
            self.commonFeature.infoMsg(msg='Select workspace...')
            time.sleep(5)

            # Click Edit button
            self.browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[id^="ext-comp"] div div[id^="ext-comp"] div:nth-child(2) div[class*="x-toolbar"] div div[id^="toolbar"] a:nth-child(2) span[id$="-edit-btnWrap"]').click()
            self.commonFeature.infoMsg(msg='Edit limit value...')
            time.sleep(3)

            # Replace limit size
            limit = self.browser.find_element_by_css_selector('input[name*="totalLimit"]')
            limit.clear()
            limit.send_keys('1')
            time.sleep(3)

            # Save change
            #self.browser.find_element_by_xpath("  //div[contains(@class, 'x-window-container')][2]//*[contains(text(), 'Save')]").click()
            self.commonFeature.infoMsg(msg='Saving...')
            self.browser.find_element_by_xpath(
                "  //div[contains(@class, 'x-window-container')][2]//*[contains(@class, 'x-btn-wrap-default-small')][1]").click()
            time.sleep(3)

            self.commonFeature.infoMsg(msg='Closing...')
            self.commonFeature.infoMsg(msg='Editting limit...')
            self.browser.find_element_by_css_selector('span[id$="-close-btnWrap"]').click()
            time.sleep(30)
            self.commonFeature.pass_test(testname='Quota Control')
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Quota Control',
                                         error='Quota Control: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Quota Control', error='Quota Control: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Quota Control', error='Quota Control Exception has happened. ' + e.message)

    def mountWorkSpace(self, workSpace='workspace_backeup'):

        try:
            self.commonFeature.unittest(test_item_name='Mount Workspace')
            time.sleep(10)
            # Click target workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(5)

            #Click Mount button
            self.browser.find_element_by_css_selector('span[id$="-mount-btnWrap"] ').click()
            time.sleep(60)
            self.commonFeature.pass_test(testname='Mount Workspace')
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Mount Workspace',
                                         error='Mount Workspace: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Mount Workspace', error='Make Snapshot: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Mount Workspace',
                                         error='Mount Workspace: Exception has happened. ' + e.message)

    def unmountWorkSpace(self, workSpace='workspace_backeup'):

        try:
            self.commonFeature.unittest(test_item_name='Unmount Workspace')
            time.sleep(10)
            # Click target workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(5)

            # Click Mount button
            self.browser.find_element_by_css_selector('span[id$="-unmount-btnWrap"] ').click()
            time.sleep(60)
            self.commonFeature.pass_test(testname='Unmount Workspace')
        except InvalidSelectorException as e1:
            self.commonFeature.infoMsg('Element is unselectable: ' + e1.msg)
            self.commonFeature.fail_test(testname='Unmount Workspace',
                                         error='Unmount Workspace: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            self.commonFeature.fail_test(testname='Unmount Workspace', error='Make Snapshot: ' + e2.msg)
            pass
        except Exception as e:
            self.commonFeature.fail_test(testname='Unmount Workspace',
                                         error='Unmount Workspace: Exception has happened. ' + e.message)

    def detectReadOnly(self):
        try:
            self.browser.find_element_by_css_selector('div[class*="x-window-container"] div[id^="ext-comp"] div div div div div[class*="x-form-cb-checked"]:nth-child(5)')
            return True
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist:' + e2.msg)
            pass
            return False
        except Exception as e:
            self.commonFeature.fail_test(testname='Make Snapshot', error='Detecting ReadOnly: Exception has happened. ' + e.message)
            pass

    def detectQuotaSwitch(self):
        try:
            self.browser.find_element_by_css_selector(
                'div[class*="grack-panel"] div[id^="ext-comp"] div div fieldset div[id^="fieldset"] div div div[class*="x-form-cb-checked"]')
            return True
        except NoSuchElementException as e2:
            self.commonFeature.infoMsg('Elemnet is not exist, meaning Quota switch off.')
            pass
            return False
        except Exception as e:
            self.commonFeature.fail_test(testname='Detecting QuotaSwitch',
                                         error='Detecting QuotaSwitch: Exception has happened. ' + e.message)
            pass

    def closeBrowser(self):
        self.commonFeature.get_test_results()
        self.commonFeature.web_logout()


if __name__ == "__main__" :

    workspace = createworkspace()
    workspace.create()
    workspace.edit(comment='hahahaha string')
    workspace.access_permission(permissionTest='others',Recurise=False)
    workspace.quota()
    workspace.makeSnapshot()
    workspace.mountWorkSpace()
    workspace.unmountWorkSpace()
    workspace.closeBrowser()

    workspace2 = createworkspace()
    workspace2.create(workSpace='sample3',raid=2)
    workspace2.closeBrowser()