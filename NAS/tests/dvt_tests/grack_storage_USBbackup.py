'''
Created on September 25th, 2018
@Author: Philip Yang

Full Title: GRACK Storage USB Backup

Workflow: Mount usb and then create clone job and run process.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
from selenium.common.exceptions import ElementNotVisibleException
import time
import device_info

usbbackupbtn = {
    'add' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div a:nth-child(1) span ',
    'edit' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div a:nth-child(2) span ',
    'run' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div a:nth-child(3) span ',
    'detail' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div a:nth-child(4) span ',
    'backupdevice' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div a:nth-child(5) span ',
    'delete' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-toolbar"] div a:nth-child(6) span ',
}


addJobitem = {
    'pickmode' : 'div[id^="addUSBBackupJob-body"] div div div div div:nth-child(2) div div div[id$="trigger-picker"]',
    'fromShare' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ",
    'fromExternal' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ",
    'pickworkspace' : 'div[id^="addUSBBackupJob"] div div div div div:nth-child(3) div div div[id$="trigger-picker"]',
    'pickUSBdevice' : 'div[id^="addUSBBackupJob"] div div div div div:nth-child(5) div div div[id$="trigger-picker"]',
    'okbtn' : 'div[id^="addUSBBackupJob"][class*="x-window-container"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span',
    'resetbtn' : 'div[id^="addUSBBackupJob"][class*="x-window-container"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span',
    'cancelbtn' : 'div[id^="addUSBBackupJob"][class*="x-window-container"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(3) span',
}

editJobitem = {
    'okbtn' : 'span[id*="editUSBBackupJob-ok-btnWrap"]',
    'resetbtn' : 'span[id*="editUSBBackupJob-reset-btnWrap"]',
    'cancelbtn' : 'span[id*="editUSBBackupJob-cancel-btnWrap"]',
}

class grack_storage_USB_backup():

    def __init__(self):
        self.utility = device_info.device_info()
        self.itemname = 'storage smart device'
        self.Browser = self.utility.browser
        self.login = False
        self.switch_status = True
        self.utility.web_login(tabname='storage')



    def _login(self):
        # Click tab of USB backup
        try:
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(4) span').click()
            time.sleep(5)
            self.login = True
            return
        except Exception:
            self.utility.infoMsg('Failed to login the page of USB backup.')
            pass

    def _addJob(self, BackupMode='fromShare', workspace_member=1, USBdevice=1, comment='sampel'):
        try:
            self.itemname = 'Create Job'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Pick Add button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['add']).click()
            time.sleep(3)
            self.utility.infoMsg('Click mode list button')
            self.Browser.find_element_by_css_selector(addJobitem['pickmode']).click()
            time.sleep(3)
            self.utility.infoMsg('Pick mode: %s' % BackupMode)
            self.Browser.find_element_by_xpath(addJobitem[BackupMode]).click()
            time.sleep(5)

            workspace = int(workspace_member)
            self.utility.infoMsg('Pick workspace number: %s' % workspace_member)
            self.Browser.find_element_by_css_selector(addJobitem['pickworkspace']).click()
            time.sleep(10)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][2]//div//ul/li[%d] "% workspace).click()
            time.sleep(5)

            USB = int(USBdevice)
            self.Browser.find_element_by_css_selector(addJobitem['pickUSBdevice']).click()
            time.sleep(10)
            self.Browser.find_element_by_xpath(
                "//div[contains(@class, 'x-boundlist')][3]//div//ul/li[%d] " % USB).click()
            time.sleep(5)

            _note = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            _note.clear()
            _note.send_keys(comment)
            time.sleep(5)

            self.utility.infoMsg('Click OK button')
            self.Browser.find_element_by_css_selector(addJobitem['okbtn']).click()
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1 :
            self.utility.fail_test(testname=self.itemname, error='Create Job tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Create Job tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Create Job tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Create Job: Exception just happened.' + e4.message)
            pass

    def _editJob(self, comment='sampel', newcomment='sample'):
        try:
            self.itemname = 'Edit Job'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Pick item: %s' % comment)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(5)

            self.utility.infoMsg('Click Edit button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['edit']).click()
            time.sleep(5)

            self.utility.infoMsg('replace comment')
            _note = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            _note.clear()
            _note.send_keys(newcomment)
            time.sleep(5)

            self.utility.infoMsg('Click OK button')
            self.Browser.find_element_by_css_selector(editJobitem['okbtn']).click()
            time.sleep(10)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Create Job tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Create Job tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Create Job tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Create Job: Exception just happened.' + e4.message)
            pass

    def _runJob(self, comment='sample', auto_interrupt=True):
        try:
            self.itemname = 'Run Job'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Pick item: %s' % comment)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(5)

            self.utility.infoMsg('Click Run button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['run']).click()
            time.sleep(5)

            self.utility.infoMsg('Click Start!')
            self.Browser.find_element_by_css_selector('span[id$="start-btnWrap"]').click()
            time.sleep(5)

            if auto_interrupt:
                self.utility.infoMsg('Interrupt the job...')
                self._autointerrupt()

            self.utility.infoMsg('Close window')
            self.Browser.find_element_by_css_selector('span[id$="close-btnWrap"]').click()
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Run Job tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Run Job tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Run Job tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Run Job: Exception just happened.' + e4.message)
            pass

    def _detailJob(self, comment='sample'):
        try:
            self.itemname = 'Detail of Job'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Pick item: %s' % comment)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(5)

            self.utility.infoMsg('Click Detail button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['detail']).click()
            time.sleep(5)

            self.utility.infoMsg('Close window')
            self.Browser.find_element_by_css_selector('span[id$="close-btnWrap"]').click()
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Delete Job tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Delete Job tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Delete Job tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Delete Job: Exception just happened.' + e4.message)
            pass

    def _deleteJob(self, comment='sample'):
        try:
            self.itemname = 'Delete Job'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Philip: Found a bug in FW 2.2.0.89.... the event trigger would block the delete button. So we need to jump a item to work around.')
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(4) span').click()

            self.utility.infoMsg('Pick item: %s' % comment)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(5)

            self.utility.infoMsg('Click delete button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['delete']).click()
            time.sleep(5)

            self.utility.infoMsg('Apply')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(10)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Delete Job tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Delete Job tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Delete Job tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Delete Job: Exception just happened.' + e4.message)
            pass

    def _backupdevice_mount(self, mount_path='/dev/sdd1'):
        try:
            self.itemname = 'Mount Device'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Click Backup Device button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['backupdevice']).click()
            time.sleep(3)
            self.utility.infoMsg('Pick device: %s' % mount_path)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % mount_path).click()
            time.sleep(3)
            self.utility.infoMsg('Mount device.')
            self.Browser.find_element_by_css_selector('span[id$="mount-btnWrap"]').click()
            time.sleep(20)
            self.utility.infoMsg('Close the window.')
            self.Browser.find_element_by_xpath("//*[contains(text(), 'Close')]").click()
            time.sleep(3)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Mount Device tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Mount Device tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Mount Device tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Mount Device: Exception just happened.' + e4.message)
            pass

    def _backupdevice_unmount(self, unmount_path='/dev/sdd1'):
        try:
            self.itemname = 'Unmount Device'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Click Backup Device button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['backupdevice']).click()
            time.sleep(3)
            self.utility.infoMsg('Pick device: %s' % unmount_path)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % unmount_path).click()
            time.sleep(3)
            self.utility.infoMsg('Unmount device.')
            self.Browser.find_element_by_css_selector('span[id$="unmount-btnWrap"]').click()
            time.sleep(20)
            self.utility.infoMsg('Close the window.')
            self.Browser.find_element_by_xpath("//*[contains(text(), 'Close')]").click()
            time.sleep(3)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Unmount Device tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Unmount Device tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Unmount Device tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname, error='Unmount Device: Exception just happened.' + e4.message)
            pass

    def _backupdevice_refresh(self):
        try:
            self.itemname = 'Refresh Device'
            self.utility.unittest(test_item_name=self.itemname)

            self.utility.infoMsg('Click Backup Device button')
            self.Browser.find_element_by_css_selector(usbbackupbtn['backupdevice']).click()
            time.sleep(3)
            self.utility.infoMsg('Refresh Device.')
            # self.Browser.find_element_by_css_selector('a[id$="refresh"] span[id$="refresh-btnWrap"]').click()
            self.Browser.find_element_by_xpath("//body//div[contains(@class,'x-window-default-resizable')][2]//div[contains(@class,'x-toolbar')]//div//div//a[10]//span").click()
            time.sleep(20)
            self.utility.infoMsg('Close the window.')
            self.Browser.find_element_by_xpath("//*[contains(text(), 'Close')]").click()
            time.sleep(3)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname,
                                   error='Refresh Device tab button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Refresh Device tab button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Refresh Device tab button is invisibled.' + e3.msg)
            pass
        except Exception as e4:
            self.utility.fail_test(testname=self.itemname,
                                   error='Refresh Device: Exception just happened.' + e4.message)
            pass

    def _autointerrupt(self):
        try:
            self.Browser.find_element_by_css_selector('span[id$="stop-btnWrap"]').click()
            return
        except:
            self.utility.infoMsg('Cannot click the stop button, perhaps the backup is finished.')
            pass
    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def usb_backup(self):
        self._login()
        if self.login:
            self._backupdevice_mount()
            self._addJob()
            self._editJob()
            self._runJob()
            self._detailJob()
            self._deleteJob()
            self._backupdevice_unmount()
            self._backupdevice_refresh()
if __name__ == "__main__":
    testing = grack_storage_USB_backup()
    testing.usb_backup()
    testing._logout()