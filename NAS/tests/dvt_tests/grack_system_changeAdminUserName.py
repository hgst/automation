'''
Created on November 6th, 2018
@Author: Philip Yang

Full Title: Change admin user name.
            To create an new Admin username and save the config.
            logout old admin account.
            login new admin account.

Workflow: Mount usb and then create clone job and run process.

Marker the chkUID() feature according GRACK design. ----- Update 2018-11-09

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
from selenium.common.exceptions import ElementNotVisibleException
import time
import device_info

_generalsettings_tab ={
    'web_manager' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'web_admin' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
    'date_time' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(3) span',
}

_recovery_tag_position = {
    'name' : 'input[name*="name"]',
    'password' : 'input[name*="password"]',
    'passwordconf' : 'input[name*="passwordconf"]',
    'question1' : 'input[name*="question1"]',
    'question2' : 'input[name*="question2"]',
    'question3' : 'input[name*="question3"]',
    'answer1' : 'input[name*="answer1"]',
    'answer2' : 'input[name*="answer2"]',
    'answer3' : 'input[name*="answer3"]',
}

class create_new_admin_account():

    def __init__(self):
        self.utility = device_info.device_info()
        self.itemname = 'Create an Admin user name'
        self.Browser = self.utility.browser
        self.login = False
        self.trouble = False
        self.utility.web_login()

    def _loginTab(self):
        # Click Web Admin Password Recovery tab button
        try:
            self.Browser.find_element_by_css_selector('a[class*="x-tab"]:nth-child(6)').click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(_generalsettings_tab['web_admin']).click()
            time.sleep(3)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='WebAdmin tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='WebAdmin tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('WebAdmin tab button cannot be clicked.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def _chkUID(self, userid, pwd, unittest):
        try:
            self.utility.infoMsg('Sending command "id username" with ssh tunnel: ' + userid)
            self.itemname = unittest
            self.utility.unittest(test_item_name=self.itemname)
            temp = self.utility.ssh_execute(login_id=userid, login_pwd=pwd, msg='id ' + userid)

            """ split the temp string and convert to int """
            self.utility.infoMsg(temp)
            _part1 = temp.split("(", 3)[1]
            _uid = _part1.split("=",1)[1]
            int_UID = int(_uid)
            self.utility.infoMsg('Your username permission: ' + _uid)

            """ Check the UID of usernmae """
            if int_UID == 500:
                self.utility.pass_test(testname=self.itemname, result="UID permission matches 500.")
            else:
                self.utility.fail_test(testname=self.itemname, error="UID permission does not match with 500.")
            return

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='UID feature is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname,
                                   error='UID feature cannot be clicked.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname,
                                   error='UID feature makes exception.')
            pass

    def _rename(self, unittest, new_name, new_password, new_password_confirm, testAlert=False):

        self._fillupString(tag='password', contain=new_password)
        self._fillupString(tag='passwordconf', contain=new_password_confirm)
        self._fillupString(tag='question1', contain='My pet')
        self._fillupString(tag='answer1', contain=new_password_confirm)
        self._fillupString(tag='question2', contain='My pup')
        self._fillupString(tag='answer2', contain=new_password_confirm)
        self._fillupString(tag='question3', contain='My puppy')
        self._fillupString(tag='answer3', contain=new_password_confirm)

        if testAlert:
            self.utility.infoMsg('Test: admin length limit and special character.')
            self.itemname='Checkign length limit alert'
            self.utility.unittest(test_item_name=self.itemname)
            self.utility.infoMsg('Inser user name: 123456789012345678901234567890123')
            self._fillupString(tag='name', contain="123456789012345678901234567890123")
            try:
                """ Saving """
                self.utility.infoMsg('Saving the config.')
                self.Browser.find_element_by_css_selector('span[id$="-adminpasswd-ok-btnWrap"]').click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-message-box"] div[class*="x-toolbar"] div[id^="toolbar"] div a:nth-child(1) span').click()
                self.utility.infoMsg('Catching the Alert message.')
                self.utility.pass_test(testname=self.itemname, result='The length limit is fine.')
                pass
            except InvalidSelectorException:
                self.utility.fail_test(testname=self.itemname, error='WebAdmin Save button or Alert messagebox cannot be clicked.')
                pass
            except NoSuchElementException:
                self.utility.fail_test(testname=self.itemname, error='WebAdmin Save button or Alert messagebox is not found.')
                pass
            except ElementNotVisibleException:
                self.utility.infoMsg('WebAdmin Save button or Alert messagebox cannot be clicked.')
                pass

            self.utility.infoMsg('Test: Admin username with special character')
            self.itemname = 'Checking special character alert'
            self.utility.unittest(test_item_name=self.itemname)
            self.utility.infoMsg('Inser user name: )(*&%$^@#/\%!$@!><?:')
            self._fillupString(tag='name', contain=")(*&%$^@#/\%!$@!><?:")
            try:
                """ Saving """
                self.utility.infoMsg('Saving the config.')
                self.Browser.find_element_by_css_selector('span[id$="-adminpasswd-ok-btnWrap"]').click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-message-box"] div[class*="x-toolbar"] div[id^="toolbar"] div a:nth-child(1) span').click()
                self.utility.infoMsg('Catching the Alert message.')
                self.utility.pass_test(testname=self.itemname, result='Special character checking is fine.')
                pass
            except InvalidSelectorException:
                self.utility.fail_test(testname=self.itemname,
                                       error='WebAdmin Save button or Alert messagebox cannot be clicked.')
                pass
            except NoSuchElementException:
                self.utility.fail_test(testname=self.itemname,
                                       error='WebAdmin Save button or Alert messagebox is not found.')
                pass
            except ElementNotVisibleException:
                self.utility.infoMsg('WebAdmin Save button or Alert messagebox cannot be clicked.')
                pass

        self.itemname = unittest
        self.utility.unittest(test_item_name=self.itemname)

        self._fillupString(tag='name', contain=new_name)


        if self.trouble:
            self.utility.fail_test(testname=self.itemname, error='Element is gone. Interrupt the testing')
            return

        try:
            """ Saving """
            self.Browser.find_element_by_css_selector('span[id$="-adminpasswd-ok-btnWrap"]').click()
            time.sleep(10)
            self.utility.infoMsg('Rename is done.')
            self.utility.pass_test(testname=self.itemname)
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='WebAdmin Save button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='WebAdmin Save button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname, error='WebAdmin Save button cannot be clicked.')
            pass

    def _fillupString(self, tag, contain):
        try:
            _name = self.Browser.find_element_by_css_selector(_recovery_tag_position[tag])
            _name.clear()
            _name.send_keys(contain)
            time.sleep(0.5)
            self.trouble = False
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error= tag + ' dialogue cannot be selected.')
            self.trouble = True
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error= tag + ' is not found.')
            self.trouble = True
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg( tag +' element is invisible.')
            self.trouble = True
            pass

    def _customize_web_logout(self):
        '''GRACK webpage logout automatically'''
        try:
            # Click tab and logout of device
            self.Browser.find_element_by_id('splitbutton-1022-btnIconEl').click()
            time.sleep(2)

            try:
                self.Browser.find_element_by_id('menuitem-1027-textEl').click()  # Apply for FW under v1.1.0
            except NoSuchElementException:
                # Handle UI changing for FW v1.1.0
                self.Browser.find_element_by_css_selector(
                    'div[id^="workspace-menu-"] div[class*="x-menu-body"] div[class*="x-box-inner"] div[class*="x-box-target"] div:nth-child(5) a span').click()
                pass
            time.sleep(2)
            self.Browser.find_element_by_id('button-1006-btnInnerEl').click()

        except NoSuchElementException as e:
            self.utility.errorMsg('Fail to logout: ' + e.msg)
        except Exception as e2:
            self.utility.errorMsg(e2.message)
        finally:
            self.utility.infoMsg("Logout finished!")

    def _customize_web_login(self, unittest, login_username='admin', login_password='gtech'):
        """ Login with new user name"""

        self.itemname = unittest
        self.utility.unittest(test_item_name=self.itemname)
        try:

            if login_password == 1:
                self.utility.infoMsg('Success to retrieve the account and password.')
                pass
            else:
                username = self.Browser.find_element_by_id("username-inputEl")
                username.clear()
                username.send_keys(login_username)
                password = self.Browser.find_element_by_id("password-inputEl")
                password.clear()
                password.send_keys(login_password)

            self.Browser.find_element_by_id("login-panel-login").click()
            time.sleep(0.5)
            try:
                self.Browser.find_element_by_css_selector('div[id^="messagebox-"]').is_displayed()
                self.utility.infoMsg('You have logged in another browser or you didn\'t logout correctly before.')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-toolbar-targetEl"] > a[id^="button"]:nth-child(2) > span[id$="-btnWrap"]').click()
            except NoSuchElementException:
                pass
            except ElementNotVisibleException:
                pass
            self.utility.pass_test(testname=self.itemname)
            time.sleep(8)
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Fail to login. Cannot find the element.')
            return
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Fail to login. Unknown error is happened.')
            return

        self.utility.infoMsg('Login finished!')

    def rename_admin(self, change_name="admin2", change_Password='gtech'):

        self._loginTab()
        # self._chkUID(userid='admin', pwd='gtech', unittest='Checking Admin account permission')

        if self.login:

            """ Test change admin name and write down hint messages. Include the testing of length and special character."""
            self._rename(new_name=change_name, new_password=change_Password, new_password_confirm=change_Password, testAlert=True, unittest='Rename with Admin account')

            """ Login with new user name """
            self._customize_web_login(login_username=change_name, login_password=change_Password, unittest='Login with new name' )
            self._loginTab()

            """ Check UID of admin """
            # self._chkUID(userid=change_name, pwd='gtech', unittest='Checking Admin account permission after rename')

            """ Recover Admin name and login again """
            self.utility.infoMsg('Recover Admin account name.')
            self._rename(new_name='admin', new_password=change_Password, new_password_confirm=change_Password,
                         testAlert=False, unittest='Recover Admin account name')
        if 1:
            """ Expect Admin forgot password """
            new_password = self._query_new_pwd()
            if self.trouble:
                self.utility.infoMsg('Failed to query new password.')
            else:
                self._customize_web_login(login_username='admin', login_password=new_password,
                                      unittest='Login with Admin')

    def _query_new_pwd(self):
        try:
            self.itemname = 'Forgot password'
            self.utility.unittest(test_item_name=self.itemname)

            """ Answer the question """
            self.utility.infoMsg('Click forget password.')
            self.Browser.find_element_by_css_selector('span[id*="forgot-password-link-btnWrap"] span span:nth-child(2)').click()
            time.sleep(1)
            self.utility.infoMsg('Insert the answer.')
            answer = self.Browser.find_element_by_css_selector('input[id^="challenge-answer-"]')
            answer.send_keys('gtech')
            time.sleep(1)
            self.utility.infoMsg('Summit')
            self.Browser.find_element_by_css_selector(
                'span[id*="password-reset-panel-password-reset-btnWrap"] span span:nth-child(2)').click()
            time.sleep(1)

            """ Parser new password """
            _temp_string = self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').text
            self.utility.infoMsg('Your new password: ' + _temp_string)
            _temp_string = _temp_string.splitlines()[0]
            _newpwd = _temp_string.split(" ", 5)
            new_password = _newpwd[4]

            """ Click OK button """
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(2)
            self.trouble = False
            self.utility.pass_test(testname=self.itemname, result='Get new password: ' + new_password)
            time.sleep(2)
            return 1

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Fail to login. Cannot find the element.')
            self.trouble = True
            return
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Fail to login. Unknown error is happened.')
            self.trouble = True
            return

if __name__ == "__main__":
    testing = create_new_admin_account()
    testing.rename_admin(change_name='admin3')
    testing._logout()
