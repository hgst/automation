'''
Created on June 19, 2018
@Author: Philip Yang

Full Title: GRACK RAID hotspare
To select a disk to join hotspare.

'''
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


class hotsparetesting():

    def __init__(self):
        self.itemname = 'GRACK hotspare testing'
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.hotspareStatus = False
        self.pickup = False

    def _pickupraid(self):
        time.sleep(2)
        try:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table[class*="x-grid-item-selected"]:nth-child(1)').is_displayed()
            self.pickup = True
        except:
            pass

    def run(self, slot = '1'):
        ''' Run hotspare
        :param raidname: choose an exist raidname to click hot spare button.
        :param slot: To remove slot harddrive.
        '''
        self.utility.unittest(test_item_name=self.itemname)
        try:
            #Click tab of RAID
            self.utility.infoMsg('*** Hotspare testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector('div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(30)

            #Select RAID
            self.utility.infoMsg('Pick up a RAID...')
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)

            self._pickupraid()
            while self.pickup is False:
                self.utility.infoMsg('...click RAID again.')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
                time.sleep(3)
                self._pickupraid()

            #Click hotspare btn
            self.Browser.find_element_by_css_selector('span[id$="-edithotspares-btnWrap"]').click()
            time.sleep(5)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not found: please make sure your RAID has been created.' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                         error='Hotspare testing: Stuck in the popup window.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: raid or hotspare cannot be selected.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                         error='Hotspare testing: Stuck in the popup window.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have provide other disk for hotspare:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Hotspare testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                         error='Hotspare testing: Exception has happened. ' + e.message)

        try:
            self.utility.infoMsg(msg='Status Checking')
            self.detectHotspares(checkslot=slot)
            time.sleep(0.5)

            if (self.hotspareStatus is False):
                self.utility.infoMsg('Select slot %s  to join hotspare.' % slot)
                time.sleep(0.5)
                self.Browser.find_element_by_css_selector(
                    'div[class^="x-autocontainer-innerCt"][id^="checkboxgridfield"] div div[id^="ext-comp"] div[class*="x-grid-view"]  div[class*="x-grid-item-container"] table:nth-child(1) tbody tr td:nth-child(1)').click()
                time.sleep(0.5)
            else:
                self.utility.infoMsg('The 1st drive slot already join hotspare.')
            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                         error='Hotspare testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('There has no disk for hotspare:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Hotspare testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                         error='Hotspare testing: Exception has happened. ' + e.message)

    def detectHotspares(self, checkslot):
        try:
            # self.Browser.find_element_by_css_selector('div[class^="x-autocontainer-innerCt"][id^="checkboxgridfield"] div div[id^="ext-comp"] div[class*="x-grid-view"]  div[class*="x-grid-item-container"] table[class*="x-grid-item-selected"]:nth-child( %s )' % checkslot)
            self.Browser.find_element_by_css_selector(
                'div[class^="x-autocontainer-innerCt"][id^="checkboxgridfield"] div div[id^="ext-comp"] div[class*="x-grid-view"]  div[class*="x-grid-item-container"] table:nth-child(1) tbody tr td:nth-child(1)').click()
            self.hotspareStatus = True
        except NoSuchElementException as e2:
            self.utility.infoMsg('Slot1 is not selected.')
            self.hotspareStatus = False
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Detecting Status: Exception has happened. ' + e.message)
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__" :
    alltesting = hotsparetesting()
    alltesting.run(slot='5')
    alltesting.logout()


