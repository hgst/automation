'''
    #abc
'''
import time
import unittest

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
#from selenium.webdriver.common.keys import Keys

LOGIN_NAME = 'admin'
LOGIN_PW = 'gtech'
tab = {
    'dashboard': 'a[class*="x-tab"]:nth-child(1)',
    'workspaces': 'a[class*="x-tab"]:nth-child(2)',
    'storage': 'a[class*="x-tab"]:nth-child(3)',
    'connect': 'a[class*="x-tab"]:nth-child(4)',
    'accessControl': 'a[class*="x-tab"]:nth-child(5)',
    'system': 'a[class*="x-tab"]:nth-child(6)'
}

subTab = {
    'storage': {'mdRAID': '.sub-menu-panel a[class*="x-tab"]:nth-child(4)'}
}

button = {'storage': {'create': 'a[id*="create"]',
                      'hotSpare': '',
                      'details': '',
                      'mount': '',
                      'unMount': '',
                      'delete': ''}
}

raidLevel = {
        'raid5': 'div ul li:nth-child(1)', 'raid6': 'div ul li:nth-child(2)'
}

raidMinSelections = {'raid5': 3, 'raid6': 4}

testRAIDLevel = 'raid5'
testRAIDName = 'myRaid5'
DEV_IP = 'http://192.168.11.105'


class MDRAID(unittest.TestCase):
    '''
        #abc
    '''
    def setUp(self):
        self.driver = webdriver.Firefox()


    def test_create_md_raid(self):
        '''
            Creation of a new RAID set by selecting the RAID level and
            the drives to use to create the RAID.
        '''
        driver = self.driver
        driver.get(DEV_IP)
        print(driver.title)

        username = driver.find_element_by_id('username-inputEl')
        username.send_keys(LOGIN_NAME)
        password = driver.find_element_by_id('password-inputEl')
        password.send_keys(LOGIN_PW)
        driver.find_element_by_id('login-panel-login').click()
        time.sleep(4)

        driver.find_element_by_css_selector(tab["storage"]).click()
        time.sleep(3)
        driver.find_element_by_css_selector(subTab["storage"]["mdRAID"]).click()
        time.sleep(3)

        driver.find_element_by_css_selector(button["storage"]["create"]).click()
        raidName = driver.find_element_by_css_selector('input[name="name"')
        raidName.send_keys(testRAIDName)
        driver.find_element_by_css_selector('div[id*="picker"').click()
        time.sleep(1)
        driver.find_element_by_css_selector(raidLevel['raid5']).click()

        # get available drives
        availableDrives = len(driver.find_elements_by_class_name('x-grid-row-checker'))
        print("Available drives =", availableDrives)

        if testRAIDLevel == 'raid5':
            if availableDrives < raidMinSelections['raid5']:
                raise "available drives less 3"
            driver.find_element_by_css_selector('table:nth-child(1) div[class*="x-grid-row-checker"]').click()
            driver.find_element_by_css_selector('table:nth-child(2) div[class*="x-grid-row-checker"]').click()
            driver.find_element_by_css_selector('table:nth-child(3) div[class*="x-grid-row-checker"]').click()
        elif testRAIDLevel == 'raid6':
            if availableDrives < raidMinSelections['raid6']:
                raise "available drives less 4"
            driver.find_element_by_css_selector('table:nth-child(1) div[class*="x-grid-row-checker"]').click()
            driver.find_element_by_css_selector('table:nth-child(2) div[class*="x-grid-row-checker"]').click()
            driver.find_element_by_css_selector('table:nth-child(3) div[class*="x-grid-row-checker"]').click()
            driver.find_element_by_css_selector('table:nth-child(4) div[class*="x-grid-row-checker"]').click()
        else:
            raise "Invalid RAID Level"

        driver.find_element_by_css_selector('a[id*="ok"]').click()
        driver.find_element_by_css_selector('div[class*="x-message-box"] a[class*="x-btn"]:nth-child(2)').click()

        time.sleep(30)
        # Check MD RAID is created or not
        # get RAID grid items
        gridItems = driver.find_elements_by_class_name('x-grid-item')
        CREATED_RAID = False
        for raidItem in gridItems:
            createdRaidName = raidItem.find_element_by_css_selector('tr td:nth-child(1)').text
            print(createdRaidName)
            createdRaidName = createdRaidName.split(':')[1]
            if createdRaidName == testRAIDName:
                CREATED_RAID = True
                break

        if CREATED_RAID:
            print("Created RAID: ", testRAIDName)

    def test_unmount_md_raid(self):
        '''
            Unmount a RAID set
        '''
        driver = self.driver
        driver.get(DEV_IP)

        username = driver.find_element_by_id('username-inputEl')
        username.send_keys(LOGIN_NAME)
        password = driver.find_element_by_id('password-inputEl')
        password.send_keys(LOGIN_PW)
        driver.find_element_by_id('login-panel-login').click()
        time.sleep(4)

        driver.find_element_by_css_selector(tab["storage"]).click()
        time.sleep(3)
        driver.find_element_by_css_selector(subTab["storage"]["mdRAID"]).click()
        time.sleep(3)

        gridItems = driver.find_elements_by_class_name('x-grid-row')
        raidCounts = len(gridItems)

        UN_MOUNTED = False
        if raidCounts != 0:
            print("raidCounts = ", raidCounts)
            for raidItem in gridItems:
                raidItem.click()
                try:
                    unmountButton = driver.find_element_by_css_selector('a[id*="-unmount"]:not(.x-btn-disabled)')
                    unmountButton.click()
                    time.sleep(5)

                    # todo: need to check unmount a RAID set is successfully or not
                    UN_MOUNTED = True
                    break
                except NoSuchElementException:
                    print("WARNING: NoSuchElementException, need to find next element.")

            if not UN_MOUNTED:
                raise Exception('"WARNING: No available RAID can be unmount.')
        else:
            raise Exception('"WARNING: No available RAID can be unmount.')

    def test_mount_md_raid(self):
        '''
            Mount a RAID set
        '''
        driver = self.driver
        driver.get(DEV_IP)

        username = driver.find_element_by_id('username-inputEl')
        username.send_keys(LOGIN_NAME)
        password = driver.find_element_by_id('password-inputEl')
        password.send_keys(LOGIN_PW)
        driver.find_element_by_id('login-panel-login').click()
        time.sleep(4)

        driver.find_element_by_css_selector(tab["storage"]).click()
        time.sleep(3)
        driver.find_element_by_css_selector(subTab["storage"]["mdRAID"]).click()
        time.sleep(3)

        gridItems = driver.find_elements_by_class_name('x-grid-row')
        raidCounts = len(gridItems)

        MOUNTED = False
        if raidCounts != 0:
            print("raidCounts = ", raidCounts)
            for raidItem in gridItems:
                raidItem.click()
                try:
                    mountButton = driver.find_element_by_css_selector('a[id*="-mount"]:not(.x-btn-disabled)')
                    mountButton.click()
                    time.sleep(5)

                    # todo: need to check mount a RAID set is successfully or not
                    MOUNTED = True
                    break
                except NoSuchElementException:
                    print("WARNING: NoSuchElementException, need to find next element.")

            if not MOUNTED:
                raise Exception('"WARNING: No available RAID can be mount.')
        else:
            raise Exception('"WARNING: No available RAID can be mount.')

    def test_delete_md_raid(self):
        '''
            Delete a RAID set
        '''
        driver = self.driver
        driver.get(DEV_IP)

        username = driver.find_element_by_id('username-inputEl')
        username.send_keys(LOGIN_NAME)
        password = driver.find_element_by_id('password-inputEl')
        password.send_keys(LOGIN_PW)
        driver.find_element_by_id('login-panel-login').click()
        time.sleep(4)

        driver.find_element_by_css_selector(tab["storage"]).click()
        time.sleep(3)
        driver.find_element_by_css_selector(subTab["storage"]["mdRAID"]).click()
        time.sleep(3)

        gridItems = driver.find_elements_by_class_name('x-grid-row')
        raidCounts = len(gridItems)

        DELETED = False
        if raidCounts != 0:
            print("raidCounts = ", raidCounts)
            for raidItem in gridItems:
                raidItem.click()
                try:
                    delButton = driver.find_element_by_css_selector('a[id*="-delete"]:not(.x-btn-disabled)')
                    delButton.click()
                    driver.find_element_by_css_selector('div[class*="x-message-box"] a[class*="x-btn"]:nth-child(2)').click()
                    time.sleep(5)

                    # todo: need to check delete a RAID set is successfully or not
                    DELETED = True
                    break
                except NoSuchElementException:
                    print("WARNING: NoSuchElementException, need to find next element.")

        if not DELETED:
            raise Exception('"WARNING: No available RAID can be delete.')

    def test_add_hotspares(self):
        '''
            To create a disk to allow automatic recovery of the
            data integrity for the selected RAID set if a drive fails
        '''
        driver = self.driver
        driver.get(DEV_IP)

        username = driver.find_element_by_id('username-inputEl')
        username.send_keys(LOGIN_NAME)
        password = driver.find_element_by_id('password-inputEl')
        password.send_keys(LOGIN_PW)
        driver.find_element_by_id('login-panel-login').click()
        time.sleep(4)

        driver.find_element_by_css_selector(tab["storage"]).click()
        time.sleep(3)
        driver.find_element_by_css_selector(subTab["storage"]["mdRAID"]).click()
        time.sleep(5)

        gridItems = driver.find_elements_by_class_name('x-grid-row')
        raidCounts = len(gridItems)

        HOTSPARE = False
        if raidCounts != 0:
            print("raidCounts = ", raidCounts)
            for raidItem in gridItems:
                raidItem.click()
                time.sleep(1)
                try:
                    hotspareButton = driver.find_element_by_css_selector('a[id*="-recover"]:not(.x-btn-disabled)')
                    hotspareButton.click()
                    time.sleep(3)

                    availableDrives = driver.find_elements_by_class_name('x-grid-row-checker')
                    availableCounts = len(availableDrives)

                    if availableCounts != 0:
                        availableDrives[0].click()

                    driver.find_element_by_css_selector('a[id*="ok"]').click()

                    # todo: need to check add hot spare is successfully or not
                    HOTSPARE = True
                    break
                except NoSuchElementException:
                    print("WARNING: NoSuchElementException, need to find next element.")

        if not HOTSPARE:
            raise Exception('"WARNING: No available RAID can add hot spare.')
    # def tearDown(self):
    #     self.driver.quit()
    #     # self.driver.close()


if __name__ == "__main__":
    # To run whole test suite
    # unittest.main()

    # #specify test(s) to execute individual
    suite = unittest.TestSuite()

    # suite.addTest(MDRAID('test_create_md_raid'))
    # suite.addTest(MDRAID('test_unmount_md_raid'))
    # suite.addTest(MDRAID('test_mount_md_raid'))
    # suite.addTest(MDRAID('test_delete_md_raid'))
    suite.addTest(MDRAID('test_add_hotspares'))

    unittest.TextTestRunner(verbosity=2).run(suite)
