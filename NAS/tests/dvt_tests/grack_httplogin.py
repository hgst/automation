'''
    Created on April 3, 2018
    @Author: Philip.Yang

    Full Title: http login and logout.
    Open broswer and key-in username/password after then click logout button report FW version.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import WebDriverException
import time
import device_info

test_item_name = 'HTTP Login & Logout'
class MyClass():

    def run(self):
        utility = device_info.device_info()
        open_browser = utility.browser
        utility.unittest(test_item_name)

        g_name = utility.grack_username
        g_passwd = utility.grack_rootpwd
        # step1 for login behavior
        step1 = False

        open_browser.get(utility.deviceIP)
        time.sleep(5)
        # Keyin username and password and warp to dashboard
        try:
            username = open_browser.find_element_by_id("username-inputEl")
            username.send_keys(g_name)
            password = open_browser.find_element_by_id("password-inputEl")
            password.send_keys(g_passwd)

            open_browser.find_element_by_id("login-panel-login").click()
            time.sleep(8)
        except NoSuchElementException as e:
            utility.fail_test(test_item_name, 'Fail: ' + e.msg)
            utility.errorMsg('Fail to login. Cannot find the element.')
            pass

        # To detect login fail msg.
        try:
            open_browser.find_element_by_css_selector('div[class*="x-message-box-error"]')
            utility.infoMsg('Catch error dialogue.')
            step1 = False
        except NoSuchElementException:
            step1 = True
            pass

        try:
            open_browser.find_element_by_css_selector('div[class*="x-message-box-guru"]')
            utility.infoMsg('Catch error dialogue. Incorrect username or password.')
            step1 = False
        except NoSuchElementException:
            step1 = True
            utility.infoMsg('Login Succese!')
            pass
        finally:
            utility.infoMsg('Login testing has finished.')


        if(step1):
            try:
                time.sleep(5) # Find element need time to wait the webpage source appear.
                getList = open_browser.find_elements_by_css_selector('div > ul > li:nth-child(2)')
                utility.infoMsg("Parsing Device Name: " + getList[0].text)
            except NoSuchElementException:
                utility.warn_test('Cannot locate the element of device name.')
                pass
            except ElementNotVisibleException:
                utility.warn_test('Device name is not visible.')
                pass
            except WebDriverException as wde:
                utility.warn_test(wde.msg)
                pass


            try:
                # Click tab and logout of device
                open_browser.find_element_by_id('splitbutton-1022-btnIconEl').click()
                time.sleep(5)
                open_browser.find_element_by_id('menuitem-1027-textEl').click()
                time.sleep(5)
                open_browser.find_element_by_id('button-1006-btnInnerEl').click()
                open_browser.close()

            except NoSuchElementException as e:
                utility.errorMsg('Fail to logout.' + e.msg)
                utility.fail_test(test_item_name, 'Fail to logout.')
                pass
            else:
                utility.infoMsg('Logout Succese!')
                utility.pass_test(test_item_name)

            finally:
                utility.infoMsg("*** Finish Testing ***")
        else:
            utility.infoMsg("Cause Login fail, so we ignore the loguot testing.")
            open_browser.close()
        utility.get_test_results()


mytest = MyClass()
mytest.run()

