'''
Created on July 31, 2018
@Author: Philip Yang

Full Title: GRACK System: Centificates

Certificates service: To add SSH/SSL certification item.
'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

function_button = {
    'createssh' : 'div[class*="x-menu-default"] div div[id$="innerCt"] div[class*="x-box-target"] div:nth-child(1) a span',
    'importssh' : 'div[class*="x-menu-default"] div div[id$="innerCt"] div[class*="x-box-target"] div:nth-child(2) a span',
    'createssl' : 'div[class*="x-menu-default"] div div[id$="innerCt"] div[class*="x-box-target"] div:nth-child(1) a span',
    'importssl' : 'div[class*="x-menu-default"] div div[id$="innerCt"] div[class*="x-box-target"] div:nth-child(2) a span',
}

certification_tab = {
    'SSH' : 'div[id$="center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(1) span',
    'SSL' : 'div[id$="center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span'
}

key_size = {
    '512' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ",
    '1024' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ",
    '2048' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[3] ",
    '4096' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[4] "
}

ssl_option ={
    'keySize' : 'div[class*="x-window-container"] div[id$="-body"] div div div div:nth-child(1) div div div[id$="trigger-picker"]',
    'pov' : 'div[class*="x-window-container"] div[id$="-body"] div div div div:nth-child(2) div div div[id$="trigger-picker"]',
    'country' : 'div[class*="x-window-container"] div[id$="-body"] div div div div:nth-child(8) div div div[id$="trigger-picker"]',
}

pov = {
    '1' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    '7' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[7] ",
    '30' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[10] ",
    '180' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[12] ",
    '365' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[14] ",
    '3650' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[19] ",
}

_ssh_privateKey = '-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA0lo6bl97jaNhWxpMIePgYw9uKAasC6oQDjBmviMRGqW4Fga0 ' \
                 '\nmiov62tZTdhiUZAfXHtGgR5x5xYMxGDPK1iRowhkLENkjCVu9O/O5HFap+vNmRVZ\nzwEQqWi+aWbj1JmmmBBxvftCJLbYt/WgH ' \
                 'q6pWWW/PGX+5OIuRLGNsb42bjTk1rBp\nx/dcWos8FYD0XLuQUMCZkebdW3HtcN8F18ooVST7zCq5MMKuzo2vTQRrtvLaAxx+\nYKIQO ' \
                 'qqBwRG+BSQ7TmELRVz47mWbI5ClBJFQVeZoeV7kkgaYf0zr4mJEA3C4acB5\n4R3fjk79Zj7NsDkBPpRuhiL+ez3koRvaW5TO7QIDAQ ' \
                 'ABAoIBAQCczI1JwPsEdYkY\ns92UVZzpupLOW0rb1wTozsOHb/RL/MPgB1eQo2nc/sQu9uEzE0+NTIcdsGgPbaxO\npitHkFnfQV4KcK ' \
                 'H+pdiz1B5Qwv5kta0oM3YijBSzc1SclQm3bGF2cKuYhjajz3h0\niC+3L1MlRGbsysraKo96vS92EufX9VObfOqrj/453YJsHgqum9TdR6 ' \
                 'i6mMonJoWx\nYgSWwE4TqDnLOFAti/wHLq7mBOZJ85onIuBJmqD9Jrptou0RqqA2vD4v7z8Ap+Lv\nFeTUyechStdIris2QcOdGymGNS9TZ ' \
                 'xLt3xHVTdjdbdrn8NomGFhZ78YOpHwV9rCM\nZ5VvDjSxAoGBAO4yiAwuMGTyVdt1I7HHNrLU/Kmm4BJ5VmM/dD/TjFar5mYzcaeR\nQmQ4 ' \
                 'sm7og/Rbv2aD2dG9K8lar0cgedjpzYFaJVGh2lJE68B48IQbh1o4ZVhS9SYz\n7NVTtOQIar4yXiFRh4WivCqtwwv36qACtGTKypgja2rYCU ' \
                 'fmIvUv87CnAoGBAOIS\n72xq9OzVNWA9l6S+BiLWppS0KbXGijtbso42sMmHQRI21/1oGYKObAUnAKSOBavT\nn8mQBADdjSSC4lXP/A8cz9 ' \
                 'qGqP6obwpRHdVviHaMlqvPmtXiRvysSBISx1h+ztol\nD67RZ9uYrVqFeKk6ox4wy6k+S0bH2iwReo1O00JLAoGAJI16lSvtX0BU7pV+QcTJ\nX ' \
                 'w4SK9Kg5hTareZJ55WGHrLIa3yPf2BqKMkOkLh7r1748zlejrIR6xdQeIDCUggm\n8iesPGezAhbzepjUh+FVlwG6g6BPYGpteJsc0jV8c+10 ' \
                 'xnYlbtt3NrFtkgOcbKud\nlS4NguVEkaPPYMovKSMPqVcCgYB0j4q+fY72pR82Fcuf9xG22LFerl1rCYiv/iFb\nzbrszT2xpHFMiGXOuJvdND ' \
                 'TSox8tbplOFWDbCWACWTFVST0Ola5dX1y2oCVQTm5x\nY2YaqXXt88ZDTL2I/VLTbrc1W3xPYRhq7DD+OgP3TiXeKwt7P3FiO2oYfmZwnjpv\nr ' \
                 'RqjbQKBgQDBsa7xtcwcjnkDHrTLufbmYRz8r9PewL404nID5hdTQnI/Hb7lzLZI\nhyYqJjkmI0c+F1hAXMUeb1BUieaYQw8ajbfIqAnuu3TImj ' \
                 'VDs3XLjlCjO/6lKeEG\ncroFGTxjWhBjTlX4ea/JYw5DW98v77dLqudP8EhofhZMeeGhs0W2eA==\n-----END RSA PRIVATE KEY-----\n'

_ssh_publicKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSWjpuX3uNo2FbGkwh4+BjD24oBqwLqhAOMGa+IxEapbgWBrSaKi/ra1lN2GJRkB9ce0aBHnHn " \
                "FgzEYM8rWJGjCGQsQ2SMJW70787kcVqn682ZFVnPARCpaL5pZuPUmaaYEHG9+0Iktti39aAerqlZZb88Zf7k4i5EsY2xvjZuNOTWsGnH91xaizwVg " \
                "PRcu5BQwJmR5t1bce1w3wXXyihVJPvMKrkwwq7Oja9NBGu28toDHH5gohA6qoHBEb4FJDtOYQtFXPjuZZsjkKUEkVBV5mh5XuSSBph/TOviYkQDcLh " \
                "pwHnhHd+OTv1mPs2wOQE+lG6GIv57PeShG9pblM7t sample2"


class certificate():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'system SSH SSL cerfiticates'
        self._login(systemtab='certificate')

    def _login(self, systemtab):
        # Click Certificates tab button
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="sub-menu-panel"] div[id*="system"] div[class*="x-tab-bar"] div[id$="-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(4) span').click()
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Certificates button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Certificates Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Certificates Button cannot be clicked.')
            pass

    def _create_import_sshitem(self, function, descriptionString):
        try:
            if function == 'createssh':
                self.itemname = 'Create SSH Certification'
                self.utility.unittest(test_item_name=self.itemname)
                self.utility.infoMsg('Create SSH item')
                time.sleep(3)
                self.Browser.find_element_by_css_selector('span[id$="-ssh-add-btnWrap"]').click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector(function_button[function]).click()
                time.sleep(0.5)

            elif function =='importssh':
                self.itemname = 'import SSH Certification'
                self.utility.unittest(test_item_name=self.itemname)

                self.utility.infoMsg('Import SSH item')
                self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
                time.sleep(0.5)
                self.Browser.find_element_by_css_selector(certification_tab['SSH']).click()
                time.sleep(0.5)
                self.Browser.find_element_by_css_selector('span[id$="-ssh-add-btnWrap"]').click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector(function_button['importssh']).click()
                time.sleep(0.5)

                self.utility.infoMsg('Write down the PrivateKey')
                time.sleep(0.5)
                _privateKKey = self.Browser.find_element_by_css_selector('textarea[name*="privatekey"]')
                _privateKKey.send_keys(_ssh_privateKey)

                self.utility.infoMsg('Write down the PubicKey')
                _publicKey = self.Browser.find_element_by_css_selector('input[name*="publickey"]')
                _publicKey.send_keys(_ssh_publicKey)
                time.sleep(0.5)

            self.utility.infoMsg('Write down the comment')
            _description = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            _description.send_keys(descriptionString)
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(15)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Certificates button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Certificates Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Certificates Button cannot be clicked.')
            pass

    def _create_import_sslitem(self, function, descriptionString, keysize='4096', periodOfvalidity='3650',
                               ssl_privateKey="", ssl_certificate=""):
        try:
            if function == 'createssl':
                self.itemname = 'Create SSL Certification'
                self.utility.unittest(test_item_name=self.itemname)
                self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
                time.sleep(0.5)
                self.utility.infoMsg('Create SSL item')
                time.sleep(3)
                self.Browser.find_element_by_css_selector('span[id$="-ssl-add-btnWrap"]').click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector(function_button[function]).click()
                time.sleep(0.5)
                self.utility.infoMsg('Fill in the address')
                _cn = self.Browser.find_element_by_css_selector('input[name*="cn"]')
                _cn.send_keys(descriptionString)
                _o = self.Browser.find_element_by_css_selector('input[name*="o"]')
                _o.send_keys('WD')
                _ou = self.Browser.find_element_by_css_selector('input[name*="ou"]')
                _ou.send_keys('unittest')
                _l = self.Browser.find_element_by_css_selector('input[name*="l"]')
                _l.send_keys('mountainview')
                _st = self.Browser.find_element_by_css_selector('input[name*="st"]')
                _st.send_keys('CA')

                self.utility.infoMsg('Chosen the key size...')
                self.Browser.find_element_by_css_selector(ssl_option['keySize']).click()
                time.sleep(1)
                self.Browser.find_element_by_xpath(key_size[keysize]).click()
                time.sleep(0.5)
                self.utility.infoMsg('Chosen the period of validity...')
                self.Browser.find_element_by_css_selector(ssl_option['pov']).click()
                time.sleep(1)
                self.Browser.find_element_by_xpath(pov[periodOfvalidity]).click()
                time.sleep(0.5)
                self.utility.infoMsg('Chosen the country...')
                self.Browser.find_element_by_css_selector(ssl_option['country']).click()
                time.sleep(1)
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')][3]//div//ul/li[5] ").click()
                time.sleep(0.5)

            elif function == 'importssl':
                self.itemname = 'import SSL Certification'
                self.utility.unittest(test_item_name=self.itemname)
                self.Browser.find_element_by_css_selector(certification_tab['SSH']).click()
                time.sleep(0.5)
                self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
                time.sleep(0.5)

                if (ssl_certificate == "") | (ssl_privateKey == ""):
                    self.utility.warn_test(testname=self.itemname,
                                           warning='You need to provided the key first. The Testing is not finished.')
                    return

                self.utility.infoMsg('Import SSL item')
                self.Browser.find_element_by_css_selector('span[id$="-ssl-add-btnWrap"]').click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector(function_button['importssl']).click()
                time.sleep(0.5)

                self.utility.infoMsg('Write down the PrivateKey')
                time.sleep(0.5)

                _privateKKey = self.Browser.find_element_by_css_selector('textarea[name*="privatekey"]')
                _privateKKey.send_keys(ssl_privateKey)

                self.utility.infoMsg('Write down the certificate')
                _publicKey = self.Browser.find_element_by_css_selector('textarea[name*="certificate"]')
                _publicKey.send_keys(ssl_certificate)
                time.sleep(0.5)
                self.utility.infoMsg('Write down the comment')
                _description = self.Browser.find_element_by_css_selector('input[name*="comment"]')
                _description.send_keys(descriptionString)
                time.sleep(0.5)

            time.sleep(0.5)
            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(15)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Certificates button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Certificates Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Certificates Button cannot be clicked.')
            pass

    def newSSH(self, subfunction='all', comment_forCreate="sample1", comment_forImport="sample2"):
        ''' new_ssh : To CREATE OR IMPORT SSH CERTIFICATION.
        :param subfunction : support 3 parameter of "all", "create" and "import".
        :param comment_forCreate : comment for create ssh certification.
        :param comment_forImport : comment for import ssh certification.
        '''
        self.utility.infoMsg('Testing plan : %s' % subfunction)
        self.Browser.find_element_by_css_selector(certification_tab['SSH']).click()
        time.sleep(0.5)
        if self.login:
            if subfunction == 'all':
                self._create_import_sshitem(function='createssh', descriptionString=comment_forCreate)
                self._create_import_sshitem(function='importssh',descriptionString=comment_forImport)
            elif subfunction == 'create':
                self._create_import_sshitem(function='createssh',descriptionString=comment_forCreate)
            elif subfunction =="import":
                self._create_import_sshitem(function='importssh',descriptionString=comment_forImport)
            else:
                print('Testing is not start yet. You send a wrong parameter.')

    def editSSH(self, comment, replaceComment, replacePrivateKey="", replacePublicKey=""):
        self.itemname = 'Edit SSH Certification'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(certification_tab['SSH']).click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(text(), '%s')]" % comment).click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ssh-edit-btnWrap"]').click()
            time.sleep(0.5)
            self.utility.infoMsg('Write down the comment')
            _description = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            _description.clear()
            _description.send_keys(replaceComment)
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Edit button or target cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Edit Button or target is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Edit Button or target is not visibled.')
            pass

    def deleteSSH(self, comment):
        self.itemname = 'Delete SSH Certification'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(certification_tab['SSH']).click()
            time.sleep(3)
            self.Browser.find_element_by_xpath("//div[contains(text(), '%s')]" % comment).click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ssh-delete-btnWrap"]').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Delete button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Delete Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Delete Button cannot be clicked.')
            pass

    def newSSL(self, subfunction='all', comment_forCreate="sample1", comment_forImport="sample2"):
        self.utility.infoMsg('Testing plan : %s' % subfunction)
        self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
        time.sleep(0.5)
        if self.login:
            if subfunction == 'all':
                self._create_import_sslitem(function='createssl', descriptionString=comment_forCreate)
                self._create_import_sslitem(function='importssl', descriptionString=comment_forImport)
            elif subfunction == 'create':
                self._create_import_sslitem(function='createssl', descriptionString=comment_forCreate)
            elif subfunction == "import":
                self._create_import_sslitem(function='importssl', descriptionString=comment_forImport)
            else:
                print('Testing is not start yet. You send a wrong parameter.')

    def editSSL(self, replaceComment, replacePrivateKey="", replacePublicKey=""):
        self.itemname = 'Edit SSL Certification'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(
                'div[id$="-ssl-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1)').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ssl-edit-btnWrap"]').click()
            time.sleep(0.5)
            self.utility.infoMsg('Replace the comment')
            _description = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            _description.clear()
            _description.send_keys(replaceComment)
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Edit button or target cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Edit Button or target is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Edit Button or target is not visibled.')
            pass

    def detailSSL(self, comment=""):
        self.itemname = 'Details of SSL Certification'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
            time.sleep(3)
            #self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            self.Browser.find_element_by_css_selector('div[id$="-ssl-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1)').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ssl-detail-btnWrap"]').click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(4) span').click()
            time.sleep(3)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Delete button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Delete Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Delete Button cannot be clicked.')
            pass

    def deleteSSL(self, comment=""):
        self.itemname = 'Delete SSL Certification'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(certification_tab['SSL']).click()
            time.sleep(3)
            #self.Browser.find_element_by_xpath("//div[contains(text(), '%s')]" % comment).click()
            self.Browser.find_element_by_css_selector(
                'div[id$="-ssl-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1)').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector('span[id$="-ssl-delete-btnWrap"]').click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Delete button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Delete Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Delete Button cannot be clicked.')
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__":
    testingSSH = certificate()
    time.sleep(8)
    testingSSH.newSSH(subfunction='all', comment_forCreate='sample1')
    testingSSH.editSSH(comment='sample1', replaceComment='samplestring')
    testingSSH.deleteSSH(comment='samplestring')
    testingSSH.logout()

    testingSSL = certificate()
    time.sleep(8)
    testingSSL.newSSL(subfunction='create', comment_forCreate='sample1')
    testingSSL.editSSL(replaceComment='SSLsample')
    testingSSL.detailSSL()
    # testingSSL.deleteSSL()
    testingSSL.logout()


