'''
Created on Auguest 13, 2018
@Author: Philip Yang

Full Title: GRACK Connect Network

General settings, ethernet Interface, ServiceDiscovery and Firewall manager.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

_netwrok_tab ={
    'general' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'interface' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
    'serviceDiscovery' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(3) span',
    'firewall' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(4) span'
}

_interface_window ={
    'deviceList' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(1) div div div div[id^="combobox"] div div div[id$="trigger-picker"]',
    'ok' : 'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span',
    'popupOK' : 'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span',
    'ok_ver2' : "//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span",
    'ipv4Picker' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(2) div div div:nth-child(1) div div div[id$="trigger-picker"]',
    'ipv4_disabled' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ",
    'ipv4_DHCP' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ",
    'ipv4_static' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[3] ",
    'ipv6Picker' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(3) div div div:nth-child(1) div div div[id$="trigger-picker"]',
    'ipv6_disabled' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    'ipv6_DHCP' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[2] ",
    'ipv6_auto' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[3] ",
    'ipv6_static' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[4] ",
    'progressing' : ' div[class*="x-message-box"][class*="x-no-progressbar"] ',
    'bond_card1' :'div[id^="checkboxgridfield"][class*="x-form-item-body"] div div div div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr td:nth-child(1) div div',
    'bond_card2' :'div[id^="checkboxgridfield"][class*="x-form-item-body"] div div div div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(2) tbody tr td:nth-child(1) div div',
    'bond_mode' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(2) div div div div:nth-child(3) div div div[id$="trigger-picker"]',
    'bond_m_broadcast' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[4] ",
    'bond_primary' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(2) div div div div:nth-child(4) div div div[id$="trigger-picker"]',
    'bond_p_none' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[1] ",
    'bond_ipv4Picker' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(3) div div div div:nth-child(1) div div div[id$="trigger-picker"]',
    'bond_ipv6Picker' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div fieldset[class*="x-fieldset"]:nth-child(4) div div div div:nth-child(1) div div div[id$="trigger-picker"]',

}

_service_functions ={
    'applefiling' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr td[class*="x-grid-cell-checkcolumn"]',
    'ftp' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(2) tbody tr td[class*="x-grid-cell-checkcolumn"]',
    'nfs' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(3) tbody tr td[class*="x-grid-cell-checkcolumn"]',
    'rsync' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(4) tbody tr td[class*="x-grid-cell-checkcolumn"]',
    'smb_cifs' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(5) tbody tr td[class*="x-grid-cell-checkcolumn"]',
    'ssh' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(6) tbody tr td[class*="x-grid-cell-checkcolumn"]',
    'websitecontrol' : 'div[id$="-center-body"] div[class*="x-panel-body"] div:nth-child(3) div[id^="gridview"] div[class*="x-grid-item-container"] table:nth-child(7) tbody tr td[class*="x-grid-cell-checkcolumn"]',
}

_firewall_window ={
    'ok' : 'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span',
    'direction' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(2) div div div[id$="trigger-picker"]',
    'action' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(3) div div div[id$="trigger-picker"]',
    'protocol' : 'div[class*="x-window-container"] div[class*="x-window-body"] div div div div div:nth-child(8) div div div[id$="trigger-picker"]',
    'ipv4' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ",
    'ipv6' : "//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ",
    'direction_list' : "//div[contains(@class, 'x-boundlist')][2]//div//ul/li[2] ",
    'action_list' : "//div[contains(@class, 'x-boundlist')][3]//div//ul/li[3] ",
    'protocol_list'  : "//div[contains(@class, 'x-boundlist')][4]//div//ul/li[2] ",
    'popupOK' : 'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span',
    'ok_ver2' : "//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span",
}


class network():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'connect Services'
        self.common_switch_status = False
        self.login = False
        self.addinterface_flag = False
        self.common_waiting = 30

    def _login(self, tab):
        try:
            self.Browser.find_element_by_css_selector(_netwrok_tab[tab]).click()
            time.sleep(2)
            self.login = True
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error=e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error=e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error=e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
            pass

    def _catchAccept_window(self):
        try:
            # To avoid stunking in the popup window.
            # self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
            # time.sleep(0.5)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(0.5)
            return
        except Exception:
            pass

    def _catchProgress_window(self):
        try:
            # To make sure the progressing is finshed.
            self.Browser.find_element_by_css_selector(_interface_window['progressing']).is_displayed()
            self.utility.infoMsg('Progressing...')
            return True
        except:
            return False

    def _catchOKbtn(self):
        time.sleep(3)
        try:
            self.Browser.find_element_by_css_selector(_interface_window['ok']).click()
            return
        except NoSuchElementException as e1:
            self.Browser.find_element_by_xpath(_interface_window['ok_ver2']).click()
            pass
        except ElementNotVisibleException as e2:
            self.Browser.find_element_by_xpath(_interface_window['ok_ver2']).click()
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Failed to click the ok button.')
            pass
    def _catchFWOKbtn(self):
        time.sleep(3)
        try:
            self.Browser.find_element_by_css_selector(_firewall_window['ok']).click()
            return
        except NoSuchElementException as e1:
            self.Browser.find_element_by_xpath(_firewall_window['ok_ver2']).click()
            pass
        except ElementNotVisibleException as e2:
            self.Browser.find_element_by_xpath(_firewall_window['ok_ver2']).click()
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Failed to click the ok button.')
            pass

    def _addinterface(self):
        try:
            self.itemname = 'Interface: Add'
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_css_selector('span[id$="-interfaces-add-btnWrap"]').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(
                'div[id^="menu-"][class*="x-box-inner"] div[class*="x-box-target"] div:nth-child(1) a span').click()
            time.sleep(5)
            self.utility.infoMsg('To pick up an ethernet card.')
            self.Browser.find_element_by_css_selector(_interface_window['deviceList']).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click()
            time.sleep(0.5)

            self.utility.infoMsg('Replace comment')
            note = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            note.clear()
            note.send_keys('samplestring')
            time.sleep(0.5)

            self.utility.infoMsg('Replace DNS')
            dns = self.Browser.find_element_by_css_selector('input[name*="dnsnameserver"]')
            dns.clear()
            dns.send_keys('8.8.8.8')
            time.sleep(0.5)

            self.utility.infoMsg('Replace DNS Search')
            dns = self.Browser.find_element_by_css_selector('input[name*="dnssearch"]')
            dns.clear()
            dns.send_keys('google')
            time.sleep(0.5)

            self.utility.infoMsg('Replace NTU')
            dns = self.Browser.find_element_by_css_selector('input[name*="mtu"]')
            dns.clear()
            dns.send_keys('4')
            time.sleep(0.5)

            self._catchOKbtn()
            time.sleep(5)
            progressing = self._catchProgress_window()
            while progressing:
                time.sleep(self.common_waiting)
                progressing = self._catchProgress_window()

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Exception just happened.')
            pass

    def _editinterface(self, ethernetCard="eth1"):
        try:
            self.itemname = 'Interface: Edit'
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % ethernetCard).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('span[id$="-interfaces-edit-btnWrap"]').click()
            time.sleep(2)

            self.utility.infoMsg('Replace comment')
            note = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            note.clear()
            note.send_keys('samplestring')
            time.sleep(0.5)

            self.utility.infoMsg('Pick IPv4')
            self.Browser.find_element_by_css_selector(_interface_window['ipv4Picker']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_interface_window['ipv4_DHCP']).click()
            time.sleep(2)

            self.utility.infoMsg('Pick IPv6')
            self.Browser.find_element_by_css_selector(_interface_window['ipv6Picker']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_interface_window['ipv6_DHCP']).click()
            time.sleep(2)

            self._catchOKbtn()
            time.sleep(5)
            progressing = self._catchProgress_window()
            while progressing:
                time.sleep(self.common_waiting)
                progressing = self._catchProgress_window()

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: Exception just happened.')
            pass

    def _bondinterface(self):
        try:
            self.itemname = 'Interface: Add bond'
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_css_selector('span[id$="-interfaces-add-btnWrap"]').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(
                'div[id^="menu-"][class*="x-box-inner"] div[class*="x-box-target"] div:nth-child(2) a span').click()
            time.sleep(5)

            self.utility.infoMsg('Replace comment')
            note = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            note.clear()
            note.send_keys('samplestring')
            time.sleep(0.5)

            self.utility.infoMsg('To pick up 2 cards to combine.')
            self.Browser.find_element_by_css_selector(_interface_window['bond_card1']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(_interface_window['bond_card2']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Pick mode')
            self.Browser.find_element_by_css_selector(_interface_window['bond_mode']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_interface_window['bond_m_broadcast']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Pick primary')
            self.Browser.find_element_by_css_selector(_interface_window['bond_primary']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_interface_window['bond_p_none']).click()
            time.sleep(0.5)

            self._catchOKbtn()
            time.sleep(5)
            progressing = self._catchProgress_window()
            while progressing:
                time.sleep(self.common_waiting)
                progressing = self._catchProgress_window()

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD INTERFACE: Exception just happened.')
            pass

    def _editboundinterface(self, ethernetCard="bond1"):
        try:
            self.itemname = 'Interface: Edit bond'
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % ethernetCard).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('span[id$="-interfaces-edit-btnWrap"]').click()
            time.sleep(2)

            self.utility.infoMsg('Replace comment')
            note = self.Browser.find_element_by_css_selector('input[name*="comment"]')
            note.clear()
            note.send_keys('bondinterface')
            time.sleep(0.5)

            self.utility.infoMsg('Replace MII')
            mii = self.Browser.find_element_by_css_selector('input[name*="bondmiimon"]')
            mii.clear()
            mii.send_keys('200')

            self.utility.infoMsg('Replace down delay')
            downdelay = self.Browser.find_element_by_css_selector('input[name*="bonddowndelay"]')
            downdelay.clear()
            downdelay.send_keys('300')

            self.utility.infoMsg('Pick IPv4')
            self.Browser.find_element_by_css_selector(_interface_window['bond_ipv4Picker']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_interface_window['ipv4_DHCP']).click()
            time.sleep(2)

            self.utility.infoMsg('Pick IPv6')
            self.Browser.find_element_by_css_selector(_interface_window['bond_ipv6Picker']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_interface_window['ipv6_DHCP']).click()
            time.sleep(2)

            self._catchOKbtn()
            time.sleep(5)
            progressing = self._catchProgress_window()
            while progressing:
                time.sleep(self.common_waiting)
                progressing = self._catchProgress_window()

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='EDIT INTERFACE: Exception just happened.')
            pass

    def _flashinterface(self, ethernetCard="eth1"):
        try:
            self.itemname = 'Interface: Flash'
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % ethernetCard).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('span[id$="-interfaces-identify-btnWrap"]').click()
            time.sleep(2)

            # Flash
            flashtiming = self.Browser.find_element_by_css_selector('input[name*="seconds"]')
            flashtiming.clear()
            flashtiming.send_keys('10')

            self.utility.infoMsg('Click Start for the flashing testing')
            self.Browser.find_element_by_css_selector(_interface_window['ok']).click()
            time.sleep(self.common_waiting)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='FLASH INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='FLASH INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='FLASH INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='FLASH INTERFACE: Exception just happened.')
            pass

    def _deleteinterface(self, unittestname, ethernetCard="eth1"):
        try:
            self.itemname = unittestname
            self.utility.unittest(test_item_name=self.itemname)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % ethernetCard).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('span[id$="-interfaces-delete-btnWrap"]').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(_interface_window['popupOK']).click()
            time.sleep(self.common_waiting)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='DELETE INTERFACE: Exception just happened.')
            pass

    def _addfirewall(self, commentstring = 'sample', ip='v4'):
        """ _addfirewall: To add ipv4 firewall configure """
        self.itemname = 'ADD FIREWALL: ' + commentstring
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Add firewall under: %s' % ip)
            self.Browser.find_element_by_css_selector('div[id$="-firewall-family-trigger-picker"]').click()
            time.sleep(2)
            if ip =='v4':
                self.Browser.find_element_by_xpath(_firewall_window['ipv4']).click()
            elif ip == 'v6':
                self.Browser.find_element_by_xpath(_firewall_window['ipv6']).click()
            else:
                self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: You have send a wrong protocol. We')
                return
            time.sleep(0.5)

            self.utility.infoMsg('Click add button.')
            self.Browser.find_element_by_css_selector('span[id*="-firewall-add-btnWrap"]').click()
            time.sleep(2)

            self.utility.infoMsg('Replace source')
            source = self.Browser.find_element_by_css_selector('input[name*="source"]')
            source.clear()
            if ip == 'v4':
                source.send_keys('192.168.11.2')
            elif ip == 'v6':
                source.send_keys('fe08::dcba')
            time.sleep(0.5)

            self.utility.infoMsg('Replace source port')
            sport = self.Browser.find_element_by_css_selector('input[name*="sport"]')
            sport.clear()
            sport.send_keys('21')
            time.sleep(0.5)

            self.utility.infoMsg('Replace desination')
            destination = self.Browser.find_element_by_css_selector('input[name*="destination"]')
            destination.clear()
            if ip == 'v4':
                destination.send_keys('192.168.11.3-192.168.11.254')
            elif ip =='v6':
                destination.send_keys('fe08::abcd')
            time.sleep(0.5)

            self.utility.infoMsg('Replace desination port')
            dport = self.Browser.find_element_by_css_selector('input[name*="dport"]')
            dport.clear()
            dport.send_keys('21')
            time.sleep(0.5)

            self.utility.infoMsg('Replace comment')
            dport = self.Browser.find_element_by_css_selector('textarea[name*="comment"]')
            dport.clear()
            dport.send_keys(commentstring)
            time.sleep(0.5)

            self._catchFWOKbtn()
            time.sleep(5)
            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector('span[id*="-firewall-apply-btnWrap"]').click()
            time.sleep(2)
            self.utility.pass_test(testname=self.itemname)

        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: Exception just happened.')
            pass

    def _editfirewall(self, keyword='sample'):
        self.itemname = 'EDIT FIREWALL'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % keyword).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('span[id$="-firewall-edit-btnWrap"]').click()
            time.sleep(2)

            self.utility.infoMsg('Change direction mode')
            self.Browser.find_element_by_css_selector(_firewall_window['direction']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_firewall_window['direction_list']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Change action mode')
            self.Browser.find_element_by_css_selector(_firewall_window['action']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_firewall_window['action_list']).click()
            time.sleep(0.5)

            self.utility.infoMsg('Change protocol mode')
            self.Browser.find_element_by_css_selector(_firewall_window['protocol']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(_firewall_window['protocol_list']).click()
            time.sleep(0.5)

            self._catchFWOKbtn()
            time.sleep(5)
            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector('span[id*="-firewall-apply-btnWrap"]').click()
            time.sleep(2)
            self.utility.pass_test(testname=self.itemname)

        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='EDIT FIREWALL: Unselectable elemnt ' + e0.msg)
            pass

        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='EDIT FIREWALL: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='EDIT FIREWALL: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='EDIT FIREWALL: Exception just happened.')
            pass

    def _updownfirewall(self, keyword,behavior='all'):
        self.itemname = 'UP DOWN FIREWALL CONFIGURE'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % keyword).click()
            time.sleep(2)
            if behavior == 'all':
                self.utility.infoMsg('Item up...')
                self.Browser.find_element_by_css_selector('span[id$="-firewall-up-btnWrap"]').click()
                time.sleep(2)
                self.utility.infoMsg('Item down...')
                self.Browser.find_element_by_css_selector('span[id$="-firewall-down-btnWrap"]').click()
                self.utility.pass_test(testname=self.itemname)
            elif behavior == 'up':
                self.Browser.find_element_by_css_selector('span[id$="-firewall-up-btnWrap"]').click()
                time.sleep(2)
                self.utility.pass_test(testname=self.itemname)
            elif behavior == 'down':
                self.Browser.find_element_by_css_selector('span[id$="-firewall-up-btnWrap"]').click()
                time.sleep(2)
                self.utility.pass_test(testname=self.itemname)
            else:
                self.utility.warn_test(testname=self.itemname, warning='UP DOWN FIREWALL: You have send a unrecognized behavior.' )
            return

        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='UP DOWN FIREWALL: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='UP DOWN FIREWALL: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='UP DOWN FIREWALL: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='UP DOWN FIREWALL: Exception just happened.')
            pass

    def _deletefirewall(self, keyword='sample'):
        self.itemname = 'DELETE FIREWALL: ' + keyword
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % keyword).click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('span[id$="-firewall-delete-btnWrap"]').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(_firewall_window['popupOK']).click()
            time.sleep(2)
            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector('span[id*="-firewall-apply-btnWrap"]').click()
            time.sleep(2)
            self.utility.pass_test(testname=self.itemname)

        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='ADD FIREWALL: Exception just happened.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def network_general(self):
        self.itemname = 'General'
        self.utility.unittest(test_item_name=self.itemname)
        self._login(tab='general')

        if self.login:
            try:
                deviceName = self.Browser.find_element_by_css_selector('input[name*="hostname"]')
                deviceName.clear()
                self.utility.infoMsg('To replace hostname')
                deviceName.send_keys('grack14')
                time.sleep(2)
                deviceName = self.Browser.find_element_by_css_selector('input[name*="domainname"]')
                deviceName.clear()
                self.utility.infoMsg('To replace domainname')
                deviceName.send_keys('domainSample')
                time.sleep(2)
                self.utility.infoMsg('Saving')
                self.Browser.find_element_by_css_selector('span[id$="general-ok-btnWrap"]').click()
                time.sleep(2)
                self._catchAccept_window()
                time.sleep(30)
                self.utility.pass_test(testname=self.itemname)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='No such element '+e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname, error='Element is invisibled '+e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Exception just happened.')
                pass

    def network_interface(self, VM_flag=True):
        self._login(tab='interface')
        self.addinterface_flag = False
        if self.login:
            if VM_flag:
                self.utility.infoMsg("We would ignored adding/flashing interface testing in VM.")
            else:
                self._addinterface()
                self._flashinterface(ethernetCard='eth2')
            self._editinterface(ethernetCard='eth2')
            self._deleteinterface(unittestname="Interface : Delete", ethernetCard='eth2')

    def network_interface_bond(self, VM_flag=True):
        self._login(tab='interface')
        self.addinterface_flag = False
        if self.login:
            if VM_flag:
                self.utility.infoMsg("We would ignored adding interface testing in VM.")
            else:
                self._bondinterface()
            self._editboundinterface(ethernetCard='bond0')
            self._deleteinterface(unittestname="Interface : Delete bond", ethernetCard='bond0')

    def network_servicediscovery(self):
        self.itemname = 'Service Discovery'
        self.utility.unittest(test_item_name=self.itemname)
        self._login(tab='serviceDiscovery')
        if self.login:
            for switch in _service_functions:
                try:
                    self.utility.infoMsg('Click switch %s' % switch)
                    self.Browser.find_element_by_css_selector(_service_functions[switch]).click()
                    time.sleep(0.5)
                except InvalidSelectorException as e0:
                    self.utility.fail_test(testname=self.itemname, error='Swtich Control: Unselectable elemnt ' + e0.msg)
                    pass
                except NoSuchElementException as e1:
                    self.utility.fail_test(testname=self.itemname, error='Swtich Control: No such element ' + e1.msg)
                    pass
                except ElementNotVisibleException as e2:
                    self.utility.fail_test(testname=self.itemname, error='Swtich Control: Element is invisibled ' + e2.msg)
                    pass
                except Exception:
                    self.utility.fail_test(testname=self.itemname, error='Swtich Control: Exception just happened.')
                    pass
            time.sleep(0.5)
            try:
                self.utility.infoMsg('Refresh the switch button.')
                self.Browser.find_element_by_css_selector('span[id$="zeroconf-refresh-btnWrap"]').click()
                time.sleep(2)
                self.Browser.find_element_by_css_selector(_service_functions['applefiling']).click()
                time.sleep(0.5)
                self.utility.infoMsg('Saving...')
                self.Browser.find_element_by_css_selector('span[id$="zeroconf-apply-btnWrap"]').click()
                time.sleep(2)
                self.utility.pass_test(testname=self.itemname)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Save/Refresh: Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='Save/Refresh: No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname, error='Save/Refresh: Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Save/Refresh: Exception just happened.')
                pass

    def network_firewall(self):
        self._login(tab='firewall')
        self.addinterface_flag = False
        if self.login:
            time.sleep(5)
            self._addfirewall(commentstring='sample',ip='v4')
            time.sleep(5)
            self._addfirewall(commentstring='sample2',ip='v4')
            time.sleep(5)
            self._updownfirewall(keyword='sample2')
            time.sleep(5)
            self._deletefirewall(keyword='sample2')
            time.sleep(5)
            self._editfirewall()
            time.sleep(5)
            self._deletefirewall()


if __name__ == "__main__":
    testing = network()
    testing.network_general()
    testing.network_interface(VM_flag=False)
    testing.network_interface_bond(VM_flag=False)
    testing.network_servicediscovery()
    testing.network_firewall()
    time.sleep(8)
    testing._logout()


