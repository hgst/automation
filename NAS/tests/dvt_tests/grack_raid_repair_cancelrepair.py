'''
Created on June 21, 2018
@Author: Philip Yang

Full Title: GRACK RAID Repair and cancelRepair
To select a disk to repair or cancel rapair.
Cancelrepair behavior which button would be clickable when repair progress running after a while.

'''
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
from selenium.common.exceptions import WebDriverException
import time
import device_info


class grack_raid_repair():

    def __init__(self):
        self.itemname = 'GRACK repair testing'
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.repairStatus = False # false: still checking, True: checking is finished.
        self.pickup = False

    def _pickupraid(self):
        time.sleep(2)
        try:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table[class*="x-grid-item-selected"]:nth-child(1)').is_displayed()
            self.pickup = True
        except:
            pass

    def repair(self, cancel_testing=True, unittestname= 'GRACK repair testing'):
        ''' cancelRepair is provided to test the cancel feature, but which also depend a lot dataset in the RAID '''
        self.itemname = unittestname
        self.utility.unittest(self.itemname)
        try:
            self.utility.infoMsg('*** Repair and cancelrapair testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(30)
            # Select RAID
            self.utility.infoMsg('Pick up a RAID...')
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)

            self._pickupraid()
            while self.pickup is False:
                self.utility.infoMsg('...click RAID again.')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
                time.sleep(3)
                self._pickupraid()

            # Click repair button
            self.Browser.find_element_by_css_selector('span[id$="-repair-btnWrap"]').click()
            time.sleep(8)

            # Click OK button --> checking ...
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(10)

            # To check repairing progress
            self.catchMsgbox()
            while self.repairStatus == False:
                    time.sleep(5)
                    self.catchMsgbox()

            # Click OK button : still chekcing or checking is done.
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(0.5)

            # Trying to cancel the checking behavior which feature coule be ignored.
            if cancel_testing:
                self.utility.infoMsg('Trying to cancel checking:')
                try:
                    self.Browser.find_element_by_css_selector(
                        'a[componentid$="cancelrepair"][class*="x-item-disabled"]')
                    time.sleep(0.5)
                    self.utility.infoMsg('Cancel Button is not clickable.')
                except WebDriverException:
                    self.utility.infoMsg('Cancel button is clickable.')
                    time.sleep(0.5)
                    self.clickCancelBtn()
                    pass

            self.utility.pass_test()
            time.sleep(5)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Repair testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Repair testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Repair testing: Exception has happened. ' + e.message)


    def clickCancelBtn(self):

        try:
            self.Browser.find_element_by_css_selector('span[id$="-cancelrepair-btnWrap"]').click()
            time.sleep(10)
            self.utility.infoMsg('Canceling...')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(0.5)
            self.utility.infoMsg('Checking has been stopped.')
        except InvalidSelectorException:
            self.utility.infoMsg('Cancel button is unclickable.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Failed to cancel the checking.')
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Failed to determine the checking status.' + e.message)



    def catchMsgbox(self):
        try:
            self.utility.infoMsg('Catching OK button...')
            time.sleep(3)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').is_displayed()
            self.repairStatus = True
            return
        except NoSuchElementException:
            self.repairStatus = False
            self.utility.infoMsg('Still checking...')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Failed to determine the checking status.' + e.message)


    def logout(self):
        self.utility.web_logout()
        time.sleep(3)
        self.utility.get_test_results()
        time.sleep(10)
        self.utility.web_logout()

if __name__ == "__main__":
    testing = grack_raid_repair()
    testing.repair(unittestname = 'Including Cancel Repair')
    #testing.repair(raidname='test', unittestname = 'Including Cancel Repair')
    time.sleep(5)
    testing.logout()
