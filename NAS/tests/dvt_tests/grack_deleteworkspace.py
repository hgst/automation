'''
    Created on May 14th, 2018
    @Author: Philip.Yang

    Full Title: Delete Work Space
    Open broswer and goto connect of tab, Click Storage >> RAID
    Determine SSH/TCP tunnel switch status then do the setup behavior.

'''
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


class deleteworkspace():


    def __init__(self):
        self.itemname = 'Delete WorkSpace'
        self.utility = device_info.device_info()  # Instance
        self.browser = self.utility.browser  # Call web browser
    def delete(self, workspacename=None):
        self.utility.unittest(self.itemname)  # Register testname


        if(workspacename):
            self.utility.web_login(tabname='workspaces')
            time.sleep(20)
            # Select workspace
            try:
                self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workspacename).click()
            except InvalidSelectorException:
                self.utility.warn_test('Your workspace is not selectable')
                pass
            except NoSuchElementException:
                self.utility.warnMsg('Your workspace is no exist. Delete workapce is stop')
                pass
            try_to_delete = self._delete_workspace()

            # if delete is failed then try to unmount the workspace.
            if(try_to_delete is False):
                self.utility.get_test_results()

        else:
            self.utility.warnMsg('Please provide the workspace name.')
            self.utility.get_test_results()

    def _delete_workspace(self):
        ''' click delete button '''
        try:
            self.browser.find_element_by_css_selector('span[id$="delete-btnWrap"]').click()
            time.sleep(3)
            self.browser.find_element_by_css_selector('a[class*="btn-focus"]').click()
            time.sleep(8)
            self.utility.pass_test('Delete workspace is done.')
            time.sleep(3)
            self.utility.web_logout()
            return True
        except InvalidSelectorException:
            self.utility.warn_test('Delete is unselectalbe.')
            return False
        except NoSuchElementException:
            self.utility.warn_test('Unable to locate element. Perhaps your workspace is not exist.')
            return False

    def _unmount_workspace(self):
        ''' click unmount button '''
        try:
            self.browser.find_element_by_css_selector('span[id$="unmount-btnWrap"]').click()
            time.sleep(3)
            self.browser.find_element_by_css_selector('a[class*="btn-focus"]').click()
            time.sleep(8)
            self.utility.pass_test('Unmount workspace is done.')
            return True
        except InvalidSelectorException:
            self.utility.warn_test('Unmount is unselectalbe.')
            return False
        except NoSuchElementException:
            self.utility.warn_test('Unable to locate element. Perhaps your workspace is not exist.')
            return False

if __name__ == "__main__" :
    deleteworkspace().delete(workspacename='workspace_backeup')

