'''
    Created on May 24, 2018
    @Author: Philip.Yang

    Full Title: To delete SMB share.
    @grack_deleteSMB

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException

import time
import device_info




class setupSMB():
    def __init__(self):
        self.itemname = 'Delete SMB/CIFS workspace'

        self.utility = device_info.device_info()
        self.browser = self.utility.browser
        self.utility.unittest(self.itemname)
        self.utility.web_login(tabname='connect')
        # SMB page
        self.browser.find_element_by_css_selector(
            'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(4)').click()
        time.sleep(3)


    def deleteSMB(self, workSpace):
        # Findout
        findshare = False
        try:
            # Find workspace
            self.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % workSpace).click()
            time.sleep(0.5)
            findshare = True
        except NoSuchElementException as e1:
            self.utility.errorMsg('Cannot find out the workspace: %s' % workSpace)
            self.utility.fail_test(testname=self.itemname, error=e1.msg)
        except InvalidSelectorException as e2:
            self.utility.errorMsg('The workspace %s cannot be selected.' % workSpace)
            self.utility.fail_test(testname=self.itemname, error=e2.msg)
        except Exception as e:
            self.utility.fail_test(testname=self.itemname, error=e.message)

        if(findshare):
            # Delete
            try:
                # Click delete button
                self.browser.find_element_by_css_selector('span[id$="-shares-delete-btnWrap"]').click()

                # Click Yes button
                self.browser.find_element_by_css_selector('div[class*="x-message-box"] div:nth-child(3) div div a:nth-child(2)').click()
                time.sleep(8)
                #self.utility.pass_test(testname=self.itemname, result='share workapsce: %s has been remove.' %workSpace)
            except NoSuchElementException as e1:
                self.utility.errorMsg('Cannot delete button cause we cannot find the pop window.')
                self.utility.fail_test(testname=self.itemname, error=e1.msg)
            except InvalidSelectorException as e2:
                self.utility.errorMsg('The button cannot be clicked.')
                self.utility.fail_test(testname=self.itemname, error=e2.msg)
            except Exception as e:
                self.utility.fail_test(testname=self.itemname, error=e.message)

            # Check
            try:
                popup_msg= self.browser.find_element_by_css_selector(
                    'div[class*="x-window-body-default-resizable"]:nth-child(2)')
                self.utility.errorMsg('Detecting an Error message: %s' % popup_msg.text)
                #Accept Error msg
                self.browser.find_element_by_css_selector('div[class*="x-toolbar-footer"] div[id^="toolbar"] a[class*="x-btn"]:nth-child(1) ').click()
                self.utility.warn_test(testname=self.itemname, warning= popup_msg)
            except NoSuchElementException as e1:
                self.utility.pass_test(testname=self.itemname, result='share workapsce: %s has been remove.' % workSpace)
            except InvalidSelectorException as e2:
                self.utility.errorMsg('The button cannot be clicked.')
                self.utility.fail_test(testname=self.itemname, error=e2.msg)
            except Exception as e:
                self.utility.fail_test(testname=self.itemname, error=e.message)


        self.utility.get_test_results()
        self.utility.web_logout()




if __name__ == "__main__" :
    setupSMB().deleteSMB(workSpace='test-workspace')
