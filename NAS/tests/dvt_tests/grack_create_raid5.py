'''
    Created on May 7th, 2018
    @Author: Philip.Yang

    Full Title: Create RAID 5 (based on FW 1.0.9)
    Open broswer and goto connect of tab, Click Storage >> RAID
    Determine SSH/TCP tunnel switch status then do the setup behavior.

    For modulized, you can set DoNotDelete to determine the RAID exist or not after creating.
'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

class createRAID5():
    def __init__(self):
        self.itemname = 'RAID5 testing'
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.step1 = False  # Create Volume
        self.step2 = False  # Unmount Volume
        self.holding = False

    def create_volume(self, raidname='raid5sample', DoNotDelete=False):
        self.itemname = 'Create RAID5'
        self.utility.unittest(self.itemname)
        self.utility.infoMsg('Prepare to create RAID5: %s' %raidname)
        self.utility.infoMsg('Don\'t Delete RAID volume after testing: %s' %DoNotDelete)
        self.utility.web_login(tabname='storage')


        try:
            self.Browser.find_element_by_css_selector('div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(8)
            self.utility.infoMsg('Get into RAID page.')
            # Click create button
            self.Browser.find_element_by_css_selector('span[id$="-create-btnWrap"]').click()
            time.sleep(8)
            name = self.Browser.find_element_by_css_selector('input[id^="textfield-"]')
            name.send_keys(raidname)
            time.sleep(20)
            self.utility.infoMsg('Pickup devices.')
            self.Browser.find_element_by_css_selector('table:nth-child(1) div[class*="x-grid-row-checker"]').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('table:nth-child(2) div[class*="x-grid-row-checker"]').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('table:nth-child(3) div[class*="x-grid-row-checker"]').click()
            time.sleep(5)
            self.utility.infoMsg('Save the volume change')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
            time.sleep(8)
            self.Browser.find_element_by_css_selector('div[id$="toolbar-targetEl"] > a[class*="x-focus"]').click()
            time.sleep(8)

            self.holding = False
            self._waiting()
            while self.holding is True:
                self._waiting()
            self._apply_check()

        except ElementNotVisibleException as e2:
            self.utility.errorMsg('Error' + e2.msg)
            self.utility.fail_test(self.itemname, e2.msg)
            pass
        except NoSuchElementException as e3:
            self.utility.errorMsg('Error, Element is not exist.' + e3.msg)
            self.utility.fail_test(self.itemname, e3.msg)
            pass
        except Exception as e:
            self.utility.errorMsg('Error' + e.message)
            self.utility.fail_test(self.itemname, e.message)
            pass

        # Try to catch error message dialogue.
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-message-box-error"]')
            self.utility.infoMsg('Catching error dialogue.')
            self.Browser.find_element_by_css_selector('span[class*="x-btn-inner-default-small"]').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                    'div[id$="toolbar-targetEl"] > a[class*="x-btn"]:nth-child(3) > span[id$="-btnWrap"] ').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('span[id$="cancel-btnWrap"]').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('div[id$="toolbar-targetEl"] > a[class*="x-focus"]').click()
            time.sleep(8)
        except NoSuchElementException:
            self.step1 = True
            time.sleep(25)
            pass
        except Exception as e:
            self.utility.fail_test(self.itemname, e.message)
            pass

        self._check_raid_exist(raid=raidname)
        if self.step1 == False:
            self.utility.infoMsg('To double check the RAID exist or not.')
            self._refresh_page()
            self._check_raid_exist(raid=raidname)

        try:
            # To avoid stunking in the popup window.
            self.Browser.find_element_by_css_selector('span[id$="cancel-btnWrap"]').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[id$="-toolbar"] div div a:nth-child(2) span').click()
        except Exception:
            pass

        if(self.step1):
            if DoNotDelete is not True:
                self.unmount(item=raidname)
        else:
            self.utility.infoMsg('Unmount is canceled.')

        if (self.step2):
            self.delete(item=raidname)
        else:
            self.utility.infoMsg('Delete is canceled.')

    def unmount(self, item):
        self.itemname = 'Unmount RAID'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            # Click RAID5 and remove
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % item).click()
            time.sleep(5)
            # Unmount
            self.Browser.find_element_by_css_selector('span[id$="-unmount-btnWrap"]').click()
            time.sleep(10)
            self.utility.infoMsg('Unmount has finished.')
            self.step2 = True
            self.utility.pass_test(self.itemname)
        except ElementNotVisibleException as e2:
            self.utility.errorMsg('Unmount Error ' + e2.msg)
            self.utility.fail_test(self.itemname, e2.msg)
            pass
        except NoSuchElementException as e3:
            self.utility.errorMsg('Error, Element is not exist.' + e3.msg)
            self.utility.fail_test(self.itemname, e3.msg)
            pass
        except Exception as e:
            self.utility.errorMsg('Unmount Error ' + e.message)
            self.utility.fail_test(self.itemname, e.message)
            pass

    def delete(self, item):
        self.itemname='Delete RAID'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            # Click RAID5 and remove
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % item).click()
            time.sleep(5)
            # Delete
            self.utility.infoMsg('Prepare to delete RAID5 ')
            self.Browser.find_element_by_css_selector('span[id$="-delete-btnWrap"]').click()
            time.sleep(10)
            self.Browser.find_element_by_css_selector(
                'div[id$="toolbar-targetEl"] > a[class*="x-btn"]:nth-child(2)').click()
            time.sleep(15)
            self.utility.infoMsg('Delete devices has finished')
            self.utility.pass_test(self.itemname)
        except ElementNotVisibleException as e2:
            self.utility.errorMsg('Delete Error ' + e2.msg)
            self.utility.fail_test(self.itemname, e2.msg)
            pass
        except NoSuchElementException as e3:
            self.utility.errorMsg('Error, Element is not exist.' + e3.msg)
            self.utility.fail_test(self.itemname, e3.msg)
            pass
        except Exception as e:
            self.utility.errorMsg('Delete Error ' + e.message)
            self.utility.fail_test(self.itemname, e.message)
            pass

    def _waiting(self):
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-window-container"] div[class*="x-mask"] div div div[class*="x-mask-msg-text"]').is_displayed()
            self.utility.infoMsg('Saving')
            self.holding = True
            time.sleep(5)
            return
        except:
            self.holding = False
            pass

    def _apply_check(self):
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-window-text"]').is_displayed()
            self.utility.infoMsg('Apply...')
            time.sleep(30)
            return
        except:
            pass

    def _refresh_page(self):
        try:
            self.utility.infoMsg('Refresh the page...')
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span').click()
            time.sleep(10)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(30)
            return
        except:
            pass

    def _check_raid_exist(self,raid):
        try:
            self.utility.infoMsg('Checking the item exist or not')
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raid).is_displayed()
            time.sleep(5)
            self.utility.pass_test(testname=self.itemname, result='RAID5 item has been created.')
            return
        except NoSuchElementException as e:
            self.utility.fail_test(testname=self.itemname, error='RAID5 item is not exist.')
            self.step1 = False
            pass

    def logout(self):
        self.utility.web_logout()
        time.sleep(5)
        self.utility.get_test_results()


if __name__ == "__main__" :
    raid_verify = createRAID5()
    raid_verify.create_volume(raidname='sample', DoNotDelete=True)
    time.sleep(5)
    raid_verify.logout()

    raid_verify2 = createRAID5()
    raid_verify2.create_volume(raidname='sample3', DoNotDelete=True)
    time.sleep(5)
    raid_verify2.logout()