'''
Created on Auguest 24, 2018
@Author: Philip Yang

Full Title: GRACK System: General Settings

Website manager, Admin password recovery  and date&time

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

_generalsettings_tab ={
    'web_manager' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'web_admin' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
    'date_time' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(3) span',
}

_webmanager = {
    'ssl_tls' : 'div[id$="-settings-body"] div div fieldset:nth-child(2) div[class*="x-fieldset-body"] div div div:nth-child(1) div div input ',
    'pick_certificate' : 'div[id$="-settings-body"] div div fieldset:nth-child(2) div[class*="x-fieldset-body"] div div div:nth-child(2) div div div[id$="trigger-picker"] ',
    'force_ssl_tls' : 'div[id$="-settings-body"] div div fieldset:nth-child(2) div[class*="x-fieldset-body"] div div div:nth-child(4) div div input ',

}
_button_status ={
    'ssl_tls' : 'div[id$="-settings-body"] div div fieldset:nth-child(2) div[class*="x-fieldset-body"] div div div[class*="x-form-cb-checked"]:nth-child(1)  ',
    'force_ssl_tls' : 'div[id$="-settings-body"] div div fieldset:nth-child(2) div[class*="x-fieldset-body"] div div div[class*="x-form-cb-checked"]:nth-child(4)  ',
    'invalid' : 'input[id^="sslcertificatecombo"][class*="x-form-invalid-field"]'
}

_recovery_tag_position = {
    'password' : 'input[name*="password"]',
    'passwordconf' : 'input[name*="passwordconf"]',
    'question1' : 'input[name*="question1"]',
    'question2' : 'input[name*="question2"]',
    'question3' : 'input[name*="question3"]',
    'answer1' : 'input[name*="answer1"]',
    'answer2' : 'input[name*="answer2"]',
    'answer3' : 'input[name*="answer3"]',
}

_recovery_tag_value = {
    'password' : 'gtech',
    'passwordconf' : 'gtech',
    'question1' : 'aaa',
    'question2' : 'bbb',
    'question3' : 'ccc',
    'answer1' : '111',
    'answer2' : '222',
    'answer3' : '333',
}

_datetime ={
    'timezone' : 'div[id$="-time-body"] div div fieldset:nth-child(2) div div div div:nth-child(1) div div div[id$="trigger-picker"]',
    'ntpposition' : 'div[id$="-time-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(2)',
    'ntpbtn' : 'div[id$="-time-body"] div div fieldset:nth-child(2) div div div div:nth-child(2) div div input[id^="checkbox"]'
}

class sys_GeneralSettings():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'System General Settings'
        self.login = False
        self._invalid = False


    def _loginTab(self,tab):
        # Click GeneralSettings tab button
        try:
            self.Browser.find_element_by_css_selector(_generalsettings_tab[tab]).click()
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='GeneralSettings tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='GeneralSettings tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('GeneralSettings tab button cannot be clicked.')
            pass

    def _checkboxstatus(self,tab, next_status):
        # Web Manager
        _switch = False
        try:
            self.Browser.find_element_by_css_selector(_button_status[tab]).is_displayed()
            _switch = True
        except NoSuchElementException:
            pass
        except ElementNotVisibleException:
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='CheckBox: Exception just happened.')
            pass
        if _switch == next_status:
            return
        else:
            self.utility.infoMsg('Changeing the checkbox status.')
            self.Browser.find_element_by_css_selector(_webmanager[tab]).click()
            return

    def _checkTimeZonecheckboxstatus(self, next_status):
        _switch = False
        try:
            self.Browser.find_element_by_css_selector(_datetime['ntpposition']).is_displayed()
            _switch = True
        except NoSuchElementException:
            pass
        except ElementNotVisibleException:
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='CheckBox: Exception just happened.')
            pass
        if _switch == next_status:
            return
        else:
            self.utility.infoMsg('Changeing the checkbox status.')
            self.Browser.find_element_by_css_selector(_datetime['ntpbtn']).click()
            return

    def _checkInvalid(self):
        try:
            self.Browser.find_element_by_css_selector(_button_status['invalid']).is_displayed()
            self._invalid = True
            return
        except NoSuchElementException:
            self._invalid = False
            pass
        except ElementNotVisibleException:
            self._invalid = False
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Invalid Message is gone.')
            return

    def _popWindow(self):
        try:
            self.utility.infoMsg('Apply...')
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(8)
            return
        except Exception:
            self.utility.infoMsg('Apply Message is gone.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def webManager(self, ssl_tls=True, force_ssl_tls=True):
        self.itemname='System Gerenal Settings: Web Manager'
        self.utility.unittest(test_item_name=self.itemname)

        self._loginTab(tab='web_manager')
        try:
            # Auto logout
            timeout = self.Browser.find_element_by_css_selector('input[name*="timeout"]')
            timeout.clear()
            timeout.send_keys('0')
            time.sleep(3)
            self.utility.infoMsg('Checking SSL and TLS status')
            self._checkboxstatus(tab='ssl_tls', next_status=ssl_tls)
            time.sleep(2)

            if ssl_tls == True:
                self.utility.infoMsg('Pick certificate')
                self.Browser.find_element_by_css_selector(_webmanager['pick_certificate']).click()
                time.sleep(3)
                self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] ul li:nth-child(2)').click()
                time.sleep(3)

                self.utility.infoMsg('Checking Force SSL and TLS status')
                self._checkboxstatus(tab='force_ssl_tls', next_status=force_ssl_tls)
                time.sleep(2)

            self._checkInvalid()
            if self._invalid:
                self.utility.infoMsg('Invalid event is happening. Try to reset the status.')
                self.Browser.find_element_by_css_selector('span[id$="-settings-reset-btnWrap"]').click()
            else:
                self.utility.infoMsg('Saving')
                self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
                time.sleep(8)
                self._popWindow()

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Web Manager: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Web Manager: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='Web Manager: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Web Manager: Exception just happened.')
            pass

    def adminpwdRecovery(self):
        self.itemname = 'System Gerenal Settings: Admin Password Recovery'
        self.utility.unittest(test_item_name=self.itemname)

        self._loginTab(tab='web_admin')

        for tagname in _recovery_tag_position:

            self.utility.infoMsg('Replace the tag: %s' % tagname)
            try:
                _tmp = self.Browser.find_element_by_css_selector(_recovery_tag_position[tagname])
                _tmp.clear()
                _tmp.send_keys(_recovery_tag_value[tagname])
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: Exception just happened.')
                pass

        try:
            self.utility.infoMsg('Saving')
            self.Browser.find_element_by_css_selector('span[id$="-adminpasswd-ok-btnWrap"]').click()
            time.sleep(8)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Admin Password Recovery: Exception just happened.')
            pass

    def date_time(self, timezone=276, ntpSwitch=True, ntpserver='pool.ntp.org'):
        self.itemname = 'System Gerenal Settings: Date and Time'
        self.utility.unittest(test_item_name=self.itemname)

        self._loginTab(tab='date_time')

        if timezone>422:
            self.utility.infoMsg('The value should under 423.')
            timezone=422

        try:
            self.utility.infoMsg('Change the time zone')
            self.Browser.find_element_by_css_selector(_datetime['timezone']).click()
            time.sleep(3)
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(%d)' % timezone).click()
            time.sleep(3)

            self._checkTimeZonecheckboxstatus(next_status=ntpSwitch)
            time.sleep(0.5)

            self.utility.infoMsg('Replace ntp servers')
            _tmp = self.Browser.find_element_by_css_selector('input[name*="ntptimeservers"]')
            _tmp.clear()
            _tmp.send_keys(ntpserver)

            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector('span[id$="-time-ok-btnWrap"]').click()
            time.sleep(8)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname,
                                   error='Date and Time: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Date and Time: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Date and Time: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Date and Time: Exception just happened.')
            pass

sysGS = sys_GeneralSettings()
sysGS.webManager(ssl_tls=True)
sysGS.adminpwdRecovery()
sysGS.date_time()
sysGS._logout()