'''
Created on July 25, 2018
@Author: Philip Yang

Full Title: GRACK Connect Rsync: Server

Rsync server: add, edit and delete module item including user and settings control.


'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

button = {
    'rsync' : 'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(7)',
    'advanced_menu' : 'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(10)',
    'server' : 'div[class*="grack-view"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span ',
    'setting' : 'div[class*="grack-view"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[id$="-body"] div[id$="innerCt"] div a:nth-child(1) span',
    'module' : 'div[class*="grack-view"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[id$="-body"] div[id$="innerCt"] div a:nth-child(2) span',
    'add' : 'div[class*="grack-view"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[id$="-server-body"] div:nth-child(2) div[id^="toolbar"] div div a:nth-child(1) span[id$="-add-btnWrap"]',
    'edit' : 'div[class*="grack-view"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[id$="-server-body"] div:nth-child(2) div[id^="toolbar"] div div a:nth-child(2) span[id$="-edit-btnWrap"]',
    'delete' : 'div[class*="grack-view"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[id$="-server-body"] div:nth-child(2) div[id^="toolbar"] div div a:nth-child(3) span[id$="-delete-btnWrap"]',
    'normal' :'div[class*="x-window-container"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div a:nth-child(1) span',
    'usermanagement' :'div[class*="x-window-container"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div a:nth-child(2) span',
    'maxlinker' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(11) div div[id$="triggerWrap"] div[id$="inputWrap"] input',
    'allowlist' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(12) div div[id$="triggerWrap"] div input',
    'denylist' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(13) div div[id$="triggerWrap"] div input',
    'modulecomment' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(15) div div div input',
    'ok' :  'div[class*="x-window-container"] div[class*="x-toolbar"] div[id$="innerCt"] div a:nth-child(1) span[id$="ok-btnWrap"]',
    'useradd' : 'div[class*="x-window-container"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-toolbar"] div div a:nth-child(1) span',
    'useredit' : 'div[class*="x-window-container"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-toolbar"] div div a:nth-child(2) span',
    'userdelete' : 'div[class*="x-window-container"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-toolbar"] div div a:nth-child(3) span',
    'usersave' : 'body div[class*="x-window-container"]:nth-child(2) div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-toolbar"] '
}

option_switch = {
    'switch' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(1) div div input',
    'chroot' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(6) div div input',
    'authuser' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(7) div div input',
    'readonly' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(8) div div input',
    'writeonly' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(9) div div input',
    'list': 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(10) div div input',
}

option_boundlist ={
    'pickWorkspace' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(2) div div div[id$="trigger-picker"]',
    'user' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(4) div div div[id$="trigger-picker"]',
    'group' : 'div[class*="x-window-container"] div[id$="-body"] div:nth-child(1) div div div:nth-child(5) div div div[id$="trigger-picker"]',
}

option_user ={
    'add' : "//body//div[@class, 'x-window-container']//div[@class, 'x-window-body']//div[@class, 'x-panel']//div[@class, 'x-panel-body']//div[2]//div[@class, 'x-toolbar']//div//div//a[1]//span",
    'edit' : "//body//div[@class, 'x-window-container']//div[@class, 'x-window-body']//div[@class, 'x-panel']//div[@class, 'x-panel-body']//div[2]//div[@class, 'x-toolbar']//div//div//a[2]//span",
    'delete' : "//body//div[@class, 'x-window-container']//div[@class, 'x-window-body']//div[@class, 'x-panel']//div[@class, 'x-panel-body']//div[2]//div[@class, 'x-toolbar']//div//div//a[3]//span",
    'save' : "//body//div[@class, 'x-window-container'][2]//div[@class, 'x-window-body']//div[@class, 'x-panel']//div[@class, 'x-toolbar']//div[@class, 'x-box-inner']//"
}

class setupRsyncServer():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'connect setupRsync server'
        self.pickup = False
        self.login = False
        self.switch = True


    def gotoRsync(self):
        # Click Rsync tab button
        try:
            self.Browser.find_element_by_css_selector(button['rsync']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(button['server']).click()
            time.sleep(0.5)
            self.login = True
            return
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found during the logining.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Button cannot be clicked.')
            self.gotoRsync_rev()
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during the logining. ' + e.message)
            pass

    def gotoRsync_rev(self):
        # Click more and Rsync tab button
        try:
            self.Browser.find_element_by_css_selector(button['advanced_menu']).click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector(button['rsync']).click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector(button['server']).click()
            time.sleep(0.5)
            self.login = True
            return
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found during the logining.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during the logining. ' + e.message)
            pass

    def user(self, username='Lara', password='tombrider', password2nd='uncharted'):
        # Add user in the server module of Rsync
        try:
            self.utility.infoMsg('Create user account under server module.')
            self.Browser.find_element_by_css_selector(button['useradd']).click()
            time.sleep(0.5)

            id = self.Browser.find_element_by_css_selector(' input[id^="usercombo"][name*="name"] ')
            id.send_keys(username)
            time.sleep(0.5)
            pwd = self.Browser.find_element_by_css_selector('input[id^="passwordfield"][name*="password"]')
            pwd.send_keys(password)
            time.sleep(0.5)

            self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div[contains(@class,'x-box-inner')]//div[contains(@class, 'x-box-target')]//a[1]//span").click()
            time.sleep(0.5)

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found during add user.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,error='Exception has happened during add user. ' + e.message)
            pass

        try:
            self.utility.infoMsg('Pick user account and change password.')
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" %username).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(button['useredit']).click()
            time.sleep(0.5)

            pwd = self.Browser.find_element_by_css_selector('input[id^="passwordfield"][name*="password"]')
            pwd.clear()
            pwd.send_keys(password2nd)
            time.sleep(0.5)

            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div[contains(@class,'x-box-inner')]//div[contains(@class, 'x-box-target')]//a[1]//span").click()
            time.sleep(0.5)

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found during edit user.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,error='Exception has happened during edit user. ' + e.message)
            pass

        try:
            self.utility.infoMsg('Pick user account and delete.')
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % username).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(button['userdelete']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2)').click()
            time.sleep(0.5)

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found during delete user.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,error='Exception has happened during delete user. ' + e.message)
            pass




    def settings(self, switch=True, port=666):

        self.itemname="Rsync Server settings"
        self.utility.unittest(test_item_name=self.itemname)

        # Detecting switch status
        try:
            self.Browser.find_element_by_css_selector('div[class*="autocontainer-innerCt"][id^="fieldset"] div[id^="checkbox"][class*="x-form-cb-checked"]').is_displayed()
            self.switch = True
        except NoSuchElementException:
            self.utility.infoMsg('Cannot catch class of x-form-cb-checked so we guess the switch is off')
            self.switch = False

        if self.switch == switch:
            self.utility.infoMsg('The switch checking is done.')
            pass
        else:
            try:
                self.Browser.find_element_by_css_selector('div[class*="autocontainer-innerCt"][id^="fieldset"] div[id^="checkbox"] div div input').click()
                time.sleep(0.5)
                self.utility.infoMsg('The switch checking is done.')
            except NoSuchElementException:
                self.utility.fail_test(testname=self.itemname, error='Cannot find out the switch button.')
                pass
            except Exception as e:
                self.utility.fail_test(testname=self.itemname,
                                       error='Exception has happened during the switch check. ' + e.message)
                pass

        try:
            self.utility.infoMsg('Write the port number.')
            selectPort = self.Browser.find_element_by_css_selector('input[name="port"]')
            selectPort.clear()
            selectPort.send_keys(port)
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Cannot fill in the port select.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during the port select. ' + e.message)
            pass

        try:
            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"').click()
            time.sleep(15)
            self.utility.infoMsg('Saving...')
            self.utility.pass_test(testname=self.itemname)
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='OK button is not found.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during the saving. ' + e.message)
            pass

    def module_add(self):
        self.itemname = "Add Rsync Module"
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(button['module']).click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(button['add']).click()
            time.sleep(5)

            # Rsync module options
            self.utility.infoMsg('Pick up a workspace.')
            self.Browser.find_element_by_css_selector(option_boundlist['pickWorkspace']).click()
            time.sleep(20)
            self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-boundlist')]//div//ul//li[1]",).click()
            time.sleep(5)

            self.utility.infoMsg('Pick up user rules.')
            self.Browser.find_element_by_css_selector(option_boundlist['user']).click()
            time.sleep(5)
            self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-boundlist')][2]//div//ul//li[1]", ).click()
            time.sleep(5)

            self.utility.infoMsg('Pick up group rules.')
            self.Browser.find_element_by_css_selector(option_boundlist['group']).click()
            time.sleep(5)
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-boundlist')][3]//div//ul//li[1]", ).click()
            time.sleep(5)

            # Click Option buttons
            for members in option_switch:
                self.utility.infoMsg("Touch: " + members)
                self.Browser.find_element_by_css_selector(option_switch[members]).click()
                time.sleep(5)

            self.Browser.find_element_by_css_selector(option_switch['authuser']).click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(option_switch['readonly']).click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(option_switch['writeonly']).click()
            time.sleep(5)

            # comment
            self.utility.infoMsg('Write the comment')
            modulecomment = self.Browser.find_element_by_css_selector(button['modulecomment'])
            modulecomment.send_keys('rsyncmodule')

            self.utility.infoMsg('Saving')
            self.Browser.find_element_by_css_selector(button['ok']).click()
            time.sleep(30)
            self.utility.pass_test(testname=self.itemname)
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg("Button cannot isn't visibled.")
            self.gotoRsync_rev()
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during setup module. ' + e.message)
            pass

    def module_edit(self, comment='rsyncmodule' ):
        self.itemname = "Edit Rsync Module"
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(button['module']).click()
            time.sleep(3)
            self.utility.infoMsg('Pick up a Rsync module')
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(10)
            self.Browser.find_element_by_css_selector(button['edit']).click()

            # maxlinker
            self.utility.infoMsg('Write the maximum linker.')
            maxlinkeNum = self.Browser.find_element_by_css_selector(button['maxlinker'])
            maxlinkeNum.clear()
            maxlinkeNum.send_keys('8')
            time.sleep(1)

            # allowlist
            self.utility.infoMsg('Write the allowed IP list.')
            allowIP = self.Browser.find_element_by_css_selector(button['allowlist'])
            allowIP.send_keys('192.168.11.150,')
            time.sleep(1)

            # denylist
            self.utility.infoMsg('Write the denied IP list.')
            denyIP = self.Browser.find_element_by_css_selector(button['denylist'])
            denyIP.send_keys('192.168.11.151,')
            time.sleep(1)

            self.Browser.find_element_by_css_selector(button['usermanagement']).click()
            time.sleep(0.5)

            # user list in the server module
            self.user()

            self.Browser.find_element_by_css_selector(button['ok']).click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg("Button cannot isn't visibled.")
            self.gotoRsync_rev()
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during edit module. ' + e.message)
            pass

    def module_delete(self, comment='rsyncmodule'):
        self.itemname = "Delete Rsync Module"
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.Browser.find_element_by_css_selector(button['module']).click()
            time.sleep(3)
            self.utility.infoMsg('Pick up a Rsync module for delete.')
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % comment).click()
            time.sleep(10)
            self.Browser.find_element_by_css_selector(button['delete']).click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2)').click()
            time.sleep(15)

            self.utility.pass_test(testname=self.itemname)

        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg("Button cannot isn't visibled.")
            self.gotoRsync_rev()
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened during delete module. ' + e.message)
            pass


    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__":
    testing = setupRsyncServer()
    testing.gotoRsync()  # make sure login behavior
    testing.settings()
    time.sleep(3)
    testing.module_add()
    time.sleep(3)
    testing.module_edit()
    time.sleep(3)
    testing.module_delete()
    testing.logout()