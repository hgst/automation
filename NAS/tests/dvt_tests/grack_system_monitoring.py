'''
Created on Auguest 29, 2018
@Author: Philip Yang

Full Title: GRACK System: Monitoring

Performance Data, settings, hardware sensor and notify.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

_functionbtn ={
    'PerformanceData' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'HWSensor' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
    'Notify' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(3) span',
    'Settings' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(4) span'
}

_monitoring_switch = {
    'switch_status' : 'div[id$="-perfstats-innerCt"] fieldset div div div div[class*="x-form-cb-checked"]',
    'switch_location' : 'div[id$="-perfstats-innerCt"] fieldset div div div div div div input',
}

_notificationbtn={
    'fan' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(1) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'temperature' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(2) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'voltages' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(3) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'filesystem' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(4) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'quotas' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(5) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'RAID' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(6) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'SMART' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(7) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'CPU usage' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(8) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'load average' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(9) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'memory usage' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(10) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'process monitoring' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(11) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
    'software update' : 'div[id$="-notifications-body"] div div[class*="x-grid-item-container"] table:nth-child(12) tbody tr[class*="x-grid-row"] td[class*="x-grid-cell-checkcolumn"]',
}

_notificationbtnlist={ 'fan', 'temperature', 'voltages', 'filesystem', 'quotas', 'RAID', 'SMART', 'CPU usage', 'load average', 'memory usage', 'process monitoring', 'software update' }

_settings_element_status={
    'function_switch' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]',
    'ssl_tls' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(3)',
    'auth' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(5)',
    'server' : 'smtp.mail.yahoo.com',
    'port' : '465',
    'sender' : 'fake@wdc.com',
    'username' : 'test1234',
    'password' : 'Welcom2WD2017',
    'primaryemail' : 'fake_receiver1@wdc.com',
    'secondaryemail' : 'fake_receiver2@wdc.com',
}

_settings_element_location = {
    'function_switch' : 'div[id$="-settings-innerCt"] fieldset:nth-child(1) div div div div div div input',
    'ssl_tls' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"]:nth-child(3) div div input',
    'auth' : 'div[id$="-settings-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"]:nth-child(5) div div input',
    'server' : 'input[name*="server"]',
    'port' : 'input[name*="port"]',
    'sender' : 'input[name*="sender"]',
    'username' : 'input[name*="username"]',
    'password' : 'input[name*="password"]',
    'primaryemail' : 'input[name*="primaryemail"]',
    'secondaryemail' : 'input[name*="secondaryemail"]',
}

_settingsinputlist={ 'server', 'port', 'sender', 'username', 'password', 'primaryemail', 'secondaryemail'}

class sys_monitoring():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'System Monitoring'
        self.login = False
        self.commonwaiting = 30


    def _loginTab(self, tab):
        # Click tab of Monitoring
        try:
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"][class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(3) span').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(_functionbtn[tab]).click()
            time.sleep(2)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='System Monitoring tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='System Monitoring tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('System Monitoring tab button cannot be clicked.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def PerformanceData(self, monitoring_switch=True):
        self.itemname = 'Monitoring : Performance Data'
        self.utility.unittest(test_item_name=self.itemname)
        self._loginTab(tab='PerformanceData')
        switch = False
        try:
            self.utility.infoMsg('To detect the switch status.')
            self.Browser.find_element_by_css_selector(_monitoring_switch['switch_status']).is_displayed()
            switch = True
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Performance Data: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            switch = False
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='Performance Data: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Performance Data: Exception just happened.')
            pass

        if switch == monitoring_switch:
            self.utility.pass_test(testname=self.itemname)
            pass
        else:
            try:
                self.utility.infoMsg('To click the switch button.')
                self.Browser.find_element_by_css_selector(_monitoring_switch['switch_location']).click()
                time.sleep(2)
                self.Browser.find_element_by_css_selector('span[id$="perfstats-ok-btnWrap"]').click()
                time.sleep(2)
                self.utility.pass_test(testname=self.itemname)
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Performance Data: Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='Performance Data: No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname,
                                       error='Performance Data: Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Performance Data: Exception just happened.')
                pass

    def Hardwaresensor(self):
        self.itemname = 'Monitoring : Hardware Sensors'
        self.utility.unittest(test_item_name=self.itemname)
        self._loginTab(tab='HWSensor')
        try:
            self.utility.infoMsg('Refresh the page.')
            self.Browser.find_element_by_css_selector('span[id$="-hwsensors-refresh-btnWrap"]').click()
            time.sleep(2)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Hardware Sensors: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Hardware Sensors: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Hardware Sensors: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Hardware Sensors: Exception just happened.')
            pass

    def _checkNotifications(self, btn):
        try:
            self.utility.infoMsg('To click button: %s' %btn)
            time.sleep(0.5)
            self.Browser.find_element_by_css_selector(_notificationbtn[btn]).click()
            time.sleep(0.5)
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Notifications button check: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Notifications button check: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Notifications button check: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Notifications button check: Exception just happened.')
            pass

    def notify(self):
        self.itemname = 'Monitoring : Notifications'
        self.utility.unittest(test_item_name=self.itemname)
        self._loginTab(tab='Notify')
        try:
            self.utility.infoMsg('To click all button')
            for switch in _notificationbtnlist:
                self._checkNotifications(btn=switch)
            time.sleep(0.5)
            self.utility.infoMsg('To click Refresh button')
            self.Browser.find_element_by_css_selector('span[id$="-notifications-refresh-btnWrap"]').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector(_notificationbtn['fan']).click()
            time.sleep(0.5)
            self.utility.infoMsg('Apply...')
            self.Browser.find_element_by_css_selector('span[id$="-notifications-apply-btnWrap"]').click()
            time.sleep(self.commonwaiting)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Notifications: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Notifications: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Notifications: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Notifications: Exception just happened.')
            pass

    def settings(self, functionSwitch=True, SSL_TLS=True, Auth=True):
        self.itemname = 'Monitoring : Settings'
        self.utility.unittest(test_item_name=self.itemname)
        time.sleep(15)
        self._loginTab(tab='Settings')

        self._switchControl(btn='function_switch', switch=functionSwitch)
        time.sleep(0.5)
        self._switchControl(btn='ssl_tls', switch=SSL_TLS)
        time.sleep(0.5)
        self._switchControl(btn='auth', switch=Auth)
        time.sleep(0.5)

        for settingsmember in _settingsinputlist:
            try:
                self.utility.infoMsg('To replace the input element: %s ' % settingsmember)
                _tmp=self.Browser.find_element_by_css_selector(_settings_element_location[settingsmember])
                _tmp.clear()
                _tmp.send_keys(_settings_element_status[settingsmember])

            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Settings: Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='Settings: No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname,
                                       error='Settings: Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Settings: Exception just happened.')
                pass
        time.sleep(0.5)

        try:
            self.utility.infoMsg('Saving')
            self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
            time.sleep(8)
            time.sleep(self.commonwaiting)
            self.utility.infoMsg('Testing')
            self.Browser.find_element_by_css_selector('span[id$="-settings-test-btnWrap"]').click()
            time.sleep(8)
            self.utility.infoMsg('Accept')
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(3)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Settings: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Settings: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Settings: Exception just happened.')
            pass


    def _switchControl(self, btn, switch=True):
        self.utility.infoMsg('To detect the switch of %s' %btn)
        _switch = False
        try:
            self.Browser.find_element_by_css_selector(_settings_element_status[btn]).is_displayed()
            _switch = True
        except NoSuchElementException as e1:
            _switch = False
            pass
        except ElementNotVisibleException as e2:
            _switch = False
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Settings Switch: Exception just happened.')
            pass
        if _switch == switch:
            return
        else:
            try:
                self.Browser.find_element_by_css_selector(_settings_element_location[btn]).click()
                return
            except InvalidSelectorException as e0:
                self.utility.fail_test(testname=self.itemname, error='Settings Switch: Unselectable elemnt ' + e0.msg)
                pass
            except NoSuchElementException as e1:
                self.utility.fail_test(testname=self.itemname, error='Settings Switch: No such element ' + e1.msg)
                pass
            except ElementNotVisibleException as e2:
                self.utility.fail_test(testname=self.itemname,
                                       error='Settings Switch: Element is invisibled ' + e2.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Settings Switch: Exception just happened.')
                pass


if __name__ == "__main__":
    testing = sys_monitoring()
    testing.PerformanceData()
    testing.Hardwaresensor()
    testing.notify()
    testing.settings()
    testing._logout()