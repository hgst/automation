'''
Created on September 4th, 2018
@Author: Philip Yang

Full Title: GRACK System: App Portal

App page: refresh, install and remove apps.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

class sys_appportal():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'System App Portal'
        self.login_success =False

    def _loginTab(self):
        # Click tab of App Portal
        try:
            self.Browser.find_element_by_css_selector('div[id^="workspace-node-tab"][class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id^="tabbar"] div a:nth-child(6) span').click()
            time.sleep(2)
            self.login_success = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='System App Portal tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='System App Portal tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('System App Portal tab button is invisibled.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def _trytopickError(self):
        try:
            time.sleep(2)
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[id^="toolbar"][class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span').click()
            time.sleep(2)
        except :
            pass

    def _refreshpage(self):
        self.itemname='App Portal: Refresh'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Click refresh button')
            self.Browser.find_element_by_css_selector('span[id$="-check-btnWrap"]').click()
            time.sleep(30)
            self.utility.infoMsg('Check error mesage')
            # To detect error message box.
            self._trytopickError()
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Refresh button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Refresh button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Refresh button is invisibled.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Refresh: Exception just happened.')
            pass

    def _pickupApp(self):
        """ Patch for FW v1.1.0"""
        pickup = False
        try:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table[class*="x-grid-item-selected"]').is_displayed()
            pickup = True
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='App cannot be clicked.')
            pass
        except NoSuchElementException:
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('App is invisibled.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='App: Exception just happened.')
            pass
        if pickup is False:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table tbody tr[class*="x-grid-row"] td:nth-child(1) div div').click()

    def _applyInstaller(self):
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span').click()
            self.utility.infoMsg('Accepted...')
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='message box OK button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.infoMsg('message box button is not exist.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('message box button is invisibled.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='message box: Exception just happened.')
            pass

        """" Patch for FW v1.1.0 """
        try:
            self.utility.infoMsg('Accept...')
            self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-window')][3]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span").click()
            time.sleep(3)
            self.utility.infoMsg('Apply...')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span').click()
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Apply cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Apply button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Apply button is invisibled.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Apply: Exception just happened.')
            pass

    def _install(self):
        self.itemname = 'App Portal: Install'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Pick up a app')
            self._pickupApp()
            time.sleep(2)
            self.utility.infoMsg('Click install button')
            self.Browser.find_element_by_css_selector('span[id$="-install-btnWrap"]').click()
            time.sleep(2)
            self.utility.infoMsg('Apply the installer.')
            self._applyInstaller()
            time.sleep(15)
            self.utility.infoMsg('Close window.')
            self.Browser.find_element_by_css_selector('span[id$="-close-btnWrap"]').click()
            time.sleep(2)
            self.utility.infoMsg('Refresh the page')
            self.Browser.find_element_by_css_selector('div[id^="message"][class*="x-message-box"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Install: button or item cannot be clicked.' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Install: button or item is not found.' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.infoMsg('Install: button or item is invisibled.' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Install: Exception just happened.')
            pass

    def _remove(self):
        self.itemname = 'App Portal: Remove'
        self.utility.unittest(test_item_name=self.itemname)
        try:
            self.utility.infoMsg('Pick up a app.')
            self._pickupApp()
            time.sleep(0.5)
            self.utility.infoMsg('Click delete button')
            self.Browser.find_element_by_css_selector('span[id$="-delete-btnWrap"]').click()
            time.sleep(2)
            self.utility.infoMsg('Apply the delete.')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(10)
            self.utility.infoMsg('Close the window.')
            self.Browser.find_element_by_css_selector('span[id$="-close-btnWrap"]').click()
            time.sleep(2)
            self.utility.infoMsg('Refresh the page.')
            self.Browser.find_element_by_css_selector('div[id^="message"][class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='Remove: button or item cannot be clicked. ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='Remove: button or item is not found or the installation fail. ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.infoMsg('Remove: button or item is invisibled. ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Remove: Exception just happened.')
            pass

    def appportal(self):
        self._loginTab()
        if self.login_success:
            self._refreshpage()
            self._install()
            self._remove()
            self._logout()

if __name__ == "__main__":
    sys_appportal().appportal()