'''
Created on July 18, 2018
@Author: Philip Yang

Full Title: GRACK Connect AFP

AFP service: add, edit and delete item.
AFP setting: function on/off

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info


afp_Switch = {
    'readonly' : "//body//div[contains(@class, 'x-window')][2]//div//div//div//div//div[4]//div//div//input",
    'guest_login' : "//body//div[contains(@class, 'x-window')][2]//div//div//div//div//div[5]//div//div//input",
    'guest_rw' : "//body//div[contains(@class, 'x-window')][2]//div//div//div//div//div[6]//div//div//input",
    'tm' : "//body//div[contains(@class, 'x-window')][2]//div//div//div//div//div[7]//div//div//input",
    'hidedDF' : "//body//div[contains(@class, 'x-window')][2]//div//div//div//div//div[10]//div//div//input",
    'casefolding' : "//body//div[contains(@class, 'x-window')][2]//div//div//div//div//div[12]//div//div//div[2]"
}

casefoldinglevel = {
    'none' : "//body//div[contains(@class, 'x-boundlist')][2]//div//ul//li[1]",
    'Lowercase' : "//body//div[contains(@class, 'x-boundlist')][2]//div//ul//li[2]",
    'Uppercase' : "//body//div[contains(@class, 'x-boundlist')][2]//div//ul//li[3]",
    'ClientLower_ServerUpper' : "//body//div[contains(@class, 'x-boundlist')][2]//div//ul//li[4]",
    'ClientUpper_ServerLower' : "//body//div[contains(@class, 'x-boundlist')][2]//div//ul//li[5]"
}

casefoldinglevel_edit = {
    'none' : "//body//div[contains(@class, 'x-boundlist')]//div//ul//li[1]",
    'Lowercase' : "//body//div[contains(@class, 'x-boundlist')]//div//ul//li[2]",
    'Uppercase' : "//body//div[contains(@class, 'x-boundlist')]//div//ul//li[3]",
    'ClientLower_ServerUpper' : "//body//div[contains(@class, 'x-boundlist')]//div//ul//li[4]",
    'ClientUpper_ServerLower' : "//body//div[contains(@class, 'x-boundlist')]//div//ul//li[5]"
}


class setupAFP():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'storage setupAFP'
        self.pickup = False
        self.afp_switch = False
        self.readonly = False
        self.guest_login = False
        self.guest_rw = False
        self.tm_switch = False
        self.hidedDF = False
        # Click AFP tab
        self.Browser.find_element_by_css_selector(
            'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(5)').click()
        time.sleep(0.5)

    def add(self, readOnly=True, guestLogin=True, guestReadWrite=True,timeMachine=True, hidingDotFiles=True, folder_display='Lowercase', comment='AFP sample string'):
        ''' Add AFP item
            :param readOnly : switch on/off
            :param guestLogin : To allow guest to use AFP service.
            :param guestReadWrite: Allow guest read/write files.
            :param timeMachine: Let Time machine support the AFP service.
            :param hidingDotFiles: hiding the dot name files.
            :param comment: AFP note
            '''
        self.itemname = 'AFP Add Item Testing'
        self.utility.unittest(self.itemname)

        try:
            # Click add btn
            self.Browser.find_element_by_css_selector(' span[id$="-shares-add-btnWrap"]').click()
            time.sleep(0.5)

            # TRY TO PICK A WORKSPACE
            self.pickWorkspace()
            time.sleep(0.5)
            if self.pickup == False:
                # TRY TO CREATE A WORKSPACE
                self.utility.infoMsg('Create a workspace first.')
                self.createWorkspace()
                time.sleep(0.5)
                self.pickWorkspace()

            self.utility.infoMsg(msg='Options status control.')
            if readOnly:
                self.Browser.find_element_by_xpath(afp_Switch['readonly']).click()
            time.sleep(0.5)
            if guestLogin:
                self.Browser.find_element_by_xpath(afp_Switch['guest_login']).click()
            time.sleep(0.5)
            if guestReadWrite:
                self.Browser.find_element_by_xpath(afp_Switch['guest_rw']).click()
            time.sleep(0.5)
            if timeMachine:
                self.Browser.find_element_by_xpath(afp_Switch['tm']).click()
            time.sleep(0.5)
            if hidingDotFiles:
                self.Browser.find_element_by_xpath(afp_Switch['hidedDF']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_xpath(afp_Switch['casefolding']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(casefoldinglevel[folder_display]).click()
            time.sleep(0.5)
            self.utility.infoMsg(msg='Options status checked')

            # Add note
            self.utility.infoMsg('Replace comment...')
            option = self.Browser.find_element_by_name('comment')
            option.clear()
            option.send_keys(comment)
            time.sleep(0.5)

            # Saving
            self.utility.infoMsg('Save item')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()

            # Catching dialogue
            self.catchingDialogue()

            time.sleep(20)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Add testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Add testing: Exception has happened. ' + e.message)
            pass

    def edit(self, folder_display='Uppercase', note='AFP sample string'):
        ''' Edit AFP item
            :param readOnly : switch on/off
            :param guestLogin : To allow guest to use AFP service.
            :param guestReadWrite: Allow guest read/write files.
            :param timeMachine: Let Time machine support the AFP service.
            :param hidingDotFiles: hiding the dot name files.
            :param note: AFP note
            '''
        self.itemname = 'AFP Edit Item Testing'
        self.utility.unittest(self.itemname)
        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click Edit btn
            self.Browser.find_element_by_css_selector(' span[id$="-shares-edit-btnWrap"]').click()
            time.sleep(3)

            self.utility.infoMsg(msg='Options status control.')
            self.Browser.find_element_by_xpath(afp_Switch['readonly']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(afp_Switch['guest_login']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(afp_Switch['guest_rw']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(afp_Switch['tm']).click()
            time.sleep(0.5)
            self.Browser.find_element_by_xpath(afp_Switch['hidedDF']).click()
            time.sleep(0.5)

            self.Browser.find_element_by_xpath(afp_Switch['casefolding']).click()
            time.sleep(2)
            self.Browser.find_element_by_xpath(casefoldinglevel_edit[folder_display]).click()
            time.sleep(0.5)

            # Saving
            self.utility.infoMsg('Save item')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
            time.sleep(10)
            # Catching dialogue
            self.catchingDialogue()
            time.sleep(0.5)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,error='Edit testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,error='Edit testing: Stuck in the page.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Edit testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,error='Delete testing: Exception has happened. ' + e.message)
            pass

    def delete(self, note='AFP sample string'):
        ''' Delete AFP item
            :param note: AFP comment
            '''

        self.itemname = 'AFP Delete Item Testing'
        self.utility.unittest(self.itemname)
        try:
            # Select target disk
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % note).click()
            time.sleep(3)

            # Click Delete btn
            self.utility.infoMsg('Preparing to delete the item.')
            self.Browser.find_element_by_css_selector(' span[id$="-shares-delete-btnWrap"]').click()
            time.sleep(3)

            # Apply
            self.utility.infoMsg('Delete the AFP item.')
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[id$="-toolbar"] div div a:nth-child(2) span').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,error='Delete testing: Stuck in the page.')
            pass

        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,error='Delete testing: Stuck in the page.')
            pass

        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Delete testing: ' + e2.msg)
            pass

        except Exception as e:
            self.utility.fail_test(testname=self.itemname,error='Delete testing: Exception has happened. ' + e.message)
            pass

    def setting(self, function_switch=True):
        ''' AFP settings
        :param function _switch on/off
        '''

        self.itemname = 'AFP settings Testing'
        self.utility.unittest(self.itemname)

        try:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div div div[id$="-innerCt"] div a:nth-child(2) span').click()
            time.sleep(3)

            self.utility.infoMsg('Catching switch status...')
            self.afpswitchStatus()
            time.sleep(3)

            if (self.afp_switch == function_switch):
                pass
            else:
                self.utility.infoMsg('Changing switch status...')
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-autocontainer-innerCt"][id^="fieldset-"] div[id^="checkbox"] div div input').click()
                time.sleep(0.5)

            # Save
            self.utility.infoMsg('Saving the settings.')
            self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
            time.sleep(15)
            self.utility.pass_test(testname=self.itemname)

        except ElementNotVisibleException as e1:
            self.utility.infoMsg('Element is not visible. ' + e1.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings testing: Stuck in the page.')
            pass
        except InvalidSelectorException as e3:
            self.utility.infoMsg('Element is unselectable: tab/button cannot be clicked.' + e3.msg)
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings testing: Stuck in the page.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the tab/button. :' + e2.msg)
            self.utility.fail_test(testname=self.itemname, error='Settings testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Settings testing: Exception has happened. ' + e.message)
            pass

    def pickWorkspace(self):
        try:
            self.utility.infoMsg('Pick up the 1st workspace.')
            self.Browser.find_element_by_css_selector('div[id$="trigger-picker"]').click()
            time.sleep(5)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            time.sleep(3)
            self.pickup = True
        except NoSuchElementException:
            self.utility.warnMsg('There has no workspace to pick up.')
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def createWorkspace(self):
        ''' createWorkspace
        Casue the UI cannot detect the workspace so that we trigger a new one for the testing.
        '''
        try:
            self.utility.infoMsg('Create workspace.')
            self.Browser.find_element_by_css_selector('div[id$="trigger-add"]').click()
            time.sleep(5)

            self.utility.infoMsg('Make a name for workspace.')
            workspaceName = self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-window')][3]//div[2]//div//div//div//div//div[2]//div//div//div//input")
            workspaceName.send_keys('AFPsampleWorkspace')
            time.sleep(2)

            # Click trigger btn
            self.utility.infoMsg('Select a RAID item for workspace.')
            self.Browser.find_element_by_css_selector('input[name*="devicefile"').click()
            time.sleep(8)
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click()
            time.sleep(3)

            # Save
            self.utility.infoMsg('Save the new workspace.')
            self.Browser.find_element_by_xpath(
                "//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span ").click()
            time.sleep(30)
            return

        except NoSuchElementException as e1:
            self.utility.fail_test('There has no RAID to create workspace, please create RAID at first.')
            print (e1.msg)
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def catchingDialogue(self):
        try:
            self.utility.infoMsg('Trying to catch any pop up dialogue.')
            # Parser the message
            error_msg = self.Browser.find_element_by_css_selector(
                'div[class*="x-plain"] div[id$="-body"] div div div[id^="container-"] div').text
            self.utility.warnMsg(msg=error_msg)
            time.sleep(0.5)
            # Click ok btn
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[id^="toolbar-"] div div a:nth-child(1)').click()
        except NoSuchElementException:
            self.utility.infoMsg(msg='Cannot detect any error message.')
            pass

    def afpswitchStatus(self):
        try:
            self.Browser.find_element_by_css_selector(' div[class*="x-autocontainer-innerCt"][id^="fieldset-"] div[id^="checkbox"][class*="x-form-cb-checked"] ').is_displayed()
            self.afp_switch = True
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to find the class:"x-form-cb-checked". Marker the switch status.')
            self.afp_switch = False
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,error='Catching switch status: Exception has happened. ' + e.message)
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()


if __name__ == "__main__":
    testing = setupAFP()
    testing.add()
    testing.edit()
    testing.delete()
    testing.setting(function_switch=True)
    testing.logout()
