'''
    Created on April 9, 2018
    @Author: Philip.Yang

    Full Title: create user group.
    Create group id and the new a user account and join it.

'''
from selenium import webdriver
from selenium.webdriver.remote import webelement
from selenium.common.exceptions import NoSuchElementException
import time
import device_info

accountControl = {
    'user' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(1) span',
    'group' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(2) span',
    'activedirectory' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span',
    'directoryservice' : ' div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(4) span'
}


class MyClass():


    def __init__(self):
        self.testname = 'Create a group and join user to group.'

    def gotest(self):
        userid1 = 'user1'
        groupid = 'grackgroup'
        commonpwd = 'grack'
        deviceinfo = device_info.device_info()
        deviceinfo.unittest(self.testname) # Print test name and current time
        deviceinfo.web_login(tabname='accessControl') # Auto open browser and login page

        # Creat group ID
        try:
            # Click Group icon under tab of AccessControl
            deviceinfo.browser.find_element_by_css_selector(accountControl['group']).click()
            time.sleep(1)
            # Click Plus Icon
            deviceinfo.browser.find_element_by_css_selector(' span[id$="-add-btnEl"]').click()
            time.sleep(1)
            # Click Add button
            deviceinfo.browser.find_element_by_css_selector('a[class*="x-menu-item-link"]:nth-child(1)').click()
            time.sleep(1)
            # And keyin group name
            addgroupname = deviceinfo.browser.find_element_by_name('name')
            addgroupname.send_keys(groupid)

            # Click Save button
            deviceinfo.browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(15)
            deviceinfo.infoMsg('Create Group success.')
        except NameError as e3:
            deviceinfo.errorMsg('Error, cannot find the element: ' + e3)
        except NoSuchElementException:
            deviceinfo.errorMsg('Error, cannot find the element in the group list.')
            raise NoSuchElementException

        try:
            # Click User icon under tab of AccessControl
            deviceinfo.browser.find_element_by_css_selector(accountControl['user']).click()
            time.sleep(1)
            # Click Plus icon
            deviceinfo.browser.find_element_by_css_selector('span[id*="add-btnWrap"]').click()

            time.sleep(1)
            # Click Add button of the menu list
            deviceinfo.browser.find_element_by_css_selector('a[class*="x-menu-item-link"]:nth-child(1)').click()
            time.sleep(1)

            # Add user
            addusername = deviceinfo.browser.find_element_by_name('name')
            addusername.send_keys(userid1)

            adduserpassword1 = deviceinfo.browser.find_element_by_name('password')
            adduserpassword1.send_keys(commonpwd)

            adduserpassword2 = deviceinfo.browser.find_element_by_name('passwordconf')
            adduserpassword2.send_keys(commonpwd)
            time.sleep(1)

            # Click Group
            deviceinfo.browser.find_element_by_css_selector('div[class*="x-window-closable"] > div[class*="x-window-body"] > div[class*="x-panel"] > div[class*="x-tab-bar"] > div[class*="x-tab-bar-body"] > div[class*="x-box-inner"] > div[class*="x-box-target"] > a[class*="x-tab"]:nth-child(2)').click()
            time.sleep(1)

            # Join Group
            # deviceinfo.browser.find_element_by_xpath("//*[contains(text(), 'grackgroup')]").click() # before v1.0.8
            deviceinfo.browser.find_element_by_css_selector('div[class*="x-window-container"] div[class*="x-window-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-grid-view"] div[class*="x-grid-item-container"] table tbody tr[class*="x-grid-row"] td:nth-child(1) div div').click()
            time.sleep(1)

            # Click OK button
            deviceinfo.browser.find_element_by_css_selector('span[id$="-ok-btnInnerEl"]').click()
            time.sleep(15)
        except NameError as e2:
            deviceinfo.errorMsg('Error, cannot find the element: ' + e2)
        except NoSuchElementException:
            deviceinfo.errorMsg('Error, cannot find the element in the group list.')
            #raise NoSuchElementException
            pass
        finally:
            deviceinfo.infoMsg('Create user1 account and Join group success.')


        try:
            # remove user account
            deviceinfo.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % userid1).click()
            deviceinfo.infoMsg("Found account: \'" + userid1 +  " \', prepare to remove.")
            time.sleep(3)
            # Remove User
            deviceinfo.browser.find_element_by_css_selector('span[id$="-delete-btnWrap"').click()
            time.sleep(3)
            deviceinfo.browser.find_element_by_id('button-1006-btnInnerEl').click()
            time.sleep(15)

            # Change to page of groups
            deviceinfo.browser.find_element_by_css_selector(accountControl['group']).click()
            deviceinfo.browser.find_element_by_xpath("//*[contains(text(), '%s')]" % groupid).click()
            deviceinfo.infoMsg("Found account: \'" + groupid + " \', prepare to remove.")
            time.sleep(3)
            # Remove group id
            deviceinfo.browser.find_element_by_css_selector('span[id$="-delete-btnWrap"').click()
            deviceinfo.browser.find_element_by_id('button-1006-btnInnerEl').click()
            time.sleep(15)
            deviceinfo.infoMsg('Remove user/group account success.')
            deviceinfo.pass_test(self.testname)
        except NameError as e3:
            print('Fail to remove user account ' + e3)
            deviceinfo.warn_test(self.testname, "Cannot file the element.")
            pass
        except NoSuchElementException:
            deviceinfo.errorMsg('Error, cannot find the elemnet.')
            deviceinfo.warn_test(self.testname, "Cannot file the element.")
            #raise NoSuchElementException
            pass
        except:
            deviceinfo.infoMsg('Error, unknow error jsut happened. ')
            deviceinfo.warn_test(self.testname, "Unknown Error")
            pass

        deviceinfo.web_logout()
        deviceinfo.get_test_results()
        time.sleep(5)
        deviceinfo.infoMsg("*** Finish Testing ***")

# Import device info
MyClass().gotest()
