'''
    Created on April 26, 2018
    @Author: Philip.Yang

    Full Title: Create Volume by selenium
    Open broswer and goto connect of tab, Click AdvancedMenu > SSH
    Determine SSH/TCP tunnel switch status then do the setup behavior.
'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
from selenium.webdriver.support.ui import Select
import time
import device_info

class setSSH():

    def __init__(self):
        self.itemname = "setup SSH"
        self.ssh_switch = False # Default switch is false. ssh has been closed.
        self.tcp_switch = False
        self.port_checked = False
    def setupSSH(self, user_ssh_switch=False, port=22, user_tcp_switch=False):

        utility = device_info.device_info()
        browser = utility.browser
        utility.unittest(self.itemname)
        utility.web_login(tabname='connect')

        try:
            int(port)
            self.port_checked = True
        except ValueError:
            utility.errorMsg('Parameter Error. Only accept numbers.')

        # Open SSH page
        _ssh_page = False
        try:
            browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(8)').click()
            time.sleep(0.5)
            _ssh_page = True
            utility.infoMsg('Get into the SSH page.')
        except ElementNotVisibleException as e:
            utility.infoMsg('Open Advanced menus')
            browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(10)').click()
            time.sleep(0.5)
            browser.find_element_by_css_selector('div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(8)').click()
            time.sleep(0.5)
            _ssh_page = True
            utility.infoMsg('Get into the SSH page.')
        except NoSuchElementException as e2:
            utility.fail_test(self.itemname, error='Fail to get \'SSH\' button.')
            pass
        except:
            utility.fail_test(self.itemname, error='Cannot find \'SSH\' button under connect page.')
            pass


        if(_ssh_page):
            try:

                ''' Detect SSH switch status'''
                try:
                    browser.find_element_by_css_selector('fieldset[class*="x-fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[class*="x-form-cb-checked"][id^="checkbox"]:nth-child(1) ')
                    utility.infoMsg('SSH switch has been opened.')
                    self.ssh_switch = True
                except InvalidSelectorException as e2:
                    utility.errorMsg(e2)
                except Exception as e:
                    utility.infoMsg('SSH switch has been closed.')
                    self.ssh_switch = False


                ''' Detect tcp tunnel switch status'''
                try:
                    browser.find_element_by_css_selector('fieldset[class*="x-fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[class*="x-form-cb-checked"][id^="checkbox"]:nth-child(6) ')
                    utility.infoMsg('TCP tunneling switch has been opened.')
                    self.tcp_switch = True
                except InvalidSelectorException as e2:
                    print (e2)
                except Exception as e:
                    utility.infoMsg('TCP tunneling switch has been closed.')
                    self.tcp_switch = False

                if(self.ssh_switch != user_ssh_switch):
                    browser.find_element_by_css_selector('fieldset[class*="x-fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="checkbox"]:nth-child(1) > div[id^="checkbox"] > div[id^="checkbox"] > input[id*="checkbox-"]').click()
                    time.sleep(0.5)

                    if(user_ssh_switch):
                        # If swith on , there has a double check dialogue.
                        browser.find_element_by_css_selector(
                            'div[class*="x-message-box"] div[id$="toolbar"] div div a:nth-child(2) span').click()
                        time.sleep(0.5)
                        browser.find_element_by_css_selector('div[id$="-toolbar-targetEl"] > a[id^="button"]:nth-child(2) > span[id$="-btnWrap"]' ).is_displayed()
                        time.sleep(0.5)
                        utility.infoMsg('Update SSH switch: ON')
                    else:
                        utility.infoMsg('Update SSH switch: OFF')


                if(self.port_checked):
                    insert_port = browser.find_element_by_css_selector('input[id^="numberfield"]')
                    insert_port.clear() # clear before value
                    insert_port.send_keys(port)
                    utility.infoMsg('Set Port: %d' % port)


                if(self.tcp_switch != user_tcp_switch):
                    browser.find_element_by_css_selector('fieldset[class*="x-fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="fieldset"] > div[id^="checkbox"]:nth-child(6) > div[id^="checkbox"] > div[id^="checkbox"] > input[id*="checkbox-"]').click()
                    utility.infoMsg('TCP Switch change.')


                utility.infoMsg('Update the status')
                browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
                time.sleep(4)
                utility.pass_test(self.itemname)
            except ElementNotVisibleException as e2:
                utility.errorMsg('Cannot touch switch status, perhaps the switch has been hidden: ' + e2.msg)
                pass
            except NoSuchElementException as e3:
                utility.errorMsg('Cannot see the element. perhaps page loading too fast or switch has been remove: '+ e3.msg)
                pass
            except Exception as e4:
                utility.errorMsg('There has an error when we control the SSH switch: ' + str(e4))
                pass
            utility.get_test_results()
        else:
            utility.get_test_results()
            pass

        utility.web_logout()

if __name__ == "__main__" :
    setSSH().setupSSH(user_ssh_switch=False, port=23, user_tcp_switch=False)
    setSSH().setupSSH(user_ssh_switch=True, port=22, user_tcp_switch=True)

