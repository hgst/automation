'''
Created on May 12, 2018
@Author: Philip Yang

Full Title: GRACK RAID balance and detail display.

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

class grack_raid_balance_details():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.balancingStatus = False
        self.pickup =False

    def _pickupraid(self):
        time.sleep(2)
        try:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table[class*="x-grid-item-selected"]:nth-child(1)').is_displayed()
            self.pickup = True
        except:
            pass

    def balance(self, itemname='RAID balance'):
        self.utility.unittest(test_item_name=itemname)
        try:
            self.utility.infoMsg('*** RAID balance testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector('div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(30)

            self.utility.infoMsg('Pick up a RAID...')
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)

            self._pickupraid()
            while self.pickup is False:
                self.utility.infoMsg('...click RAID again.')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
                time.sleep(3)
                self._pickupraid()

            self.Browser.find_element_by_css_selector('div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector('span[id$="-balance-btnWrap"]').click()
            time.sleep(3)
            self.utility.infoMsg('Balancing...')
            self.balancingStatus = False
            time.sleep(10)

            # To detect the ok button under the messgebox.
            self.catchMsgbox(unit_test_name=itemname)
            balancecounter=1
            while self.balancingStatus == False:
                time.sleep(10)
                self.catchMsgbox(unit_test_name=itemname)
                balancecounter+=1
                if balancecounter>5:
                    self.utility.warn_test(testname=itemname,
                                           warning='Cannot catch the pop-up messagebox after balanced. Guessing the balancing is done.')
                    self.balancingStatus = True

            self.utility.infoMsg('*** RAID balance testing is done ***')
            if balancecounter<5:
                self.Browser.find_element_by_css_selector(' div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
                self.utility.pass_test(testname=itemname)
            time.sleep(3)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname=itemname,
                                   error='RAID balance testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname=itemname, error='balance testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=itemname,
                                   error='RAID balance testing: Exception has happened. ' + e.message)

    def details(self, itemname='RAID details'):
        self.utility.unittest(test_item_name=itemname)
        try:
            self.utility.infoMsg('*** RAID details parseing testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(30)

            self.utility.infoMsg('Pick up a RAID...')
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)

            self._pickupraid()
            while self.pickup is False:
                self.utility.infoMsg('...click RAID again.')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
                time.sleep(3)
                self._pickupraid()

            time.sleep(5)
            self.Browser.find_element_by_css_selector('span[id$="-detail-btnWrap"]').click()
            time.sleep(3)
            #self.detailParser(itemname)
            time.sleep(5)
            self.utility.infoMsg('*** RAID details testing is done ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                ' div[class*="x-window-container"] div[class*="x-toolbar"] div div a:nth-child(4) span').click()
            time.sleep(15)
            self.utility.pass_test(testname=itemname)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname=itemname,
                                   error='RAID details testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname=itemname, error='Details testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=itemname,
                                   error='RAID details testing: Exception has happened. ' + e.message)

    def detailParser(self, itemname):
        try:
            details = self.Browser.find_element_by_css_selector(' div[class*="x-window-container"] div[id^="ext-comp"] div div div div textarea ')
            time.sleep(5)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname=itemname,
                                   error='RAID details testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname=itemname, error='Span testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname=itemname,
                                   error='RAID details testing: Exception has happened. ' + e.message)

    def logout(self):
        self.utility.infoMsg('Preparing to logout.')
        time.sleep(0.5)
        self.utility.web_logout()
        time.sleep(5)
        self.utility.get_test_results()


    def catchMsgbox(self, unit_test_name):
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            self.balancingStatus = True
            return
        except NoSuchElementException:
            self.utility.warnMsg('Cannot catch the pop-up messagebox after balanced.')
            self.balancingStatus = False
            pass
        except Exception as e:
            self.utility.warn_test(testname=unit_test_name,
                                   warning='Failed to determine the balancing status.' + e.message)





if __name__ == "__main__" :
    test = grack_raid_balance_details()
    test.balance()
    test.details()
    time.sleep(5)
    test.logout()

