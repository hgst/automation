'''
Created on Auguest 28, 2018
@Author: Philip Yang

Full Title: GRACK System: OS RAID

locate, pause and detail

'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

_functionbtn ={
    'add' : 'span[id$="-add-btnWrap"]',
    'cancel' : 'span[id$="-cancelrepair-btnWrap"]',
    'detail' : 'span[id$="-detail-btnWrap"]',
    'locate' : 'span[id$="-locate-btnWrap"]',
    'edit' : 'span[id$="-edit-btnWrap"]',
    'up' : 'span[id$="-up-btnWrap"]',
    'down' : 'span[id$="-down-btnWrap"]',
    'apply' : 'span[id$="-apply-btnWrap"]',
    'close' : 'span[id$="-close-btnWrap"]',
}



class sys_OSRAID():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'System OS RAID'
        self.login = False
        self._invalid = False
        self._repairStatus = False

    def _loginTab(self):
        # Click tab of OS RAID
        try:
            self.Browser.find_element_by_css_selector('div[id^="workspace-node-tab"][class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id^="tabbar"] div a:nth-child(2) span').click()
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='OS RAID tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='OS RAID tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('OS RAID tab button cannot be clicked.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

    def _pause(self):
        try:
            self.utility.infoMsg('Try to stop the locate step.')
            self.Browser.find_element_by_css_selector(_functionbtn['cancel']).click()
            time.sleep(2)
            self.utility.infoMsg('Confirm the cancel behavior.')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(0.5)
            self.utility.infoMsg('Checking has been stopped.')
            return
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='OS RAID CANCEL: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='OS RAID CANCEL: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='OS RAID CANCEL: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='OS RAID CANCEL: Exception just happened.')
            pass

    def _catchMsgbox(self):
        try:
            self.utility.infoMsg('Catching OK button...')
            time.sleep(3)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').is_displayed()
            self._repairStatus = True
            return
        except NoSuchElementException:
            self._repairStatus = False
            self.utility.infoMsg('Still checking...')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Failed to determine the checking status.' + e.message)

    def locate_pause(self,raidsystem='G-RACK12 System', pause=False):
        self.itemname='OS RAID: locate'
        self.utility.unittest(test_item_name=self.itemname)
        self._loginTab()
        try:
            self.utility.infoMsg('Pick up a RAID')
            time.sleep(8)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidsystem).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_functionbtn['locate']).click()
            time.sleep(2)
            _tmp = self.Browser.find_element_by_css_selector('input[name*="seconds"]')
            _tmp.clear()
            _tmp.send_keys(5)
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
            time.sleep(3)
            if pause:
                self._pause()
            else:
                self.utility.infoMsg('Waiting for the checking')
                self._catchMsgbox()
                while self._repairStatus == False:
                    time.sleep(5)
                    self._catchMsgbox()

            # Click OK button : still chekcing or checking is done.
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            time.sleep(0.5)

            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='OS RAID LOCATE: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='OS RAID LOCATE: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='OS RAID LOCATE: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='OS RAID LOCATE: Exception just happened.')
            pass

    def detail(self, raidsystem='G-RACK12 System'):
        self.itemname = 'OS RAID: detail'
        self.utility.unittest(test_item_name=self.itemname)
        self._loginTab()
        try:
            self.utility.infoMsg('Pick up a RAID')
            time.sleep(8)
            self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidsystem).click()
            time.sleep(0.5)

            self.Browser.find_element_by_css_selector(_functionbtn['detail']).click()
            time.sleep(8)

            self.Browser.find_element_by_css_selector(_functionbtn['close']).click()
            self.utility.pass_test(testname=self.itemname)
        except InvalidSelectorException as e0:
            self.utility.fail_test(testname=self.itemname, error='OS RAID DETAIL: Unselectable elemnt ' + e0.msg)
            pass
        except NoSuchElementException as e1:
            self.utility.fail_test(testname=self.itemname, error='OS RAID DETAIL: No such element ' + e1.msg)
            pass
        except ElementNotVisibleException as e2:
            self.utility.fail_test(testname=self.itemname, error='OS RAID DETAIL: Element is invisibled ' + e2.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='OS RAID DETAIL: Exception just happened.')
            pass

OSRAID = sys_OSRAID()
OSRAID.detail()
OSRAID.locate_pause()
OSRAID._logout()
