'''
Created on Auguest 2, 2018
@Author: Philip Yang

Full Title: GRACK Connect FTP

FTP service: (1) SSL and TLS config (2) Ban list (3) Workspaces (4) Settings


'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import InvalidSelectorException
import time
import device_info

ftp_tab ={
    'ssl_tls' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span',
    'banlist' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span',
    'workspace' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(3) span',
    'settings' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(4) span'
}

ssl_tls_switchstatus ={
    'on_off' : 'div[id$="tls-innerCt"] fieldset:nth-child(1) div div div div[id^="checkbox"][class*="x-form-cb-checked"]',

    'required' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"][class*="x-form-cb-checked"]:nth-child(1)',
    'no_certificate_request' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"][class*="x-form-cb-checked"]:nth-child(2)',
    'no_session_reuse' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"][class*="x-form-cb-checked"]:nth-child(3)',
    'implicit_ssl' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"][class*="x-form-cb-checked"]:nth-child(4)'
}

ssl_tls_switch ={
    'on_off' : 'div[id$="tls-innerCt"] fieldset:nth-child(1) div div div:nth-child(1) div[id^="checkbox"][class*="x-form-cb-checked"]',

    'required' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"]:nth-child(1) div div input',
    'no_certificate_request' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"]:nth-child(2) div div input',
    'no_session_reuse' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"]:nth-child(3) div div input',
    'implicit_ssl' : 'div[id$="tls-innerCt"] fieldset:nth-child(2) div div div div[id^="checkbox"]:nth-child(4) div div input'
}

features = {
    'add': 'span[id$="-ban-add-btnWrap"][class*="x-btn-wrap"]',
    'edit': 'span[id$="-ban-edit-btnWrap"][class*="x-btn-wrap"]',
    'delete': 'span[id$="-ban-delete-btnWrap"][class*="x-btn-wrap"]',
    'addworkspace': 'span[id$="-shares-add-btnWrap"][class*="x-btn-wrap"]',
    'editworkspace': 'span[id$="-shares-edit-btnWrap"][class*="x-btn-wrap"]',
    'deleteworkspace': 'span[id$="-shares-delete-btnWrap"][class*="x-btn-wrap"]'
}

settings_switch_status ={
    'featrureSwitch' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(1)',
    'anonymousFTP' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(1) div div div div[class*="x-form-cb-checked"]:nth-child(7)',

    'validShield' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(2)',
    'bandwidthLimit' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(3)',

    'passiveFTP' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(5)',
    'FXP' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(9)',

    'resume' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(10)',
    'identProtocol' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(11)',

    'reverseDNSlookup' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(12)',
    'transferLog' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div[class*="x-form-cb-checked"]:nth-child(13)'
}

settings_switch = {
    'featrureSwitch' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(1) div div div div:nth-child(1) div div input',
    'anonymousFTP' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(1) div div div div:nth-child(7) div div input',
    'validShield' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(2) div div input',
    'bandwidthLimit' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(3) div div input',
    'passiveFTP' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(5) div div input',
    'FXP' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(9) div div input',
    'resume' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(10) div div input',
    'identProtocol' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(11) div div input',
    'reverseDNSlookup' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(12) div div input',
    'transferLog' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div[class*="grack-panel"] div[id$="-settings-body"] div div fieldset:nth-child(2) div div div div:nth-child(13) div div input'
}

settings_textfield ={
    'port' : 'input[id^="numberfield"][name*="port"]',
    'maxclient' : 'input[id^="numberfield"][name*="maxclients"]',
    'maxconnectionsperhost' : 'input[id^="numberfield"][name*="maxconnectionsperhost"]',
    'maxloginattempts' : 'input[id^="numberfield"][name*="maxloginattempts"]',
    'timeoutidle' : 'input[id^="numberfield"][name*="timeoutidle"]',
    'displaylogin' : 'input[id^="textfield"][name*="displaylogin"]',
    'minpassiveports' : 'input[id^="numberfield"][name*="minpassiveports"]',
    'maxpassiveports' : 'input[id^="numberfield"][name*="maxpassiveports"]',
    'masqueradeaddress' : 'input[id^="textfield"][name*="masqueradeaddress"]',
    'dynmasqrefresh' : 'input[id^="numberfield"][name*="dynmasqrefresh"]',
    'maxuptransferrate':'input[id^="numberfield"][name*="maxuptransferrate"]',
    'maxdowntransferrate':'input[id^="numberfield"][name*="maxdowntransferrate"]',
}

textfield_text ={
    'port' : '23',
    'maxclient' : '10',
    'maxconnectionsperhost' : '0',
    'maxloginattempts' : '8',
    'timeoutidle' : '3600',
    'displaylogin' : 'Sample String',
    'minpassiveports' : '49999',
    'maxpassiveports' : '65530',
    'masqueradeaddress' : '192.168.11.75',
    'dynmasqrefresh' : '30',
    'maxuptransferrate':'30000',
    'maxdowntransferrate':'30000',
}

class setupFTP():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='connect')
        self.itemname = 'connect setupAFP'
        self.common_switch_status = False
        self.login = False
        self.progress = False
        self._gotoFTP()  # make sure login behavior

    def _gotoFTP(self):
        # Click FTP tab button
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(6)').click()
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='FTP button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='FTP Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('FTP Button cannot be clicked.')
            self._gotoFTP_rev()
            pass

    def _gotoFTP_rev(self):
        # Click more and FTP tab button
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(10)').click()
            time.sleep(1)
            self.Browser.find_element_by_css_selector(
                'div[class*="x-panel-body-default"][id^="workspace-node-tab"] div[id$="-body"] div:nth-child(2) div a:nth-child(6)').click()
            time.sleep(1)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='FTP button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='FTP Button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('FTP Button cannot be clicked.')
            pass

    def _gotoTab(self, tabname):
        try:
            self.Browser.find_element_by_css_selector(ftp_tab[tabname]).click()
            return
        except Exception as e:
            self.utility.warnMsg(msg='Tab %s is not found. ' % tabname)
            pass

    def ssl_tls(self, switch=True, sw_required=True, sw_no_certificate_request=True, sw_no_session_reuse=True, sw_implicit_ssl=True ):
        self.itemname = 'SSL and TLS Testing'
        self.utility.unittest(self.itemname)

        if self.login:
            self._gotoTab(tabname='ssl_tls')
            time.sleep(2)
            self.utility.infoMsg('Configure the SSL certificate')
            self._check_switch(switchname = 'on_off')
            self._pick_ssl(switch_status = switch)
        if self.progress:
            self.utility.infoMsg('Configure "required" switch')
            self._check_switch(switchname='required')
            self._pick_option(switch_status=sw_required ,switchname='required')

            self.utility.infoMsg('Configure "no_certificate_request" switch')
            self._check_switch(switchname='no_certificate_request')
            self._pick_option(switch_status=sw_no_certificate_request, switchname='no_certificate_request')

            self.utility.infoMsg('Configure "no_session_reuse" switch')
            self._check_switch(switchname='no_session_reuse')
            self._pick_option(switch_status=sw_no_session_reuse, switchname='no_session_reuse')

            self.utility.infoMsg('Configure "implicit_ssl" switch')
            self._check_switch(switchname='implicit_ssl')
            self._pick_option(switch_status=sw_implicit_ssl, switchname='implicit_ssl')

            try:
                self.utility.infoMsg('Saving')
                self.Browser.find_element_by_css_selector('span[id$="-ok-btnWrap"]').click()
                time.sleep(3)
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Cannot save the SSL/TLS status.')
            self.utility.pass_test(testname=self.itemname)

        else:
            self.utility.warn_test(testname=self.itemname, warning='You need to create SSL certificate first or the testing would be ignored.')

    def _check_switch(self, switchname):
        try:
            self.Browser.find_element_by_css_selector(ssl_tls_switchstatus[switchname]).is_displayed()
            self.common_switch_status = True
            return
        except NoSuchElementException:
            self.common_switch_status = False
            self.utility.infoMsg('Cannot catch "x-form-cb-checked" so we guess the switch is turn off.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error="Cannot detect the swithc status.")
            pass

    def _pick_ssl(self,switch_status):
        try:
            if switch_status == self.common_switch_status:
                self.progress = True
                pass
            elif switch_status:
                self.Browser.find_element_by_css_selector(
                    'div[id$="tls-innerCt"] fieldset:nth-child(1) div div div div[id^="checkbox"] div div input').click()
                time.sleep(0.5)
                self.Browser.find_element_by_css_selector(
                    'div[id$="tls-innerCt"] fieldset:nth-child(1) div div div div[id^="sslcertificatecombo"] div div div[id$="trigger-picker"]').click()
                time.sleep(3)
                self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[2] ").click()
                time.sleep(0.5)
                self.progress = True
            else:
                self.Browser.find_element_by_css_selector(
                    'div[id$="tls-innerCt"] fieldset:nth-child(1) div div div div[id^="checkbox"] div div input').click()
                time.sleep(0.5)
                self.progress = True
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='SSL certificate cannot be clicked.')
            self.progress = False
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Certificates is not found. You need to create or import SSL certificate first.')
            self.progress = False
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Certificates Button cannot be visibled.')
            self.progress = False
            pass

    def _pick_option(self,switch_status, switchname):
        try:
            if switch_status == self.common_switch_status:
                self.progress = True
                pass
            else:
                self.Browser.find_element_by_css_selector(ssl_tls_switch[switchname]).click()
                time.sleep(0.5)
                self.progress = True
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Option cannot be clicked.')
            self.progress = False
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Option is not found.')
            self.progress = False
            pass
        except ElementNotVisibleException:
            self.utility.infoMsg('Option Button is not visibled.')
            self.progress = False
            pass

    def banlist(self):
        if self.login:
            self._gotoTab(tabname='banlist')
            time.sleep(2)
            self.utility.infoMsg('Add member of banlist')
            self._BanMember(job='add',pickevent=5, occurrence='5', time_interval='12:30:30', expire='11:11:11')
            self.utility.infoMsg('Edit member of banlist')
            self._BanMember(job='edit', pickevent=8, occurrence='10', time_interval='00:30:30', expire='00:11:11')
            self.utility.infoMsg('Delete member of banlist')
            self._BanMember(job='delete')
        else:
            self.utility.infoMsg('Be sure the device is online.')

    def _BanMember(self, job, pickevent=1, occurrence='2', time_interval='00:30:00', expire='00:10:00'):
        try:

            if job =='add':
                self.itemname='Add banlist member'
                self.utility.unittest(test_item_name=self.itemname)
                self.utility.infoMsg('Click Add button')
                self.Browser.find_element_by_css_selector(features[job]).click()
                time.sleep(2)
            elif job =='edit':
                self.itemname = 'Edit banlist member'
                self.utility.unittest(test_item_name=self.itemname)
                self.Browser.find_element_by_css_selector('div[class*="x-grid-item-container"] table:nth-child(1)').click()
                time.sleep(2)
                self.utility.infoMsg('Click Edit button')
                self.Browser.find_element_by_css_selector(features[job]).click()
                time.sleep(2)

            elif job =='delete':
                self.itemname = 'Delete banlist member'
                self.utility.unittest(test_item_name=self.itemname)
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-grid-item-container"] table:nth-child(1)').click()
                time.sleep(2)
                self.Browser.find_element_by_css_selector(features[job]).click()
                time.sleep(2)
                self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) ').click()
                time.sleep(2)
                self.utility.pass_test(testname=self.itemname)
                time.sleep(5)
                return
            else:
                self.utility.infoMsg("You send a wrong features of banlist so that testing would be ignored." )
                return

            self.utility.infoMsg('Pick Event: %d' % pickevent)
            self.Browser.find_element_by_css_selector('div[id^="combobox-"][class*="x-form-arrow-trigger"]').click()
            time.sleep(2)
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(%d)' % pickevent).click()
            time.sleep(2)

            self.utility.infoMsg('Replace occurrence of %s' % occurrence)
            _occurrence = self.Browser.find_element_by_css_selector('input[id^="numberfield"][name*="occurrence"]')
            _occurrence.clear()
            _occurrence.send_keys(occurrence)
            time.sleep(2)

            self.utility.infoMsg('Replace time interval: %s' % time_interval)
            _ti = self.Browser.find_element_by_css_selector('input[id^="textfield"][name*="timeinterval"]')
            _ti.clear()
            _ti.send_keys(time_interval)
            time.sleep(2)

            self.utility.infoMsg('Replace expire: %s' % expire)
            _expire = self.Browser.find_element_by_css_selector('input[id^="textfield"][name*="expire"]')
            _expire.clear()
            _expire.send_keys(expire)
            time.sleep(2)

            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector('div[class*="x-window-container"] div[class*="x-toolbar"] div[id$="-innerCt"] div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
            self.utility.pass_test(testname=self.itemname)
            time.sleep(5)
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Button or textfield is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Button or textfield is not visibled.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened. ' + e.message)
            pass

    def workspace(self):
        if self.login:
            self._gotoTab(tabname='workspace')
            time.sleep(2)
            self.utility.infoMsg('Add workspace')
            self._workspace(job='addworkspace')
            self.utility.infoMsg('Edit workspace')
            self._workspace(job='editworkspace', description='replaceString')
            self.utility.infoMsg('Delete workspace')
            self._workspace(job='deleteworkspace')
        else:
            self.utility.infoMsg('Be sure the device is online.')

    def _workspace(self, job, description="testIng123"):
        try:
            if job == 'addworkspace':
                self.itemname = 'Add workspace'
                self.utility.unittest(test_item_name=self.itemname)
                self.utility.infoMsg('Click Add button')
                self.Browser.find_element_by_css_selector(features[job]).click()
                time.sleep(2)
                self._pickWorkspace()

                if self.progress == False:
                    # TRY TO CREATE A WORKSPACE
                    self.utility.infoMsg('Create a workspace first.')
                    self._createWorkspace()
                    self._pickWorkspace()

            elif job == 'editworkspace':
                self.itemname = 'Edit workspace'
                self.utility.unittest(test_item_name=self.itemname)
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-grid-item-container"] table:nth-child(1)').click()
                time.sleep(2)
                self.utility.infoMsg('Click Edit button')
                self.Browser.find_element_by_css_selector(features[job]).click()
                time.sleep(2)

            elif job == 'deleteworkspace':
                self.itemname = 'Delete workspace'
                self.utility.unittest(test_item_name=self.itemname)
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-grid-item-container"] table:nth-child(1)').click()
                time.sleep(2)
                self.Browser.find_element_by_css_selector(features[job]).click()
                time.sleep(2)
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) ').click()
                time.sleep(2)
                self.utility.pass_test(testname=self.itemname)
                time.sleep(5)
                return
            else:
                self.utility.infoMsg("You send a wrong features of banlist so that testing would be ignored.")
                return

            self.utility.infoMsg('Replace comment: %s' % description)
            _description = self.Browser.find_element_by_css_selector('input[id^="textfield"][name*="comment"]')
            _description.clear()
            _description.send_keys(description)
            time.sleep(2)

            self.utility.infoMsg('Saving...')
            self.Browser.find_element_by_css_selector(
                'div[class*="x-window-container"] div[class*="x-toolbar"] div[id$="-innerCt"] div a:nth-child(1) span[id$="-ok-btnWrap"]').click()
            self.utility.pass_test(testname=self.itemname)
            time.sleep(5)
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Button or textfield is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Button or textfield is not visibled.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened. ' + e.message)
            pass

    def _pickWorkspace(self):
        try:
            self.utility.infoMsg('Pick up the 1st workspace.')
            self.Browser.find_element_by_css_selector(
                'div[id^="sharedfoldercombo"][class*="x-form-arrow-trigger"]').click()
            time.sleep(2)
            self.Browser.find_element_by_xpath("//div[contains(@class, 'x-boundlist')]//div//ul/li[1] ").click()
            time.sleep(2)
            self.progress = True
            return
        except NoSuchElementException:
            self.utility.warnMsg('There has no workspace to pick up.')
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def _createWorkspace(self):
        try:
            self.utility.infoMsg('Create workspace.')
            self.Browser.find_element_by_css_selector('div[id^="sharedfoldercombo"][class*="x-form-add-trigger"]').click()
            time.sleep(5)

            self.utility.infoMsg('Make a name for workspace.')
            workspaceName = self.Browser.find_element_by_name('name')
            workspaceName.send_keys('sampleWorkspace')
            time.sleep(2)

            # Click trigger btn
            self.utility.infoMsg('Select a RAID item for workspace.')
            self.Browser.find_element_by_css_selector('input[name="devicefile"').click()
            time.sleep(8)
            self.Browser.find_element_by_css_selector('div[class*="x-boundlist"] div ul li:nth-child(1)').click()
            time.sleep(3)

            # Save
            self.utility.infoMsg('Save the new workspace.')
            self.Browser.find_element_by_xpath(
                "//div[contains(@class, 'x-window-container')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span ").click()
            time.sleep(30)
            return

        except NoSuchElementException as e1:
            self.utility.fail_test('There has no RAID to create workspace, please create RAID at first.')
            print (e1.msg)
            pass
        except Exception:
            self.utility.warnMsg('Exception is happened.')
            pass

    def settings(self):
        if self.login:
            self._gotoTab(tabname='settings')
            time.sleep(2)
            self._settings_switch_control()
            time.sleep(2)
            self._settings_replace_textfield()
            time.sleep(2)
        else:
            self.utility.infoMsg('Be sure the device is online.')

    def _settings_replace_textfield(self):
        self.itemname = 'Settings: Replace Text Item'
        self.utility.unittest(test_item_name=self.itemname)
        for counter in settings_textfield:
            try:
                self.utility.infoMsg('Replace: %s ' % counter)
                _text = self.Browser.find_element_by_css_selector(settings_textfield[counter])
                _text.clear()
                _text.send_keys(textfield_text[counter])
            except InvalidSelectorException:
                self.utility.fail_test(testname=self.itemname, error='Button cannot be clicked.')
                return
            except NoSuchElementException:
                self.utility.fail_test(testname=self.itemname,error='Button or textfield is not found.')
                return
            except ElementNotVisibleException:
                self.utility.fail_test(testname=self.itemname,error='Button or textfield is not visibled.')
                return
            except Exception as e:
                self.utility.fail_test(testname=self.itemname,error='Exception has happened. ' + e.message)
                return
        self.utility.pass_test(testname=self.itemname)
        time.sleep(0.5)
        self._saveSettings()

    def _settings_switch_control(self):
        self.itemname = 'Settings: All Switch Item ON'
        self.utility.unittest(test_item_name=self.itemname)
        for counter in settings_switch_status:
            try:
                self.Browser.find_element_by_css_selector(settings_switch_status[counter]).is_displayed()
                self.progress = True
                self.utility.infoMsg('Switch %s : ON' % counter)
                pass
            except InvalidSelectorException:
                self.utility.fail_test(testname=self.itemname, error='Button cannot be clicked.')
                return
            except NoSuchElementException:
                self.progress = False
                pass
            except ElementNotVisibleException:
                self.utility.fail_test(testname=self.itemname, error='Button or textfield is not visibled.')
                return
            except Exception as e:
                self.utility.fail_test(testname=self.itemname, error='Exception has happened. ' + e.message)
                return
            if self.progress is not True:
                self.utility.infoMsg(' To click Switch: %s' % counter)
                self.Browser.find_element_by_css_selector(settings_switch[counter]).click()

        self.utility.pass_test(testname=self.itemname)
        time.sleep(0.5)
        self._saveSettings()

    def _saveSettings(self):
        try:
            self.Browser.find_element_by_css_selector('span[id$="-settings-ok-btnWrap"]').click()
            time.sleep(3)
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Button or textfield is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Button or textfield is not visibled.')
            pass
        except Exception as e:
            self.utility.fail_test(testname=self.itemname,
                                   error='Exception has happened. ' + e.message)
            pass

    def logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()



if __name__ == "__main__":
    testing_step1 = setupFTP()
    testing_step1.ssl_tls()
    testing_step1.banlist()
    testing_step1.logout()
    time.sleep(8)
    testing_step2 = setupFTP()
    testing_step2.workspace()
    testing_step2.settings()
    testing_step2.logout()
