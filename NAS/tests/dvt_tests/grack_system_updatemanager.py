'''
    Created on September 20th, 2018
    @Author: Philip.Yang

    Full Title: Update Manager
    Based on FW: 1.1.0
    1.0.8 support refresh page, auto-upate, create snapshot and load snapshot.
    1.1.0 support new feature: offline firmware update.
'''

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
from selenium.common.exceptions import ElementNotVisibleException
import time
import device_info

_updatemanager_tab ={
    'update' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(1) span',
    'snapshot' : 'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-tab-bar"] div[class*="x-tab-bar-body"]  div[class*="x-box-inner"]  div[class*="x-box-target"] a:nth-child(2) span',
}

class updatemanager():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='system')
        self.itemname = 'System Update Manager'
        self.login = False
        self.status = False

    def _loginTab(self, tab):
        # Click tab of Update Manager
        try:
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"][class*="x-panel-body"] div:nth-child(2) div[class*="x-tab-bar"] div[class*="x-tab-bar-body"] div[id^="tabbar"] div a:nth-child(7) span').click()
            time.sleep(5)
            self.Browser.find_element_by_css_selector(_updatemanager_tab[tab]).click()
            self.login = True
            return
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Update Manager tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Update Manager tab button is not found.')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname, error='Update Manager tab button is invisibled.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Update Manager: Exception just happened.')
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.infoMsg('Holding.')
        time.sleep(600)
        self.Browser.close()

    def _refresh_button(self):
        self.itemname="Update Manager: Refresh"
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('Click reload button')

        try:
            self.Browser.find_element_by_css_selector('span[id$="packages-check-btnWrap"]').click()
            time.sleep(60)
            self._detectErrorWindow()
            self.utility.pass_test(testname=self.itemname)
            return
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Refresh button cannot be clicked.'+ e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Refresh tab button is not found.'+ e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Refresh tab button is invisibled.'+ e3.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Refresh: Exception just happened.')
            pass

    def _autoupdate(self):
        self.itemname = "Update Manager: Auto-update"
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('Click update button')
        time.sleep(10)
        try:
            self.Browser.find_element_by_css_selector('span[id$="packages-upgrade-btnWrap"]').click()
            time.sleep(3)
            self._detectPopupWindow()
            if self.status:
                self.utility.fail_test(testname=self.itemname)
            else:
                comment = self.Browser.find_element_by_css_selector(
                    'div[id^="messagebox"][class*="x-message-box"] div[id^="messagebox"][class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').text
                self.utility.infoMsg(msg=comment)
                self.utility.infoMsg(msg='To confirm the behavior...')
                self.Browser.find_element_by_css_selector(
                    'div[id^="messagebox"][class*="x-box-inner"] > div[id^="messagebox"][class*="x-box-target"] > a:nth-child(2) > span').click()

                time.sleep(300) # Cannot control the delay timing by the messagebox msg has displaied or not.
                self.utility.infoMsg('Catch reboot message.')
                comment = self.Browser.find_element_by_css_selector('div[id^="ext-comp"][class*="x-message-box"] div[class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').text
                self.utility.infoMsg(msg=comment)
                self.Browser.find_element_by_css_selector('div[id^="ext-comp"][class*="x-message-box"] div[id$="-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span').click()
                time.sleep(3)
                self.utility.pass_test(testname=self.itemname)
                return
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Auto-update button cannot be clicked.'+ e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Auto-update button is not found.'+ e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Auto-update button is invisibled.'+ e3.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Auto-update: Exception just happened.')
            pass

    def _detectPopupWindow(self):
        self.utility.infoMsg('Detect the popup window...')
        try:
            self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-message-box')][2]").is_displayed()
            time.sleep(5)
            comment = self.Browser.find_element_by_xpath("//body//div[contains(@class, 'x-message-box')][2]//div[contains(@class, 'x-window-body')]//div//div//div[contains(@class, 'x-container')]//div//div//div[contains(@class, 'x-container')]//div//div//div").text
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-window')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span").click()
            self.status = True
        except Exception:
            self.utility.infoMsg('Popup window is invisible.')
            self.status = False
            pass

    def _detectErrorWindow(self):
        self.utility.infoMsg('Detect the popup window...')
        try:
            self.Browser.find_element_by_css_selector('div[id^="window"][class*="x-message-box"]').is_displayed()
            time.sleep(5)
            comment = self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-message-box')][2]//div[contains(@class, 'x-window-body')]//div//div//div[contains(@class, 'x-container')]//div//div//div[contains(@class, 'x-container')]//div//div//div").text
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-window')][2]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span").click()
            self.status = True
        except Exception:
            self.utility.infoMsg('Popup window is invisible.')
            self.status = False
            pass

    def _click_OK_btn(self):
        try:
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class,'x-window')][2]//div[contains(@class,'x-toolbar')]//div//div//a[1]//span").click()
            time.sleep(60)
            self._detectErrorWindow_manualupdate(windowlevel=2)
            return
        except:
            self.utility.infoMsg('Failed to click the OK button, try again...')
            pass

        try:
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class,'x-window')][3]//div[contains(@class,'x-toolbar')]//div//div//a[1]//span").click()
            self.utility.infoMsg('Upload file and che')
            time.sleep(60)
            self._detectErrorWindow_manualupdate(windowlevel=3)
            return
        except:
            self.utility.fail_test(testname=self.itemname, error="Failed to click the OK button.")
            self.status = True
            pass

    def _detectErrorWindow_manualupdate(self, windowlevel):
        self.utility.infoMsg('Detect the popup window...')
        try:
            window = int(windowlevel)
            comment = self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-message-box')][%d]//div[contains(@class, 'x-window-body')]//div//div//div[contains(@class, 'x-container')]//div//div//div[contains(@class, 'x-container')]//div//div//div" % window).text
            self.utility.infoMsg(msg=comment)
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class, 'x-message-box')][%d]//div[contains(@class, 'x-toolbar')]//div//div//a[1]//span" % window).click()
            time.sleep(5)
            self.utility.infoMsg('Click cancel button.')
            self.Browser.find_element_by_xpath(
                "//body//div[contains(@class,'x-window')][%d]//div[contains(@class,'x-toolbar')]//div//div//a[2]//span" %windowlevel).click()
            self.status = True
        except Exception:
            self.utility.infoMsg('Popup window is invisible.')
            self.status = False
            pass

    def _manuallyuploadFW(self, path):
        self.itemname = "Update Manager: Upload Firmware"
        self.utility.unittest(test_item_name=self.itemname)
        self.utility.infoMsg('Click upload button')
        time.sleep(10)
        try:
            self.Browser.find_element_by_css_selector('span[id$="packages-upload-btnWrap"]').click()
            time.sleep(3)
            self.utility.infoMsg('Insert the file path')
            self.Browser.find_element_by_css_selector(
                'div[id$="triggerWrap"] div[id$="-trigger-filebutton"] div input[id^="fileuploadfield"]').send_keys(
                path)

            self.utility.infoMsg('Click ok button and upload the binary file.')
            self._click_OK_btn()
            if self.status:
                self.utility.fail_test(testname=self.itemname, error='Found the Error message. When we upload the binary.')
                return
            else:
                self.Browser.find_element_by_css_selector(
                    'div[id^="messagebox"][class*="x-box-inner"] > div[id^="messagebox"][class*="x-box-target"] > a:nth-child(2) > span').click()

                time.sleep(300)  # Cannot control the delay timing by the messagebox msg has displaied or not.
                self.utility.infoMsg('Catch reboot message.')
                comment = self.Browser.find_element_by_css_selector(
                    'div[id^="ext-comp"][class*="x-message-box"] div[class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').text
                self.utility.infoMsg(msg=comment)
                self.Browser.find_element_by_css_selector(
                    'div[id^="ext-comp"][class*="x-message-box"] div[id$="-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(1) span').click()
                time.sleep(3)
                self.utility.pass_test(testname=self.itemname)
                return
        except InvalidSelectorException as e1:
            self.utility.fail_test(testname=self.itemname, error='Upload Firmware button cannot be clicked.' + e1.msg)
            pass
        except NoSuchElementException as e2:
            self.utility.fail_test(testname=self.itemname, error='Upload Firmware button is not found.' + e2.msg)
            pass
        except ElementNotVisibleException as e3:
            self.utility.fail_test(testname=self.itemname, error='Upload Firmware button is invisibled.' + e3.msg)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Upload Firmware: Exception just happened.')
            pass

    def updatemanager110(self, auto=True, manual=False, filepath=""):
        self._loginTab(tab='update')
        if self.login:
            self._refresh_button()
            if auto:
                self._autoupdate()
            if manual :
                if filepath == "":
                    self.utility.infoMsg('Please provide the URL of the binary file.')
                    return
                self._manuallyuploadFW(path=filepath)
        else:
            self.utility.infoMsg('Ignoring the test cause the login is failed.')

    def snapshot(self, loadsnapshot=True, snapshot_member=1):
        self._loginTab(tab='snapshot')
        self.itemname = "Update Manager: Create Snapshot"
        self.utility.unittest(test_item_name=self.itemname)


        if self.login:
            # Create Snapshot
            try:
                self.utility.infoMsg('Click create button')
                self.Browser.find_element_by_css_selector('div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-toolbar"] div div a:nth-child(1)').click()
                time.sleep(10)
                self.utility.infoMsg('Apply...')
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-message-box"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span').click()
                time.sleep(10)
                self._detectPopupWindow()
                if self.status:
                    self.utility.fail_test(testname=self.itemname)
                else:
                    self.utility.pass_test(testname=self.itemname)
            except InvalidSelectorException as e1:
                self.utility.fail_test(testname=self.itemname, error='Create Snapshot button cannot be clicked.'+ e1.msg)
                pass
            except NoSuchElementException as e2:
                self.utility.fail_test(testname=self.itemname, error='Create Snapshot button is not found.'+ e2.msg)
                pass
            except ElementNotVisibleException as e3:
                self.utility.fail_test(testname=self.itemname, error='Create Snapshot button is invisibled.'+ e3.msg)
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname, error='Create Snapshot: Exception just happened.')
                pass

            if loadsnapshot:
                self.itemname = "Update Manager: Load Snapshot"
                self.utility.unittest(test_item_name=self.itemname)
                snapshot_member_int = int(snapshot_member)
                # Load snapshot
                try:
                    self.utility.infoMsg('Pick up a snapshot from history list.')
                    self.Browser.find_element_by_css_selector('div[id$="-snapshots-body"][class*="x-panel-body"] div div[class*="x-grid-item-container"] table:nth-child(%d) tbody tr' % snapshot_member_int).click()
                    time.sleep(5)
                    self.utility.infoMsg('Click to load the snpashot.')
                    self.Browser.find_element_by_css_selector(
                        'div[id$="-center-body"] div[class*="x-panel"] div[class*="x-panel-body"] div:nth-child(2) div[class*="x-toolbar"] div div a:nth-child(2)').click()
                    time.sleep(10)
                    self.utility.infoMsg('Apply.')
                    self.Browser.find_element_by_css_selector(
                        'div[id^="messagebox-"][class*="x-message-box"] div[class*="x-toolbar"] div[class*="x-box-inner"] div[class*="x-box-target"] a:nth-child(2) span').click()
                    time.sleep(30)
                    self.utility.pass_test(testname=self.itemname)
                except InvalidSelectorException as e1:
                    self.utility.fail_test(testname=self.itemname, error='Load Snapshot button cannot be clicked.'+ e1.msg)
                    pass
                except NoSuchElementException as e2:
                    self.utility.fail_test(testname=self.itemname, error='Load Snapshot button is not found.'+ e2.msg)
                    pass
                except ElementNotVisibleException as e3:
                    self.utility.fail_test(testname=self.itemname, error='Load Snapshot button is invisibled.'+ e3.msg)
                    pass
                except Exception:
                    self.utility.fail_test(testname=self.itemname, error='Load Snapshot: Exception just happened.')
                    pass
            else:
                self.utility.infoMsg('Ignoring the test cause the login is failed.')



if __name__ == "__main__" :
    testing = updatemanager()
    testing.updatemanager110(auto=False, manual=True, filepath="/home/jeffrey/Downloads/gtech.deb")
    testing.snapshot(snapshot_member=10)
    testing._logout()