'''
Created on May 11, 2018
@Author: Philip Yang

Full Title: GRACK RAID span and shrink features.

'''
import time
import device_info
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException


class span_shrink():

    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='storage')
        self.pickup = False

    def _pickupraid(self):
        time.sleep(2)
        try:
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table[class*="x-grid-item-selected"]:nth-child(1)').is_displayed()
            self.pickup = True
        except:
            pass

    def span(self):
        try:
            self.utility.unittest('Span testing')
            self.utility.infoMsg('*** Span testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(60)

            self.utility.infoMsg('Pick up a RAID...')
            # self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidname).click()
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)

            self._pickupraid()
            while self.pickup is False:
                self.utility.infoMsg('...click RAID again.')
                self.Browser.find_element_by_css_selector('div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
                time.sleep(3)
                self._pickupraid()

            self.utility.infoMsg('Click span button')
            self.Browser.find_element_by_css_selector('span[id$="-grow-btnWrap"]').click()
            time.sleep(8)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname='Span testing',
                                         error='Span testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname='Span testing', error='Span testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname='Span testing',
                                         error='Span testing: Exception has happened. ' + e.message)


        try:
            self.utility.infoMsg('Select the 1st drive for spanning.')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[class^="x-autocontainer-innerCt"][id^="checkboxgridfield"] div div[id^="ext-comp"] div[class*="x-grid-view"]  div[class*="x-grid-item-container"] table:nth-child(1) tbody tr td:nth-child(1)').click()
            time.sleep(5)
        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname='Span testing',
                                         error='Span testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Please make sure you have provide other disk for spanning:' + e2.msg)
            self.utility.fail_test(testname='Span testing', error='Span testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname='Span testing',
                                         error='Span testing: Exception has happened. ' + e.message)


        try:
            self.Browser.find_element_by_css_selector(' span[id$="-ok-btnWrap"] ').click()
            self.utility.infoMsg('Saving')
            time.sleep(30)

            # Catch messagebox
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div:nth-child(3) div div a:nth-child(1) span').click()
            self.catchMsgbox(unit_test_name='Span testing')
            time.sleep(5)
            self.utility.pass_test(testname='Span testing', result='Spanning is finished.')
            time.sleep(10)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname='Span testing',
                                         error='Span testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to save the status. Elemnet is not exist:' + e2.msg)
            self.utility.fail_test(testname='Span testing', error='Span testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname='Span testing',
                                         error='Span testing: Exception has happened. ' + e.message)


    def shrink(self):
        try:
            self.utility.unittest('Shrink testing')
            self.utility.infoMsg('*** Shrink testing ***')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[id^="workspace-node-tab"] div:nth-child(3) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(3) span').click()
            time.sleep(60)

            self.utility.infoMsg('Pick up a RAID...')
            # self.Browser.find_element_by_xpath("//*[contains(text(), '%s')]" % raidname).click()
            self.Browser.find_element_by_css_selector(
                'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
            time.sleep(5)

            self._pickupraid()
            while self.pickup is False:
                self.utility.infoMsg('...click RAID again.')
                self.Browser.find_element_by_css_selector(
                    'div[id$="-center-body"] div div[class*="x-panel-body"] div[class*="x-grid-view"] div[class*="x-grid-item-container"] table:nth-child(1) tbody tr').click()
                time.sleep(3)
                self._pickupraid()

            self.Browser.find_element_by_css_selector('span[id$="-remove-btnWrap"]').click()
            time.sleep(5)

        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname='Shrink testing',
                                         error='Shrink testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to select tab button. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname='Shrink testing', error='Shrink testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname='Shrink testing',
                                         error='Shrink testing: Exception has happened. ' + e.message)

        try:
            self.utility.infoMsg('Select the 3rd drive for removing.')
            time.sleep(5)
            self.Browser.find_element_by_css_selector(
                'div[class^="x-autocontainer-innerCt"][id^="checkboxgridfield"] div div[id^="ext-comp"] div[class*="x-grid-view"]  div[class*="x-grid-item-container"] table:nth-child(3) tbody tr td:nth-child(1)').click()
            time.sleep(5)
        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname='Shrink testing',
                                         error='Shrink testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to remove disk. Elemnet is missing:' + e2.msg)
            self.utility.fail_test(testname='Shrink testing', error='Shrink testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname='Shrink testing',
                                         error='Shrink testing: Exception has happened. ' + e.message)

        try:
            self.Browser.find_element_by_css_selector(' span[id$="-ok-btnWrap"] ').click()
            self.utility.infoMsg('Saving')
            time.sleep(15)
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div:nth-child(3) div div a:nth-child(2) span').click()
            self.utility.infoMsg('Apply')
            time.sleep(8)
            self.utility.pass_test(testname='Shrink testing', result='Removing is finished.')
            time.sleep(0.5)


        except InvalidSelectorException as e1:
            self.utility.infoMsg('Element is unselectable: ' + e1.msg)
            self.utility.fail_test(testname='Shrink testing',
                                         error='Shrink testing: Stuck in the popup window.')
            pass
        except NoSuchElementException as e2:
            self.utility.infoMsg('Failed to save the status. Elemnet is not exist:' + e2.msg)
            self.utility.fail_test(testname='Shrink testing', error='Shrink testing: ' + e2.msg)
            pass
        except Exception as e:
            self.utility.fail_test(testname='Shrink testing',
                                         error='Shrink testing: Exception has happened. ' + e.message)

    # Hint to balance
    def catchMsgbox(self, unit_test_name):
        try:
            self.Browser.find_element_by_css_selector(
                'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            return
        except NoSuchElementException:
            self.utility.infoMsg('Cannot catch the pop-up messagebox after spanning.')
            pass
        except Exception as e:
            self.utility.warn_test(testname=unit_test_name,
                                   warning='Failed to determine the balancing status.' + e.message)

    def logout(self):
        self.utility.infoMsg('Preparing to logout.')
        time.sleep(10)
        self.utility.web_logout()
        time.sleep(10)
        self.utility.get_test_results()

if __name__ == "__main__" :

    alltesting = span_shrink()
    alltesting.span()
    alltesting.shrink()
    alltesting.logout()