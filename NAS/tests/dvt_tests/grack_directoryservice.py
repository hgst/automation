'''
    Created on September 18th, 2018
    @Author: Philip.Yang

    Full Title: AccessControl/Directory Service
    FW: 1.1.0

'''
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidSelectorException
from selenium.common.exceptions import ElementNotVisibleException
import time
import device_info


_directoryservice_member_position ={
    'host' : 'input[name*="host"]',
    'port' : 'input[name*="port"]',
    'base' : 'input[name*="base"]',
    'rootbinddn' : 'input[name*="rootbinddn"]',
    'rootbindpw' : 'input[name*="rootbindpw"]',
    'usersuffix' : 'input[name*="usersuffix"]',
    'groupsuffix' : 'input[name*="groupsuffix"]',
    'extraoptions' : 'textarea[name*="extraoptions"]',
    'extraclientoptions' : 'textarea[name*="extraclientoptions"]',
    'directory_switch' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div div fieldset div div div div:nth-child(1) div div input',
    'ssl_tls_switch' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div div fieldset div div div div:nth-child(4) div div input',
}

_directoryservice_switchmember_status ={
    'directory_switch' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div div fieldset div div div div[class*="x-form-cb-checked"]:nth-child(1)',
    'ssl_tls_switch' : 'div[id$="-center-body"] div[class*="x-panel"] div[id$="-body"] div div fieldset div div div div[class*="x-form-cb-checked"]:nth-child(4)',
}


_directoryservice_inputmember = { 'host', 'port', 'base', 'rootbinddn', 'rootbindpw', 'usersuffix', 'groupsuffix', 'extraoptions', 'extraclientoptions'}

_directoryservice_switchmember = {'directory_switch' , 'ssl_tls_switch'}

class directoryservice():
    def __init__(self):
        self.utility = device_info.device_info()
        self.Browser = self.utility.browser
        self.utility.web_login(tabname='accessControl')
        self.itemname = 'Directory Service'
        self.login = False
        self.switch = False

    def _loginTab(self):
        # Click tab of Monitoring
        try:
            self.utility.infoMsg('Click Tab of Directory Service')
            self.Browser.find_element_by_css_selector('div[class*="x-panel-body-default"] div:nth-child(2) div[id^="tabbar"] div[id$="-body"] div[id$="-innerCt"] div a:nth-child(4) span').click()
            time.sleep(8)
            self.login = True
        except InvalidSelectorException:
            self.utility.fail_test(testname=self.itemname, error='Directory Service tab button cannot be clicked.')
            pass
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname, error='Directory Service tab button is not found. To make sure the FW version is over 1.1.0')
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname, error='Directory Service tab is invisible.')
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname, error='Directory Service: Exception just happened. Login has some probelems.')
            pass

    def test(self, Functionswitch=True, SSLTLSswitch= True, host='192.168.11.65', port='389', base="dc=sldap, dc=com", rootbinddn="uid=root, cn=users, dc=sldap, dc=com", rootbindpw='11111', usersuffix="cu=users", groupsuffix="cn=groups", extraoptions="", extraclientoptions=""):

        self._loginTab()
        self.utility.unittest(test_item_name=self.itemname)
        if self.login:
            for member in _directoryservice_switchmember:
                if member == 'directory_switch':
                    switchstatus = Functionswitch
                else:
                    switchstatus = SSLTLSswitch

                try:
                    self._getSwitchStatus(member)
                    if switchstatus == self.switch:
                        pass
                    else:
                        self.utility.infoMsg('Change the switch %s status.' % member)
                        self.Browser.find_element_by_css_selector(_directoryservice_member_position[member]).click()
                        self._checkMessageBox()
                        if self.enable == False:
                            self.utility.fail_test(testname=self.itemname, error='Please turn off the ActiveDirectory first.')
                            return
                        time.sleep(3)
                except InvalidSelectorException:
                    self.utility.fail_test(testname=self.itemname, error='Directory Service tab button cannot be clicked.')
                    pass
                except NoSuchElementException:
                    self.utility.fail_test(testname=self.itemname, error='Directory Service tab button is not found.')
                    pass
                except ElementNotVisibleException:
                    self.utility.fail_test(testname=self.itemname, error='Directory Service tab button is invisible.')
                    pass
                except Exception:
                    self.utility.fail_test(testname=self.itemname, error='Directory Service: Exception just happened. Login has some probelems.')
                    pass

            #replace the value

            self._replaceValue(value=host, inputmember='host')
            self._replaceValue(value=port, inputmember='port')
            self._replaceValue(value=base, inputmember='base')
            self._replaceValue(value=rootbinddn, inputmember='rootbinddn')
            self._replaceValue(value=rootbindpw, inputmember='rootbindpw')
            self._replaceValue(value=usersuffix, inputmember='usersuffix')
            self._replaceValue(value=groupsuffix, inputmember='groupsuffix')
            self._replaceValue(value=extraoptions, inputmember='extraoptions')
            self._replaceValue(value=extraclientoptions, inputmember='extraclientoptions')

            try:
                self.utility.infoMsg('Click Save button')
                self.Browser.find_element_by_css_selector('span[id$="ok-btnWrap"]').click()
                time.sleep(5)
                self.utility.infoMsg('Confirm the change')
                self.Browser.find_element_by_css_selector(
                    'div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(2) span').click()
                time.sleep(30)
            except InvalidSelectorException:
                self.utility.fail_test(testname=self.itemname,
                                       error='Directory Service OK or Apply button cannot be clicked.')
                pass
            except NoSuchElementException:
                self.utility.fail_test(testname=self.itemname,
                                       error='Directory Service OK or Apply button is not found.')
                pass
            except ElementNotVisibleException:
                self.utility.fail_test(testname=self.itemname,
                                   error='Directory Service OK or Apply button is invisible.')
                pass
            except Exception:
                self.utility.fail_test(testname=self.itemname,
                                       error='Directory Service: Exception just happened. Login has some probelems.')
                pass
            try:
                self.utility.infoMsg('Try to pick up popup message.')
                comment = self.Browser.find_element_by_css_selector(
                    'div[id^="messagebox"][class*="x-message-box"] div[id^="messagebox"][class*="x-window-body"] div div div[class*="x-container"] div div div[class*="x-container"] div div div[class*="x-component"]').text
                self.utility.infoMsg(msg=comment)
                self.Browser.find_element_by_css_selector(
                    'div[id^="messagebox"][class*="x-box-inner"] > div[id^="messagebox"][class*="x-box-target"] > a:nth-child(1) > span').click()
                self.utility.pass_test(testname=self.itemname)
                return
            except:
                self.utility.warn_test(testname=self.itemname, warning='Confirm window is invisible.')
                pass

    def _getSwitchStatus(self, statuscheck):
        try:
            self.Browser.find_element_by_css_selector(_directoryservice_switchmember_status[statuscheck]).is_displayed()
            self.switch=True
            return
        except:
            self.switch=False
            pass

    def _checkMessageBox(self):
        try:
            self.Browser.find_element_by_css_selector('div[class*="x-message-box"] div[class*="x-toolbar"] div div a:nth-child(1) span').click()
            self.utility.infoMsg('Pick up Enable message.')
            self.enable = False
            return
        except:
            self.enable = True
            pass

    def _replaceValue(self, value, inputmember):
        try:
            _value = self.Browser.find_element_by_css_selector(_directoryservice_member_position[inputmember])
            _value.clear()
            _value.send_keys(value)
            time.sleep(2)
        except NoSuchElementException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Directory Service input member %s is not found.' % inputmember)
            pass
        except ElementNotVisibleException:
            self.utility.fail_test(testname=self.itemname,
                                   error='Directory Service input member %s is invisible.' % inputmember)
            pass
        except Exception:
            self.utility.fail_test(testname=self.itemname,
                                   error='Directory Service: Exception just happened. The %s has some probelems.' % inputmember )
            pass

    def _logout(self):
        self.utility.get_test_results()
        time.sleep(8)
        self.utility.web_logout()

if __name__ == "__main__" :
    testing = directoryservice()
    testing.test(Functionswitch=True)
    testing._logout()

    testing2 = directoryservice()
    testing2.test(Functionswitch=False)
    testing2._logout()