__author__ = 'erica.lee@wdc.com'

import os
import sys
from requests import Request

WORKSPACE = os.environ["WORKSPACE"]
InventoryAPI = "/configs/docker/aat/aat/NAS/tools/InventoryAPI"
sys.path.append(WORKSPACE+InventoryAPI)

import InventoryAPI
invApi = InventoryAPI.InventoryAPI()

product_info = getattr(invApi.deviceApi, 'get')(os.environ.get('DEVICE_ID'))
daemon = ''
config = {
          'serialUser': 'buildmeister',
          'serialPassword':'fituser99',
          'ssh_password': 'welc0me',
          'uut_browser': 'firefox',
          'wf_browser': 'chrome',
          'wf_server': 'stage6',
          'execution_plan': 'my plan',
          'reboottype': 'interrupted',
          'testminutes': 5760,
          'is_testable': True}

config.update({'internalIPAddress':product_info['device']['internalIPAddress']})
config.update({'macAddress':product_info['device']['macAddress']})

for key, value in product_info['device'].items():
    if type(value) is dict:
        default_url = 'http://sevtwautoweb.labspan.wdc.com/InventoryServer'
        if key == 'product':
            daemon = 'products'
            url = str.format("{0}/{1}/{2}", default_url, daemon, value['id'])
            r = invApi.restCall(Request('GET', url))
            config.update({'product': r[key]['name']})
        elif key == 'sshGateway':
            daemon = 'SSHGateways'
            url = str.format("{0}/{1}/{2}", default_url, daemon, value['id'])
            r = invApi.restCall(Request('GET', url))
            config.update({'sshGatewayPort': r[key]['port']})

        elif key == 'serialServer':
            daemon = 'serialServers'
            url = str.format("{0}/{1}/{2}", default_url, daemon, value['id'])
            r = invApi.restCall(Request('GET', url))
            config.update({'serialServer': r[key]['ipAddress'], 'serialServerPort': r[key]['port']})

        elif key == 'powerSwitch':
            daemon = 'powerSwitches'
            url = str.format("{0}/{1}/{2}", default_url, daemon, value['id'])
            r =invApi.restCall(Request('GET', url))
            config.update({'powerSwitchPort': r[key]['port'], 'powerSwitch':r[key]['ipAddress']})

os.chdir(WORKSPACE+'/configs/docker/aat/aat/NAS/tests/aat_tests')
with open("test_device.txt", "w") as f:
    f.write(str(config))
