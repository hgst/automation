'''
Created on Jan 19, 2016

@author: tsui_b

@brief: Command line interface for Inventory Server Product commands
'''

import argparse
import InventoryAPI

class InventortyProduct(object):
    def __init__(self):
        """
        Command line interface to get or update Product state in Inventory Server
        """
        # Overide the length limit of help texts
        formatter_class=lambda prog: MyFormatter(prog, max_help_position=55, width=100)
        parser = argparse.ArgumentParser(formatter_class=formatter_class)
        parser.add_argument('-url', help='Inventory Server URL or IP Address')
        # Create parent parsers for common arguments
        parent_productId = self.parent_parser(argument='productId',
                                              type=int, required=True, 
                                              help='The ID of specified Product')
        parent_name = self.parent_parser(argument='name', required=True, 
                                         help='The name of specified Product')
        # Create sub parsers
        subparsers = parser.add_subparsers(metavar='COMMAND', dest='command')
        parser_list = subparsers.add_parser('list', help='List all Product info')
        parser_get = subparsers.add_parser('get', help='Get specified Product info by Product ID',
                                           formatter_class=formatter_class,
                                           parents=[parent_productId])
        parser_create = subparsers.add_parser('create', help='Create a new Product in list',
                                              formatter_class=formatter_class,
                                              conflict_handler='resolve',
                                              parents=[parent_name])
        parser_update = subparsers.add_parser('update', help='Update a Product from list',
                                              formatter_class=formatter_class,
                                              conflict_handler='resolve',
                                              parents=[parent_productId, parent_name])
        parser_delete = subparsers.add_parser('delete', help='Delete a Product from list',
                                              formatter_class=formatter_class,
                                              parents=[parent_productId])
        parser_getDevices = subparsers.add_parser('getDevices', help='Get device info by Product ID',
                                                  formatter_class=formatter_class,
                                                  parents=[parent_productId])
        args = parser.parse_args()
        self.run(args)

    def run(self, args):
        '''
        Get Inventory command and server url first, 
        remove them from args Namespace, and send the rest arguments to InventoryAPI
        '''
        server_url = args.url
        if server_url and not server_url.startswith('http://'):
            server_url = 'http://' + server_url
        cmd = args.command
        del args.url
        del args.command
        invApi = InventoryAPI.InventoryAPI(server_url)
        getattr(invApi.productApi, cmd)(**vars(args))

    def parent_parser(self, argument, **kwarg):
        parent_parser = argparse.ArgumentParser(add_help=False)
        parent_parser.add_argument('--{}'.format(argument), **kwarg)
        return parent_parser

class MyFormatter(argparse.HelpFormatter):
    """
    Corrected _max_action_length for the indenting of subactions
    """
    def add_argument(self, action):
        if action.help is not argparse.SUPPRESS:
            # find all invocations
            get_invocation = self._format_action_invocation
            invocations = [get_invocation(action)]
            current_indent = self._current_indent
            for subaction in self._iter_indented_subactions(action):
                # compensate for the indent that will be added
                indent_chg = self._current_indent - current_indent
                added_indent = 'x' * indent_chg
                invocations.append(added_indent+get_invocation(subaction))

            # update the maximum item length
            invocation_length = max([len(s) for s in invocations])
            action_length = invocation_length + self._current_indent
            self._action_max_length = max(self._action_max_length, action_length)

            # add the item to the list
            self._add_item(self._format_action, [action])

if __name__ == "__main__":
    InventortyProduct()
