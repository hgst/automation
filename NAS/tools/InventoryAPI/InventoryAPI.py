'''
Created on Jan 19, 2016

@author: tsui_b

@brief: Inventory Server Rest commands with 5 different devices
'''

import logging
import time
from requests import Request, Session

LOG_FILENAME = 'inventoryapi.log'
MAX_DEVICE_NUM = 999

class InventoryAPI(object):
    def __init__(self, server_url=None, logger=None):
        """ 
        Inventory Server REST client
        @ param url: Optional URL for the inventory server
        @ param logger: Optional logger to use for debug and error messages
        """
        if server_url:
            self.url = server_url
        else:
            self.url = 'http://sevtwautoweb.labspan.wdc.com/InventoryServer'
        
        if logger:
            self.log = logger
        else:
            self.log = createLogger('InventoryAPI')

        self._session = Session()
        # Default headers
        self.headers = {'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
        # References to encapsulated APIs
        self.deviceApi = InventoryAPI.DeviceAPI(self)
        self.powerSwitchApi = InventoryAPI.PowerSwitchAPI(self)
        self.serialServerApi = InventoryAPI.SerialServerAPI(self)
        self.sshGatewayApi = InventoryAPI.SSHGatewayAPI(self)
        self.productApi = InventoryAPI.ProductAPI(self)

    def restCall(self, request):
        preparedRequest = self._session.prepare_request(request)
        preparedRequest.headers = self.headers
        response = self._session.send(preparedRequest)
        self.log.info("REST Call: {}".format(response.url))
        jsonResponse = response.json()
        self.log.info("JSON Response: {}".format(jsonResponse))

        if jsonResponse is {} and jsonResponse['statusCode'] != 1000:
            raise InventoryException(jsonResponse['status'], jsonResponse['statusCode'], jsonResponse['message'])

        return response.json()

    class DeviceAPI(object):
        def __init__(self, invApi):
            self.invApi = invApi
            self.log = createLogger('Inventoryserverapi-deviceapi')

        def list(self):
            self.log.info('List all devices')
            url = str.format("{0}/devices", self.invApi.url)
            data = {'max':MAX_DEVICE_NUM}
            request = Request('GET', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def get(self, deviceId):
            self.log.info('Get device with device ID:{}'.format(deviceId))
            url = str.format("{0}/devices/{1}", self.invApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def create(self, macAddress, productId, internalIPAddress=None, location=None, firmware=None,
                   powerSwitchId=None,
                   sshGatewayId=None, serialServerId=None):
            url = str.format("{0}/devices", self.invApi.url)
            data = {'macAddress': macAddress, 'product': productId, 'internalIPAddress': internalIPAddress,
                    'location': location, 'firmware': firmware,
                    'powerSwitch': powerSwitchId, 'sshGateway': sshGatewayId, 'serialServer': serialServerId}
            self.log.info('Create device with following info:{}'.format(data))
            request = Request('POST', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def update(self, deviceId, macAddress=None, productId=None, internalIPAddress=None, location=None,
                   firmware=None, powerSwitchId=None,
                   sshGatewayId=None, serialServerId=None, isBusy=None, isOperational=None, jenkinsJob=None):
            url = str.format("{0}/devices/{1}", self.invApi.url, deviceId)
            data = {'macAddress': macAddress, 'product': productId, 'internalIPAddress': internalIPAddress,
                    'location': location, 'firmware': firmware,
                    'powerSwitch': powerSwitchId, 'sshGateway': sshGatewayId, 'serialServer': serialServerId,
                    'isBusy': isBusy, 'isOperational': isOperational, 'jenkinsJob': jenkinsJob}
            self.log.info('Update device:{0} with following info:{1}'.format(deviceId, data))
            request = Request('PUT', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def delete(self, deviceId):
            self.log.info('Delete device with device ID:{}'.format(deviceId))
            url = str.format("{0}/devices/{1}", self.invApi.url, deviceId)
            request = Request('DELETE', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def checkIn(self, deviceId):
            self.log.info('Check in decive with device ID:{}'.format(deviceId))
            url = str.format("{0}/api/device/checkIn/{1}", self.invApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse['checkedIn']

        def checkOut(self, deviceId, jenkinsJob, force=False):
            self.log.info('Check out decive with device ID:{0} and Jenkins Job:{1}, force={2}'.format(deviceId, jenkinsJob, force))
            url = str.format("{0}/api/device/checkOut/{1}", self.invApi.url, deviceId)
            params = {'jenkinsJob': jenkinsJob, 'force': force}
            request = Request('GET', url, params=params)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse['checkedOut']

        def checkOutRetry(self, productName, firmwareVersion='', jenkinsJob='', retryCounts='', retryDelay=''):
            if not retryCounts:
                retryCounts = self.retryCounts

            if not retryDelay:
                retryDelay = self.retryDelay

            count = 0
            found_device = False
            # Keep looking for available device
            while (count < int(retryCounts)) and (found_device is False):
                # Get the list of products
                product_list = self.invApi.productApi.list()['list']

                # Get the product we want to checkout
                print 'Product looking for: {}'.format(productName)
                product = None
                for p in product_list:
                    if str(p['name']) == productName:
                        product = p
                        break

                if product is None:
                    print 'ERROR: Product:{} is not found in product list!'.format(productName)
                    return False

                # Get devices for a product
                devices = self.invApi.productApi.getDevices(product['id'])['devices']
                if not devices:
                    print 'ERROR: No devices for product:{}'.format(productName)
                    return False

                # Get an available device with same firmware
                uut = None
                for d in devices:
                    if (d['isOperational'] is True) and (d['isBusy'] is False):
                        if firmwareVersion and (d['firmware'] != firmwareVersion):
                            continue
                        uut = d
                        found_device = True
                        break

                if uut is None:
                    count += 1
                    print 'No available devices, retry after {0} seconds, remaining {1} retries...'.format(retryDelay, int(retryCounts) - count)
                    time.sleep(int(retryDelay))

            if uut is None:
                print 'ERROR: Failed to find uut:{0} with FW:{1}'.format(productName, firmwareVersion)
                return False

            # CheckOut the device
            if not self.checkOut(uut['id'], jenkinsJob):
                print 'ERROR: Failed to check out the device with ID:{}'.format(uut['id'])
                return False
            else:
                return uut['id']

        def isAvailable(self, deviceId):
            self.log.info('Check if device:{} is available to use'.format(deviceId))
            url = str.format("{0}/api/device/isAvailable/{1}", self.invApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse['isAvailable']

        def getSSHGateway(self, deviceId):
            self.log.info('Get SSH Gateway info with device ID:{}'.format(deviceId))
            url = str.format("{0}/api/device/getSSHGateway/{1}", self.invApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse['sshGateway']

        def getSerialServer(self, deviceId):
            self.log.info('Get Serial Server info with device ID:{}'.format(deviceId))
            url = str.format("{0}/api/device/getSerialServer/{1}", self.invApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse['serialServer']

        def getPowerSwitch(self, deviceId):
            self.log.info('Get Power Switch info with device ID:{}'.format(deviceId))
            url = str.format("{0}/api/device/getPowerSwitch/{1}", self.invApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse['powerSwitch']

        def getDeviceByJob(self, jenkinsJob):
            self.log.info('Getting device info with Jenkins Job:{}'.format(jenkinsJob))
            url = str.format("{0}/api/device/getDeviceByJob", self.invApi.url)
            params = {'jenkinsJob': jenkinsJob}
            request = Request('GET', url, params=params)
            jsonResponse = self.invApi.restCall(request)
            self.log.debug('Response = ' + str(jsonResponse))
            return jsonResponse['device']

    class PowerSwitchAPI(object):
        def __init__(self, invApi):
            self.invApi = invApi
            self.log = createLogger('Inventoryserverapi-powerswitchapi')

        def list(self):
            self.log.info('list all Power Switches')
            url = str.format("{0}/powerSwitches", self.invApi.url)
            data = {'max':MAX_DEVICE_NUM}
            request = Request('GET', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def get(self, powerSwitchId):
            self.log.info('Get Power Switch with ID:{}'.format(powerSwitchId))
            url = str.format("{0}/powerSwitches/{1}", self.invApi.url, powerSwitchId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def create(self, ipAddress, port):
            self.log.info('Create Power Switch with IP Address:{0} and port:{1}'.format(ipAddress, port))
            url = str.format("{0}/powerSwitches", self.invApi.url)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('POST', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def update(self, powerSwitchId, ipAddress=None, port=None):
            self.log.info('Update Power Switch:{0} with IP Address:{1} and port:{2}'.format(powerSwitchId, ipAddress, port))
            url = str.format("{0}/powerSwitches/{1}", self.invApi.url, powerSwitchId)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('PUT', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def delete(self, powerSwitchId):
            self.log.info('Delete Power Switch with ID:{}'.format(powerSwitchId))
            url = str.format("{0}/powerSwitches/{1}", self.invApi.url, powerSwitchId)
            request = Request('DELETE', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

    class SerialServerAPI(object):
        def __init__(self, invApi):
            self.invApi = invApi
            self.log = createLogger('Inventoryserverapi-serialserverapi')

        def list(self):
            self.log.info('list all Serial Servers')
            url = str.format("{0}/serialServers", self.invApi.url)
            data = {'max':MAX_DEVICE_NUM}
            request = Request('GET', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def get(self, serialServerId):
            self.log.info('Get Serial Server with ID:{}'.format(serialServerId))
            url = str.format("{0}/serialServers/{1}", self.invApi.url, serialServerId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def create(self, ipAddress, port):
            self.log.info('Create Serial Server with IP Address:{0} and port:{1}'.format(ipAddress, port))
            url = str.format("{0}/serialServers", self.invApi.url)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('POST', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def update(self, serialServerId, ipAddress=None, port=None):
            self.log.info('Update Serial Server:{0} with IP Address:{1} and port:{2}'.format(serialServerId, ipAddress, port))
            url = str.format("{0}/serialServers/{1}", self.invApi.url, serialServerId)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('PUT', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def delete(self, serialServerId):
            self.log.info('Delete Serial Server with ID:{}'.format(serialServerId))
            url = str.format("{0}/serialServers/{1}", self.invApi.url, serialServerId)
            request = Request('DELETE', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

    class SSHGatewayAPI(object):
        def __init__(self, invApi):
            self.invApi = invApi
            self.log = createLogger('Inventoryserverapi-sshgatewayapi')

        def list(self):
            self.log.info('list all SSH Gateways')
            url = str.format("{0}/SSHGateways", self.invApi.url)
            data = {'max':MAX_DEVICE_NUM}
            request = Request('GET', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def get(self, sshGatewayId):
            self.log.info('Get SSH Gateway with ID:{}'.format(sshGatewayId))
            url = str.format("{0}/SSHGateways/{1}", self.invApi.url, sshGatewayId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def create(self, ipAddress, port):
            self.log.info('Create SSH Gateway with IP Address:{0} and port:{1}'.format(ipAddress, port))
            url = str.format("{0}/SSHGateways", self.invApi.url)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('POST', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def update(self, sshGatewayId, ipAddress=None, port=None):
            self.log.info('Update SSH Gateway:{0} with IP Address:{1} and port:{2}'.format(sshGatewayId, ipAddress, port))
            url = str.format("{0}/SSHGateways/{1}", self.invApi.url, sshGatewayId)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('PUT', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def delete(self, sshGatewayId):
            self.log.info('Delete SSH Gateway with ID:{}'.format(sshGatewayId))
            url = str.format("{0}/SSHGateways/{1}", self.invApi.url, sshGatewayId)
            request = Request('DELETE', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

    class ProductAPI(object):
        def __init__(self, invApi):
            self.invApi = invApi
            self.log = createLogger('Inventoryserverapi-productapi')

        def list(self):
            self.log.info('list all Products')
            url = str.format("{0}/products", self.invApi.url)
            data = {'max':MAX_DEVICE_NUM}
            request = Request('GET', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def get(self, productId):
            self.log.info('Get Product with device ID:{}'.format(productId))
            url = str.format("{0}/products/{1}", self.invApi.url, productId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def create(self, name):
            self.log.info('Create Product with product name:{}'.format(name))
            url = str.format("{0}/products", self.invApi.url)
            data = {'name': name}
            request = Request('POST', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def update(self, productId, name):
            self.log.info('Update Product:{0} with product name:{1}'.format(productId, name))
            url = str.format("{0}/products/{1}", self.invApi.url, productId)
            data = {'name': name}
            request = Request('PUT', url, params=data)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def delete(self, productId):
            self.log.info('Delete Product with ID:{}'.format(productId))
            url = str.format("{0}/products/{1}", self.invApi.url, productId)
            request = Request('DELETE', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

        def getDevices(self, productId):
            self.log.info('Get the devices with product ID:{}'.format(productId))
            url = str.format("{0}/api/product/getDevices/{1}", self.invApi.url, productId)
            request = Request('GET', url)
            jsonResponse = self.invApi.restCall(request)
            return jsonResponse

def createLogger(logname):
    # set up logging to file
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=LOG_FILENAME,
                        filemode='w')
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(formatter)
    logger = logging.getLogger(logname)
    logger.addHandler(console)
    return logger

class InventoryException(Exception):
    def __init__(self, status=None, statusCode=None, message=None):
        self.status = status
        self.statusCode = statusCode
        self.message = message