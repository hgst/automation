'''
Created on Jan 19, 2016

@author: tsui_b

@brief: Command line interface for Inventory Server Devices commands
'''

import argparse
import InventoryAPI

class InventoryDevices(object):
    def __init__(self):
        """
        Command line interface to get or update Device state in Inventory Server
        """
        # Overide the length limit of help texts
        formatter_class=lambda prog: MyFormatter(prog, max_help_position=55, width=100)
        parser = argparse.ArgumentParser(formatter_class=formatter_class)
        parser.add_argument('-url', help='Inventory Server URL or IP Address')
        # Create parent parsers for common arguments
        parent_deviceId = self.parent_parser(argument='deviceId', type=int, required=True, help='The ID of specified device')
        parent_jenkinsJob = self.parent_parser(argument='jenkinsJob', required=True, help='Jenkins job name')
        parent_internalIPAddress = self.parent_parser(argument='internalIPAddress', help='Device IP Address')
        parent_location = self.parent_parser(argument='location', help='Device Location')
        parent_firmware = self.parent_parser(argument='firmware', help='Device Firmware Version')
        parent_powerSwitchId = self.parent_parser(argument='powerSwitchId', help='Device Power Switch ID')
        parent_sshGatewayId = self.parent_parser(argument='sshGatewayId', help='Device SSH Gateway ID')
        parent_serialServerId = self.parent_parser(argument='serialServerId', help='Device Serial Server ID')
        # Create sub parsers
        subparsers = parser.add_subparsers(metavar='COMMAND', dest='command')
        subparsers.add_parser('list', help='List all devices info')
        parser_get = subparsers.add_parser('get', help='Get specified device info by device ID',
                                           parents=[parent_deviceId])
        parser_create = subparsers.add_parser('create', help='Create a new device in list',
                                              formatter_class=formatter_class,
                                              conflict_handler='resolve',
                                              parents=[parent_internalIPAddress,
                                                       parent_location, parent_firmware, parent_powerSwitchId,
                                                       parent_sshGatewayId, parent_serialServerId])
        parser_create.add_argument('--macAddress', required=True, help='Device MAC address')
        parser_create.add_argument('--productId', required=True, help='Device product name')        
        parser_update = subparsers.add_parser('update', help='Update a device from list',
                                              formatter_class=formatter_class,
                                              conflict_handler='resolve',
                                              parents=[parent_deviceId, parent_internalIPAddress,
                                                       parent_location, parent_firmware, parent_powerSwitchId,
                                                       parent_sshGatewayId, parent_serialServerId])
        parser_update.add_argument('--macAddress', help='Device MAC address')
        parser_update.add_argument('--productId', help='Device product name')
        parser_update.add_argument('--isBusy', choices=['True', 'False'], help='Device busy status')
        parser_update.add_argument('--isOperational', choices=['True', 'False'], help='Device operational status')
        parser_update.add_argument('--jenkinsJob', help='Jenkins job name')
        parser_delete = subparsers.add_parser('delete', help='Delete a device from list', 
                                              parents=[parent_deviceId])
        parser_checkin = subparsers.add_parser('checkIn', help='Checkin a device and update isBusy status to False',
                                               parents=[parent_deviceId])
        parser_checkout = subparsers.add_parser('checkOut', help='Checkout a device to use and update isBusy status to True',
                                                formatter_class=formatter_class,
                                                parents=[parent_deviceId, parent_jenkinsJob])
        parser_checkout.add_argument('--force', dest='force', action='store_true', help='Force to checkout the device')
        parser_isavilable = subparsers.add_parser('isAvailable', help='Check if specified device is available to use',
                                                  parents=[parent_deviceId])
        parser_getsshgateway = subparsers.add_parser('getSSHGateway', help='Get SSH Gateway info of specified device ID',
                                                     parents=[parent_deviceId])
        parser_getserialserver = subparsers.add_parser('getSerialServer', help='Get Serial Server info of specified device ID',
                                                       parents=[parent_deviceId])
        parser_getpowerswitch = subparsers.add_parser('getPowerSwitch', help='Get Power Switch info of specified device ID',
                                                      parents=[parent_deviceId])
        parser_getdevicebyjob = subparsers.add_parser('getDeviceByJob', help='Get device info of specified Jenkins job name',
                                                      parents=[parent_jenkinsJob])
        args = parser.parse_args()
        self.run(args)

    def run(self, args):
        '''
        Get Inventory command and server url first, 
        remove them from args Namespace, and send the rest arguments to InventoryAPI
        '''
        server_url = args.url
        if server_url and not server_url.startswith('http://'):
            server_url = 'http://' + server_url
        cmd = args.command
        del args.url
        del args.command
        invApi = InventoryAPI.InventoryAPI(server_url)
        getattr(invApi.deviceApi, cmd)(**vars(args))

    def parent_parser(self, argument, **kwarg):
        parent_parser = argparse.ArgumentParser(add_help=False)
        parent_parser.add_argument('--{}'.format(argument), **kwarg)
        return parent_parser

class MyFormatter(argparse.HelpFormatter):
    """
    Corrected _max_action_length for the indenting of subactions
    """
    def add_argument(self, action):
        if action.help is not argparse.SUPPRESS:
            # find all invocations
            get_invocation = self._format_action_invocation
            invocations = [get_invocation(action)]
            current_indent = self._current_indent
            for subaction in self._iter_indented_subactions(action):
                # compensate for the indent that will be added
                indent_chg = self._current_indent - current_indent
                added_indent = 'x' * indent_chg
                invocations.append(added_indent+get_invocation(subaction))

            # update the maximum item length
            invocation_length = max([len(s) for s in invocations])
            action_length = invocation_length + self._current_indent
            self._action_max_length = max(self._action_max_length, action_length)

            # add the item to the list
            self._add_item(self._format_action, [action])

if __name__ == "__main__":
    InventoryDevices()
