import os, unittest, sys
import mylogger
import logging

l = mylogger.Logger()
loggerT = l.myLogger()
loggerT = logging.getLogger('TestRunner')

#from UnitTestFramework.Log import Logger

#import logger from grandparent directory
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

#from sshAPI.test.SSHUnitTest import SSHTest
#from restAPI.test.RESTUnitTest import RESTUnitTest
from seleniumAPI.test.SeleniumUnitTest import SeleniumUnitTest

__all__ = ['TestRunner']


        
class TestRunner(object):
    
    def __init__(self):
        #self.logger = None
        
        #self.s = mylogger.Logger()
        #self.m = self.s.myLogger()
        #self.m.info("Begin unit testing...")
        
        

        loggerT.info("Begin unit testing...")
        
        '''
        self.currentDir = os.path.dirname(os.path.realpath(__file__))
        
        # create logger and log file
        self.logger = None
        self.logger = logging.getLogger('TestRunner')
        logging.basicConfig(filename = self.currentDir + '/AATunitTest.log', filemode='a', level=logging.DEBUG)
        
        #create file handler
        fh = logging.FileHandler('AATunitTest.log')       
        fh.setLevel(logging.DEBUG)                 
        #create console handler
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)        
        #create formatter and add to handlers
        formatter = logging.Formatter('%(asctime)s %(module)s:%(funcName)s:%(lineno)d %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)                                
        #format = logging.Formatter(fmt)
        
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)
        
        #self.debug = self.logger.debug
        '''
    def testlogging(self):
        #self.m.debug("inside function...")
        loggerT.info("inside function...")
        x = 2 + 5
        return x
    
    '''        
    def main(self):
        rc = 0
        try:
            print "begin testing"
            logger.info('i am in main of TestRunner')  
            
        except Exception,ex:
            #self.logger.writeError('Caught the following exception in the framework\n' + str(ex))
            raise Exception, 'not working...\n' + str(ex)
            print "error...."
            rc = 1
                            
        #self.logger.writeInfo('Testing complete')
        return rc 
    '''
            
if __name__ == '__main__':
         
    a = TestRunner()
    result = a.testlogging()
    
    #suite = unittest.makeSuite(SSHTest)
    suite = unittest.makeSuite(SeleniumUnitTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
                     
    #rc = TestRunner().main()
    #For some reason sys.exit() doesn't always exit,but os does
    #os._exit(rc)