'''
# Log module to be used by all unit testing modules
# Reference: http://stackoverflow.com/questions/9321741/printing-to-screen-and-writing-to-a-file-at-the-same-time

'''

import os
import time
import logging

class Logger:
    logger = None
    def myLogger(self):
        if None == self.logger:
            # set up logging to file
            logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)-15s %(module)-15s:%(funcName)-27s:%(lineno)-5d %(message)s',
                            datefmt='%m-%d-%Y %H:%M:%S',
                            filename='AATUnitTest.log',
                            filemode='w')
            # define a Handler which writes INFO messages or higher to the sys.stderr
            console = logging.StreamHandler()
            console.setLevel(logging.INFO)
            # set a format which is simpler for console use
            formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
            # tell the handler to use this format
            console.setFormatter(formatter)
            # add the handler to the root logger
            logging.getLogger('').addHandler(console)
        return self.logger
