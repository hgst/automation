""" If colorama is not installed, use this module instead. It will prevent tests from failing without it by replacing
    the colorama methods and constants with placeholders. This allows the tests to run, but will not give color
    support.
"""

def init(autoreset=False, convert=None, strip=None, wrap=True):
    pass

def deinit():
    pass

def reinit():
    pass

class MonoFore(object):
    BLACK   = ''
    RED     = ''
    GREEN   = ''
    YELLOW  = ''
    BLUE    = ''
    MAGENTA = ''
    CYAN    = ''
    WHITE   = ''
    RESET   = ''

class MonoBack(object):
    BLACK   = ''
    RED     = ''
    GREEN   = ''
    YELLOW  = ''
    BLUE    = ''
    MAGENTA = ''
    CYAN    = ''
    WHITE   = ''
    RESET   = ''

class MonoStyle(object):
    BRIGHT    = ''
    DIM       = ''
    NORMAL    = ''
    RESET_ALL = ''

Fore = MonoFore()
Back = MonoBack()
Style = MonoStyle()

