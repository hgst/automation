__author__ = 'Troy'

import os
import sys
from global_libraries.wd_environment import EnvironmentVariables


def get_silk_results_dir():
    try:
        directory = EnvironmentVariables.get('sctm_exec_resultsfolder')
    except KeyError:
        directory = os.path.dirname(os.path.abspath(sys.argv[0]))

    return directory