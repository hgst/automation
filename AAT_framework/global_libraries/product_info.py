""" Contains a set of dictionaries that define product-specific settings and attributes

"""
from system_health.src.os_modules.metrics import Metrics
from system_health.src.os_modules.metrics import Stats

RAID_JBOD = 11
RAID_SPANNING = 12

class MediaServers(object):
    # Supported media servers
    TWONKY = 'twonky'

class ProductList(object):
    # platform_list: Constants used to map a product to the name listed in the "Product" field on the Inventory Server
    DEFAULT = 'Default' # Use for the default value for attributes that nearly all products have the same value
    AURORA = 'Aurora'
    BLACK_ICE = 'Black Ice'
    GLACIER = 'Glacier'
    KINGSCANYON = 'Kings Canyon'
    LIGHTNING = 'Lightning'
    SEQUOIA = 'Sequoia'
    SPRITE = 'Sprite'
    YELLOWSTONE = 'Yellowstone'
    YOSEMITE = 'Yosemite'
    ZION = 'Zion'
    GRANDTETON = 'Grand Teton'
    RANGERPEAK = 'Ranger Peak'
    BRYCECANYON = 'Bryce Canyon'
    BLACKCANYON = 'Black Canyon'
    MIRRORMAN = 'Mirrorman'

class Attributes(object):
    # The attributes and values for each product. Be sure to add any new attributes to testdevice.py -> Fields.
    # Although it will work without adding the attribute to Fields, auto-complete will not work without this extra step.

    # The model number as reported by the firmware
    model_number = {ProductList.GLACIER: 'GLCR',
                    ProductList.AURORA: 'BBAZ',
                    ProductList.KINGSCANYON: 'KC2A',
                    ProductList.YOSEMITE: 'BWAZ',
                    ProductList.ZION: 'BZVM',
                    ProductList.LIGHTNING: 'LT4A',
                    ProductList.SPRITE: 'BNEZ',
                    ProductList.YELLOWSTONE: 'BWZE',
                    ProductList.SEQUOIA: 'sq',
                    ProductList.GRANDTETON: 'BWVZ',
                    ProductList.RANGERPEAK: 'BVBZ',
                    ProductList.BRYCECANYON: 'BBCL',
                    ProductList.BLACKCANYON: 'BNFA',
                    ProductList.MIRRORMAN: 'BAGX'}

    # The number of drives per product
    number_of_drives = {ProductList.DEFAULT: 4,
                        ProductList.SEQUOIA: 1,
                        ProductList.BLACK_ICE: 1,
                        ProductList.GLACIER: 1,
                        ProductList.AURORA: 2,
                        ProductList.KINGSCANYON: 2,
                        ProductList.YOSEMITE: 2,
                        ProductList.ZION: 2,
                        ProductList.LIGHTNING: 4,
                        ProductList.SPRITE: 4,
                        ProductList.YELLOWSTONE: 4,
                        ProductList.GRANDTETON: 2,
                        ProductList.RANGERPEAK: 2,
                        ProductList.BRYCECANYON: 2,
                        ProductList.BLACKCANYON: 4,
                        ProductList.MIRRORMAN: 1}

    # True if the device supports setting the link speed
    supports_link_speed = {ProductList.DEFAULT: True,
                           ProductList.BLACK_ICE: False,
                           ProductList.GLACIER: False,
                           ProductList.SEQUOIA: False,
                           ProductList.MIRRORMAN: False}

    # True if the device supports volume encryption
    supports_volume_encryption = {ProductList.DEFAULT: True,
                                  ProductList.ZION: False,
                                  ProductList.GRANDTETON: False}

    # Expected write performance in mebibytes/second by RAID level
    # Aurora, Sprite, Yosemite are all 105
    expected_write_speed = {ProductList.DEFAULT: {0: 90, 1: 90, 5: 65, 10: 105, RAID_JBOD: 85, RAID_SPANNING: 85},
                            ProductList.KINGSCANYON: {0: 65, 1: 65, 5: 65, 10: 65, RAID_JBOD: 65, RAID_SPANNING: 65},
                            ProductList.LIGHTNING: {0: 40, 1: 40, 5: 40, 10: 40, RAID_JBOD: 50, RAID_SPANNING: 40},
                            ProductList.YELLOWSTONE: {0: 90, 1: 90, 5: 90, 10: 90},
                            ProductList.ZION: {0: 65, 1: 65, 5: 65, 10: 65, RAID_JBOD: 65, RAID_SPANNING: 65},
                            ProductList.GRANDTETON: {0: 65, 1: 65, 5: 65, 10: 65, RAID_JBOD: 65, RAID_SPANNING: 65}}
    write_speed_tolerance = {ProductList.DEFAULT: (-.10, .20)}

    # Expected read performance in mebibytes/second
    # Aurora and Yellowstone are both 100
    expected_read_speed = {ProductList.DEFAULT: {0: 100, 1: 100, 5: 65, 10: 100, RAID_JBOD: 85, RAID_SPANNING: 85},
                           ProductList.AURORA: {RAID_JBOD: 100},
                           ProductList.KINGSCANYON: {0: 95, 1: 95, 10: 95, RAID_JBOD: 80, RAID_SPANNING: 80},
                           ProductList.LIGHTNING: {0: 90, 1: 90, 10: 90, RAID_JBOD: 80, RAID_SPANNING: 80},
                           ProductList.SPRITE: {0: 105, 1: 105, 10: 105},
                           ProductList.YOSEMITE: {0: 105, 1: 105, 10: 105},
                           ProductList.ZION: {0: 65, 1: 65, 5: 65, 10: 65, RAID_JBOD: 65, RAID_SPANNING: 65},
                           ProductList.GRANDTETON: {0: 65, 1: 65, 5: 65, 10: 65, RAID_JBOD: 65, RAID_SPANNING: 65}}
    read_speed_tolerance = {ProductList.DEFAULT: (-.10, .20)}

    # Toolchain used to build binaries. This won't actually build, but the toolchain is part of the filename on Nexus
    toolchain = {ProductList.DEFAULT: 'ARMv5',
                    ProductList.SEQUOIA: 'ARMv7_64k',
                    ProductList.YELLOWSTONE: 'Armada',
                    ProductList.YOSEMITE: 'Armada',
                    ProductList.GLACIER: 'Armada',
                    ProductList.KINGSCANYON: 'Armada',
                    ProductList.MIRRORMAN: 'Armada'}

    # group information
    group = {ProductList.LIGHTNING: 'My_Cloud_LT4A',
             ProductList.GLACIER: 'My_Cloud_GLCR',
             ProductList.KINGSCANYON: 'My_Cloud_KC2A',
             ProductList.ZION: 'My_Cloud_BZVM',
             ProductList.SPRITE: 'My_Cloud_DL4100',
             ProductList.YELLOWSTONE: 'My_Cloud_EX4100',
             ProductList.YOSEMITE: 'My_Cloud_EX2100',
             ProductList.AURORA: 'My_Cloud_DL2100',
             ProductList.BLACK_ICE: 'NA',
             ProductList.GRANDTETON: 'My_Cloud_BWVZ',
             ProductList.RANGERPEAK: 'My_Cloud_BVBZ',
             ProductList.BRYCECANYON: 'My_Cloud_PR2100',
             ProductList.BLACKCANYON: 'My_Cloud_PR4100',
             ProductList.MIRRORMAN: 'My_Cloud_BAGX'}

    # download page
    download_page = {ProductList.LIGHTNING: 'mcp4',
             ProductList.GLACIER: 'mc',
             ProductList.KINGSCANYON: 'mcp2',
             ProductList.ZION: 'mcm',
             ProductList.SPRITE: 'mcdl4100',
             ProductList.YELLOWSTONE: 'mcex4100',
             ProductList.YOSEMITE: 'mcex2100',
             ProductList.AURORA: 'mcdl2100'}

    # Media Server
    media_server = {ProductList.DEFAULT: MediaServers.TWONKY}
    media_server_user = {ProductList.DEFAULT: None}
    media_server_password = {ProductList.DEFAULT: None}
    default_media_server_user = {ProductList.DEFAULT: None}
    default_media_server_password = {ProductList.DEFAULT: None}

    # UPnP information
    default_media_server_id = {ProductList.LIGHTNING: 'VEN_011A&amp;DEV_0082&amp;REV_01',
                               ProductList.GLACIER: 'VEN_011A&amp;DEV_0095&amp;REV_01',
                               ProductList.KINGSCANYON: 'VEN_011A&amp;DEV_0091&amp;REV_01',
                               ProductList.ZION: 'VEN_011A&amp;DEV_0093&amp;REV_01',
                               ProductList.SPRITE: 'VEN_011A&amp;DEV_0097&amp;REV_01',
                               ProductList.YELLOWSTONE: 'VEN_011A&amp;DEV_0101&amp;REV_01',
                               ProductList.YOSEMITE: 'VEN_011A&amp;DEV_0103&amp;REV_01',
                               ProductList.AURORA: 'VEN_011A&amp;DEV_009A&amp;REV_01',
                               ProductList.BLACK_ICE: 'VEN_011A&amp;DEV_0105&amp;REV_01',
                               ProductList.GRANDTETON: 'VEN_011A&amp;DEV_030A&amp;REV_01',
                               ProductList.RANGERPEAK: 'VEN_011A&amp;DEV_0313&amp;REV_01',
                               ProductList.BRYCECANYON: 'VEN_011A&amp;DEV_0401&amp;REV_01',
                               ProductList.BLACKCANYON: 'VEN_011A&amp;DEV_0403&amp;REV_01',
                               ProductList.MIRRORMAN: 'VEN_011A&amp;DEV_0195&amp;REV_01'}
                               
    x_hardware_id = {ProductList.LIGHTNING: 'VEN_011A&amp;amp;DEV_0083&amp;amp;REV_01',
                     ProductList.GLACIER: 'VEN_011A&amp;amp;DEV_0096&amp;amp;REV_01',
                     ProductList.KINGSCANYON: 'VEN_011A&amp;amp;DEV_0092&amp;amp;REV_01',
                     ProductList.ZION: 'VEN_011A&amp;amp;DEV_0094&amp;amp;REV_01',
                     ProductList.SPRITE: 'VEN_011A&amp;amp;DEV_0098&amp;amp;REV_01',
                     ProductList.YELLOWSTONE: 'VEN_011A&amp;amp;DEV_0102&amp;amp;REV_01',
                     ProductList.YOSEMITE: 'VEN_011A&amp;amp;DEV_0104&amp;amp;REV_01',
                     ProductList.AURORA: 'VEN_011A&amp;amp;DEV_009B&amp;amp;REV_01',
                     ProductList.BLACK_ICE: 'VEN_011A&amp;amp;DEV_0106&amp;amp;REV_01',
                     ProductList.GRANDTETON: 'VEN_011A&amp;amp;DEV_030B&amp;amp;REV_01',
                     ProductList.RANGERPEAK: 'VEN_011A&amp;amp;DEV_0314&amp;amp;REV_01',                               
                     ProductList.BRYCECANYON: 'VEN_011A&amp;amp;DEV_0402&amp;amp;REV_01',
                     ProductList.BLACKCANYON: 'VEN_011A&amp;amp;DEV_0404&amp;amp;REV_01',
                     ProductList.MIRRORMAN: 'VEN_011A&amp;amp;DEV_0196&amp;amp;REV_01'}
                     
    x_model_id = {ProductList.LIGHTNING: '02BF98D2-1B50-4a1f-83D9-A85402782902',
                  ProductList.GLACIER: '02BF98D2-1B50-4a1f-83D9-A85402783701',
                  ProductList.KINGSCANYON: '02BF98D2-1B50-4a1f-83D9-A85402782903',
                  ProductList.ZION: '02BF98D2-1B50-4a1f-83D9-A85402782904',
                  ProductList.SPRITE: '02BF98D2-1B50-4a1f-83D9-A85402783801',
                  ProductList.YELLOWSTONE: '02BF98D2-1B50-4a1f-83D9-A85402783A01',
                  ProductList.YOSEMITE: '02BF98D2-1B50-4a1f-83D9-A85402783B01',
                  ProductList.AURORA: '02BF98D2-1B50-4a1f-83D9-A85402783901',
                  ProductList.BLACK_ICE: '02BF98D2-1B50-4a1f-83D9-A85402784001',
                  ProductList.GRANDTETON: '02BF98D2-1B50-4a1f-83D9-A85402784100',
                  ProductList.RANGERPEAK: '02BF98D2-1B50-4a1f-83D9-A85402784101',
                  ProductList.BRYCECANYON: '228c4938-d533-4c48-9b9a-32fc1ea8b785',
                  ProductList.BLACKCANYON: '069ce97d-d560-4bf0-8960-ddec38f93634',
                  ProductList.MIRRORMAN: '02BF98D2-1B50-4a1f-83D9-A85402783702'}
                  
    x_container_id = {ProductList.LIGHTNING: '73656761-7465-7375-636b-0090a9f64000',
                      ProductList.GLACIER: '73656761-7465-7375-636b-0090a9f67001',
                      ProductList.KINGSCANYON: '73656761-7465-7375-636b-0090a9f65001',
                      ProductList.ZION: '73656761-7465-7375-636b-0090a9f66001',
                      ProductList.SPRITE: '73656761-7465-7375-636b-0090a9f68001',
                      ProductList.YELLOWSTONE: '73656761-7465-7375-636b-0090a9f6A001',
                      ProductList.YOSEMITE: '73656761-7465-7375-636b-0090a9f6B001',
                      ProductList.AURORA: '73656761-7465-7375-636b-0090a9f69001',
                      ProductList.BLACK_ICE: '73656761-7465-7375-636b-0090a9f70011',
                      ProductList.GRANDTETON: '73656761-7465-7375-636b-0090a9f6F006',
                      ProductList.RANGERPEAK: '73656761-7465-7375-636b-0090a9f6F008',
                      ProductList.BRYCECANYON: '7035cb1a-0ee5-4607-8c31-f5382d2aed6b',
                      ProductList.BLACKCANYON: 'e3ac1a8f-6639-4120-b0ec-7c4374a43ba8',
                      ProductList.MIRRORMAN: '73656761-7465-7375-636b-0090a9f67002'}
    # shutdown time per product
    shutdown_time = {ProductList.DEFAULT: 180}
    # boot time per product
    boot_time = {ProductList.DEFAULT: 180}
    # reboot time per product
    reboot_time = {ProductList.DEFAULT: 300}

    # WebUI timeout. Format is min, max, increment
    web_ui_timeout = {ProductList.DEFAULT: [5, 30, 1]}

    # Health monitor attributes
    command_parameters = {ProductList.DEFAULT: {'sar': '-m ALL -u ALL -P ALL -w -q -r -R -W -B -S -d -p -b ' + \
                                                       '-n DEV -n EDEV -n SOCK -n IP -n EIP -n TCP -n ETCP -n UDP'}}
    # Thresholds. Default is:
    # Free memory minimum value < 1000
    health_thresholds = {ProductList.DEFAULT: {Metrics.memory_free: {Stats.min: '< 1000'}}}
    # Change to this default to have it always immediately fail (for testing purposes)
    #health_thresholds = {ProductList.DEFAULT: {Metrics.swap_used: {Stats.total: '>= 1'}}}
