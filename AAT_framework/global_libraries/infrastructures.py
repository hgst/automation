""" @brief Contains the framework infrastructure settings for the specific location

    Get the environment dictionary with environment[location].
    Then, pull the location-specific environment settings from
    the environment dictionary.
"""

# Environment dictionary keys
INV_SERVER = 'inventory_server'
FTP_SERVER = 'ftpServer'
UPS_UNIT = 'upsUnit'
AD_SERVER = 'adServer'
AD_DOMAIN_NAME = 'adDomainName'

# Irvine's environment dictionary
irvine = {INV_SERVER: 'http://sevautoweb.labspan.wdc.com/InventoryServer',
          FTP_SERVER: '192.168.11.39',
          AD_SERVER: '192.168.11.102',
          AD_DOMAIN_NAME: 'IRV-SEV.COM',
          UPS_UNIT:   '192.168.11.39'}

# Mountain View's environment dictionary
mountain_view = {INV_SERVER: 'None',
                 FTP_SERVER: '127.0.0.1',
                 AD_SERVER: '127.0.0.1',
                 AD_DOMAIN_NAME: 'MV-SEV.COM',
                 UPS_UNIT:   '127.0.0.1'}

# Taiwan's environment dictionary
taiwan = {INV_SERVER: 'http://sevtwautoweb.labspan.wdc.com/InventoryServer',
          FTP_SERVER: '192.168.11.99',
          AD_SERVER: '192.168.11.201',
          AD_DOMAIN_NAME: 'TW-SEV.COM',
          UPS_UNIT:   '192.168.11.99'}

# Local environment dictionary.  This will let you run an inventory server locally
local = {INV_SERVER: 'None',
         FTP_SERVER: '127.0.0.1',
         AD_SERVER: '127.0.0.1',
         AD_DOMAIN_NAME: 'LOCAL-SEV.COM',
         UPS_UNIT:   '127.0.0.1'}

# Dictionary of environment dictionaries, with the location as the key
environment = {'Irvine': irvine,
               'Taiwan': taiwan,
               'MV': mountain_view,
               'Local': local}

