"""Contains methods used to interact with and manipulate files.
Created on Jul 29, 2013

@author: Troy Hoffman
(C) 2013 - 2015 Western Digital Corporation

"""
import time
import hashlib
import tempfile
import os
import errno
import shutil
import gzip
import ntpath
import glob
import shlex
import subprocess

from wd_random import getrandombytes
from wd_formatting import hex_to_ascii, units_to_bytes, get_mb_second
from wd_logging import create_logger
from global_libraries import wd_misc
import wd_exceptions


log_name = 'wd_localfilesystem'
logger = create_logger(log_name, root_log=log_name)

def createpath(path):
    try:
        os.makedirs(path)
    except OSError:
        # Only raise the exception if the error isn't caused by the path already existing
        if not os.path.isdir(path):
            raise


def delete_path(path, delete_contents=False, output_debug=True):
    for next_path in glob.glob(path):
        if output_debug:
            logger.debug('Deleting directory ' + next_path)
        try:
            if delete_contents:
                shutil.rmtree(next_path)
            else:
                os.rmdir(next_path)
        except OSError, e:
            # Do not raise for certain error conditions
            if e.errno == 16:
                logger.warning('Error 16 (device or resource busy) when deleting directory {}'.format(next_path))
            if e.errno == 13:
                logger.warning('Error 13 (permission denied) when deleting directory {}'.format(next_path))
            if e.errno == 2:
                logger.warning('Error 2 (No such file or directory) when deleting directory {}'.format(next_path))
            elif not os.path.isdir(next_path):
                logger.warning('Could not delete {} because it doesn\'t exist'.format(next_path))
            else:
                raise




def delete_file(filename, output_debug=True):
    """ Deletes a file or files matching a wildcard

    :param filename: The file to delete
    :return: 0 on success, error code on failure
    """
    for next_file in glob.glob(filename):
        if os.path.isfile(next_file):
            if output_debug:
                logger.debug('Deleting file ' + next_file)
            try:
                os.remove(next_file)
                return 0
            except OSError, e:
                if output_debug:
                    logger.debug('Failed to delete file. Error was ' + str(e.strerror))
                if e.errno > 0:
                    return e.errno
                else:
                    return 1


def create_file(filename,
                size,
                randomfile=False,
                pattern=None,
                block_size=1024 * 1024):
    """ Create a file and return the time in seconds it took to create.

    Keyword arguments:
        filename - The fully qualified name of the file to create
        randomfile - If true, puts random data in the file
        pattern - If true, repeats the supplied pattern in the file. To
                  specify unprintable characters, begin the string with 0x
                  and use hex versions of the ASCII characters in the
                  pattern (such as 0xFF00FF00).

                  NOTE: If both pattern and randomfile are true, the data
                  will be random.
        block_size - The file is created by writing data in blocks of the
                     specified size in bytes.
                     If not specified, it defaults to 1 MiB.

    In the event of an error, the return value will be None.
    """

    filesize = units_to_bytes(size)

    if not randomfile:
        if pattern:
            characters = hex_to_ascii(pattern)
        else:
            characters = "\0'"

    loops = filesize / block_size  # Use blocks to speed up processing

    # Create the path to the file, if necessary
    path = ntpath.split(filename)[0]

    createpath(path)

    outfile = open(filename, "wb", 0)

    # Create the data block
    if randomfile:
        block = getrandombytes(block_size)
    elif pattern:
        # Make the block bigger than block_size to allow
        # for patterns that don't divide evenly into the block_size.
        # noinspection PyUnboundLocalVariable
        block = characters * (block_size / len(characters) + 1)
        # Trim it to the correct size
        block = block[:block_size]
    else:
        block = '\0' * block_size

    # Write the data
    create_starttime = time.time()

    if loops > 0:
        for dummy in range(0, loops):
            outfile.write(block)

    # Write any remainder for pattern files.
    # Random and \0 will never have a remainder since they're a
    # single character and will always divide into block_size evenly.
    bytes_remaining = filesize % block_size
    if bytes_remaining > 0:
        block = characters * (bytes_remaining / len(characters) + 1)
        block = block[:bytes_remaining]
        outfile.write(block)

    closefile(outfile)

    createtime = round(time.time() - create_starttime, 2)
    return createtime


def closefile(fd):
    # Clears all buffers for file with the filedescriptor fd and closes it.
    file.flush(fd)
    os.fsync(fd)
    fd.close()


def gethash(filename, method='sha256', getspeed=False):
    """Calculate a hash code for filename using the method passed.

    If getspeed is True, return hash code and
    read speed (MB/second) as a list. If it is False (default), return
    just the hash code as a string.

    NOTE: By setting getspeed, it will need to do two read operations. The
    first is to get the read speed by reading to RAM. The second is to get
    the hash. The reason for this is that calculating the hash is CPU-
    intensive, so it will return read speeds lower than the actual data
    transfer speed.

    Return None on error.

    The method that is passed will be attempted. Some example methods are:
        md5
        sha1
        sha256 (default)

    """
    blocksize = 65536

    if getspeed:
        read_starttime = time.time()
        logger.debug('Calculating read speed')
        logger.debug('   starttime = ' + str(read_starttime))

        # Calculate the read speed first to avoid a false low speed due to
        # extra overhead when calculating the hash
        with open(filename, 'rb', 0) as afile:
            dummy = afile.read(blocksize)
            while len(dummy) > 0:
                dummy = afile.read(blocksize)

        readtime = time.time() - read_starttime
        bytesread = os.path.getsize(filename)
        speed = get_mb_second(bytesread, readtime)

        logger.debug("   end time = " + str(time.time()))
        logger.debug("   readtime = " + str(readtime))
        logger.debug("   bytes = " + str(bytesread))
        logger.debug("   speed = " + str(speed))
        logger.debug('Calculating hash code')

    hasher = hashlib.new(method.lower())

    with open(filename, 'rb', 0) as afile:
        data = afile.read(blocksize)

        while len(data) > 0:
            hasher.update(data)
            data = afile.read(blocksize)

    logger.debug('Hash code = ' + hasher.hexdigest())

    if getspeed:
        # noinspection PyUnboundLocalVariable
        return [hasher.hexdigest(), speed]
    else:
        return hasher.hexdigest()

def copyfiles(source_file,
              destination_path=tempfile.gettempdir(),
              copymethod='shutil',
              output_debug=True):

    totalspeed = 0
    file_count = 0
    for nextfile in glob.glob(source_file):
        speed = copyfile(source_file=nextfile,
                         destination_path=destination_path,
                         copymethod=copymethod,
                         output_debug=output_debug)
        if speed is not None:
            totalspeed += speed
            file_count += 1
        else:
            raise wd_exceptions.FailedToExecuteShellCommand('File copy failed')

    if file_count == 0:
        raise wd_exceptions.FileNotFoundError('Could not find any files in {}'.format(source_file))

    return totalspeed / file_count


def copyfile(source_file,
             destination_path=tempfile.gettempdir(),
             dest_filename=None,
             copymethod='shutil',
             output_debug=True):
    """ Copy a file and return the write speed as MB/s. Return None on failure.

    Keyword arguments:
        source_file - The fully qualified file name
        destination_path - The location to copy the file.
                           If not specified, it goes to the temp directory.
        dest_filename - The name of the file.
                        If not specified, it uses the same name as
                        the sourceFile.
        copymethod - A  string specifying the type of copy to perform.

                     Supported options are:
                         shutil - Use shutil to copy (default method).
                         os - Use an OS shell command to copy.
                         Anything else - Use the exact shell command to do the copy

    NOTE: Passing an unsupported copyMethod will default to shutil.
    """

    if '*' in source_file:
        # Contains a wild card, so call copyfiles
        copyfiles(source_file,
                  destination_path,
                  copymethod,
                  output_debug)

    # Change the copy method to lower case so it's case-insensitive
    copymethod = copymethod.lower()
    # Create the destination directory if necessary
    if destination_path != '':
        if not(os.path.exists(destination_path) and not os.path.isdir(destination_path)):
            os.makedirs(destination_path)

    if not dest_filename:
        # Do not rename during copy
        dest_filename = os.path.basename(source_file)

    dest_file_path = os.path.join(destination_path, dest_filename)

    copy_starttime = time.time()
    if output_debug:
        logger.debug('Copying ' + source_file + ' to ' + dest_file_path)
        logger.debug('Starting copy at ' + str(copy_starttime))

    # Copy the file. Only two methods supported so far. Others may be added
    # later. If other methods are added, make sure the default is
    # _copyfile_shutil to maintain backwards compatibility.
    if copymethod == 'os':
        # Copy using the OS command
        success = _copyfile_os(source_file, dest_file_path)
    elif copymethod == 'shutil':
        # Default
        success = _copyfile_shutil(source_file, dest_file_path)
    else:
        if _run_command(copymethod) is None:
            # Failed, return False
            success = False
        else:
            success = True

    if success is True:
        # Calculate the time
        copytime = time.time() - copy_starttime

        if os.path.isfile(dest_file_path):
            bytescopied = os.path.getsize(dest_file_path)
            # Calculate the speed
            mbsecond = get_mb_second(bytescopied, copytime)

            return mbsecond

    # Copy failed
    return None


def gzip_file(filename, extension='.gz'):
    """Compress a file using gzip.

    On success, return the output filename.
    On error, return None.

    """
    infile = open(filename, 'rb')
    outfile = gzip.open(filename + extension, 'wb')
    outfile.writelines(infile)
    closefile(outfile)
    closefile(infile)

    return filename + extension

def get_unique_filename(basename, extension):
    return wd_misc.get_unique_filename(filename, extension)

def _run_command(command):
    # Run command and send output to NULL.
    if os.name == 'nt':
        null_ = 'nul'
    else:
        null_ = '/dev/null'

    # noinspection PyBroadException
    try:
        os.system(command + ' >> ' + null_ + ' 2>&1')
        return 0
    except Exception:
        return None

def run_command_locally(command, shell=False, raise_exceptions=True):
    # Command can be either a string or a list of the command and options
    logger.debug('Executing command. Shell={}. Command:{}     {}'.format(shell, os.linesep, command))
    if not shell:
        # Make sure command is an iterable, not a string, since calling as shell with a single string gives a
        # command not found error (for example 'ls -a' tries to execute a command named 'ls -a' not 'ls' with
        # the '-a' switch)
        if isinstance(command, basestring):
            # Split
            if os.name == 'nt':
                posix = False
            else:
                posix = True

            command = shlex.split(command, posix=posix)

    try:
        output = subprocess.check_output(command, shell=shell)
    except subprocess.CalledProcessError as result:
        error_message = ('Failed to execute {}. Error code: {} - Output:{}     {}'.format(command,
                                                                                          result.returncode,
                                                                                          os.linesep,
                                                                                          result.output))
        if raise_exceptions:
            raise wd_exceptions.FailedToExecuteShellCommand(error_message)
        else:
            logger.debug(error_message)
            returncode = result.returncode
            if 0 == returncode:
                returncode = 1

            return returncode, result.output

    return 0, output


# Copies the source file to the destination path using OS commands
def _copyfile_os(sourcefile, destfile):
    # Build the command
    if os.name == 'nt':
        command = 'copy /Y %s %s' % (sourcefile, destfile)
    else:
        command = 'cp -f %x %s'

    result, output = run_command_locally(command, shell=True)

    if result > 0:
        # Failed, return False
        logger.debug('Failed to copy file: {}'.format(output))
        return False
    else:
        return True


# Copies the source to the destination using shutil
def _copyfile_shutil(sourcefile,
                     destfile,
                     buffer_large=10485760,
                     buffer_small=1024):

    # Check if it is a small file and, if so, use a smaller buffer
    if os.path.getsize(sourcefile) <= buffer_large:
        buffersize = buffer_small
    else:
        buffersize = buffer_large

    with open(sourcefile, 'rb', 0) as fsrc:
        with open(destfile, 'wb', 0) as fdst:
            shutil.copyfileobj(fsrc, fdst, buffersize)
            try:
                shutil.copystat(sourcefile, destfile)
            except OSError, err:
                if err.errno != errno.EPERM:
                    # Permission error can be caused by source or destination being NTFS
                    raise

    return True