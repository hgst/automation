""" Set of functions used to compare and manipulate dictionaries """

CHANGED = 'changed'
UNCHANGED = 'unchanged'
ADDED = 'added'
REMOVED = 'removed'


def compare(first, second):
    """
    Report the differences between two dictionaries.
    :param first: The first dictionary to compare
    :param second: The second dictionary to compare

    Returns a dictionary of sets. Keys are:
        changed    - A set of what changed between first and second
        unchanged  - A set of what is identical in both first and second
        added      - A set of what is in second, but not first
        removed    - A set of what is in first, but not second

    Note that the sets only contain the keys from the respective
    dictionaries, not the dictionary values, and it does not tell you
    what changed in the changed keys. This also is not recursive. If the dictionaries
    contain dictionaries, and those dictionaries have changed, you'll see that there's a difference,
    but it's up to you to run those dictionaries through this method again to find what changed in the nested
    dictionaries.
    """

    changes = {}

    # First create three sets of keys from the dictionaries
    first_keys = set(first)
    second_keys = set(second)
    common = first_keys.intersection(second_keys)

    # Build the sets of changed and unchanged keys
    changed = set()
    unchanged = set()

    for key in common:
        if first[key] == second[key]:
            unchanged.add(key)
        else:
            changed.add(key)

    changes[CHANGED] = changed
    changes[UNCHANGED] = unchanged

    # Now, the added keys (only in second)
    changes[ADDED] = second_keys - common

    # And removed (only in first)
    changes[REMOVED] = first_keys - common

    return changes
