__author__ = 'Troy Hoffman'
import random

def getrandombytes(size):
    """ Return a random string of the specified number of bytes."""
    random.seed()

    random_string = ''

    for u_t in range(1, size):
        random_string += chr(random.getrandbits(8))

    return random_string

