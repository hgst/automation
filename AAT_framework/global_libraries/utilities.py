import decorator
import hashlib
import os
import paramiko
import re
import sarge
import socket
import string
import sys
import threading
import time
import traceback
import types
import errno
import random

from global_libraries import wd_random

try:
    import psutil
except ImportError:
    print ('*WARN* psutil not installed')

paramiko.util.log_to_file('paramiko.txt')
if sys.platform.startswith('linux'):
    import pexpect
# dlipower is the web power switch controller module
try:
    from dlipower import dlipower
except ImportError:
    import dlipower
POWER_SWITCH_CYCLETIME = 3
TEST_SHARE_NAME = 'SmartWare'
TEST_SHARE_DESC = 'atestshare'

@decorator.decorator
def decode_unicode_args(func, *args, **kwargs):
    """
    Decorator to decode Unicode arguments into ASCII.
    """
    args = list(args)
    temp_args = list(args)
    for i, arg in enumerate(temp_args):
        if type(arg) == types.UnicodeType:
            try:
                debug ('*DEBUG* Encoding {0} as ASCII'.format(arg))
                args[i] = arg.encode('ascii', 'ignore')
            except UnicodeDecodeError:
                traceback.print_exc()
    temp_kwargs = dict(kwargs)
    for key, arg in temp_kwargs.items():
        if type(arg) == types.UnicodeType:
            try:
                debug ('*DEBUG* Encoding {0} as ASCII'.format(arg))
                kwargs[key] = arg.encode('ascii', 'ignore')
            except UnicodeDecodeError:
                traceback.print_exc()
    return func(*args, **kwargs)

@decorator.decorator
def clean_share_name(func, *args, **kwargs):
    """
    Decorator to remove dashes and underscores from share names.
    """
    temp_kwargs = dict(kwargs)
    for key, arg in temp_kwargs.items():
        debug ('{0}={1}'.format(key, arg))
        if key == 'share_name':
            kwargs[key] = arg.replace('_', '').replace('-', '')
            break
    return func(*args, **kwargs)

class UtiltiesError(Exception):
    pass

power_lock = threading.RLock()

printable = string.ascii_letters + string.digits + string.punctuation + string.whitespace
@decode_unicode_args
def hex_escape(s):
    return ''.join(c if c in printable else r'\x{0:02x}'.format(ord(c)) for c in s)

illegal_unichrs = [ (0x00, 0x08), (0x0B, 0x1F), (0x7F, 0x84), (0x86, 0x9F),
                (0xD800, 0xDFFF), (0xFDD0, 0xFDDF), (0xFFFE, 0xFFFF),
                (0x1FFFE, 0x1FFFF), (0x2FFFE, 0x2FFFF), (0x3FFFE, 0x3FFFF),
                (0x4FFFE, 0x4FFFF), (0x5FFFE, 0x5FFFF), (0x6FFFE, 0x6FFFF),
                (0x7FFFE, 0x7FFFF), (0x8FFFE, 0x8FFFF), (0x9FFFE, 0x9FFFF),
                (0xAFFFE, 0xAFFFF), (0xBFFFE, 0xBFFFF), (0xCFFFE, 0xCFFFF),
                (0xDFFFE, 0xDFFFF), (0xEFFFE, 0xEFFFF), (0xFFFFE, 0xFFFFF),
                (0x10FFFE, 0x10FFFF) ]

illegal_ranges = ["%s-%s" % (unichr(low), unichr(high))
                  for (low, high) in illegal_unichrs
                  if low < sys.maxunicode]

illegal_xml_re = re.compile(u'[%s]' % u''.join(illegal_ranges))

def clean_xml(dirty):
    return illegal_xml_re.sub('', dirty)

@decode_unicode_args
def escape(s):
    return ''.join(c if c in printable else '' for c in s)

printable_not_punc = string.ascii_letters + string.digits + string.whitespace
@decode_unicode_args
def escape_punctuation(s):
    return ''.join(c if c in printable_not_punc else '' for c in s)

@decode_unicode_args
def replace_escape_sequence(string, show_hex=False):
    """Delete Linux control codes from serial output."""
    # replace unicode escape, followed by formatting (used by Linux
    # to format console output)
    if show_hex:
        string = hex_escape(string)
    else:
        string = escape(string)
    re1 = re.compile('(.)(\\[)(\\?)?(\\d+)([a-z])', re.IGNORECASE | re.DOTALL)
    re2 = re.compile('(.)(\\[)(\\d+)(;)(\\d+)([a-z])', re.IGNORECASE | re.DOTALL)
    string = re1.sub('', string)
    return re2.sub('', string).strip().replace('\r', '')

@decode_unicode_args
def format_mac_address(string, ignore_errors=False):
    """Pull a MAC address out of a string and format it. Raise an exception
    if no MAC address is found."""
    string = replace_escape_sequence(string.upper())
    if not (':' in string or '-' in string):
        string = ':'.join(string[i:i + 2] for i in range(0, 12, 2))
    a = re.compile('([a-fA-F0-9]{2}[:|\-]?){6}').search(string)
    if a:
        return string[a.start(): a.end()].replace('-', ':').strip().strip(':')
    if not ignore_errors:
        raise UtiltiesError('Invalid MAC address: ' + str(string))

def enable_ssh(ip_address):
    debug ('Enabling SSH on {0}'.format(ip_address)())
    if sys.platform.startswith('linux'):
        sarge.run('wget --quiet --timeout=5 --tries=1 "http://{0}/api/1.0/rest/ssh_configuration?owner=admin&pw=&enablessh=enable&rest_method=put"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 --post-data="owner=admin&pw=&enablessh=enable&rest_method=put" "http://{0}/api/1.0/rest/ssh_configuration"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 "http://{0}/api/1.0/rest/ssh_configuration?auth_username=admin&auth_password=&enablessh=enable&rest_method=put"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 --post-data="auth_username=admin&auth_password=&enablessh=enable&rest_method=put" "http://{0}/api/1.0/rest/ssh_configuration"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 "http://{0}/api/2.1/rest/ssh_configuration?owner=admin&pw=&enablessh=true&rest_method=put"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 --post-data="owner=admin&pw=&enablessh=true&rest_method=put" "http://{0}/api/2.1/rest/ssh_configuration"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 "http://{0}/api/2.1/rest/ssh_configuration?auth_username=admin&auth_password=&enablessh=true&rest_method=put"'.format(ip_address))
        sarge.run('wget --quiet --timeout=5 --tries=1 --post-data="auth_username=admin&auth_password=&enablessh=true&rest_method=put" "http://{0}/api/2.1/rest/ssh_configuration"'.format(ip_address))

@decode_unicode_args
def is_operational(ip_address, port=22, username='root', password='welc0me'):
    if sys.platform.startswith('linux'):
        # use pexpect to clear SSH host keys
        command = 'ssh-keygen -R [{0}]:{1} -f "/home/jenkins/.ssh/known_hosts"'.format(ip_address, port)
        print command
        child = pexpect.spawn(command)
        if child.before:
            debug ('*INFO* {0}'.format(child.before))
    try:
        debug ('*INFO* opening SSH connection')
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=ip_address, port=port, username=username, password=password, timeout=5)
        return True
    except:
        return False

@decode_unicode_args
def run_on_device(command, ip_address, port=22, username='sshd', password='welc0me', timeout=20, retry=False):
    """ Runs a command on a device through SSH

    :param command: The command to run
    :param ip_address: The IP address of the target device
    :param port: The port to use
    :param username: The username to authenticate with
    :param password: The password to authenticate with
    :param timeout: The amount of time to wait before giving up
    :param retry: If True, wait and retry on a connection failure. Will try up to 3 times.
    :return: The output from stdout concatenated with the output from stderr
    """

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    stdout = None

    attempts = 3 if retry else 1
    for x in xrange(0, attempts):
        try:
            client.connect(hostname=ip_address, port=port, username=username, password=password, timeout=timeout)
            stdin, stdout, stderr = client.exec_command(command)
            time.sleep(3)
            client.close()
            break
        except (socket.timeout, socket.error, paramiko.SSHException, paramiko.AuthenticationException, AttributeError) as e:
            if 'Authentication' in str(e):
                debug ('*ERROR* authentication to {0} failed with {1}/{2}'.format(ip_address, username, password))
            elif 'Connection refused' in str(e):
                debug ('*ERROR* SSH connection to {0} refused with {1}/{2}'.format(ip_address, username, password))
            elif 'timeout' in str(e):
                # Raise the original exception so the stack trace will be useful
                raise
            else:
                debug ('*INFO* {0}'.format(traceback.format_exc()))
            time.sleep(10)

    retval = ''
    if stdout:
        retval += '\nstdout:\n'
        retval += stdout.readlines()

    if stderr:
        retval += '\nstderr:\n'
        retval += stderr.readlines()

    return retval

def hashfile(filepath):
    start = time.time()
    sha1 = hashlib.sha1()
    with open(filepath, 'rb') as f:
        while True:
            buf = f.read(512)
            if not buf:
                break
            sha1.update(buf)
    digest = sha1.hexdigest()
    debug ('*INFO* Hash computed in {0} seconds'.format(int(time.time() - start)))
    return digest

def make_dir(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def get_params(self, parameters):
    debug ('*DEBUG* _get_params() running')
    for key, value in parameters.items():
        debug ('*DEBUG* ------------------------------')
        debug ('*DEBUG* "{0}"="{1}"'.format(key, value))
        debug ('*DEBUG* "{0}" starts with "{1}" : "{2}"'.format(str(value), '{0}='.format(key), str(value).startswith('{0}='.format(key))))
        if str(value).startswith('{0}='.format(key)):
            parameters[key] = str(value).split('{0}='.format(key))[1]
            debug ('*DEBUG* "{0}"="{1}"'.format(key, value))
    debug ('*DEBUG* _get_params() exiting')
    return {key: value for (key, value) in parameters.items() if key != 'self'}

def log(msg, level='*INFO*'):
    print ('{0} {1}'.format(level, msg))

def info(msg):
    log(msg, '*INFO*')

def warn(msg):
    log(msg, '*WARN*')

def error(msg):
    log(msg, '*ERROR*')

def debug(msg):
    log(msg, '*DEBUG*')

def trace(msg):
    log(msg, '*TRACE*')

def deprecation_warning():
    warn('This keyword is deprecated and should not be used.')
    raise DeprecationWarning()

def free_disk_space(path):
    """From https://code.google.com/p/psutil/wiki/Documentation#Disks
    """
    try:
        usage = psutil.disk_usage(path)
        return usage.free
    except:
        pass

    # From http://stackoverflow.com/questions/4274899/get-actual-disk-space
    st = os.statvfs(path)
    free = st.f_bavail * st.f_frsize
    # total = st.f_blocks * st.f_frsize
    # used = (st.f_blocks - st.f_bfree) * st.f_frsize
    return free


def to_int(s):
    return int(''.join([c for c in str(s) if c.isdigit()]))


def getrandombytes(size):
    return wd_random.getrandombytes(size)

def remove_duplicates(list):
    """ Remove duplicate items from a list while preserving order (a set loses order)

    :param list: The list to remove the duplicates from
    :return: A list without the duplicates
    """

    new_list = []

    for item in list:
        if item not in new_list:
            new_list.append(item)

    return new_list
