""" Used to track how long something takes.
        Supports pausing and lap times.
"""
import sys
import time

from global_libraries import wd_exceptions
from wd_logging import create_logger


class Stopwatch(object):
    """ Used to track how long something takes.
        Supports pausing and lap times.
    """

    _LINUX = 'Linux'
    _WINDOWS = 'Windows'

    log = create_logger('AAT.performance')

    def __init__(self, timer=None, precision=2, start_timer=True):
        """ Constructor
        @Param timer - If set, is_timer_reached() returns true if specified number of seconds has elapsed,
        false if it has not. If not set, is_timer_reached() throws an exception.
        """

        if sys.platform.startswith('win'):
            self._platform = self._WINDOWS
        elif sys.platform.startswith('linux'):
            self._platform = self._LINUX
        else:
            self._platform = self._LINUX

        self._timer = timer
        self._precision = precision

        # These attributes track the stopwatch values and are set in _reset_stopwatch()
        # By doing it this way, the reset function can call _reset_stopwatch() to ensure everything is properly reset.
        self._createtime = None
        self._starttime = None
        self._elapsedtime = None
        self._isrunning = None
        self._lastlap = None
        self._laps = None

        self._reset_stopwatch()
        if start_timer:
            self.start()

    def _reset_stopwatch(self, keep_running=True):
        if keep_running and not self._isrunning:
            # If the timer isn't running, set this to False to prevent it from starting the timer
            keep_running = False

        self._createtime = self._get_time()
        self._starttime = 0
        self._elapsedtime = 0
        self._lastlap = 0
        self._laps = []
        self._isrunning = False

        if keep_running:
            self.start()

    def start(self):
        """ Starts or resumes the timer, but only if it's stopped """
        if not self._isrunning:
            self._starttime = self._get_time()
            self._isrunning = True
            if self._lastlap == 0:
                self._lastlap = self._starttime

    def stop(self):
        """ Stops or pauses the timer and updates the elapsed time"""
        if self._isrunning is True:
            self._elapsedtime += self._get_time() - self._starttime
            self._isrunning = False

    def reset(self, keep_running=True):
        """ Resets the elapsed time to 0

            :param keep_running If False, stops the timer
        """
        self._reset_stopwatch(keep_running=keep_running)

    def lap(self):
        """ Creates a "lap" time, which is the amount of time elapsed
            since the last call to lap()"""
        if self._isrunning:
            lap_start = self._get_time()
            lap_elapsed = lap_start - self._lastlap
            self._lastlap = lap_start

            self._laps.append(round(lap_elapsed, self._precision))
        else:
            # If we just add a lap of time 0, it could mess things up. So, raise an exception instead of warning
            raise wd_exceptions.StopWatchNotRunning('lap called while stopwatch is stopped')

    def get_elapsed_time(self):
        """ Returns the total elapsed amount of time as seconds, rounded to self._precision"""
        elapsed = self._elapsedtime
        if self._isrunning is True:
            elapsed += self._get_time() - self._starttime
        else:
            self.log.warning('get_elapsed_time called while stopwatch is stopped. Starting stopwatch')
            self.start()
            elapsed = 0

        return round(elapsed, self._precision)

    def get_lap_times(self):
        """ Returns a list containing all of the lap time information.
            If the timer is running the last lap will be the amount of time
            that has elapsed so far this lap.
        """
        if self._laps is not None:
            lap_list = list(self._laps)
            lap_elapsed = self._get_time() - self._lastlap

            if lap_elapsed > 0:
                lap_list.append(round(lap_elapsed, self._precision))

            return lap_list
        else:
            # If we just return a list of laps with a time of 0, it could mess things up.
            # So, raise an exception instead of warning
            raise wd_exceptions.StopWatchNotRunning('get_lap_times called without starting stopwatch')

    def is_timer_reached(self):
        """ Returns True if the countdown timer has been reached.
        NOTE: If the timer parameter was not set when the class was instantiated, this will throw an exception.


        :return: :raise Exception:
        """
        if self._timer is not None:
            if self._isrunning:
                if self.get_elapsed_time() > self._timer:
                    return True
                else:
                    return False
            else:
                if not self._isrunning:
                    self.log.warning('is_timer_reached called while stopwatch is stopped. Starting stopwatch')
                    self.start()
                    return False
        else:
            raise wd_exceptions.InvalidParameter('is_timer_reached called, but ' +
                                                 'Stopwatch was instantiated without a timer value.')

    def _get_time(self):
        # Returns the current time, using time.clock() for Windows or
        # time.time() for everything else
        if self._platform == self._WINDOWS:
            return time.clock()
        else:
            return time.time()
