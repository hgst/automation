import logging
import os
import sys
import time
from inspect import stack

from wd_silk import get_silk_results_dir


class _MaxLevelFilter(logging.Filter):
    def __init__(self, maxlevel):
        super(_MaxLevelFilter, self).__init__()
        self.maxlevel = maxlevel

    def filter(self, record):
        return record.levelno <= self.maxlevel


class _LogCount(object):
    """ Decorator to count the number of times each log level is called and take a screenshot
    """

    def __init__(self, func, screenshot_basename, outputdir, take_screenshot=False, store_messages=False):
        self._func = func
        self.counter = 0
        self.selenium_client = None
        self.screenshot_basename = screenshot_basename
        self.outputdir = outputdir
        self.take_screenshot = take_screenshot
        self.store_messages = store_messages
        self.messages = []

    def __call__(self, *args, **kwargs):
        # Add a very brief delay to ensure the previous log's output is finished. This prevents getting the stdout and
        # stderr output out of order
        time.sleep(0.01)
        self.counter += 1

        if self.store_messages:
            self.messages.append(args[0])

        if self.selenium_client and self.take_screenshot:
            filename = self.screenshot_basename + '_' + str(self.counter) + '.png'
            path = os.path.join(self.outputdir, filename)
            self.selenium_client.capture_browser_screenshot(path)

        return self._func(*args, **kwargs)


def _decorate_log(log, outputdir):
    # Add the _LogCount decorators
    log.exception = _LogCount(log.exception, 'exception', outputdir, take_screenshot=True, store_messages=True)
    log.critical = _LogCount(log.critical, 'critical', outputdir, take_screenshot=True, store_messages=True)
    log.error = _LogCount(log.error, 'error', outputdir, take_screenshot=True, store_messages=True)
    log.warning = _LogCount(log.warning, 'warning', outputdir, store_messages=True)
    log.info = _LogCount(log.info, 'info', outputdir)
    log.debug = _LogCount(log.debug, 'debug', outputdir)

    log.setLevel(logging.DEBUG)


def _create_root_log(logname='AAT', outputdir=None, overwrite_log=False):
    if '.' in logname:
        logname = logname.split('.')[0]

    # See if the root logger already exists
    rootlog = logging.getLogger(logname)

    if rootlog.handlers:
        # Already created, so return
        return

    # Add it as a root logger
    filename = logname + '_debug.log'


    if outputdir is None:
        outputdir = '.'

    logfile = os.path.join(outputdir, filename)

    tofile = logging.FileHandler(filename=logfile, mode='w')
    tofile.setLevel(logging.DEBUG)
    tofile.name = 'file'
    tofile.setFormatter(logging.Formatter(
        '%(asctime)-19s %(name)-12s: %(levelname)-8s %(message)s',
        '%Y-%m-%d %H:%M:%S'))

    # Two handlers. One writes Info to stdout. The other writes warning and higher to stderr. Debug is
    # supressed.
    info_warning = logging.StreamHandler(sys.stdout)
    info_warning.addFilter(_MaxLevelFilter(logging.INFO))
    info_warning.setLevel(logging.INFO)
    info_warning.name='stdout'
    # set a format which is simpler for console use
    info_warning.setFormatter(logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s'))

    error_plus = logging.StreamHandler(sys.stderr)
    error_plus.setLevel(logging.WARNING)
    error_plus.name='stderr'
    # set a format which is simpler for console use
    error_plus.setFormatter(logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s'))

    rootlog.addHandler(tofile)
    rootlog.addHandler(info_warning)
    rootlog.addHandler(error_plus)

    _decorate_log(rootlog, outputdir)


def create_logger(logname=None, outputdir=None, root_log='AAT', overwrite_log=False):
    """ Configure the logging system
    :param logname: The name of the logger. Default is AAT (the root logger)
    :return: The logger object
    """
    if logname is None or logname == '':
        # Figure out what script called this function
        if len(stack()) > 2:
            # Called from getLogger()
            caller_frame = stack()[2]
        else:
            # Called directly
            caller_frame = stack()[1]

        callingscript = caller_frame[0].f_globals.get('__file__', None)
        callingscript = os.path.basename(callingscript)
        if '.' in callingscript:
            callingscript = callingscript.split('.')[0]
        logname = '{}.{}'.format(root_log,
                                 callingscript)

    if outputdir is None:
        outputdir = get_silk_results_dir()

    if not os.path.exists(outputdir):
        try:
            os.makedirs(outputdir)
        except:
            pass

    if not logname.startswith('{}.'.format(root_log)):
        logname = '{}.{}'.format(root_log, logname)

    _create_root_log(root_log, outputdir, overwrite_log)

    log = logging.getLogger(logname)

    _decorate_log(log, outputdir)

    return log


def set_log_level(logname, loglevel, root_log='AAT'):
    rootlog = logging.getLogger(root_log)

    root_handlers = rootlog.handlers

    for handler in root_handlers:
        if handler.name == logname:
            handler.setLevel(loglevel)
            return