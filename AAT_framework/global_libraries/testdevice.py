""" Module to track the test devices being used """

import copy

from global_libraries import utilities
from global_libraries.product_info import ProductList


class Fields(object):
    """ Class used to prevent typos when accessing device fields

        Be sure to use Fields.<field> instead of the constant string. For example,
        instead of 'internalIPAddress', use Fields.internal_ip_address. This will
        prevent a typo like 'internalIpAddress' from causing a bug that might be
        hard to track down.

        All fields from the inventory server database should be in here, with
        names that match the output of the APIs used to get device information.

        Fields that are not stored on the inventory server, such as ssh_password,
        should also be in this class. The naming convention should follow that of
        the inventory server names.
    """
    device_id = 'id'
    device_class = 'class'
    mac_address = 'macAddress'
    product = 'product'
    jenkins_job = 'jenkinsJob'
    execution_plan = 'execution_plan'
    location = 'location'
    is_busy = 'isBusy'
    is_operational = 'isOperational'
    internal_ip_address = 'internalIPAddress'
    power_switch = 'powerSwitch'
    power_switch_port = 'powerSwitchPort'
    ssh_gateway = 'sshGateway'
    ssh_gateway_port = 'sshGatewayPort'
    ssh_username = 'sshUsername'
    ssh_password = 'sshPassword'
    default_ssh_password = 'defaultSshPassword'
    serial_server = 'serialServer'
    serial_server_port = 'serialServerPort'
    serial_username = 'serialUser'
    serial_password = 'serialPassword'
    default_serial_password = 'defaultSerialPassword'
    date_created = 'dateCreated'
    last_updated = 'lastUpdated'
    web_username = 'webUsername'
    default_web_username = 'defaultWebUsername'
    web_password = 'webPassword'
    default_web_password = 'defaultWebPassword'
    default_ad_password = 'defaultADPassword'
    ad_password = 'adPassword'
    ad_username = 'adUsername'
    default_ad_username = 'defaultADUsername'
    internal_ssh_port = 'internalSSHPort'
    actual_fw_version = 'deviceFWVersion'
    expected_fw_version = 'firmware'
    number_of_physical_drives = 'numberOfPhysicalDrives'
    configured_raid_level = 'configuredRAIDLevel'
    rest_username = 'restUsername'
    default_rest_username = 'defaultRestUsername'
    rest_password = 'restPassword'
    default_rest_password = 'defaultRestPassword'

    # Test control
    debug_mode = 'debug'
    suppress_exceptions = 'suppress'

    # Product attributes
    number_of_drives = 'number_of_drives'
    supports_link_speed = 'supports_link_speed'
    expected_read_speed = 'expected_read_speed'
    expected_write_speed = 'expected_write_speed'
    read_speed_tolerance = 'read_speed_tolerance'
    write_speed_tolerance = 'write_speed_tolerance'
    toolchain = 'toolchain'
    supports_volume_encryption = 'supports_volume_encryption'
    boot_time = 'boot_time'
    reboot_time = 'reboot_time'
    shutdown_time = 'shutdown_time'

    # UPnP attributes
    default_media_server_id = 'default_media_server_id'
    x_hardware_id = 'x_hardware_id'
    x_model_id = 'x_model_id'
    x_container_id = 'x_container_id'

    # RAID test command line switches
    raidlevel = 'raidlevel'
    downgrade_to_jbod = 'downgrade_to_jbod'
    expand_capacity = 'expand_capacity'
    limit_capacity = 'limit_capacity'
    remainder_as_spanning = 'remainder_as_spanning'

    # Which media server is running
    media_server = 'media_server'
    # Media server authentication
    media_server_user = 'media_server_user'
    media_server_password = 'media_server_password'
    default_media_server_user = 'default_media_server_user'
    default_media_server_password = 'defalt_media_server_password'

    # Infrastructure fields
    ftp_server = 'ftpServer'

    ad_server = 'adServer'
    ad_domain_name = 'adDomainName'

    ups_unit = 'upsUnit'

    # Web UI timeout
    web_ui_timeout = 'web_ui_timeout'

    # Health command parameters
    command_parameters = 'command_parameters'
    # Health thresholds
    health_thresholds = 'health_thresholds'

    # Test case parameters (can be set by command line to change test case behavior from Silk)
    full_data_volume_size = 'full_data_volume_size'

    # The browser to use for the UUT
    uut_browser = 'uut_browser'

def run_on_device(self, command, retry=False, timeout=20):
    """ Runs a command on the UUT through SSH

    :param command: The command to run
    :param timeout: The amount of time to wait before giving up
    :param retry: If True, wait and retry on a connection failure. Will try up to 3 times.
    :return: The output from stdout concatenated with the output from stderr
    """
    return utilities.run_on_device(command=command,
                                   ip_address=self.settings[Fields.internal_ip_address],
                                   port=self.settings[Fields.internal_ssh_port],
                                   timeout=timeout,
                                   retry=retry)


class DeviceDefaults(object):
    """ Used to track default values for certain fields per product

        Each dictionary is a set of default values per product.
        Default is meant to be the base used by all products.
        It should be copied a new dictionary for each supported product
        and any unique defaults overwritten by assigning new values to the new
        dictionary.

        In addition, each supported device should be in device_list.
    """

    default_terminal_password = 'welc0me'
    default_ui_username = 'admin'
    default_ui_password = ''
    default_ad_password = 'Branded1'
    default_ad_username = 'fitbranded'

    default = {Fields.internal_ssh_port: '22',
               Fields.ssh_username: 'sshd',
               Fields.ssh_password: default_terminal_password,
               Fields.default_ssh_password: default_terminal_password,
               Fields.serial_username: 'sshd',
               Fields.serial_password: default_terminal_password,
               Fields.default_serial_password: default_terminal_password,
               Fields.web_username: default_ui_username,
               Fields.web_password: default_ui_password,
               Fields.default_web_username: default_ui_username,
               Fields.default_web_password: default_ui_password,
               Fields.ad_password: default_ad_password,
               Fields.default_ad_password: default_ad_password,
               Fields.ad_username: default_ad_username,
               Fields.default_ad_username: default_ad_username,
               Fields.rest_username: default_ui_username,
               Fields.default_rest_username: default_ui_username,
               Fields.rest_password: default_ui_password,
               Fields.default_rest_password: default_ui_password}

    lightning = copy.deepcopy(default)

    sequoia = copy.deepcopy(default)
    sequoia[Fields.ssh_username] = 'root'

    device_list = {ProductList.LIGHTNING: lightning,
                   ProductList.SEQUOIA: sequoia}
