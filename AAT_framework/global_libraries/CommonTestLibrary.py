""" @brief A collection of utilities used by all APIs.
"""
import datetime
import filecmp
import os
import socket
import string
import sys
import time
import traceback
import inspect
import re
import subprocess
import shlex

import zipfile
import tarfile

# Temporarily disabled until we can update the infrastructure to include this module.
# import rfc3987

import paramiko
import sarge
import logging
import threading
import os.path
import xml.etree.ElementTree

from global_libraries import utilities
from global_libraries.wd_logging import create_logger
from global_libraries.wd_logging import set_log_level
from global_libraries import wd_exceptions
from global_libraries import wd_silk
from global_libraries.wd_environment import EnvironmentVariables

paramiko.util.log_to_file('paramiko.txt')

# sarge's default timeout is pretty short, so increase it to a few minutes
sarge.default_capture_timeout = 360

GATEWAY = os.environ.get('test-gateway') or '10.6.163.158'
PRIVATE_SUBNET = os.environ.get('test-subnet') or '192.168.11.'
PRIVATE_GATEWAY = PRIVATE_SUBNET + '1'

EXCEPTION = logging.CRITICAL + 10


def background_thread(repeat_count=None, loop_delay=None):
    """ Decorator used to to run a function in the background
    Based on http://stackoverflow.com/questions/5179467/equivalent-of-setinterval-in-python
    Retrieved 2013 July 23

    Modified to be more AAT-friendly and less like the original Java function. Changes from original:
        * Instead of defaulting to a non-stop loop, it defaults to a single run
        * Instead of a delay before starting, it immediately runs and pauses before repeating



    :param repeat_count:    The number of times to repeat. Set to 0 to repeat indefinitely.
                            Set to None to only run one time.
    :param loop_delay:      The number of seconds to wait before repeating

    :return: A thread even object

    To use:
    1) Decorate the function that should run in the background
    2) Call the function to start the thread
    3) Assign its return value to a variable. This is the thread event object.
    4) Call <thread event object>.set() to stop the thread

    NOTE: The repeat counts and loop_delay are fixed and hard-coded. They can be assigned from a global variable,
          but the assignment takes place during construction, so it can't be changed during run-time.

          For example:
            loop_delay = 5

            @background_thread(loop_delay)
            def repeat_message(self, message):
                print message

            def __init__(self):
                loop_delay = self.ask_user_for_delay_time()
                repeat_message('This will repeat')

          In this example, the loop_delay will be 5 seconds, even though the variable was set to whatever the user
          wanted before starting the thread. This is because the decorated repeat_message method is constructed with
          the value of 5 before __init__ is called.

          If you need a variable loop_delay, use a value of None. Then, have the method being decorated handle the
          delay. You can do this for the number of repeats as well, but you won't be able to stop the thread with the
          set() command. This is because the check for isSet() is in the decorator, so the loop inside the function
          won't know about it. It also will cause a delay after the last execution. It's better to just used a fixed
          delay.

          The following example will let the user control how long to wait between calls to the thread.

            @background_thread()
            def repeat_message(self, message, delay):
                print message
                time.sleep(delay)

            def __init__(self):
                user_delay = self.ask_user_for_delay_time()
                repeat_message('This will repeat', user_delay)

    Example:

    from testCaseAPI.src.testclient import TestClient
    from global_libraries.CommonTestLibrary import background_thread
    import time

    class Mine(TestClient):
        # This thread will repeat indefinitely every 1 second
        @background_thread(0, 1)
        def print_counter(self):
            self.counter += 1
            print self.counter

        def run(self):
            self.counter = 0
            print_thread = self.print_counter()
            time.sleep(5)
            print 'Stopping'
            print_thread.set()
            time.sleep(5)

    m = Mine()

    """

    def outer_wrap(function):
        def wrap(self, *args, **kwargs):
            stop = threading.Event()

            def inner_wrap():
                i = 0
                while i != repeat_count and not stop.isSet():
                    function(self, *args, **kwargs)
                    i += 1
                    if i != repeat_count and loop_delay is not None:
                        stop.wait(loop_delay)

            t = threading.Timer(0, inner_wrap)
            t.daemon = True
            t.start()
            return stop

        return wrap

    return outer_wrap


def is_running_on_silk():
    try:
        build = EnvironmentVariables.get('sctm_build')
    except KeyError:
        return False
    else:
        return True

def get_silk_results_dir():
    return wd_silk.get_silk_results_dir()

aat_log = create_logger(logname='AAT.CommonTestLibrary', outputdir=get_silk_results_dir())


class CommandError(Exception):
    pass


class FileCompareError(Exception):
    pass


def get_class_attributes(cls):
    nonuser = dir(type('', (object,), {}))
    return [item
            for item in inspect.getmembers(cls)
            if item[0] not in nonuser]


def get_private_gateway():
    """Return the IP of the private network's gateway"""
    return PRIVATE_GATEWAY


def get_host_ip():
    """
    Get the host PC's IP address (on the private network in the
    automation rack).
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect((PRIVATE_GATEWAY, 80))
    host_ip = s.getsockname()[0]
    s.close()
    return host_ip


def get_result_code(result):
    """ Return 0 if the request is successful (2xx).
            Return the result code if it is not.
        """
    if ((200 <= result.status_code < 300) and
                'stack trace' not in result.content.lower() and
                'bad request' not in result.content.lower()):
        retval = 0
    else:
        retval = result.status_code
        print 'Result failed with code ' + str(result.status_code)
        print 'Content: ' + str(result.content)

    return retval

def convert_seq_firmware_to_alpha_format(firmware):
    print 'Converting Sequoia firmware with 04.04.00-200 format'
    a, build = firmware.split('-')
    print a, build
    e, version, g = a.split('.')
    version1 = e[1:2]
    print version1, version, build
    return version1 + '.' + version + '.' + build

def convert_seq_firmware_format(firmware):
    print 'Converting Sequoia firmware with 040400-200 format'
    a, build = firmware.split('-')
    print a, build
    version = firmware[2:4]
    version1 = firmware[1:2]
    print version, version1
    s_firmware_version = version1 + '.' + version
    s_firmware_build = build
    return s_firmware_version + '.' + s_firmware_build

@utilities.decode_unicode_args
def clean_url(url):
    """ @brief Takes a url and returns a cleaned-up string
    
        @param url: The URL to clean up
        
        Removes spaces and trailing backslash, then verifies that the string is actually a URL.
        Returns the cleaned up URL, or None if the url parameter is not properly formatted as a URL.
        
        To be valid, url must comply with RFC 3987.
    """
    # Check for a valid string
    if url is None or not isinstance(url, basestring):
        return None

    # Remove white space
    new_url = str(url).strip().rstrip('/')
    # Remove trailing slash if any
    new_url = new_url.rstrip('/')

    # # Confirm the URL matchs the URI format specified in RFC 3987
    # if rfc3987.match(clean_url, rule='URI') is None:
    # return None
    # else:
    # return clean_url

    # Old method
    # Regular expression used to validate a URL
    url_re = '(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)?([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?'
    # Check for valid URL
    if not re.match(url_re, new_url):
        return None
    else:
        return new_url


@utilities.decode_unicode_args
def discover_upnp(ip_address):
    # Use nmap to find upnp services on the specified device
    # TODO: actually parse the nmap result for upnp specific services
    nmap_result = run_and_print('nmap -sV -sC {0}'.format(ip_address),
                                fail_on_error=False, print_stdout=False)
    nmap_result = [line for line in nmap_result if 'upnp' in line and 'open' in line]
    return '\n'.join(nmap_result)


@utilities.decode_unicode_args
def scan_arp(mac_address):
    """Scan the ARP table for a MAC address"""
    p = run_and_print('arp -n', print_stdout=False)
    for line in p:
        if 'incomplete' not in line and line.strip():
            print '*INFO* ' + str(line.strip())
            try:
                if utilities.format_mac_address(line.split()[2],
                                                ignore_errors=True) == \
                        utilities.format_mac_address(mac_address):
                    ip_address = line.split()[0]
                    print '*INFO* IP=' + str(ip_address)
                    return ip_address
            except IndexError:
                pass


@utilities.decode_unicode_args
def find_ip(mac_address):
    """Find the IP corresponding to the given MAC address. If no MAC
    is provided, use this object's own MAC address."""

    host_ip = get_host_ip()
    broadcast_ip1 = PRIVATE_GATEWAY.split('.')
    broadcast_ip1[3] = '255'
    broadcast_ip1 = string.join(broadcast_ip1, '.')
    broadcast_ip2 = host_ip.split('.')
    broadcast_ip2[3] = '255'
    broadcast_ip2 = string.join(broadcast_ip2, '.')
    print '*INFO* host_ip = {0}'.format(host_ip)
    print '*INFO* broadcast_ip1 = {0}'.format(broadcast_ip1)
    print '*INFO* broadcast_ip2 = {0}'.format(broadcast_ip2)
    ip_address = None

    print '*INFO* Trying to find IP address for {0}'.format(mac_address)
    if sys.platform.startswith('linux'):
        ip_address = scan_arp(mac_address)
        if ip_address:
            return ip_address

        # if we havent found our test unit yet, ping active IPs
        # found using nmap, along with the broadcast IPs
        # TODO: Fix this. Where was nmap_output supposed to come from?
        for line in nmap_output:
            if line.startswith('Nmap scan report'):
                ip = line.split()[-1].strip('()').strip()
                if len(line.split()) < 6:
                    print '*INFO* nmap line: {0}'.format(line)
                    run_and_print('ping -c 1 -w 1 {0}'.format(ip),
                                  ignore_errors=True, print_stdout=False)
        run_and_print('ping -q -b -c 1 {0}'.format(broadcast_ip1),
                      ignore_errors=True, print_stdout=False)
        run_and_print('ping -q -b -c 1 {0}'.format(broadcast_ip2),
                      ignore_errors=True, print_stdout=False)
        ip_address = scan_arp(mac_address)

        if ip_address:
            return ip_address


@utilities.decode_unicode_args
# TODO: figure out why popen can't run mount commands
def run_without_popen(cmd, read_output=True):
    """Run a command using os.system."""
    print '*INFO* {0}'.format(cmd)

    # write the output to a file
    output_file = 'cmd_output.txt'
    cmd = '{0} >> {1} 2>&1'.format(cmd, output_file)

    result = os.system(cmd)

    # read and print the output
    if read_output and os.path.isfile(output_file):
        with open(output_file, 'r') as o:
            for line in o.readlines():
                print '*INFO* {0}'.format(line)
        os.unlink(output_file)
    # Check whether command executed successfully
    if result != 0:
        print '*ERROR* Command returned non-zero exit status.'
        raise CommandError('Command returned non-zero exit status {0}: {1}'.format(result, cmd))


@utilities.decode_unicode_args
def run_and_print(command, ignore_errors=False, fail_on_error=False,
                  shell=False, print_stdout=True, failure_strings=None):
    """
    Use sarge to run the command, then print stdout and stderr, then
    return stdout as a list of lines.

    -If ignore_errors is set to True, STDERR won't get printed out.
    -If fail_on_error is set to True, an Exception will be raised if the
    return code is nonzero or if an AttributeError is caught.
    -If shell=True, the command is run in the OS's shell. This can be
    dangerous.
    -If print_stdout is True, STDOUT gets printed to the console.
    -failure_strings expects a list of strings. If any of these strings are
    found in stderr and ignore_errors is False, an exception is raised.
    """
    print '*INFO* {0} CMD: {1}'.format(time.asctime(), command)
    if not failure_strings:
        failure_strings = []
    try:
        p = sarge.capture_both(command, shell=shell)
    except Exception:
        if not ignore_errors:
            raise
        else:
            utilities.info(traceback.format_exc())
            return traceback.format_exc()
    lines = p.stdout.readlines()
    if print_stdout:
        for line in lines:
            print '*INFO* {0} STDOUT: {1}'.format(time.asctime(),
                                                  line.strip())
    if not ignore_errors:
        if p.stderr.read().strip():
            print '*WARN* {0}'.format(command)
        for line in p.stderr:
            print '*WARN* {0} STDERR: {1}'.format(time.asctime(),
                                                  line.strip())
            for string in failure_strings:
                if string in line:
                    raise CommandError('*ERROR* found failure string: {0}'.format(string))
    try:
        if p.returncode != 0:
            if fail_on_error:
                raise CommandError('*ERROR* command failed: {0}'.format(command))
    except AttributeError:
        if fail_on_error:
            raise CommandError('*ERROR* command failed: {0}'.format(command))
    return lines


@utilities.decode_unicode_args
def store_test_files(test_name, files=None, exclude_ext=None):
    """Store the test files in a zip so they can be used for debugging.
        Excludes the extensions specified by exclude_ext.
        This should be used to archive generated test content.
        Generates [test_name].zip"""

    assert test_name, 'test_name is required when storing test files.'
    test_name = utilities.escape_punctuation(test_name).replace(' ', '_')
    if not exclude_ext:
        exclude_ext = ['.py', '.txt', '.png', '.deb', '.log', '.zip', '.sha1',
                       '.xml', '.conf', '.csv', '.html', '.7z', '.rar',
                       '.testfile', '.lint', '.sc']
    total_filesize = 0
    archive_count = 0

    # noinspection PyBroadException
    try:
        archive_filename = '{0}-{1}.zip'.format(test_name, archive_count)
        archive = zipfile.ZipFile(archive_filename, 'w', zipfile.ZIP_DEFLATED)
        if not files:
            files = os.listdir('.')
        utilities.info(files)
        for filename in files:
            if os.path.isfile(filename) and os.path.splitext(filename)[1] not in exclude_ext and '.' in filename:
                # Cannot create zips larger than 2GB, so check if we need to start a new one
                # assuming 0 compression to be safe
                file_size = os.path.getsize(filename)
                if file_size > 1024 * 1024 * 100:
                    utilities.info('File is too big, skipping')
                    continue
                utilities.info('Storing test filename - {0}'.format(filename))
                if total_filesize + file_size >= 2147483648:
                    archive.close()
                    total_filesize = 0
                    archive_count += 1
                    archive = zipfile.ZipFile('{0}-{1}.zip'.format(test_name, archive_count), 'w',
                                              zipfile.ZIP_DEFLATED)
                archive.write(filename)
                total_filesize += file_size
        archive.close()
        if not total_filesize and not archive_count:
            os.unlink(archive_filename)
    except Exception:
        utilities.warn(traceback.format_exc())
        for filename in os.listdir('.'):
            if filename.endswith('zip'):
                os.unlink(filename)


@utilities.decode_unicode_args
def should_be_equal_as_files(filename1, filename2, msg=None):
    if not filecmp.cmp(filename1, filename2, shallow=False):
        if not msg:
            raise FileCompareError('{0} != {1}'.format(filename1, filename2))
        else:
            raise FileCompareError(msg)
    utilities.info('{0} == {1}'.format(filename1, filename2))


def get_log_timestamp(line):
    # noinspection PyPep8
    re1 = '((?:2|1)\\d{3}(?:-|\\/)(?:(?:0[1-9])|(?:1[0-2]))(?:-|\\/)(?:(?:0[1-9])|(?:[1-2][0-9])|(?:3[0-1]))(?:T|\\s)(?:(?:[0-1][0-9])|(?:2[0-3])):(?:[0-5][0-9]):(?:[0-5][0-9]))'  # Time Stamp 1
    timestamp = None
    rg = re.compile(re1, re.IGNORECASE | re.DOTALL)
    m = rg.search(line)
    if m:
        timestamp = m.group(1)
    return timestamp


def convert_timestamp_to_datetime(timestamp):
    if not timestamp:
        return None
    return datetime.datetime.strptime(timestamp.replace('T', ' '), '%Y-%m-%d %H:%M:%S')


@utilities.decode_unicode_args
def file_should_not_contain(filename, strings, start_time=None, context=10, test_name=None,
                            ignore_strings=None):
    """Fails if file contains the specified string(s). Ignores messages before the specified start time"""
    if type(strings) == str:
        strings = [strings]
    if type(ignore_strings) == str:
        ignore_strings = [ignore_strings]
    if not test_name:
        test_name = str(time.asctime())
    failures = []
    if start_time:
        start = convert_timestamp_to_datetime(start_time)

    last_error_line_number = -999
    last_valid_timestamp = None
    print '*INFO* Scanning {0} for {1}'.format(filename, strings)
    with open(filename) as file_to_scan:
        for line_number, line in enumerate(file_to_scan):
            if line_number < last_error_line_number + context:
                utilities.info(line.strip())
            if start:
                log_timestamp = convert_timestamp_to_datetime(get_log_timestamp(line))
                if log_timestamp:
                    last_valid_timestamp = log_timestamp
                else:
                    log_timestamp = last_valid_timestamp
                # noinspection PyUnboundLocalVariable
                if log_timestamp and log_timestamp < start:
                    continue
            for string in strings:
                if string.lower() in line.lower():
                    ignore = False
                    if ignore_strings:
                        for ignore_string in ignore_strings:
                            if ignore_string.lower() in line.lower():
                                utilities.info('Ignoring, found "{0}" in "{1}"'.format(ignore_string, line))
                                ignore = True
                        if ignore:
                            continue
                    last_error_line_number = line_number + 1
                    utilities.info('-' * 20)
                    utilities.info(line.strip())
                    failures.append((line_number + 1, string, line.strip()))
    if failures:
        error_msg = 'Found strings:\n'
        for line_number, string, line in failures:
            error_msg += 'Found "{0}" at line {1}: "{2}"\n'.format(string, line_number, line)
        # utilities.info(error_msg)
        store_test_files(test_name)
        raise Exception(error_msg)


@utilities.decode_unicode_args
def file_should_contain(filename, strings, start_time=None):
    """Search for the specified string(s) within the file. Ignores messages before the specified start time"""
    if type(strings) == str:
        strings = [strings]
    success = []
    if start_time:
        start = convert_timestamp_to_datetime(start_time)
    else:
        start = None
    print '*INFO* Scanning {0} for {1}'.format(filename, strings)
    with open(filename) as file_to_scan:
        for line_number, line in enumerate(file_to_scan):
            if start:
                log_timestamp = convert_timestamp_to_datetime(get_log_timestamp(line))
                if log_timestamp and log_timestamp < start:
                    continue
            for string in strings:
                if string.lower() in line.lower():
                    success.append((line_number + 1, string, line.strip()))
    if success:
        msg = '*INFO* Found strings:\n'
        for line_number, string, line in success:
            msg += 'Found "{0}" at line {1}: "{2}"\n'.format(string, line_number, line)
        print msg
    else:
        error_msg = '*ERROR* Did not find strings: {0}'.format(strings)
        raise Exception(error_msg)


@utilities.decode_unicode_args
def return_all_matches(filenames, strings, target_filename=None):
    if type(strings) == str:
        strings = [strings]
    if type(filenames) == str:
        filenames = [filenames]
    success = []
    for filename in filenames:
        print '*INFO* Scanning {0} for {1}'.format(filename, strings)
        with open(filename) as file_to_scan:
            for line_number, line in enumerate(file_to_scan):
                for string in strings:
                    if string.lower() in line.lower():
                        success.append((line_number + 1, string, line.strip()))
    if success:
        msg = '*INFO* Found strings:\n'
        for line_number, string, line in success:
            msg += 'Found "{0}" at line {1}: "{2}"\n'.format(string, line_number, line)
        print msg
        if target_filename:
            with open(target_filename, 'w') as output:
                output.write(msg)
    return success


@utilities.decode_unicode_args
def get_datestamped_filename(extension='txt', destination_folder=None):
    """Return a datestamped filename. If the file exists, use a counter to get a unique name.
        Intended for screenshots or other test-generated files"""
    timestamp = datetime.datetime.now().isoformat().replace(':', '-')
    file_name = '{0}.{1}'.format(timestamp, extension)
    if destination_folder:
        file_name = os.path.join(destination_folder, file_name)
    dupe_count = 0
    while os.path.isfile(file_name):
        dupe_count += 1
        file_name = time.strftime('{0}-{1}.{2}'.format(timestamp, dupe_count, extension))
        if destination_folder:
            file_name = os.path.join(destination_folder, file_name)
    return file_name


def clear_cache():
    if sys.platform.startswith('linux'):
        run_and_print('sudo sync', ignore_errors=True)
        run_and_print('echo 1 | sudo tee /proc/sys/vm/drop_caches', ignore_errors=True)
        run_and_print('echo 2 | sudo tee /proc/sys/vm/drop_caches', ignore_errors=True)
        run_and_print('echo 3 | sudo tee /proc/sys/vm/drop_caches', ignore_errors=True)


def success_message():
    """
    Best used with "Run keyword if" to get a response from using it as an
    if statement
    """
    print "Success!"


def percent_error(item1, item2):
    """
    Returns the percent error of two numbers
    """
    return abs((item1 - item2) / item1)


def convert_to_float(item1):
    """
    Converts item into a floating point number
    """
    return float(item1)


def configure_logger(logname=None):
    return create_logger(logname=logname, outputdir=get_silk_results_dir())


def getLogger(logger_name=None):
    """ Wrapper for configure_logger. Added for easier compatibility with the old method of using logging.getLogger()

    :param logger_name: The name of the logger
    :return The logger object
    """

    return configure_logger(logger_name)


def set_console_log_level(loglevel):
    if loglevel > logging.INFO:
        set_log_level('stderr', loglevel)
        # Disable stdout
        set_log_level('stdout', logging.CRITICAL + 1)
    else:
        set_log_level('stdout', loglevel)
        set_log_level('stderr', logging.WARNING)


def set_file_log_level(loglevel):
    set_log_level('file', loglevel)


def get_count(error_level):
    # noinspection PyUnresolvedReferences
    loggers = logging.Logger.manager.loggerDict
    aat_loggers = {}
    logger_name = ''

    for logger in loggers:
        if str(logger).startswith('AAT'):
            aat_loggers[logger] = loggers[logger]

    count = 0
    try:
        for logger_name in aat_loggers:
            logger = logging.getLogger(logger_name)
            if error_level == EXCEPTION:
                count += logger.exception.counter
            if error_level == logging.CRITICAL:
                count += logger.critical.counter
            elif error_level == logging.ERROR:
                count += logger.error.counter
            elif error_level == logging.WARNING:
                count += logger.warning.counter
            elif error_level == logging.INFO:
                count += logger.info.counter
            elif error_level == logging.DEBUG:
                count += logger.debug.counter
    except AttributeError:
        aat_log.warning('Logger ' +
                        logger_name +
                        ' not properly configured. Be sure it calls ctl.GetLogger, not Logging.GetLogger')

    return count


def get_messages(error_level):
    # noinspection PyUnresolvedReferences
    loggers = logging.Logger.manager.loggerDict
    aat_loggers = {}
    logger_name = ''

    for logger in loggers:
        if str(logger).startswith('AAT'):
            aat_loggers[logger] = loggers[logger]

    messages = []
    try:
        for logger_name in aat_loggers:
            logger = logging.getLogger(logger_name)
            if error_level == EXCEPTION:
                messages.extend(logger.exception.messages)
            if error_level == logging.CRITICAL:
                messages.extend(logger.critical.messages)
            elif error_level == logging.ERROR:
                messages.extend(logger.error.messages)
            elif error_level == logging.WARNING:
                messages.extend(logger.warning.messages)
            elif error_level == logging.INFO:
                messages.extend(logger.info.messages)
            elif error_level == logging.DEBUG:
                messages.extend(logger.debug.messages)
    except AttributeError:
        aat_log.warning('Logger ' +
                        logger_name +
                        ' not properly configured. Be sure it calls ctl.GetLogger, not Logging.GetLogger')

    if len(messages) == 0:
        messages = ['None']

    return messages


def exception(message=None):
    retval = ''
    if message:
        retval += 'Exception: ' + message + os.linesep
    retval += traceback.format_exc()

    return retval


def get_silk_test_name():
    try:
        filename = EnvironmentVariables.get('sctm_test_name')
    except KeyError:
        filename = sys.argv[0]

    basename = os.path.basename(filename)
    return os.path.splitext(basename)[0]


class HandleExceptions(object):
    def __init__(self, log=None):
        if log:
            self.logger = log
        else:
            try:
                self.logger = getLogger('AAT.HandleExceptions')
            except:
                # No AAT, so load a root logger
                self.logger = getLogger('HandleException')

    def __call__(self, func):
        def wrapped_function(*args, **kwargs):
            result = 1

            try:
                result = func(*args, **kwargs)
            except:
                message = 'Failed to execute function ' + str(func.__name__)
                self.logger.warning(exception(message))
            finally:
                return result

        return wrapped_function


class RestFunction(object):
    def __init__(self, log=None):
        if log:
            self.logger = log
        else:
            try:
                self.logger = getLogger('AAT.HandleExceptions')
            except:
                # No AAT, so load a root logger
                self.logger = getLogger('HandleException')

    def __call__(self, func):
        def wrapped_function(*args, **kwargs):
            result = 1

            try:
                result = func(*args, **kwargs)
            except:
                message = 'Failed to execute function ' + str(func.__name__)
                self.logger.warning(exception(message))
            finally:
                return result

        return wrapped_function


def run_command_locally(command, shell=False, raise_exceptions=True):
    # Command can be either a string or a list of the command and options
    aat_log.debug('Executing command. Shell={}. Command:{}     {}'.format(shell, os.linesep, command))
    if not shell:
        # Make sure command is an iterable, not a string, since calling as shell with a single string gives a
        # command not found error (for example 'ls -a' tries to execute a command named 'ls -a' not 'ls' with
        # the '-a' switch)
        if isinstance(command, basestring):
            # Split
            if os.name == 'nt':
                posix = False
            else:
                posix = True

            command = shlex.split(command, posix=posix)

    try:
        output = subprocess.check_output(command, shell=shell)
    except subprocess.CalledProcessError as result:
        error_message = ('Failed to execute {}. Error code: {} - Output:{}     {}'.format(command,
                                                                                          result.returncode,
                                                                                          os.linesep,
                                                                                          result.output))
        if raise_exceptions:
            raise wd_exceptions.FailedToExecuteShellCommand(error_message)
        else:
            aat_log.info(error_message)
            returncode = result.returncode
            if 0 == returncode:
                returncode = 1

            return returncode, result.output

    return 0, output


def extract_from_archive(archive, filename=None, destination='.'):
    if archive.endswith('.zip'):
        zip_handle = open(archive, 'rb')
        a = zipfile.ZipFile(zip_handle)
    else:
        zip_handle = None
        a = tarfile.open(archive)

    if filename:
        a.extract(filename, destination)
    else:
        a.extractall(destination)

    if zip_handle:
        zip_handle.close()
    else:
        a.close()


def get_latest_nexus_build_url(repo_base, repository_id, group, artifact, package_type='jar', version='LATEST'):
    if not repo_base.startswith('http://'):
        repo_base = 'http://' + repo_base

    return '{0}/service/local/artifact/maven/redirect?r={1}&g={2}&a={3}&p={4}&v={5}'.format(repo_base,
                                                                                            repository_id,
                                                                                            group,
                                                                                            artifact,
                                                                                            package_type,
                                                                                            version)


def is_number(input_string):
    """ Determine if a string is a number

    :param input_string: String to parse
    :return: True if input_string is a number, False if it is not
    """
    try:
        float(input_string)
        return True
    except (TypeError, ValueError):
        return False


def yes_no_to_true_false(option):
    """ Converts a "yes" or "no" string to True or False

    :param option: The string to convert
    :return: A boolean of True or False
    :raises: wd_exceptions.InvalidParameter if the string doesn't start with "y" or "n" (case insensitive)
    """

    if option is True:
        return True

    if option is False:
        return False

    # First, convert the string to lower
    answer = option.lower()

    if answer[:1] == 'y':
        return True
    elif answer[:1] == 'n':
        return False
    else:
        raise wd_exceptions.InvalidParameter('"{}" is not yes or no'.format(option))


def raid_mode_to_level(raid_mode):
    raid_levels = {'Unknown': -1, 'RAID0': 0, 'RAID1': 1, 'RAID5': 5, 'RAID10': 10, 'JBOD': 11, 'spanning': 12}

    return raid_levels[raid_mode]


def raid_level_to_mode(raid_level):
    if raid_level == -1:
        retval = 'Unknown'
    elif raid_level == 11:
        retval = 'JBOD'
    elif raid_level == 12:
        retval = 'spanning'
    else:
        retval = 'RAID{}'.format(raid_level)

    return retval


def validate_raid_path(current_raid,
                       target_raid,
                       number_of_drives,
                       downgrade_to_jbod,
                       expand_capacity,
                       limit_capacity,
                       remainder_as_spanning,
                       raid5_spare,
                       migrate_to_raid_1,
                       raid_1_spanning_volumes):
    aat_log.info('Validating RAID path from {} to {} with {} drives'.format(raid_level_to_mode(current_raid),
                                                                            raid_level_to_mode(target_raid),
                                                                            number_of_drives))

    aat_log.info('Downgrade: {}, '.format(downgrade_to_jbod) +
                 'Expand: {}, '.format(expand_capacity) +
                 'Limit: {}, '.format(limit_capacity) +
                 'Spanning: {}, '.format(remainder_as_spanning) +
                 'Spare: {}, '.format(raid5_spare) +
                 'Migrate: {}, '.format(migrate_to_raid_1) +
                 'RAID 1 spanning volumes: {}'.format(raid_1_spanning_volumes))

    if number_of_drives == 0:
        raise wd_exceptions.InvalidRAIDLevel('Unit has no drives. Cannot set RAID level.')

    if number_of_drives > 26:
        # Currently limited to 26 drives because the command line can only calculate sda through sdz. Need
        # to change algorithm if drive count > 26
        raise wd_exceptions.InvalidRAIDLevel('Cannnot configure {} drives. Limit is 26.'.format(number_of_drives))

    if target_raid == 0:
        # RAID 0 requires at least 2 drives
        if number_of_drives < 2:
            raise wd_exceptions.InvalidRAIDLevel('RAID 0 needs at least 2 drives')

    if raid_1_spanning_volumes and target_raid != 1:
        # Requires RAID 1
        raise wd_exceptions.InvalidRAIDLevel('The raid_1_spanning_volumes parameter requires RAID 1')

    if target_raid == 1:
        # RAID 1 requires an even number of drives
        if number_of_drives % 2:
            raise wd_exceptions.InvalidRAIDLevel('RAID 1 needs an even number of drives')

        if raid_1_spanning_volumes:
            if not remainder_as_spanning:
                raise wd_exceptions.InvalidRAIDLevel('The raid_1_spanning_volumes parameter ' +
                                                     'requires remainder as spanning')
            number_of_volumes = number_of_drives / 2

            volume_list = to_list(raid_1_spanning_volumes)

            for volume in volume_list:
                if volume > number_of_volumes:
                    raise wd_exceptions.InvalidRAIDLevel('Volume {} in raid_1_spanning_volume '.format(volume) +
                                                         'is invalid. Only {} '.format(number_of_volumes) +
                                                         'volumes available.')

    if target_raid == 5:
        # RAID 5 requires at least 3 drives
        if number_of_drives < 3:
            raise wd_exceptions.InvalidRAIDLevel('RAID 5 needs at least 3 drives ')

    if target_raid == 10:
        # RAID 10 requires a multiple of 4 drives
        if number_of_drives % 4:
            raise wd_exceptions.InvalidRAIDLevel('RAID 10 needs a multiple of 4 drives ')

    if downgrade_to_jbod:
        if target_raid != 11:
            raise wd_exceptions.InvalidRAIDLevel('Can only downgrade if JBOD is selected as the target')
        if current_raid != 1:
            raise wd_exceptions.InvalidRAIDLevel('Can only downgrade to JBOD when coming from RAID 1')

    if migrate_to_raid_1:
        if target_raid != 1:
            raise wd_exceptions.InvalidRAIDLevel('Can only migrate to RAID 1 if RAID 1 is selected as the target')
        if current_raid != 11:
            raise wd_exceptions.InvalidRAIDLevel('Can only migrate to RAID 1 when coming from JBOD')

    if expand_capacity and current_raid != 1:
        raise wd_exceptions.InvalidRAIDLevel('Can only expand capacity with RAID 1')

    if limit_capacity or remainder_as_spanning:
        if target_raid > 10:
            raise wd_exceptions.InvalidRAIDLevel('Cannot limit capacity with JBOD or Spannning')

    # Check spare
    if raid5_spare:
        if target_raid != 5 or number_of_drives < 4:
            raise wd_exceptions.InvalidRAIDLevel('Spare drive only valid for RAID 5 with at least 4 drives')


def to_list(item):
    """ Convert item into a list

    :param item: An item
    :return: A list containing the item, or the item itself it if is already a list
    """

    new_list = []
    if isinstance(item, list):
        new_list = item
    elif isinstance(item, tuple):
        for next_item in item:
            new_list.append(next_item)
    else:
        new_list = [item, ]

    return new_list


def to_tuple(item):
    """ Convert item into a list

    :param item: An item
    :return: A list containing the item, or the item itself it if is already a list
    """

    new_tuple = []
    if isinstance(item, tuple):
        new_tuple = item
    elif isinstance(item, list):
        for next_item in item:
            new_tuple.append(next_item)
    else:
        new_tuple = [item, ]

    return new_tuple


def get_xml_tags(xml_content, tag):
    """Returns the first instance of the given tag in the given XML tree."""
    aat_log.debug('Searching for {0} in {1}'.format(tag, xml_content))
    tag_content = []
    if xml_content:
        xml_content = utilities.clean_xml(xml_content)
        tree = xml.etree.ElementTree.fromstring(xml_content)
        if tag:
            tag = str(tag).strip().strip('"\'')
        for element in tree.iter(tag):
            text = None
            if element.text:
                aat_log.debug('element.text: {0}'.format(element.text))
                text = element.text.strip()
            if not text:
                # we have an element that has children
                aat_log.debug('element tostring: {0}'.format(xml.etree.ElementTree.tostring(element)))
                text = xml.etree.ElementTree.tostring(element)
            tag_content.append(text)
    return tag_content


def is_disk_ready(disk_xml):
    """ Returns True if the supplied disk XML data shows a healthy, non-failed, and connected drive

    :param disk_xml: A <disk> section from /var/www/xml/sysinfo.xml
    :return: True if the disk is ready (healthy, not failed, and connected) or False if it is not
    """
    connected = True if get_xml_tags(disk_xml, 'connected')[0] == '1' else False
    healthy = True if get_xml_tags(disk_xml, 'healthy')[0] == '1' else False
    failed = True if get_xml_tags(disk_xml, 'failed')[0] == '1' else False

    if connected and healthy and not failed:
        return True
    else:
        return False


def kwargs_to_args(arg_list, kwargs):
    """ Converts kwargs into a list of values that can be expanded into local variables. Useful for renaming
        parameters in a method and allowing older code to still call the function with the old name for the parameter.
        Returns None for any arguments not in kwargs. Raises an exception if kwargs contains arguments not in arg_list.

        Example of usage:

        def test(self, mode, drive_bays=None, **kwargs):
            arg_list = ['number_of_drives', 'test', 'test2']
            bays, test, test2 = CommonTestLibrary.kwargs_to_args(arg_list, kwargs)

            if bays is not None:
                drive_bays = bays

            print drive_bays, test, test2

    :param arg_list: A list of argument names to search for in kwargs. If an argument in arg_list is not in kwargs,
                     this function will return None for that position.
    :param kwargs: The **kwargs argument from the calling function. If an argument is in kwargs, but not arg_list,
                   raises TypeError with a list of unexpected keyword arguments.
    :return: A list of values from kwargs in the order matching arg_list. None is passed for an argument if it is not
             in kwargs.
    """
    ret_list = []
    for arg in arg_list:
        if arg in kwargs:
            ret_list.append(kwargs[arg])
            del kwargs[arg]
        else:
            ret_list.append(None)

    if len(kwargs) > 0:
        raise TypeError("test() got an unexpected keyword argument '{}'".format(kwargs.keys()))

    return ret_list


def get_drive_names(drive_mappings, drive_bays=0, start_bay=1):
    """ Using drive_mappings, return a string contain the requested drive names

    :param drive_mappings: A mapping of bays to drive names, from the SSH client's get_drive_mappings() function
    :param drive_bays: The number of drive bays to use, or a list of specific drive bays
    :param start_bay: The bay to start with
    :return: A string with the drive names, such as sda (1 drive) or sdasdb (2 drives)
    """
    retval = ''

    highest_bay = 0
    for bay in drive_mappings:
        if bay > highest_bay:
            highest_bay = bay

    drives_mapped = 0
    if is_number(drive_bays):
        drives_needed = drive_bays
    else:
        # noinspection PyTypeChecker
        drives_needed = len(drive_bays)

    for drive in xrange(start_bay, highest_bay + 1):
        if drive in drive_mappings:
            if is_number(drive_bays):
                # We're getting the first drives, regardless of bay numbers
                retval += drive_mappings[drive]
                drives_needed -= 1
            else:
                if drive in drive_bays:
                    retval += drive_mappings[drive]
                    drives_needed -= 1

            if drives_needed == 0:
                return retval

    if drives_needed > 0:
        raise wd_exceptions.InvalidDeviceState('Not all requested drives are available.')
    else:
        # Started at 0, so return all available drives
        return retval
