__author__ = 'hoffman_t'

from datetime import timedelta
import time
import string
import threading
import os
import sys
import json
import platform
import struct

if platform.system() == 'Windows':
    from ctypes import windll, create_string_buffer

import colorama

from wd_localfilesystem import get_unique_filename


class Tracker(object):
    """  Track the test progress and results, log data, and show status.

    Features:
        * Dynamic dictionary used to store anything needed by the calling
          function, including test configuration, test progress,
          and test results.
        * Dynamic status screen that can display a two-column status
          screen on the console. The contents of each column are dynamically-
          generated and controlled by the calling function.
        * Dynamic report generation to any combination of log file, screen,
          or a specific file.
        * Auto logfile name generation based off the calling function's
          script name. If the logfile already exists, the class will
          prompt the user to append, overwrite, or rename the file.
        * Support for a continuation file which will save the contents of the
          testdata dictionary every 10 seconds. When the class is constructed,
          if the continuation file exists, the class asks to continue the test.
          If the user chooses to continue, the testdata dictionary is
          reloaded from the continuation file. It is up to the calling script
          to handle the continuation logic from this point.

    The __init__ constructor takes arguments to specify the logfile
    location, test title, and the name of the continuation file. See the
    __init__ documentation for more details.

    To track test progress, results, or configuration, use the
    testdata dictionary. See below for details on this attribute.

    To display the on-screen status, first set up columnlist. See below for
    details on this attribute.

    To start displaying the status, call start_status(). See the documentation
    for the startstatus() method for details.

    In addition to testdata, the message() method can be used to
    message the user. The message can be displayed on the status screen,
    in the log, or both (default).

    Attributes:
        testdata - For each item you want to track, add an entry to the
                   testdata dictionary. The key of each entry  is used by
                   the status screen and log as the field name.
                   The value is the data and can be anything.
        columnlist - This is a list of two lists. The first list
                    (columnlist[0]) is a sequence of fields to display
                    in the left column of the status. The second list
                    (columnlist[1]) is the right column. Use the same values
                    as the key names in testdata. It is recommended to
                    assign constants to the values to prevent typos.

                    If an item in columnlist is not found in testdata,
                    it will be considered a section header. The status will
                    add three rows to the column. The first and last rows
                    will be a row of dashes, and the field name will be put
                    between them. For example, including 'Test Information'
                    where 'Test Information' isn't in testdata, gives
                    this in the status:

                                ----------------
                                Test Information
                                ----------------

        specialfields - Dictionary of fields that need special formatting.
                        The key is the field name and should match a key
                        in testdata. The value is the type of formatting
                        to apply.

                        Formats are defined as class constants. Possible
                        formats are:
                            FORMAT_TESTCOUNT - Appends totaltests to the
                                               field's value. For example,
                                               instead of displaying
                                               "Test Number: 10," display
                                               "Test Number: 10/100."
                            FORMAT_RUNTIME - Displays either the time elapsed
                                             since the test was started
                                             or "In Setup."

                            FORMAT_TIME_REMAINING - Displays either the time
                                                    remaining for the test or
                                                    "In Setup."

                                                    Set the field's value in
                                                    testdata to the duration
                                                    of the test in seconds.
                            FORMAT_ERROR - If the value > 0, highlights
                                           it as red.
                            FORMAT_WARNING - If the value > 0, highlights it
                                             as yellow.
                            FORMAT_BOOLEAN - If the value is True, displays
                                             "Yes." If False, displays "No."
                            FORMAT_PERCENT - Adds a percent to the
                                             end of the value
                            FORMAT_LOOKUP - Looks the value up in a dictionary
                                            and uses the dictionary value
                                            instead of the raw value.
                            FORMAT_TIMEOUT - Used for timeout countdowns.
                                             If it reaches 0, it displays
                                             "Timeout Reached" in red.
                                             Value needs to be manually updated
                                             by calling function.

                                             If value is None, displays "N/A".
                            FORMAT_ROUND - Rounds the value to 1 decimal place
        lookup_tables - A dictionary that contains lookup tables. The key will
                        be a fieldname and should match a key in the
                        dictionary. The value is another dictionary that maps
                        raw values (such as'c') to friendly values
                        (such as 'Cold Boot').


        total_tests - The total number of tests that will be run.

    Class Properties:
        time_elapsed - The amount of time that has elapsed since the test
                       has started.
        continue_test - If True, the user has elected to continue the test.
        logfile - The name of the logfile

    Example code:
        import testtrack

        TESTRUNS = 'Number of tests'
        FAILURES = 'Failures'
        COMPORT = 'Serial port'
        REBOOT_TYPE = 'Reboot Method'

        track = testtrack.Tracker ('My script v2.1')

        track.errorFields = (ERRORS_WRITE, ERRORS_READ)
        track.testCountField = TESTRUNS

        track.testdata[TESTRUNS] = 1
        track.testdata[FAILURES] = 0
        track.testdata[COMPORT] = 'COM1'
        track.testdata[REBOOTTYPE] = 'Cold boot'

        track.columnlist[0] = ['Test Configuration', REBOOTTYPE]
        track.columnlist[1] = ['Test Progress', TESTRUNS, FAILURES]

        track.start_status(refreshRate=5)
        track.message('Test has begun')
"""

    testdata = {}
    columnlist = [[], []]
    lookup_tables = {}
    total_tests = None
    specialfields = {}

    # Constants for formatting special fields
    FORMAT_TESTCOUNT = 1
    FORMAT_RUNTIME = 2
    FORMAT_TIME_REMAINING = 4
    FORMAT_ERROR = 8
    FORMAT_WARNING = 16
    FORMAT_BOOLEAN = 32
    FORMAT_PERCENT = 64
    FORMAT_LOOKUP = 128
    FORMAT_TIMEOUT = 256
    FORMAT_ROUND = 512

    # Properties
    @property
    def time_elapsed(self):
        return self._time_elapsed

    @property
    def logfile(self):
        return self._logfile

    @property
    def continue_test(self):
        return self._continue_test

    # Key name for the log file in the continuation file
    LOG_FILE = 'Continuation_Log File'

    # Key names for attributes that are dumped to the log and stored in the
    # continuation file
    TOTAL_TESTS = 'Continuation_Total Tests'
    RUN_TIME = 'Continuation_Run_Time'

    # Init the properties
    _logfile = None
    _time_elapsed = None
    _continue_test = False
    _logfile_id = ''
    _silent_mode = False
    _silk_mode = False
    _status_file = None

    # The name of the continuation file
    _continuation_file = ''

    # A message to display at the bottom of the status screen
    _message = None

    # The time the test was started
    _test_starttime = None

    # Console parameters
    _consolewidth = None
    _consoleheight = None
    _title = ''

    # When true, the status is auto-refreshing
    _is_status_running = False
    # Controls how often the update timer will run.
    _refreshrate = 1
    # Used to stop the status refresh
    _stopper_status = None
    # Tracks the last string used for the status text
    _previous_status = None
    # Tracks the previous runtime so it is added
    # to the elapsed time on a continuation
    _previous_runtime = 0

    def start_status(self, refreshrate=1):
        """Start the status that is displayed on the console.

        This will repeat once every refreshrate seconds. Call stop_statusatus()
        to stop repeating.  Once the status is started, any updates to
        testdata or columnlist will be reflected on the next refresh.

        If called while the status is already started, it will change the
        refresh rate. This will also immediately update the status.

    """

        if self._is_status_running:
            # The status is already running, so stop it first.
            # This will avoid having multiple instances running.
            # It also will allow it to immediately change the refresh rate.
            self.stop_status()

        self._refreshrate = refreshrate
        self._stopper_status = self._refreshstatus()
        self._is_status_running = True

    def stop_status(self):
        """Stop the auto-refresh of the status."""

        self._stopper_status.set()
        self._is_status_running = False

    def message(self, text, log=True, status=True, console=False):
        """  Message the user

        Keyword arguments:
            text - The text to send to the user
            log - If true, log the text
            status - If true, display it on the bottom of the status screen
            console - If true, print it directly to the console

        """

        if status:
            self._message = str(text)

        if log:
            self._log(str(text))

        if console:
            print text

    def log(self, text):
        """ Wrapper for message() that sends text to the log only """
        self.message(text, log=True, status=False, console=False)

    def status(self, text):
        """ Wrapper for message() that sends text to the status only """
        self.message(text, log=False, status=True, console=False)

    def console(self, text):
        """ Wrapper for message() that sends text to the console only """
        self.message(text, log=False, status=False, console=True)

    def end_test(self):
        """End the test.

        Will stop the status and write final data to the .cont file

        """

        self.stop_status()
        self._write_continuation_file()

    def start_test(self):
        """Start the test.

        This will set _test.starttime. It will also put a message in the log
        stating that the test has started and will dump testData to the log.
        This way, the test configuration will be recorded in the log.

        It also will begin updating the continuation file if applicable.

        """

        if self._test_starttime is None:
            self._test_starttime = time.time()

        self._log('')
        if self.continue_test is True:
            self._log('*** Continuing Previous Test Run ***')
        else:
            self._log('*** New Test Run ***')
        self._log('')
        self._log('*** Test Configuration Data***')
        timestring = time.strftime('%Y/%m/%d - %I:%M:%S %p - ',
                                   time.localtime())
        with open(self._logfile, 'ab') as data:
            data.write(timestring)
            json.dump(self.testdata, data)
            data.write(os.linesep)

        if self._continuation_file:
            self._log('Continuation file: ' + self._continuation_file)
            self._start_continuation_file()

    def clear_log(self):
        """Clear the log file."""
        if os.path.isfile(self._logfile):
            os.remove(self._logfile)

    class Timeout(object):
        # Used to track timeouts
        _lastupdate = 0

        @property
        def remaining(self):
            return self._lastupdate

        def __init__(self, duration, printstatus=False):
            """Class constructor.

            Keyword arguments:
                duration - The length of the timeout
                printstatus - If true, uses the message() method of the
                              Tracker class to display the timeout information
                              to the user. This will only go to the status,
                              not the console or log.
            """
            self._duration = int(duration)
            self._starttime = 0
            self._printstatus = printstatus
            self._lastupdate = self._duration

            if self._printstatus:
                self.message('Waiting up to ' +
                             str(self._duration) + ' seconds', log=False,
                             console=False)

        def start(self):
            """Start or restart the timeout timer."""
            self._starttime = time.time()

        def isreached(self):
            """"Return True if the timeout has been reached, false if not."""
            elapsed = time.time() - self._starttime

            if elapsed >= self._duration:
                return True
            else:
                remaining = self._duration - elapsed

                if self._printstatus:
                    # Print an update every time it is called

                    if remaining < self._lastupdate - 10:
                        print '{} seconds remaining'.format(remaining)

                self._lastupdate = remaining

                return False

    def _generate_logfile_name(self, is_unique=True, logfile_path=''):
        """Return a logfile name based off the script name

        Keyword arguments:
            isUnique - if True, create a unique name, otherwise prompt

        """

        # Determine the logfile name
        if self._logfile_id is None:
            logfile_base = os.path.join(logfile_path,
                                        os.path.basename(os.sys.argv[0]))
        else:
            logfile_base = os.path.join(logfile_path,
                                        os.path.basename(os.sys.argv[0]) +
                                        str(self._logfile_id))

        # Get unique file names
        if is_unique is True:
            logfile_name = get_unique_filename(logfile_base, '.log')
        else:
            logfile_name = logfile_base + '.log'

        # If the log file already exists, ask the user to overwrite or append
        if os.path.exists(logfile_name):
            if self._silent_mode:
                # Automatically append
                answer = 'a'
            else:
                user_prompt = ('A log file already exists. ' +
                               'Would you like to: ' + os.linesep +

                               'overwrite (o), append (a), or ' +
                               'rename (r) the file?'
                               )
                answer = prompt_user(message=user_prompt,
                                     options=['o', 'a', 'r'],
                                     default='a')
            if answer == 'o':
                # Delete the log file
                os.remove(logfile_name)
            elif answer == 'r':
                # Call this function recursively with the isUnique flag True
                logfile_name = self._generate_logfile_name(True)
                # Do nothing for append

        return logfile_name

    def __init__(self,
                 title,
                 unique_logname=True,
                 logfile_name=None,
                 logfile_path='',
                 logfile_id='',
                 continuation_file=None,
                 silent_mode=False):

        """Class constructor.

        Keyword arguments:
            title - The title that appears at the top of the status and in
                    the log. It is usually a friendly name for the script and
                    should include version information.

            unique_logname - If True, the logfile name that is generated
                             will always be unique. This is ignored if
                             logfile_name is passed. In that case, it is up
                             to the calling function to create a unique
                             file name.

            logfile_path - If passed, the logfile specified in logfile_name is
                           stored in the specified path. If logfile_name is not
                           passed, the generated logfile name is stored in the
                           specified path.

            logfile_name - If passed, the supplied logfile name is used
                           instead of the class generating a name. If the file
                           exists, data will be appended to the existing file.
                           It is up to the calling function to delete the file
                           if this is not desired.

                           If not passed, the class will generate a name based
                           off the script name. If the file exists,
                           the user will be prompted to overwrite, append,
                           or use a new name.

                           self._logfile will automatically be updated
                           with the logfile name to allow the calling function
                           to display the name of the logfile to the user
                           if desired.

                           This parameter can be an absolute path. However,
                           keep in mind that it will be concatenated with
                           logfile_path, so take care if both parameters are
                           passed.

            logfile_id - If passed, the logfile_id is appended to the logfile
                         name. This allows running the same script multiple
                         times on the same machine. An example of a logfile_id
                         would be the IP address of the UUT, though anything
                         that is part of a valid filename can be used.

            continuation_file - If passed, checks if the test can be continued
                                and prompts the user to continue or restart
                                the test. If the user chooses to continue,
                                any existing logfile will be appended to
                                automatically. The constructor will set
                                the continue_test attribute to True to inform
                                the calling script that the test should
                                be continued.

                                If the continuation_file does not exist,
                                it is created and test progress will be
                                written to the file to allow the test to
                                be continued if it is interrupted.

                                If not passed, continuation is not supported
                                and the test will have to be restarted if it
                                is interrupted.

                                To support continuing, the contents of
                                testdata are dumped to the continuation file
                                every 10 seconds. When the user chooses to
                                continue a test, testdata is loaded from the
                                continuation file.

                                If not passed, a continuation file is still
                                built so data can be recovered in case the
                                test system crashes. It will not, however,
                                prompt the user to continue and will not load
                                the data when the test is restarted.

            silent_mode - If True, does not ask the user to continue the test.
                          Will automatically start it over and append to any
                          existing log. Used mainly for automation.

            NOTE: If the test is continued, logfile_name is ignored, and the
                  previous logfile will be appended to.

        """

        self._consolewidth, self._consoleheight = getconsolesize()
        self._title = title
        self._logfile_id = logfile_id
        self._silent_mode = silent_mode

        # Configure colorama
        colorama.init()

        self._continue_test = False

        if continuation_file:
            continuation_file = os.path.join(logfile_path, continuation_file)

            self._continuation_file = continuation_file
            # Try to reload the test
            if self._reload_test() is True:
                # Test was reloaded, use the old log file
                self._logfile = self.testdata[self.LOG_FILE]
                # Set the attribute
                self._continue_test = True
                # Create the status file name
                self._status_file = self._logfile + '_status.txt'
                return

        if logfile_name:
            # Use the supplied logfile name
            self._logfile = os.path.join(logfile_path, logfile_name)
        else:
            # Generate a logfile name
            self._logfile = self._generate_logfile_name(unique_logname,
                                                        logfile_path)

        if continuation_file is None:
            # Create a continuation file that will hold the data
            # so it can be recovered in the event of a test interruption
            self._continuation_file = self._logfile + '.cont'

        # Create the status file name
        self._status_file = self._logfile + '_status.txt'

        # Save the remaining attributes into the continuation file
        self.testdata[self.LOG_FILE] = self._logfile
        self.testdata[self.TOTAL_TESTS] = self.total_tests

        # See if the silk file exists. If so, put it in silk mode.
        if os.path.exists('silk.tt'):
            self._silk_mode = True

    def _reload_test(self):
        # See if the continuation file exists, ask the user to continue,
        # and return True if continuing
        if os.path.exists(self._continuation_file):
            if self._silent_mode is False:
                # Ask the user to continue
                if prompt_user('Would you like to continue the previous test') == 'Y':
                    # Read the data into testData
                    with open(self._continuation_file) as data:
                        self.testdata = json.load(data)
                        if self.RUN_TIME in self.testdata:
                            self._previous_runtime = (
                                self.testdata[self.RUN_TIME])
                        else:
                            self._previous_runtime = 0

                        if self._previous_runtime is None:
                            self._previous_runtime = 0

                    return True

            # Remove the continuation file
            os.remove(self._continuation_file)
            return False

    def change_title(self, title):
        """Change the title of the status screen."""
        self._title = title

    def set_test_count(self, number_tests=None):
        if number_tests:
            self.total_tests = number_tests

    def generatereport(self,
                       columns,
                       specialfields=None,
                       reportwidth=None,
                       reportfile=None,
                       log=True,
                       console=True):
        """ Generate a report.

            Keyword arguments:
                columns - A tuple of two tuples. The first tuple is the left
                          column. The second is the right column. See the
                          documentation for the class columnlist attribute for
                          details on how this works.

                specialfields - If passed, contains formatting options for
                                field. Default is the class attribute's value
                reportwidth - If passed, sets the width of the report. Default
                              is the console width.
                reportfile - If passed, send the report to the specified file.
                             It will append to the file if it already exists.
                log - If true, send the report to the log file.
                console - If true, send the report to the console.

                NOTE: reportfile, log, and console can all be set independently
                to allow sending to multiple locations.

        """

        if reportwidth is None:
            reportwidth = self._consolewidth

        if specialfields is None:
            specialfields = self.specialfields

        reporttext = self._generatereport(columns, specialfields, reportwidth)

        if reportfile is not None:
            try:
                report = open(reportfile, 'ab')
                report.write(reporttext)
                report.close()
            except:
                self._log('Error writing report file ' + reportfile)

        if log:
            self._log(reporttext, includetime=False)

        if console:
            clearscreen()
            print reporttext

    # noinspection PyMethodParameters
    def _setinterval(interval, times=-1):
        # Decorator used to cause a function to repeat.
        # Based on http://stackoverflow.com/questions/5179467/equivalent-of-setinterval-in-python @IgnorePep8
        # Retrieved 2013 July 23

        # NOTE: Do not add "self" to the parameter list, or this will break.

        # This will be the actual decorator,
        # with fixed interval and times parameter
        def outer_wrap(function):

            # This will be the function to be called
            def wrap(self, *args, **kwargs):
                stop = threading.Event()

                if interval is None:
                    _interval = self._refreshrate
                else:
                    _interval = interval

                # This is another function to be executed
                # in a different thread to simulate setInterval
                def inner_wrap():
                    i = 0
                    while i != times and not stop.isSet():
                        stop.wait(_interval)
                        function(self, *args, **kwargs)
                        i += 1

                t = threading.Timer(0, inner_wrap)
                t.daemon = True
                t.start()
                return stop

            return wrap

        return outer_wrap

    def _formatcolumns(self, columnlist, specialfields):
        # Build a list containing the data that should be displayed
        # in the columns. Elements that are in an odd index are
        # in the left column.
        # This list is then read through item by item, printing each
        # line of the status. The first value is used for the field name.
        # The second value is used for the data for that field.
        # If no data is needed, use none for the second element.
        # If a value is present for the data, a colon and space
        # is added to the field name.

        columns = [[], []]
        format_length = []
        padding = 0

        for column in (0, 1):
            if columnlist[column]:
                for dummy, item in enumerate(columnlist[column]):
                    value = None
                    format_chars = ''

                    if item != '' and item is not None:
                        if item in self.testdata:
                            value = self.testdata[item]

                        # Format the value
                        if item in specialfields:
                            value, format_chars = self._format_value(
                                item,
                                value,
                                specialfields[item])
                        if value is not None:
                            columns[column].append([item, value])
                            # Only append for column 0 because formatting in
                            # column 1 does not affect alignment
                            if column == 0:
                                format_length.append(len(format_chars))
                        else:
                            # No value, so it's a header
                            columns[column].append(['-' * len(str(item))])
                            columns[column].append([item])
                            columns[column].append(['-' * len(str(item))])
                            if column == 0:
                                format_length.append(0)
                                format_length.append(0)
                                format_length.append(0)
                    else:
                        # Blank line
                        columns[column].append([''])
                        if column == 0:
                            format_length.append(0)

        # Even out the columns
        if len(columns[0]) < len(columns[1]):
            # The first column is shorter, so pad it
            padding = len(columns[1]) - len(columns[0])
            for dummy in range(0, padding):
                columns[0].append([''])
                # Add a format_length so there will be no index error when
                # column 1 is longer than column 0
                format_length.append(0)

        if len(columns[1]) < len(columns[0]):
            padding = len(columns[0]) - len(columns[1])
            for dummy in range(0, padding):
                columns[1].append([''])

        return columns, format_length

    def _format_value(self, field, value, format_type):
        # Apply formatting to the value
        reset = ''
        color = ''

        # The first group of formats do not require a value
        if format_type & self.FORMAT_RUNTIME > 0:
            if self._test_starttime:
                elapsed = (int
                           (time.time() - self._test_starttime +
                            int(self._previous_runtime)
                            )
                           )
                self._time_elapsed = timedelta(seconds=elapsed)
                # Store the total elapsed time as a number of seconds
                self.testdata[self.RUN_TIME] = elapsed
                value = self._time_elapsed
            else:
                value = 'In Setup'
                color = colorama.Fore.GREEN  # @UndefinedVariable

        if format_type & self.FORMAT_TIMEOUT > 0:
            if value:
                if value <= 0:
                    # Ran out
                    value = 'Timeout Reached'
                    color = colorama.Fore.RED  # @UndefinedVariable
                else:
                    # Round to nearest second
                    value = int(float(value))
            else:
                value = 'N/A'

        if format_type & self.FORMAT_TIME_REMAINING > 0:
            if self._test_starttime:
                if value:
                    endtime = self._test_starttime + value
                    timeremaining = endtime - time.time()

                    if timeremaining <= 0:
                        value = 'Complete'
                        color = colorama.Fore.GREEN  # @UndefinedVariable
                    else:
                        value = str(timedelta(seconds=int(timeremaining) + 1))
                else:
                    value = 'Indefinite'
            else:
                value = 'In Setup'
                color = colorama.Fore.GREEN  # @UndefinedVariable

        if value is not None:
            # Each of these require a value
            if format_type & self.FORMAT_ERROR > 0:
                if int(value) > 0:
                    # There are errors, so highlight as red
                    color = colorama.Fore.RED  # @UndefinedVariable

            if format_type & self.FORMAT_WARNING > 0:
                if int(value) > 0:
                    # There are warnings, so turn the text yellow
                    color = colorama.Fore.YELLOW  # @UndefinedVariable

            if format_type & self.FORMAT_BOOLEAN > 0:
                if value:
                    value = 'Yes'
                    color = colorama.Fore.GREEN  # @UndefinedVariable
                else:
                    value = 'No'

            if format_type & self.FORMAT_TESTCOUNT > 0:
                if int(float(value)) == 0:
                    value = 'In Setup'
                    color = colorama.Fore.GREEN  # @UndefinedVariable
                else:
                    if self.total_tests:
                        value = str(value) + ' / ' + str(self.total_tests)

            if format_type & self.FORMAT_PERCENT > 0:
                value += '%'

            if format_type & self.FORMAT_LOOKUP > 0:
                if field in self.lookup_tables:
                    lookup_table = self.lookup_tables[field]

                    if value in lookup_table:
                        value = lookup_table[value]

            if format_type & self.FORMAT_ROUND > 0:
                value = round(float(value), 1)

        if value is not None:
            if color != '':
                reset = colorama.Fore.RESET  # @UndefinedVariable

            if self._silk_mode:
                returntext = str(value)
                formattext = ''
            else:
                returntext = color + str(value) + reset
                formattext = color + reset
            return returntext, formattext
        else:
            return '', ''

    @_setinterval(10)
    def _start_continuation_file(self):
        # Starts the background process to save the test every 10 seconds
        self._write_continuation_file()

    def _write_continuation_file(self):
        try:
            with open(self._continuation_file, 'w') as data:
                json.dump(self.testdata, data)
        except:
            self._log('WARNING: Could not save continuation file.')

    @_setinterval(None)
    def _refreshstatus(self):
        self._showstatus()

    def _generatereport(self, columnlist, specialfields=None, consolewidth=None):
        # Returns a string containing text that can be used for the status
        # or a report. Pass a two item tuple containing the desired columns.
        # consolewidth can be passed to override the console width. Useful when
        # generating a report meant for a file, not the console.

        if consolewidth is None:
            consolewidth = self._consolewidth

        if specialfields is None:
            specialfields = {}

        # The starting location for the first column.
        # It's set to be 1 character from the left, but is here so it can be
        # easily adjusted if necessary
        firstcolumn = 1

        # The starting location for the second column.
        # It's half of the screenWidth + 3
        secondcolumn = consolewidth / 2 + 3

        columns, formatlength = self._formatcolumns(columnlist, specialfields)

        # First, determine the longest key length in each column
        # for alignment purposes
        max_fieldlength = [0, 0]
        for column in (0, 1):
            for index, element in enumerate(columns[column]):
                field = str(element[0])

                if len(element) == 2:
                    # Only count it if there is a value so horizontal lines and
                    # section names don't cause the colon to be
                    # shifted all the way over
                    if len(field) > max_fieldlength[column]:
                        max_fieldlength[column] = len(field)

        # Create the text for the report. Start with the header
        reporttext = ('*' * consolewidth + '* ' +
                      string.center(self._title, consolewidth - 4) +
                      ' *' + '*' * consolewidth
                      )

        # Now, build the rest of the report text
        infolines = []
        columntext = ''

        for column in (0, 1):
            for index, element in enumerate(columns[column]):
                # Do not convert to a string yet, or they become "None"
                field = element[0]

                if field == '':
                    field = None

                value = None

                if len(element) == 2:
                    value = element[1]

                    if value == '':
                        value = None

                if field:
                    # Determine the number of leading spaces
                    if column == 0:
                        leadingspaces = (' ' * firstcolumn)
                        maxwidth = consolewidth / 2
                    else:
                        # Second column, so reduce the number of spaces
                        # by the length of the previous column text
                        leadingspaces = (' ' *
                                         (secondcolumn -
                                          len(infolines[index]) +
                                          formatlength[index]))
                        maxwidth = consolewidth

                    # The string to place in the column
                    columntext = leadingspaces + field

                    if value is not None:
                        spacecount = max_fieldlength[column] - len(field)
                        columntext += (' ' * spacecount) + ': ' + str(value)

                        # Truncate if too long
                        if len(columntext) > maxwidth:
                            columntext = (columntext[:maxwidth - 1] +
                                          '...')
                else:
                    columntext = ''

                if column == 1:
                    columntext += os.linesep

                if column == 0:
                    # For the first column, add a new line
                    infolines.append(columntext)
                else:
                    # Add to the text if it's the second column
                    infolines[index] += columntext

        # Now, append the infolines to the reporttext
        for index in range(len(infolines)):
            reporttext += infolines[index]

        return reporttext

    def _showstatus(self):
        # Show the status. Pass a two item tuple containing the desired columns
        # and a dictionary of any special fields

        statustext = self._generatereport(self.columnlist, self.specialfields)

        # Append the user message, if any
        if self._message is not None:
            statustext += os.linesep + self._message

        # See if the status text has changed. If so, refresh the screen.
        # If not, do nothing. This prevents flickering if the status
        # is called repeatedly.
        if statustext != self._previous_status:
            clearscreen()
            self._previous_status = statustext

            # Supress the output if silk mode is on or if the refreshrate
            # is 0 or less.
            if not self._silk_mode and self._refreshrate > 0:
                print statustext

            # Also output to self._status_file
            statusfile = open(self._status_file, 'wb')
            statusfile.write(statustext)
            statusfile.close()

    def _log(self, text, includetime=True):
        # Add the text to the log file.
        # If includetime is False, do not include the timestamp.
        logfile = open(self._logfile, 'ab')
        if os.linesep not in text:
            # Add a line break
            text += os.linesep

        if includetime:
            timestring = time.strftime('%Y/%m/%d - %I:%M:%S %p - ',
                                       time.localtime())
            text = timestring + text

        logfile.write(text)
        logfile.close()

        # Dump to the screen when in silkmode
        if self._silk_mode:
            print text.strip


def getconsolesize():
    # Returns the width of the screen as x, y
    # Code based on
    # http://code.google.com/p/python-consolesize/downloads/detail?name=consolesize-1.0.tar.gz&can=2&q=class

    if platform.system() == 'Windows':
        # stdin handle is -10
        # stdout handle is -11
        # stderr handle is -12

        h = windll.kernel32.GetStdHandle(-12)  # @UndefinedVariable
        csbi = create_string_buffer(22)
        res = windll.kernel32.GetConsoleScreenBufferInfo(h, csbi)  # @UndefinedVariable @IgnorePep8

        if res:
            (u_bufx, u_bufy, u_curx, u_cury, u_wattr,
             left, top, right, bottom, u_maxx, u_maxy) = struct.unpack(
                "hhhhHhhhhhh", csbi.raw)
            sizex = right - left + 1
            sizey = bottom - top + 1
        else:
            # Can't determine actual size - return default values
            sizex, sizey = 80, 25
    else:
        try:
            (sizey, sizex) = os.popen('stty size', 'r').read().split()
        except:
            sizey = 22
            sizex = 80

    return int(sizex), int(sizey)


def clearscreen():
    if os.name == 'nt':
        command = 'cls'
    else:
        command = 'clear'
    os.system(command)


def prompt_user(message, options=None, default='Y'):
    """Prompt the user with message and return the answer.

    Keyword arguments:
        message - A string that is displayed to the user
        options - A list of possible options (default is ['Y', 'N']
        default - The default return value if the user hits enter.
                  The default option will be capitalized and
                  all other options will be lower-case.
                  Set to None or '' for no default

    Return the selected option.

    NOTE: Although the matching is case insensitive, the result
    will be the same case as in the options list.

    Based on: http://stackoverflow.com/questions/3041986/python-command-line-yes-no-input  # @IgnorePep8
    Retrieved 25 Jun 2013

    Heavily modified to accept any set of options, not just
    Yes/No, and to be case insensitive.

    """

    # Convert the default to a lower case string
    if default:
        default = str(default).lower()
    else:
        default = ''

    if options is None:
        options = ['Y', 'N']

    # Convert the options into a prompt
    prompt = ''

    for option in options:
        option_string = str(option).lower()

        if default == option_string:
            # Add it as upper case
            prompt += option_string[:1].upper() + option_string[1:]
        else:
            # Add it as lower case
            prompt += option_string

        # Add a slash
        prompt += '/'

    # Remove the last slash
    prompt = prompt[:-1]

    # Now, present the prompt
    while True:
        sys.stdout.write(message + ' (' + prompt + ') -> ')
        choice = raw_input().lower()
        if default != '' and choice == '':
            return default
        else:
            # See if it's in the options
            for option in options:
                # Ignore extra characters after the option,
                # allowing the user to enter "yes" instead of 'y'
                # if the option is 'y'

                if choice[:len(option)] == option.lower():
                    # It's a match, so return the option
                    return option

        sys.stdout.write('Invalid selection. Please try again' + os.linesep)
