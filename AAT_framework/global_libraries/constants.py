from seleniumAPI.src.ui_map import Pages
from seleniumAPI.src.ui_map import Elements

from global_libraries.testdevice import Fields
from global_libraries.testdevice import DeviceDefaults

class Modes(object):
    CLOUD_ACCESS_AUTO = 'auto'
    CLOUD_ACCESS_MANUAL = 'manual'
    CLOUD_ACCESS_WINXP = 'winxp'

class RebootType(object):
    WARM = 'warm'
    COLD = 'cold'

class USB_Backup_Mode(object):
    COPY = '1'
    SYNC = '2'
    INCREMENTAL = '3'
