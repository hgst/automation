""" Contains a collection of methods used to work with the network.

Created on January 7, 2015

@author: Troy Hoffman
(C) 2015 Western Digital Corporation

"""
import socket
import binascii
import string
import re
import urllib2
import os

from CommonTestLibrary import configure_logger

logger = configure_logger('AAT.wd_networking')

def clean_mac_address(mac_address):
    """ Removes the separators from the MAC address

    :param mac_address: MAC address to strip
    :return: A string of just the hex values of the MAC address
    """

    # Start by getting just the hex values from the mac_address
    new_mac = re.sub('[^' + string.hexdigits + ']', '', mac_address)

    if len(new_mac) == 12:
        # Valid string, so use it
        return string.upper(new_mac)
    else:
        raise ValueError('MAC address of ' + mac_address + ' is invalid.')


def get_broadcast_ip(ip_address):
    """ Returns the broadcast IP address of ip_address

    :param ip_address: The device's IP address
    :return: The broadcast IP address
    """
    split_ip = ip_address.split('.')
    broadcast_ip = split_ip[0] + '.' + split_ip[1] + '.' + split_ip[2] + '.255'

    return broadcast_ip


def send_wol(mac_address, ip_address='255.255.255.255', password='', wol_port=9):
    """ Sends a magic packet to the device specified by mac_address and ip_address to wake it up using WoL

    :param mac_address: MAC address of the device
    :param ip_address: Last known IP address of the device
    :param password: Optional WoL password
    :param wol_port: Optional WoL port
    """
    # Get the packet
    magic_packet = _get_magic_packet(mac_address, password)

    # *** Step 1 - Configure the socket ***
    # Create a socket
    wol_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Enable permission to transmit broadcast messages (set SO_BROADCAST to 1) on the socket level (SOL_SOCKET)
    wol_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    # *** Step 2 - Create the connection ***
    # AF_INET addresses are a tuple of (host, port)
    address = (get_broadcast_ip(ip_address), wol_port)
    # Make the connection.
    wol_socket.connect(address)

    # *** Step 3 - Send the data ***
    # Send the packet out
    wol_socket.send(magic_packet)

    # *** Step 4 - Cleanup ***
    # Close the socket
    wol_socket.close()


def _get_magic_packet(mac_address, password=''):
    """ Returns the magic packet for the specified MAC address

    :param mac_address: The MAC address for the packet
    :return: The magic packet
    """
    mac = clean_mac_address(mac_address)

    # Create the "header" of the WoL magic packet payload
    magic_packet = binascii.a2b_hex('FF' * 6)
    # Add 16 repetitions of the MAC address
    magic_packet += binascii.a2b_hex(mac * 16)

    if password:
        if len(password) != 8 and len(password) != 12:
            raise ValueError('WoL password of ' + password + ' is invalid. Password must be 4 or 6 hexadecimal digits.')
        else:
            magic_packet += binascii.a2b_hex(password)
    return magic_packet

def is_web_server_up(host):
    """ Returns True if the web server is up. False if it is not.

    :param host: The IP address or host name of the web server
    :return: True or False
    """

    url = 'http://' + str(host)
    try:
        urllib2.urlopen(url)
        return True
    except urllib2.URLError:
        logger.debug('Unable to connect to ' + url)
        return False

def is_url_reachable(url):
    try:
        web_page = urllib2.urlopen(url)
    except urllib2.URLError:
        return False
    else:
        web_page.close()
        return True

def download_web_file(url, destination='.', dest_filename=None):
    source_file = urllib2.urlopen(url)

    try:
        new_url = source_file.url
        final_source_file = urllib2.urlopen(new_url)
    except:
        new_url = url
        final_source_file = source_file

    if not dest_filename:
        dest_filename = os.path.basename(new_url)

    final_filename = os.path.join(destination, dest_filename)

    with open(final_filename, 'wb') as dest_file:
        dest_file.write(final_source_file.read())

    return final_filename


