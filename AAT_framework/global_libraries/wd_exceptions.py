class SSHDisabledError(Exception):
    pass


class DeviceUnreachableError(Exception):
    pass


class InsufficientFreeSpace(Exception):
    pass


# wd_shares exceptions
class UnsupportedProtocol(Exception):
    pass


class ShareNotMounted(Exception):
    pass

class ShareReadOnly(Exception):
    pass

class UnsupportedOperatingSystem(Exception):
    pass


class ActionTimeout(Exception):
    pass


class InvalidElementType(Exception):
    pass


class ItemNotInList(Exception):
    pass


class GenerateFileError(Exception):
    pass

class VerifyFileError(Exception):
    pass

class PasswordError(Exception):
    pass

class FileNotFoundError(Exception):
    pass

class NexusUnreachable(Exception):
    pass


class CannotReachPage(Exception):
    pass

class RestCallUnsuccessful(Exception):
    pass

class InvalidDefaultValue(Exception):
    pass

class InvalidParameter(Exception):
    pass

class InvalidCommandLineOption(Exception):
    pass

class InvalidDeviceState(Exception):
    pass

class UnsupportedLanguage(Exception):
    pass

class ProductMismatch(Exception):
    pass

class WizardAlreadyOpened(Exception):
    pass

class WizardElementUndefined(Exception):
    pass

class WizardUnknownPage(Exception):
    pass

class WizardInvalidOption(Exception):
    pass

class InvalidRAIDLevel(Exception):
    pass

class AuthenticationError(Exception):
    pass

class SocketError(Exception):
    pass

class ValueError(Exception):
    pass

class NoRunningTests(Exception):
    pass

class MultipleRunningTests(Exception):
    pass

class StopWatchNotRunning(Exception):
    pass

class FailedToExecuteShellCommand(Exception):
    pass

class FailedToSetAuthentication(Exception):
    pass

class MediaServerError(Exception):
    pass

class TextMismatch(Exception):
    pass

class ClickElementError(Exception):
    pass

class FailedToSetRemoteAccessServer(Exception):
    pass

class IncorrectModel(Exception):
    pass