""" Contains a collection of methods used to format strings.

Created on Jul 29, 2013

@author: Troy Hoffman
(C) 2013 Western Digital Corporation


"""

import datetime

# Lists for converting between bytes and human-friendly strings
_units_binary = ['Bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB']
_units_decimal = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB']


def integer(string):
    """ Converts a string to an integer

    :param string: String to convert
    :return: Integer value
    """

    return int(float(string))

def bytes_to_units(byte_count, use_binary=True, precision=2):
    """Convert bytes into a string such as 10 KiB, 20 MiB, etc.

    :param byte_count - The number of bytes to convert
    :param use_binary - If true, use binary units (MiB) instead of decimal units (MB)
    :param precision - The number of decimal digits to round to

    Supports the following:
        Binary units: Bytes, KiB, MiB, GiB, TiB, PiB
        Decimal units: Bytes, KB, MB, GB, TB, PB

    units_to_bytes performs the opposite function.

    """
    byte_count = str(byte_count)

    if byte_count.isdigit():
        # Convert to a float so we can get better precision
        bytes_float = float(byte_count)

        if use_binary:
            unit_list = _units_binary
            multiplier = 1024
        else:
            unit_list = _units_decimal
            multiplier = 1000

        index = 0

        # Dividing by two lets this go down to 0.5 units.
        while bytes_float >= multiplier / 2:
            index += 1
            bytes_float /= multiplier

        value = str(round(bytes_float, precision))

        return value + ' ' + unit_list[index]
    else:
        return byte_count


def units_to_bytes(units_string):
    """Take a string (such as 10 KiB) and return a number of bytes as an int.

    :param units_string - A human-readable string, such as "10 KiB"

    Supported units:
        Binary units: Bytes, KiB, MiB, GiB, TiB, PiB
        Decimal units: Bytes, KB, MB, GB, TB, PB

    bytes_to_units performs the opposite function.

    """

    units_string_lower = str(units_string).lower()
    units_binary_lower = [x.lower() for x in _units_binary]
    units_decimal_lower = [x.lower() for x in _units_decimal]

    # First, see if just a number is being passed
    if units_string_lower.isdigit():
        # It's a number, so just return the number of bytes as an int.
        return int(float(units_string_lower))

    # It's a string, so figure out what units are being used
    index = -1
    for unitname in units_binary_lower:
        index += 1
        if unitname in units_string_lower:
            # Unit found, so remove it and any spaces
            return 1024 ** index

    # Not a binary unit.  Try decimal
    index = -1
    for unitname in units_decimal_lower:
        index += 1
        if unitname in units_string_lower:
            # Unit found, so remove it and any spaces
            return 1000 ** index

    return None  # No value, invalid string


def seconds_to_string(time_in_seconds):
    """Take a number of seconds and return a human-readable string.

    :param time_in_seconds - The amount of time in seconds

    """
    return str(datetime.timedelta(seconds=time_in_seconds))


def string_to_seconds(time_string):
    """Take a string, such as 24h, and return the number of seconds.

    :param time_string - The time as a string

    Supported unit labels:
        s - seconds
        m - minutes
        h - hours
        d - days
        w - weeks

    """
    units = {"s": 1, "m": 60, "h": 3600, "d": 86400, "w": 604800}

    timestring_ = time_string.lower()

    # Parse the string and convert to a number and units
    number = int(timestring_[:-1])
    unit = timestring_[-1]

    return number * units[unit]


def hex_to_ascii(hex_string):
    """Take a string of hex characters and convert it to ASCII.

    :param hex_string - The string of hex characters

    The supplied hex_string must start with "0x".  If not, it returns the
    hex_string as-is.

    """
    if hex_string.startswith('0x'):
        # Remove the 0x for conversion purposes and decode
        return hex_string[2:].decode('hex')
    else:
        return hex_string


def get_mb_second(bytes_transferred, seconds, precision=2):
    """Given a number of bytes and a number of seconds, return MB/second.

    :param bytes_transferred - The number of bytes transferred
    :param seconds - The number of seconds it took to transfer
    :param precision - The number of digits to round to
    """
    if seconds <= 0:
        return 0

    bytessecond = float(bytes_transferred) / float(seconds)
    mbsecond = bytessecond / (1024 ** 2)
    return round(mbsecond, precision)
