""" Methods dealing with OS environment variables

"""
import os


class EnvironmentVariables(object):
    """ This class contains wrappers used to interact with the OS environment variables.

    It uses a static variable to hold the sanitized values rather than an instance variable so calling functions
    won't have to create an instance just to read an environment variable.

    """
    _variables = None

    def __init__(self):
        EnvironmentVariables._variables = self.get_variables()

    @staticmethod
    def get_variables():
        """ Parses the OS environment and sanitizes the Silk variables (removes the leading # and makes them lower case)

        :return: A dictionary of environment variables
        """
        environment_variables = {}

        for key in os.environ.keys():
            new_key = key
            if new_key.lower().startswith('#sctm_'):
                # Remove the leading #
                new_key = new_key[1:]

            if new_key.lower().startswith('sctm_'):
                # Make the Silk environment variables all lower case
                new_key = new_key.lower()

            environment_variables[new_key] = os.environ[key]

        return environment_variables

    @staticmethod
    def get(variable):
        """ Returns the value of an OS environment variable

        :param variable: The variable to get
        :return: The value of the variable. Raises a KeyError if it doesn't exist
        """
        if EnvironmentVariables._variables is None:
            EnvironmentVariables._variables = EnvironmentVariables.get_variables()

        if variable.lower().startswith('sctm_'):
            # All silk variables are lower case
            variable = variable.lower()

        return EnvironmentVariables._variables[variable]
