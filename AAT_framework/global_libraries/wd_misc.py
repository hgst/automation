__author__ = 'Troy'
import os

def get_unique_filename(basename, extension):
    """Return a unique file name for baseName + extension."""
    if os.path.isfile(basename + extension):
        # It exists, so try to come up with a unique name
        copynumber = 1

        while os.path.isfile(basename + '_' + str(copynumber) + extension):
            copynumber += 1

        # Do not append extension or you wind up with .extension.extension.
        basename = basename + '_' + str(copynumber)

    # Return the complete name
    filename = basename + extension
    return filename

