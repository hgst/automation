from robot.libraries.BuiltIn import BuiltIn

libraries = {}


def inherit(library):
    """
    Returns an instance of the specified library for use instead of
    inheriting.
    
    In other modules/libraries, put 
    | from Inherit import _inherit
    at the top and
    | self.sshlibrary = _inherit('SSHLibrary')
    in the constructor or function where you need to access the library.
    (Replace SSHLibrary with the library you want to use)
    """
    try:
        return BuiltIn().get_library_instance(library)
    except AttributeError:
        if not library in libraries:
            exec('import {0}'.format(library))
            instance = eval('{0}.{0}()'.format(library))
            libraries[library] = instance
        return libraries[library]


def _inherit(library):
    return inherit(library)
