""" Contains functions used to monitor the UUT's health.

(C) 2015 Western Digital Corporation

"""
import time

from global_libraries.testdevice import Fields
from global_libraries import CommonTestLibrary

from os_modules.linux_health import Health as Health_Linux
from os_modules.metrics import Metrics
from os_modules.metrics import Stats


class HealthMonitor(object):
    OS_LINUX = 'linux'

    # Expose the Metrics class for calling functions
    Metrics = Metrics

    def __init__(self,
                 uut=None,
                 os=OS_LINUX,
                 thresholds=None,
                 command_parameters={}):
        """ Monitors system health

        :param uut: If present, UUT dictionary with info for remote monitoring. If not, the local system is monitored.
                    NOTE: If present, the defaults for the remaining parameters (os, thresholds, command_parameters)
                    will be read from the UUT dictionary.
        :param os: The OS of the target system. Default is Linux.
        :param thresholds: An optional dictionary of metrics and threshold values. Use HealthMonitor.Metrics for
                           the Metric names.
        :param command_parameters: An optional dictionary to override the default health command parameters.
                                   For Linux: {'sar': <string of options>, 'top': <string of options>}

        """

        self._thresholds_crossed = []
        self._hashes_crossed = []

        self._log = CommonTestLibrary.getLogger('AAT.health_monitor')

        self._is_running = False
        self._stop_monitoring = False

        if thresholds is not None:
            self._thresholds = thresholds
        elif uut is not None:
            self._thresholds = uut[Fields.health_thresholds]
        else:
            self._thresholds = {}

        if os == self.OS_LINUX:
            self._health = Health_Linux(uut, command_parameters)

    def start(self, monitor_interval=10):
         if not self._is_running:
            self._stop_monitoring = False
            self._is_running = True
            self._health.start(monitor_interval)
            self._monitor_health(monitor_interval)

    def stop(self):
        self._stop_monitoring = True
        self._is_running = False
        self._stop_monitoring = True

    def get_min(self, metric):
        return self._health.get_min(metric)

    def get_max(self, metric):
        return self._health.get_max(metric)

    def get_total(self, metric):
        return self._health.get_total(metric)

    def get_count(self, metric):
        return self._health.get_count(metric)

    def get_average(self, metric):
        return self._health.get_average(metric)

    def get_thresholds_crossed(self):
        """ Get the list of thresholds crossed

        :return: A list of any thresholds that were crossed
        """
        return self._thresholds_crossed

    @CommonTestLibrary.background_thread()
    def _monitor_health(self, monitor_interval=10):
        while not self._stop_monitoring:
            # Check the thresholds
            for metric in self._thresholds:
                threshold = self._thresholds[metric]

                if Stats.min in threshold:
                    self._check_threshold(metric, Stats.min, self.get_min(metric), threshold[Stats.min])

                if Stats.max in threshold:
                    self._check_threshold(metric, Stats.max, self.get_max(metric), threshold[Stats.max])

                if Stats.total in threshold:
                    self._check_threshold(metric, Stats.total, self.get_total(metric), threshold[Stats.total])

                if Stats.average in threshold:
                    self._check_threshold(metric, Stats.average, self.get_average(metric), threshold[Stats.average])

            for i in xrange(0, monitor_interval):
                if not self._stop_monitoring:
                    time.sleep(.5)

    def _check_threshold(self, metric, stat, value, comparison):
        # Format of comparison is: <operator> value
        # For example: '> 30'
        # Valid operators are: < <= > >= = == != <>

        operator = comparison.split()[0]
        threshold = float(comparison.split()[1])

        metric_name = metric[1:]

        values = {}
        if not self._health.is_dictionary_metric(metric):
            # Turn it into a dictionary
            values['0'] = value
        else:
            values = value

        for key in values:
            val = values[key]
            # See if this threshold has already been crossed
            hash = '{}{}{}'.format(metric, key, stat, comparison)

            if hash not in self._hashes_crossed and val is not None:
                is_crossed = False
                if operator == '<' and val < threshold:
                    is_crossed = True
                    operator = 'less than'

                if operator == '<=' and val > threshold:
                    operator = 'less than or equal to'
                    is_crossed = True

                if operator == '>' and val > threshold:
                    operator = 'greater than'
                    is_crossed = True

                if operator == '>=' and val >= threshold:
                    operator = 'greater than or equal to'
                    is_crossed = True

                if (operator == '=' or operator == '==') and val > threshold:
                    operator = 'equal to'
                    is_crossed = True

                if (operator == '!=' or operator == '<>') and val > threshold:
                    operator = 'not equal to'
                    is_crossed = True

                if is_crossed:
                    if metric == Metrics.cpu_usage:
                        allowed_processes = self._thresholds[metric].get(Stats.allowed_processes, None)
                        if allowed_processes:
                            # See if the listed process is using the most CPU
                            process, cpu = self._health.get_highest_cpu_process()

                            if process in allowed_processes:
                                # It is not crossed, so exit the function
                                return

                    if self._health.is_dictionary_metric(metric):
                        error_message = '{} {} {} value of {} is {} {}'.format(key,
                                                                               metric_name,
                                                                               stat,
                                                                               val,
                                                                               operator,
                                                                               threshold)
                    else:
                        error_message = '{} {} value of {} is {} {}'.format(metric_name,
                                                                            stat,
                                                                            val,
                                                                            operator,
                                                                            threshold)

                    self._log.warning('Health monitor threshold crossed: ' + error_message)
                    self._thresholds_crossed.append(error_message)
                    self._hashes_crossed.append(hash)



