""" Contains Linux-specific OS calls to monitor system health

(C) 2015 Western Digital Corporation

"""
from global_libraries.wd_formatting import integer
from global_libraries import CommonTestLibrary
from sshAPI.src.sshclient import SshClient
from global_libraries.testdevice import Fields

from metrics import Metrics
from metrics import Stats


class Health(object):
    def __init__(self,
                 uut=None,
                 command_parameters=None):
        """ Constructor for the LinuxHealth class.

        :param uut: If present, UUT information for remote monitoring. If not, the local system is monitored.
        :param command_parameters: An optional dictionary to override the default health command parameters.
                                   {'sar': <string of options>, 'top': <string of options>}
        """
        if uut:
            self._ssh = SshClient(uut=uut)
            command_parameters = uut[Fields.command_parameters]
        else:
            self._ssh = None

        if command_parameters is None:
            command_parameters = {}

        # Default command parameters
        if 'sar' not in command_parameters:
            command_parameters['sar'] = '-m ALL -u ALL -P ALL -w -q -r -R -W -B -S -d -p -b ' + \
                                        '-n DEV -n EDEV -n SOCK -n IP -n EIP -n TCP -n ETCP -n UDP'
        self._command_parameters = command_parameters

        self._stop_collecting = False
        self._is_running = False

        # Metrics
        self._default_values = {Stats.count: 0,
                                Stats.min: None,
                                Stats.max: None,
                                Stats.total: 0}

        self._cpu_usage = {}

        self._proc_sec = self._default_values.copy()
        self._mem_free = self._default_values.copy()
        self._mem_used = self._default_values.copy()
        self._swap_free = self._default_values.copy()
        self._swap_used = self._default_values.copy()
        self._disk_read_rate = {}
        self._disk_write_rate = {}

        self._net_rx_rate = {}
        self._net_rx_packets_sec = {}
        self._net_rx_err_sec = {}
        self._net_rx_drop_sec = {}

        self._net_tx_rate = {}
        self._net_tx_packets_sec = {}
        self._net_tx_err_sec = {}
        self._net_tx_drop_sec = {}

        self._dictionary_metrics = [Metrics.cpu_usage,
                                    Metrics.disk_read_rate,
                                    Metrics.disk_write_rate,
                                    Metrics.net_rx_rate,
                                    Metrics.net_rx_packets_sec,
                                    Metrics.net_rx_errors_sec,
                                    Metrics.net_rx_dropped_sec,
                                    Metrics.net_tx_rate,
                                    Metrics.net_tx_packets_sec,
                                    Metrics.net_tx_errors_sec,
                                    Metrics.net_tx_dropped_sec]

    def _add_interval(self, metric, value, key=None):
        if key:
            # It's a dictionary, so retrieve the key
            dictionary_entry = metric.get(key, self._default_values.copy())
            self._add_value(dictionary_entry, value)
            metric[key] = dictionary_entry
        else:
            self._add_value(metric, value)

    # noinspection PyMethodMayBeStatic
    def _add_value(self, dictionary, value):
        if dictionary[Stats.min] is None or value < dictionary[Stats.min]:
            dictionary[Stats.min] = value

        if dictionary[Stats.max] is None or value > dictionary[Stats.max]:
            dictionary[Stats.max] = value

        dictionary[Stats.count] += 1
        dictionary[Stats.total] += value

    def _get_dictionary_value(self, metric, key):
        dictionary = getattr(self, metric)
        values = {}

        for instance in dictionary:
            if key == Stats.average:
                if dictionary[instance][Stats.count] > 0:
                    values[instance] = dictionary[instance][Stats.total] / dictionary[instance][Stats.count]
                else:
                    values[instance] = 0
            else:
                values[instance] = dictionary[instance][key]

        return values

    def get_count(self, metric):
        if metric in self._dictionary_metrics:
            return self._get_dictionary_value(metric, Stats.count)
        else:
            return getattr(self, metric)[Stats.count]

    def get_min(self, metric):
        if metric in self._dictionary_metrics:
            return self._get_dictionary_value(metric, Stats.min)
        else:
            return getattr(self, metric)[Stats.min]

    def get_max(self, metric):
        if metric in self._dictionary_metrics:
            return self._get_dictionary_value(metric, Stats.max)
        else:
            return getattr(self, metric)[Stats.max]

    def get_total(self, metric):
        if metric in self._dictionary_metrics:
            return self._get_dictionary_value(metric, Stats.total)
        else:
            return getattr(self, metric)[Stats.total]

    def get_average(self, metric):
        if metric in self._dictionary_metrics:
            return self._get_dictionary_value(metric, Stats.average)
        else:
            value = getattr(self, metric)
            if value[Stats.count] > 0:
                return value[Stats.total] / value[Stats.count]
            else:
                return 0

    def get_highest_cpu_process(self):
        # Issue a top command to get the process using the most CPU
        line = self._execute('top -b -n 1 | head -n 8 | tail -n 1').strip()
        cpu_usage = line.split[8]
        process = line.split[11]
        return process, cpu_usage

    def _log_sar_intervals(self, sar_data):
        intervals = sar_data.split('Average:', 1)[0]
        # Dump to file
        with open('srmt_sar.out', 'a') as sar_out:
            sar_out.write(intervals)

    def _parse_sar_data(self, sar_data):
        if 'Average:' in sar_data:
            average_data = 'Average:' + sar_data.split('Average:', 1)[1]
            average_data_lines = average_data.splitlines()

            # Reset the flags
            in_cpu = False
            in_proc = False
            in_mem = False
            in_swap = False
            in_disks = False
            in_net = False
            in_net_errors = False

            for line in average_data_lines:
                if line == '':
                    # Reset the flags
                    in_cpu = False
                    in_proc = False
                    in_mem = False
                    in_swap = False
                    in_disks = False
                    in_net = False
                    in_net_errors = False
                else:
                    data = line.split()[1:]

                    if in_cpu:
                        if len(data) >= 10:
                            cpu_name = data[0]
                            cpu_usage = 100 - integer(data[10])

                            self._add_interval(self._cpu_usage, cpu_usage, cpu_name)

                    if in_proc:
                        self._add_interval(self._proc_sec, integer(data[0]))

                    if in_mem:
                        if len(data) >= 2:
                            self._add_interval(self._mem_free, integer(data[0]))
                            self._add_interval(self._mem_used, integer(data[1]))

                    if in_swap:
                        if len(data) >= 2:
                            self._add_interval(self._swap_free, integer(data[0]))
                            self._add_interval(self._swap_used, integer(data[1]))

                    if in_disks:
                        if len(data) >= 4:
                            disk_name = data[0]
                            read_rate = float(data[2])
                            write_rate = float(data[3])

                            self._add_interval(self._disk_read_rate, read_rate, disk_name)
                            self._add_interval(self._disk_write_rate, write_rate, disk_name)

                    if in_net:
                        if len(data) >= 5:
                            interface = data[0]
                            rx_packets_sec = integer(data[1])
                            tx_packets_sec = integer(data[2])
                            rx_rate = integer(data[3])
                            tx_rate = integer(data[4])

                            self._add_interval(self._net_rx_packets_sec, rx_packets_sec, interface)
                            self._add_interval(self._net_tx_packets_sec, tx_packets_sec, interface)
                            self._add_interval(self._net_rx_rate, rx_rate, interface)
                            self._add_interval(self._net_tx_rate, tx_rate, interface)

                    if in_net_errors:
                        if len(data) >= 5:
                            interface = data[0]
                            rx_err_sec = integer(data[1])
                            tx_err_sec = integer(data[2])
                            rx_drop_sec = integer(data[3])
                            tx_drop_sec = integer(data[4])

                            self._add_interval(self._net_rx_err_sec, rx_err_sec, interface)
                            self._add_interval(self._net_tx_err_sec, tx_err_sec, interface)
                            self._add_interval(self._net_rx_drop_sec, rx_drop_sec, interface)
                            self._add_interval(self._net_tx_drop_sec, tx_drop_sec, interface)

                    if '%usr' in line:
                        in_cpu = True

                    if 'proc/s' in line:
                        in_proc = True

                    if 'kbmemfree' in line:
                        in_mem = True

                    if 'kbswpfree' in line:
                        in_swap = True

                    if 'DEV' in line:
                        in_disks = True

                    if 'rxpck/s' in line:
                        in_net = True

                    if 'rxerr/s' in line:
                        in_net_errors = True

    @CommonTestLibrary.background_thread()
    def _collect_sar_data(self, collection_interval=10):
        while not self._stop_collecting:
            # Execute a sar command
            sar_command = 'sar {} {} 1'.format(self._command_parameters['sar'],
                                               collection_interval)

            sar_results = self._execute(sar_command)
            self._parse_sar_data(sar_results[1])

    def _execute(self, command):
        if self._ssh is None:
            return CommonTestLibrary.run_command_locally(command)
        else:
            return self._ssh.execute(command)

    def start(self, collection_interval=10):
        if not self._is_running:
            self._stop_collecting = False
            self._is_running = True
            self._collect_sar_data(collection_interval)

    def stop(self):
        self._stop_collecting = True
        self._is_running = False

    def is_dictionary_metric(self, metric):
        return metric in self._dictionary_metrics
