""" File to hold the Metrics class

"""
class Stats(object):
    """ A set of attributes to define which stats are collected for each metric

        count: The number of times the metric is collected
        min: The minimum value ever reached for a metric
        max: The maximum value ever reached for a metric
        total: The total amount collected for a metric since test start
        average: The running average for a metric since test start

        cpu_usage_process: When checking CPU usage, the threshold is not considered to be crossed if this process name
                           is using the most CPU (listed as the first entry in top).
    """
    count = 'count'
    min = 'min'
    max = 'max'
    total = 'total'
    average = 'average'

    allowed_processes = 'allowed_processes'

class Metrics(object):
    """ A set of attributes to define the various metrics that can be monitored

        CPU Metrics:
        cpu_usage - The % of load the CPU is under (100 - idle %)

        Process Metrics:
        processes_sec - The number of proceses created per second

        Memory Metrics:
        memory_free - The amount of free RAM
        memory_used - The amount of RAM being used
        swap_free - The amount of free swap space
        swap_used - The amount of swap being used

        Disk metrics:
        disk_read_rate - The rate of data being read from the disk
        disk_write_rate - The rate of data being written to the disk

        Network Metrics:
        net_rx_rate - The rate of data being received by the NIC
        net_rx_packets_sec - The number of packets being received by the NIC
        net_rx_errors_sec - The number of errors per second being received by the NIC
        net_rx_dropped_sec - The number of packets per second being dropped by the receiving end of the NIC

        net_tx_rate - The rate of data being transmitted by the NIC
        net_tx_packets_sec - The number of packets being transmitted by the NIC
        net_tx_errors_sec - The number of errors per second being transmitted by the NIC
        net_tx_dropped_sec - The number of packets per second being dropped by the transmitting end of the NIC

    """
    # The string corresponds to a private variable name. This allows the get function to return the value based off
    # the metric requested without a bunch of if statements.

    cpu_usage = '_cpu_usage'

    processes_sec = '_proc_sec'

    memory_free = '_mem_free'
    memory_used = '_mem_used'

    swap_free = '_swap_free'
    swap_used = '_swap_used'

    disk_read_rate = '_disk_read_rate'
    disk_write_rate = '_disk_write_rate'

    net_rx_rate = '_net_rx_rate'
    net_rx_packets_sec = '_net_rx_packets_sec'
    net_rx_errors_sec = '_net_rx_err_sec'
    net_rx_dropped_sec = '_net_rx_drop_sec'

    net_tx_rate = '_net_tx_rate'
    net_tx_packets_sec = '_net_tx_packets_sec'
    net_tx_errors_sec = '_net_tx_err_sec'
    net_tx_dropped_sec = '_net_tx_drop_sec'

