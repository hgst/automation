import time

from testCaseAPI.src.testclient import TestClient
from global_libraries.performance import Stopwatch

class MyClass(TestClient):
    def run(self):
        self.log.info('From test case')
        my_sw = Stopwatch()
        my_timer = Stopwatch(timer=15)

        my_sw.start()
        my_timer.start()

        time.sleep(5)
        print my_sw.get_elapsed_time()
        print my_timer.get_elapsed_time()

        my_timer.stop()

        time.sleep(5)
        print my_sw.get_elapsed_time()
        print my_timer.get_elapsed_time()

        my_timer.start()

        while True:
            print my_timer.is_timer_reached()
            if my_timer.is_timer_reached():
                break
            print 'In loop'
            print my_sw.get_elapsed_time()
            print my_timer.get_elapsed_time()
            time.sleep(1)

        print 'Exception coming:'
        print my_sw.is_timer_reached()

MyClass()










