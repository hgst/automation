from testCaseAPI.src.testclient import TestClient
import global_libraries.CommonTestLibrary as ctl
import logging

class MyClass(TestClient):
    def run(self):
        self.log.info('Info')
        self.log.debug('Debug - should not show')
        self.log.debug('You should see this debug')
        self.log.error('Error')
        self.log.critical('Critical')
        self.log.debug('You should see this debug')
        self.log.info('info2')

MyClass()
