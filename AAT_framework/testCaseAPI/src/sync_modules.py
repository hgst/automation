""" When run, this syncs the versions of the installed modules with those on the Execution Servers, downgrading or
    upgrading as necessary.
"""
import sys
import pip
from cStringIO import StringIO

from module_info import EXEC_MODULES
from global_libraries.wd_logging import create_logger


# A list of modules where the pip install name is different than the installed module name
MODULE_NAMES = {'robot': 'robotframework',
                'Selenium2Library': 'robotframework-selenium2library',
                'Crypto': 'pycrypto',
                'pil': 'Pillow'
                }


def sync():
    logger = create_logger('AAT.sync_modules')

    installed_modules = []
    failed_installs = []

    for module_name in EXEC_MODULES:
        do_install = False
        version = ''
        expected_version = EXEC_MODULES[module_name]
        # Attempt to import it
        imported = None
        try:
            imported = __import__(module_name)
        except ImportError:
            # Do nothing
            pass

        if imported:
            try:
                version = imported.__version__
            except AttributeError:
                pass

            if expected_version != version:
                do_install = True
        else:
            do_install = True

        if module_name.endswith('_version'):
            do_install = False

        if do_install:
            if module_name in MODULE_NAMES:
                pip_name = MODULE_NAMES[module_name]
            else:
                pip_name = module_name

            logger.info('Installing version ' + str(expected_version + ' of ' + module_name))
            module_info = pip_name + '==' + str(expected_version)

            # Temporarily re-direct stdout to a variable
            old_out = sys.stdout
            result = StringIO()
            sys.stdout = result

            pip.main(['install', '-I', module_info])

            sys.stdout = old_out

            result_text = result.getvalue()

            pip_command = 'pip install -I ' + module_info
            if 'Successfully installed ' + pip_name in result_text:
                logger.info('Installed ' + module_name)
                installed_modules.append(pip_command)
            else:
                logger.error('Error installing ' + module_name)
                logger.debug(result_text)
                failed_installs.append(pip_command)

                if 'Unable to find vcvarsall.bat' in result_text:
                    logger.info('Unable to find vcvarsall.bat. '
                                  'Please install Visual C++ 2008 Express Edition from '
                                  'http://go.microsoft.com/?linkid=7729279')

                if '_AES.pyd' in result_text:
                    logger.info('Cannot overwrite Crypto files. Please manually install without running Python.')

                if 'is intended for UNIX-like operating systems' in result_text:
                    logger.info('This module is not intended for Windows.')

    logger.info('Successfully installed ' + str(len(installed_modules)) + ' modules')
    for install_command in installed_modules:
        logger.info(install_command)

    logger.info('Failed to install ' + str(len(failed_installs)) + ' modules')
    for install_command in failed_installs:
        logger.info(install_command)


if __name__ == "__main__":
    sync()