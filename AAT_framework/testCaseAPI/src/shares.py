from restAPI.src.restclient import RestClient

from global_libraries import utilities

# global variables
TEST_SHARE_NAME = utilities.TEST_SHARE_NAME

class Shares(object):
    def __init__(self, uut):
        self.uut = uut
        self._rest = RestClient(self.uut)

    # # @brief Deletes a users share access
    #  @param share_name: The share name of the share to be adjusted.
    #                     Default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #
    def delete_share_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        try:
            result = self._rest.delete_share_access(share_name, username)
            return self._get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to delete share access\n' + str(ex))

    # # @brief Returns a users share access
    #  @param share_name: The share name of the share to be adjusted.
    #                     Default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <share_access_list>
    #        <share_name>test</share_name>
    #        <share_access>
    #            <username>est</username>
    #            <user_id>est</user_id>
    #           <access>RO</access>
    #        </share_access>
    #        <share_access>
    #            <username>@group0</username>
    #            <user_id>@group0</user_id>
    #            <access>RW</access>
    #        </share_access>
    #    </share_access_list>
    #    \endverbatim
    #
    def get_share_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        try:
            result = self._rest.get_share_access(share_name, username)
            return self._get_result_code(result), result
        except Exception, ex:
            logger1.info('Failed to retrieve share access\n' + str(ex))

    # # @brief Creates share access for a user
    #  @param share_name: The share name of the share to be adjusted, default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #  @param access:     The level of access to be granted, default is 'RW'
    #  @return:
    #    XML Response Example
    #    \verbatim
    #    <share_access>
    #        <status>success</status>
    #    </share_access>
    #    \endverbatim
    #
    def create_share_access(self, share_name=TEST_SHARE_NAME, username='admin', access='RW'):
        try:
            result = self._rest.create_share_access(share_name, username, access)
            return self._get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to create share access\n' + str(ex))

    # # @brief Updates share access
    #  @param share_name: The share name of the share to be adjusted, default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #  @param access:     The level of access to be granted, default is 'RW'
    #  @return:
    #    XML Response Example
    #    \verbatim
    #    <share_access>
    #        <status>success</status>
    #    </share_access>
    #    \endverbatim
    #
    def update_share_access(self, share_name=TEST_SHARE_NAME, username='admin', access='RW'):
        try:
            result = self._rest.update_share_access(share_name, username, access)
            return self._get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to update share access\n' + str(ex))
