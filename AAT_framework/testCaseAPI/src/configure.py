import os
import sys
import argparse

from inventoryServerAPI.InventoryServerApi import RestAPI as inventory_rest_api
from inventoryServerAPI.InventoryServerApi import InventoryException
from global_libraries.testdevice import Fields
from global_libraries.testdevice import DeviceDefaults
from global_libraries.product_info import ProductList
from global_libraries.product_info import Attributes
from global_libraries import CommonTestLibrary as ctl
import global_libraries.infrastructures as infra
from global_libraries.wd_environment import EnvironmentVariables

AAT_ENV = 'AAT_ENV'

class _InitializationFailedError(Exception):
    pass

class _DeviceNotFoundError(Exception):
    pass

""" @brief Class used to configure the test device used by a particular test
"""


class Device(object):
    """ Device class

        Pass device_file to override the file used to get information when
        not running a Jenkins job.

        To access device information, use the Device.UUT{} dictionary combined
        with the Fields class.
    """
    def __init__(self, device_file=None, ip_address=None, do_configure=True):
        self.LOCAL_INV_SERVER = 'http://localhost/InventoryServer'
        if device_file is None:
            device_file = 'test_device.txt'

        self.settings = {}
        self.logger = ctl.getLogger('AAT.configure')

        server_url = self._get_inv_server_url()
        inventory_client = inventory_rest_api(server_url)
        self.device_api = inventory_client.DeviceAPI(inventory_client)
        self.product_api = inventory_client.ProductAPI(inventory_client)

        self._device_file = device_file

        if do_configure:
            self._configure_uut(ip_address)

        location_infrastructure = infra.environment[os.environ[AAT_ENV]]
        for key in location_infrastructure:
            self.settings[key] = location_infrastructure[key]

    def _get_inv_server_url(self):
        try:
            location = os.environ[AAT_ENV]
        except KeyError:
            if ctl.is_running_on_silk():
                # This should never happen if it's running on Silk, so raise an exception
                message = ('No location set. Set the AAT_ENV to match one of the following locations:')
                self.logger.error(message)
                self.list_locations()
                raise InventoryException('')
            else:
                self.logger.info('No location set. Assuming local.')
                location = 'Local'
                os.environ[AAT_ENV] = location
        else:
            self.logger.info('Location is ' + location)
        finally:
            self.list_environment_variables(debug=True)

        if location not in infra.environment:
            self.logger.error('Invalid location of "{}"'.format(location))
            self.list_locations()
            raise InventoryException()

        # Get the URL From testenvironment
        url = infra.environment[location][infra.INV_SERVER]
        if url.lower() in ('none', 'n/a', 'na', ''):
            return self.LOCAL_INV_SERVER
        else:
            return ctl.clean_url(url)

    def list_locations(self):
        self.logger.info('Valid locations are:')
        for location in infra.environment:
            self.logger.info(location)

    def list_environment_variables(self, debug=False):
        if debug:
            self.logger.debug('Current environment variables:')
        else:
            self.logger.info('Current environment variables:')

        for key in os.environ.keys():
            message = "%30s %s " % (key, os.environ[key])
            if debug:
                self.logger.debug(message)
            else:
                self.logger.info(message)


    def _configure_uut(self, ip_address=None):
        self._reset_uut_values(ip_address=ip_address)

        # Convert any integer values from string to integer
        integer_fields = {Fields.device_id,
                          Fields.internal_ssh_port,
                          Fields.power_switch_port,
                          Fields.serial_server_port,
                          Fields.ssh_gateway_port}

        if ip_address is not None:
            self.settings[Fields.internal_ip_address] = ip_address

        for field in integer_fields:
            if self.settings[field] is not None:
                self.settings[field] = int(float(self.settings[field]))

        self._set_product_values()

    def _set_product_values(self):
        # Set the product-specific values, such as the number of drives
        product = self.settings[Fields.product]

        product_attributes = ctl.get_class_attributes(Attributes)

        for attribute in product_attributes:
            self._set_value(attribute[0], product)
        # self._set_value(Fields.number_of_drives, product)
        # self._set_value(Fields.supports_link_speed, product)

        # Set the expected performance numbers
        self._set_expected_performance()

    def _set_value(self, field_name, product):
        # Sets the value for field_name for the given product
        values = getattr(Attributes, field_name)
        if product in values:
            value = values[product]
        else:
            value = values.get(ProductList.DEFAULT, 'N/A')

        self.settings[field_name] = value
        setattr(Fields, field_name, field_name)

    def _reset_uut_values(self, ip_address=None):
        # Clear the UUT dictionary and set defaults
        self.settings.clear()

        # Read from Silk or the test_device.txt file
        self._get_uut_config(ip_address=ip_address)


    def _get_uut_config(self, ip_address=None):
        # Get the test unit's config from either the inventory database or
        # from a local config file, but not both.
        #
        # It then loads the default value for anything that wasn't passed.
        #
        # Then, it checks the command line to see if anything was passed.
        # If so, it overrides what was found in the inventory server or
        # local config file.
        #
        # So, if a value is set in mutliple locations, the flow is:
        # 1) Default values
        # 2) Silk environment variables, or if they're not set, test_device.txt
        # 3) Command line options
        # 4) The IP address argument, if passed to this method

        self.logger.debug('Getting device and test configuration')

        # First, fill in all fields with None so the UUT dictionary is complete
        field_list = ctl.get_class_attributes(Fields)

        for fieldname in field_list:
            if fieldname[1] not in self.settings:
                self.settings[fieldname[1]] = None


        self._get_info_from_silk(ip_address)
        # Always try to get from file. Will fill in anything not specified by Silk server, but will not override
        self._get_info_from_file(ip_address)

        if ip_address:
            self.settings[Fields.internal_ip_address] = ip_address

        product = self.settings[Fields.product]

        if product in DeviceDefaults.device_list:
            defaults = DeviceDefaults.device_list[product]
        else:
            defaults = DeviceDefaults.default

        for field in defaults:
            if self.settings[field] is None:
                self.settings[field] = defaults[field]

        # Re-read the command line
        self._parse_command_line()

        # Check the execution plan name
        if not self.settings.get(Fields.execution_plan, None):
            self.settings[Fields.execution_plan] = 'Local Test'

        # Check the device name
        if not self.settings.get(Fields.product, None):
            self.settings[Fields.product] = ''

        # Output the testname and product to the console (to get in the main Silk log)
        self.logger.info('    Test Name: {}'.format(ctl.get_silk_test_name()))
        product_name = '    Product: '
        if self.settings[Fields.product]:
            product_name += self.settings[Fields.product]
        else:
            product_name+= 'N/A'

        self.logger.info(product_name)

        # And the UUT MAC and IP address
        self.logger.info('    UUT MAC Address: {}'.format(self.settings[Fields.mac_address]))
        self.logger.info('    UUT IP Address: {}'.format(self.settings[Fields.internal_ip_address]))

        # Set the default_* values based off the starting values
        self.settings[Fields.default_web_password] = self.settings[Fields.web_password]
        self.settings[Fields.default_ssh_password] = self.settings[Fields.ssh_password]
        self.settings[Fields.default_serial_password] = self.settings[Fields.serial_password]
        self.settings[Fields.default_ad_password] = self.settings[Fields.ad_password]

        # Confirm that the IP address has been set. Cannot test without it.
        if self.settings[Fields.internal_ip_address] is None:
            raise _DeviceNotFoundError('No IP address specified for UUT.')

    def _parse_command_line(self):
        # First, configure the parser. It will look for every field in the Fields class
        self.logger.debug('Parsing command line:')
        self.logger.debug(sys.argv)
        field_list = ctl.get_class_attributes(Fields)

        # Build a list of boolean items
        boolean_fields = [Fields.debug_mode, Fields.suppress_exceptions]

        for item in field_list:
            if item[0].startswith('is_'):
                boolean_fields.append(item[1])

        # Create the parser
        cmd_line_parser = argparse.ArgumentParser()

        # Fill in the arguments by adding every field
        for field_name in field_list:
            if field_name[1] in boolean_fields:
                help_text = None

                if field_name[1] == Fields.debug_mode:
                    help_text = 'Enable debugging to the console'

                if field_name[1] == Fields.suppress_exceptions:
                    help_text = 'Suppress exceptions at the API level'

                cmd_line_parser.add_argument('--' + field_name[1], action='store_true', help=help_text)
            else:
                cmd_line_parser.add_argument('--' + field_name[1], dest=field_name[1])

        args = cmd_line_parser.parse_args()

        # Iterate through the values dictionary and transfer any non-None values to settings{}
        for field_name in field_list:
            value = getattr(args, field_name[1])
            if field_name[1] in boolean_fields:
                # Booleans that are not set are False
                if value is None:
                    value = False

            if value is not None:
                self.settings[field_name[1]] = value

    def _get_info_from_silk(self, ip_address=None):
        self.list_environment_variables()
        environment_variables = EnvironmentVariables.get_variables()

        try:
            # See if it's running on Silk
            self.settings[Fields.execution_plan] = environment_variables['sctm_execdef_name']
        except KeyError, ex:
            # Not under Silk, so return false
            self.logger.info('Not running on Silk')
            return False

        self.logger.debug('All SCTM environment variables:')
        for key in environment_variables:
            if key.startswith('sctm'):
                self.logger.debug(key + " = " + str(environment_variables[key]))

        if self._get_inv_server_url() is not self.LOCAL_INV_SERVER:
            version = environment_variables['sctm_version']
            build = environment_variables['sctm_build']
            silk_build = self.settings[Fields.execution_plan] + "-" + version + "." + build
            self.logger.info("Running on Silk. Checkout owner is " + str(silk_build))
            self.logger.info('Execution plan is ' + self.settings[Fields.execution_plan])


            checkout_device = {}
            checkout_device = self.device_api.getDeviceByJob(silk_build)
            self.logger.debug('_get_info_from_silk: checkout_device = ' + str(checkout_device))

            device_id = checkout_device.get('id')
            if device_id is None:
                raise _InitializationFailedError('No device ID could be found for build ' + str(silk_build))

            serial_info = self.device_api.getSerialServer(device_id)
            checkout_device[Fields.serial_server] = serial_info.get('ipAddress')
            checkout_device[Fields.serial_server_port] = serial_info.get('port')

            powerswitch_info = self.device_api.getPowerSwitch(device_id)
            checkout_device[Fields.power_switch] = powerswitch_info.get('ipAddress')
            checkout_device[Fields.power_switch_port] = powerswitch_info.get('port')

            product_id = checkout_device.get('product').get('id')
            product_info = self.product_api.get(product_id)
            product_name = product_info.get('product').get('name')

            # Only for BVT project used
            if product_name.endswith('-BVT'):
                product_name = product_name.split('-')[0]

            checkout_device[Fields.product] = product_name

            # Fill in the rest of the UUT dictionary
            for key, value in checkout_device.iteritems():
                if self.settings.get(key) is None:
                    self.settings[key] = value

            if ip_address:
                self.settings[Fields.internal_ip_address] = ip_address
            return True
        else:
            self.logger.info('Running from Silk, but no inventory server for environment. Using test_device.txt.')

    def _get_info_from_file(self, ip_address=None):
        # Pull the information from the device information text file.
        if os.path.isfile(self._device_file):
            dict = eval(open(self._device_file).read())

            for key in dict:
                if self.settings.get(key) is None:
                    self.settings[key] = dict[key]

            self.logger.debug('UUT = {0}'.format(self.settings))

            return True
        else:
            self.logger.warn('{0} not found in {1}'.format(self._device_file,
                                                           os.getcwd()))
            return False

    def _set_expected_performance(self):
        # Build up the expected_write_speed and expected_read_speed dictionaries
        self._set_performance(Fields.expected_read_speed, Attributes.expected_read_speed[ProductList.DEFAULT])
        self._set_performance(Fields.expected_write_speed, Attributes.expected_write_speed[ProductList.DEFAULT])

    def _set_performance(self, field, defaults):
        # 11 = JBOD, 12 = Spanning
        raid_modes = (0, 1, 5, 10, 11, 12)
        expected_speed = self.settings[field]
        for raid_mode in raid_modes:
            if raid_mode not in expected_speed:
                expected_speed[raid_mode] = defaults[raid_mode]

        self.settings[field] = expected_speed
