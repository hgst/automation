"""
Functions dealing with manipulating user accounts
"""

from restAPI.src.restclient import RestClient
from sshAPI.src.sshclient import SshClient

from global_libraries.testdevice import Fields
import global_libraries.CommonTestLibrary as commonTestLibrary
from configure import Device


class Users(Device):
    """
    A set of functions dealing with user accounts
    """

    def __init__(self, uut=None):
        super(Users, self).__init__()
        self.uut = uut

    def printuut(self):
        """ Simple test to print the UUT information. Used for debugging. """
        print self.uut

    def set_admin_password(self, newpassword):
        """ Sets the admin password

        It is up to the calling function to track the previous password to set it back.
        Alternatively, a call to reset_admin_password will remove the password when done.

        :param newpassword: The new admin password
        :return: 0 = success. Non-zero = failure code
        """

        # Wrapper for update_user()
        old_password = self.uut[Fields.web_password]
        result = self.update_user(user='admin',
                                  old_password=old_password,
                                  new_password=newpassword,
                                  fullname=None,
                                  is_admin=True)

        if result == 0:
            self.uut[Fields.web_password] = newpassword
            return 0
        else:
            return 1

    # # @brief Updates a user with new information
    #  @details  A single user can be identified either by the user_id or by the
    #            username. Only after it is found that there is no user with a user_id
    #            that matches, then the user will attempt to be found based on a
    #            matching username. Such a request to update a single user requires one
    #            of the two parameters to be specified. To change a user's password both
    #            old_password and new_password are required; except for an Admin user,
    #            who only needs to supply the new_password parameter.
    #            When testing the API, the password must be base 64 encoded.
    #            Refer to the Create User section for restrictions.
    #            Either the user_id or the username must be used to identify the user to
    #            be updated. Neither of these values can be modified.
    #
    #            NOTE: Do not use this to update the admin password. Doing so
    #                  will make it impossible for the Web UI and REST calls
    #                  to be accessed. If you need to change the admin
    #                  password, use set_admin_password() instead.
    #
    #  @param user:         name of the user to be created
    #  @param old_password: old password of the given user, default is 'None'
    #  @param new_password: new password of the given user, default is 'None'
    #  @param fullname:     the fullname of the user, default is 'None'
    #  @param is_admin:     whether or not the user is an admin, default is 'None'
    #  @return:
    #  XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #      <username>myUserName<\username>
    #      <user_id>2</user_id>
    #    </users>
    #    \endverbatim
    #
    def update_user(self, user, old_password=None, new_password=None, fullname=None, is_admin=None):
        """

        :param user:
        :param old_password:
        :param new_password:
        :param fullname:
        :param is_admin:
        :return:
        """
        try:
            result = self._rest.update_user(user, old_password, new_password, fullname, is_admin)
            return commonTestLibrary.get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to update user\n' + str(ex))

    # # @brief Resets admin account to default using SSH
    #
    # Cannot use REST because we don't know the current admin password
    def reset_admin_password(self):
        """


        :return:
        """
        try:
            # Set the password to empty
            result, output = self.execute("passwd -d admin")
        except Exception, ex:
            logger1.info('Failed while deleting  admin password\n' + str(ex))
            return 1
        else:
            return result

    # # @brief Returns all of the users
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <users>
    #         <user_id>2</user_id>
    #         <username>guest</username>
    #         <fullname>Guest User</fullname>
    #         <is_admin>false</is_admin>
    #         <is_password>false</is_password>
    #     </users>
    #    \endverbatim
    #

    def get_all_users(self):
        """


        :return:
        """
        try:
            result = self._rest.get_all_users()
            return commonTestLibrary.get_result_code(result), result
        except Exception, ex:
            logger1.info('Failed to retrieve list of users\n' + str(ex))

    # # @brief Gets the user account information
    #  @param username: name of the user you want information about
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <users>
    #         <user_id>2</user_id>
    #         <username>guest</username>
    #         <fullname>Guest User</fullname>
    #         <is_admin>false</is_admin>
    #         <is_password>false</is_password>
    #     </users>
    #    \endverbatim
    #
    def get_user(self, username):
        """

        :param username:
        :return:
        """
        try:
            result = self._rest.get_user(username)
            return commonTestLibrary.get_result_code(result), result
        except Exception, ex:
            logger1.info('Failed to retrieve user account information\n' + str(ex))
    # # @brief Deletes a user
    #  @details A single user can be identified either by the user_id or by the
    #           username. Only after it is found that there is no user with a user_id
    #           that matches, then the user will attempt to be found based on a
    #           matching username. Such a request to delete a single user requires
    #           one of the two parameters to be specified. There is no facility to
    #           delete all users in one API call. Either the user_id or username
    #           must be used to identify the user to be deleted.
    #  @param user: the user to be deleted, can be a user_id or a user name
    #  @return:
    #  XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #    </users>
    #    \endverbatim
    #

    def delete_user(self, user):
        try:
            result = self._rest.delete_user(user)
            return commonTestLibrary.get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to delete user\n' + str(ex))

    # # @brief Deletes all users except those that are excluded
    #  @param exclude: A list of users to be excluded from deletion, defaults to admin
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #    </users>
    #    \endverbatim
    #
    def delete_all_users(self, exclude=None):
        try:
            result = self._rest.delete_all_users(exclude)
            return commonTestLibrary.get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to delete all users except those on exclude list\n' + str(ex))

    # # @brief Creates a user
    #  @details A username must be an alphanumeric value between 1 and 32 characters
    #           and cannot contain spaces; but is allowed to start with a letter or a
    #           number, and contain the underscore character.
    #           The length of a password can be between 0 and 255 characters; and can
    #           contain any ASCII character.
    #  @param username: name of the user to be created
    #  @param password: password of the user to be created, default is 'None'
    #  @param fullname: The fullname of the user, default is 'none'
    #  @param is_admin: Whether the new user is an admin, default is 'False'
    #  @return:
    #  XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #      <username>myUserName<\username>
    #      <user_id>2</user_id>
    #    </users>
    #    \endverbatim
    #
    def create_user(self, username, password=None, fullname=None, is_admin=False):
        try:
            result = self._rest.create_user(username, password, fullname, is_admin)
            return commonTestLibrary.get_result_code(result)
        except Exception, ex:
            logger1.info('Failed to create user\n' + str(ex))

