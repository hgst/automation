""" Work with file shares

@author: Troy Hoffman
(C) 2015 Western Digital Corporation

NOTE: This uses sudo to mount shares. To allow sudo to run these commands without a password, please do the following:

1) Go to a command prompt
2) Execute sudo visudo

Make these changes:
1)	Comment out the line Defaults   requiretty
2)	Add the following to the end of the file:
    ALL  ALL=(ALL) NOPASSWD: /usr/sbin/mount.cifs, /usr/bin/umount, usr/sbin/mount.nfs


"""
import os

from global_libraries import CommonTestLibrary
from global_libraries import wd_exceptions as exceptions

# Supported operating systems. Value is the return string from os.name.
WINDOWS = 'nt'
LINUX = 'posix'

# Constants for each supported protocol.
# The values are dictionaries listing the methods used for each OS.
PROT_SAMBA = 'Samba'
PROT_NFS = 'NFS'
PROT_AFP = 'AFP'


class Share(object):
    """ Methods used to work with file shares.

        Class properties:
            mountpoint = The mountpoint used for the share. This is set
                         when the share is mounted and cleared when it is
                         unmounted. As a result, if the share is mounted,
                         unmounted, and remounted, the mountpoint may change.
    """

    # Dictionary listing which operating systems are supported in each protocol
    _supported_os = {PROT_SAMBA: [WINDOWS, LINUX],
                     PROT_NFS: [LINUX],
                     PROT_AFP: [LINUX]}

    # The saved mountpoint
    _mountpoint = None

    # The protocol.
    _protocol = None

    log = CommonTestLibrary.getLogger('AAT.tc_shares')

    samba = PROT_SAMBA
    nfs = PROT_NFS
    afp = PROT_AFP

    @property
    def mountpoint(self):
        """

        :return: The current mountpoint for the share
        """
        if not self._ismounted:
            self.mount()

        return self._mountpoint + os.path.sep

    def __init__(self,
                 protocol,
                 hostname,
                 sharename,
                 user=None,
                 password=None,
                 read_only_ok=False):
        """ Keyword arguments:
                protocol = The protocol to use. To prevent typos, use one of
                           the class constants beginning
                           with PROT_, such as PROT_SAMBA
                hostname = The host name or IP address of the device
                           hosting the Samba share
                sharename = The name of the share
                user = The user name needed for authentication.
                       Default is 'guest'.
                password = The password needed for authentication.
                           Default is None.

        """
        self._ismounted = False
        self.read_only_ok = read_only_ok

        # Set the user for guest access
        if user is None:
            if protocol == PROT_AFP:
                user = 'admin'
            else:
                user = 'guest'

        if protocol in self._supported_os:
            os_list = self._supported_os[protocol]

            operating_system = os.name

            if operating_system in os_list:
                # The operating system is supported
                self._user = user
                self._password = password
                self._protocol = protocol
                self._build_sharename(hostname, sharename)
            else:
                # Not supported
                self.log.debug(protocol + ' shares not supported in ' + operating_system)
                raise exceptions.UnsupportedProtocol()
        else:
            # Unsupported share protocol
            self.log.debug(protocol + ' shares not supported')
            raise exceptions.UnsupportedProtocol()

    def mount(self):
        """ Mount the share.

            Note that this is not persistent, so a reboot will unmount the
            share. When called, the mountpoint property will be set.

        """
        if not self._ismounted:
            # Only try this if the share isn't already mounted
            self._set_mountpoint()
            if self._protocol == PROT_SAMBA:
                self._mountsamba()
            elif self._protocol == PROT_NFS:
                self._mountnfs()
            elif self._protocol == PROT_AFP:
                self._mountafp()

            # See if the mount is valid
            if os.name == LINUX:
                good_mount = os.path.ismount(self._mountpoint)
            else:
                # Just see if the mount is a directory
                good_mount = os.path.isdir(self._mountpoint)

            if not good_mount:
                raise exceptions.ShareNotMounted('{} failed to mount'.format(self._mountpoint))

            # Check for write access
            filehandle = None
            try:
                filehandle = open(os.path.join(self._mountpoint, 'wd_aat_test.tmp'), 'w')
                filehandle.write('AAT Test')
            except IOError:
                err_message = 'Mounted {} to {} using {}'.format(self._sharelocation, self._mountpoint, self._protocol)

                if not self.read_only_ok:
                    raise exceptions.ShareReadOnly(err_message)
                else:
                    self.log.debug(err_message)
            else:
                self.log.debug('Mounted {} with write access to {} using {}'.format(self._sharelocation,
                                                                                    self._mountpoint,
                                                                                    self._protocol))
            finally:
                if filehandle:
                    filehandle.close()

        return self._mountpoint

    def unmount(self, force_unmount=False):
        """ Unmounts the share

        :param force_unmount: If True, the share doesn't have to have been mounted by tc_shares.
        :return:
        """
        if not self._ismounted and not force_unmount:
            # Called without mounting, so throw an exception
            self.log.debug('Unmount called without mounting first.')
            raise exceptions.ShareNotMounted()

        # Build the unmount command
        if os.name == WINDOWS:
            # Unmount the share
            umount_cmd = 'net'
            umount_args = 'use ' + self._mountpoint + ' /delete /yes'
        elif os.name == LINUX:
            umount_cmd = 'sudo umount'
            umount_args = '-l -f ' + self._mountpoint
        else:
            self.log.debug('Operating system unsupported:' + os.name)
            raise exceptions.UnsupportedOperatingSystem()

        # Execute the command
        CommonTestLibrary.run_command_locally(umount_cmd + ' ' + umount_args, shell=True)

        if LINUX == os.name:
            os.rmdir(self._mountpoint)

        # Clear the mountpoint
        self._mountpoint = None
        self._ismounted = False

    def _set_mountpoint(self):
        # Finds a free mountpoint and sets the mountpoint attribute
        if self._mountpoint:
            return self._mountpoint

        if os.name == WINDOWS:
            # Returns the next available drive letter (starting at Z:)
            drives = CommonTestLibrary.run_command_locally('wmic logicaldisk get description,name', shell=True)[1]

            if self._mountpoint:
                # See if the current mountpoint is still available
                if self._mountpoint not in drives:
                    return

            # No existing mountpoint, or the existing mountpoint is invalid, so get a new mountpoint
            for drive in reversed(range(65, 91)):
                driveletter = chr(drive) + ':'
                if not (driveletter in drives):
                    self._mountpoint = driveletter
                    return

            # No drives, so throw an exception
            self.log.info('No available drives')
            raise exceptions.ShareNotMounted()
        elif os.name == LINUX:
            # Returns a mount point.
            mountpoint = None

            if self._mountpoint:
                if not os.path.isdir(self._mountpoint):
                    # It's good, so use it
                    mountpoint = self._mountpoint

            number = 0
            while not mountpoint:
                mp = './mnt/wdaat' + str(number)
                if (not os.path.exists(mp) and
                        not os.path.ismount(mp) and
                        not os.path.isdir(mp)):
                    mountpoint = mp

                number += 1
                if number > 255:
                    self.log.info('Unable to find a mountpoint')
                    raise exceptions.ShareNotMounted()

            os.makedirs(mountpoint)
            self._mountpoint = mountpoint

    def _build_sharename(self, hostname, sharename):
        # Build share location
        if os.name == WINDOWS:
            # Windows - build the share location
            if self._protocol == self.samba:
                self._sharelocation = '\\\\' + hostname + '\\' + sharename
        elif os.name == LINUX:
            # Linux
            if self._protocol == self.nfs:
                self._sharelocation = hostname + ':/nfs/' + sharename
            elif self._protocol == self.samba:
                self._sharelocation = '//' + hostname + '/' + sharename
            elif self._protocol == self.afp:
                self._sharelocation = hostname + '/' + sharename
            else:
                raise exceptions.UnsupportedProtocol('Protocol of {} not implemented'.format(self._protocol))

    def _mountnfs(self):
        mount_cmd = 'sudo mount.nfs -o nolock {} {}'.format(self._sharelocation, self._mountpoint)
        # Run the mount command
        self.log.debug('Mounting NFS share {} to {}'.format(self._sharelocation, self._mountpoint))
        CommonTestLibrary.run_command_locally(mount_cmd, shell=True)
        self._ismounted = True

    def _mountafp(self):
        # mount_afp afp://admin@192.168.6.109/Public mnt/afp
        authentication_string = self._user
        if self._password is not None:
            authentication_string += ':{}'.format(self._password)

        # afp_mount needs an absolute path to prevent the mount point from being somewhere unexpected
        absolute_path = os.path.abspath(self._mountpoint)

        mount_cmd = 'mount_afp "afp://{}@{}" "{}"'.format(authentication_string,
                                                          self._sharelocation,
                                                          absolute_path)
        # Run the mount command
        self.log.debug('Mounting AFP share {} to {}'.format(self._sharelocation, self._mountpoint))
        CommonTestLibrary.run_command_locally(mount_cmd, shell=True)
        self._ismounted = True

    def _mountsamba(self):
        mountpoint = self._mountpoint
        sharelocation = self._sharelocation
        user = self._user
        password = self._password

        if os.name == WINDOWS:
            # Build the mount command
            mount_cmd = 'net'
            mount_args = 'use ' + mountpoint + ' ' + sharelocation

            if password:
                mount_args += ' ' + password

            if user is not None and user not in ('' or 'guest'):
                mount_args += ' /USER:' + user
        elif os.name == LINUX:
            if user is None:
                authentication = 'guest'
            else:
                if password is None:
                    password = ''

                authentication = 'username=' + user + ',password=' + password

            mount_cmd = 'sudo mount.cifs'
            mount_args = (sharelocation +
                          ' ' +
                          mountpoint +
                          ' -o ' +
                          authentication +
                          ',' +
                          'rw,nounix,file_mode=0777,dir_mode=0777')
        else:
            self.log.debug('Operating system unsupported:' + os.name)
            raise exceptions. UnsupportedOperatingSystem()

        # Run the mount command
        self.log.debug('Mounting {} '.format(sharelocation))
        CommonTestLibrary.run_command_locally(mount_cmd + ' ' + mount_args, shell=True)
        self._ismounted = True
