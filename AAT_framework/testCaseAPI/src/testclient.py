# # @package TestClient
# Created on July 14, 2014
# @brief This module contains all functions that are callable by a test case
# @author: vasquez_c, tran_jas, hoffman_t

# Module notes:
# PIP           This is part of the Python installation, but you'll need to add c:\Python27\Scripts to your path
#
# Pycrypto      Under Windows, getting this to install through PIP is tricky. So, rather than fight with dependencies
#               such as having the exact version of the compiler, having the paths right, etc., it is easier to
#               download the binary installer from http://www.voidspace.org.uk/python/modules.shtml#pycrypto.
#               Note that most of the time, you'll be using 32-bit Python, even under 64-bit Windows.
#
#               Once installed, you need to go to C:\Python27\Lib\site-packages and rename the "crypto" directory
#               so it starts with an upper-case C.
#
# NfSpy         Does not work under Windows

import os
import time
import sys
from xml.etree import ElementTree
import urllib2
import cStringIO
import string
import glob
import logging
import ctypes
import subprocess
from PIL import Image
from PIL import ImageDraw
from random import randint
from socket import error as SocketError

from configure import Device
# noinspection PyPep8Naming
from global_libraries import CommonTestLibrary as ctl
from global_libraries import wd_localfilesystem as wdlfs
from global_libraries import wd_formatting
from global_libraries.constants import RebootType

# import other APIs
from restAPI.src.restclient import RestClient
from restAPI.src.restclient import RestError
from sshAPI.src.sshclient import SshClient
from cgiAPI.src.cgiclient import CgiClient
from seleniumAPI.src.seleniumclient import SeleniumClient
from serialAPI.src.serialclient import SerialClient
from powerSwitchAPI.src.powerswitchclient import PowerSwitchClient
from networkSharesAPI.src.networksharesclient import NetworkSharesClient
from global_libraries import utilities
from global_libraries.testdevice import Fields
from global_libraries.product_info import Attributes
from seleniumAPI.src.ui_map import Elements
from seleniumAPI.src.seleniumclient import ElementNotReady
from seleniumAPI.src.strings import Strings
from global_libraries.wd_networking import is_web_server_up
from global_libraries.wd_networking import download_web_file
import global_libraries.wd_exceptions as exception
from global_libraries.performance import Stopwatch
from module_info import EXEC_MODULES
from mediaServerAPI.src.mediaserverapi import MediaServer

from system_health.src.health_monitor import HealthMonitor

from global_libraries.wd_environment import EnvironmentVariables

# global variables
TEST_SHARE_NAME = utilities.TEST_SHARE_NAME
TEST_SHARE_DESC = utilities.TEST_SHARE_DESC
REST_CALL_TIMEOUT = 90

# The dictionary of module versions from the exec server.
# TODO: Update with actual values

# ===================================================================================
# Class TESTClient
# ===================================================================================
# # @class TESTClient()
# class TESTClient is used by the test case to perform all functionality.
# Test cases should remain generic and not need to worry about authentication,
# IP addresses, etc.
#
# Method return code: 0 means success. A non-zero return code is an error code and means failures
# Standard method return values (check the function documentation for exceptions):
#
# getters: Begins with "get_" and returns a tuple. The first element is the return code. The second is the return value
# boolean: Begins with "is_" and returns True or False
# Setters/Action: Returns just the return code


class TestClient(object):
    """
    TestClient
    """
    # # @brief Constructor
    # @param checkDevice: If true (default), confirm the device is reachable
    # @param ip_address: Override the IP address from test_device.txt or the inventory server
    # @param selenium_timeout: Override the 30 second timeout on web UI commands
    #
    # Check device does the following. The test client functions that are called are listed so you can set
    # checkDevice=False, but still do some checking/setup.
    #
    # * Confirm the web UI can be reached - port_test()
    # * Gets the unit's firmware version and stores it in the UUT dictionary - get_firmware_version_number()
    # * Confirms the unit's firmware version matches the version listed on the inventory server - check_fw_version()
    # * Enable SSH - set_ssh(enable=True)
    # * Reset the admin password - reset_admin_password() <- This will throw an exception if SSH is disabled, so be sure
    # to wrap it in an exception handler

    # Import tc_shares. Must be in here so the methods will be accessible through this class.
    # Inspection thinks this is unused, but it's used
    from tc_shares import Share

    # Logger for the test client
    _tc_log = ctl.getLogger('AAT.testclient')
    # Loger for the test case
    log = ctl.getLogger('AAT.{}'.format(ctl.get_silk_test_name()))

    ExceptionHandler = ctl.HandleExceptions(log)

    def run(self):
        """ Placeholder for test case run to allow cases without a run() function to run without error """
        # Set self.run_executed to False so test.end() won't be called. If this function is overridden, then
        # it will run test.end()
        self.run_executed = False

    def run_aat(self):
        """ Same as run, but for AAT-only tests """
        self.run_aat_executed = False

    def run_art(self):
        """ Same as run, but for ART-only tests """
        self.run_art_executed = False

    def tc_cleanup(self):
        """ Placeholder to allow test cases without tc_cleanup to run without error """
        pass

    def disconnect_user_sessions(self, username=None, timeout=None):
        """ Disconnect all sessions for the specified user. If not specified, disconect all sessions.

        :param username: The user to disconnect. If not specified, disconnects all users
        """

        if username:
            self.execute('wto -d {}'.format(username), timeout=timeout)
        else:
            self.execute('wto -a', timeout=timeout)

    # ##
    # noinspection PyPep8Naming
    def __init__(self,
                 checkDevice=True,
                 ip_address=None,
                 selenium_timeout=30,
                 device_file=None,
                 running_health_monitor=False,
                 health_monitor_interval=10,
                 health_monitor_thresholds=None,
                 execute_test=True,
                 **kwargs):
        self.sw = Stopwatch()
        self.sw.start()
        # Dump a list of all installed modules
        self._tc_log.info('==================== Starting test {} ===================='.format(ctl.get_silk_test_name()))
        self._check_loaded_modules()
        self.skip_cleanup = False
        self.environ = EnvironmentVariables()

        # Create a test device
        device = Device(ip_address=ip_address, device_file=device_file)
        self.uut = device.settings
        if self.uut[Fields.uut_browser] is None:
            # Default to Firefox browser
            self.uut[Fields.uut_browser] = 'firefox'

        self.output_test_settings()

        # Build the list of fields
        all_fields = ctl.get_class_attributes(Fields)
        field_list = []

        for field in all_fields:
            field_list.append(field[1])

        # Parse out any arguments passed to the constructor
        for arg in kwargs:
            if arg not in field_list:
                self.log.error('Invalid field passed to test client constructor: {}'.format(arg))
                exit(1)
            else:
                self.uut[arg] = kwargs[arg]

        if self.uut[Fields.debug_mode]:
            ctl.set_console_log_level(logging.DEBUG)

        # Create objects for the APIs
        self._rest = RestClient(self.uut)
        self._ssh = SshClient(self.uut)
        self._spc = SerialClient(self.uut)
        self._nsc = NetworkSharesClient(self.uut)
        self._psc = PowerSwitchClient(self.uut)
        self._cgi = CgiClient(self.uut)
        self._ms = MediaServer(self.uut)
        self.run_executed = True
        self.run_aat_executed = True
        self.run_art_executed = True
        self._sel = SeleniumClient(self.uut, wait_time=selenium_timeout)
        self.picture_counts = 0

        # self._tc_log.exception.selenium_client = self._sel
        self._tc_log.critical.selenium_client = self._sel
        self._tc_log.error.selenium_client = self._sel
        self._tc_log.warning.selenium_client = self._sel
        self.log.critical.selenium_client = self._sel
        self.log.error.selenium_client = self._sel
        self.log.warning.selenium_client = self._sel
        self.log.exception.selenium_client = self._sel

        self.create_file_installed_uut = False
        self.create_file_installed_locally = False

        self.Elements = Elements
        self.Fields = Fields

        self.uut[Fields.actual_fw_version] = None

        self._files_copied = []
        self._files_copied_network = []
        self._paths_created = []
        self._paths_created_network = []
        self._mountpoints = {}
        self._users = {}
        self._admin_password = self.uut[self.Fields.default_web_password]
        self._result = 1  # Default to failure
        self.tests = {}  # Dictionary of tests and results
        self._test_list = []  # List of tests run. Needed so the results will be in the proper order
        self.TEST_STARTED = 'in progress'
        self.TEST_PASSED = 'passed'
        self.TEST_WARNED = 'passed with warning'
        self.TEST_FAILED = 'FAILED'

        self.running_health_monitor = running_health_monitor
        # Merge any passed health monitor thresholds
        if health_monitor_thresholds:
            self.uut[Fields.health_thresholds].update(health_monitor_thresholds)

        self._health_monitor = HealthMonitor(uut=self.uut)

        # Set up and check the device
        if checkDevice:
            # Confirm the device is reachable
            if is_web_server_up(self.uut[Fields.internal_ip_address]):
                result = self.port_test()
                if result > 0:
                    self._tc_log.info('Device API not reachable. Cannot run test.')
                    raise exception.DeviceUnreachableError()
            else:
                self._tc_log.info('Device web server is not running. Cannot run test.')
                raise exception.DeviceUnreachableError()

            # Get the actual firmware version
            firmware_version = self.get_firmware_version_number()
            self.log.info('Device is running firmware version: {}'.format(firmware_version))

            # Confirm the version is correct
            self.check_fw_version()

            # Enable SSH
            self.set_ssh(enable=True)

            # Make sure Language is English
            self.log.info('Language Check')
            status_code, current_language = self.get_language_configuration()
            self.log.info('Current Language is: {0}'.format(current_language))
            if 'en_US' not in current_language:
                self.log.info('Update Language to English')
                self.update_language_configuration()
                status_code, current_language = self.get_language_configuration()
                self.log.info('Current Language is: {0}'.format(current_language))

            # skip these tests if Sequoia
            if self.uut[Fields.product] == 'Sequoia':
                pass
            else:
                # Check the number of drives
                actual_drive_count = self.get_drive_count()

                if actual_drive_count != self.uut[self.Fields.number_of_drives]:
                    self.log.warning('Drive count mismatch. ' +
                                     '{} physical drives in the unit, '.format(actual_drive_count) +
                                     'but there are {} bays'.format(self.uut[self.Fields.number_of_drives]))

                # Store the physical drive count
                self.uut[Fields.number_of_physical_drives] = actual_drive_count

                # Get the current RAID level
                raid_level = self.get_raid_level()
                self.uut[Fields.configured_raid_level] = raid_level

                # Reset the admin password
                self.reset_admin_password(ignore_errors=True)

                # Start health monitoring
                if self.running_health_monitor:
                    self._health_monitor.start(monitor_interval=health_monitor_interval)

            # Handle password change requirement for Sequoia
            if self.uut[Fields.product] == 'Sequoia':
                self.uut[Fields.ssh_username] = 'root'
                self._spc.update_root_password_seq()

            # Disconnect all logged-in users
            self.disconnect_user_sessions()

            # Confirm that the test is running on the correct product
            self._check_product_name()

        else:
            # Not checking device, so assume the following values
            self.uut[Fields.configured_raid_level] = -1  # Unknown RAID
            self.uut[Fields.number_of_physical_drives] = self.uut[Fields.number_of_drives]  # All drives installed

        self.default_values = self.get_ui_default_values()

        # Start timing the run
        self.sw.lap()
        elapsed_seconds = self.sw.get_lap_times()[0]
        self.log.info('Setup completed in {}'.format(wd_formatting.seconds_to_string(elapsed_seconds)))

        if execute_test:
            self._execute_test()

    def _execute_test(self):
        # Start the tests
        run_sw = Stopwatch()

        try:
            self.log.info('Running any common tests')
            run_sw.start()
            self.run()
            elapsed_seconds = run_sw.get_elapsed_time()
            self.log.info('Common tests completed in {}'.format(wd_formatting.seconds_to_string(elapsed_seconds)))

            if self._is_aat():
                self.log.info('Running any AAT tests')
                run_sw.reset(keep_running=True)
                self.run_aat()
                elapsed_seconds = run_sw.get_elapsed_time()
                self.log.info('AAT tests completed in {}'.format(wd_formatting.seconds_to_string(elapsed_seconds)))

            if self._is_art():
                run_sw.reset(keep_running=True)
                self.log.info('Running any ART tests')
                self.run_art()
                elapsed_seconds = run_sw.get_elapsed_time()
                self.log.info('ART tests completed in {}'.format(wd_formatting.seconds_to_string(elapsed_seconds)))

        except SystemExit:
            pass
        except Exception:
            self._tc_log.exception('Test failed with an exception')
            raise
        finally:
            if self.running_health_monitor:
                try:
                    self._health_monitor.stop()
                except AttributeError:
                    self._tc_log.warning('No health monitor to stop')
                else:
                    crossed_thresholds = self._health_monitor.get_thresholds_crossed()
                    if len(crossed_thresholds) > 0:
                        # Thresholds crossed. Fail the test.
                        self._tc_log.error('The following health thresholds were crossed: {}'.format(crossed_thresholds))

            # Get the result before cleanup, just in case there are any exceptions during cleanup
            self.sw.lap()
            self.log.info('Tests took {}'.format(wd_formatting.seconds_to_string(self.sw.get_lap_times()[1])))
            self._result = self.get_test_results()
            # Always run tc_cleanup since each test case has to explicitly define this function
            self.tc_cleanup()

            if not self.skip_cleanup:
                self._cleanup()
            self.sw.lap()
            self.log.info('Cleanup took {}'.format(wd_formatting.seconds_to_string(self.sw.get_lap_times()[2])))
            self.log.info('Total time was {}'.format(wd_formatting.seconds_to_string(self.sw.get_elapsed_time())))
            if self.run_executed or self.run_aat_executed or self.run_art_executed:
                self.end()

    def _check_product_name(self):
        expected_product = self.uut[Fields.product]
        model_number = self.get_model_number()

        actual_product = None
        for next_product in Attributes.model_number:
            if Attributes.model_number[next_product] == model_number:
                actual_product = next_product
                break

        if not actual_product:
            self.log.warning('Model number {} is unknown'.format(model_number))
            actual_product = 'Unknown'

        # Check for no product. If so, fill it in with whatever is running
        if '' == expected_product:
            # Fill in the UUT
            self.uut[Fields.product] = actual_product
        else:
            # The product was passed to the test, so make sure we're running on the correct device
            if actual_product != expected_product:
                error_message = 'Test specified {}, but the test device is actually {}'.format(expected_product,
                                                                                               actual_product)
                raise exception.ProductMismatch(error_message)

    def _is_art(self):
        # Return True if the test execution plan name ends with -ART
        execution_plan = self.uut[Fields.execution_plan]

        if execution_plan.endswith('-ART'):
            return True
        else:
            return False

    def _is_aat(self):
        # Return True if the ART environment variable is NOT set
        return not self._is_art()

    def _compare_modules(self, loaded_modules):
        mismatches = 0
        local_keys = set(loaded_modules.keys())
        exec_keys = set(EXEC_MODULES.keys())

        common_modules = local_keys.intersection(exec_keys)
        extra_modules = local_keys - exec_keys
        original_missing_modules = exec_keys - local_keys
        missing_modules = set()

        # Check the missing modules to see if they can be imported manually
        for module in original_missing_modules:
            try:
                imported = __import__(module)
            except ImportError:
                missing_modules.add(module)
                imported = None

            if imported:
                try:
                    version = imported.__version__
                except AttributeError:
                    missing_modules.add(module)
                else:
                    # It is actually on the system, just not imported yet, so switch which set it is in
                    common_modules.add(module)
                    loaded_modules[module] = version

        for module in common_modules:
            local_version = loaded_modules[module]
            exec_version = EXEC_MODULES[module]

            if local_version != exec_version:
                mismatches += 1
                if local_version < exec_version:
                    age = 'older'
                    fail_point = 'locally'
                    pass_point = 'on the execution server'
                else:
                    age = 'newer'
                    fail_point = 'on the execution server'
                    pass_point = 'locally'

                self._tc_log.warning('Module {} is {} than the version on the execution server.'.format(module, age))
                self._tc_log.debug('This may cause tests to fail {}, but pass {}'.format(fail_point, pass_point))
                self._tc_log.debug('Local version is       {}'.format(local_version))
                self._tc_log.debug('Exec server version is {}'.format(exec_version))

        for module in extra_modules:
            mismatches += 1
            self._tc_log.warning('Module {} on the local system is not on the execution server.'.format(module))
            self._tc_log.debug('This may cause tests to pass locally, but fail on the execution server.')
            self._tc_log.debug('Version is {}'.format(loaded_modules[module]))

        for module in missing_modules:
            mismatches += 1
            self._tc_log.warning('Module {} on the execution server is not on the local server'.format(module))
            self._tc_log.debug('This may cause tests to pass on the execution server, but fail locally')
            self._tc_log.debug('Version is {}'.format(EXEC_MODULES[module]))

        if mismatches > 0:
            self._tc_log.info('*** Mismatched Python modules detected ***')
        else:
            self._tc_log.info('All loaded Python modules match the execution server')

    def _check_loaded_modules(self):
        module_dictionary = {}
        for module_name in sys.modules.keys():
            module = sys.modules[module_name]
            try:
                version = module.__version__
            except AttributeError:
                # No version attribute, so skip
                pass
            else:
                module_dictionary[module_name] = str(version)

        # Convert to a string that looks like a dictionary so it can be copied and pasted and a string
        # that looks like a bunch of import statements
        import_string = os.linesep + 'Versioned modules that were imported:'
        module_string = os.linesep + 'Installed versioned Python packages:' + os.linesep + '{'
        for module in sorted(module_dictionary.keys()):
            import_string += os.linesep + 'import ' + module
            module_string += ("u'" + str(module) + "': '" + str(module_dictionary[module]) + "'," +
                              os.linesep +
                              "                ")
        module_string = module_string[:-18]
        module_string += os.linesep + '                }' + os.linesep

        self._tc_log.debug(import_string + os.linesep)
        self._tc_log.debug(module_string)

        # Uncomment once this code is reviewed. This will cause it to throw warnings when modules do not match what
        # is on the execution server.
        self._compare_modules(module_dictionary)

    # noinspection PyBroadException
    def _run_cleanup(self, function, *args, **kwargs):
        """ Wrapper used for cleanup functions. Handles exceptions, logging, and benchmarking.

        :param function:
        :param args:
        :param kwargs:
        :return:
        """
        sw = Stopwatch()
        sw.start()
        self._tc_log.debug('Running cleanup: {}'.format(function.__name__))
        self._tc_log.debug('                 args   = {}'.format(args))
        self._tc_log.debug('                 kwargs = {}'.format(kwargs))
        try:
            function(*args, **kwargs)
        except:
            self._tc_log.warning(ctl.exception('Cleanup: {} failed'.format(function.__name__)))
        else:
            self._tc_log.debug('                 {} finished in {} seconds'.format(function.__name__,
                                                                                  sw.get_elapsed_time()))
        sw.stop()

    # noinspection PyBroadException,PyBroadException,PyBroadException
    def _cleanup_delete_copied_files(self):
        file_paths = []

        if self._files_copied:
            for filename in self._files_copied:
                self._tc_log.debug('Deleting file {}'.format(filename))
                try:
                    wdlfs.delete_file(filename)
                except:
                    self._tc_log.warning(ctl.exception('Could not delete {}'.format(filename)))
                else:
                    file_paths.append(os.path.dirname(os.path.abspath(filename)))

        if self._files_copied_network:
            for share, filepath in self._files_copied_network:
                mountpoint = self.mount_share(share)
                dest_file = os.path.join(mountpoint, filepath)

                self._tc_log.debug('Deleting file {} on share {}'.format(filepath, share))
                try:
                    wdlfs.delete_file(dest_file)
                except:
                    self._tc_log.warning(ctl.exception('Could not delete {}'.format(dest_file)))
                else:
                    file_paths.append(os.path.dirname(os.path.abspath(dest_file)))

        if self._paths_created:
            for path in set(self._paths_created):
                self._tc_log.debug('Deleting path {}'.format(path))
                wdlfs.delete_path(path, delete_contents=True)

        if self._paths_created_network:
            for share, path in set(self._paths_created_network):
                mountpoint = self.mount_share(share)
                dest_path = os.path.join(mountpoint, path)
                self._tc_log.debug('Deleting path {}'.format(dest_path))
                wdlfs.delete_path(dest_path, delete_contents=True)

    # noinspection PyBroadException,PyBroadException
    def _cleanup_delete_temp_files(self):
        # Clean up all temp files and directories
        test_base = self.get_test_client_directory()

        try:
            wdlfs.delete_file(os.path.join(test_base, 'tmp*'))
        except:
            pass

        try:
            wdlfs.delete_path(os.path.join(test_base, 'tmp*'), delete_contents=True)
        except:
            pass

    # noinspection PyBroadException,PyBroadException,PyBroadException
    def _cleanup_reconnect_drives(self):
        if self.uut[self.Fields.product] == 'Sequoia':
            self.log.debug('Skip reconnect drives for Sequoia')
        else:
            # Checked disks is connect or not, if not, try to insert it.
            if self.get_drive_count() == self.uut[self.Fields.number_of_drives]:
                pass
            else:
                drive_mapping = self.get_drive_mappings()
                for x in range(1, self.uut[self.Fields.number_of_drives]+1):
                    if x not in drive_mapping:
                        self._tc_log.info('Drive {} is disconnected, try to insert it'.format(x))
                        self.insert_drives(x)
                if self.get_drive_count() == self.uut[self.Fields.number_of_drives]:
                    self._tc_log.debug('Clean up for drives connection checked succeed')
                else:
                    self._tc_log.warning('Clean up for drives connection checked failed')

    def _cleanup_close_firefox_browsers(self):
        try:
            self._tc_log.debug('Close all Firefox browsers')
            result = subprocess.check_output('ps -e | grep firefox', shell=True)
            pid = result.split()[0]
            error_code = subprocess.call('kill {}'.format(pid), shell=True)
            if error_code == 0:
                self._tc_log.debug('All Firefox browsers are closed')
            else:
                self._tc_log.warning('Failed to close Firefox browsers! Error code: {}'.format(error_code))
        except subprocess.CalledProcessError:
            self._tc_log.debug('No Firefox browsers exist')
        except Exception as e:
            self._tc_log.warning('Failed to check Firefox status! Exception: {}'.format(repr(e)))

    def get_ui_default_values(self):
        # TODO: Determine the currently-selected language of the UI and use that
        s = Strings('English')
        return s.default_values

    def start_test(self, testname=None, message=None):
        """ Start a test.

        :param testname: If present, use the specified string as the unique identifier for this test. If not present,
                         generate a unique ID.
        :return: The testname
        """
        if testname is None:
            # Generate a unique testname
            test_number = 1
            while True:
                if str(test_number) in self.tests:
                    test_number += 1
                else:
                    testname = str(test_number)
                    break

        if message is None:
            message = testname

        self.log.info('*** Test started: {} ***'.format(message))
        if testname in self.tests:
            if self.tests[testname] == self.TEST_STARTED:
                self.log.warning('Duplicate test {} added. Setting to warning.'.format(testname))
                self.tests[testname] = self.TEST_WARNED
            else:
                self.log.warning('Duplicate test {} added. '.format(testname) +
                                 'Leaving status as {}'.format(self.tests[testname]))
        else:
            self.tests[testname] = self.TEST_STARTED
            self._test_list.append(testname)

        return testname

    def _find_running_test(self):
        # Return the test that is currently running. Throw an exception if the number of tests at self.TEST_STARTED
        # is not exactly one.
        started_tests = []
        for test in self.tests:
            if self.tests[test] == self.TEST_STARTED:
                started_tests.append(test)

        if len(started_tests) == 0:
            raise exception.NoRunningTests('No tests are currently running. Cannot log test result.')

        if len(started_tests) > 1:
            raise exception.MultipleRunningTests('Must pass testname when mutliple tests are runnning. ' +
                                                 'Cannot determine test for result.')

        return started_tests[0]

    def pass_test(self, testname=None, result=None):
        """ Passes the specified test

        :param testname: If present, the test to pass. If not, it will pass the test that is currently
                         in the state self.TEST_STARTED.
        :param result: Optional string to include in the log

        NOTE: If more than one test is in the state of self.TEST_STARTED, then testname MUST be passed. Otherwise,
              an exception will be thrown, and the entire test case will fail. This is because it is unclear which
              test is being passed, so the overall test results will be ambiguous.
        """
        if result is None:
            # Only one parameter was passed, so first see if the test exists
            if testname not in self.tests:
                # Assume it's a result, not a testname
                result = testname
                testname = None

        if testname is None:
            testname = self._find_running_test()

        if testname in self.tests:
            if self.tests[testname] == self.TEST_STARTED:
                self.tests[testname] = self.TEST_PASSED
                self.log.info('*** Test: {} - passed ***'.format(testname))
            else:
                self.log.warning('*** Test: {} - marked as passed, but status is {} ***'.format(testname,
                                                                                              self.tests[testname]))
        else:
            self.log.warning('Test: {} - marked as passed, but not started. Passing with a warning. ***'.format(testname))
            self.tests[testname] = self.TEST_WARNED
            self._test_list.append(testname)

        if result:
            self.log.info('*** Test result: {} ***'.format(result))

    def fail_test(self, testname=None, error=None, filename=None):
        """ Fail the test

        :param testname: The test to fail. If None, fail the last started test
        :param error: An error message. If None, use the test name
        :param filename: An optional file to store in the Silk results

        """
        if error is None:
            # Only one parameter was passed, so first see if the test exists
            if testname not in self.tests:
                # Assume it's an error, not a testname
                error = testname
                testname = None

        if testname is None:
            testname = self._find_running_test()

        if testname in self.tests:
            if self.tests[testname] == self.TEST_STARTED:
                self.tests[testname] = self.TEST_FAILED
                self.log.error('*** Test: {} - FAILED: {}***'.format(testname, error))
            else:
                self.log.error('Test: {} - marked as failed, but status is already {} '.format(testname,
                                                                                             self.tests[testname]) +
                               ' - Marking as failed: {} ***'.format(error))
                self.tests[testname] = self.TEST_FAILED
        else:
            self.log.error('*** Test: {} - marked as failed, but it was never started. '.format(testname) +
                           'Marking as failed: {} ***'.format(error))
            self.tests[testname] = self.TEST_FAILED
            self._test_list.append(testname)

        if filename is not None:
            self.log.info('{} stored as part of this error'.format(filename))
            self.store_file_for_silk_result(filename)

    def warn_test(self, testname=None, warning=None):
        """ Pass the test with a warning

        :param testname: The test to pass. If None, pass the last started test
        :param warning: A warning message. If None, use the test name
        """
        if warning is None:
            # Only one parameter was passed, so first see if the test exists
            if testname not in self.tests:
                # Assume it's a warning, not a testname
                warning = testname
                testname = None

        if testname is None:
            testname = self._find_running_test()

        if testname in self.tests:
            if self.tests[testname] == self.TEST_STARTED:
                self.tests[testname] = self.TEST_WARNED
                self.log.warning('*** Test: {} - passed with WARNING: {} ***'.format(testname, warning))
            else:
                if self.tests[testname] == self.TEST_FAILED:
                    self.log.warning('Test: {} - marked as passed with warning, '.format(testname) +
                                     'but status is already {}'.format(self.tests[testname]) +
                                     ' - Leaving status. ***')
                else:
                    self.log.warning('Test: {} - marked as passed with warning, '.format(testname) +
                                     ' but status is already {}'.format(self.tests[testname]) +
                                     ' - Marking as passed with warning: ***'.format(warning))
                self.tests[testname] = self.TEST_WARNED
        else:
            self.log.warning('Test: {} - marked as passed with warning, but it was never started. '.format(testname) +
                             'Marking as warning: {} ***'.format(warning))
            self.tests[testname] = self.TEST_WARNED
            self._test_list.append(testname)

    def checkbox_should_be_selected(self, element):
        self._sel.checkbox_should_be_selected(element)

    def checkbox_should_not_be_selected(self, element):
        self._sel.checkbox_should_not_be_selected(element)

    def checkbox_should_be_disabled(self, element):
        self._sel.checkbox_should_be_disabled(element)

    def is_checkbox_selected(self, element):
        try:
            self._sel.checkbox_should_be_selected(element)
        except ElementNotReady:
            raise
        except:
            return False
        else:
            return True

    def get_dropdown_list_options(self, element, attribute='textContent'):
        """ Get a dictionary of all options in a drop down list

        :param locator: The locator to use. Add an asterisk to use a wildcard
        :param attribute: The attribute to use for the text. Default is textContent.

        :return: A dictionary of options, with the key being the 'rel' attribute (position in list)
        """
        return self._sel.get_dropdown_list_options(element, attribute)

    def set_selenium_timeout(self, timeout):
        self._sel.set_timeout(timeout)

    def validate_element(self, element, cancel_button=None, expected_label=None, expected_string=None):
        # First, see if the element has a label
        self.log.debug('Validating element {}'.format(element.name))
        if not expected_label and element.label:
            # Get the expected label text
            expected_label = self.default_values[element.label]

        if expected_label:
            self.log.debug('Checking label of {} - should be "{}"'.format(element.name, expected_label))
            self.element_text_should_be(element.label, expected_label)
            self.log.debug('Text is correct')

        if element.is_hidden:
            self.element_should_not_be_visible(element)
        else:
            self.element_should_be_visible(element)

        if element.control_type == Elements.TYPE_CHECKBOX:
            # Verify if the checkbox is disabled by default
            if element.is_disabled:
                self.log.debug('Seeing if checkbox is disabled by default')
                self.checkbox_should_be_disabled(element)
                self.log.debug('Setting is correct')

            # For checkboxes, the default value will be 0 (unchecked) or 1 (checked)
            self.log.debug('Seeing if checkbox is checked')
            if self.default_values[element] == 0:
                # Confirm it's not checked
                if self.is_checkbox_selected(element):
                    raise exception.InvalidDefaultValue('Checkbox should not be checked')

                self.log.debug('Setting is correct')
                if not element.is_disabled:
                    self.log.debug('Checking checkbox')
                    self.select_checkbox(element)
                    self.checkbox_should_be_selected(element)
                    self.log.debug('Re-clearing checkbox')
                    self.unselect_checkbox(element)
                    self.checkbox_should_not_be_selected(element)
                    self.log.debug('Checkbox cleared successfully')
            elif self.default_values[element] == 1:
                # Confirm it's checked
                if not self.is_checkbox_selected(element):
                    raise exception.InvalidDefaultValue('Checkbox should be checked')
                if not element.is_disabled:
                    self.log.debug('Setting is correct')
                    self.log.debug('Clearing checkbox')
                    self.unselect_checkbox(element)
                    self.checkbox_should_not_be_selected(element)
                    self.log.debug('Re-checking checkbox')
                    self.select_checkbox(element)
                    self.checkbox_should_be_selected(element)
                    self.log.debug('Checkbox checked successfully')

        else:
            # Verify if the element is disabled by default
            if element.is_disabled:
                self.log.debug('Seeing if element is disabled by default')
                self.element_should_be_disabled(element)
                self.log.debug('Setting is correct')

            if not expected_string:
                expected_string = self.default_values[element]

            attribute_types = [Elements.TYPE_HREF, Elements.TYPE_DATAFLD,
                               Elements.TYPE_PLACEHOLDER, Elements.TYPE_TITLE]
            if element.control_type in attribute_types:
                self.log.debug('Checking element default attribute: {}'.format(element.control_type) +
                              ' - should be "{}"'.format(expected_string))
                self.check_attribute(element, expected_string, element.control_type)
            else:
                self.log.debug('Checking element default value - should be "%s"' % expected_string)
                if element.control_type in (Elements.TYPE_TEXTBOX, Elements.TYPE_PASSWORD):
                    self.textfield_value_should_be(element, expected_string)
                else:
                    self.element_text_should_be(element, expected_string)
            self.log.debug('Value is correct')

            if element.control_type == Elements.TYPE_TEXTBOX and cancel_button:
                # Change the existing text
                new_text = 'Changed Text'
                self.log.debug('Changing text to {}'.format(new_text))
                self.input_text_check(element, new_text, do_login=False)
                self.textfield_value_should_be(element, new_text)
                self.wait_until_element_is_visible(cancel_button)
                self.click_wait_and_check(cancel_button)
                # Verify that it changed back
                self.textfield_value_should_be(element, expected_string)
                self.log.debug('Text changed back successfully')

    def textfield_value_should_be(self, element, expected, message=''):
        self._sel.textfield_value_should_be(element, expected, message)

    def reset_current_directory(self):
        """ Changes the current directory back to the test client directory

        """
        test_base = self.get_test_client_directory()
        # Change back to the directory where the test client was launched
        os.chdir(test_base)

    @staticmethod
    def get_test_client_directory():
        """

        :return: The directory where the test client was launched
        """

        return os.path.dirname(os.path.abspath(sys.argv[0]))

    # noinspection PyBroadException
    def _cleanup(self):
        # This tc_cleanup is run for every test. Will do things like reset passwords
        # back to default, delete generated test files, etc.
        #
        # All functions should be wrapped in exception handlers to ensure everything is attempted.
        self.log.info('Running cleanup')

        self._run_cleanup(self._sel.close_all_browsers)
        self._run_cleanup(self.reset_current_directory)
        self._run_cleanup(self.disconnect_user_sessions)
        self._run_cleanup(self.set_ssh_password, self.uut[Fields.default_ssh_password], use_web_ui=False)
        self._run_cleanup(self._cleanup_delete_copied_files)
        self._run_cleanup(self._cleanup_delete_temp_files)
        self._run_cleanup(self._cleanup_reconnect_drives)
        self._run_cleanup(self._ms.reset_authentication)

        # Remove mounted shares
        self._run_cleanup(self._unmount_all)

        # Delete all mnt directories
        test_base = self.get_test_client_directory()
        self._run_cleanup(wdlfs.delete_file, os.path.join(test_base, 'mnt'))
        self._run_cleanup(wdlfs.delete_path, os.path.join(test_base, 'mnt'), delete_contents=True)

        # Reset the web password
        default_web_password = self.uut[Fields.default_web_password]
        if default_web_password != self.uut[Fields.web_password]:
            self._run_cleanup(self.set_admin_password, default_web_password)

        # Delete users
        for user in self._users:
            self._run_cleanup(self.delete_user, user)

        self._run_cleanup(self.delete_all_groups)

        # Reset the NIC
        self._run_cleanup(self.execute, 'setNetworkDhcp.sh ifname={0}'.format('bond0'), timeout=-1)
        self._run_cleanup(self.execute, 'setNetworkDhcp.sh ifname={0}'.format('egiga0'), timeout=-1)

        # Close all Firefox browsers that are opened by the other timeout cases
        if ctl.is_running_on_silk():
            self._run_cleanup(self._cleanup_close_firefox_browsers)

    def check_fw_version(self):
        # Read the expected FW version (from Inventory Server or test_device.txt)
        expected_fw_version = self.uut[Fields.expected_fw_version]

        # And the firmware version on the device
        actual_fw_version = self.uut[Fields.actual_fw_version]

        # Only check if the actual firmware version was found
        if actual_fw_version:
            # If the Firmware field on the Inventory Server is filled in and does not match, raise an exception
            if expected_fw_version:
                if str(expected_fw_version) != str(actual_fw_version):
                    raise exception.InvalidDeviceState('Firmware version mismatch. ' +
                                                       'Actual version is {}, '.format(actual_fw_version) +
                                                       'but it should be {}'.format(expected_fw_version))
                else:
                    self._tc_log.info('Device firmware matches expected firmware.')
            else:
                if ctl.is_running_on_silk():
                    self._tc_log.warning(
                        'Inventory server has no firmware information. ' +
                        'Cannot confirm device has correct firmware version.')
                else:
                    self._tc_log.info('Not running on Silk. Cannot confirm device has correct firmware version.')

    # noinspection PyBroadException
    def end(self):
        # Close all browser windows. Needed in case cleanup was skipped.
        self._run_cleanup(self._sel.close_all_browsers)
        exit(self._result)

    def warn_exception(self, message):
        # Reports an exception as a warning
        self._tc_log.warning(ctl.exception(message))

    # @ExceptionHandler
    def go_to_url(self, url='', use_ssl=False, port=None):
        """ Go to the specified URL in the browser

        :param url: The URL to go to
        :param use_ssl: If true, use HTTPS. If the the URL contains //, this is ignored
        :param port: The port to use. If the URL contains //, this is ignored
        """
        self._sel.go_to_url(url, use_ssl, port)

    def select_checkbox(self, element, timeout=None):
        self._sel.select_checkbox(element, timeout)

    def unselect_checkbox(self, element, timeout=None):
        self._sel.unselect_checkbox(element, timeout)

    # @ brief Convert a raw_string containing multiple lines of key/value pairs into a dictionary
    #
    # @ parameter
    @staticmethod
    def string_to_dictionary(raw_string, delimiter='='):
        dictionary = {}
        for line in cStringIO.StringIO(raw_string):
            line = string.strip(line)
            key, value = string.split(line, delimiter)
            dictionary[key] = value

        return dictionary

    # #@brief Get all the Twonky settings as a dictionary
    def get_twonky_config(self):
        ip_address = self.uut[Fields.internal_ip_address]
        code, page = self.get_page_contents_ex('http://{}:9000/rpc/get_all'.format(ip_address))

        if page:
            # Parse the raw settings
            return 0, self.string_to_dictionary(page)
        else:
            # Failed
            self._tc_log.warning('Failed to get Twonky settings')
            return 1, {}

    # #@brief Get the contents of the specified URL as a string
    # @parameter url: The URL to retrieve
    # @returns Result code which can be 0 as pass or the returned status code as failure
    # @returns full_page[0] is the page contents of FIRST PAGE ONLY
    # which is being returned as second member return parameter (FIRST PAGE CONTENT)
    # @returns full_page, will be a list of all the pages contents and is a LIST
    def get_page_contents(self, url, chunk=3450):
        response = urllib2.urlopen(url)
        page = None
        full_page = []
        while True:
            page = response.read(chunk)
            if not page:
                break
            full_page.append(page)
        # ---------------------------------------------------------- if page:
        resultcode = self._get_result_code(response.getcode(), page)
        assert isinstance(page, basestring), "page is not a string: %r" % page
        if full_page:
            first_page_content = full_page[0]
        else:
            first_page_content = ''
        return resultcode, first_page_content, full_page

    def get_page_contents_ex(self, url):
        response = urllib2.urlopen(url)
        page = ''
        while True:
            data = response.read()
            if data:
                page += data
            else:
                break
        assert isinstance(page, basestring), "page is not a string: %r" % page
        resultcode = self._get_result_code(response.getcode(), page)
        return resultcode, page

    def get_test_results(self):
        """ Get the final test results

            Log the number of errors and warnings, build the output.xml file, and return the final result
        :return: Number of errors (0 is pass, with or without warnings)
        """
        self._tc_log.info('')
        self._tc_log.info('*** Test case complete ***')

        test_count = len(self.tests)
        if test_count > 0:
            # Use self.tests for results
            passed = 0
            failed = 0
            warned = 0

            # Store the results in a list that will later be iterated. We don't want to output the results yet because
            # we want the total number of pass/failed/warned output first, so this is going to take two loops.
            test_results = []

            for test in self._test_list:
                if self.tests[test] == self.TEST_PASSED:
                    passed += 1
                elif self.tests[test] == self.TEST_WARNED:
                    warned += 1
                elif self.tests[test] == self.TEST_FAILED:
                    failed += 1
                else:
                    self._tc_log.error('Test: {} has no result. Assuming it failed.'.format(test))
                    failed += 1
                    self.tests[test] = self.TEST_FAILED

                test_results.append('   {}: {}'.format(test, self.tests[test]))

            if ctl.get_count(ctl.EXCEPTION) > 0:
                self._tc_log.info('*** Exceptions encountered. Test case FAILED ***')
                failed = 1 if failed == 0 else failed
            elif failed > 0:
                self._tc_log.info('*** Test case FAILED ***')
            elif warned > 0:
                self._tc_log.info('*** Test case passed with warnings ***')
            else:
                self._tc_log.info('*** Test case passed.')

            self._tc_log.info('')
            self._tc_log.info('*** Results summary ***')
            self._tc_log.info('    {} total tests'.format(test_count))
            self._tc_log.info('    {} failed'.format(failed))
            self._tc_log.info('    {} passed ({} with a warning)'.format(passed + warned, warned))
            self._tc_log.info('')
            self._tc_log.info('*** Individual test results ***')
            for result in test_results:
                self._tc_log.info('    {}'.format(result))

            self._build_outputxml(failed, warned, 0)
        else:
            # Exceptions count as exceptions, critical, and errors
            # Critical counts as critical and errors
            exceptions = ctl.get_count(ctl.EXCEPTION)
            critical = ctl.get_count(ctl.logging.CRITICAL)
            failed = ctl.get_count(ctl.logging.ERROR) + critical
            warnings = ctl.get_count(ctl.logging.WARNING)

            if failed > 0:
                warn_text = ' and {} warning{}'.format(warnings, 's' if warnings != 1 else '') if warnings > 0 else ''
                self._tc_log.info('Test case FAILED with {} error{}{}'.format(failed,
                                                                              's' if failed != 1 else '',
                                                                              warn_text))
            elif warnings > 0:
                self._tc_log.info('Test case passed with {} warning{}'.format(warnings, 's' if warnings != 1 else ''))
            else:
                self._tc_log.info('Test case passed')

            self._tc_log.info('Breakdown:')

            critical_text = 'critical: {}'.format(critical) if critical > 0 else ''
            exception_text = 'exceptions: {}'.format(exceptions) if exceptions > 0 else ''
            comma = ', ' if critical > 0 and exceptions > 0 else ''

            formatted_text = '({}{}{})'.format(critical_text, comma, exception_text)
            sub_error_text = formatted_text if critical + exceptions > 0 else ''

            self._tc_log.info('    Error messages:   {} {}'.format(failed, sub_error_text))
            self._tc_log.info('    Warning messages: {} '.format(warnings))
            self._build_outputxml(failed, warnings, exceptions, 1)

        exception_messages = ctl.get_messages(ctl.EXCEPTION)
        critical_messages = ctl.get_messages(ctl.logging.CRITICAL)
        error_messages = ctl.get_messages(ctl.logging.ERROR)
        warning_messages = ctl.get_messages(ctl.logging.WARNING)

        self._tc_log.info('')
        self._tc_log.info('*** Errors and Warnings Encountered ***')
        self._tc_log.info('    Exceptions:')
        for message in exception_messages:
            self._tc_log.info('              {}'.format(message))
        self._tc_log.info('    Critical:')
        for message in critical_messages:
            self._tc_log.info('              {}'.format(message))
        self._tc_log.info('    Errors:')
        for message in error_messages:
            self._tc_log.info('              {}'.format(message))
        self._tc_log.info('    Warnings:')
        for message in warning_messages:
            self._tc_log.info('              {}'.format(message))

        return failed

    def store_file_for_silk_result(self, filename):
        """ Takes a file and stores it in the Silk result directory so it will be stored with the test results

        :param filename: The file to store
        """
        self.copy_file(source_file=filename, destination_path=ctl.get_silk_results_dir())

    def _build_outputxml(self, errors, warnings, exceptions, tests=1):
        directory = ctl.get_silk_results_dir()
        outputfile = os.path.join(directory, 'output.xml')
        self._tc_log.debug(outputfile)
        testname = ctl.get_silk_test_name()

        test = ElementTree.Element('Test')
        test.attrib['TestItem'] = testname

        # WasSuccess
        wassuccess = ElementTree.SubElement(test, 'WasSuccess')
        if errors == 0:
            wassuccess.text = 'True'
        else:
            wassuccess.text = 'False'

        # RunCount
        runcount = ElementTree.SubElement(test, 'RunCount')
        runcount.text = str(tests)

        # FailureCount
        failurecount = ElementTree.SubElement(test, 'FailureCount')
        if errors > 0:
            failurecount.text = str(errors)
        else:
            failurecount.text = '0'

        # ErrorCount
        errorcount = ElementTree.SubElement(test, 'ErrorCount')
        errorcount.text = str(errors)

        # WarningCount
        warningcount = ElementTree.SubElement(test, 'WarningCount')
        warningcount.text = str(warnings)

        # Asserts
        asserts = ElementTree.SubElement(test, 'Asserts')
        asserts.text = str(exceptions)

        xmlstring = ElementTree.tostring(test)

        self._tc_log.debug('Output.xml is {}'.format(outputfile))
        outfile = open(outputfile, 'w')
        outfile.write(xmlstring)
        outfile.close()

    # @ExceptionHandler
    def _get_result_code(self, result, content=None):
        """ Return 0 if the request is successful (2xx).
            Return the result code if it is not.
        """
        if result is None:
            return 0

        if content is None:
            # Add option to send result and content separately for methods that don't return the standard result object
            try:
                status_code = result.status_code
            except AttributeError:
                # Not an object, so return the result as an int
                return result
            else:
                content = result.content.lower()
        else:
            content = content.lower()
            status_code = result

        bad_strings = ['stack trace', 'bad request']
        if (200 <= status_code < 300) and not (any(x in content for x in bad_strings)):
            retval = 0
        elif status_code == 403:
            self._tc_log.debug('The Object already exists\n\"{0}\"'.format(status_code))
            retval = 0
        else:
            retval = status_code
            self._tc_log.debug('Result failed with code {}'.format(status_code))
            self._tc_log.debug('Content: {}'.format(content))

        return retval

    # #############################################################################
    # User Management Functions                                                  #
    # #############################################################################
    # # @brief Sets the admin password
    # @param newpassword: The new admin password
    # @return:
    # XML Response Example:
    # \verbatim
    # <users>
    # <status>success</status>
    # <username>myUserName<\username>
    # <user_id>2</user_id>
    # </users>
    # \endverbatim
    # @ExceptionHandler
    def set_admin_password(self, newpassword):
        # Wrapper for update_user()
        old_password = self.uut[Fields.web_password]
        result = self.update_user(user='admin',
                                  old_password=old_password,
                                  new_password=newpassword,
                                  fullname=None,
                                  is_admin=True)

        if result == 0:
            self.uut[Fields.web_password] = newpassword
            return 0
        else:
            return 1

    # # @brief Sets the user's password through SSH
    #
    # @param user: The user to change
    # @param password: The password to use. If password is an empty string, it will delete the password
    def _set_password_through_ssh(self, user, password):
        result = self.execute("account -m -u {} -p '{}'".format(user, password))
        # Save config to flash:
        # SAVE_PASSWD
        self.execute('cp -f /etc/passwd  /usr/local/config/')

        # SAVE_SHADOW
        self.execute('cp -f /etc/shadow /usr/local/config/')

        # SAVE_SMBPW
        self.execute('cp -f /etc/samba/smbpasswd /usr/local/config/')

        # SAVE_GROUP
        self.execute('cp -f /etc/group /usr/local/config/')

        # SAVE_WEBDAV
        self.execute('cp -f /etc/passwd.webdav /usr/local/config/')
        return result

    # # @brief Sets the ssh password
    # @param newpassword: The new ssh password. If not passed, resets to the default password
    def set_ssh_password(self, newpassword=None, confirm_password=None, use_web_ui=False):
        if newpassword is None:
            newpassword = self.uut[Fields.default_ssh_password]

        if confirm_password is None:
            confirm_password = newpassword

        current_password = self._ssh.get_ssh_password()
        if current_password == newpassword:
            # Nothing to do
            result = 0
        else:
            if use_web_ui:
                if confirm_password != newpassword:  # Only used for testing blank SSH password if union on Web UI
                    return self._sel.update_ssh_password(newpassword, confirm_password)
                else:
                    result = self._sel.update_ssh_password(newpassword, confirm_password)
            else:
                ssh_user = self.uut[self.Fields.ssh_username]
                result = self._set_password_through_ssh(ssh_user, newpassword)[0]

            self.uut[Fields.ssh_password] = newpassword
            self._tc_log.debug('Changed SSH password to {}'.format(self.uut[Fields.ssh_password]))
            # Reconnect with the new authentication
            self.ssh_connect()

        return result

    def update_ssh_username(self, username):
        self.uut[Fields.ssh_username] = username
        self._tc_log.debug('Change self.uut[Fields.ssh_username] to {}'.format(username))

    # # @brief Updates a user with new information
    # @details  A single user can be identified either by the user_id or by the
    # username. Only after it is found that there is no user with a user_id
    # that matches, then the user will attempt to be found based on a
    # matching username. Such a request to update a single user requires one
    # of the two parameters to be specified. To change a user's password both
    # old_password and new_password are required; except for an Admin user,
    # who only needs to supply the new_password parameter.
    # When testing the API, the password must be base 64 encoded.
    # Refer to the Create User section for restrictions.
    #            Either the user_id or the username must be used to identify the user to
    #            be updated. Neither of these values can be modified.
    #
    #            NOTE: Do not use this to update the admin password. Doing so
    #                  will make it impossible for the Web UI and REST calls
    #                  to be accessed. If you need to change the admin
    #                  password, use set_admin_password() instead.
    #
    #  @param user:         name of the user to be created
    #  @param old_password: old password of the given user, default is 'None'
    #  @param new_password: new password of the given user, default is 'None'
    #  @param fullname:     the fullname of the user, default is 'None'
    #  @param is_admin:     whether or not the user is an admin, default is 'None'
    #  @return:
    #  XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #      <username>myUserName<\username>
    #      <user_id>2</user_id>
    #    </users>
    #    \endverbatim
    #
    # @ExceptionHandler
    def update_user(self, user, old_password=None, new_password=None, fullname=None, is_admin=None):
        result = self._rest.update_user(user, old_password, new_password, fullname, is_admin)
        result = self._get_result_code(result)

        if result == 0:
            self._update_current_password()

            if user == self.uut[self.Fields.default_web_username]:
                # Update the local admin password
                self._admin_password = new_password
            else:
                self._users[user] = new_password

        return result

    def download_config_file(self, config_file, proxies=None):
        """ Get the config file from the unit

        :param config_file: The destination file
        :param proxies: If present, which proxies to use for Requests. Used to debug using something like Charles.
        """
        self._sel.download_config_file(config_file=config_file, proxies=proxies)

    def upload_config_file(self, config_file, proxies=None, timeout=300):
        """ Uploads the specified config file and reboots the unit

        :param config_file: The config file to upload
        :param proxies: If present, which proxies to use for Requests. Used to debug using something like Charles.
        :param timeout: The amount of time to wait for the unit to reboot
        """

        self._sel.upload_config_file(config_file=config_file, proxies=proxies)

        # Wait for the unit to reboot
        self._tc_log.info('Waiting for unit to reboot')
        timer = Stopwatch(timer=timeout)
        while True:
            if self.is_ready():
                break

            if timer.is_timer_reached():
                raise Exception('Timed out waiting to boot')

            time.sleep(1)

    # # @brief Resets admin account to default using SSH
    #
    # Cannot use REST because we don't know the current admin password
    def reset_admin_password(self, ignore_errors=False):
        try:
            # Set the password to default
            result, output = self._set_password_through_ssh('admin', self.uut[Fields.default_web_password])
        except exception.SSHDisabledError:
            if ignore_errors:
                self._tc_log.debug(ctl.exception('Could not reset admin password. '
                                                 'If this is the first time the test was run after a factory reset, '
                                                 'this is expected since SSH is not enabled yet.'))

                return 0
            else:
                raise
        else:
            self._admin_password = self.uut[Fields.default_web_password]
            self._update_current_password()
            return result

    # Changes self.uut[Fields.web_password] to the current logged-in user's password. Should be called anytime a
    # password is updated.
    def _update_current_password(self):
        self.uut[Fields.web_password] = self._get_user_password(self.uut[Fields.web_username])

    # Returns the password for the given user
    def _get_user_password(self, user):
        # Lower case since it's case insensitive
        user = user.lower()
        if user == self.uut[Fields.default_web_username]:
            return self._admin_password
        else:
            return self._users[user]

            ######################################################################################################
            #                              REST API function wrappers                                            #
            ######################################################################################################

    # # @brief Reboots the test unit
    def reboot(self, max_boot_time=None, use_rest=True, reboot_type=RebootType.WARM):
        """ Reboots the UUT

        :param max_boot_time: The maximum amount of time to wait for the reboot
        :param use_rest: If True, use a REST call, not SSH. NOTE: Some devices must use REST, so this is ignored for
                         those devices.
        :param reboot_type: The type of reboot to do. REBOOT_WARM - Issue a shutdown request and let the firmware
                            reboot. REBOOT_COLD - Cycle power without a shutdown.
        :return:
        """
        # Use a short timeout since it will always fail
        model = self.get_model_number()
        rest_only_list = ['GLCR']
        if model in rest_only_list:
            use_rest = True
        if max_boot_time == None:
            max_boot_time = self.uut[Fields.reboot_time]
            self._tc_log.debug('max_boot_time: '+str(max_boot_time))
        if reboot_type == RebootType.WARM:
            if use_rest:
                try:
                    result = self._rest.reboot(timeout=(10, 10))
                except RestError as e:
                    if 'timeout' in e.message:
                        # It was a timeout error, which is expected, so pass
                        pass
                    else:
                        # It was a different REST error, so raise it
                        raise
                else:
                    # If this returned, that means it didn't actually reboot
                    self._tc_log.info('Failed to issue reboot command through REST:')
                    self._tc_log.info(str(result))
                    raise Exception('Failed to reboot')
            else:
                self._ssh.reboot()
        elif reboot_type == RebootType.COLD:
            self.power_cycle()
        else:
            raise exception.InvalidParameter('Reboot type of {} is invalid.'.format(reboot_type))

        # Wait for the shutdown to finish
        start = time.time()
        self._tc_log.info('Device is powering off for reboot')
        while self.is_device_pingable():
            time.sleep(1)
            if time.time() - start >= max_boot_time:
                raise Exception('Device failed to power off within {} seconds'.format(max_boot_time))

        # Now, wait for the system to come back up.
        self._tc_log.info('Waiting for device to power back up')
        start_time = time.time()
        while not self.is_ready():
            self._tc_log.debug('Web is not ready')
            if time.time() - start_time > max_boot_time:
                raise Exception('Timed out waiting to boot')
        self._tc_log.info('Web is ready')


        # The SSH connection will be invalid after a reboot, so disconnect
        self._ssh.disconnect()

        # And reconnect
        self.ssh_connect()



    # # @brief Shuts the test unit down.
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <shutdown>
    #       <status>success</status>
    #     </shutdown>
    #    \endverbatim
    #
    # @ExceptionHandler
    def shutdown(self, timeout=120):
        start = time.time()
        self._rest.shutdown()

        # Loop until the pings stop
        while self.is_device_pingable():
            time.sleep(0.5)
            if time.time() - start >= timeout:
                raise Exception('Device failed to power off within {} seconds'.format(timeout))

    # # @brief Logs into the test unit
    #  @param username: the name of the admin account default is 'admin'
    #  @param password: the password for the admin account default is blank
    #  @return
    #    XML Response Example:
    #    \verbatim
    #    <local_login>
    #        <status>success</status>
    #    </local_login>
    #    \endverbatim
    #
    # @ExceptionHandler
    def local_login(self, username='admin', password=''):
        result = self._rest.local_login(username, password)
        return self._get_result_code(result)

    # # @brief Logs out of the test unit
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <logout>
    #         <status>success</status>
    #     </logout>
    #    \endverbatim
    #
    # @ExceptionHandler
    def logout(self):
        result = self._rest.logout()
        return self._get_result_code(result)

    # # @brief Logs into the test unit from a mobile device
    #  @param device_user_id:        the id used to log into the system
    #  @param device_user_auth_code: the auth code of the user
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <login>
    #        <status>success</status>
    #        <username>myUser</username>
    #        <userid>myUserId</userid>
    #     </login>
    #    \endverbatim
    #
    # @ExceptionHandler
    def login(self, device_user_id, device_user_auth_code):
        result = self._rest.login(device_user_id, device_user_auth_code)
        return self._get_result_code(result)

    # # @brief Tests communications by retrieving the device id
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <port_test>
    #         <deviceid>27258</deviceid>
    #     </port_test>
    #    \endverbatim
    #

    def is_device_pingable(self):
    # # @brief Test connection
    # c need to smaller than w, otherwise response always be 256
    # respone : When you specify both -w, and -c, whichever comes first will terminate the ping command.

        command = 'ping '
        if os.name == 'nt':
            command += '-n 1 -w 1000'
        else:
            command += '-c 1 -W 1'

        command += ' {} > /dev/null 2>&1'.format(self.uut[self.Fields.internal_ip_address])

        response = os.system(command)
        self._tc_log.debug('response: {0}'.format(response))

        if response == 0:
            return True
        else:
            return False

    # Need its own handler since we want to return true or false
    def port_test(self):
        try:
            result = self._rest.port_test()
            return self._get_result_code(result)
        except urllib2.URLError:
            # TODO: Limit exception type
            self.warn_exception('Failed to reach the device')
            return 1

    # # @brief Returns the status of processes on the test unit
    #  @return:
    #    XML Response Example:
    #     \verbatim
    #     <status>
    #         <communicationmanager>running</communicationmanager>
    #         <mediacrawler>running</mediacrawler>
    #         <miocrawler>running</miocrawler>
    #     </status>
    #    \endverbatim
    #
    # @ExceptionHandler
    def status(self):
        result = self._rest.status()
        return self._get_result_code(result)

    # # @brief Returns the system state of the test unit
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <system_state>
    #         <status>ready</status>
    #         <temperature>good</temperature>
    #         <smart>good</smart>
    #         <volume>good</volume>
    #         <free_space>good</free_space>
    #     </system_state>
    #     \endverbatim
    #
    def system_state(self):
        result = self._rest.system_state()
        return self._get_result_code(result)

    # # @brief Queries the system state
    #  @return: True if the system is ready, false otherwise
    #
    def is_ready(self):
        try:
            return self._rest.is_ready(timeout=5)
        except:
            # TODO: Limit exception type
            return False

    # # @brief Starts a short or long smart test
    #  @details Only one test can be started at a time
    #  @param test: 'start_short' or 'start_long'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <smart_test>
    #        <status>success</status>
    #    </smart_test>
    #    \endverbatim
    #
    # @ExceptionHandler
    def start_smart_test(self, test='start_short'):
        result = self._rest.start_smart_test(test)
        return self._get_result_code(result)

    # # @brief Returns the status of a smart test
    #  @return:
    #    \verbatim
    #    percent_complete : {Number between 0 and 100}
    #    status : {good/bad/in_progress/aborted}
    #    \endverbatim
    #    XML Response Example:
    #    \verbatim
    #    <smart_test>
    #        <percent_complete>30</percent_complete>
    #        <status>inprogress</status>
    #    </smart_test>
    #    \endverbatim
    #
    def get_smart_results(self):
        result = self._rest.get_smart_results()
        return self._get_result_code(result), result

    # # @brief Returns all of the users
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <users>
    #         <user_id>2</user_id>
    #         <username>guest</username>
    #         <fullname>Guest User</fullname>
    #         <is_admin>false</is_admin>
    #         <is_password>false</is_password>
    #     </users>
    #    \endverbatim
    #
    def get_all_users(self):
        result = self._rest.get_all_users()
        return self._get_result_code(result), result

    # # @brief Gets the user account information
    #  @param username: name of the user you want information about
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <users>
    #         <user_id>2</user_id>
    #         <username>guest</username>
    #         <fullname>Guest User</fullname>
    #         <is_admin>false</is_admin>
    #         <is_password>false</is_password>
    #     </users>
    #    \endverbatim
    #
    def get_user(self, username):
        result = self._rest.get_user(username)
        return self._get_result_code(result), result

    # # @brief Modifies SSH configuration
    #  @param enable:    True or False
    #  @param enablessh: None (C.V. if set to true or false return the ssh_configuration instead of setting it up.)
    #  @return:
    #    XML Response Example:
    #    \verbatim77
    #     <ssh_configuration>
    #       <status>Success</status>
    #     </ssh_configuration>
    #    \endverbatim
    #
    def set_ssh(self, enable=True, enablessh=None, reset_ssh_client=True):
        # Reset the SSH client to clear any existing keys and prevent a bad key exception
        if reset_ssh_client:
            self._ssh = SshClient(self.uut)

        # Enable SSH
        result = self._rest.set_ssh(enable, enablessh)
        return self._get_result_code(result), result

    # # @brief Returns the current SSH configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <ssh_configuration>
    #         <enablessh>true</enablessh>
    #     </ssh_configuration>
    #    \endverbatim
    #
    def get_ssh_configuration(self):
        result = self._rest.get_ssh_configuration()
        return self._get_result_code(result), result

    # # @brief Returns the firmware info
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_info>
    #     <current_firmware>
    #       <package>
    #         <name>MyBookLiveDuo </name>
    #         <version>00.00.00-000</version>
    #         <description>Core F/W</description>
    #         <package_build_time>1364429632</package_build_time>
    #         <last_upgrade_time>1364429632</last_upgrade_time>
    #       </package>
    #     </current_firmware>
    #     <firmware_update_available>
    #       <available>false</available>
    #     </firmware_update_available>
    #     <upgrades>
    #       <available>false</available>
    #       <message></message>
    #     </upgrades>
    #    </firmware_info>
    #    \endverbatim
    #
    def get_firmware_info(self):
        result = self._rest.get_firmware_info()
        return self._get_result_code(result), result

    # # @brief Check for available firmware update
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_info>
    #        <status>success</status>
    #    </firmware_info>
    #    \endverbatim
    #
    def firmware_info(self):
        result = self._rest.firmware_info()
        return self._get_result_code(result), result

    # # @brief Causes the NAS to fetch and update firwmware
    #  @param image: image of the FW update
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update>
    #        <status>success</status>
    #    </firmware_update>
    #    \endverbatim
    #
    def firmware_update(self, image):
        result = self._rest.firmware_update(image)
        return self._get_result_code(result)

    def firmware_update_by_file(self, fw_filename):
        result = self._rest.firmware_update_by_file(fw_filename)
        return self._get_result_code(result)

    # # @brief Deletes all alerts on the test unit
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <users>
    #        <status>success</status>
    #    </users>
    #    \endverbatim
    #
    def delete_all_alerts(self):
        result = self._rest.delete_all_alerts()
        return self._get_result_code(result)

    # # @brief Deletes a single alert by its id
    #  @param alert_id: The id of the alert you want to delete
    #  @param ack:      Whether the alert is acknowledged, default is False
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <alerts>
    #      <status>{return status}</status>
    #    </alerts>
    #    \endverbatim
    #
    def delete_alert(self, alert_id, ack=False):
        result = self._rest.delete_alert(alert_id, ack)
        return self._get_result_code(result)

    # # @brief Acknowledge an alert by its id
    #  @param alert_id: the id of the alert you want to acknowledge
    #  @param ack:      Determines whether to acknowledge the alert, defaults to True
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <alerts>
    #        <status>{return status}</status>
    #    </alerts>
    #    \endverbatim
    #
    def acknowledge_alert(self, alert_id, ack=True):
        result = self._rest.acknowledge_alert(alert_id, ack)
        return self._get_result_code(result)

    # # @brief Retrieves all alerts
    #  @param simple:        Optional
    #  @param include_admin: Optional
    #  @param admin:         Optional
    #  @param specific:      Optional
    #  @param all:           Optional
    #  @param hide_ack:      Optional
    #  @param min_level:     Optional
    #  @param limit:         Optional
    #  @param offset:        Optional
    #  @param order:         Optional
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <?xml version="1.0" encoding="utf-8"?>
    #     <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    #       <channel>
    #         <title>alerts</title>
    #         <link>{alert link}</link>
    #         <atom:link href="{IP Address}/api/1.0/rest/alerts?format=rss" rel="self" type="application/rss+xml" />
    #         <description>System Alerts</description>
    #         <lastBuildDate>{alert date}</lastBuildDate>
    #         <language>{language}</language>
    #         <ttl>1</ttl>
    #         <item>
    #             <title>{alert title}</title>
    #             <link>{IP Address}/api/1.0/rest/display_alert?code={alert code}&amp;timestamp={alert time stamp}&amp;alert_id={alert id}&amp;acknowledged={alert acknowledgement}</link>
    #             <description>{alert description}</description>
    #             <pubDate>{xml publish date}</pubDate>
    #             <guid isPermaLink="true">{IP Address}/api/2.1/rest/display_alert?code={alert code}&amp;timestamp={alert time stamp}&amp;alert_id={alert id}&amp;acknowledged={alert acknowledgement}&amp;zuid=60d409a4</guid>
    #         </item>
    #       </channel>
    #     </rss>
    #    \endverbatim
    #
    def get_alerts(self, simple=True, include_admin=True, admin=False,
                   specific=False, all=True, hide_ack=True, min_level=10,
                   limit=20, offset=0, order='desc'):
        result = self._rest.get_alerts(simple, include_admin, admin, specific, all, hide_ack, min_level, limit,
                                       offset, order)
        return self._get_result_code(result), result

    # # @brief Returns the alert catalogue
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alert_events>
    #       <alerts>
    #         <alert>
    #           <code>0201</code>
    #           <severity>critical</severity>
    #           <scope>all</scope>
    #           <admin_ack_only>1</admin_ack_only>
    #           <description>Drive failed</description>
    #         </alert>
    #         <alert>
    #           <code>1202</code>
    #           <severity>warning</severity>
    #           <scope>all</scope>
    #           <admin_ack_only>0</admin_ack_only>
    #           <description>Drive too small</description>
    #         </alert>
    #         <alert>
    #           <code>2203</code>
    #           <severity>Info</severity>
    #           <scope>admin</scope>
    #           <admin_ack_only>1</admin_ack_only>
    #           <description>Drive is being initialized</description>
    #         </alert>
    #       </alerts>
    #     </alert_events>
    #    \endverbatim
    #
    def get_alert_catalogue(self):
        result = self._rest.get_alert_catalogue()
        return self._get_result_code(result), result

    # # @brief Returns the alert configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alert_configuration>
    #         <email_enabled>false</email_enabled>
    #         <email_returnpath>nas.alerts@wdc.com</email_returnpath>
    #         <min_level_email>10</min_level_email>
    #         <min_level_rss>10</min_level_rss>
    #         <email_recipient_0></email_recipient_0>
    #         <email_recipient_1></email_recipient_1>
    #         <email_recipient_2></email_recipient_2>
    #         <email_recipient_3></email_recipient_3>
    #         <email_recipient_4></email_recipient_4>
    #     </alert_configuration>
    #    \endverbatim
    #
    def get_alert_configuration(self):
        result = self._rest.get_alert_configuration()
        return self._get_result_code(result), result

    # # @brief Set an alert configuration for a single email/recipient pair
    #  @param recipient:     recipient of the alert
    #  @param email_address: the email address for the alert
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alert_configuration>
    #         <status>SUCCESS</status>
    #     </alert_configuration>
    #    \endverbatim
    #
    def set_alert_email(self, recipient=None, email_address=None):
        result = self._rest.set_alert_email(recipient, email_address)
        return self._get_result_code(result)

    # # @brief Set alert configuration, does
    #  @param email_enabled:     Optional
    #  @param min_level_email:   Optional
    #  @param min_level_rss:     Optional
    #  @param email_recipient_0: Optional
    #  @param email_recipient_1: Optional
    #  @param email_recipient_2: Optional
    #  @param email_recipient_3: Optional
    #  @param email_recipient_4: Optional
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alert_configuration>
    #         <status>SUCCESS</status>
    #     </alert_configuration>
    #    \endverbatim
    #
    def set_alert_configuration(self,
                                email_enabled='${CURRENT}',
                                min_level_email='${CURRENT}',
                                min_level_rss='${CURRENT}',
                                email_recipient_0='${CURRENT}',
                                email_recipient_1='${CURRENT}',
                                email_recipient_2='${CURRENT}',
                                email_recipient_3='${CURRENT}',
                                email_recipient_4='${CURRENT}'):
        result = self._rest.set_alert_configuration(email_enabled, min_level_email, min_level_rss,
                                                    email_recipient_0, email_recipient_1, email_recipient_2,
                                                    email_recipient_3, email_recipient_4)
        return self._get_result_code(result)

    # # @brief Sends an email to all configured recipients
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alert_configuration_test>
    #         <email_recipient_0 Status="Success">john.doe@wdc.com</email_recipient_0>
    #     </alert_configuration_test>
    #    \endverbatim
    #
    def test_alert_configuration(self):
        result = self._rest.test_alert_configuration()
        return self._get_result_code(result)

    # # @brief Sends email alerts
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alert_configuration_test>
    #         <email_recipient_0 Status="Success">john.doe@wdc.com</email_recipient_0>
    #     </alert_configuration_test>
    #    \endverbatim
    #
    def send_email_alerts(self):
        result = self._rest.send_email_alerts()
        return self._get_result_code(result)

    # # @brief Generates an alert
    #  @param alert_code:               Code of the alert you want to generate
    #  @param alert_description_string: String of the alert you want to generate
    #  @param user_id:                  id of the user to receive the alert
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <alerts>
    #       <status>{return status}</status>
    #     </alerts>
    #    \endverbatim
    #
    def generate_alert(self, alert_code, alert_description_string, user_id):
        result = self._rest.generate_alert(alert_code, alert_description_string, user_id)
        return self._get_result_code(result)

    # # @brief Deletes a user
    #  @details A single user can be identified either by the user_id or by the
    #           username. Only after it is found that there is no user with a user_id
    #           that matches, then the user will attempt to be found based on a
    #           matching username. Such a request to delete a single user requires
    #           one of the two parameters to be specified. There is no facility to
    #           delete all users in one API call. Either the user_id or username
    #           must be used to identify the user to be deleted.
    #  @param user: the user to be deleted, can be a user_id or a user name
    #  @return:
    #  XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #    </users>
    #    \endverbatim
    #
    def delete_user(self, user):
        result = self._rest.delete_user(user)
        return self._get_result_code(result)

    # # @brief Deletes all users except those that are excluded
    #  @param exclude: A single user or a list of users to be excluded from deletion, defaults to admin
    #  @param use_rest: True - use rest call to delete user; False - use ssh command to delete user
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #    </users>
    #    \endverbatim
    #
    def delete_all_users(self, exclude='admin', use_rest=True):
        if use_rest:
            result = self._rest.delete_all_users(exclude)
            # return self._get_result_code(result)
            return result
        else:
            self._ssh.delete_all_users(exclude)

    # # @brief Creates a user
    #  @details A username must be an alphanumeric value between 1 and 32 characters
    #           and cannot contain spaces; but is allowed to start with a letter or a
    #           number, and contain the underscore character.
    #           The length of a password can be between 0 and 255 characters; and can
    #           contain any ASCII character.
    #  @param username: name of the user to be created
    #  @param password: password of the user to be created, default is 'None'
    #  @param fullname: The fullname of the user, default is 'none'
    #  @param is_admin: Whether the new user is an admin, default is 'False'
    #  @param group_names: optional, defaults to 'cloudholders'.
    #  @return:
    #  XML Response Example:
    #    \verbatim
    #    <users>
    #      <status>success</status>
    #      <username>myUserName<\username>
    #      <user_id>2</user_id>
    #    </users>
    #    \endverbatim
    #
    def create_user(self, username, password=None, fullname=None, is_admin=False, group_names='cloudholders'):
        username = username.lower()
        if username == self.uut[self.Fields.default_web_username]:
            raise exception.InvalidParameter('User {} is a reserved name'.format(username))

        result = self._rest.create_user(username, password, fullname, is_admin, group_names)
        return_code = self._get_result_code(result)
        if return_code == 0:
            self._users[username.lower()] = password
            self._update_current_password()

        return return_code

    # # @brief Switches the active user to username
    #
    # Logs out of the web UI if it's loaded and logs in with the new user
    def switch_user(self, username):
        username = username.lower()

        self.uut[self.Fields.web_username] = username
        self._update_current_password()

        if self._sel._is_browser():
            self.close_webUI()
            self.login_webUI()

    # # @brief Switches the active user to the default admin user
    def switch_to_admin(self):
        self.switch_user(self.uut[Fields.default_web_username])

    # # @brief Returns total disk size in MB
    #  @return: total disk size in MB
    #
    def get_disk_size(self):
        return self._rest.get_disk_size()

    # # @brief Returns total disk usage in MB
    #  @return: total disk usage in MB
    #
    def get_disk_usage(self):
        return self._rest.get_disk_usage()

    # # @brief Returns the storage usage on the drive
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <storage_usage>
    #      <size>152162</size>
    #      <usage>145031554</usage>
    #      <video>0</video>
    #      <photos>81082671</photos>
    #      <music>62489606</music>
    #      <other>1459277</other>
    #      <shares>
    #        <share>
    #          <sharename>USB_DRIVE</sharename>
    #          <usage>51033037</usage>
    #          <video>0</video>
    #          <photos>49732482</photos>
    #          <music>0</music>
    #          <other>1300555</other>
    #        </share>
    #        <share>
    #          <sharename>WD_USB</sharename>
    #          <usage>93998517</usage>
    #          <video>0</video>
    #          <photos>31350189</photos>
    #          <music>62489606</music>
    #          <other>158722</other>
    #        </share>
    #      </shares>
    #    </storage_usage>
    #    \endverbatim
    #
    def get_storage_usage(self):
        result = self._rest.get_storage_usage()
        return self._get_result_code(result), result

    # # @brief Retrieves the system information
    #  @return
    #    XML Response Example:
    #    \verbatim
    #    <system_information>
    #      <manufacturer>Western Digital Corporation</manufacturer>
    #      <manufacturer_url>http://www.wdc.com</manufacturer_url>
    #      <model_description>WD My Cloud 4-Bay Network Storage</model_description>
    #      <model_name>WDMyCloudEX4</model_name>
    #      <model_url>http://products.wd.com/wdmycloudex4</model_url>
    #      <model_number>LT4A</model_number>
    #      <host_name>WDMyCloudEX4</host_name>
    #      <capacity>1</capacity>
    #      <serial_number>string</serial_number>
    #      <master_drive_serial_number>6078486b:1a4455a5:bc7c7109:4227785e</master_drive_serial_number>
    #      <mac_address>00:90:A9:66:56:C6</mac_address>
    #      <uuid>73656761-7465-7375-636b-0090a96656c6</uuid>
    #      <wd2go_server/><dlna_server/>
    #    </system_information>
    #    \endverbatim
    #
    def get_system_information(self):
        result = self._rest.get_system_information()
        return self._get_result_code(result), result

    # # @brief Retrieves system configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <system_configuration>
    #      <path_to_config>/CacheVolume/name-20120530-1823.conf</path_to_config>
    #    </system_configuration>
    #    \endverbatim
    #
    def get_system_configuration(self, attach_file=False):
        result = self._rest.get_system_configuration(attach_file)
        return self._get_result_code(result), result

    # # @brief Restore system configuration
    #  @param filepath: file path to the system configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <restore_status>
    #      <status_code></status_code>
    #      <status_description></status_description>
    #    </restore_status>
    #    \endverbatim
    #
    def restore_system_configuration(self, filepath=None):
        result = self._rest.restore_system_configuration(filepath)
        return self._get_result_code(result)

    # # @brief Performs a system restore on the unit and waits for it to complete
    #  @param erase: should be 'format', 'zero', or 'systemOnly'
    #  @param timeout: How long to wait for the factory restore to finish
    def perform_factory_restore(self, erase='format', timeout=300, wait_after_finish=0):
        self.start_factory_restore(erase=erase)
        self.log.info('Factory restore started')
        self.wait_for_factory_restore_to_finish(timeout=timeout, wait_after_finish=wait_after_finish)
        self.log.info('Factory restore complete')

    # # @brief Starts a restore on the drive
    #  @param erase: should be 'format', 'zero', or 'systemOnly'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <system_factory_restore>
    #      <status>success</status>
    #    </system_factory_restore>
    #    \endverbatim
    #
    def start_factory_restore(self, erase='format'):
        result = self._rest.start_factory_restore(erase)
        return self._get_result_code(result)

    # # @brief Retrieves the factory restore progress
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <system_factory_restore>
    #      <percent></percent>
    #      <status>idle</status>
    #    </system_factory_restore>
    #    \endverbatim
    #
    def get_factory_restore_progress(self):
        result = self._rest.get_factory_restore_progress()
        return self._get_result_code(result), result

    # # @brief Polls the factory restore progress until it begins
    #
    def wait_for_factory_restore_to_start(self):
        result = self._rest.wait_for_factory_restore_to_start()
        return self._get_result_code(result)

    # # @brief Polls the factory restore progress until it finishes
    #
    def wait_for_factory_restore_to_finish(self, timeout=300, wait_after_finish=60):
        result = self._rest.wait_for_factory_restore_to_finish(timeout, wait_after_finish=wait_after_finish)
        return self._get_result_code(result)

    # # @brief Retrieves the firmware update configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update_configuration>
    #      <auto_install>disable</auto_install>
    #      <auto_install_day>0</auto_install_day>
    #      <auto_install_hour>3</auto_install_hour>
    #    </firmware_update_configuration>
    #    \endverbatim
    #
    def get_firmware_update_configuration(self):
        result = self._rest.get_firmware_update_configuration()
        return self._get_result_code(result), result

    # # @brief Modifies firmware update configuration
    #  @param auto_install:      enable/disable, whether auto update happens
    #  @param auto_install_day:  represents the update day, 0 is every day, 1-7 is Monday-Sunday
    #  @param auto_install_hour: The hour of the update, 0-23 hour of day
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update_configuration>
    #      <status>success</status>
    #    </firmware_update_configuration>
    #    \endverbatim
    #
    def set_firmware_update_configuration(self, auto_install="disable", auto_install_day="0", auto_install_hour="3"):
        result = self._rest.set_firmware_update_configuration(auto_install, auto_install_day, auto_install_hour)
        return self._get_result_code(result)

    # # @brief Retrieves firmware update status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update>
    #      <status>idle</status>
    #      <completion_percent></completion_percent>
    #      <error_code></error_code>
    #      <error_description></error_description>
    #    </firmware_update>
    #    \endverbatim
    #
    def get_firmware_update_status(self):
        result = self._rest.get_firmware_update_status()
        return self._get_result_code(result), result

    # # @brief Causes the NAS to automatically update FW
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update>
    #      <status>idle</status>
    #      <completion_percent></completion_percent>
    #      <error_code></error_code>
    #      <error_description></error_description>
    #    </firmware_update>
    #    \endverbatim
    #
    def set_firmware_update(self):
        result = self._rest.set_firmware_update()
        return self._get_result_code(result)

    # # @brief Polls firmware update status until it has begun
    #
    def wait_for_firmware_update_to_start(self):
        return self._rest.wait_for_firmware_update_to_start()

    # # @brief Polls firmware update status until it completes
    #
    def wait_for_firmware_update_to_complete(self):
        return self._rest.wait_for_firmware_update_to_complete()

    # # @brief Starts a firmware update using the given uri path
    #  @param firmware_uri: uri must be url encoded
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update
    #      <status>success</status>
    #    </firmware_update>
    #    \endverbatim
    #
    def start_firmware_update_from_uri(self, firmware_uri):
        result = self._rest.start_firmware_update_from_uri(firmware_uri)
        return self._get_result_code(result)

    # # @brief Starts a firmware update using the given file
    #  @details
    #    If the specified path is a file on the test PC, it will be uploaded as part of the POST data (multipart file).
    #    http://docs.python-requests.org/en/v1.0.0/user/quickstart/#post-a-multipart-encoded-file
    #
    #    Otherwise, it will be assumed that the path refers to a file on the NAS (sent as a string)
    #    Example: '/DataVolume/shares/public/ap2nc-024006-048-20121206.deb'
    #    **Note**: This type of update will fail if the .deb is not extracted.
    #  @param firmware_path: Path of the file on the device
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <firmware_update
    #      <status>success</status>
    #    </firmware_update>
    #    \endverbatim
    #
    def start_firmware_update_from_file(self, firmware_path):
        result = self._rest.start_firmware_update_from_file(firmware_path)
        return self._get_result_code(result)

    # # @brief Returns the firmware version
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <version>
    #      <firmware>01.00.08-432</firmware>
    #      <modules>
    #        <module>
    #          <module_name>albummetadata</module_name>
    #          <module_version>1.0</module_version>
    #          <module_description>Module has albummetadata related components.</module_description>
    #        </module>
    #        -
    #        -
    #      </modules>
    #    </version>
    #    \endverbatim
    #
    def get_firmware_version(self):
        return self._rest.get_firmware_version()

    # # @brief Returns the firmware version number
    #  @return: The firmware version number
    #

    def get_firmware_version_number(self):
        self._tc_log.debug('Getting firmware version from device.')
        try:
            result = self._rest.get_firmware_version_number()
        except Exception as e:
            self._tc_log.warning("Failed to get firmware version by RESTful api, exception: {0}".format(repr(e)))
            result = self._ssh.get_current_firmware_version()[-1]
        self._tc_log.info('Firmware version is {}'.format(result))
        if self.uut[Fields.product] == 'Sequoia':
            result = ctl.convert_seq_firmware_to_alpha_format(result)
        else:
            self.uut[Fields.actual_fw_version] = result
        return result

    # # @brief Update UUT to specific firmware version
    #
    def firmware_update_nexus(self, device_ip, current_firmware, product, group):
        return self._rest.firmware_update_nexus(device_ip, current_firmware, product, group)

    # # @brief Returns the network configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <?xml version="1.0" encoding="utf-8"?>
    #    <network_configurations>
    #      <network_configuration>
    #        <ifname>eth0</ifname>
    #        <iftype>wired</iftype>
    #        <proto>dhcp_client</proto>
    #        <ip>192.168.0.165</ip>
    #        <netmask></netmask>
    #        <gateway></gateway>
    #        <dns0></dns0>
    #        <dns1></dns1>
    #        <dns2></dns2>
    #      </network_configuration>
    #      <network_configuration>
    #        <ifname>ath0</ifname>
    #        <iftype>wireless</iftype>
    #        <proto>dhcp_client</proto>
    #        <ip></ip>
    #        <netmask></netmask>
    #        <gateway></gateway>
    #        <dns0></dns0>
    #        <dns1></dns1>
    #        <dns2></dns2>
    #      </network_configuration>
    #    </network_configurations>
    #    \endverbatim
    #
    def get_network_configuration(self):
        result = self._rest.get_network_configurations()
        return self._get_result_code(result), result

    # # @brief Sets the network configuration to the given values
    #  @details In DHCP case, ip, netmask, gateway, dns0, dns1 and dns2 values are ignored.
    #  @param proto:   The network mode must be set to 'dhcp_client' or 'static'.
    #  @param ip:      Any Class A, B or C address except 127.X.X.X and those with all zeros or ones in the host portion
    #  @param netmask: Is the number of leading binary 1s to make up mask. Valid values are 1 to 31.
    #  @param gateway: If gateway is set, IP and gateway must be on the same network.
    #  @param dns0:    If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
    #  @param dns1:    If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
    #  @param dns2:    If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <network_configuration>
    #      <status>success</status>
    #    </network_configuration>
    #    \endverbatim
    #
    def set_network_configuration(self,
                                  proto=None,
                                  ip=None,
                                  netmask=None,
                                  gateway=None,
                                  dns0=None,
                                  dns1=None,
                                  dns2=None):
        result = self._rest.set_network_configuration(proto, ip, netmask, gateway, dns0, dns1, dns2)
        return self._get_result_code(result)

    # # @brief Returns network configurations
    #  @param ifname: Name of the network configuration to get, default is 'None' which returns all networks
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <?xml version="1.0" encoding="utf-8"?>
    #    <network_configurations>
    #      <network_configuration>
    #        <ifname>eth0</ifname>
    #        <iftype>wired</iftype>
    #        <proto>dhcp_client</proto>
    #        <ip>192.168.0.165</ip>
    #        <netmask></netmask>
    #        <gateway></gateway>
    #        <dns0></dns0>
    #        <dns1></dns1>
    #        <dns2></dns2>
    #      </network_configuration>
    #      <network_configuration>
    #        <ifname>ath0</ifname>
    #        <iftype>wireless</iftype>
    #        <proto>dhcp_client</proto>
    #        <ip></ip>
    #        <netmask></netmask>
    #        <gateway></gateway>
    #        <dns0></dns0>
    #        <dns1></dns1>
    #        <dns2></dns2>
    #      </network_configuration>
    #    </network_configurations>
    #    \endverbatim
    #
    def get_network_configurations(self, ifname=None):
        result = self._rest.get_network_configurations(ifname)
        return self._get_result_code(result), result

    # # @brief Sets a/all network configuration/s
    #  @details In DHCP case, ip, netmask, gateway, dns0, dns1 and dns2 values are ignored.
    #  @param ifname:  The network interface name (eth0, ath0,...)
    #  @param iftype:  The network interface type (wired or wireless)
    #  @param proto:   The network mode must be set to 'dhcp_client' or 'static'
    #  @param ip:      Any Class A, B or C address except 127.X.X.X and those with all zeros or ones in the host portion
    #  @param netmask: Is the number of leading binary 1s to make up mask. Valid values are 1 to 31
    #  @param gateway: If gateway is set, IP and gateway must be on the same network
    #  @param dns0:    If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
    #  @param dns1:    If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
    #  @param dns2:    If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <network_configuration>
    #      <status>success</status>
    #    </network_configuration>
    #    \endverbatim
    #
    # TODO: Figure out what to do about ifname and iftype, which aren't being used
    # noinspection PyUnusedLocal
    def set_network_configurations(self,
                                   ifname=None,
                                   iftype=None,
                                   proto=None,
                                   ip=None,
                                   netmask=None,
                                   gateway=None,
                                   dns0=None,
                                   dns1=None,
                                   dns2=None):
        result = self._rest.set_network_configuration(proto, ip, netmask, gateway, dns0, dns1, dns2)
        return self._get_result_code(result)

    # # @brief Deletes a volume
    #  @param volume_id: id of the volume to be deleted, defaults to 2
    #  @return:
    #  XML Response Example:
    #    | <volumes>
    #    |   <status>success</status>
    #    | </volumes>
    def delete_volume(self, volume_id='2'):
        result = self._rest.delete_volume(volume_id)
        return self._get_result_code(result)

    # # @brief Returns volume
    #  @param volume_id: id of the volume to be retrieved, if no id is provided returns all volumes
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <volumes>
    #      <volume>
    #        <volume_id>1</volume_id>
    #        <label>shares</label>
    #        <base_path>/shares</base_path>
    #        <drive_path>/dev/sda4</drive_path>
    #        <is_connected>true</is_connected>
    #      </volume>
    #    </volumes>
    #    \endverbatim
    #
    def get_volumes(self, volume_id=None):
        result = self._rest.get_volumes(volume_id)
        return self._get_result_code(result), result

    # # @brief Adds an existing volume into the volume database
    #  @param volume_id:    Id of the volume
    #  @param label:        Name of the volume
    #  @param base_path:    base path to the volume
    #  @param drive_path:   path to the volume
    #  @param is_connected: if the volume is connected or not
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <volumes>
    #      <status>success</status>
    #    </volumes>
    #    \endverbatim
    #
    def create_volume(self, volume_id='2', label='test', base_path='', drive_path='/', is_connected=True):
        result = self._rest.create_volume(volume_id, label, base_path, drive_path, is_connected)
        return self._get_result_code(result)

    # # @brief Get info for all USB drives attached to test unit
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <usb_drives>
    #        <usb_drive>
    #        <name>SanDisk Cruzer</name>
    #        <handle>1</handle>
    #        <serial_number>1000150906110937598D</serial_number>
    #        <vendor>SanDisk</vendor>
    #        <model>Cruzer</model>
    #        <revision>1100</revision>
    #        <ptp>false</ptp>
    #        <smart_status>unsupported</smart_status>
    #        <standby_timer>unsupported</standby_timer>
    #        <lock_state>unsupported</lock_state>
    #        <usage>875.061248</usage>
    #        <capacity>7988.678656</capacity>
    #        <vendor_id>0781</vendor_id>
    #        <product_id>5530</product_id>
    #        <usb_port>2</usb_port>
    #        <usb_version>2.0</usb_version>
    #        <usb_speed>480</usb_speed>
    #        <is_connected>true</is_connected>
    #        <volumes>
    #            <volume>
    #                <volume_id>1</volume_id>
    #                <base_path>/shares/Cruzer</base_path>
    #                <label></label>
    #                <mounted_date>1338572532</mounted_date>
    #                <usage>875.061248</usage>
    #                <capacity>7988.678656</capacity>
    #                <read_only>false</read_only>
    #                <shares>
    #                    <share>Cruzer</share>
    #                </shares>
    #            </volume>
    #        </volumes>
    #        </usb_drive>
    #    </usb_drives>
    #    \endverbatim
    #
    def get_usb_info(self):
        result = self._rest.get_usb_info()
        return self._get_result_code(result), result

    # # @brief Retrieve info for a USB drive
    #  @param handler: Handler for the usb drive, defaults to 'temp'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <usb_drive>
    #       <name>Western_Digital My_Passport_0778</name>
    #       <handle>WX81E32UHY88</handle>
    #       <serial_number>WX81E32UHY88</serial_number>
    #       <vendor>Western_Digital</vendor>
    #       <model>My_Passport_0778</model>
    #       <revision>1.042</revision>
    #       <ptp/>
    #       <smart_status/>
    #      <standby_timer/>
    #      <lock_state>unlocked</lock_state>
    #       <password_hint/>
    #       <usage>201.562500</usage>
    #       <capacity>476908.000000</capacity>
    #       <vendor_id>1058</vendor_id>
    #       <product_id>0778</product_id>
    #       <usb_port>1</usb_port>
    #       <usb_version>3.00</usb_version>
    #       <usb_speed>5000</usb_speed>
    #       <is_connected>true</is_connected>
    #       <file_system_type>ntfs</file_system_type>
    #       <volumes>
    #           <volume>
    #               <volume_id>WX81E32UHY88</volume_id>
    #               <base_path>/mnt/USB/USB2_e1</base_path>
    #               <label>New Volume</label>
    #               <mounted_date>1402945717</mounted_date>
    #               <usage>201.562500</usage>
    #               <capacity>476904.996094</capacity>
    #               <read_only/>
    #               <shares>
    #                   <share>My_Passport_0778-1</share>
    #               </shares>
    #            </volume>
    #       </volumes>
    #    </usb_drive>
    #    \endverbatim
    #
    def get_usb_info_handler(self, handler='temp'):
        result = self._rest.get_usb_info_handler(handler)
        return self._get_result_code(result), result

    # # @brief Unlocks a USB locked drive
    #  @param handler:        handler for the USB drive
    #  @param drive_password: password for the USB drive
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <usb_drive>
    #        <status>success</status>
    #    </usb_drive>
    #    \endverbatim
    #
    def unlock_usb_drive(self, handler='temp', drive_password='temp'):
        result = self._rest.unlock_usb_drive(handler, drive_password)
        return self._get_result_code(result)

    # # @brief Ejects a USB Drive
    #  @details The DELETE request is used to eject a specified Usb drive, given drive's handler
    #           It causes all the shares associated with the drive to be deleted and all of its partitions unmounted.
    #           Example: http://192.168.1.125/api/2.1/rest/usb_drive/WX61C22C7470?rest_method=DELETE
    #  @param handler: the handler of the USB drive to be ejected
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <usb_drive>
    #        <status>success</status>
    #    </usb_drive>
    #    \endverbatim
    #
    def eject_usb_drive(self, handler='temp'):
        result = self._rest.eject_usb_drive(handler)
        return self._get_result_code(result)

    # # @brief Returns USB drive tags as a list of strings that can be set as test tags
    #  @return: A list of strings
    #
    def get_usb_drive_tags(self):
        result = self._rest.get_usb_drive_tags()
        return self._get_result_code(result), result

    # # @brief Returns a share
    #  @param share_name: name of the share to be retrieved
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <shares>
    #        <share>
    #            <share_name>Public</share_name>
    #            <description/>
    #            <size>0</size>
    #            <remote_access>true</remote_access>
    #            <public_access>true</public_access>
    #            <media_serving>ture</media_serving>
    #            <volume_id>WD-WMC4M1170610_2</volume_id>
    #            <dynamic_volume>false</dynamic_volume>
    #        </share>
    #    </shares>
    #    \endverbatim
    #
    def get_share(self, share_name=TEST_SHARE_NAME):
        result = self._rest.get_share(share_name)
        return self._get_result_code(result), result

    # # @brief Creates share(s)
    #  @param share_name_prefix:     Name of the share(s) prefix to be created, default is 'share'
    #  @param number_of_shares:      Number of shares to be created, default is 1 for a single share
    #  @param description:           Share description - default is determined by TEST_SHARE_DESC in utilities library
    #  @param public_access:         Whether Public access is enabled, default is True
    #  @param media_serving:         Media serving setting, takes either 'any' or 'none', default is 'any'
    #  @param volume_name:           If volume_name is specified, create shares with the volume's id.
    #                                If not, try to search existed volumes from Volume_1 to Volume_4 and create shares in it.
    def create_shares(self, number_of_shares=1, share_name='share', description=TEST_SHARE_DESC,
                      media_serving='any', force_webui=False, volume_name=''):
        if force_webui:
            self._sel.create_shares(number_of_shares, share_name)
            return 0
        else:
            volume_id = ''
            result_code, result = self.get_volumes()
            if result_code != 0:
                raise exception.RestCallUnsuccessful('Failed to get volume info! Error code: {0}'.format(result_code))
            else:
                # Need to check base_path to know which volume_id we want to get
                volume_path_list = self.get_xml_tags(result.text, 'base_path')
                volume_id_list = self.get_xml_tags(result.text, 'volume_id')
                if volume_name:
                    try:
                        # base_path format: /shares/Volume_1
                        volume_index = volume_path_list.index('/shares/{}'.format(volume_name))
                        volume_id = volume_id_list[volume_index]
                    except:
                        raise exception.ItemNotInList('Cannot find the volume id of specified volume: {0}'.format(volume_name))
                else:
                    # Currently the maximum volume number is 4
                    max_volumes = 4
                    for volume in range(1, max_volumes+1):
                        volume_name = 'Volume_{}'.format(volume)
                        try:
                            volume_index = volume_path_list.index('/shares/{0}'.format(volume_name))
                            volume_id = volume_id_list[volume_index]
                            self.log.debug('Select volume: {0} to create shares.'.format(volume_name))
                            break
                        except:
                            if volume == max_volumes:
                                raise exception.ItemNotInList('Cannot find any available volume to create shares!')
                            else:
                                continue
            failed_creates = []
            share = share_name
            for i in range(1, number_of_shares + 1):
                if number_of_shares > 1:
                    share = '{0}{1}'.format(share_name, i)

                result = self._rest.create_shares(share, description, media_serving, volume_id=volume_id)
                result_code = self._get_result_code(result)
                if result_code != 0:
                    error_info = 'Share:{0}, Description:{1}, Media_Serving:{2}, Error_Code:{3}'.format(share, description, media_serving, result_code)
                    failed_creates.append(error_info)
                    self.log.warning('Failed to create share folder by Rest API! {0}'.format(error_info))
            if failed_creates:
                return failed_creates
            else:
                return 0

    # # @brief Creates a given number of shares, or one by default
    #  @param numberOfshares: Number of shares to create, is 1 by default
    #  @param force_webui : Enable true to force using webUI
    #
    # noinspection PyPep8Naming,PyPep8Naming
    def create_shares_webUI(self, numberOfshares=1):
        return self.create_shares(number_of_shares=numberOfshares, force_webui=True)

    # # @brief Updates a share
    #  @param share_name:     Name of the share to be updated
    #  @param new_share_name: New name of the share, default is None
    #  @param description:    Share description, default is determined by the TEST_SHARE_DESC in the utilities library
    #  @param public_access:  Whether Public access is enabled, default is True
    #  @param media_serving:  Media serving setting, default is 'any'
    def update_share(self, share_name, new_share_name=None, description=TEST_SHARE_DESC,
                     public_access=True, media_serving='any'):
        result = self._rest.update_share(share_name, new_share_name, description, public_access, media_serving)
        return self._get_result_code(result)

    # # @brief Returns all shares
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <shares>
    #        <share>
    #            <share_name>Public</share_name>
    #            <description/>
    #           <size>0</size>
    #            <remote_access>true</remote_access>
    #            <public_access>true</public_access>
    #            <media_serving>true</media_serving>
    #            <volume_id>WD-WMC4M1170610_2</volume_id>
    #            <dynamic_volume>false</dynamic_volume>
    #        </share>
    #    </shares>
    #    \endverbatim
    #
    def get_all_shares(self):
        result = self._rest.get_all_shares()
        return self._get_result_code(result), result

    # # @brief Deletes a users share access
    #  @param share_name: Name of the share to be adjusted - default determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #
    def delete_share_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        result = self._rest.delete_share_access(share_name, username)
        return self._get_result_code(result)

    # # @brief Returns a users share access
    #  @param share_name: Name of the share to be adjusted, default determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <share_access_list>
    #        <share_name>test</share_name>
    #        <share_access>
    #            <username>est</username>
    #            <user_id>est</user_id>
    #           <access>RO</access>
    #        </share_access>
    #        <share_access>
    #            <username>@group0</username>
    #            <user_id>@group0</user_id>
    #            <access>RW</access>
    #        </share_access>
    #    </share_access_list>
    #    \endverbatim
    #
    def get_share_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        result = self._rest.get_share_access(share_name, username)
        return self._get_result_code(result), result

    # # @brief Creates share access for a user
    #  @param share_name: The share name of the share to be adjusted,
    #                     default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #  @param access:     The level of access to be granted, default is 'RW'
    #  @return:
    #    XML Response Example
    #    \verbatim
    #    <share_access>
    #        <status>success</status>
    #    </share_access>
    #    \endverbatim
    #
    def create_share_access(self, share_name=TEST_SHARE_NAME, username='admin', access='RW'):
        result = self._rest.create_share_access(share_name, username, access)
        return self._get_result_code(result), result

    # # @brief Updates share access
    #  @param share_name: The share name of the share to be adjusted,
    #                     default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    #  @param access:     The level of access to be granted, default is 'RW'
    #  @return:
    #    XML Response Example
    #    \verbatim
    #    <share_access>
    #        <status>success</status>
    #    </share_access>
    #    \endverbatim
    #
    def update_share_access(self, share_name=TEST_SHARE_NAME, username='admin', access='RW'):
        result = self._rest.update_share_access(share_name, username, access)
        return self._get_result_code(result), result

    # # @brief Updates a share to have network access as Public
    #  @param share_name: The share name of the share to be adjusted,
    #                     default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    # http://192.168.1.138/api/2.6/rest/shares/jtran?username=admin&public_access=true&auth_password=&auth_username=admin
    # @return:
    # <shares>
    #    <status>success</status>
    # </shares>
    def update_share_network_access(self, share_name=TEST_SHARE_NAME, username='admin', public_access=True):
        result = self._rest.update_share_network_access(share_name, username, public_access)
        return self._get_result_code(result)

    # # @brief Get a share network access
    #  @param share_name: The share name of the share to be adjusted,
    #                     default is determined by TEST_SHARE_NAME in utilities library
    #  @param username:   The name of the user that will have access adjusted, default is 'admin'
    # http://192.168.1.138/api/2.6/rest/shares/jtran?username=admin&auth_password=&auth_username=admin
    # @return:
    # <shares>
    #   <share>
    #     <share_name>jtran</share_name>
    #     <description>atestshare</description>
    #     <size>0</size>
    #     <remote_access>true</remote_access>
    #     <public_access>true</public_access>
    #     <media_serving>any</media_serving>
    #     <volume_id>WD-WX31D847KLZL_2</volume_id>
    #     <dynamic_volume>false</dynamic_volume>
    #   </share>
    # </shares>
    def get_share_network_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        result = self._rest.get_share_network_access(share_name, username)
        return self._get_result_code(result), result

    # # @brief Retrieves the system state
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <system_state>
    #      <status>ready</status>
    #      <temperature>good</temperature>
    #      <smart>good</smart>
    #      <volume>good</volume>
    #      <free_space>good</free_space>
    #    </system_state>
    #    \endverbatim
    #
    def get_system_state(self):
        result = self._rest.get_system_state()
        return self._get_result_code(result), result

    # # @ Returns the date&time configuration of the system
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <date_time_configuration>
    #        <datetime>{timestamp}</datetime>
    #        <ntpservice>{on/off}</ntpservice>
    #        <ntpsrv0>{Fixed address of time server}</ntpsrv0>
    #        <ntpsrv1>{Fixed address of time server}</ntpsrv1>
    #        <ntpsrv_user>{User enterable time server}</ntpsrv_user>
    #        <time_zone_name>{Name from timeZones}</time_zone_name>
    #    </date_time_configuration>
    #    \endverbatim
    #
    def get_date_time_configuration(self):
        result = self._rest.get_date_time_configuration()
        return self._get_result_code(result), result

    # # @brief Sets the date&time configuration settings
    #  @param datetime:       A time stamp for the new time
    #  @param ntpservice:     Ntp service activated or not
    #  @param ntpsrv0:        Fixed address of time server 0
    #  @param ntpsrv1:        Fixed address of time server 1
    #  @param ntpsrv_user:    User selectable time server
    #  @param time_zone_name: Name from timeZones
    #
    def set_date_time_configuration(self,
                                    datetime=None,
                                    ntpservice=None,
                                    ntpsrv0=None,
                                    ntpsrv1=None,
                                    ntpsrv_user=None,
                                    time_zone_name=None):
        result = self._rest.set_date_time_configuration(datetime, ntpservice, ntpsrv0, ntpsrv1, ntpsrv_user,
                                                        time_zone_name)
        return self._get_result_code(result)

    # # @brief Enables remote access
    #
    def enable_remote_access(self):
        result = self._rest.enable_remote_access()
        return self._get_result_code(result)

    # # @brief Disables remote access
    #
    def disable_remote_access(self):
        result = self._rest.disable_remote_access()
        return self._get_result_code(result)

    # # @brief Returns device info
    #  @return:
    #    XML Response Example
    #    \verbatim
    #    <device>
    #        <device_id></device_id>
    #        <device_type>8</device_type>
    #        <communication_status>disabled</communication_status>
    #        <remote_access>true</remote_access>
    #        <local_ip></local_ip>
    #        <default_ports_only>true</default_ports_only>
    #        <manual_port_forward>FALSE</manual_port_forward>
    #        <manual_external_http_port></manual_external_http_port>
    #        <manual_external_https_port></manual_external_https_port>
    #        <internal_port>80</internal_port>
    #        <internal_ssl_port>443</internal_ssl_port>
    #    </device>
    #    \endverbatim
    #
    def get_device_info(self):
        result = self._rest.get_device_info()
        return self._get_result_code(result), result

    # # @brief set_cloud_access_connection_options
    #@param connectivity: The connectivity mode to use. Can be one of the following from global_libraries.constants:
    #                                  CLOUD_ACCESS_AUTO, CLOUD_ACCESS_MANUAL, or CLOUD_ACCESS_WINXP
    #@param http_port: For CLOUD_ACCESS_MANUAL, the HTTP port to use
    #@param https_port: For CLOUD_ACCESS_MANUAL, the HTTPSport to use
    #
    def set_cloud_access_connection_options(self, connectivity, http_port=80, https_port=443):
        result = self._rest.set_cloud_access_connection_options(connectivity, http_port, https_port)
        return self._get_result_code(result), result

    # # @brief Retrives one device user or a list to which a given user has access
    #  @details An Admin User can get a list of all Device Users if a username is not provided in the parameter list
    #  @param device_user_id: User id to be retrieved, defaults to 'None'
    #  @param username:       Defaults to 'None'
    #  @param format:         Defaults to 'None'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device_users>
    #       <device_user>
    #          <device_user_id>62580</device_user_id>
    #          <device_user_auth_code>6b8ebf52e99c0f1437d4bd52c84a75dd</device_user_auth_code>
    #          <username>guest</username>
    #          <device_reg_date>1297211862</device_reg_date>
    #          <type></type>
    #          <name></name>
    #          <active></active>
    #          <email>guest@mywebmail.net</email>
    #          <dac>951914401933</dac>
    #          <dac_expiration>1297298315</dac_expiration>
    #          <type_name></type_name>
    #          <application></application>
    #       </device_user>
    #    </device_users>
    #    \endverbatim
    #
    def get_device_user(self, device_user_id=None, username=None, format=None):
        result = self._rest.get_device_user(device_user_id, username, format)
        return self._get_result_code(result), result

    # # @brief Creates a new device user
    #  @param email:      Email address for the new device user
    #  @param user_id:    ID for the device user to be associated
    #  @param sender:     Name of the sender
    #  @param send_email: True/False describes whether to send email or not
    #  @param alias:      Display name for this device on the wd2go Portal
    #  @param format:     Set to 1 to return a DAC
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device_user>
    #        <status>success</status>
    #        <device_user_id>62580</device_user_id>
    #        <device_user_auth>6b8ebf52e99c0f1437d4bd52c84a75dd</device_user_auth>
    #    </device_user>
    #    \endverbatim
    #
    def create_device_user(self, email=None,
                           user_id=None,
                           sender=None,
                           send_email=None,
                           alias=None,
                           format=None):
        result = self._rest.create_device_user(email, user_id, sender, send_email, alias, format)
        return self._get_result_code(result)

    # # @brief Updates a device user
    #  @param device_user_id: id of the user to be updated
    #  @param type: The type of device user
    #            1    iPhone
    #            2    iPad Touch
    #            3    iPad
    #            4    Andoid Phone
    #            5    Andoid Tablet
    #            6    Sync Application
    #  @param name:
    #  @param email:
    #  @param device_user_auth_code:
    #  @param type_name:
    #  @param application:
    #  @param resend_email:
    #  @param is_active:
    #  @param format:
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <users>
    #        <status>success</status>
    #    </users>
    #    \endverbatim
    #
    def update_device_user(self, device_user_id,
                           type,
                           name,
                           email,
                           device_user_auth_code=None,
                           type_name=None,
                           application=None,
                           resend_email=None,
                           is_active=None,
                           format=None):
        result = self._rest.update_device_user(device_user_id, type, name, email, device_user_auth_code, type_name,
                                               application, resend_email, is_active, format)
        return self._get_result_code(result)

    # # @brief Deletes an existing device user
    #  @param device_user_id:        Id of the device user to be deleted
    #  @param device_user_auth_code: Auth code of the device user to be deleted
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <users>
    #       <status>success</status>
    #    </users>
    #    \endverbatim
    #
    def delete_device_user(self, device_user_id, device_user_auth_code):
        result = self._rest.delete_device_user(device_user_id, device_user_auth_code)
        return self._get_result_code(result)

    # # @brief Registers a remote device
    #  @param name: Name of the remote device to be registered, defaults to 'myNAS'
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device>
    #      <status>success</status>
    #    </device>
    #    \endverbatim
    #
    def register_remote_device(self, name='myNAS'):
        result = self._rest.register_remote_device(name)
        return self._get_result_code(result)

    # # @brief Used for updating the device attributes
    #  @details At least one of the seven parameters should be provided.
    #           If manual_port_forward is false, both manual_external_http_port and
    #           manual_external_https_port will be unset. If manual_port_forward is true,
    #           must provide both manual_external_http_port and manual_external_https_port.
    #  @param name:
    #  @param remote_access:
    #  @param default_ports_only:
    #  @param manual_port_forward:
    #  @param manual_external_http_port:
    #  @param manual_external_https_port:
    #  @param format:
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device>
    #      <status>success</status>
    #    </device>
    #    \endverbatim
    #
    def set_remote_device_attributes(self, name=None,
                                     remote_access=None,
                                     default_ports_only=None,
                                     manual_port_forward=None,
                                     manual_external_http_port=None,
                                     manual_external_https_port=None,
                                     format=None):
        result = self._rest.set_remote_device_attributes(name, remote_access, default_ports_only,
                                                         manual_port_forward, manual_external_http_port,
                                                         manual_external_https_port, format)
        return self._get_result_code(result)

    # # @brief Returns media server database status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <media_server_database>
    #      <version>2.2.2.6492C</version>
    #      <time_db_update>1338486474</time_db_update>
    #      <music_tracks>0</music_tracks>
    #      <pictures>0</pictures>
    #      <videos>0</videos>
    #      <scan_in_progress></scan_in_progress>
    #    </media_server_database>
    #    \endverbatim
    #
    def get_media_server_database_status(self):
        result = self._rest.get_media_server_database_status()
        return self._get_result_code(result), result

    # # @brief Configures the media server database
    #  @param database: The media server database configuration setting.
    #                   Acceptable values 'rebuild', 'reset_defaults", 'rescan'.
    #                   Default value is 'rebuild'.
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <media_server_database>
    #      <status>success</status>
    #    </media_server_database>
    #    \endverbatim
    #
    def configure_media_server_database(self, database='rebuild'):
        result = self._rest.configure_media_server_database(database)
        return self._get_result_code(result)

    # # @brief Returns the media server configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <media_server_configuration>
    #        <enable_media_server>true</enable_media_server>
    #    </media_server_configuration>
    #    \endverbatim
    #
    def get_media_server_configuration(self):
        result = self._rest.get_media_server_configuration()
        return self._get_result_code(result), result

    # # @brief Modifies media server configuration
    #  @param enable_media_server: True or False to enable media server
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <media_server_configuration>
    #        <status>success</status>
    #    </media_server_configuration>
    #    \endverbatim
    #
    def set_media_server_configuration(self, enable_media_server):
        result = self._rest.set_media_server_configuration(enable_media_server)
        return self._get_result_code(result)

    # # @brief  Returns the media crawler status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <mediacrawler_status>
    #        <volumes>
    #            <volume>
    #                <volume_id>05348c95:efb3f44f:03916b6c:573be1d0</volume_id>
    #                <volume_state>idle</volume_state>
    #                <categories>
    #                    <category>
    #                       <category_type>videos</category_type>
    #                       <mdate>1402679637</mdate>
    #                       <extracted_count>8</extracted_count>
    #                       <transcoded_count>8</transcoded_count>
    #                       <total>7</total>
    #                    </category>
    #                    <category>
    #                    <category_type>music</category_type>
    #    \endverbatim
    #
    def get_media_crawler_status(self):
        result = self._rest.get_media_crawler_status()
        return self._get_result_code(result), result

    # # @brief Updates media crawler for a share
    #  @param share_name: Name of the share
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <mediacrawler>
    #        <status>success</status>
    #    </mediacrawler>
    #    \endverbatim
    #
    def set_mediacrawler(self, share_name=TEST_SHARE_NAME):
        result = self._rest.set_mediacrawler(share_name)
        return self._get_result_code(result)

    # # @brief Return the meta data of a share
    #  @param share_name: Name of the share to get back
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <metadb_info>
    #    <generated_time>1402699451</generated_time>
    #    <last_purge_time>1400084753</last_purge_time>
    #    <last_updated_db_time>1402679637</last_updated_db_time>
    #    <file>
    #        <path>/Public/FTPTest</path>
    #        <name>FourMethodsOfFlushRiveting.mp4</name>
    #        <size>39266531</size>
    #        <media_type>videos</media_type>
    #        <modified>1397157362</modified>
    #        <deleted>false</deleted>
    #        <status>2</status>
    #        <title>FourMethodsOfFlushRiveting.mp4</title>
    #        <date>-1</date>
    #        <duration>572</duration>
    #        <stars/>
    #        <director/>
    #        <writer/>
    #        <genre/>
    #        <cover_art>true</cover_art>
    #    </file>
    #    \endverbatim
    #
    def get_meta_data(self, share_name=TEST_SHARE_NAME):
        result = self._rest.get_meta_data(share_name)
        return self._get_result_code(result), result

    # # @brief Retrieves the meta data summary of a share
    #  @param share_name: Name of the share data to be retrieved
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <metadb_summary>
    #        <file_count>10</file_count>
    #        <size>96189380</size>
    #        <path>/Public</path>
    #    </metadb_summary>
    #    \endverbatim
    #
    def get_meta_data_summary(self, share_name=TEST_SHARE_NAME):
        result = self._rest.get_meta_data_summary(share_name)
        return self._get_result_code(result), result

    # # @brief Retrieves miocrawler status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <miocrawler_status>
    #        <mdate>1402697708</mdate>
    #        <desc/><etype>0</etype>
    #        <mc_version>2.0</mc_version>
    #    </miocrawler_status>
    #    \endverbatim
    #
    def get_miocrawler_status(self):
        result = self._rest.get_miocrawler_status()
        return self._get_result_code(result), result

    # # @brief Retrieves miodb
    #  @param compress: Whether or not the data will be compressed, defaults to true
    #
    def get_mio_data(self, compress=True):
        result = self._rest.get_mio_data(compress)
        return self._get_result_code(result), result

    # # @brief gets the RAID drives status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <raid_drives_status>
    #        <status>drive_raid_already_formatted</status>
    #        <status_desc>RAID is already formatted</status_desc>
    #        <busy_drive_location></busy_drive_location>
    #        <drives>
    #            <partition_uuid>05348c95:efb3f44f:03916b6c:573be1d0</partition_uuid>
    #            <drive>
    #                <location>1</location>
    #                <model>WDC WD30EFRX-68AX9N0</model>
    #                <serial_number>WD-WCC1T1604614</serial_number>
    #                <drive_size>3000592982016</drive_size>
    #                <valid_drive_list>allowed</valid_drive_list>
    #                <smart_status/><raid_mode>5</raid_mode>
    #                <removable>1</removable>
    #            </drive>
    #    \endverbatim
    #
    # noinspection PyPep8Naming
    def get_RAID_drives_status(self):
        result = self._rest.get_RAID_drives_status()
        return self._get_result_code(result), result

    # # @brief Returns the number of installed drives
    # # Please remember that the get_RAID_drives_status does not return value for SQ and GLCR
    # # So, I added the IF statement when number of drives less than 2 (Static return) - O.K.
    def get_drive_count(self):
        if self.uut[Fields.number_of_drives] > 1:
            status, result = self.get_RAID_drives_status()
            drive_list = self.get_xml_tags(result, 'drive')
            return len(drive_list)
        else:
            return 1

    def wait_for_raid_to_sync(self, number_of_volumes=1, show_progress=True, timeout=300):
        """ Waits until the RAID sync is complete for all volumes

        :param number_of_volumes: The number of volumes to check
        :param show_progress: If True, the % complete goes to the console. If False, it goes to the debug log
        :param timeout: The amount of time to wait with no increase in the progress percentage before timing out
        """
        self._ssh.wait_for_raid_to_sync(number_of_volumes=number_of_volumes,
                                        show_progress=show_progress,
                                        timeout=timeout)

    def usb_backup_usage(self, source_path='', dest_path='', job_name='job1',
                         mode='1', device_type=1, direction=1, usage='jobadd'):
        """ @brief usb backup usage

        :param source_path: path of source
        :param dest_path: path of destination
        :param job_name: name of backup job
        :param mode: {1:copy, 2:sync, 3:Incremental}
        :param device_type: {1:storage, 2:mtp}
        :param direction: {1:USB -> NAS, 2: NAS -> USB}
        :param usage: [jobadd/jobedit/jobdel/jobrun/jobstop/jobrs/jobrs_list]

        """
        self._ssh.usb_backup_usage(source_path=source_path, dest_path=dest_path, job_name=job_name,
                                   mode=mode, device_type=device_type, direction=direction, usage=usage)

    # # @brief Retrieves user RAID status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <user_raid_status>
    #        <raid_status>GOOD</raid_status>
    #    </user_raid_status>
    #    \endverbatim
    #
    # noinspection PyPep8Naming
    def get_user_RAID_status(self):
        result = self._rest.get_user_RAID_status()
        return self._get_result_code(result), result

    # # @brief Returns the RAID configuration status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <user_raid_status>
    #        <raid_status>GOOD</raid_status>
    #    </user_raid_status>
    #    \endverbatim
    #
    def get_raid_configuration_status(self):
        # get_raid_configuration_status presently returning a 404 error. Alpha has been contacted.
        result = self._rest.get_raid_configuration_status()
        return self._get_result_code(result), result

    # # @ Wait for the system is busy when Remove/Insert HDD drives.
    def wait_until_HDD_busy_is_ready(self):
        self.log.info('Waiting up to 80 seconds for the drive to become busy')
        timer = Stopwatch(timer=80)
        timer.start()

        while True:
            drives_status = self.get_RAID_drives_status()
            drives_status1 = self.get_xml_tags(drives_status, 'status')
            self.log.debug('Raid Drives Status = {}'.format(drives_status1))

            if drives_status1[0] == 'busy':
                self.log.info('Drive took {} seconds to become busy'.format(timer.get_elapsed_time()))
                break
            else:
                if timer.is_timer_reached():
                    self.log.warning('Drive never became busy')
                    break
                else:
                    time.sleep(2)

        self.log.info('Waiting up to 250 seconds for drive to become ready')
        timer = Stopwatch(timer=250)
        timer.start()

        while True:
            drives_status = self.get_RAID_drives_status()
            drives_status1 = self.get_xml_tags(drives_status, 'status')
            self.log.debug('Raid Drives Status = {}'.format(drives_status1))

            if drives_status1[0] not in 'busy':
                self.log.info('Drive took {} seconds to become ready'.format(timer.get_elapsed_time()))
                break
            else:
                if timer.is_timer_reached():
                    raise exception.ActionTimeout('Drive is still busy')
                else:
                    time.sleep(2)

    # # @brief Post RAID configuration status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <user_raid_status>
    #        <raid_status>GOOD</raid_status>
    #    </user_raid_status>
    #    \endverbatim
    #
    def raid_configuration_status(self):
        result = self._rest.raid_configuration_status()
        return self._get_result_code(result), result

    # # @brief Create directory
    #  @param share_name: Name of the share for the directory to be created on
    #  @param dir_path: The path of the dir on the share
    #
    def create_directory(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        result = self._rest.create_directory(share_name, dir_path)
        return self._get_result_code(result), result

    # # @brief Retrieves directories of a share given the share name
    #  @param share_name: name of the share to be retrieved
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <dir>
    #        <entry>
    #            <is_dir>true</is_dir>
    #            <path>/Public</path>
    #            <name>Shared Pictures</name>
    #            <mtime>1403204736</mtime>
    #        </entry>
    #        <entry>
    #            <is_dir>true</is_dir>
    #            <path>/Public</path>
    #            <name>Shared Music</name>
    #            <mtime>1403204736</mtime>
    #        </entry>
    #        <entry>
    #            <is_dir>true</is_dir>
    #            <path>/Public</path>
    #            <name>Shared Videos</name>
    #            <mtime>1403204736</mtime>
    #        </entry>
    #    </dir>
    #    \endverbatim
    #
    def get_directory(self, share_name=TEST_SHARE_NAME):
        result = self._rest.get_directory(share_name)
        return self._get_result_code(result), result

    # # @brief Retrieves the contents of a directory of a share given a share name and dir path
    #  @param share_name: Name of the share to be retrieved
    #  @param dir_path:   Name of the dir path to the directory to be retrieved
    #
    def get_directory_contents(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        result = self._rest.get_directory_contents(share_name, dir_path)
        return self._get_result_code(result), result

    # # @brief Retrieves directory contents
    #
    def get_dir_contents(self):
        result = self._rest.get_dir_contents()
        return self._get_result_code(result), result

    # # @brief Returns the free space of a given directory in bytes
    #  @param directory: The directory to check
    def get_directory_freespace(self, directory):
        platform = sys.platform

        if platform.startswith('linux'):
            filesystem = os.statvfs(directory)
            freebytes = filesystem.f_bavail * filesystem.f_frsize
        else:
            free = ctypes.c_ulonglong(0)
            ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(directory), None, None, ctypes.pointer(free))
            freebytes = free.value

        return freebytes

    def get_share_freespace(self, sharename):
        """ Returns the number of bytes free on a particular share

        :param sharename: The share to check
        :return: Number of bytes free
        """

        status, output = self.execute('df -kP -B 1 /shares/{}'.format(sharename))
        data_line = output.split('\n')[1]
        free_bytes = int(data_line.split()[3])

        return free_bytes

    # # @brief Delete a directory within a share given share and directory name
    #  @param share_name: Name of the share containing the directory
    #  @param dir_path:   Path to the directory to be deleted
    #
    def delete_directory(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        result = self._rest.delete_directory(share_name, dir_path)
        return self._get_result_code(result)

    # # @brief Copy a directory from one share to another
    #  @param source_share_name: Name of the source share
    #  @param source_dir_path:   Name of the source directory
    #  @param dest_share_name:   Name of the destination share
    #  @param dest_dir_path:     Name of the destination directory
    #
    def copy_directory(self, source_share_name=TEST_SHARE_NAME, source_dir_path='test_dir',
                       dest_share_name=TEST_SHARE_NAME, dest_dir_path='test_dir2'):
        result = self._rest.copy_directory(source_share_name, source_dir_path, dest_share_name, dest_dir_path)
        return self._get_result_code(result)

    # # @brief Retrieve directory info inside a share, given directory and share name
    #  @param share_name: Name of the share containing the dir_path
    #  @param dir_path:   The path of the directory within the share
    #
    def get_directory_info(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        result = self._rest.get_directory_info(share_name, dir_path)
        return self._get_result_code(result)

    # # @brief Retrieve file info inside a share, given share and file name
    #  @param share_name: Name of the share containing the file
    #  @param file_name:  The name of the file inside the share
    #
    def get_file(self, share_name=TEST_SHARE_NAME, file_name='test_file'):
        result = self._rest.get_file(share_name, file_name)
        return self._get_result_code(result)

    # # @brief Deletes file info inside a share, given directory and file name
    #  @param share_name: Name of the share containing the file
    #  @param file_name:  The name of the file to be deleted
    #
    def delete_file(self, share_name=TEST_SHARE_NAME, file_name='test_file'):
        result = self._rest.delete_file(share_name, file_name)
        return self._get_result_code(result)

    def mount_share(self,
                    share_name,
                    protocol=None,
                    user=None,
                    password=None,
                    read_only_ok=False,
                    enable_protocol=True):
        """ Mounts the specified share

        :param share_name: The name of the share on the UUT
        :param protocol: The protocol to use (use self.Share.<protocol). Defaults to Samba
        :param user: Optional username to use
        :param password: Optional password
        :param read_only_ok: If the share mounts as read only and read_only_ok is False, raise an exception
        :param enable_protocol: If True, enable the selected protocol. For Samba, this does nothing. For NFS and
                                AFP, it enables the protocol in the Network Services. For NFS, it also enables
                                NFS for the selected share.
        :return: The mountpoint
        """
        if not protocol:
            protocol = self.Share.samba

        if protocol in self._mountpoints:
            shares = self._mountpoints[protocol]
        else:
            shares = {}

        if share_name in shares:
            # Already mounted, so just return the mountpoint
            return shares[share_name].mountpoint
        else:
            if protocol == self.Share.nfs and self.uut[Fields.product] == 'Lightning':
                self.log.error('Lightning does not support NFS service')
                return
            if protocol == self.Share.nfs:
                if enable_protocol:
                    self.execute('nfs start')
                    self.enable_nfs_share_access(share_name)
            elif protocol == self.Share.afp:
                if enable_protocol:
                    self.execute('afp start')

            share = self.Share(protocol=protocol,
                               hostname=self.uut[self.Fields.internal_ip_address],
                               sharename=share_name,
                               user=user,
                               password=password,
                               read_only_ok=read_only_ok)

            shares[share_name] = share
            self._mountpoints[protocol] = shares

            return share.mountpoint

    def unmount_share(self, share_name, protocol=None):
        if not protocol:
            protocol = self.Share.samba

        if protocol in self._mountpoints:
            shares = self._mountpoints[protocol]

            if share_name in shares:
                share = shares.pop(share_name, None)
                if share:
                    share.unmount()

                    # Copy the shares dictionary back to the protocol dictionary
                    self._mountpoints[protocol] = shares
                    return

        self._tc_log.debug('Could not unmount {} share of {} because it is not mounted'.format(protocol, share_name))

    def _unmount_all(self):
        for protocol in self._mountpoints:
            shares = self._mountpoints[protocol]

            for share_name in shares:
                share = shares[share_name]
                try:
                    share.unmount()
                except exception.ShareNotMounted:
                    self._tc_log.warning('Could not unmount {} share: {}'.format(protocol, share_name))
                    pass

        self._mountpoints = {}

    # # @brief Copy a file from one share to another
    #  @param source_share_name: Name of the source share
    #  @param source_file:       Name of the source file to be copied
    #  @param dest_share_name:   Name of the destination share
    #  @param dest_file:         Name of the destination file
    #
    def copy_file(self,
                  source_file,
                  dest_file=None,
                  source_path='',
                  destination_path='',
                  source_share_name=None,
                  dest_share_name=None,
                  source_user=None,
                  source_password=None,
                  source_protocol=None,
                  dest_user=None,
                  dest_password=None,
                  dest_protocol=None,
                  **kwargs):
        """ Copies a file, mounting a share if necessary

        :param source_file: The source file, without the path
        :param dest_file: The destination file, without the path

        :param source_path: The path of the source file. If source_share_name is specified, this is relative to the
                            mount point. If not, it is used as-is.
        :param source_share_name: If passed, the specified share is mounted for the source file
        :param source_protocol: The protocol to use when mounting the source mount point
        :param source_user: The user to use when mounting the souce share. Default is anonymous.
        :param source_password: The password to use when mounting the source share.

        :param destination_path: The path of the destination file. If destination_share_name is specified,
                                 this is relative to the mount point. If not, it is used as-is.
        :param dest_share_name: If passed, the specified share is mounted for the destination file
        :param dest_protocol: The protocol to use when mounting the destination mount point
        :param dest_user: The user to use when mounting the destination share. Default is anonymous.
        :param dest_password: The password to use when mounting the destination share.

        :return: Copy speed in MiB/second
        """
        # If a legacy test passes user or password use it for both source and destination, but only if
        # the source and destination user or password is not passed as well.
        obsolete_args = ['user', 'password']

        user, password = ctl.kwargs_to_args(obsolete_args, kwargs)

        if source_user is None:
            source_user = user

        if source_password is None:
            source_password = password

        if dest_user is None:
            dest_user = user

        if dest_password is None:
            dest_password = password

        if source_share_name:
            # Copying from a network share, so mount it
            source_path = os.path.join(self.mount_share(source_share_name,
                                                        user=source_user,
                                                        password=source_password,
                                                        protocol=source_protocol),
                                       source_path)

        source_pattern = os.path.join(source_path, source_file)

        if dest_share_name:
            # Copying to a network share
            dest_path = self.mount_share(dest_share_name,
                                         user=dest_user,
                                         password=dest_password,
                                         protocol=dest_protocol)
            relative_path = destination_path
            destination_path = os.path.join(dest_path, relative_path)
        else:
            relative_path = ''

        # result = self._rest.copy_file(source_share_name, source_file, dest_share_name, dest_file)
        # return self._get_result_code(result)
        self._tc_log.debug('Copying the following: ')
        self._tc_log.debug('Source pattern = {}'.format(source_pattern))
        self._tc_log.debug('Destination path = {}'.format(destination_path))
        self._tc_log.debug('Destination file = {}'.format(dest_file))

        rate = 0
        files_copied = 0
        for next_file in glob.glob(source_pattern):
            if dest_file is None:
                source_pathname, source_filename = os.path.split(next_file)
                if source_pathname == destination_path:
                    # They are the same, so prepend "Copy_of_" to destination filename
                    destination_filename = 'Copy_of_{}'.format(source_filename)
                else:
                    destination_filename = source_filename
            else:
                destination_filename = dest_file

            speed = wdlfs.copyfile(source_file=next_file,
                                   destination_path=destination_path,
                                   dest_filename=destination_filename)
            if speed:
                rate += speed
            files_copied += 1
            # Track the copy
            if dest_share_name:
                # Store it as a network share file
                file_info = (dest_share_name, os.path.join(relative_path, destination_filename))
                self._files_copied_network.append(file_info)

                path_info = (dest_share_name, relative_path)
                self._paths_created_network.append(path_info)
            else:
                self._files_copied.append(os.path.join(destination_path, destination_filename))

        if files_copied > 0:
            return rate / (files_copied * 1.0)
        else:
            return 0

    @staticmethod
    def delete_dir_local(path, delete_contents=False):
        """ Deletes the directory specified by path

        :param path: The directory to delete
        :param delete_contents: If True, deletes even if the directory isn't empty. Default is False.
        """
        wdlfs.delete_path(path=path, delete_contents=delete_contents)

    @staticmethod
    def delete_file_local(filename):
        """ Deletes the specified file

        :param filename: The file to delete
        """
        wdlfs.delete_file(filename=filename)

    # # @brief Retrieves file contents
    #  @param share_name: Name of the share containing the file to be retrieved
    #  @param file_name:  Name of the file to be retrieved
    #
    def get_file_contents(self, share_name=TEST_SHARE_NAME, file_name='test_file'):
        result = self._rest.get_file_contents(share_name, file_name)
        return self._get_result_code(result)

    # # @brief Downloads a file
    #  @param share_name: Name of the share containing the file
    #  @param file_name:  Name of the file to be downloaded
    #  @param saved_file: Name of the saved file
    #
    def download_file(self, share_name=TEST_SHARE_NAME, file_name='test_file', saved_file='save_file'):
        result = self._rest.download_file(share_name, file_name, saved_file)
        return self._get_result_code(result)

    # # @brief Uploads a file to the device
    #  @details Note: File uploads are limited to 1GB. Any larger files must use chunked PUT data.
    #  @param share_name:  Name of the share to contain the file
    #  @param dir_name:    Directory name where the file will be contained
    #  @param file_name:   Name of the file to be uploaded
    #  @param format:      Format of the device
    #  @param create_path: True or False, create the dir_path if it doesn't already exist
    #
    def upload_file_contents(self, share_name=TEST_SHARE_NAME, dir_name='test_dir', file_name='test_file', format='xml',
                             create_path=False):
        result = self._rest.upload_file_contents(share_name, dir_name, file_name, format, create_path)
        return self._get_result_code(result)

    # # @brief Modifies or replaces an existing file
    #  @details Note: File uploads are limited to 1GB. Any larger files must use chunked PUT data.
    #  @param share_name:  Name of the share to contain the file
    #  @param dir_name:    Directory name where the file will be contained
    #  @param file_name:   Name of the file to be uploaded
    #  @param format:      Format of the device
    #  @param create_path: True or False, create the dir_path if it doesn't already exist
    #
    def set_file_contents(self, share_name=TEST_SHARE_NAME, dir_name='test_dir', file_name='test_file', format='xml',
                          create_path=False):
        result = self._rest.set_file_contents(share_name, dir_name, file_name, format, create_path)
        return self._get_result_code(result)

    # # @brief Retrieves file info inside a directory of a share, given share, directory and file name
    #  @param share_name: Name of the share to contain the file
    #  @param dir_path:   Directory name where the file will be contained
    #  @param file_name:  Name of the file to be uploaded
    #    XML Response Example:
    #    \verbatim
    #    <files>
    #        <file>
    #            <path/>
    #            <name>AFP-74BD.mp4</name>
    #            <size>52347989</size>
    #            <modified>2011-07-20 11:40:49</modified>
    #            <last_updated>2014-06-13 08:48:48</last_updated>
    #            <deleted>false</deleted>
    #        </file>
    #    </files>
    #    \endverbatim
    #
    def get_file_info(self, share_name=TEST_SHARE_NAME, dir_path='test_dir', file_name='test_file'):
        result = self._rest.get_file_info(share_name, dir_path, file_name)
        return self._get_result_code(result)

    # # @brief Retrieves language configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <language_configuration>
    #        <language>en_US</language>
    #    </language_configuration>
    #    \endverbatim
    #
    def get_language_configuration(self):
        result = self._rest.get_language_configuration()
        language = self.get_xml_tags(result, 'language')
        return self._get_result_code(result), language

    # # @brief Modifies language configuration
    #  @details System restore before this can be executed
    #  @param language: language to be configured to
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <language_configuration>
    #        <language>en_US</language>
    #    </language_configuration>
    #    \endverbatim
    #
    def set_language_configuration(self, language='en_US'):
        result = self._rest.set_language_configuration(language)
        return self._get_result_code(result)

    # # @brief Updates language configuration
    #  @param language: Language to be updated to
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <language_configuration>
    #        <status>Success</status>
    #    </language_configuration>
    #    \endverbatim
    #
    def update_language_configuration(self, language='en_US'):
        result = self._rest.update_language_configuration(language)
        return self._get_result_code(result)

    # # @brief Retrieves eula status
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <eula_acceptance>
    #        <accepted>yes</accepted>
    #    </eula_acceptance>
    #    \endverbatim
    #
    def get_eula_status(self):
        result = self._rest.get_eula_status()
        try:
            error_message = self.get_xml_tag(xml_content=result.content, tag='error_message')
        except RestError:
            self._tc_log.debug('Rest error when reading XML tag. EULA not accepted.')
            return 0

        if result.status_code == 404 and str(error_message) == "EULA not accepted":
            self._tc_log.debug('Got a 404, but that is expected because the EULA is not accepted yet.')
            return 0, result
        else:
            return 1, result

    # # @brief Method to delete a individual share
    #  @param share_name: Name of share
    def delete_share(self, share_name=TEST_SHARE_NAME):
        result = self._rest.delete_share(share_name)
        return self._get_result_code(result)

    # # @brief if use_UI is True then accepts the EULA through REST Call
    # # @brief if use_UI is False then accepts the EULA though Selenium web UI
    #  @return when use_UI is FALSE:
    #    XML Response Example:
    #    \verbatim
    #     <eula_acceptance>
    #        <accepted>success</accepted>
    #    </eula_acceptance>
    #    \endverbatim
    #  @return when use_UI is True:
    #  The returned values can be: None, 0, 1 and 2
    #  "0" means : logged in to the UI, EULA page found and accepted and Main Content on the main page appeared
    #  "1" means : Logged in to the UI, but no EULA could be found
    #  "None" means: Failed to log in or validate the presence of one of the elements
    #                (such as login button, EUla checkbox, ....)
    #  "2" means : Logged in to the UI, EULA page found and accepted and now Mandatory Firmware update menu is shown up
    # noinspection PyPep8Naming
    def accept_eula(self, use_UI=False):
        if use_UI:
            result = self._sel.accept_eula()
            return result
        else:
            result = self._rest.accept_eula()
            return 0, self._get_result_code(result)

    # # @brief Retrieves a device registration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #     <device_Registration>
    #        <registered>success</registered>
    #    </device_registration>
    #    \endverbatim
    #
    def get_device_registration(self):
        result = self._rest.get_device_registration()
        return self._get_result_code(result)

    # # @brief Modifies a device registration
    #  @param registered: True or False, whether the device is registered or not
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device_registration>
    #        <status>success</status>
    #    </device_registration>
    #    \endverbatim
    #
    def set_device_registration(self, registered):
        result = self._rest.set_device_registration(registered)
        return self._get_result_code(result)

    # # @brief Registers a new device
    #  @param country: Country the device is registered in, default is 'US'
    #                  \verbatim
    #                  ISO country code from OS
    #                  \endverbatim
    #  @param lang:    Language the device is registered in, default is 'ENG'
    #                  \verbatim
    #                  3 Letter language code
    #                  CHS - Chinese
    #                  CHT - Simplified Chinese
    #           CZE or CES - Czech
    #           DUT or NLD - Dutch
    #                  ENG - English
    #                  FRA - French
    #                  DEU - German
    #                  HUN - Hungarian
    #                  ITA - Italian
    #                  JPN - Japanese
    #                  KOR - Korean
    #                  NOR - Norwegian
    #           PLK or POL - Polish
    #                  PBR - Portuguese Brazil
    #                  RUS - Russian
    #                  ESN - Spanish
    #                  SWE - Swedish
    #                  TUR - Turkish
    #                  \endverbatim
    #  @param first:   First name of person the device is registered to, default is 'test'
    #  @param last:    Last name of the person the device is registered to, default is 'test'
    #  @param email:   Email address of the person the device is registered to, default is 'test@test.com'
    #  @param option:  'yes' or 'no', default is no
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device_registration>
    #        <status>success</status>
    #    </device_registration>
    #    \endverbatim
    #
    def device_registration(self, country='US', lang='ENG', first='test', last='test', email='test@test.com',
                            option='yes'):
        result = self._rest.device_registration(country, lang, first, last, email, option)
        return self._get_result_code(result)

    # # @brief Retrieves device description
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <?xml version="1.0" encoding="utf-8"?>
    #    <device_description>
    #      <machine_name>wdvenue</machine_name>
    #      <machine_desc>My Book Live Network Storage</machine_desc>
    #    </device_description>
    #    \endverbatim
    #
    def get_device_description(self):
        result = self._rest.get_device_description()
        return self._get_result_code(result), result

    # # @brief Modifies device description
    #  @param machine_name: New name of the machine
    #                       \verbatim
    #                       The machine name may only contain ASCII letters 'A' through 'Z'
    #                       'a' to 'z', the digits '0' through '9', and the hyphen '-'. The
    #                       machine name may only be between 1 and 16 characters long.
    #                       \endverbatim
    #  @param machine_desc: New description of the machine
    #                       \verbatim
    #                       Device description must begin with alphanumeric and be no more than
    #                       42 characters long.
    #                       \endverbatim
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <device_description>
    #        <status>success</status>
    #    </device_description>
    #    \endverbatim
    #
    def set_device_description(self, machine_name, machine_desc):
        result = self._rest.set_device_description(machine_name, machine_desc)
        return self._get_result_code(result)

    # # @brief Retrieves FTP Server Configuration
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <network_services_configuration>
    #        <enable_ftp>true</enable_ftp>
    #    </network_services_configuration>
    #    \endverbatim
    #
    # noinspection PyPep8Naming
    def get_FTP_server_configuration(self):
        result = self._rest.get_FTP_server_configuration()
        return self._get_result_code(result), result

    # # @brief Modifies FTP server configuration
    #  @param enable_ftp: True or False
    #  @return:
    #    XML Response Example:
    #    \verbatim
    #    <network_services_configuration>
    #        <status>success</status>
    #    </network_services_configuration>
    #    \endverbatim
    #
    # noinspection PyPep8Naming
    def set_FTP_server_configuration(self, enable_ftp):
        result = self._rest.set_FTP_server_configuration(enable_ftp)
        return self._get_result_code(result)

    # # @brief Enable the FTP service
    #
    def enable_ftp_service(self):
        return self._sel.enable_ftp_service()

    # # @brief Enable FTP Share access
    #
    def enable_ftp_share_access(self, share_name=utilities.TEST_SHARE_NAME, access='Anonymous None'):
        return self._sel.enable_ftp_share_access(share_name, access)

    # # @brief Enable Media Streaming
    #
    def toggle_media_streaming(self, enable_streaming=True):
        return self._sel.toggle_media_streaming(enable_streaming)

    # # @brief Enable Share Media Serving
    #
    def enable_media_serving(self, share_name):
        return self._sel.enable_media_serving(share_name)

    # # @brief Runs a random test keyword
    #  @param *args:
    #  @param **kwargs:
    #
    def random_rest_calls(self, *args, **kwargs):
        return self._rest.random_rest_calls(args, kwargs)

    # # @brief Returns all instances of the given tag in the given XML tree
    #  @param xml_content: The XML tree to be searched for the tag
    #  @param tag:         The tag to be retrieved
    #  @return All instances of the given tag in the given XML tree
    #
    def get_xml_tags(self, xml_content, tag):
        return self._rest.get_xml_tags(xml_content, tag)

    # # @brief Returns the first instance of the given tag in the given XML tree
    #         Note: make sure to use .content when getting xml tag
    #  @param xml_content: The XML tree to be searched for the tag
    #  @param tag:         The tag to be retrieved
    #  @param verbose:     Enables debug text, default is True
    #  @return  Only the first instance of the given tag will be retrieved.
    #           If you want all the tags use get_xml_tags.
    #
    def get_xml_tag(self, xml_content, tag, verbose=True):
        return self._rest.get_xml_tag(xml_content, tag, verbose)

    # # @brief Returns a dictionary of all the tags and their content in the given XML Tree
    #  @param xml_content: The XML content to be parse
    #  @param flat:        True or False, defaults to True. If flat, returns only
    #  @return: A dictionary containing all of tags in the XML tree and their contents
    #
    def get_xml_children_dict(self, xml_content, flat=True):
        return self._rest.get_xml_children_dict(xml_content, flat)

    # # @brief Deletes all shares
    def delete_all_shares(self):
        '''
        Deletes all shares with the exception of Public, SmartWare, TimeMachineBackup and USB shares
        '''
        self.log.debug('Deletes all shares except for default shares.')
        default_shares = ['Public', 'SmartWare', 'TimeMachineBackup']
        usb_info = self.get_usb_info()
        if usb_info[0] != 0:
            self.log.warning('Failed to get USB info from Rest API! Error code: {}'.format(usb_info[0]))
        else:
            usb_shares = self.get_xml_tags(usb_info[1], 'share')
            if usb_shares:
                default_shares += usb_shares
        share_info = self.get_all_shares()
        if share_info[0] != 0:
            raise exception.RestCallUnsuccessful('Failed to get shares info from Rest API! error code:{}'.format(share_info[0]))
        else:
            all_shares = self.get_xml_tags(share_info[1], 'share_name')
            delete_shares = list(set(all_shares) - set(default_shares))
            for share in delete_shares:
                self.log.debug('Deleting share folder: {}'.format(share))
                self._ssh.delete_share(share)
        '''
            The original ssh api below will erase shares info in databases
            but will not delete the folders in each volume.
        '''
        # return self._ssh.delete_all_shares()

    # # @brief Gets raid mode as a string
    # Always returns JBOD for a single drive device
    def get_raid_mode(self):
        if self.uut[Fields.number_of_drives] > 1:
            return self._rest.get_raid_mode()
        else:
            return 'JBOD'

    # # @brief Gets raid mode as a number
    #                0-10 for RAID mode 0-1,
    #                11 for JBOD,
    #                12 for spanning
    # Always returns 11 (JBOD) for a single drive device

    def get_raid_level(self, raid_mode=None):
        if raid_mode is None:
            raid_mode = self.get_raid_mode()
        return ctl.raid_mode_to_level(raid_mode)

    # ************************************************************************************************************#
    #    SSH API function wrappers                                                                               #
    # ************************************************************************************************************#

    # # @brief Establish SSH connection with test unit
    #  Raises an exception on failure
    #
    def ssh_connect(self, username=None, password=None, port=None, server_ip=None):
        try:
            self._ssh.connect(username=username, password=password, port=port, server_ip=server_ip)
        except SocketError:
            # Enable SSH and retry
            self.set_ssh(enable=True)
            self._ssh.connect(username=username, password=password, port=port, server_ip=server_ip)

    # # @brief Disconnects SSH connection
    #  @return: Exception is thrown if failed
    #
    def ssh_disconnect(self):
        self._ssh.disconnect()

    # # @brief Method to start AFP service on test unit
    #  @return: Exception is thrown if failed
    #
    def test_start_afp_service(self):
        self._ssh.start_afp_service()

    # # @brief Method to stop AFP service on test unit
    #  @return: Exception is thrown if failed
    #
    def stop_afp_service(self):
        self._ssh.stop_afp_service()

    # # @brief Method to restart AFP service on test unit
    #  @return: Exception is thrown if failed
    #
    def restart_afp_service(self):
        self._ssh.restart_afp_service()

    # # @brief Method to stop service on test unit
    #  @return: Exception is thrown if failed
    #
    def stop_services(self):
        self._ssh.stop_services()

    # # @brief Method to create zero file on test unit
    #  @return: 0 is returned if successful
    #
    def create_zero_file(self, destination, filename='output.dat', blocksize=1024, count=1024):
        self._ssh.create_zero_file(destination, filename, blocksize, count)

    # # @brief Method to create random file on test unit
    #  @return: 0 is returned if successful
    #
    def create_random_file(self, destination, filename='output.dat', blocksize=1024, count=1024):
        self._ssh.create_random_file(destination, filename, blocksize, count)

    # # @brief Method to format USB drive attached to test unit
    #  @return: 0 is returned if successful
    #
    def format_usb_drive(self, drive):
        self._ssh.format_usb_drive(drive)

    # # @brief Method to mount a USB drive
    #  @return: Exception is thrown if failed
    #
    def mount_drive(self, drive, mountpoint):
        self._ssh.mount_drive(drive, mountpoint)

    # # @brief Method to get unmount USB drive attached to test unit
    #  @return: Exception is thrown if failed
    #
    def unmount_drive(self, drive):
        self._ssh.unmount_drive(drive)

    # # @brief Method to get USB drive attached to test unit
    #  @return: Exception is thrown if failed
    #
    def find_usb_drive(self):
        return self._ssh.find_usb_drive()

    # # @brief Method to find SATA drive on test unit
    #  @return: Exception is thrown if failed
    #
    def find_sata_drive(self):
        return self._ssh.find_sata_drive()

    # # @brief Method to set unit's time
    #  @param new_time: New time
    #                   \verbatim
    #                   Recognized TIME format:
    #                   [[[[[YY]YY]MM]DD]hh]mm[.ss]
    #                   \endverbatim
    #  @return: Exception is thrown if failed
    #
    def set_uut_time(self, new_time):
        self._ssh.set_uut_time(new_time)
        self._tc_log.debug('Set UUT time successfully')

    # # @brief Method to retrieve platform name
    #  @return: Returns platform name
    #  Exception is thrown if failed
    #
    def get_platform(self):
        return self._ssh.get_platform()

    # # @brief Method to retrieve model number
    #  @return: Returns model number
    #  Exception is thrown if failed
    #
    def get_model_number(self, use_rest=False):
        if self.uut[Fields.product] == 'Sequoia':
            use_rest = True
        if use_rest:
            xml_response = self.get_system_information()
            return self.get_xml_tag(xml_response[1], 'model_number')
        else:
            try:
                retval = self._ssh.get_model_number()
            except ValueError:
                self.log.debug('Retrying (Get Model Number)')
                retval = self._ssh.get_model_number()
        return retval

    # # @brief Method to retrieve unit's current uptime
    #  @return: Returns the time in HH:MM:SS
    #  Exception is thrown if failed
    #
    def get_uptime(self):
        return self._ssh.get_uptime()

    # # @brief Method to retrieve SSH username from test unit
    #  @return: Returns the username of the test unit and compares it to the expected value of 'sshd'
    #
    def get_ssh_username(self):
        return self._ssh.get_ssh_username()

    # # @brief Method to retrieve SSH password from the test unit
    #  @return: Returns the password which is then compared to expected value of 'welc0me'
    #
    def get_ssh_password(self):
        return self._ssh.get_ssh_password()

    # # @brief Method to modify user account
    #  @param user:      Username of the user to be modified
    #  @param passwd:    Password of the user to be modified
    #  @param fullname:  Modified full name, defaults to 'none'
    #  @return: Exception is thrown if failed
    #
    def modify_user_account(self, user, passwd=None, fullname=None):
        self._ssh.modify_user_account(user=user, passwd=passwd, fullname=fullname)

    # # @brief Method to execute a passed in command
    #  @param command: The command to be executed
    #  @param timeout: The amount of time to wait for the command to finish. If set to a negative value,
    #                  then it executes in the background
    #  @return: Return value is compared to verify if it's executed successfully or not
    #
    def execute(self, command, timeout=60, enable_ssh=True):
        try:
            retval = self._ssh.execute(command, timeout=timeout)
        except SocketError:
            if enable_ssh:
                # Enable SSH and retry
                self.set_ssh(enable=True)
                retval = self._ssh.execute(command, timeout=timeout)
            else:
                raise

        return retval

    # # @brief Method to remove a drive from the test unit
    #  @param drive_list: Number of drives to remove from the test unit, Range 1~4, param can be a list.
    #
    def remove_drives(self, drive_list=None):
        self._ssh.remove_drives(drive_list=drive_list)
        self.wait_until_HDD_busy_is_ready()

    # # @brief Method to insert a drive from the test unit
    #  @param drive_list: Number of drives to insert from the test unit, Range 1~4, param can be a list.
    #
    def insert_drives(self, drive_list=None):
        self._ssh.insert_drives(drive_list=drive_list)
        self.wait_until_HDD_busy_is_ready()

    def open_scp_connection(self):
        """
        Open a new SCP connection to the test unit
        """
        return self._ssh.open_scp_connection()

    def scp_from_device(self, path, is_dir=False, timeout=20, verbose=False, local_path='.', retry=False):
        """
        SCP a file or directory from the device
        """
        return self._ssh.scp_from_device(path, is_dir, timeout, verbose, local_path, retry)

    def scp_file_to_device(self, files, remote_path='/usr/local/nas/', recursive=False, preserve_times=False):
        """
        SCP a file or directory from the device
        """
        return self._ssh.scp_file_to_device(files, remote_path, recursive, preserve_times)

    def open_sftp_connection(self):
        """
        Open a new SFTP connection to the test unit
        """
        return self._ssh.open_sftp_connection()

    def sftp_from_device(self, path, target_path='.', preserve_mtime=False):
        """
        SFTP a file or directory from the device
        """
        return self._ssh.sftp_from_device(path, target_path, preserve_mtime=preserve_mtime)

    def sftp_file_to_device(self, files, remote_path='/usr/local/nas/', recursive=False, preserve_times=False):
        """
        SFTP a file or directory from the device
        """
        return self._ssh.sftp_file_to_device(files, remote_path, recursive, preserve_times)

    # # @brief Sets quota on all volumes in a give group or user
    #  @param name: Name of the group or user to set the quota to. required
    #  @param group_or_user: Type of quota to set(group or user), defaults to 'group'
    #  @param amount: amount allocated space in MB, defaults to '100' MB
    #  @return: Exception is thrown if failed
    #
    def set_quota_all_volumes(self, name, group_or_user='group', amount=100):
        self._ssh.set_quota_all_volumes(name, group_or_user, amount)

    # # @brief Method to create user group(s)
    #  @param groupname:        Name of users group
    #  @param number_of_groups: Number of groups to be created, defaults to 1
    #  @param memberusers:      The user that you want to join the group, defaults to 'none'
    #  @param force_webui:      If True, use Selenium. If False (default), use best available method
    def create_groups(self, group_name='group', number_of_groups=1, memberusers=None, force_webui=False):
        if force_webui:
            self._sel.create_groups(number_of_groups, group_name)
        else:
            group = group_name
            for i in range(1, number_of_groups + 1):
                if number_of_groups > 1:
                    group = '{0}{1}'.format(group_name, i)

                self._tc_log.info('Creating group {}'.format(group))
                self._ssh.create_groups(group=group, memberusers=memberusers)

        return 0

    # # @brief Creates the given number of users
    #  @details Users are named starting from the start_prefix and go until
    #           the correct number of groups are created. Creates one user by default
    #  @param start_prefix:  The number that groups being created start at
    #  @param numberOfusers: Number of groups to create
    #

    # # @brief Method to delete user group
    #  @param group: Name of user group
    def delete_group(self, group):
        self._ssh.delete_group(group=group)

    # # @brief Method to modify user group
    #  @param group:       Groupname of the group to be modified
    #  @param memberusers: Group members of the group to be modified
    def update_group(self, group, memberusers):
        self._ssh.update_group(group=group, memberusers=memberusers)

    def get_drive_mappings(self, start_bay=1):
        """ Get a list of drive bays to drive name mappings

        :param start_bay: The bay to start with
        :return: A list of drive mappings
        """

        return self._ssh.get_drive_mappings(start_drive=start_bay)

    # # @brief Method to Create different RAID
    #  @param raidtype:         Type of RAID to be created. Can be either a string or an integer.
    #                           String values: JBOD, spanning, RAID0, RAID1, RAID5, RAID10
    #                           Integer values:  11,   12,     0,     1,     5,     10
    #  @param volume_size:      Size of Volume to be created, only used for RAID w/ spanning
    #  @param number_of_drives: Drives of RAID to be created, Range 1~4, defaults to the number of physical drives
    #  @param spanning:         RAID w/ w/o spanning, defaults to 'no'
    #  @param spare:            RAID w/ w/o spare, defaults to 'no'
    #  @param raid1_span_vol:   If set, a list of volume numbers to use for the spanning volume. Only valid for RAID 1
    #  @param debug_mode:       If True, report the commands to configure the RAID, but do not execut them
    #  @param force_rebuild:    If True, rebuild the RAID even if the unit is already configured for the selected RAID
    def configure_raid(self,
                       raidtype,
                       drive_bays=None,
                       volume_size=None,
                       spanning=False,
                       spare=False,
                       raid1_span_vol=None,
                       debug_mode=False,
                       force_rebuild=False,
                       **kwargs):

        obsolete_args = ['number_of_drives']

        bays, = ctl.kwargs_to_args(obsolete_args, kwargs)

        if bays is not None:
            drive_bays = bays

        if not force_rebuild:
            if not ctl.is_number(raidtype):
                raid_level = ctl.raid_mode_to_level(raidtype)
            else:
                raid_level = raidtype

            current_raid = self.get_raid_level()

            if current_raid == raid_level:
                return

        self._ssh.configure_raid(raidtype=raidtype,
                                 drive_bays=drive_bays,
                                 volume_size=volume_size,
                                 spanning=spanning,
                                 spare=spare,
                                 raid1_span_vol=raid1_span_vol,
                                 debug_mode=debug_mode)

    # # @brief Method to clear cache
    #
    @staticmethod
    def clear_cache():
        ctl.clear_cache()

    # # @brief Method to delete user group
    #  @param group: Name of user group
    def delete_all_groups(self):
        self._ssh.delete_all_groups()

    # *********************************************************************************************************#
    #    Selenium2Library API function wrappers                                                               #
    # *********************************************************************************************************#

    def access_browser(self, ip_addr):
        self._sel.go_to_url(url=ip_addr)

    # noinspection PyPep8Naming
    def login_webUI(self):
        self._sel.login_webUI()

    # # @brief Opens a webdriver browser and navigates to the web ui
    #
    # noinspection PyPep8Naming
    def access_webUI(self, use_ssl=False, do_login=True, accept_eula=True):
        # Handle EULA acceptance [ITR 99359]
        # Currently, exception is thrown if EULA not accepted, so checking for None instead of 404
        if self.get_eula_status() is None and accept_eula:
            self.accept_eula()

        self._sel.access_webUI(use_ssl, do_login)

    # # @brief Opens a webdriver browser in HTTPS and navigates to the web ui
    #
    # noinspection PyPep8Naming
    def access_webUI_https(self):
        # Handle EULA acceptance [ITR 99359]
        # Currently, exception is thrown if EULA not accepted, so checking for None instead of 404
        if self.get_eula_status() is None:
            self.accept_eula()
        self._sel.access_webUI_https()

    # # @brief Closes the webdriver browser
    #
    # noinspection PyPep8Naming
    def close_webUI(self):
        self._sel.close_webUI()

    # # @brief Select a frame within a UI page identified by `locator`.
    def select_frame(self, locator):
        self._sel.select_frame(locator)

    # # @brief Unselect a frame within a UI page that previously selected
    def unselect_frame(self):
        self._sel.unselect_frame()

    def is_element_visible(self, locator, wait_time=None):
        return self._sel.is_element_visible(locator, wait_time=wait_time)

    def is_element_enabled(self, locator, wait_time=None):
        return self._sel.is_enabled(locator, timeout=wait_time)

    def element_should_be_visible(self, element):
        self._sel.element_should_be_visible(element)

    def element_should_not_be_visible(self, element):
        self._sel.element_should_not_be_visible(element)

    def element_should_be_disabled(self, element):
        self._sel.element_should_be_disabled(element)

    def mouse_up(self, element):
        self._sel.mouse_up(element)

    def mouse_down(self, element):
        self._sel.mouse_down(element)

    def mouse_over(self, element):
        self._sel.mouse_over(element)

    def mouse_out(self, element):
        self._sel.mouse_out(element)

    def current_frame_contains(self, text):
        self._sel.current_frame_contains(text)

    def current_frame_should_not_contain(self, text):
        self._sel.current_frame_should_not_contain(text)

    # #@ Brief: Selects the window found with `locator` as the context of actions.
    #
    # @param locator: Can be in 3 formats:
    # 1. window title -> title=My Document
    # 2. window javascript name -> name=${name}
    # 3. window's current url -> url=http://google.com
    # If locator is 'None', it will select the main window
    def select_window(self, locator=None):
        self._sel.select_window(locator)

    # #@ Brief: Closes currently opened pop-up window.
    #
    def close_window(self):
        self._sel.close_window()

    # # @brief Sets the RAID configuration of the test unit to JBOD
    #
    def configure_jbod(self):
        self._sel.configure_jbod()

    # # @brief Sets the RAID configuration of the test unit to Spanning
    #
    def configure_spanning(self):
        self._sel.configure_spanning()

    # # @brief Sets the RAID configuration of the test unit to RAID 0
    #
    def configure_raid0(self):
        self._sel.configure_raid0()

    # # @brief Sets the RAID configuration of the test unit to RAID 1
    #
    def configure_raid1(self):
        self._sel.configure_raid1()

    # # @brief Sets the RAID configuration of the test unit to RAID 5
    #
    def configure_raid5(self):
        self._sel.configure_raid5()

    # # @brief Sets the RAID configuration of the test unit to RAID 10
    #
    def configure_raid10(self):
        self._sel.configure_raid10()

    # # @brief Sets the RAID configuration of the test unit to RAID 0 w/ Spanning
    #
    def configure_raid0spanning(self):
        self._sel.configure_raid0spanning()

    # # @brief Sets the RAID configuration of the test unit to RAID 1 w/ Spanning
    #
    def configure_raid1spanning1(self):
        self._sel.configure_raid1spanning1()

    # # @brief Sets the RAID configuration of the test unit to RAID 1 w/ Spanning
    #
    def configure_raid1spanning2(self):
        self._sel.configure_raid1spanning2()

    # # @brief Sets the RAID configuration of the test unit to RAID 1 w/ Spanning
    #
    def configure_raid1spanningboth(self):
        self._sel.configure_raid1spanningboth()

    # # @brief Sets the RAID configuration of the test unit to RAID 5 w/ Spanning
    #
    def configure_raid5spanning(self):
        self._sel.configure_raid5spanning()

    # # @brief Sets the RAID configuration of the test unit to RAID 10 w/ Spanning
    #
    def configure_raid10spanning(self):
        self._sel.configure_raid10spanning()

    # # @brief Sets the RAID configuration of the test unit to RAID 5 w/ Hot Spare
    #
    def configure_raid5hotspare(self):
        self._sel.configure_raid5hotspare()

    # # @brief Sets the RAID configuration of the test unit to RAID 5 w/ Hot Spare
    #
    def configure_raid5hotsparespanning(self):
        self._sel.configure_raid5hotsparespanning()

    # # @brief Gets to the given page within the web ui
    #  @param page_name: Name of the page you are getting to
    #                    \verbatim
    #                    Acceptable Pages:
    #                    'Home'
    #                    'Users'
    #                    'Shares'
    #                    'loud_Access'
    #                    'Backups'
    #                    'Storage'
    #                    'Apps'
    #                    'Settings'
    #                    \endverbatim
    #
    def get_to_page(self, page_name=''):
        self._sel.get_to_page(page_name)

    # # @brief Gets the text of the given element ID within the web ui
    #  @param Element ID: Name of the element ID of the desired textbox
    #  @param Expected Text: This is the text comparison of what you expect that textbox content to be
    def element_text_should_be(self, element_id, expected_text):
        self._sel.element_text_should_be(element_id, expected_text)

    # noinspection PyPep8Naming
    def create_multiple_users(self, start_prefix=1, numberOfusers=1):
        self._sel.create_multiple_users(start_prefix, numberOfusers)

    # # @brief Assigns an user to a specific group
    #  @param user_name: Name of the user to assign to a group
    #  @param group_name: Name of the group to assign user to
    #
    def assign_user_to_group(self, user_name, group_name):
        self._sel.assign_user_to_group(user_name, group_name)

    # # @brief Assigns an user to a specific share
    #  @param user_name: Name of the user to assign to a share
    #  @param share_name: Name of the share to assign user to
    #
    def assign_user_to_share(self, user_name, share_name, access_type='rw'):
        self._sel.assign_user_to_share(user_name, share_name, access_type)

    # # @brief Assigns a group to a specific share with the given access level
    #  @param group_name: Name of the group to assign to a share
    #  @param share_number: Number of the share to assign group to
    #  @detail: The share number corresponds to the share name, e.g. share1 is share number 1,
    #           share34 is share number 34.
    #
    #           Note: This function assumes that the share(s) was/were created using the create_shares function
    #           share1, share2 ... share99
    #           Also, share must be set to private instead of public. Toggle the Public switch off
    #
    #           Acceptable access_level:
    #            rw - Read/Write access
    #            r  - Read access only
    #            d  - No access
    #
    def assign_group_to_share(self, group_name, share_number, access_type='rw'):
        self._sel.assign_group_to_share(group_name, share_number, access_type)

    # # @brief Assigns a user a quota to the first volume
    #  @param user_name:     Name of the user to be assigned a quota
    #  @param size_of_quota: Size of the quota to be assigned
    #  @param unit_size:     Units of the quota size - Must be MB, GB, or TB
    #
    def create_user_quota(self, user_name, size_of_quota, unit_size):
        self._sel.create_user_quota(user_name, size_of_quota, unit_size)

    # # @brief Assigns a group a quota to the first volume
    #  @param group_name:    Name of the group to be assigned a quota
    #  @param size_of_quota: Size of the quota to be assigned
    #  @param unit_size:     Units of the quota size - Must be MB, GB, or TB
    #
    def create_group_quota(self, group_name, size_of_quota, unit_size):
        self._sel.create_group_quota(group_name, size_of_quota, unit_size)

    # # @brief Enters Invalid ID names for VLAN
    #
    def invalid_vlan_id(self):
        self._sel.invalid_vlan_id()

    # # @brief Clicks on element identify by 'locator'
    #  @param locator: ID of element to click on
    def click_element(self, locator, timeout=30, check_is_ready=True):
        self._sel.click_element(locator, timeout, check_is_ready=check_is_ready)

    def click_download_button(self,
                              element,
                              dest_file='downloaded_file',
                              timeout=None,
                              check_is_ready=True,
                              check_page=True,
                              do_login=True,
                              check_browser=True):
        """ Clicks the specified button and downloads the file it gets from the UUT. Used for buttons that present
            a system Save As dialog when clicked

        :param element: The element to click
        :param dest_file: The path and filename to save the data as
        :param timeout: How long to wait before giving up
        :param check_is_ready: Confirm that the element is ready before clicking
        :param check_page: Confirm that the UI is on the correct page
        :param do_login: Login if necessary
        :param check_browser: Checks the browser state
        """
        self._sel.click_download_button(element=element,
                                        dest_file=dest_file,
                                        timeout=timeout,
                                        check_is_ready=check_is_ready,
                                        check_page=check_page,
                                        do_login=do_login,
                                        check_browser=check_browser)

    def click_upload_button(self,
                            element,
                            source_file='downloaded_file',
                            timeout=None,
                            check_is_ready=True,
                            check_page=True,
                            do_login=True,
                            check_browser=True):
        """ Clicks the specified button and downloads the file it gets from the UUT. Used for buttons that present
            a system Save As dialog when clicked

        :param element: The element to click
        :param source_file: The path and filename to save the data as
        :param timeout: How long to wait before giving up
        :param check_is_ready: Confirm that the element is ready before clicking
        :param check_page: Confirm that the UI is on the correct page
        :param do_login: Login if necessary
        :param check_browser: Checks the browser state
        """
        self._sel.click_upload_button(element=element,
                                      source_file=source_file,
                                      timeout=timeout,
                                      check_is_ready=check_is_ready,
                                      check_page=check_page,
                                      do_login=do_login,
                                      check_browser=check_browser)

    # # @brief Return the associated text of locator
    #
    def get_text(self, locator, check_is_ready=True, retry=3, retry_delay=1):
        """
            Handle exception with retry
        :param locator: UI element to get the text value
        :param check_is_ready:
        :param retry: how many retries when exception occurs
        :param retry_delay: delay between every retry
        :return: Success to get the text
        """
        for i in xrange(retry, 0, -1):
            try:
                element_text = self._sel.get_text(locator=locator, check_is_ready=check_is_ready)
            except Exception as e:
                if i > 1:
                    # There are still retries left
                    self.log.info("{}. {} retries left.".format(e.message, i - 1))
                    time.sleep(retry_delay)
                else:
                    # Out of retries, so raise the exception
                    self.log.debug("get_text(): Reach maximum retry, raise exception")
                    raise
            else:
                # Success, so exit the function
                return element_text

    # # @brief Returns the value attribute of element identified by 'locator'
    #
    def get_value(self, locator):
        element = self._sel._build_element_info(locator)
        if self._sel.is_ready(element):
            return self._sel.driver.get_value(element.locator)
        else:
            raise ElementNotReady('Element not ready')

    # # @brief Verifies if element is visible or not
    #  @param locator: ID of element to wait for
    def is_visible(self, locator, timeout=None):
        try:
            self._sel.is_visible(locator, timeout)
        except Exception, ex:
            self._tc_log.info('Error while clicking on element\n{}'.format(ex))

    # # @brief Waits until element specified with 'locator' is visible
    #         Fails if 'timeout' expires before the element is visible.
    #  @param locator: ID of element to wait for
    def wait_until_element_is_visible(self, locator, timeout=None):
        self._sel.wait_until_element_is_visible(locator, timeout)

    # # @brief Waits until element specified with 'locator' is clickable
    #
    #  @param locator: ID of element to wait for
    def wait_until_element_is_clickable(self, locator, timeout=None):
        self._sel.wait_until_element_is_clickable(locator, timeout)

    # # @brief Double clicks a button identified by `locator`.
    #         Key attributes for buttons are `id`, `name` and `value`.
    #  @param locator: ID of the button to click on
    def double_click_element(self, locator):
        self._sel.double_click_element(locator)

    # # @brief Inputs text into text field
    #
    #  @param locator: ID of the text field where the text will be entered
    #         text: string to enter
    def input_text(self, locator, text, do_login=True, check_browser=True, check_page=True):
        self._sel.input_text(locator, text, do_login=do_login, check_browser=check_browser, check_page=check_page)

    # # @brief Inputs text into a password field
    #
    #  @param locator: ID of the text field where the text will be entered
    #         text: string to enter
    def input_password(self, locator, text, do_login=True, check_browser=True, check_page=True):
        self._sel.input_password(locator, text, do_login=do_login, check_browser=check_browser, check_page=check_page)

    # # @brief Clicks a button identified by `locator`.
    #         Key attributes for buttons are `id`, `name` and `value`.
    #  @param locator: ID of the button to click on
    def click_button(self, locator, check_is_ready=True):
        self._sel.click_button(locator, check_is_ready=check_is_ready)

    # # @brief Waits until the given element is not logically visible
    #
    #  @param locator: ID of the element to wait until is not visible before continuing
    #         time_out: Time to wait before failure; default is 6O seconds
    def wait_until_element_is_not_visible(self, locator, time_out=60):
        self._sel.wait_until_element_is_not_visible(locator, time_out)

    def select_from_list_by_value(self, locator, *values):
        self._sel.select_from_list_by_value(locator, *values)

    # # @brief Simulates user reloading page
    #
    def reload_page(self):
        self._sel.reload_page()

    # # @brief Waits until button specified with 'locator' is clickable then click
    #
    def wait_and_click(self, locator, timeout=None):
        return self._sel.wait_and_click(locator, timeout)

    # # @brief Finds an element in the group, user and share sublists given the name
    #          of that element.
    #          Group user or share is necessary to determine which list the element is in
    #
    def find_element_in_sublist(self, name, group_user_or_share):
        return self._sel.find_element_in_sublist(name, group_user_or_share)

    # # @brief Deletes all groups
    #   @param group_name - If present, only delete groups starting with group_name
    #          Deletes all groups
    # noinspection PyPep8Naming
    def delete_groups_webUI(self, group_name=''):
        return self._sel.delete_groups(group_name)

    # # @brief Drags element identified with `source` which is a locator.
    #          Element will be moved by xoffset and yoffset.  each of which is a
    #          negative or positive number specify the offset.
    def drag_and_drop_by_offset(self, source, xoffset, yoffset):
        return self._sel.drag_and_drop_by_offset(source, xoffset, yoffset)

    # # @brief Simulates user pressing key on element identified by `locator`.
    #          `key` is either a single character, or a numerical ASCII code of the key
    #          lead by '\\'.
    def press_key(self, locator, key):
        return self._sel.press_key(locator, key)

    # # @brief Finds an element.
    #
    #  @param locator: ID of the element to find.
    def element_find(self, locator):
        return self._sel.element_find(locator)

    # # @ Brief Change web access timeout value
    #
    # @param timeOut: timeout value, optional, default is 30 minutes
    #
    def set_web_access_timeout(self, timeout='30', force_webui=False):
        """ Changes the timeout for the web UI

        :param timeout: The number of minutes to wait.
        :param force_webui: If True, uses Selenium and the Web UI. Timeout must be one of the values in the UI if True.
        """
        if force_webui:
            return self._sel.set_web_access_timeout(timeout)
        else:
            self.execute('xmldbc -s /system_mgr/idle/time {}'.format(timeout))

    # @Brief Select an item from drop down menu
    #
    # @param dropdown_menu: menu where item will be selected
    # @param dropdown_item: item which will be selected from menu
    #
    def select_item_drop_down_menu(self, dropdown_menu, dropdown_item, scroll_value=20):
        return self._sel.select_item_drop_down_menu(dropdown_menu, dropdown_item, scroll_value)

    # # @brief Executes the given JavaScript code.
    #
    #  @param *code:  may contain multiple lines of code and may be divided into
    #                 multiple cells in the test data. In that case, the parts are
    #                 concatenated together without adding spaces.
    def execute_javascript(self, *code):
        self._sel.execute_javascript(*code)

    # #@ Brief: Verify if the expected value of specified attribute is correct
    #
    # @param locator: The locator of element
    # @param expected_value: The expected value of specified attribute
    # @param attribute_type: Can be "href", "datafld", "style"..etc.
    def check_attribute(self, locator, expected_value, attribute_type):
        self._sel.check_attribute(locator, expected_value, attribute_type)

    # #@ Brief Get browser windows size
    #
    # Returns current window size as `width` then `height`.
    #
    def get_window_size(self):
        return self._sel.get_window_size()

    # # @ Brief Set browser window size
    #
    # @param  width: Set the 'width' of the current window to the specified value
    #        height: Set the 'height' of the current window to the specified value
    #
    def set_window_size(self, width, height):
        self._sel.set_window_size(width, height)

    # #@ Brief: Re-try the target locator until check_locator is verified
    #           If check_locator is None, set check_locator to target locator and verify if it is invisible after click
    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        self._sel.click_wait_and_check(locator, check_locator, visible, retry, timeout)

    # #@ Brief: Retry the link element until it is set to the correct value
    #
    def click_link_element(self, locator, linkvalue, retry=3, timeout=5):
        self._sel.click_link_element(locator, linkvalue, retry, timeout)

    def click_element_at_coordinates(self, locator, xoffset, yoffset, click_delay=0.25):
        """ Click element identified by locator at x/y coordinates of the element.
            Cursor is moved from the center of the element and x/y coordinates are calculted from that point.
        @param locator: the reference locator
        @param xoffset: x coordinate from the center of locator
        @param yoffset: y coordinate from the center of locator
        @param click_delay: the delay time between mouse_down and mouse_up
        """
        self._sel.click_element_at_coordinates(locator, xoffset, yoffset, click_delay)

    # #@ Brief: Retry the text element until it is set to the correct value
    #
    def input_text_check(self, locator, text, do_login=True, check_browser=True, check_page=True, retry=3):
        self._sel.input_text_check(locator, text, do_login, check_browser, check_page, retry)

    def _take_screen_shot(self, message='Screenshot', prefix=None, sel=None):
        if not prefix:
            prefix = ctl.get_silk_test_name()
        filename = '{}_Screen_{}.png'.format(prefix, self.picture_counts)
        output_dir = ctl.get_silk_results_dir()
        path = os.path.join(output_dir, filename)
        if sel._is_browser():
            sel.capture_browser_screenshot(path)
            self.log.info("{}: {}".format(path, message))
            self.picture_counts += 1

    # #@ Brief: Take screen shot if web browser exists
    #
    def take_screen_shot(self, message='Screenshot', prefix=None):
        """
            Take Screen Shot for debugging

            @param prefix: If given, prefix will be part of screenshot filename. Otherwise, use TC filename by default
        """
        self._take_screen_shot(message, prefix, self._sel)

    def open_context_menu_and_check(self, locator, check_locator, retry=3, timeout=5):
        self._sel.open_context_menu_and_check(locator, check_locator, retry, timeout)

        # ********************************************************************************************************#
        #    serialAPI function wrappers
        # ********************************************************************************************************#

    def initialize_serial_port(self):
        """
        Initializes the serial port
        """
        return self._spc.initialize_serial_port()

    def close_serial_connection(self):
        return self._spc.close_serial_connection()

    # # @brief Tests if the serial port is connected to the test unit
    #  @return: Returns True if the serial port is connected to the test unit
    #
    def serial_is_connected(self):
        return self._spc.serial_is_connected()

    # # @brief Starts the serial port, does nothing if the port is already started
    #
    def start_serial_port(self):
        self._spc.start_serial_port()

    # # @brief Debugs the serial port
    #  @param msg: Message to debug the serial port with
    #
    def serial_debug(self, msg):
        self._spc.serial_debug(msg)

    # # @brief Reads everything from the serial port
    #  @return: Everything read from the serial port
    #
    def serial_read_all(self):
        return self._spc.serial_read_all()

    # # @brief Reads a chunk of data from the serial port
    #  @return: What was read from the serial port
    #
    def serial_read(self):
        return self._spc.serial_read()

    # # @brief Reads a line from the serial port
    #  @return: A line that was read from the serial port
    #
    def serial_readline(self):
        return self._spc.serial_readline()

    # # @brief Writes to the serial port and appends a new line
    #  @param buff: The buffer containing the data to write to the serial port
    #
    def serial_write(self, buff):
        self._spc.serial_write(buff)

    # # @brief Writes to the serial port
    #  @param buff: The buffer containing the data to write to the serial port
    def serial_write_bare(self, buff):
        self._spc.serial_write_bare(buff)

    # # @brief Waits for the given string or until timeout
    #  @param string: The string to wait for
    #
    # noinspection PyShadowingNames
    # The parameter should not have been called "string," but since it's not using string functions, it's okay.
    def serial_wait_for_string(self, string, timeout=None):
        self._spc.serial_wait_for_string(string, timeout)

        # ************************************************************************************************************#
        #    Common Test Library function wrappers
        # ************************************************************************************************************#

    # # @brief Run any Linux command on the current device
    #  @param command: The Linux command to be run on the device
    #
    def run_on_device(self, command, timeout=60):
        self._tc_log.debug('Running command: {}'.format(command))
        status, output = self.execute(command, timeout=timeout)
        return output

    def get_model_from_fw(self):
        model_command = "cat system.conf | grep 'model_number' | cut -d \"=\"  -f 2 | tr -d '\"'"
        return self.run_on_device(model_command)

    def send_keys(self,
                  element,
                  keystrokes,
                  timeout=None,
                  check_page=True,
                  do_login=True):

        self._sel.send_keys(element, keystrokes, timeout, check_page, do_login)

    def _install_spaceeater(self, install_on_uut=False):
        # Downloads SpaceEater for the proper platform and copies it
        platform = sys.platform

        nexus_group = 'DataIntegrity-'
        nexus_artifact = 'SpaceEater_'

        if install_on_uut:
            if self.create_file_installed_uut:
                return

            architecture = self.uut[Fields.toolchain]
            extension = 'tar.gz'
        else:
            if self.create_file_installed_locally:
                return

            if platform.startswith('linux'):
                architecture = 'Linux'
                extension = 'tar.gz'
            else:
                architecture = 'Windows'
                extension = 'zip'

        nexus_artifact += architecture
        nexus_group += architecture

        create_file_url = ctl.get_latest_nexus_build_url(repo_base='http://repo.wdc.com',
                                                         repository_id='snapshots',
                                                         group=nexus_group,
                                                         artifact=nexus_artifact,
                                                         package_type=extension)

        dest_filename = '{}.{}'.format(nexus_artifact, extension)

        if ctl.is_running_on_silk():
            self._tc_log.debug('SpaceEater build url: {0}'.format(create_file_url))
            filename = download_web_file(create_file_url, dest_filename=dest_filename)
        else:
            if not os.path.isfile(dest_filename):
                # Try with a more generic name
                pattern = 'SpaceEater_{}*.{}'.format(architecture, extension)

                file_list = glob.glob(pattern)

                if not file_list:
                    raise exception.NexusUnreachable('Not running on Silk. Download the archive from Nexus and '
                                                     'copy it to the test directory locally. '
                                                     'It can be downloaded from: {}{}'.format(os.linesep,
                                                                                              create_file_url))
                else:
                    dest_filename = file_list[0]

            filename = dest_filename
            self.log.info('Using pre-copied archive: {}'.format(filename))

        if install_on_uut:
            self.copy_file(filename, dest_share_name='Public')
            # Get it ready for Linux
            filename = filename.replace(' ', '\ ')
            filename = filename.replace('(', '\(')
            filename = filename.replace(')', '\)')

            ssh_command = 'tar -xvf /shares/Public/{} -C /shares/Public'.format(os.path.basename(filename))
            self._tc_log.debug('Extracting with command {}'.format(ssh_command))
            self._tc_log.debug(self.execute(ssh_command))
            self.create_file_installed_uut = True
            # Get the version
            spaceeater_path = self._get_spaceeater_path(run_on_uut=True)
            result, output = self.execute('{} --version'.format(spaceeater_path))
        else:
            ctl.extract_from_archive(filename)
            self.create_file_installed_locally = True
            spaceeater_path = self._get_spaceeater_path(run_on_uut=False)
            result, output = ctl.run_command_locally('{} --version'.format(spaceeater_path), raise_exceptions=False)

        self.log.info('SpaceEater installed. Version information: {}'.format(output))

    def _get_spaceeater_destination(self, dest_share_name, dest_dir_path, run_on_uut, protocol=None):
        if run_on_uut:
            if dest_share_name is None:
                dest_share_name = 'Public'

            path = '/shares/{}'.format(dest_share_name)

            if dest_dir_path != '':
                path += '/{}'.format(dest_dir_path)
        else:
            if dest_share_name:
                share_path = self.mount_share(dest_share_name, protocol=protocol)
                path = os.path.join(share_path, dest_dir_path)
            else:
                path = dest_dir_path

        return path

    def _get_spaceeater_path(self, run_on_uut, dest_share_name='Public'):
        if run_on_uut:
            return '"/shares/{}/spaceeater"'.format(dest_share_name)
        else:
            space_eater_path = os.path.join(self.get_test_client_directory(), 'spaceeater')

            if os.name == 'nt':
                return '{}.exe'.format(space_eater_path)
            else:
                return '"{}"'.format(space_eater_path)

    def create_image(self,
                     x_resolution,
                     y_resolution=None,
                     dest_share_name=None,
                     dest_dir_path=None,
                     color=None,
                     filename=None,
                     extension='jpg',
                     resolution_variance=0,
                     random_data=True,
                     dest_user=None,
                     dest_password=None,
                     dest_protocol=None):
        """ Creates a valid media file of the type specified by the file extension. Uses the PIL, so most image
            files should be supported. So far, only tested with jpg, png, and bmp formats. The image will be a solid
            block of a color.

        :param x_resolution: The X resolution of the image
        :param y_resolution: If not specified, default to x_resolution
        :param dest_share_name: If specified, create in the specified share on the UUT
        :param dest_dir_path: The relative or absolute path to the file
        :param color: The color to use for the fill as a tuple (R, G, B). Defaults to random values.
        :param filename: The filename to use, without the extension. Defaults to a hex string of the chosen color.
        :param extension: The extension to use. Determines the output type.
        :param resolution_variance: If > 0, randomly adjust the resolution up to +/- this percentage
        :param random_data: If True (default), generate 10-pixel high bands of random colors (slower, but larger files).
                            If False, generate a solid block of color (faster, but smaller files).
        :param dest_user: If present, use this user when mounting dest_share_name. Default is anonymous.
        :param dest_password: If present, use this password when mounting dest_share_name
        :param dest_protocol: If present, use this protocol when mounting dest_share_name. Default is Samba.

        :return: The path and filename of the image that was created
        """
        bpp = 3  # bytes, not bits, per pixel
        mode = 'RGB'

        if y_resolution is None:
            y_resolution = x_resolution

        min_x_resolution = int(x_resolution - x_resolution * float(resolution_variance) / 100)
        max_x_resolution = int(x_resolution + x_resolution * float(resolution_variance) / 100)

        min_y_resolution = int(y_resolution - y_resolution * float(resolution_variance) / 100)
        max_y_resolution = int(y_resolution + y_resolution * float(resolution_variance) / 100)

        if color is None:
            color = (randint(0, 255), randint(0, 255), randint(0, 255))

        if filename is None:
            r, g, b = color
            filename = 'image_{}_{}_{}'.format(hex(r), hex(g), hex(b))

        filename += '.' + extension

        if dest_share_name:
            share_path = self.mount_share(dest_share_name,
                                          user=dest_user,
                                          password=dest_password,
                                          protocol=dest_protocol)
            path = os.path.join(share_path, dest_dir_path)
        else:
            path = dest_dir_path

        outfile = os.path.join(path, filename)

        if not os.path.isdir(path):
            wdlfs.createpath(path)

            if dest_share_name:
                # Store it as a network share path
                path_info = (dest_share_name, dest_dir_path)
                self._paths_created_network.append(path_info)
            else:
                self._paths_created.append(path)

        resolution = (randint(min_x_resolution, max_x_resolution), randint(min_y_resolution, max_y_resolution))

        if random_data:
            image = Image.new(mode, resolution, None)
            band_size = resolution[0] * 10

            for t in xrange(0, (resolution[0] * resolution[1] * bpp) / band_size):
                color = (randint(0, 255), randint(0, 255), randint(0, 255))
                band = Image.new(mode, (resolution[0], 10), color)
                image.paste(band, (0, t * 10))
        else:
            image = Image.new(mode, resolution, color)

        draw = ImageDraw.Draw(image)
        # Add some text
        draw.text((10, 10), 'Random image generated by WD AAT')
        draw.text((10, 20), outfile)

        image.save(outfile)

        # Store the relative path and filename to store in the _files_copied* list for cleanup
        file_path = os.path.join(dest_dir_path, filename)
        if dest_share_name:
            file_info = (dest_share_name, file_path)
            self._files_copied_network.append(file_info)
        else:
            self._files_copied.append(file_path)

        # Finally, return the path to the file
        return file_path

    def create_file(self,
                    filesize,
                    units='KiB',
                    no_repeat=False,
                    file_count=1,
                    dest_share_name=None,
                    dest_dir_path='',
                    run_on_uut=False,
                    extension='jpg',
                    protocol=None):
        """

        :param filesize: The size of the file, in units (see units definition)
        :param units: The units for the size. Can be binary (KiB, MiB, etc), binary (KB, MB), or the word "bytes".
                      Default is KiB
        :param no_repeat: If True, the entire file is completely random.
                          If False (default), a block of random data is repeated. This speeds up generation.
        :param file_count: The number of files to create
        :param dest_share_name: If passed, a share is mounted using the specified sharename (such as Public) and
                                the file is written directly to the mounted share
        :param dest_dir_path: If passed, the file will be created in this path. If dest_share_name, the path is
                              relative to the mount point. If it is not passed, the path is used as-is (relative or
                              absolute).
        :param run_on_uut: If True, SpaceEater is run directly on the UUT. This flag affects the final path as follows.
                               If dest_share_name is passed, then the base path will be /shares/<dest_share_name>.
                               If dest_share_name is not passed, then the base path will be /shares/Public.
                               dest_dir_path will always be relative to the base path.
        :protocol: If passed, specifies the protocol used to mount the share. Default is Samba.
                   Ignored if dest_share_name is not passed or if run_on_uut is True.
        :return: A tuple of (mbps, [file_list]
        """

        if run_on_uut and dest_share_name is None:
            dest_share_name = 'Public'

        self._install_spaceeater(install_on_uut=run_on_uut)

        if no_repeat:
            blocksize = ' --blocksize {} {}'.format(filesize, units)
        else:
            blocksize = ''

        # Add a space for units
        if units != '':
            units = ' {}'.format(units)

        final_path = self._get_spaceeater_destination(dest_share_name=dest_share_name,
                                                      dest_dir_path=dest_dir_path,
                                                      run_on_uut=run_on_uut,
                                                      protocol=protocol)

        created_path = False
        if final_path != '' and dest_dir_path:
            if run_on_uut:
                self.execute('mkdir -p "{}"'.format(final_path))
                created_path = True
            else:
                if not os.path.isdir('"{}"'.format(final_path)):
                    wdlfs.createpath('"{}"'.format(final_path))
                    created_path = True

        if created_path:
            if dest_share_name:
                # Store it as a network share path
                path_info = (dest_share_name, dest_dir_path)
                self._paths_created_network.append(path_info)
            else:
                self._paths_created.append(final_path)

        toolpath = self._get_spaceeater_path(run_on_uut=run_on_uut, dest_share_name=dest_share_name)

        if extension:
            extension_string = ' --extension {}'.format(extension)
        else:
            extension_string = ''

        if final_path:
            destination_string = ' --destination "{}" '.format(final_path)
        else:
            destination_string = ''

        command_parameters = {'toolpath': toolpath,
                              'filesize': filesize,
                              'units': units,
                              'filecount': file_count,
                              'blocksize': blocksize,
                              'extension': extension_string,
                              'destination': destination_string}

        command = '{toolpath} --filesize {filesize}{units} --number {filecount}{blocksize}' \
                  ' --writeonly --remove{extension}{destination}'.format(**command_parameters)

        self._tc_log.info('Executing command: {}'.format(command))

        if run_on_uut:
            result, output = self.execute(command)
            # Set permission on the files so they can be deleted through SMB
            self.execute('chmod 777 /shares/{}testfile_*'.format(dest_share_name))
        else:
            result, output = ctl.run_command_locally(command, raise_exceptions=False)

        if result > 0:
            if result == 127:
                # SpaceEater not found
                raise exception.FileNotFoundError('{} not found'.format(toolpath))

            self._tc_log.debug('Result code: {}{}{}'.format(result, os.linesep, output))
            raise exception.GenerateFileError(str(result))

        file_list = []

        # Use None, not 0, in case the total time is 0 seconds. The first time a time is recorded, it will change to
        # a float.
        mbps = None
        files_generated = 0
        for line in output.split(os.linesep):
            if 'SpaceEater version' in line:
                print line

            if 'Creating test file: ' in line:
                self._tc_log.debug(line)
                generated_file = line.split('Creating test file: ')[1].split(' of size ')[0]
                filename = os.path.basename(generated_file)
                file_path = os.path.join(dest_dir_path, filename)
                file_list.append(file_path)

                if dest_share_name:
                    # Store it as a network share file
                    file_info = (dest_share_name, file_path)
                    self._files_copied_network.append(file_info)
                else:
                    self._files_copied.append(file_path)

            if 'Generation time was ' in line:
                self._tc_log.debug(line)

            if "Total write time" in line:
                # Found the total line, so extract MB/Second
                self._tc_log.debug(line)
                if mbps is None:
                    mbps = 0
                mbps += float(line.split('(')[1].split(' ')[0])
                files_generated += 1

        if mbps is not None:
            return mbps / files_generated, file_list

        # Must be a problem
        self._tc_log.debug(output)

        raise exception.GenerateFileError()

    def _read_file(self,
                   filename,
                   dest_share_name,
                   dest_dir_path,
                   run_on_uut,
                   get_read_speed):

        # Reads a file using SpaceEater
        # :param filename: The file to read
        # :param dest_share_name: The destination share name
        # :param dest_dir_path: The destination path
        # :param run_on_uut: If True, copy and run SpaceEater on the UUT, not locally
        # :param get_read_speed: If True, only read the file to get a read speed; do not validate the data
        # Returns the results code and output from SpaceEater. Does not do any parsing.

        self._install_spaceeater(install_on_uut=run_on_uut)

        dest_dir_path = self._get_spaceeater_destination(dest_share_name=dest_share_name,
                                                         dest_dir_path=dest_dir_path,
                                                         run_on_uut=run_on_uut)

        if filename is None:
            filepath = '"*"'
        else:
            filepath = os.path.join(dest_dir_path, filename)

        toolpath = self._get_spaceeater_path(run_on_uut=run_on_uut, dest_share_name=dest_share_name)

        command = '{} --testonly {}'.format(toolpath, filepath)

        if get_read_speed:
            command += " --readonly"

        if run_on_uut:
            self._tc_log.debug('Verifying {} with remote command {}'.format(filepath, command))
            result, output = self.execute(command)
        else:
            self._tc_log.debug('Verifying {} with local command {}'.format(filepath, command))
            result, output = ctl.run_command_locally(command, raise_exceptions=False)

        if result == 127:
            # SpaceEater not found
            raise exception.FileNotFoundError('{} not found'.format(toolpath))

        return result, output

    def get_read_speed(self,
                       filename=None,
                       dest_share_name=None,
                       dest_dir_path='',
                       run_on_uut=False):

        """ Reads the given file and returns mbps

        :param filename:
        :param dest_share_name:
        :param dest_dir_path:
        :param run_on_uut:
        :return:
        """
        result, output = self._read_file(filename=filename,
                                         dest_share_name=dest_share_name,
                                         dest_dir_path=dest_dir_path,
                                         run_on_uut=run_on_uut,
                                         get_read_speed=True)

        if result > 0:
            self._tc_log.debug('Result code: {}{}{}'.format(result, os.linesep, output))
            raise exception.VerifyFileError('Could not read file')

        for line in output.split(os.linesep):
            if 'SpaceEater version' in line:
                print line

            if "Total time" in line:
                # Found the total line, so extract MB/Second
                self._tc_log.debug(line)
                mbs = line.split('(')[1].split(' ')[0]
                return int(float(mbs))

        # Must be a problem
        self._tc_log.debug('Unable to get read speed.{}{}'.format(os.linesep, output))

        raise exception.VerifyFileError()

    def is_file_good(self,
                     filename=None,
                     dest_share_name=None,
                     dest_dir_path='',
                     run_on_uut=False,
                     measure_performance=False):
        """ Confirms that the given file is not corrupted. Only works with files using
            the SpaceEater tool (which is used by the create_file() method). If no file is specified, it will test all
            testfile_* files in the current directory.

        :param filename: The file or files to check
        :param dest_share_name: If passed, mount the specified share
        :param dest_dir_path: The path to the file. If passed with dest_share_name, this is appended to the mount point
        :param run_on_uut: If True, run SpaceEater on the UUT through SSH to verify. This is useful when verifying
                              a very large file and it is not necessary to test it through the network
        :param measure_performance: If True, return True/False plus the MiB/second read speed. This gets the performance
                                    while validating the file, so on local reads, the read performance may be
                                    artifically low. For true read performance, use get_read_speed() instead.
        :return: If measure_performance is False, True if all files are good, False if one or more is corrupt.
                 If measure_performance is True, (True/False, read speed)
        """

        result, output = self._read_file(filename=filename,
                                         dest_share_name=dest_share_name,
                                         dest_dir_path=dest_dir_path,
                                         run_on_uut=run_on_uut,
                                         get_read_speed=False)

        if result > 0:
            # Failed verification, so immediately return
            self._tc_log.info('File is corrupt. Verification result code is: {}'.format(result))
            self._tc_log.info('Output: {}{}'.format(os.linesep, output))
            if measure_performance:
                return (False, 0)
            else:
                return False
        else:
            valid_data = True

        # Find the number of files verified
        files_verified = 0
        read_rate = 0

        for line in output.split('\n'):
            if "Total time" in line:
                # Found the total line, so extract MB/Second
                mbs = line.split('(')[1].split(' ')[0]
                read_rate = int(float(mbs))

            if 'SpaceEater version' in line:
                print line

            if 'Verifying file' in line:
                files_verified += 1

        if files_verified == 0:
            # No files verified, throw an exception
            self._tc_log.info('Unable to verify file. Output: {}{}'.format(os.linesep, output))
            raise exception.VerifyFileError('No confirmation of files being verified')
        else:
            self._tc_log.info('Verification complete. {} files verified.'.format(files_verified))

        if measure_performance:
            return (valid_data, read_rate)
        else:
            return valid_data

    def _update_file_status(self, file_list, total_files, last_percent):
        # Get the number of files processed + 1, and convert to a long
        files_processed = (len(file_list) + 1) * 1.0
        percent_complete = int((files_processed / total_files) * 100)

        if percent_complete != last_percent:
            # New percentage, so update the status
            self._tc_log.info('{} / {} files ({}%)'.format(len(file_list) + 1,
                                                           total_files,
                                                           percent_complete))
        return percent_complete

    def generate_media_files(self,
                             media_info,
                             total_files,
                             destination,
                             share_name=None,
                             copy_through_ssh=False,
                             files_per_folder=20,
                             max_folders=100,
                             all_unique=False,
                             media_variance=25,
                             random_data=True,
                             share_user=None,
                             share_password=None,
                             share_protocol=None):

        self.log.info('Generating {} files'.format(total_files))

        # A list of files that were created
        file_list = []

        # Determine the media information
        media_type = media_info[0].lower()
        if 'jpg' == media_type:
            extension, x_resolution, y_resolution = media_info
        else:
            raise exception.InvalidParameter('Media type of {} is not supported'.format(media_type))

        last_percent = 0
        if not all_unique:
                # Generate the files in the root
                base_file_count = total_files if total_files <= files_per_folder else files_per_folder
                for file_number in xrange(1, base_file_count + 1):
                    last_percent = self._update_file_status(file_list, total_files, last_percent)
                    file_created = self.create_image(x_resolution=x_resolution,
                                                     y_resolution=y_resolution,
                                                     dest_share_name=share_name,
                                                     dest_dir_path=destination,
                                                     filename='aat_%04d' % file_number,
                                                     extension='jpg',
                                                     resolution_variance=media_variance,
                                                     random_data=random_data,
                                                     dest_user=share_user,
                                                     dest_password=share_password,
                                                     dest_protocol=share_protocol)
                    file_list.append(file_created)

        # Create a list of folders that can be used to copy files
        base_folders = []
        for folder_number in xrange(1, max_folders + 1):
            base_folders.append(os.path.join(destination, 'aat_%04d' % folder_number))

        # Loop until all files are created
        while len(file_list) < total_files:
            last_percent = self._update_file_status(file_list, total_files, last_percent)
            sub_folders = []

            for folder in base_folders:
                if copy_through_ssh:
                    self.execute('mkdir -p /shares/{}/{}'.format(share_name, folder.replace(' ', '\\ ')))
                    if folder != '':
                        path_info = (share_name, folder)
                        self._paths_created_network.append(path_info)
                if all_unique:
                    files_remaining = total_files - len(file_list)
                    next_chunk = files_remaining if files_remaining < files_per_folder else files_per_folder
                    for file_number in xrange(1, files_remaining + 1):
                        last_percent = self._update_file_status(file_list, total_files, last_percent)
                        file_created = self.create_image(x_resolution=x_resolution,
                                                         y_resolution=y_resolution,
                                                         dest_share_name=share_name,
                                                         dest_dir_path=folder,
                                                         filename='aat_%04d' % file_number,
                                                         extension=extension,
                                                         resolution_variance=media_variance,
                                                         dest_user=share_user,
                                                         dest_password=share_password,
                                                         dest_protocol=share_protocol)

                        file_list.append(file_created)
                else:
                    files_remaining = total_files - len(file_list)
                    next_chunk = files_remaining if files_remaining < files_per_folder else files_per_folder
                    for file_number in xrange(1, next_chunk + 1):
                        last_percent = self._update_file_status(file_list, total_files, last_percent)
                        file_name = 'aat_%04d' % file_number
                        if copy_through_ssh:
                            base_dir = '/shares/{}/{}'.format(share_name, destination)
                            if destination != '':
                                self._paths_created_network.append((share_name, destination))
                            self.execute('cp "{}/{}.{}" "/shares/{}/{}/{}.{}"'.format(base_dir,
                                                                                      file_name,
                                                                                      extension,
                                                                                      share_name,
                                                                                      folder,
                                                                                      file_name,
                                                                                      extension))

                            self.execute('chmod 777 /shares/{}/{}/{}.{}'.format(share_name,
                                                                                folder,
                                                                                file_name,
                                                                                extension))
                        else:
                            file_name += '.' + extension
                            self.copy_file(source_file=file_name,
                                           source_path=destination,
                                           destination_path=folder,
                                           source_share_name=share_name,
                                           dest_share_name=share_name,
                                           source_user=share_user,
                                           source_password=share_password,
                                           source_protocol=share_protocol,
                                           dest_user=share_user,
                                           dest_password=share_password,
                                           dest_protocol=share_protocol)

                        file_info = (share_name, '{}/{}'.format(folder, file_name))
                        self._files_copied_network.append(file_info)
                        file_list.append(file_info[1])

                # Create additional subfolders, in case we need them
                for subfolder in xrange(1, max_folders + 1):
                    sub_folders.append(os.path.join(folder, 'aat_%04d' % subfolder))

                if len(file_list) >= total_files:
                        break

            # Move the sub_folders to the base_folders list
            base_folders = sub_folders

        return file_list

        # ************************************************************************************************************#
        #    networkSharesAPI function wrappers
        # ************************************************************************************************************#

    # # @brief Generates a file of the given size in kilobytes
    #  @param kilobytes: Size of the file to generate, in Kilobytes
    #
    def generate_test_file(self, kilobytes, prefix=None):
        file_generated = self._nsc.generate_test_file(kilobytes, prefix=prefix)
        self._files_copied.append(file_generated)

        return file_generated

    # # @brief Generates test files of the given size in kilobytes
    #  @param kilobytes: Size of the files to generate, in Kilobytes
    #
    def generate_test_files(self, kilobytes=50, file_count=50, extension='jpg', prefix=None):
        file_list = self._nsc.generate_test_files(kilobytes, file_count, extension, prefix=prefix)

        if file_list:
            for filename in file_list:
                self._files_copied.append(filename)

        return file_list

    # # @brief Hashes a file with sha1 hash
    #  @param filepath: Path of the file to hash
    #

    def hashfile(self, filepath):
        """
        Hash a given file (locally)
        """
        return self._nsc.hashfile(filepath)

    # # @brief Hashes a file with md5 hash
    #  @param filepath: Path of the file to hash
    #
    def hashfile_md5(self, filepath):
        """
        Hash a given file (locally)
        """

        return wdlfs.gethash(filepath, method='md5')

    # # @brief Hashes a file with md5 hash for a file on the device
    #  @param filepath: Path of the file to hash
    #
    def md5_checksum(self, path, filename):
        """
        Hash a given file on UUT
        """
        return self._ssh.md5_checksum(path, filename)

    # # @brief Generate test files in a temp folder on the workspace
    #  @param kilobytes:  Size of files to generate in kilobytes, default is 50
    #  @param file_count: Number of files to generate, default is 50
    #  @param extension:  Extension of the files to generate, default is jpg
    #  @return: Returns the files that were generated
    #
    def generate_files_on_workspace(self, kilobytes=50, file_count=50, extension='jpg'):
        file_list = self._nsc.generate_files_on_workspace(kilobytes, file_count, extension)
        for filename in file_list:
            self._files_copied.append(filename)

        return file_list

    # # @brief Apply checksum on a temp directory in the workspace
    #  @return: Returns the checksum generated
    #
    def checksum_files_on_workspace(self, dir_path=None, method='sha256'):
        return self._nsc.checksum_files_on_workspace(dir_path, method)

    # # @brief Applies checksum on files on the test share
    #  @return Returns the checksum generated
    #
    def checksum_files_on_share(self, share_name=None):
        return self._nsc.checksum_files_on_share(share_name)

    # # @brief Writes the given files to the test unit with the given ip with SMB
    #         Note: Make sure shares are public otherwise a connection error will happen
    #  @param files:      List of files to write to the test unit
    #  @return: Returns the files written
    #
    def write_files_to_smb(self, username='admin', password='', files=None, path=os.sep,
                           share_name=utilities.TEST_SHARE_NAME,
                           timeout=30, delete_after_copy=False, performance=False):

        return self._nsc.write_files_to_smb(username, password, files, path, share_name,
                                            timeout, delete_after_copy, performance)

    # # @brief Reads the given files from the test unit with the given ip
    #  @param files:      Files to be read from the smb, default is None
    #  @return: The files read from the test unit
    #
    def read_files_from_smb(self, username='admin', password='', files=None, path=os.sep,
                            share_name=utilities.TEST_SHARE_NAME,
                            timeout=30, delete_after_copy=False, performance=False):

        return self._nsc.read_files_from_smb(username, password, files, path, share_name,
                                             timeout, delete_after_copy, performance)

    # # @brief Enables the SMB debug setting
    #
    def enable_smb_debugging(self):
        return self._nsc.enable_smb_debugging()

    # # @brief Disables the SMB debug setting
    #
    def disable_smb_debugging(self):
        return self._nsc.disable_smb_debugging()

    # # @brief Writes the given files to the NFS
    #  @param files:      Files to be written
    #  @return: Returns the files that were written
    #
    def write_files_to_nfs(self,
                           files,
                           path=os.sep,
                           share_name=utilities.TEST_SHARE_NAME,
                           timeout=30,
                           delete_after_copy=True):
        return self._nsc.write_files_to_nfs(files,
                                            path,
                                            share_name,
                                            timeout,
                                            delete_after_copy)

    # # @brief Enable the NFS service
    #
    def enable_nfs_service(self):
        return self._sel.enable_nfs_service()

    # # @brief Enable NFS Share access
    #
    def enable_nfs_share_access(self, share_name=utilities.TEST_SHARE_NAME, access='write'):
        return self._sel.enable_nfs_share_access(share_name, access)

    # # @brief Deletes the given file
    #  @param filename:   Name of the file to be deleted
    #
    def delete_file_from_smb(self, filename, username='admin', password='', share_name=utilities.TEST_SHARE_NAME,
                             timeout=30):
        return self._nsc.delete_file_from_smb(filename, username, password, share_name, timeout)

    # # @brief Delete all files from the test unit
    #
    def delete_all_files_from_smb(self, username='admin', password='', share_name=utilities.TEST_SHARE_NAME, timeout=30,
                                  path=os.sep):
        return self._nsc.delete_all_files_from_smb(username, password, share_name, timeout, path)

    # # @brief Returns a list of all files on the test unit
    #  @return: A list of all files on the test unit
    #
    def list_files_on_smb(self, username='admin', password='', share_name=utilities.TEST_SHARE_NAME, timeout=30,
                          path=os.sep):
        return self._nsc.list_files_on_smb(username, password, share_name, timeout, path)

    # # @brief Returns a list of all files on the test unit
    #  @param ip_address: IP address of the test unit
    #  @return: A list of all files on the test unit
    #
    def list_files_on_nfs(self, ip_address):
        return self._nsc.list_files_on_nfs(ip_address)

    def send_magic_packet(self):
        self._nsc.send_magic_packet()

    # # @brief Writes the given files to the test unit with the given ip with FTP
    #         Note: Make sure ftp shares are read/write granted otherwise a connection error will happen
    #  @param files:      List of files to write to the test unit
    #  @return: Returns the files written
    #

    def write_files_to_ftp(self, ftp_ip=None, username=None, password=None, retries=3,
                           files=None, share_name='Public', delete_after_copy=True):
        return self._nsc.write_files_to_ftp(ftp_ip, username, password, retries,
                                            files, share_name, delete_after_copy)

    # # @brief Reads the given files from the test unit with the given ip
    #  @param files:      Files to be read from the FTP, default is None
    #  @return: The files read from the test unit
    #
    def read_files_from_ftp(self, ftp_ip=None, username=None, password=None, retries=3,
                            files=None, share_name='Public', delete_after_copy=False):
        return self._nsc.read_files_from_ftp(ftp_ip, username, password, retries,
                                             files, share_name, delete_after_copy)

        # ************************************************************************************************************#
        #    powerSwitchAPI function wrappers
        #    psc = Power Switch Client
        # ************************************************************************************************************#

    # # @brief Power cycles the given port
    #  @param power_switch_port: Port of the power switch
    #  @param cycletime:         Time it will be cycled for
    #
    def power_cycle(self, power_switch_port=None, cycletime=None):
        return self._psc.power_cycle(power_switch_port=power_switch_port, cycletime=cycletime)

    # # @brief Powers on the given power switch port
    #  @param power_switch_port: Port of the power switch
    #
    def power_on(self, power_switch_port=None):
        return self._psc.power_on(power_switch_port=power_switch_port)

    # # @brief Powers off the given power switch port
    #  @param power_switch_port: Port of the power switch
    #
    def power_off(self, power_switch_port=None):
        return self._psc.power_off(power_switch_port=power_switch_port)

    # # @brief Queries the status of the given power switch port
    #  @param power_switch_port: Port of the power switch
    #  @return: The status of an outlet, will be either 'ON', 'OFF', or 'Unknown'
    #
    def outlet_status(self, power_switch_port=None):
        return self._psc.outlet_status(power_switch_port=power_switch_port)

    # # @brief Waits for an element to be visible and then click
    #  @return: A list of the status of all outlets
    #
    def outlet_status_list(self):
        return self._psc.outlet_status_list()

        # ************************************************************************************************************#
        #    cgiAPI function wrappers
        #    cgic = CGI Client
        # ************************************************************************************************************#

    # # @brief Send CGI command
    #
    #
    def save_config_file(self):
        self._cgi.save_config_file()

    def delete_app(self, appName='IceCast', uutIP=None):
        self._cgi.delete_app(appName, uutIP)

    def add_app(self, appName='IceCast', uutIP=None):
        self._cgi.add_app(appName, uutIP)

    def reset_network_config_dhcp(self):
        networkProto = ''.join(self.get_xml_tags(self.get_network_configuration()[1],'proto'))
        if networkProto <> 'dhcp_client':
            self._cgi.reset_network_config_dhcp()
        else:
            self.log.info('Network is already in dhcp mode. Nothing to do')

    def get_alerts_cgi(self):
        result = self._cgi.get_alerts()
        return result

    def delete_remote_backup_job(self, job_name=None):
        self._cgi.delete_remote_backup_job(job_name)

    def get_file_info_cgi(self, file_name, parent_share_path):
        """ Get file information from CGI command
        @param file_name: The full name of the file, e.g. dummyfile.txt
        @param parent_share_path: The full path of the file's parent folder, e.g. /mnt/HD/HD_a2/Public/
        @return: XML Response Example:
                 <status>
                    <change_time>2015-7-23 3:17</change_time>
                    <modify_time>2015-7-23 3:17</modify_time>
                    <access_time>2015-7-23 3:17</access_time>
                    <owner>root</owner>
                    <group>root</group>
                    <permission>rw-rw-rw-</permission>
                 </status>
        """
        result = self._cgi.get_file_info(file_name, parent_share_path)
        return result

    def enable_ftp_service_by_cgi(self, state='ON'):
        """
        :param state: Turn ON or OFF the ftp service
        """
        result = self._cgi.enable_ftp_service(state=state)
        cgi_result = self.get_xml_tags(result, 'result')
        if '1' in cgi_result:
            self._tc_log.info("CGI: FTP service is set to {} successfully".format(state))
        else:
            self._tc_log.error("CGI ERROR: Fail to set ftp service to {}".format(state))

    def get_usb_mapping_info(self):

        result = self._cgi.get_usb_mapping_info()
        data = self.get_xml_tags(result, 'data')
        return data

    def output_test_settings(self, debug_only=True):
        # Log the test settings to the debug log
        if debug_only:
            logger = self.log.debug
        else:
            logger = self.log.info

        logger('')
        logger('*** Begin Test Settings ***')

        for setting in sorted(self.uut):
            logger('    {}: {}'.format(setting, self.uut[setting]))

        logger('*** End Test Settings ***')
        logger('')

    def get_environment_variable(self, variable):
        """ Returns the value of an OS environment variable

        :param variable: The variable to get
        :return: The value of the variable. Raises a KeyError if it doesn't exist
        """
        return self.environ.get(variable)


