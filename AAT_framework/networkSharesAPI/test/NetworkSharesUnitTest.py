'''
#
# Created on August 6, 2014
# @author: Jason Tran
#
# This module contains the class and methods to validate NetworkSharesAPI methods.
#
'''

import unittest
import logging

'''
## To run unit test from commamnd line
import os, sys
import inspect

#import logger from grandparent directory
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir2 = os.path.dirname(parentdir)
sys.path.insert(0,parentdir2)
from UnitTestFramework import mylogger
from networkSharesAPI.src import NETWORKSHARESClient
from global_libraries import CommonTestLibrary

'''

from UnitTestFramework import mylogger
from networkSharesAPI.src.networksharesclient import NetworkSharesClient
import global_libraries.CommonTestLibrary
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import TestClient


l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('NetworkSharesAPIUnitTest')

# Variables to use for unit testing
ip_address = '192.168.1.120'
ssh_username = 'sshd'
ssh_password = 'welc0me'
username = 'admin'
password = ''
file_to_read = 'vpnsetup.dmg'
filename = 'file1.jpg'

test = TestClient(ip_address=ip_address)
test.uut[Fields.ssh_password] = ssh_password
test.uut[Fields.ssh_username] = ssh_username
test.uut[Fields.web_username] = username
test.uut[Fields.web_password] = password


# instantiate NetworkSharesAPI class
try:
    myNet = NetworkSharesClient(ip_address)
except Exception, ex:
    logger1.info('Failed to instantiate NetworkSharesAPI ' + str(ex))

generated_files = myNet.generate_files_on_workspace(1024, 5)

'''
#instantiate CommonTestLibrary class
try:
    #myCTL = CommonTestLibrary(ip_address, ssh_username, ssh_password, username, password)
    myCTL = CommonTestLibrary(ip_address)
except Exception,ex:
    logger1.info('Failed to instantiate CommonTestLibrary ' + str(ex))
'''

class NetworkSharesUnitTest(unittest.TestCase):

    def setUp(self):
        logger1.info('Start executing ' + str(self._testMethodName))

    def tearDown(self):
        logger1.info('Finished executing ' + str(self._testMethodName))

    # # @brief Copy generated files from workspace to a share
    #  @param [in] files: to be copied to share
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_0_write_files_to_smb(self):
        try:
            # generated_files = myNet.generate_files_on_workspace(1024, 5)
            myNet.write_files_to_smb(generated_files)
        except Exception, ex:
            logger1.info('Failed write file to SMB ' + str(ex))

    # # @brief Read files existing on given share, copy to workspace to caculate the transfer rate
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_1_read_files_from_smb(self):
        try:
            myNet.read_files_from_smb(file_to_read)
        except Exception, ex:
            logger1.info('Failed read file from SMB ' + str(ex))

    # # @brief Enables the SMB debug setting
    def test_enable_smb_debugging(self):
        try:
            myNet.enable_smb_debugging()
        except Exception, ex:
            logger1.info('Error enabling smb debugging ' + str(ex))

    # # @brief Disables the SMB debug setting
    def test_disable_smb_debugging(self):
        try:
            myNet.disable_smb_debugging()
        except Exception, ex:
            logger1.info('Error disabling smb debugging' + str(ex))

    # not working
    # # @brief Writes the given files the given path and share name via NFS
    #  @param [in] ip_address: test unit IP
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_4_write_files_to_nfs(self):
        try:
            myNet.write_files_to_nfs(generated_files)
        except Exception, ex:
            logger1.info('Error writing files to nfs' + str(ex))

    # # @brief Delete a file on a given share via SMB
    #  @details Delete a file from the test unit with the given IP address using SMB
    #  @param [in] ip_address: test unit IP
    #  @param [in] filename: name of file to be deleted
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_3_delete_file_from_smb(self):
        try:
            myNet.delete_file_from_smb(filename)
        except Exception, ex:
            logger1.info('Error deleting files from smb' + str(ex))

    # # @brief Delete files on a given share via SMB
    #  @details Delete all files from the test unit with the given using SMB
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_4_delete_all_files_from_smb(self):
        try:
            myNet.delete_all_files_from_smb()
        except Exception, ex:
            logger1.info('Error deleting allfiles from smb' + str(ex))

    # # @brief List files on a given share via SMB
    #  @details Returns a list of all the files on the test unit using SMB
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_2_list_files_on_smb(self):
        try:
            myNet.list_files_on_smb()
        except Exception, ex:
            logger1.info('Error listing files on smb' + str(ex))

    # Not working yet
    # # @brief List files on a given share via NFS
    #  @param [in] ip_address: test unit IP
    #  @param [optional] share_name: default test share is SmartWare
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_5_list_files_on_nfs(self):
        try:
            myNet.list_files_on_nfs(ip_address)
        except Exception, ex:
            logger1.info('Error listing files on nfs' + str(ex))

    # # @brief Open connection with test unit via NFS
    def test_connect_nfs(self):
        try:
            status = myNet.connect_nfs()
            print status
        except Exception, ex:
            logger1.info('Error connect via nfs' + str(ex))

    # # @brief Open connection with test unit via SMB
    def test_connect_smb(self):
        try:
            status = myNet.connect_smb()
            print status
        except Exception, ex:
            logger1.info('Error connect via nfs' + str(ex))

    '''
    ## @brief Method to execute constructed command
    def cmd_run(self, cmd):
        self.cmd = cmd
        return subprocess.check_output(self.cmd, shell=True)
    '''

    # # @brief Generate test files on current workspace
    #  @param [optional] kilobytes: file size, default value is 50
    #  @param [optional] file_count: number of files, default value is 50
    #  @param [optional] extension: file type, default value is jpg
    def test_generate_files_on_workspace(self):
        try:
            status = myNet.generate_files_on_workspace()
            print status
        except Exception, ex:
            logger1.info('Error in generating test files on workspace ' + str(ex))

    # # @brief Generate a single test file on local working directory
    #  @param [optional] kilobytes: file size, default value is 50
    #  @param [optional] extension: file type, default value is jpg
    def test_generate_test_file(self):
        try:
            status = myNet.generate_test_file()
            print status
        except Exception, ex:
            logger1.info('Error in generating a test file %s', str(ex))

    # # @brief Get a checksum of existing file
    #  @param [in] filepath: file location
    def test_hashfile(self):
        # local file
        filepath = '/home/buildmeister/Desktop/AFP-74BD.mp4'
        try:
            status = myNet.hashfile(filepath)
            logger1.info('checksum of %s: %s', str(filepath), str(status))
        except Exception, ex:
            logger1.info('Error in getting checksum of %s, %s', str(filepath), str(ex))

    # # @brief Get checksum of all files on a given share
    #  @param [optional] share_name: default share is SmartWare
    def test_2_checksum_files_on_share(self):
        try:
            status = myNet.checksum_files_on_share()
            print status
            # logger1.info('checksum of %s: %s', str(filepath), str(status))
        except Exception, ex:
            logger1.info('Error in getting checksum of %s', str(ex))

    # # @brief Get checksum of files inside a directory on local working space
    #  @param [optional] dir_path: directory to get checksum
    def test_checksum_files_on_workspace(self):
        try:
            status = myNet.checksum_files_on_workspace()
            logger1.info('checksum of current working workspace: %s', str(status))
        except Exception, ex:
            logger1.info('Error in getting checksum of current working space: %s', str(ex))

if __name__ == '__main__':
    # # To run whole test suite
    # unittest.main()


    # #specify test(s) to execute individual
    suite = unittest.TestSuite()

    # suite.addTest(NetworkSharesUnitTest("test_generate_files_on_workspace"))
    suite.addTest(NetworkSharesUnitTest("test_checksum_files_on_workspace"))
    # suite.addTest(NetworkSharesUnitTest("test_2_list_files_on_smb"))
    # suite.addTest(NetworkSharesUnitTest("test_0_write_files_to_smb"))
    suite.addTest(NetworkSharesUnitTest("test_2_checksum_files_on_share"))

    # suite.addTest(NetworkSharesUnitTest("test_connect_smb"))


    runner = unittest.TextTestRunner()
    unittest.TextTestRunner(verbosity=2).run(suite)
