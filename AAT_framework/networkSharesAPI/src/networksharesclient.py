import os
import re
import time
import traceback
import socket
import logging
import timeit
import shutil
import tempfile
import random
import subprocess

# Only import for non-Windows system
if os.name != 'nt':
    import nfspy


from global_libraries import utilities
from global_libraries.testdevice import Fields

from global_libraries import wol
from global_libraries.Inherit import inherit
import global_libraries.CommonTestLibrary as ctl
import global_libraries.wd_exceptions as exception
from global_libraries.wd_localfilesystem import gethash
from ftplib import FTP
from ftplib import error_perm

try:
    import smb
    import smb.base
    import smb.SMBConnection as SMBConnection
    import smb.smb_structs
    from nmb.NetBIOS import NetBIOS
except ImportError as e:
    print '*WARN* pysmb not installed'
    print '*WARN* {0}'.format(traceback.format_exc())

class NetworkSharesError(Exception):
    pass

class NetworkSharesClient(object):

    def __init__(self, uut):
        self.uut = uut
        self.log = ctl.getLogger('AAT.networksharesclient')

    def enable_smb_debugging(self):
        smb.base.SMB.log.setLevel(logging.DEBUG)
        smb.SMBConnection.SMBConnection.log.setLevel(logging.DEBUG)
        utilities.info('ENABLING SMB DEBUGGING: {0}'.format(time.asctime()))

    def disable_smb_debugging(self):
        smb.base.SMB.log.setLevel(logging.INFO)
        smb.SMBConnection.SMBConnection.log.setLevel(logging.INFO)
        utilities.info('DISABLING SMB DEBUGGING: {0}'.format(time.asctime()))

    @utilities.decode_unicode_args
    def _get_bios_name(self, remote_smb_ip, timeout=10):
        for i in xrange(0, 3):
            try:
                bios = NetBIOS()
                srv_name = bios.queryIPForName(remote_smb_ip, timeout=timeout)
                if srv_name is not None:
                    return srv_name
            except:
                self.log.warning(ctl.exception("Looking up timeout, check remote_smb_ip again!!"))
            finally:
                bios.close()

        raise exception.DeviceUnreachableError('Unable to reach device.')


    @utilities.decode_unicode_args
    def connect_smb(self, username, password, timeout):
        """
        Connects to the test unit with the given ip address, username, or admin.
        Default for username is admin, and the default password is blank
        """
        start = timeit.default_timer()
        while True:
            try:
                utilities.info('CONNECTING TO SHARE: {0}'.format(time.asctime()))
                netbios_name = self._get_bios_name(self.uut[Fields.internal_ip_address])[0]
                utilities.info('Connecting SMB with user="{0}", pass="{1}" client="{2}", host="{3}"'.format(username,
                                password, socket.gethostname(), self.uut[Fields.internal_ip_address]))
                conn = SMBConnection.SMBConnection(username, password, my_name=socket.gethostname(), remote_name=netbios_name, use_ntlm_v2=False)
                # Todo: Investigate using if not conn.connect(self.uut[Fields.internal_ip_address]): instead of the assert
                try:
                    assert conn.connect(self.uut[Fields.internal_ip_address])
                except AssertionError:
                    raise NetworkSharesError('Failed to connect to SMB share with user="{0}", pass="{1}" client="{2}", host="{3}"'.format(self.uut[Fields.web_username], self.uut[Fields.web_password], socket.gethostname(), self.uut[Fields.internal_ip_address]))
                for shareddevice in conn.listShares():
                    self.log.debug('Share: {0} ({1})'.format(shareddevice.name, shareddevice.comments))
                return conn
            except socket.error:
                if timeit.default_timer() - start > timeout:
                    raise
                utilities.warn(traceback.format_exc())
                time.sleep(10)

    @utilities.decode_unicode_args
    def connect_nfs(self):
        """
        Connects to the test unit with the given ip address, username, or admin.
        Default for username is admin, and the default password is blank
        """
        utilities.info('CONNECTING TO SHARE')
        utilities.info('Connecting NFS with user="{0}", pass="{1}" client="{2}", host="{3}"'.format(self.uut[Fields.web_username], self.uut[Fields.web_password], socket.gethostname(), self.uut[Fields.internal_ip_address]))
        conn = nfspy.NfSpy(server=self.uut[Fields.internal_ip_address])
        return conn

    @utilities.decode_unicode_args
    def write_files_to_smb(self, username, password, files, path, share_name, timeout, delete_after_copy, performance):
        """
        Writes the given files to the test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """
        if not files:
            files = []
        if type(files) == str:
            files = [files]
        delete_after_copy = str(delete_after_copy).strip().lower() == 'true'
        performance = str(performance).strip().lower() == 'true'
        utilities.info('WRITING FILES TO SHARE: {0}'.format(time.asctime()))
        utilities.debug('files="{0}"'.format(files))
        utilities.info('share_name="{0}"'.format(share_name))
        if not path.startswith(os.sep):
            path = os.sep + path
        utilities.info('path="{0}"'.format(path))
        conn = self.connect_smb(username, password, timeout)
        remote_files = []
        try:
            self.list_files_on_smb(username, password, share_name, timeout, path)
        except smb.smb_structs.OperationFailure as e:
            self.log.debug(ctl.exception('Failed to write files'))
            raise
        files_to_transfer = []
        total_filesize = 0

        for filename in files:
            abs_filename = os.path.abspath(filename)
            target_path = os.path.join(path, os.path.basename(filename))
            utilities.debug('Writing file: "{0}" to "{1}"'.format(abs_filename, target_path))
            utilities.debug('File exists: "{0}"'.format(os.path.isfile(abs_filename)))
            file_size = os.stat(abs_filename).st_size
            total_filesize += file_size
            files_to_transfer.append((target_path, abs_filename))

        utilities.info('STARTING TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        start = timeit.default_timer()

        for target_path, abs_filename in files_to_transfer:
            with open(abs_filename, 'rb') as file_obj:
                conn.storeFile(service_name=share_name, path=target_path, file_obj=file_obj, timeout=timeout)
        end = timeit.default_timer()
        duration = end - start
        if duration == 0:
            duration = 1

        utilities.info('FINISHED TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        utilities.info('Transfer duration: {0} seconds'.format(int(duration)))
        utilities.info('Transferred data: {0} MiB'.format(int(total_filesize / 1048576)))
        utilities.info('Transfer speed: {0} MiB/s'.format(int(total_filesize / (duration * 1048576))))
        

        for target_path, abs_filename in files_to_transfer:
            if delete_after_copy:
                utilities.info('Deleting: {0}'.format(abs_filename))
                os.unlink(abs_filename)
            remote_files.append(target_path)
            
        if delete_after_copy: 
            # Remove temporary folder
            tempDir = os.getcwd()
            if '/tmp' in tempDir:
                utilities.info('Deleting temporary folder: {0}'.format(tempDir))
                shutil.rmtree(tempDir)
                # Change directory to parent directory    
                os.chdir('..')                  
        self.list_files_on_smb(username, password, share_name, timeout, path)
        return remote_files, duration

    @utilities.decode_unicode_args
    def write_files_to_nfs(self, files=None, path=os.sep, share_name=utilities.TEST_SHARE_NAME, timeout=30, delete_after_copy=True):
        """
        Writes the given files to the given share on the test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """
        if not files:
            files = []
        delete_after_copy = str(delete_after_copy).strip().lower() == 'true'
        utilities.info('WRITING FILES TO SHARE')
        utilities.info('files="{0}"'.format(files))
        utilities.info('share_name="{0}"'.format(share_name))
        if not path.startswith(os.sep):
            path = os.sep + path
        utilities.info('path="{0}"'.format(path))
        conn = self.connect_nfs()
        remote_files = []
        self.list_files_on_nfs(path=path, share_name=share_name, timeout=timeout)
        for filename in files:
            abs_filename = os.path.abspath(filename)
            target_path = os.path.join(path + share_name, os.path.basename(filename))
            utilities.info('Writing file: "{0}" to "{1}"'.format(abs_filename, target_path))
            utilities.info('File exists: "{0}"'.format(os.path.isfile(abs_filename)))
            with open(abs_filename, 'rb') as file_obj:
                conn.Write(service_name=share_name, path=target_path, file_obj=file_obj, timeout=timeout)
            if delete_after_copy:
                os.unlink(abs_filename)
            remote_files.append(target_path)
        self.list_files_on_nfs(path=path, share_name=share_name, timeout=timeout)
        return remote_files

    @utilities.decode_unicode_args
    def read_files_from_smb(self, username, password, files, path, share_name, timeout, delete_after_copy, performance):
        """
        Read files from a given share, copy to local workspace to calculate the transfer rate
        If file does not exist, it creates a empty file at the workspace (should error out?)
        """

        if not files:
            files = []
        if type(files) == str:
            files = [files]

        delete_after_copy = str(delete_after_copy).strip().lower() == 'true'
        performance = str(performance).strip().lower() == 'true'
        utilities.info('READING FILES FROM SHARE: {0}'.format(time.asctime()))
        utilities.info('debug="{0}"'.format(files))
        utilities.info('share_name="{0}"'.format(share_name))
        if not path.startswith(os.sep):
            path = os.sep + path
        utilities.info('path="{0}"'.format(path))
        conn = self.connect_smb(username, password, timeout)
        server_files = self.list_files_on_smb(username, password, share_name, timeout, path)

        
        # create a temp folder
        # Note:change directory back to a valid location since the generate_test_files function
        # leaves the current directory set to a temp directory that does not exist anymore.
        #os.chdir('..')
        currentDir = os.getcwd()
        if '/tmp' not in currentDir:
            tempDir = tempfile.mkdtemp(dir=currentDir)
            os.chdir(tempDir)
        
        files_to_transfer = []
        total_filesize = 0

        for filename in files:
            if filename in server_files:
                utilities.debug('Reading file: "{0}"'.format(filename))
                basename = os.path.basename(filename)
                path_filename = path + filename
                files_to_transfer.append((basename, filename, path_filename))
            else:
                utilities.info('{0} does not exist'.format(filename))
        utilities.info('STARTING TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        start = timeit.default_timer()

        for basename, filename, path_filename in files_to_transfer:
            with open(basename, 'wb') as file_obj:
                conn.retrieveFile(service_name=share_name, path=path_filename, file_obj=file_obj, timeout=timeout)

        end = timeit.default_timer()
        duration = end - start
        if duration == 0:
            duration = 1

        utilities.info('FINISHED TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        utilities.info('Transfer duration: {0} seconds'.format(int(duration)))

        for basename, filename, path_filename in files_to_transfer:
            file_size = os.stat(filename).st_size
            total_filesize += file_size
            utilities.debug('"{0}" exists: "{1}"'.format(filename, os.path.isfile(filename)))

            if delete_after_copy:
                conn.deleteFiles(service_name=share_name, path_file_pattern=path + filename, timeout=timeout)


        utilities.info('Transferred data: {0} MiB'.format(int(total_filesize / 1048576)))
        utilities.info('Transfer speed: {0} MiB/s'.format(int(total_filesize / (duration * 1048576))))

        total_filesize = int(total_filesize / 1048576)
        if delete_after_copy:
            # Remove temporary folder
            tempDir = os.getcwd()
            if '/tmp' in tempDir:
                utilities.info('Deleting temporary folder: {0}'.format(tempDir))
                shutil.rmtree(tempDir)
                os.chdir('..')
        return files, duration, total_filesize

    # Delete a file from SMB, provided file name
    @utilities.decode_unicode_args
    def delete_file_from_smb(self, filename, username, password, share_name, timeout):
        """
        Deletes a single file (using the given filename) on the given share on the
        test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """
        filename = os.sep + filename
        utilities.info('DELETING FILE FROM SHARE: {0}'.format(time.asctime()))
        utilities.info('filename="{0}"'.format(filename))
        utilities.info('share_name="{0}"'.format(share_name))
        if not filename.startswith(os.sep):
            filename = os.sep + filename
        conn = self.connect_smb(username, password, timeout)
        #try:
        conn.deleteFiles(service_name=share_name, path_file_pattern=filename, timeout=timeout)
        utilities.info('{0}{1} was deleted from smb'.format(share_name, filename))
        #except Exception as e:
        #    self.log.warning(ctl.exception('Failed to delete file through SMB'))

    @utilities.decode_unicode_args
    def delete_files_from_smb(self, username, password, share_name, timeout, pattern):
        """
        Deletes all files using the given pattern on the given share on the
        test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """

        utilities.info('DELETING FILES FROM SHARE: {0}'.format(time.asctime()))
        utilities.info('pattern="{0}"'.format(pattern))
        utilities.info('share_name="{0}"'.format(share_name))
        if not pattern.startswith(os.sep):
            pattern = os.sep + pattern
        conn = self.connect_smb(username, password, timeout)
        conn.deleteFiles(service_name=share_name, path_file_pattern=pattern, timeout=timeout)

    @utilities.decode_unicode_args
    def delete_all_files_from_smb(self, username, password, share_name, timeout, path):
        """
        Deletes all files on the given share on the
        test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """
        utilities.info('DELETING ALL FILES ON SHARE: {0}'.format(time.asctime()))
        files = self.list_files_on_smb(username, password, share_name, timeout, path)
        for filename in files:
            #try:
            self.delete_files_from_smb(username, password, share_name, timeout, pattern=filename)
            utilities.info('Files were deleted from smb')
            #except Exception as e:
            #    self.log.warning(ctl.exception('Failed to delete file through SMB'))

    @utilities.decode_unicode_args
    def list_files_on_smb(self, username, password, share_name, timeout, path):
        """
        Lists all files on the given share on the
        test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """
        if not path.startswith(os.sep):
            path = os.sep + path
        utilities.info('path="{0}"'.format(path))
        utilities.info('share_name="{0}"'.format(share_name))
        conn = self.connect_smb(username, password, timeout)
        files = []
        for sharedfile in conn.listPath(share_name, path):
            utilities.debug('"{0}"'.format(sharedfile.filename))
            files.append(sharedfile.filename)
        return files

    @utilities.decode_unicode_args
    def list_files_on_nfs(self, path=os.sep, share_name=utilities.TEST_SHARE_NAME, timeout=30):
        """
        Lists all files on the given share on the
        test unit with the given ip address, username, and password.
        Default username is admin, default password is blank
        """
        utilities.info('LISTING FILES ON SHARE')
        if not path.startswith(os.sep):
            path = os.sep + path
        utilities.info('path="{0}"'.format(path))
        utilities.info('share_name="{0}"'.format(share_name))
        conn = self.connect_nfs()
        time.sleep(3)
        files = []
        # nfs://192.168.6.132/nfs/SmartWare
        for sharedfile in conn.Listdir(share_name, path):
            utilities.info('"{0}"'.format(sharedfile.filename))
            files.append(sharedfile.filename)
        return files


    #@utilities.decode_unicode_args
    def cmd_run(self, cmd):
        self.log.debug('running command: ' + str(cmd))
        return subprocess.check_output(cmd, shell=True)

    @utilities.decode_unicode_args
    def generate_test_files(self, kilobytes=50, file_count=50, extension='jpg', destination=None, prefix=None):
        """ Create some files for transferring. Return the list of files.
        """
        # create a temp directory to store generated files
        # os.chdir('../test') do not need this anymore
        if not destination:
            current_dir = os.getcwd()
            temp_dir = tempfile.mkdtemp(dir=current_dir)
        else:
            temp_dir = destination

        self.log.info('Creating {0} file(s) of size {1} KB and extension {2}'.format(file_count, kilobytes, extension))

        if not prefix:
            prefix = 'file'

        while utilities.free_disk_space('/') < (int(kilobytes) * int(file_count) * 1024):
            free_space = utilities.free_disk_space(temp_dir)
            size = int(kilobytes) * int(file_count) * 1024
            self.log.info('Not enough free disk space to generate test files: {0} bytes < {1} bytes'.
                          format(free_space, size))
            raise exception.InsufficientFreeSpace()

        kilobytes = int(kilobytes)
        files = []
        time.sleep(3)
        for i in range(int(file_count)):
            filename = '{0}{1}.{2}'.format(prefix, i, extension)
            file_path = os.path.join(temp_dir, filename)
            with open(file_path, 'w') as testfile:
                buf_list = list(os.urandom(1024))
                buf = ''.join(buf_list)
                for j in range(kilobytes):
                    testfile.write(buf)
                    if not (j % 100):
                        random.shuffle(buf_list)
                        buf = ''.join(buf_list)
            files.append(file_path)
        utilities.info('Generated files: {0}'.format(files))
        return files


    @utilities.decode_unicode_args
    def generate_test_file(self, kilobytes=50, extension='jpg', prefix=None):
        '''Create a single test file and return the filename.'''
        return self.generate_test_files(kilobytes, 1, extension, prefix=prefix)[0]


    @utilities.decode_unicode_args
    def hashfile(self, filepath):
        """
        Applies hash on a given file
        """
        return utilities.hashfile(filepath)

    @utilities.decode_unicode_args
    def checksum_files_on_share(self, share_name=utilities.TEST_SHARE_NAME):

        common = inherit('CommonTestLibrary')
        myCommand = 'find /shares/' + str(share_name) + '/* -type f -exec busybox md5sum {} + | awk ' + "'{print $1}'" + ' | sort | busybox md5sum'
        checksum = ','.join(common.run_on_device(command=myCommand, ip_address=self.uut[Fields.internal_ip_address]))
        # strip anything other than  alphanumeric and return final string
        return re.sub(r'\W+', '', checksum)

    @utilities.decode_unicode_args
    def generate_files_on_workspace(self, kilobytes=50, file_count=50, extension='jpg'):
        return self.generate_test_files(kilobytes, file_count, extension)

    #@utilities.decode_unicode_args
    def checksum_files_on_workspace(self, dir_path=None, method='sha256'):
        strPath = ''.join(dir_path)
        if dir_path is None:
            dir_path = os.getcwd()
        self.log.debug('current directory: {}'.format(strPath))
        checksum = gethash(filename=strPath, method=method)
        
        return checksum
    
    def send_magic_packet(self):
        mac_address =  self.uut[Fields.mac_address]
        self.log.info('mac address: %s' %mac_address)
        nas_ip_address = self.uut[Fields.internal_ip_address]
        broadcast_tmp = nas_ip_address.split('.')
        broadcast_tmp = '.'.join(broadcast_tmp[0:3])
        broadcast_ip = broadcast_tmp+'.255'
        self.log.info('Broadcast IP: %s' %broadcast_ip)
        wol.send_magic_packet(self.uut[Fields.mac_address],ip_address=broadcast_ip,port=9)

    ########################################################################################
    #  New FTP functions                                                                   #
    #    * connect_ftp(), read_files_from_ftp(), write_files_to_ftp()                      #
    #  Please make sure you enable the ftp and ftp share access in TestClient              #
    #    * enable_ftp_service(), enable_ftp_share_access()                                 #
    ########################################################################################
    @utilities.decode_unicode_args
    def connect_ftp(self, ftp_ip=None, username=None, password=None, retry=3):
        """
            Connect ftp

        :param ftp_ip: target ftp ip address or uut if not specified
        :param username: user name for ftp account (if not specified, assume this is an anonymous login)
        :param password: user password for ftp account
        :param retry: how many retries to establish the ftp connection
                      By default, retry is set to 3.
                      If set to -1 , it means no retry if exception occurs
        :return: ftp connection object
        """
        ftp_ip = ftp_ip or self.uut[Fields.internal_ip_address]
        useAnonymous = False

        if not username:
            useAnonymous = True

        ftpconn = None
        while True:
            try:
                ftpconn = FTP(ftp_ip)
            except Exception as e:
                if retry == -1:  # No retry to verify the connection
                    raise
                elif retry == 0:
                    self.log.error("Reached maximum retries to make FTP connection.")
                    raise
                elif retry > 0:
                    self.log.info("Remaining {} retries: FTP connection failed, exception: {}, ".format(retry, repr(e)))
                    retry -= 1
                    time.sleep(1)
                    continue
            else:
                if ftpconn:
                    self.log.info("FTP connection to {} is established successfully.".format(ftp_ip))
                    break
                else:
                    retry -= 1

        # Anonymous login
        if useAnonymous:
            try:
                ftpconn.login()
            except Exception as e:
                self.log.info("Anonymous failed to login. exception: {}".format(repr(e)))
                raise
            else:
                self.log.info("Anonymous login is succeeded.")

        # account login
        else:
            try:
                ftpconn.login(username, password)
            except Exception as e:
                self.log.info("(user={}, pwd={}) account failed to login. "
                              "exception: {}".format(username, password, repr(e)))
                raise
            else:
                self.log.info("(user={}, pwd={}) account login succeeded.".format(username, password))

        return ftpconn

    @utilities.decode_unicode_args
    def read_files_from_ftp(self, ftp_ip=None, username=None, password=None, retries=3,
                            files=None, share_name='Public', delete_after_copy=False):
        """
            Download
            1) Make sure you enable ftp
            2) Make sure you enable ftp share access
            share_name = 'Public' if anonymous

        :param ftp_ip: target ftp ip address or uut if not specified
        :param username: user name for ftp account (if not specified, assume this is an anonymous login)
        :param password: user password for ftp account
        :param retries: how many retries to establish the ftp connection
                        By default, retry is set to 3.
                        If set to -1 , it means no retry if exception occurs
        :param files: files going to read from ftp under share_name
        :param share_name: the name of the share folder which will be used for upload files
        :param delete_after_copy: True or False.
                                  If set to True, all files in /tmp folders will be deleted at the end
        :return: (files_to_transfer, duration)
                 how many files being transferred and duration
        """
        self.log.info("=========== START - FTP download temporary files for read access test ===========")
        try:
            conn = self.connect_ftp(ftp_ip, username, password, retries)
        except Exception as e:
            self.log.error("Unable to setup the ftp connection. Program exit due to exception: {}".format(repr(e)))
            exit(1)
        # Switch 1-level folder
        try:
            conn.cwd(share_name)
        except Exception as e:
            self.log.debug('Available Directories:')
            conn.dir()
            self.log.error("Unable to switch to the share folder: {}, exception: {}".format(share_name, repr(e)))
            raise
        else:
            self.log.info("Switch to {} folder".format(share_name))

        # Get available files under share_name folder
        serverfiles = conn.nlst()

        # Temporary files
        if not files:
            files = []
        if type(files) == str:
            files = [files]

        files = map(os.path.basename, files)  # get filename instead of abspath
        currentDir = os.getcwd()
        if '/tmp' not in currentDir:
            tempDir = tempfile.mkdtemp(dir=currentDir)
            os.chdir(tempDir)

        # Going to download files from ftp
        files_to_transfer = []
        total_filesize = 0
        self.log.info('STARTING TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        start = timeit.default_timer()

        for filename in files:
            try:
                if filename in serverfiles:
                    with open(filename, "wb") as f:
                        def callback(data):
                            f.write(data)
                        resp = conn.retrbinary("RETR "+filename, callback, blocksize=1048576)
                    if '226' in resp:  # File successfully transferred
                        self.log.info("Succeed to save download file to {}".format(os.path.abspath(filename)))
                        files_to_transfer.append(filename)
                        total_filesize += os.stat(filename).st_size
                    else:
                        self.log.error("Upload {} failed".format(os.path.basename(filename)))
                else:
                    self.log.info("{} file does not exist.".format(filename))
            except error_perm:
                self.log.debug('Available Directories:')
                conn.dir()
                raise
            except Exception as e:
                raise Exception("FAILED: ftpReadTest failed, can not download {}, "
                                "exception: {}".format(os.path.basename(filename), repr(e)))

        end = timeit.default_timer()
        duration = end - start
        if duration == 0:
            duration = 1

        conn.close()

        self.log.info('FINISHED TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        self.log.info('Transfer duration: {0} seconds'.format(int(duration)))
        self.log.info('Transferred data: {0} MiB'.format(int(total_filesize / 1048576)))
        self.log.info('Transfer speed: {0} MiB/s'.format(int(total_filesize / (duration * 1048576))))

        if delete_after_copy:
            # Remove temporary folder
            tempDir = os.getcwd()
            if '/tmp' in tempDir:
                self.log.info('Deleting temporary folder: {0}'.format(tempDir))
                shutil.rmtree(tempDir)
                os.chdir('..')

        self.log.info("=========== END - FTP download temporary files for read access test ===========")
        return (files_to_transfer, duration)

    @utilities.decode_unicode_args
    def write_files_to_ftp(self, ftp_ip=None, username=None, password=None, retries=3,
                           files=None, share_name='Public', delete_after_copy=True):
        """
            Upload
            1) Make sure you enable ftp
            2) Make sure you enable ftp share access
            share_name = 'Public' if anonymous

        :param ftp_ip: target ftp ip address or uut if not specified
        :param username: user name for ftp account (if not specified, assume this is an anonymous login)
        :param password: user password for ftp account
        :param retries: how many retries to establish the ftp connection
                        By default, retry is set to 3.
                        If set to -1 , it means no retry if exception occurs
        :param files: files going to read from ftp under share_name
        :param share_name: the name of the share folder which will be used for upload files
        :param delete_after_copy: True or False.
                                  If set to True, all files in /tmp folders will be deleted at the end
        :return: (files_to_transfer, duration)
                 how many files being transferred and duration
        """
        self.log.info("=========== START - FTP upload temporary files for write access test ===========")
        try:
            conn = self.connect_ftp(ftp_ip, username, password, retries)
        except Exception as e:
            self.log.error("Unable to setup the ftp connection. Program exit due to exception: {}".format(repr(e)))
            exit(1)
        # Switch one-level folder
        try:
            conn.cwd(share_name)
        except Exception as e:
            self.log.debug('Available Directories: ')
            conn.dir()
            self.log.error("Unable to switch to the share folder: {}, exception: {}".format(share_name, repr(e)))
            raise
        else:
            self.log.info("Switch to {} folder".format(share_name))

        # Temporary files
        if not files:
            files = []
        if type(files) == str:
            files = [files]

        files = map(os.path.abspath, files)  # get abspath
        # Going to write files to ftp
        total_filesize = 0
        for file in files:
            absFilename = os.path.abspath(file)
            file_size = os.stat(absFilename).st_size
            total_filesize += file_size

        self.log.info('STARTING TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        start = timeit.default_timer()

        for filename in files:
            try:
                with open(filename, "rb") as f:
                    resp = conn.storbinary("STOR "+os.path.basename(filename), f, 1048576) # 1048576 = 1MB
                if '226' in resp: # File successfully transferred
                    self.log.info("{} is uploaded successfully".format(os.path.basename(filename)))
                else:
                    self.log.error("Upload {} failed".format(os.path.basename(filename)))
            except error_perm:
                self.log.debug('Available Directories: ')
                conn.dir()
                raise
            except Exception as e:
                raise Exception("FAILED: ftpWriteTest failed, unable to upload {} onto ftp server. "
                                "exception: {}".format(os.path.basename(filename), repr(e)))

        end = timeit.default_timer()
        duration = end - start
        if duration == 0:
            duration = 1

        conn.close()

        self.log.info('FINISHED TRANSFER: {0}, {1}, {2}'.format(time.asctime(), time.clock(), time.time()))
        self.log.info('Transfer duration: {0} seconds'.format(int(duration)))
        self.log.info('Transferred data: {0} MiB'.format(int(total_filesize / 1048576)))
        self.log.info('Transfer speed: {0} MiB/s'.format(int(total_filesize / (duration * 1048576))))

        if delete_after_copy:
            # Remove temporary folder
            tempDir = os.getcwd()
            if '/tmp' in tempDir:
                self.log.info('Deleting temporary folder: {0}'.format(tempDir))
                shutil.rmtree(tempDir)
                # Change directory to parent directory
                os.chdir('..')
        self.log.info("=========== END - FTP upload temporary files for write access test ===========")
        return (files, duration)


