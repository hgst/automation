'''
Created on August 1, 2014

'''
import threading
import paramiko
import socket
import time

from global_libraries import utilities
from global_libraries.testdevice import Fields
import global_libraries.CommonTestLibrary as ctl

paramiko.util.log_to_file('paramiko.txt')

power_lock = threading.RLock()
POWER_SWITCH_CYCLETIME = 3

try:
    from dlipower import dlipower
except ImportError:
    import dlipower

class PowerSwitchError(Exception):
    pass

class PowerSwitchClient(object):

    def __init__(self, uut):
        self.uut = uut
        self.log = ctl.getLogger('AAT.powerswitchclient')

    @utilities.decode_unicode_args
    def power_command(self, power_switch_ip=None, power_switch_port=None, cycletime=POWER_SWITCH_CYCLETIME, command='cycle', userid='admin', password='1234'):

        if power_switch_ip is None:
            power_switch_ip = self.uut[Fields.power_switch]

        self.log.debug('Power switch attributes: ' + str(power_switch_ip) + str(power_switch_port) + command)
        """
        Run the specified web power switch command.
        """
        power_lock.acquire()

        try:
            self.log.debug('Starting: Power {0} port {1} on power switch {2}'.format(command, power_switch_port, power_switch_ip))
            self.log.debug('Value of conditional Boolean: {0}'.format(True))
            while True:
                try:
                    try:
                        self.log.debug('Inside True valuation -- Power {0} port {1} on power switch {2}'.format(command, power_switch_port, power_switch_ip))

                        powerswitch = dlipower.powerswitch(cycletime=cycletime, hostname=power_switch_ip,
                                                       userid=userid, password=password)
                    except AttributeError:
                        powerswitch = dlipower.PowerSwitch(cycletime=cycletime, hostname=power_switch_ip,
                                                       userid=userid, password=password)
                    if power_switch_port:
                        return powerswitch.command_on_outlets(command=command,
                                                   outlets=[power_switch_port])
                    elif command == 'statuslist':
                        return powerswitch.statuslist()
                    time.sleep(5)
                    break
                except (socket.timeout, socket.error):
                    pass
        except:
            power_lock.release()
            raise
        power_lock.release()
        self.log.debug('Finished: Power {0} port {1} on power switch {2}'.format(command, power_switch_port, power_switch_ip))

    @utilities.decode_unicode_args
    def _power_command(self, power_switch_ip=None, power_switch_port=None, cycletime=None, command='cycle'):
        """
        Run the specified web power switch command.
        """
        try:
            return self.power_command(power_switch_ip, power_switch_port, cycletime, command)
        except AttributeError:
            raise PowerSwitchError('No power switch info found for this test unit.')

    @utilities.decode_unicode_args
    def power_cycle(self, power_switch_ip=None, power_switch_port=None, cycletime=POWER_SWITCH_CYCLETIME, msg=' [POWER] Power cycle!'):
        """Power cycle the given power switch IP and port"""
        if power_switch_port is None:
            power_switch_port = self.uut[Fields.power_switch_port]

        self._power_command(power_switch_ip, power_switch_port, cycletime=cycletime, command='cycle')

    @utilities.decode_unicode_args
    def power_on(self, power_switch_ip=None, power_switch_port=None, msg=' [POWER] Power on!'):
        """Power on the a given power switch IP and port"""
        if power_switch_port is None:
            power_switch_port = self.uut[Fields.power_switch_port]

        self._power_command(power_switch_ip, power_switch_port, command='on')

    @utilities.decode_unicode_args
    def power_off(self, power_switch_ip=None, power_switch_port=None, msg=' [POWER] Power off!'):
        """ Turn off a power to an outlet. False = success, True = Fail """
        if power_switch_port is None:
            power_switch_port = self.uut[Fields.power_switch_port]

        self._power_command(power_switch_ip, power_switch_port, command='off')

    @utilities.decode_unicode_args
    def outlet_status(self, power_switch_ip=None, power_switch_port=None, msg=' [POWER] Outlet Status!'):
        """ Return the status of an outlet, returned value will be one of: ON,  OFF, Unknown """
        if power_switch_port is None:
            power_switch_port = self.uut[Fields.power_switch_port]

        return self._power_command(power_switch_ip, power_switch_port, command='status')

    @utilities.decode_unicode_args
    def outlet_status_list(self, power_switch_ip=None, msg=' [POWER] Power Status List!'):
        """ Return the status of all outlets in a list """
        return self._power_command(power_switch_ip, command='statuslist')
