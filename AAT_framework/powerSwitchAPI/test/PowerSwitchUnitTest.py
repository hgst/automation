'''
#
# Created on August 6, 2014
# @author: Jason Tran
#
# This module contains the class and methods to run PowerSwitch API test cases.
#
'''

import unittest
import logging

from powerSwitchAPI.src.powerswitchclient import PowerSwitchClient
from UnitTestFramework import mylogger
from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import TestClient

l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('PowerSwitchAPIUnitTest')

# Variables to be used for following tests
PowerSwitchIP = '192.168.1.121'
port = 1
command_on = 'on'
command_off = 'off'
cycle_time = 10

test = TestClient(ip_address='192.168.1.120')
test.uut[Fields.power_switch] = PowerSwitchIP
test.uut[Fields.power_switch_port] = port



class PowerSwitchUnitTest(unittest.TestCase):

    def setUp(self):
        logger1.info('Start executing %s', self._testMethodName)

        # instantiate PowerSwitchAPI class
        try:
            self.myP = PowerSwitchClient(test.uut)
        except Exception, ex:
            logger1.info('Failed to instantiate\n' + str(ex))

    def tearDown(self):
        logger1.info('Finished executing %s', self._testMethodName)

    def test_power_on(self):
        try:
            self.myP.power_on()
        except Exception, ex:
            logger1.info('Failed to power on\n' + str(ex))

    def test_power_off(self):
        try:
            self.myP.power_off()
        except Exception, ex:
            logger1.info('Failed to power off\n' + str(ex))

    def test_power_cycle(self):
        try:
            self.myP.power_cycle()
        except Exception, ex:
            logger1.info('Failed to power cycle\n' + str(ex))

    def test_power_command(self):
        try:
            self.myP.power_command(PowerSwitchIP, port, cycle_time)
        except Exception, ex:
            logger1.info('Failed to power off\n' + str(ex))

    def test_power_command_on(self):
        try:
            self.myP.power_command(PowerSwitchIP, port, cycle_time, command_on)
        except Exception, ex:
            logger1.info('Failed to power on\n' + str(ex))

    def test_power_command_off(self):
        try:
            self.myP.power_command(PowerSwitchIP, port, cycle_time, command_off)
        except Exception, ex:
            logger1.info('Failed to power off\n' + str(ex))

    def test_outlet_status(self):
        try:
            status = self.myP.outlet_status()
            logger1.info('Status of port %s: %s', str(port), status)
        except Exception, ex:
            logger1.info('Failed to get status of %s, %s \n', port, str(ex))

    def test_outlet_status_list(self):
        try:
            status = self.myP.outlet_status_list()
            logger1.info('Status of all port: %s', status)
        except Exception, ex:
             logger1.info('Failed to get status of all ports, %s \n', str(ex))


if __name__ == '__main__':
    # # To run whole test suite
    unittest.main()

    '''
    ##specify test(s) to execute individual
    suite = unittest.TestSuite()
    suite.addTest(PowerSwitchUnitTest("test_outlet_status"))
    suite.addTest(PowerSwitchUnitTest("test_outlet_status_list"))
    runner = unittest.TextTestRunner()
    #runner.run(suite)
    unittest.TextTestRunner(verbosity=2).run(suite)
    '''
