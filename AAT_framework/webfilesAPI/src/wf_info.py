""" Contains default values for Webfiles server configuration

"""
__author__ = 'naralasetty_s'

import copy
from wf_keys import Keys


class WebFilesInfo(object):
    """  A set of dictionaries with values specific to the selected server (stage 6, Production, etc.)
    """
    wf_server_url = {'stage6': 'webfiles:omgWebTopTeamIsTheB3steam!@stage6webfiles.remotewd1.com',
                     'stage10': 'webfiles:omgWebTopTeamIsTheB3steam!@stage10webfiles.remotewd5.com'}


class WebFilesDefault(object):
    """ Used to track default values for certain fields per server

        Each dictionary is a set of default values per server.
        Default is meant to be the base used by all servers.
        It should be copied to a new dictionary for each supported server
        and any unique defaults overwritten by assigning new values to the new
        dictionary.

        In addition, each supported server should be in server_list.
    """

    default_username = 'user'
    default_password = 'password'
    default_browser = 'chrome'

    default = {Keys.wf_user: default_username,
               Keys.wf_password: default_password,
               Keys.wf_browser: default_browser}

    stage6 = copy.deepcopy(default)
    stage10 = copy.deepcopy(default)

    stage10[Keys.wf_user] = 'stage10user'

    server_list = {'stage_6': stage6,
                   'stage_10': stage10}
