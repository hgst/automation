""" Webfiles class. Test cases should subclass the WebFiles class.

"""
__author__ = 'hoffman_t'

from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.wf_seleniumclient import WfSeleniumClient
from wf_configure import Server
from wf_keys import Keys

from global_libraries.testdevice import Fields
# noinspection PyPep8Naming
from global_libraries import CommonTestLibrary as ctl


class WebFiles(TestClient):
    """ This class extends the TestClient API to support Webfiles
    """
    def wf_launch(self, use_ssl=True):
        """ Launches a web browser and connects to the Webfiles main page with the sign in button
        :param use_ssl: If True, connect with HTTPS
        """
        self._wf_sel.wf_launch(use_ssl=use_ssl)

    def wf_login(self, user, password, use_ssl=True):
        """ Launches the WF UI, clicks the login button, enters the user and password, and logs in

        :param user: The user to login with
        :param password: The password to use
        :param use_ssl: If True, use HTTPS
        """
        self._wf_sel.wf_login(user, password, use_ssl)

    def _wf_cleanup(self):
        # Runs final cleanup for Webfiles
        self._run_cleanup(self._wf_sel.close_all_browsers)

    # Methods that override the TestClient class
    # noinspection PyPep8Naming
    def __init__(self,
                 checkDevice=True,
                 ip_address=None,
                 selenium_timeout=30,
                 device_file=None,
                 health_monitor_interval=10,
                 health_monitor_thresholds=None):
        TestClient.__init__(self,
                            checkDevice=checkDevice,
                            ip_address=ip_address,
                            selenium_timeout=selenium_timeout,
                            device_file=device_file,
                            health_monitor_interval=health_monitor_interval,
                            health_monitor_thresholds=health_monitor_thresholds,
                            execute_test=False)

        # Merge the wf_keys with Fields for easier debugging
        for key in ctl.get_class_attributes(Keys):
            setattr(Fields, key[0], key[1])

        server = Server(self.uut)
        self.wf = server.settings

        if self.wf[Keys.wf_browser] is None:
            # Default to Chrome browser
            self.uut[Keys.wf_browser] = 'chrome'

        if self.wf[Keys.wf_browser] is None:
            # Default to Chrome browser
            self.uut[Keys.wf_browser] = 'chrome'

        self._wf_sel = WfSeleniumClient(self.wf, self.uut, wait_time=selenium_timeout)

        # Switch the Selenium client to use Webfiles, but first save the UUT's client
        self._uut_sel = self._sel
        self._sel = self._wf_sel

        self._execute_test()

    def end(self):
        """ Close all browser windows. Needed in case cleanup was skipped.
        """
        self._wf_cleanup()
        exit(self._result)

    def take_screen_shot(self, message='Screenshot', prefix=None):
        """
            Take Screen Shot for debugging

            @param prefix: If given, prefix will be part of screenshot filename. Otherwise, use TC filename by default
            @param message: If given, the message to include in the log
        """
        self._take_screen_shot(message, prefix, self._wf_sel)

