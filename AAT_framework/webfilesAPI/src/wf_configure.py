""" Configures the Webfiles server settings
"""
__author__ = 'hoffman_t'


# noinspection PyPep8Naming
from global_libraries import CommonTestLibrary as ctl
from global_libraries import wd_exceptions

from webfilesAPI.src.wf_info import WebFilesInfo
from webfilesAPI.src.wf_info import WebFilesDefault

from webfilesAPI.src.wf_keys import Keys
from global_libraries.testdevice import Fields
from global_libraries.product_info import ProductList  # Needed to get DEFAULTS constant


class Server(object):
    """ Settings for the Webfiles server being tested

    """
    def __init__(self, uut_settings, server_url=None):
        self.settings = {}

        # Copy all wf_ parameters from the uut settings dictionary to the server settings
        for key in uut_settings:
            if key.startswith('wf_'):
                self.settings[key] = uut_settings[key]

        # Now, remove them from the uut dictionary. This isn't 100% necessary, but since the values in the Server's
        # settings dictionary may change, this makes debugging easier by not having duplicate keys in both places.
        for key in self.settings:
            del uut_settings[key]

        self._set_wf_values()

        if server_url:
            self.settings[Keys.wf_server_url] = server_url

    def _set_wf_values(self):
        # Set the WebFiles dictionary values based off current server
        try:
            server = self.settings[Keys.wf_server]
        except KeyError:
            raise wd_exceptions.InvalidParameter('Must define {} in test_device.txt or '.format(Keys.wf_server) +
                                                 'through Silk')

        # Get a list of all class attributes from wf_info's WebFilesInfo class
        wf_attributes = ctl.get_class_attributes(WebFilesInfo)

        # For each attribute, assign it to the wf dictionary
        for attribute in wf_attributes:
            self._set_value_wf(attribute[0], server)

        # Set the default values
        if server in WebFilesDefault.server_list:
            defaults = WebFilesDefault.server_list[server]
        else:
            defaults = WebFilesDefault.default

        for key in defaults:
            if key not in self.settings:
                self.settings[key] = defaults[key]

    def _set_value_wf(self, key_name, server):
        # Sets the value for key_name for the given server
        values = getattr(WebFilesInfo, key_name)
        if server in values:
            value = values[server]
        else:
            value = values.get(ProductList.DEFAULT, 'N/A')

        self.settings[key_name] = value
        setattr(Fields, key_name, key_name)
