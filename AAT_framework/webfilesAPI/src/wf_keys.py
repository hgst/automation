""" A set of keys used by the wf[] dictionary

"""
__author__ = 'hoffman_t'


class Keys(object):
    """ Class used to prevent typos when accessing device fields. These will be copied into the global Fields class
        by the Webfiles class. To access these, use Fields.<field> instead of the constant string. For example,
        instead of 'wf_server', use Fields.wf_server. This will prevent a typo like 'wf_sever' from causing a bug
        that might be hard to track down.

        Note that these must be unique and must be unique when included in Fields. To make it easier to prevent
        duplicates when the classes are merged, each Webfiles key should begin with "wf_"
    """
    # The browser to use for Webfiles
    wf_browser = 'wf_browser'

    # WebFiles key names
    wf_server = 'wf_server'
    wf_server_url = 'wf_server_url'
    wf_user = 'wf_user'
    wf_password = 'wf_password'
    wf_default_user = 'wf_default_user'
    wf_default_password = 'wf_default_password'
