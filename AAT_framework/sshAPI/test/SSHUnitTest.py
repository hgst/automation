'''
Created on May 8, 2014
@author: Jason Tran

This module contains the class and methods to run SSH API test cases

'''

import unittest
import logging

'''
## to run tests from command line

#import logger from sibbling directory
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir0 = os.path.dirname(currentdir)
sys.path.insert(0,parentdir0)
from src import SSHClient

#import logger from grandparent directory
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir2 = os.path.dirname(parentdir)
sys.path.insert(0,parentdir2)
from UnitTestFramework import mylogger
'''

from UnitTestFramework import mylogger
from sshAPI.src.sshclient import SshClient
from global_libraries import scp
from global_libraries import utilities
from global_libraries.testdevice import Fields


from testCaseAPI.src.testclient import TestClient

l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('sshAPI')

# # Variables to execute test cases on local test unit
#  Need to update to your unit's values
username = 'sshd'
hostname = '192.168.1.120'
password = 'welc0me'
USBdrive = '/dev/sde1'
mymountpoint = '/mnt/USB/USB1_e1'
firmware_version = '1.03.35'
testfile_dir = 'C:\\temp\\CrossCompiler'
# testfile_dir = '/mnt/hgfs/Trunk-Robot/AAT_framework/serialAPI/src/'

test = TestClient(ip_address=hostname)
test.uut[Fields.ssh_password] = password
test.uut[Fields.ssh_username] = username

class SSHTest(unittest.TestCase):

    # # Method is called at the beginning of test to open SSH connection with test unit
    #  IP must be provided, using defaul username 'admin' and blank password
    #  mylogger class is instantiated
    def setUp(self):
        try:

            logger1.info('Start executing %s', self._testMethodName)

            self.myssh = SshClient(test.uut)

            # connecto to test unit
            self.myssh.connect()

        except Exception, ex:
            logger1.info('Failed to connect\n' + str(ex))
            # raise Exception, ('Failed to connect\n' + str(ex))

    # # Method is called at the end of test to close SSH connection with test unit
    #  IP must be provided, using defaul username 'admin' and blank password
    def tearDown(self):
        try:
            self.myssh.disconnect()
            logger1.info('Finished executing %s', self._testMethodName)
        except Exception, ex:
            logger1.info('Failed to disconnect\n' + str(ex))
            # raise Exception, 'Failed to disconnect\n' + str(ex)

    # # Method to execute a passed in command
    #  Return value is compared to verify if it's executed sucessfully or not
    def test_execute(self):
        returnvalue = self.myssh.execute("uptime")
        result = returnvalue[0]
        expected = 0
        self.assertEqual(result, expected, 'failed to execute')

    # # Method to retrieve SSH username from test unit
    #  Return value is compared to expected value of 'sshd'
    def test_get_ssh_username(self):
        returnvalue = self.myssh.get_ssh_username()
        print returnvalue
        expected = 'sshd'
        self.assertEqual(returnvalue, expected, 'ssh username does not match')

    # # Method to retrieve SSH password from the test unit
    #  Return value is compared to expected value of 'fituser'
    def test_get_ssh_password(self):
        returnvalue = self.myssh.get_ssh_password()
        print returnvalue
        expected = 'fituser'
        self.assertEqual(returnvalue, expected, 'password does not match')

    # # Method to retrieve unit's current uptime
    #  Exception is thrown if failed
    def test_get_uptime(self):
        try:
            self.myssh.get_uptime()
        except Exception, ex:
            logger1.info('Error in getting uptime\n' + str(ex))
            raise Exception, 'Error in getting uptime\n' + str(ex)

    # # Method to clear unit's alerts
    #  Exception is thrown if failed
    def test_clear_alerts(self):
        try:
            self.myssh.clear_alerts()
        except Exception, ex:
            logger1.info('Failed to clear alerts\n' + str(ex))
            raise Exception, 'Failed to clear alerts\n' + str(ex)

    # # Method to clear unit's alerts
    #  Exception is thrown if failed
    def test_clean_alert(self):
        try:
            self.myssh.clean_alert()
        except Exception, ex:
            logger1.info('Failed to clean alert\n' + str(ex))
            raise Exception, 'Failed to clean alert\n' + str(ex)

    # # Method to create unit's alert
    #  Exception is thrown if failed
    def test_generate_alert(self):
        try:
            self.myssh.generate_alert()
        except Exception, ex:
            logger1.info('Failed to send alert\n' + str(ex))
            raise Exception, 'Failed to send alert\n' + str(ex)

    # # Method to set unit's firmware
    #  Value for firmware_version variable is set at top of this file
    #  Exception is thrown if failed
    def test_set_firmware_version(self):
        try:
            self.myssh.set_firmware_version(firmware_version)
            logger1.info('Set unit with this firmware version: ' + firmware_version)
        except Exception, ex:
            logger1.info('Failed to set firmware version' + str(ex))
            raise Exception, "Failed to set firmware version\n" + str(ex)

    # # Method to get unit's firmware version
    #  Exception is thrown if failed
    def test_get_current_firmware_version(self):
        try:
            self.myssh.get_current_firmware_version()
        except Exception, ex:
            logger1.info('Failed to get current firmware version\n' + str(ex))
            raise Exception, "Failed to get current firmware version\n"

    # # Method to set unit's time
    #  Exception is thrown if failed
    def test_set_uut_time(self):
        try:
            self.myssh.set_uut_time()
            logger1.info('Set UUT time successfully')
        except Exception, ex:
            logger1.info('Failed to set UUT time\n' + str(ex))
            raise Exception, "Failed to set UUT time\n" + str(ex)

    # # Method to get unit's platform
    #  Exception is thrown if failed
    def test_get_platform(self):
        try:
            self.myssh.get_platform()
        except Exception, ex:
            logger1.info('Failed to get platform\n' + str(ex))
            raise Exception, "Failed to get platform\n" + str(ex)

    # # Method to mount a USB drive
    #  Exception is thrown if failed
    def test_mount_drive(self):
        try:
            self.myssh.mount_drive(USBdrive, mymountpoint)
        except Exception, ex:
            logger1.info('Failed to mount USB drive\n' + str(ex))
            raise Exception, "Failed to mount drive\n" + str(ex)

    # # Method to get unmount USB drive attached to test unit
    #  Exception is thrown if failed
    def test_unmount_drive(self):
        try:
            self.myssh.unmount_drive(USBdrive)
        except Exception, ex:
            logger1.info('Failed to unmount USB drive\n' + str(ex))
            raise Exception, "Failed to unmount drive\n" + str(ex)

    # # Method to get USB drive attached to test unit
    #  Exception is thrown if failed
    def test_find_usb_drive(self):
        try:
            self.myssh.find_usb_drive()
        except Exception, ex:
            logger1.info("Failed to find USB drive\n" + str(ex))
            raise Exception, "Failed to find USB drive\n" + str(ex)

    # # Method to find SATA drive on test unit
    #  Exception is thrown if failed
    def test_find_sata_drive(self):
        try:
            self.myssh.find_sata_drive()
        except Exception, ex:
            logger1.info('Failed to find SATA drive\n' + str(ex))
            raise Exception, "Failed to find SATA drive\n" + str(ex)

    # # Method to stop service on test unit
    #  Exception is thrown if failed
    def test_stop_services(self):
        try:
            self.myssh.stop_services()
        except Exception, ex:
            logger1.info('Failed to stop service\n' + str(ex))
            raise Exception, "Failed to stop service\n" + str(ex)

    # # Method to create zero file on test unit
    #  0 is returned if successful
    def test_create_zero_file(self):
        # try:
        #    self.myssh.create_zero_file(mymountpoint)
        # except Exception,ex:
        #    raise Exception, 'Failed to create zero file\n%s' % str(ex)
        returnvalue = self.myssh.create_zero_file(mymountpoint)
        print returnvalue
        result = returnvalue[0]
        expected = 0
        self.assertEqual(result, expected, 'failed to create zero file\n')

    # # Method to format USB drive attached to test unit
    #  0 is returned if successful
    def test_format_usb_drive(self):
        # try:
        #    self.myssh.format_usb_drive(USBdrive)
        # except Exception,ex:
        #    raise Exception, 'Failed to format_usb drive\n%s' % str(ex)
        returnvalue = self.myssh.format_usb_drive(USBdrive)
        print returnvalue
        result = returnvalue[0]
        expected = 0
        self.assertEqual(result, expected, 'Failed to format_usb drive\n')

    # # Method to add an user to test unit
    #  Exception is thrown if failed
    def test_add_user_account(self):
        try:
            self.myssh.add_user_account()
        except Exception, ex:
            logger1.info('Failed to add user\n' + str(ex))
            raise Exception, "Failed to add user\n" + str(ex)

    # # Method to delete user account on test unit
    #  Exception is thrown if failed
    def test_delete_user_account(self):
        try:
            self.myssh.delete_user_account()
        except Exception, ex:
            logger1.info('Failed to delete user\n' + str(ex))
            raise Exception, "Failed to delete user\n" + str(ex)

    # # Method to modify user account on test user
    #  Exception is thrown if failed
    def test_modify_user_account(self):
        try:
            self.myssh.modify_user_account()
        except Exception, ex:
            logger1.info('Failed to modify user\n' + str(ex))
            raise Exception, "Failed to modify user\n" + str(ex)

    # # Method to start AFP service on test unit
    #  Exception is thrown if failed
    def test_start_afp_service(self):
        try:
            self.myssh.start_afp_service()
        except Exception, ex:
            logger1.info('Failed to start AFP service\n' + str(ex))
            raise Exception, "Failed to start AFP service\n" + str(ex)

    # # Method to stop AFP service on test unit
    #  Exception is thrown if failed
    def test_stop_afp_service(self):
        try:
            self.myssh.stop_afp_service()
        except Exception, ex:
            logger1.info('Failed to stop AFP service\n' + str(ex))
            raise Exception, "Failed to stop AFP service\n" + str(ex)

    # # Method to restart AFP service on test unit
    #  Exception is thrown if failed
    def test_restart_afp_service(self):
        try:
            self.myssh.restart_afp_service()
        except Exception, ex:
            logger1.info('Failed to restart AFP service\n' + str(ex))
            raise Exception, "Failed to restart AFP service\n" + str(ex)

    # ## Need to verify - added 9/8 - 1pm.

    # # @brief Open a new SCP connection to the test unit
    #  @param [in] ip_address
    #  @param [optional] username: default username is admin
    #  @param [optional] password: default password is ''
    def test_open_scp_connection(self):
        try:
            status = self.myssh.open_scp_connection()
            print status
        except Exception, ex:
            logger1.info('Error opening scp connection %s', str(ex))

    # # @brief SCP a file or directory from the device
    #  @param [in] path: directory to be copied, or path to file to be copied
    #  @param [in] is_dir: required to pass in with True value if copy directory, default value is False (for file copy)
    def test_scp_from_device(self):
        try:
            self.myssh.scp_from_device(path='/shares/SmartWare/', is_dir=True)
        except Exception, ex:
            logger1.info('Error scp file/folder from device %s', str(ex))

    # # @brief Copy directory/file to test device
    #  @param [in] files: files to be copied
    #  @param [optional] remote_path: where files copied to, default value is '/usr/local/nas/'
    #  @param [optional] recursive: default is False for file copy, set to True for directory copy
    def test_scp_to_device(self):
        try:
            # self.myssh.scp_file_to_device(files='/mnt/hgfs/Trunk-Robot/AAT_framework/Log.py')
            self.myssh.scp_file_to_device(files=testfile_dir, recursive=True)
        except Exception, ex:
            logger1.info('Error scp file/folder to device %s', str(ex))


def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(SSHTest))
    return suite

if __name__ == '__main__':

    # # To run whole test suite
    # unittest.main()


    # #specify test(s) to execute individual
    suite = unittest.TestSuite()
    suite.addTest(SSHTest("test_scp_to_device"))
    suite.addTest(SSHTest("test_scp_from_device"))
    suite.addTest(SSHTest("test_open_scp_connection"))
    runner = unittest.TextTestRunner()
    # runner.run(suite)
    unittest.TextTestRunner(verbosity=2).run(suite)
