'''
Created on May 06, 2014

@author: vasquez_c

*** Variables ***
host
user
password
port

*** Test Cases ***

How to use:

1) First instantiate the class as follow:

    import SSH
    x = SSHClient.SSHClient()

2) Use the following commands:
    x.connect()
    x.execute("any command such as uptime, pwd, date, etc")
    x.disconnect()
    '''
import cmd
import os
import paramiko
import time
import pysftp
import string


from global_libraries import scp
from global_libraries import utilities
from global_libraries import CommonTestLibrary as ctl
from global_libraries.testdevice import Fields
import global_libraries.wd_exceptions as exceptions
from global_libraries.wd_networking import is_web_server_up
from global_libraries.performance import Stopwatch

paramiko.util.log_to_file('paramiko.txt')

#===================================================================================
# Class SSHClient
#===================================================================================

class SshClient(cmd.Cmd):
    '''
    Create an SSH connection to a server and execute commands.
    Usage:
        import SSHClient
        ssh = SSHClient()
        ssh.connect('host','user','password', port=22)
        if ssh.connected() is False:
            sys.exit('Connection failed')

    '''
    def __init__(self, uut):
        self.uut = uut
        self.ssh = None
        self.scp_client = None
        cmd.Cmd.__init__(self)
        self.deviceAPIdir = '/usr/local/sbin/'
        self.currentDir = os.path.dirname(os.path.realpath(__file__))

        # Setup the logger
        self.logger = ctl.getLogger('AAT.SSHClient')
        self.debug = self.logger.debug

        self.debug('SSH Client constructed:')
        self.debug('IP address: {0}, username: {1}, password: {2}, port: {3}'.
                   format(self.uut[Fields.internal_ip_address],
                          self.uut[Fields.ssh_username],
                          self.uut[Fields.ssh_password],
                          self.uut[Fields.internal_ssh_port]))

    def connect(self, username=None, password=None, port=None, server_ip=None):
        '''
        Connect to an SSH server and authenticate to it

        Returns an SSH connection
        Raises an exception on failure:
           wd_exceptions.AuthenticationError on authentication error
           wd_exceptions.SocketError on a socket error
           The actual exception in all other cases
        '''
        if username is None:
            username = self.uut[Fields.ssh_username]

        if password is None:
            password = self.uut[Fields.ssh_password]

        if port is None:
            port = self.uut[Fields.internal_ssh_port]

        if server_ip is None:
            ip_address = self.uut[Fields.internal_ip_address]
        else:
            ip_address = server_ip

        self.logger.debug('Creating SSH connection to {}:{} as {} with password {}'.format(ip_address,
                                                                                           port,
                                                                                           username,
                                                                                           password))
        if self.ssh is None:
            self.ssh = paramiko.SSHClient()
        else:
            # Already connected, so disconnect first
            if hasattr(self.ssh, 'connect'):
                self.disconnect()

        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            self.ssh.connect(hostname=ip_address,
                             username=username,
                             password=password,
                             port=port)
        except paramiko.AuthenticationException:
            raise exceptions.AuthenticationError('Authentication failed')
        else:
            self.debug('SSH connection established.')
            return self.ssh

    # noinspection PyMethodParameters
    def check_connections(func):
        """
        Decorator to check SSH connections.
        """
        def deco(self, *args, **kwargs):
            if self.ssh is None:
                self.ssh = self.connect()
                if not hasattr(self.ssh, 'connect'):
                    # Didn't work. See if the device is reachable
                    if is_web_server_up(self.uut[Fields.internal_ip_address]):
                        raise exceptions.SSHDisabledError()
                    else:
                        raise exceptions.DeviceUnreachableError()
            else:
                ret = getattr(self.ssh.get_transport(), 'is_active', None)
                if ret is None or (ret is not None and not ret()):
                    self.ssh = self.connect()
            # noinspection PyCallingNonCallable
            return func(self, *args, **kwargs)
        return deco

    # noinspection PyArgumentList
    @check_connections
    def execute(self, command, timeout=60, debug_results=True, verbose_debug=False):
        """Execute a command on the server. If the server allows it, the channel will then be directly connected
        to the stdin, stdout, and stderr of the command being executed."""
        output = ''
        self.debug('Executing command: %s on %s' % (command, self.uut[Fields.internal_ip_address]))
        timer = Stopwatch(timer=timeout)

        stdin, stdout, stderr = self.ssh.exec_command(command, timeout=timeout)
        stdin.close()
        status = 0
        erstatus = 0

        if timeout < 0:
            stdout.close()
            stderr.close()
            return 0, 'No output'
        else:
            # Start the inactivity timeout
            timer.start()
            while not stdout.channel.exit_status_ready():
                # wait for exit status
                status = stdout.channel.recv_exit_status()
                erstatus = stderr.channel.recv_exit_status()
                # Close the stdout and stderr channel
                stdout.channel.close()
                stderr.channel.close()
                for line in stdout.read().splitlines():
                    if output:
                        # Data received, so add to the output and reset the timeout timer
                        output += os.linesep
                        timer.reset()
                    output += line
                    if verbose_debug:
                        self.debug('line: %s' % line)
                for line in stderr.read().splitlines():
                    if output:
                        output += os.linesep
                        timer.reset()
                    output += line
                    self.debug('erline: %s' % line)
                if timer.is_timer_reached():
                    raise exceptions.ActionTimeout('No data received by SSH client in {} seconds'.format(timeout))
            self.debug('status: %s, erstatus: %s' % (status, erstatus))
            if debug_results:
                self.debug('output: %s' % output)
            return status, output

    def disconnect(self):
        '''Close the channel. All future read/write operations on the channel will fail.'''
        if self.ssh is not None:
            self.debug('Disconnecting from: %s' % (self.uut[Fields.internal_ip_address]))
            self.ssh.close()
            self.debug('SSH connection closed...')

    #===========================================================================================================#
    #                                         SCP                                                               #
    #===========================================================================================================#
    @utilities.decode_unicode_args
    def _scp_connection(self):
        """Open a new SCP connection to the test unit."""
        self.logger.debug('Opening SCP connection for {0}:{1}'.format(self.uut[Fields.internal_ip_address], self.uut[Fields.internal_ssh_port]))
        transport = paramiko.Transport((self.uut[Fields.internal_ip_address], self.uut[Fields.internal_ssh_port]))
        transport.connect(username=self.uut[Fields.ssh_username], password=self.uut[Fields.ssh_password])

        self.logger.debug('SSH client transport: ' + str(transport))
        self.scp_client = scp.SCPClient(transport)
        self.logger.debug('SCP client: ' + str(self.scp_client))
        return self.scp_client


    @utilities.decode_unicode_args
    def _from_device(self, remote_path, local_path='', recursive=False, preserve_times=False):
        """
        Transfer files from remote host to localhost

        @param remote_path: path to retreive from remote host. since this is
            evaluated by scp on the remote host, shell wildcards and
            environment variables may be used.
        @type remote_path: str
        @param local_path: path in which to receive files locally
        @type local_path: strk
        @param recursive: transfer files and directories recursively
        @type recursive: bool
        @param preserve_times: preserve mtime and atime of transfered files
            and directories.
        @type preserve_times: bool
        """
        self.logger.debug('SCP transfer from remote to localhost: ' + str(remote_path) + ' to ' + str(local_path))
        if not self.scp_client or not self.scp_client.transport.is_active():
            self.scp_client = self._scp_connection()
        return self.scp_client.get(remote_path, local_path, recursive, preserve_times)



    @utilities.decode_unicode_args
    def _to_device(self, files, remote_path='/usr/local/nas/', recursive=False, preserve_times=False):
        """
        Transfer files to remote host.

        @param files: A single path, or a list of paths to be transfered.
            recursive must be True to transfer directories.
        @type files: string OR list of strings
        @param remote_path: path in which to receive the files on the remote
            host. defaults to '.'
        @type remote_path: str
        @param recursive: transfer files and directories recursively
        @type recursive: bool
        @param preserve_times: preserve mtime and atime of transfered files
            and directories.
        @type preserve_times: bool
        """
        self.logger.debug('Working directory: ' + str(os.getcwd()))
        self.logger.debug('SCP transfer files to remote host: ' + str(files) + ' to ' + str(remote_path))
        if not self.scp_client or not self.scp_client.transport.is_active():
            self.scp_client = self._scp_connection()
        if not os.path.isfile(files) and not os.path.isdir(files):
            self.logger.warning('FILE NOT FOUND DURING SCP TRANSFER: ' + files)
            self.logger.debug("DIDN'T SCP TRANSFER FILE/DIR: " + files)
        else:
            self.logger.debug('SENDING FILE/DIR: ' + files)
            return self.scp_client.put(files, remote_path, recursive, preserve_times)


    @utilities.decode_unicode_args
    def scp_from_device(self, path, is_dir=False, timeout=20, verbose=False, target_path='.', retry=False):
        """
        SCP a file or directory from the device.
        """
        if not self.scp_client:
            self._scp_connection()
        return self._from_device(remote_path=path, local_path=target_path, recursive=is_dir,
                          preserve_times=True)

    def open_scp_connection(self):
        """Open a new SCP connection to the test unit."""
        self.scp_client = self._scp_connection()

    @utilities.decode_unicode_args
    def scp_file_to_device(self, files, remote_path='/usr/local/nas/', recursive=False, preserve_times=False):
        """Perform an SCP transfer from localhost to device."""

        if not self.scp_client:
            self._scp_connection()
        self._to_device(files, remote_path, recursive, preserve_times)



    #===========================================================================================================#
    #                                         SFTP                                                              #
    #===========================================================================================================#



    def open_sftp_connection(self):
        """Open a new SFTP connection to the test unit."""
        self.sftp_client = self._sftp_connection()

    def _sftp_connection(self):
        """Open a new SFTP connection to the test unit."""
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        self.sftp_client = pysftp.Connection(self.uut[Fields.internal_ip_address],username=self.uut[Fields.ssh_username], password=self.uut[Fields.ssh_password], cnopts=cnopts)

        return self.sftp_client

    def sftp_from_device(self, path, target_path='.', preserve_mtime=False):

        if not self.sftp_client:
            self._sftp_connection()

        return self.sftp_client.get_d(remotedir=path, localdir=target_path, preserve_mtime=False)


    @utilities.decode_unicode_args
    def sftp_file_to_device(self, files, remote_path, recursive=False, preserve_times=False):
        """Perform an SFTP transfer from localhost to device."""

        if not self.sftp_client:
            self._sftp_connection()


        with self.sftp_client.cd(remote_path):
            self.sftp_client.put(files)






    #===========================================================================================================#
    #                                       SSH Utilities                                                       #
    #===========================================================================================================#
    def get_ssh_username(self):
        '''
        Returns the test unit's SSH username

        '''
        return self.uut[Fields.ssh_username]

    def get_ssh_password(self):
        '''
        Returns the test unit's SSH password

        '''
        return self.uut[Fields.ssh_password]

    def get_uptime(self):
        '''
        Returns system uptime in HH:MM:SS

        '''
        return self.execute('uptime')

    def clear_alerts(self):
        '''
        Clear alerts through SSH

        '''

        myCommand = self.deviceAPIdir + 'clearAlerts.sh'
        self.execute(myCommand)

    def clean_alert(self):
        '''
        Clean alert through SSH

        '''

        myCommand = self.deviceAPIdir + 'cleanAlert.sh'
        self.execute(myCommand)

    def generate_alert(self, alertCode='0022', alertName='Power Supply Failure'):
        '''
        Generate alert through SSH

        '''
        myCommand = self.deviceAPIdir + 'sendAlert.sh ' + alertCode + " \'" + alertName + "\'"
        self.execute(myCommand)

    def set_firmware_version(self, versionNumber):
        '''
        Set the firmware version.
        Note: Although the file takes the new version number it does not update the device.
        '''

        myCommand = "echo " + versionNumber + " > /etc/version"
        self.execute(myCommand)

    def get_current_firmware_version(self):
        '''
        Returns current firmware version
        Note: /usr/sbin/read_version -n firmware
              cat /etc/version
        '''
        return self.execute("cat /etc/version")

    def set_uut_time(self, newtime='198511140600.00'):
        '''
        Recognized TIME format:
        [[[[[YY]YY]MM]DD]hh]mm[.ss]

        example: date -s 198511140600.00  '14 NOV 1985 18:00:00'

        Note: if not entered 198511140600.00 (default) would be used as default value
        '''

        myCommand = "date -s " + newtime
        self.execute(myCommand)

    def get_platform(self):
        '''
        Returns current device model Name

        '''
        return self.execute('/usr/local/sbin/getDeviceModelName.sh')


    def get_model_number(self, number_of_attempts=3):
        '''
        Returns current device model number

        '''
        ModelCommand = 'cat /usr/local/twonky/resources/devicedescription-custom-settings.txt | grep MODELNUMBER'
        for attempt in xrange(number_of_attempts, 0, -1):
            try:
                returnvalue = self.execute(ModelCommand)
                a, b, modelNumber = returnvalue[1].split("#")
                break
            except ValueError as e:
                self.logger.debug(ctl.exception('Unable to get model number. ' +
                                                'Return value is {}'.format(returnvalue[1])))

                if attempt == 1:
                    return 'Unknown'
                else:
                    time.sleep(3)

        return modelNumber


    def mount_drive(self, drive, mountpoint):
        '''
        mounts the specified drive and returns a mount point.
        usage:
            mount_drive('/dev/sde1','/mnt/USB/USB2_e1')
        '''

        myCommand = "mount " + drive + " " + mountpoint
        grepCommand = "mount | grep " + "\'" + drive + "\'" + ' | awk ' + "\'{print $3}\'"
        self.execute(myCommand)
        self.execute(grepCommand)

    def unmount_drive(self, drive):
        '''
        unmounts the specified drive.
        usage:
            unmount_drive('/dev/sde1')
        '''

        myCommand = "umount " + drive
        self.execute(myCommand)

    def find_usb_drive(self):
        '''
        finds the available usb drive.

        '''

        myCommand = 'df -h | grep \'/mnt/USB/USB\' | awk \'{print $1}\''
        return self.execute(myCommand)

    def find_sata_drive(self):
        '''
        finds available sata drives.

        '''
        myCommand = 'df -h | grep \'/mnt/HD_\' | awk \'{print $1}\''
        return self.execute(myCommand)

    def stop_services(self):
        '''
        Stop services runing in the unit.

        '''
        self.execute('/usr/local/modules/script/kill_running_process')

    def create_zero_file(self, destination, filename, blocksize, count):
        '''
        destination --> file location eg: mountpoint (required)
        blocksize --> bs (default:1024)
        count --> count (default:10240)

        '''

        myCommand = 'dd if=/dev/zero of={0}/{1} bs={2} count={3}'.format(destination, filename, blocksize, count)

        result = self.execute(myCommand)
        return result

    def create_random_file(self, destination, filename, blocksize, count):
        '''
        destination --> file location eg: mountpoint (required)
        blocksize --> bs (default:1024)
        count --> count (default:10240)

        '''
        myCommand = 'dd if=/dev/urandom of={0}/{1} bs={2} count={3}'.format(destination, filename, blocksize, count)
        result = self.execute(myCommand)
        return result

    def md5_checksum(self, path, filename):
        '''
        path -> location on the UUT. eg: /shares/Public/
        filename -> file name of file needed to hash. eg: file0.jpg
        '''
        checksum = self.execute('busybox md5sum {0}{1}'.format(path, filename))
        result = checksum[1]
        hash = result[:32]
        return hash

    def format_usb_drive(self, drive):
        '''
        formats the specified drive.
        usage:
            format_usb_drive('/dev/sde1')
        '''
        unmountCommand = 'umount ' + drive
        myCommand = 'mke2fs -t ext4 ' + drive

        # make sure that the drive is unmount
        self.execute(unmountCommand)
        time.sleep(3)
        result = self.execute(myCommand)
        return result

    def add_user_account(self, user='user1', passwd='11111', groupmember='#g1#,#g2#', firstname='User_One', passwdhint='My hint', lastname='User_lastname', email='someone@somesite.com'):
        '''
        -a --> add command
        -u user --> user name
        -g groupname --> group name
        -p : passwd --> passwd
        -l : list --> group member or user join groups
        -L : list --> group member or user join groups(Import file)
        -f : first name
        -h : password hint
        -t : last name
        -e : email
        ex: add user
            --> account -a -u 'a1' -p '11111' -l '#g1#,#g2#' [-f first name] [-h passwd_hint] [-t last name]  [-e email]

        '''

        myCommand = 'account -a -u \'{0}\' -p \'{1}\' -l \'{2}\' -f \'{3}\' -h \'{4}\' -t \'{5}\' -e \'{6}\''.format(self.uut[Fields.ssh_username], passwd, groupmember, firstname, passwdhint, lastname, email)

        self.execute(myCommand)

    def delete_user_account(self, user='user1'):
        '''
        -d --> delete command
        -u user --> user

        ex: delete user
            --> account -d -u 'a1'

            myCommand = "account -d -u '+'\''+ username + '\'"
        '''

        myCommand = 'account -d -u {0}'.format(user)

        self.execute(myCommand)
    
    def delete_all_users(self,exclude='admin'):
        '''
        -d --> del command
        -u user --> user name
        -i : list --> list of available users
        ex: del user
            --> account -d -u 'user1'
        '''
        # Convert to a list if necessary
        if isinstance(exclude, basestring):
            exclude = [exclude]
        if 'admin' not in exclude:
            exclude.append('admin')
            
        users = self.execute('account -i user')[1].split()
        if not users:
            raise exceptions.InvalidDeviceState('User list is empty')

        for user in users:
            if user not in exclude:
                self.logger.debug('deleting user:{0}'.format(user))
                self.execute('account -d -u {0}'.format(user))

    def modify_user_account(self, user, passwd=None, fullname=None):
        '''
        -m --> modify command
        -u : user --> user name
        -g : groupname --> group name
        -p : passwd --> password
        -l : list --> group member or user join groups
        -L : list --> group member or user join groups(Import file)
        -f : full name (first name + last name)

        ex: modify user
            --> account -m -u 'a1' -p '11111' [-f full name]
        '''
        
        myCommand = 'account -m -u \'{0}\''.format(user)

        # myCommand = 'account -m -u \'{0}\' -p \'{1}\' -l \'{2}\' -f \'{3}\' -h \'{4}\' -t \'{5}\' -e \'{6}\''.format(user,passwd,groupmember,firstname,passwdhint,lastname,email)
        if fullname is not None:
            myCommand += ' -f \'{0}\''.format(fullname)
                             
        if passwd is not None:
            myCommand += ' -p \'{0}\''.format(passwd)
           
        self.logger.debug('myCommand:{0}'.format(myCommand))

        self.execute(myCommand)

    def create_groups(self, group, memberusers=None):
        '''
        -a --> add command
        -g groupname --> group name
        -l : list --> group member or user join groups
        ex: add group
            --> account -a -g 'group1' -l '#user1#,#user2#'
        '''

        myCommand = 'account -a -g \'{0}\' -l \'{1}\''.format(group, memberusers)

        self.execute(myCommand)

    def delete_group(self, group):
        '''
        -d --> del command
        -g groupname --> group name
        -l : list --> group member or user join groups
        ex: del group
            --> account -d -g 'group1'
        '''

        myCommand = 'account -d -g \'{0}\''.format(group)

        self.execute(myCommand)

    def delete_all_groups(self):
        '''
        -d --> del command
        -g groupname --> group name
        -i : list --> list of available groups
        ex: del group
            --> account -d -g group
        '''
        groups = self.execute('account -i group')[1].split()
        for group in groups:
            self.logger.debug('deleting group:{0}'.format(group))
            self.execute('account -d -g {0}'.format(group))

    def update_group(self, group, memberusers=None):
        '''
        -m --> modify command
        -g groupname --> group name
        -l : list --> group member or user join groups
        ex: modify group member
            --> account -m -g 'g1' -l '#a1#,#a2#'
        '''

        myCommand = 'account -m -g \'{0}\' -l \'{1}\''.format(group, memberusers)

        self.execute(myCommand)

    def start_afp_service(self):
        '''
        Starts Apple File Protocol
        '''

        myCommand = '/usr/sbin/afp start'
        self.execute(myCommand)

    def stop_afp_service(self):
        '''
        Stops Apple File Protocol
        '''

        myCommand = '/usr/sbin/afp stop'
        self.execute(myCommand)

    def restart_afp_service(self):
        '''
        Restarts Apple File Protocol
        '''

        self.logger.debug(self.execute('/usr/sbin/afp restart'))

    def remove_drives(self, drive_list):
        '''
        Removes drives or remove specific location drive
        '''
        if not drive_list:
            least_condition = self.uut[Fields.number_of_drives] - 1
            if least_condition != 0:
                for counter in range(0, least_condition, 1):
                    self.logger.debug(self.execute('sata_pwr_ctl -n {0} -p0'.format(counter+1)))
            if least_condition == 0:
                raise exceptions.InvalidDefaultValue('Requires at least one hard drive.')
        if drive_list:
            if str(drive_list).isdigit():
                least_condition = self.uut[Fields.number_of_drives] - 1
                if least_condition == 0:
                    raise exceptions.InvalidDefaultValue('Requires at least one hard drive.')
                else:
                    self.logger.debug(self.execute('sata_pwr_ctl -n {0} -p0'.format(drive_list)))
            if not str(drive_list).isdigit():
                least_condition = self.uut[Fields.number_of_drives] - len(drive_list)
                if least_condition > 0:
                    for counter in drive_list:
                        self.logger.debug(self.execute('sata_pwr_ctl -n {0} -p0'.format(counter)))
                if least_condition <= 0:
                    raise exceptions.InvalidDefaultValue('Requires at least one hard drive.')

    def insert_drives(self, drive_list):
        '''
        Inserts drives or insert specific location drive
        '''
        if not drive_list:
            numberofdrives = self.uut[Fields.number_of_drives]
            for counter in range(0, numberofdrives, 1):
                self.logger.debug(self.execute('sata_pwr_ctl -n {0} -p1'.format(counter+1)))
        if drive_list:
            if str(drive_list).isdigit():
                self.logger.debug(self.execute('sata_pwr_ctl -n {0} -p1'.format(drive_list)))
            if not str(drive_list).isdigit():
                for counter in drive_list:
                    self.logger.debug(self.execute('sata_pwr_ctl -n {0} -p1'.format(counter)))

    def set_quota_all_volumes(self, name, group_or_user, amount):
        '''
        Sets group or user quota on all volumes in a given group.

        '''
        if group_or_user == 'group':
            myCommand = '/usr/sbin/quota_set -g {0} -s {1} -a'.format(name, amount)
        elif group_or_user == 'user':
            myCommand = '/usr/sbin/quota_set -u {0} -s {1} -a'.format(name, amount)
        else:
            raise exceptions.InvalidParameter('group_or_user value of {} is invalid.'.format(group_or_user))

        return self.execute(myCommand)

    def delete_share(self,share_name):
        '''
        Deletes a single given share with the exception of Public, SmartWare and TimeMachineBackup
        '''
        self.execute('/usr/sbin/smbif -b {0};smbcom'.format(share_name))

    def delete_all_shares(self):
        '''
        Deletes all shares with the exception of Public, SmartWare and TimeMachineBackup
        '''
        self.execute('/usr/sbin/smbcv -x;smbcv;smbwddb;smbcom')

    def configure_raid(self,
                       raidtype,
                       drive_bays=None,
                       volume_size=None,
                       spanning=False,
                       spare=False,
                       raid1_span_vol=None,
                       debug_mode=False):
        """ Create RAID with different RAID type

        :param raidtype: The RAID mode to switch to. Can be a number or a name
        :param drive_bays: Either the number of bays to assign to the RAID or a list of specific bays
        :param volume_size: If present, the size to limit the volume to. Valid for all RAID sizes by JBOD or Spanning.
        :param spanning: If True, use the remaining space as a spanning volume. If True and volume_size is None,
                         it will default to a 250 MB volume. Valid for all RAID sizes by JBOD or Spanning.
        :param spare: If True, use one drive as a spare. Only valid for RAID 5
        :param raid1_span_vol: A list of volumes to use for RAID 1 with spanning
        :param debug_mode: If True, do not execute the commands. Instead, just output to the console


        TODO: Implement the following RAID features: expand capacity, downgrade to JBOD, migrate to RAID 1

        From Vance:
        just remind that 'downgraded to JBOD' and 'migrate to RAID1' execute command is like as below:(command will be dynamic for different situation)

        If RAID1 with sdasdb need to downgrade to JBOD with Drive 1(sda), the command should be:
        diskmgr -v"1" -m1 -ksdasdb --kill_running_process --load_module --raid1_to_std
        PS: Only can choose sda or sdb to downgrade. Cannot choose both sda & sdb, don't know the reason. VolumeID should be RAID1's volumeID

        If JBOD with sda need to migrate to RAID1 with sdasdb,
        the command should be:
        diskmgr -v"1" -m4 -f3 -k"sda" -c"sdb" --kill_running_process --load_module -t
        PS: Migration will need a long time to process raid sync. VolumeID should be RAID's volumeID.

        Cannot used Destroy command in downgraded and migration function.
        """
        diskmgr_command = 'diskmgr'
        number_of_volumes = 1

        if volume_size is None and spanning is True:
            volume_size = 250

        # Convert the raidtype into a raid value
        if not ctl.is_number(raidtype):
            raid_level = ctl.raid_mode_to_level(raidtype)
        else:
            raid_level = raidtype

        if drive_bays is None:
            # Get all drive bays
            drive_bays = len(self.get_drive_mappings())
            if raid_level == 1 and drive_bays % 2 > 0:
                if drive_bays > 1:
                    # More than 1 drive, but it's odd, so subtract 1 to make it even
                    drive_bays -= 1

            if raid_level == 10 and drive_bays % 4 > 0:
                if drive_bays > 4:
                    # More than 4 drives, but not divisible by 4, so subtract to make it even
                    drive_bays -= drive_bays % 4

        # Do not convert to a list because get_drive_letters won't work properly
        if ctl.is_number(drive_bays):
            drives_requested = drive_bays
        else:
            drives_requested = len(drive_bays)

        # Verify that the RAID upgrade path is valid
        ctl.validate_raid_path(current_raid=self.uut[Fields.configured_raid_level],
                               target_raid=raid_level,
                               number_of_drives=drives_requested,
                               downgrade_to_jbod=False,
                               expand_capacity=False,
                               limit_capacity=True if volume_size is not None else False,
                               remainder_as_spanning=spanning,
                               raid5_spare=spare,
                               migrate_to_raid_1=False,
                               raid_1_spanning_volumes=raid1_span_vol)
        option_list = []
        # option_list contains a list of tuples that consist of:
        # (Volume Number, RAID Mode, Drive Letter(s))

        if raid_level == 11:
            # JBOD. Mode 1 (normal)
            drive_mappings = self.get_drive_mappings()
            if ctl.is_number(drive_bays):
                # Get the first drive_bays number of ready drives
                volume = 0
                for bay in drive_mappings:
                    drive_name = drive_mappings[bay]
                    volume += 1
                    option_list.append((volume, 1, drive_name))

                    if volume == drive_bays:
                        break

                if volume != drive_bays:
                    raise exceptions.InvalidDeviceState('Not all requested drives are available.')
            else:
                volume = 0
                for bay in drive_bays:
                    volume += 1
                    if bay in drive_mappings:
                        drive_name = drive_mappings[bay]
                        option_list.append((volume, 1, drive_name))
                    else:
                        raise exceptions.InvalidDeviceState('Not all requested drives are available.')
        elif raid_level == 12:
            # Span, mode 2 (incorrectly called JBOD by diskmgr help text)
            option_list.append((1, 2, self._get_drive_names(drive_bays)))
        elif raid_level == 1:
            # RAID 1, mode 4
            number_of_volumes = drives_requested / 2
            for drive_set in range(0, number_of_volumes):
                if ctl.is_number(drive_bays):
                    drive_letters = self._get_drive_names(2, (drive_set * 2) + 1)
                else:
                    drive_letters = self._get_drive_names(drive_bays[drive_set * 2:2 + (drive_set * 2)])

                option_list.append((drive_set + 1, 4, drive_letters))
        elif raid_level == 0:
            # RAID 0, mode 3
            option_list.append((1, 3, self._get_drive_names(drive_bays)))
        elif raid_level == 5:
            # RAID 5, but handled differently if there is a spare
            if spare:
                option_list.append((1, 7, self._get_drive_names(drive_bays - 1)))
            else:
                option_list.append((1, 5, self._get_drive_names(drive_bays)))
        elif raid_level == 10:
            # RAID 10, mode 6
            number_of_volumes = drives_requested / 4
            for drive_set in range(0, number_of_volumes):
                if ctl.is_number(drive_bays):
                    drive_letters = self._get_drive_names(4, (drive_set * 4) + 1)
                else:
                    drive_letters = self._get_drive_names(drive_bays[drive_set * 4:4 + (drive_set * 4)])

                option_list.append((drive_set + 1, 6, drive_letters))
        else:
            raise exceptions.InvalidParameter('raid_level of {} is not supported'.format(raid_level))

        command = diskmgr_command

        drive_list = ''
        for options in option_list:
            parameters = ''
            volume_number, raid_mode, drive_letters = options

            parameters += '-v{} -m{}'.format(volume_number, raid_mode, drive_letters)
            drive_list += drive_letters

            if volume_size is not None:
                # Set the volume size
                parameters += ' -s {}'.format(volume_size)
                volume_size += 100

            parameters += ' -f3 -k{}'.format(drive_letters)

            command += ' ' + parameters

        if raid_level == 5:
            if spare:
                spare_drive = self._get_drive_names(1, drive_bays)
                command += ' -j{}'.format(spare_drive)
                drive_list += spare_drive

            command += ' --assume-clean 0'

        if spanning:
            if raid1_span_vol:
                raid1_span_vol = ctl.to_list(raid1_span_vol)
            elif raid_level == 1:
                raid1_span_vol = range(1, number_of_volumes + 1)

            for options in option_list:
                # Append the spanned volumes
                skip_volume = False
                volume_number, raid_mode, drive_letters = options

                if raid1_span_vol:
                    if volume_number not in raid1_span_vol:
                        # Not listed, so do not create a span for this volume
                        skip_volume = True

                if not skip_volume:
                    number_of_volumes += 1
                    parameters = '-v{} -m2 -f3 -k{} -3'.format(number_of_volumes,
                                                               drive_letters)

                    command += ' ' + parameters

        destroy_raid = 'diskmgr -D{} --kill_running_process'.format(self._get_drive_names(0))
        command += ' --kill_running_process --load_module -n'

        self.logger.info('Destroying RAID with command {}'.format(destroy_raid))
        if not debug_mode:
            self.execute(destroy_raid)
        self.logger.info('Creating RAID set with command {}'.format(command))
        if not debug_mode:
            self.execute(command)
            if raid_level == 5:
                # RAID 5 takes a very long time.
                self.wait_for_raid_to_sync(number_of_volumes, timeout=3600)
            elif raid_level == 11:
                # JBOD no need to wait for raid sync (not mdraid)
                pass
            else:
                self.wait_for_raid_to_sync(number_of_volumes)
        self.logger.info('RAID configuration complete')

    def wait_for_raid_to_sync(self, number_of_volumes=1, show_progress=True, timeout=300):
        """ Waits until the RAID sync is complete for all volumes

        :param number_of_volumes: The number of volumes to check
        :param show_progress: If True, the % complete goes to the console. If False, it goes to the debug log
        :param timeout: The amount of time to wait with no increase in the progress percentage before timing out
        """
        # Start with md1 since md0 is the OS logical drive
        last_percent = []
        volumes_syncing = []

        for volume in range(1, number_of_volumes + 1):
            last_percent.append('')
            volumes_syncing.append(True)

        timer = Stopwatch(timer=timeout)
        timer.start()

        self.logger.info('Waiting for RAID Sync to start')

        while True in volumes_syncing:
            for volume in range(0, number_of_volumes):
                if volumes_syncing[volume]:
                    message = None
                    status = self.execute('mdadm --detail /dev/md{}'.format(volume + 1), debug_results=False)[1]
                    rebuild_status_text = 'Rebuild Status : '
                    status_location = status.find(rebuild_status_text)
                    if status_location > 0:
                        percent = status[status_location:]
                        percent = percent.split(':')[1].split('%')[0]
                        if last_percent[volume] != percent:
                            message = 'Volume md{} - Syncing RAID: {}%'.format(volume + 1, percent)
                            last_percent[volume] = percent
                            timer.reset(keep_running=True)
                    else:
                        if 'State : active' in status or 'State : clean' in status:
                            # Completed
                            volumes_syncing[volume] = False
                            message = 'Volume md{} - RAID Sync Complete'.format(volume + 1)

                    if message:
                        if show_progress:
                            self.logger.info(message)
                        else:
                            self.logger.debug(message)

            if timer.is_timer_reached():
                raise exceptions.ActionTimeout('Timeout waiting for RAID sync to finish')
            time.sleep(1)

        self.logger.info('RAID Sync Complete')

        # Dump the mdadm results of each volume to the debug log
        for volume in range(0, number_of_volumes):
            status = self.execute('mdadm --detail /dev/md{}'.format(volume + 1))[1]
            self.logger.debug(status)

    def get_drive_mappings(self, start_drive=1):
        # Build up a list of drive bay to drive name mappings
        drive_mappings = {}
        xml_contents = self.execute('cat /var/www/xml/sysinfo.xml')[1]
        disks = ctl.get_xml_tags(xml_contents, 'disk')

        for disk in disks:
            bay_number = int(filter(str.isdigit, disk.split('\n')[0]))
            if bay_number >= start_drive:
                if ctl.is_disk_ready(disk):
                    drive_name = ctl.get_xml_tags(disk, 'name')[0]
                    drive_mappings[bay_number] = drive_name

        return drive_mappings

    def _get_drive_names(self, drive_bays=0, start_bay=1):
        return ctl.get_drive_names(self.get_drive_mappings(start_drive=start_bay),
                                   drive_bays=drive_bays,
                                   start_bay=start_bay)
                                   
    def reboot(self):
        rebootCommand = '/usr/sbin/do_reboot'
        status = self.execute(rebootCommand)[1]
        self.logger.info('Rebooting: {0}'.format(rebootCommand))
        self.logger.debug(status)

    # # @brief usb backup usage
    # @param:
    #        source_path: path of source
    #        dest_path:   path of destination
    #        job_name:    name of backup job
    #        mode:        {1:copy, 2:sync, 3:Incremental}
    #        device_type: {1:storage, 2:mtp}
    #        direction:   {1:USB -> NAS, 2: NAS -> USB}
    #        usage:       [jobadd/jobedit/jobdel/jobrun/jobstop/jobrs/jobrs_list]

    def usb_backup_usage(self, source_path='', dest_path='', job_name='job1',
                         mode='1', device_type=1, direction=1, usage='jobadd'):

        command = 'usb_backup -a "%s" ' %job_name

        if usage in ['jobdel', 'jobrun', 'jobstop']:
            command = command + '-c ' + usage
            if usage == 'jobrun':
                command =command + ' &'
        else:
            command = command + '-m ' + mode + ' -t {0} -s "{1}" -d "{2}" -c {3}'.format(direction, source_path, dest_path, usage)

        self.execute(command)
