'''
#
# Created on Septempter 11, 2014
# @author: Jason Tran
# 
# This module contains the class and methods to validate SerialAPI methods.
#
'''

import unittest
import logging
import time

from UnitTestFramework import mylogger
from serialAPI.src.serialclient import SerialClient
#from global_libraries.CommonTestLibrary import CommonTestLibrary

l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('SerialAPIUnitTest')

# Variables to use for unit testing
serial_server_ip = '192.168.1.117'
serial_port_mapping = '7000'
command = 'free | grep Mem | awk \'{print $3/$2 * 100.0}\'' 
buff = 'ls'
mystring = '***'
msg = 'hello'

#instantiate NetworkSharesAPI class
try:
    mySerial = SerialClient(serial_server_ip, serial_port_mapping)
except Exception,ex:
    logger1.info('Failed to instantiate SerialAPI ' + str(ex))

class SerialUnitTest(unittest.TestCase):
    
    def setUp(self):
        mySerial.initialize_serial_port()
        time.sleep(3)                
        logger1.info('Start executing %s', self._testMethodName)
         
    def tearDown(self):
        mySerial.close_serial_connection()
        logger1.info('Finished executing %s', self._testMethodName)
       
    ## @brief Verify if serial connection is established
    def test_serial_is_connected(self):
        try:
            mySerial.serial_is_connected()
            logger1.info('Serial is connected')
        except Exception,ex:
            logger1.info('Serial is not connected %s', str(ex))
    
    ## @brief Execute command via serial connection
    #  @param [in] command: command to be executed on test unit via serial connection
    #  @param [optional] timeout: default value is 20
    def test_serial_write(self):
        try:
            status = mySerial.serial_write(command)
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to execute command %s, error: %s', str(command), str(ex))

    ## @brief Send command without 'return key' --- method prob won't be used
    #  @param [in] buff: buff to be executed on test unit via serial connection
    #  @param [optional] timeout: default value is 20
    def test_serial_write_bare(self):
        try:
            status = mySerial.serial_write_bare(buff)
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to execute command %s, error: %s', str(buff), str(ex))
            
    ## @brief Return a chunk of data from the serial port
    def test_serial_read(self):
        try:
            status = mySerial.serial_read()
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to read data from serial connection, %s', str(ex))

    ## @brief Reads everything from the read queue
    def test_serial_read_all(self):
        try:
            status = mySerial.serial_read_all()
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to read data from serial connection, %s', str(ex))
            
    ## @brief Return a line from the serial port
    def test_serial_readline(self):
        try:
            status = mySerial.serial_readline()
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to read a line of data from serial connection, %s', str(ex))    

    ## @brief Wait for the given string or for the timeout (in seconds, default 5)
    #  @param [in] string: value to look for
    #  @param [optional] timeout: default value is 5
    def test_serial_wait_for_string(self):
        try:
            status = mySerial.serial_wait_for_string(mystring)
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to wait for %s, %s', str(mystring), str(ex))  

    ## @brief Send a message to serial connection (in seconds, default 5)
    #  @param [in] msg: message to be sent
    #  @param [optional] timeout: default value is 5
    def test_serial_debug(self):
        try:
            status = mySerial.serial_debug(msg)
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to wait for %s, %s', str(mystring), str(ex)) 
            
    ## @brief Starts the serial port, does nothing if it's already started
    def test_start_serial_port(self):
        try:
            status = mySerial.start_serial_port()
            time.sleep(3)
            logger1.info('status: %s', status)
        except Exception,ex:
            logger1.info('Failed to start serial port connection %s', str(ex)) 
                                                                       
if __name__ == '__main__':
    ## To run whole test suite
    unittest.main()
    

    '''
    ##specify test(s) to execute individual
    suite = unittest.TestSuite()
    #suite.addTest(SerialUnitTest("test_serial_is_connected"))
    suite.addTest(SerialUnitTest("test_serial_debug"))

    
    runner = unittest.TextTestRunner()
    unittest.TextTestRunner(verbosity=2).run(suite)

    '''