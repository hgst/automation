import Queue
import decorator
import socket
import sys
import telnetlib
import thread
import threading
import time
from global_libraries import utilities
from global_libraries.testdevice import Fields
from global_libraries.Inherit import inherit
from global_libraries import CommonTestLibrary as ctl


serial_lock = threading.RLock()


@decorator.decorator
def _synchronize(func, *args, **kwargs):
    serial_lock.acquire()
    result = func(*args, **kwargs)
    serial_lock.release()
    return result


class SerialClient(object):
    """
    This library wraps Python's telnetlib to provide access to a test unit's
    serial port via a serial server that forwards serial ports to telnet ports.
    """

    def __init__(self, uut, debug=False):
        self.uut = uut
        self.common = None
        self.serial = None
        self.read_queue = None
        self.logging_thread = None
        self.logger = ctl.getLogger('AAT.serialclient')
        self.debug = debug
        self.input_queue = Queue.Queue()

        # thread lock used for telling serial reader thread to stop
        self.serial_connected = thread.allocate_lock()

    def initialize_serial_port(self):
        self.common = inherit('CommonTestLibrary')
        try:
            if self.common.has_serial_mapping:
                self.uut[Fields.serial_server_port] = self.common.serial_port_mapping
                self.uut[Fields.serial_server] = self.common.serial_server_ip
        except (Exception, AttributeError):
            self.logger.warning(ctl.exception('Failed to initialize SerialPortLibrary'))

        if self.uut[Fields.serial_server_port] and self.uut[Fields.serial_server]:
            self.logger.debug('Initializing SerialPortLibrary')
            try:
                self._start_logging()
            except Exception as e:
                if 'Connection refused' in str(e):
                    self.logger.warning(
                        'Connection refused to serial port: {0}:{1}'.format(self.uut[Fields.serial_server],
                                                                            self.uut[
                                                                                Fields.serial_server_port]))
                else:
                    raise

    @_synchronize
    def _connect(self):
        """Connect to the serial port (if it exists)."""
        # don't open up a new connection if we're not supposed to be connected
        if not self.serial_connected.locked():
            self.logger.warning('Tried to open serial connection but serial lock not acquired')
            return
        if self.serial:
            try:
                self.serial.close()
            except Exception as e:
                self.logger.warning('Error closing serial port: {0}'.format(e))
        self.logger.debug('Connecting to serial server {0} port: {1}'.format(self.uut[Fields.serial_server],
                                                                             self.uut[Fields.serial_server_port]))
        self.serial = telnetlib.Telnet(self.uut[Fields.serial_server], self.uut[Fields.serial_server_port], timeout=10)
        if not self.read_queue:
            self.read_queue = Queue.Queue()

    @_synchronize
    def close_serial_connection(self):
        """
        Closes the serial connection
        """
        if self.serial_connected.locked():
            # release the lock so that the reader thread can stop
            self.serial_connected.release()
        if self.serial is not None:
            self.serial.close()
        if self.logging_thread:
            self.logging_thread.join(timeout=60)

    def serial_is_connected(self):
        """Returns True if the serial port is connected."""
        if self.serial is not None:
            self.logger.debug('Connected to serial port: {0}'.format(self.uut[Fields.serial_server],
                                                                     self.uut[Fields.serial_server_port]))
        else:
            self.logger.debug(
                'serial_server_ip={0}'.format(self.uut[Fields.serial_server], self.uut[Fields.serial_server_port]))
            self.logger.warning('Serial port not connected.')
            import gc

            for obj in gc.get_objects():
                if isinstance(obj, SerialClient):
                    self.logger.debug('Instance of SerialClient: {0}'.format(obj))
        return self.serial is not None

    @_synchronize
    def start_serial_port(self):
        """
        Starts the serial port, does nothing if it's already started
        """
        if self.serial is not None:
            self.logger.debug('Serial port is already started')
        elif not self.serial_connected.locked():
            self._start_logging()
        else:
            self.serial_connected.release()
            time.sleep(30)
            self._start_logging()

    @_synchronize
    def _start_logging(self):
        """Set up a logger."""
        if not self.serial_connected.locked():
            self.logger.debug('Starting serial port logger.')

            # acquire the serial_connected lock so that the thread won't stop
            self.serial_connected.acquire()
            self.logging_thread = threading.Thread(target=self._log_serial_messages)
            self.logging_thread.daemon = True
            self.logging_thread.start()


    def _log_serial_messages(self):
        """Loop that runs inside self.logging_thread."""
        data = ''
        while self.serial_connected.locked():
            while not self.input_queue.empty():
                self.logger.debug(str(self.input_queue.get()))
            sys.stdout.flush()
            # if the lock is no longer locked, stop looping
            if not self.serial_connected.locked():
                self.logger.debug('Serial port logging thread exiting')
                return
            if not self.serial:
                self._connect()
            previous_data = data
            try:
                data = utilities.replace_escape_sequence(self.serial.read_until('\n', timeout=5))
                data = data.replace('\n', '\r\n')
                if data and data != previous_data:
                    self.logger.debug('SERIAL DATA: ' + str(data))
                    self.read_queue.put(data, timeout=10)
                    self.logger.debug(data)
            except (EOFError, AttributeError) as e:
                if self.serial_connected.locked():
                    self.logger.warning(
                        ctl.exception('Error reading from serial port: {0} at {1}'.format(str(e), time.asctime())))
                    self.logger.debug('Reopening serial connection')
                    self._connect()
                else:
                    self.logger.debug('serial port logging thread exiting')
                    return

    def serial_debug(self, msg):
        """
        Places a given message into the input queue
        """
        self.input_queue.put(msg)

    @_synchronize
    def serial_read_all(self):
        """
        Reads everything from the read queue
        """
        if self.read_queue:
            output = []
            while not self.read_queue.empty():
                out = self.read_queue.get()
                self.logger.debug('SERIAL: {0}'.format(out))
                output.append(out)
            return output
        else:
            self.logger.warning('Cannot read all from serial port, serial port not connected')

    @_synchronize
    def serial_read(self):
        """Return a chunk of data from the serial port."""
        if self.read_queue:
            output = self.read_queue.get()
            self.logger.debug('SERIAL: {0}'.format(output))
            return output
        else:
            self.logger.warning('Cannot read from serial port, serial port not connected')

    @_synchronize
    def serial_readline(self):
        """Return a line from the serial port."""
        output = self.serial_wait_for_string('\n')
        self.logger.debug('SERIAL: {0}'.format(output))
        return output

    @_synchronize
    @utilities.decode_unicode_args
    def serial_write(self, buff, timeout=20):
        """Write to the serial port and append a newline."""
        start = time.time()
        while self.serial and (time.time() - start < timeout):
            try:
                result = self.serial.write('{0}\n'.format(buff))
                return result
            except socket.error:
                self._connect()
            time.sleep(1)
        self.logger.warning('Cannot write "{0}" to serial port, serial port not connected'.format(buff))

    @_synchronize
    @utilities.decode_unicode_args
    def serial_write_bare(self, buff, timeout=20):
        """Write to the serial port."""
        start = time.time()
        while self.serial and (time.time() - start < timeout):
            try:
                self.serial.write(buff)
                return
            except socket.error:
                self._connect()
            time.sleep(1)
        self.logger.warning('Cannot write "{0}" to serial port, serial port not connected'.format(buff))

    @_synchronize
    @utilities.decode_unicode_args
    def serial_wait_for_string(self, string, timeout=5):
        """Wait for the given string or for the timeout (in seconds, default 5)."""
        if self.read_queue:
            self.logger.debug('Waiting for string: {0}'.format(string))
            if not string:
                return self.serial_read()
            buff = ''
            start = time.time()
            while True:
                try:
                    buff += self.read_queue.get(timeout=timeout)
                except Queue.Empty:
                    time.sleep(1)
                if string in buff or time.time() - start > timeout:
                    for line in buff.splitlines():
                        self.logger.debug('Returning: {0}'.format(line))
                    return buff
                time.sleep(1)
        else:
            self.logger.warning('Cannot wait for "{0}" from serial port, serial port not connected'.format(string))

    # Sequoia (04.04) requires root default password to be changed
    # This method will replaced default password with fituser99 then change back to default to satisfy requirement
    def update_root_password_seq(self):
        default_password = 'welc0me'
        temp_password = 'fituser99'
        login_prompt = 'WDMyCloud login:'
        prompt = 'WDMyCloud:~#'
        self.start_serial_port()
        time.sleep(15)
        self.serial_write('\n')
        time.sleep(2)
        read_result = self.serial_read()
        self.logger.debug('read result: {0}'.format(read_result))
        loop_counter = 0

        # waiting for login prompt or prompt
        while read_result != login_prompt and read_result != prompt:
            self.serial_write('\n')
            time.sleep(5)
            read_result = self.serial_read()
            self.logger.debug('read result: {0}'.format(read_result))
            loop_counter += 1
            if loop_counter == 12:
                break

        if loop_counter < 12:
            self.logger.debug('Found a prompt')
        else:
            self.logger.warning('No prompt is displayed')

        # if prompt is displayed, nothing to do
        # if login prompt is displayed, continue
        if read_result == login_prompt:
            self.logger.debug('Found login prompt')
            self.serial_wait_for_string('WDMyCloud login:', 30)
            self.serial_write('root')
            self.serial_wait_for_string('Password:', 30)
            self.serial_write(default_password)
            read_result = self.serial_read()
            self.logger.debug('read result: {0}'.format(read_result))

            # change password if found 'You are required to change your password immediately (root enforced)'
            if read_result == 'You are required to change your password immediately (root enforced)':
                self.logger.debug('Need to update password')
                self.serial_wait_for_string('(current) UNIX password: ', 30)
                self.serial_write(default_password)
                self.serial_wait_for_string('Enter new UNIX password: ', 30)
                self.serial_write(temp_password)
                self.serial_wait_for_string('Retype new UNIX password: ', 30)
                self.serial_write(temp_password)
                self.serial_wait_for_string('WDMyCloud:~# ', 30)
                self.serial_write('passwd')
                self.serial_wait_for_string('Enter new UNIX password: ', 30)
                self.serial_write(default_password)
                self.serial_wait_for_string('Retype new UNIX password: ', 30)
                self.serial_write(default_password)
                read_result = self.serial_read()
                self.logger.debug('read result: {0}'.format(read_result))
        else:
            self.logger.debug('ssh password requirement already met')
