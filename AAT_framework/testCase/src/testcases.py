'''
Created on July 14, 2014

@author: vasquez_c, tran_jas
'''

import time
import os
import sys
import logging
from global_libraries.CommonTestLibrary import CommonTestLibrary
from testCaseAPI.src.testclient import TestClient

# set working directory
os.chdir('../test')

ctl = CommonTestLibrary()
ctl.initialize_test_unit()

# instantiate TESTClient class
test = TestClient(ctl.ip_address, ctl.power_switch_ip, ctl.serial_server_ip, ctl.serial_port_mapping, ctl.username, 
                  ctl.password, ctl.ssh_username, ctl.ssh_password, ctl.ssh_port, ctl.serial_user, ctl.serial_password)

######################### ######################### ######################### ######################### ######################### 
########################################  Please do not delete anything above this line #########################################
######################### ######################### ######################### ######################### ######################### 

from UnitTestFramework import mylogger
l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('testCases')

success_user_create = '<Response [201]>'

class TESTCases(object):
        
       
    # Create user
    def create_user(self, testuser):
        try:
            returnvalue = test.create_user(testuser)
        except Exception,ex:
            logger1.info('Failed to create user %s, %s', str(testuser), str(ex)) 
            
    def delete_user(self, testuser):
        try:
            test.delete_user(testuser)
        except Exception,ex:
            logger1.info('Failed to delete user %s, %s', str(testuser), str(ex)) 
            
    def get_user(self, testuser):
        try:
            test.get_user(testuser)
        except Exception,ex:
            logger1.info('Failed to get user %s info, %s', str(testuser), str(ex)) 

    def get_all_users(self):
        try:
            test.get_all_users()
        except Exception,ex:
            logger1.info('Failed to get all user info, %s', str(ex))            
                                         
if __name__ == '__main__':
    0
    tc = TESTCases()

    '''
    tc.create_user("testuser")
    tc.get_user("testuser")
    tc.get_all_users()
    tc.delete_user("testuser")

    '''
    files = test.generate_test_file(1024)
    test.write_files_to_smb(files)
    test.delete_file_from_smb(files, share_name='SmartWare')
    print('PASS')
    
    
 