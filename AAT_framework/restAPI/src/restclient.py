"""
Created on May 27, 2014

@author:
"""


from urllib import quote_plus
import base64
import copy
import datetime
import decorator
import os
import random
import requests
import sys
import time
import types
import xml.etree.ElementTree
import xmltodict
import urllib2
import collections

from global_libraries import wd_exceptions
from requests import exceptions
from requests import models

from global_libraries.testdevice import Fields
# noinspection PyPep8Naming
import global_libraries.CommonTestLibrary as ctl
from global_libraries import utilities
from global_libraries.performance import Stopwatch
from global_libraries.constants import Modes

TEST_SHARE_NAME = utilities.TEST_SHARE_NAME
TEST_SHARE_DESC = utilities.TEST_SHARE_DESC
REST_CALL_TIMEOUT = 90  # 90 second timeout


#
# Methods for wrapping REST functions
#


@decorator.decorator
def _convert_response_to_content(func, *args, **kwargs):
    """
    Decorator to convert Response objects to their content.
    """
    args = list(args)
    temp_args = list(args)
    for i, next_arg in enumerate(temp_args):
        if type(next_arg) == requests.models.Response:
            arg_list = [next_arg]
        else:
            if isinstance(next_arg, collections.Iterable):
                arg_list = next_arg
            else:
                arg_list = [next_arg]

        for arg in arg_list:
            if type(arg) == requests.models.Response:
                args[i] =  arg.content

    temp_kwargs = dict(kwargs)
    for key, next_arg in temp_kwargs.items():
        if type(next_arg) == requests.models.Response:
            arg_list = [next_arg]
        else:
            if isinstance(next_arg, collections.Iterable):
                arg_list = next_arg
            else:
                arg_list = [next_arg]

        for arg in arg_list:
            if type(arg) == requests.models.Response:
                kwargs[key] = arg.content

    return func(*args, **kwargs)


@decorator.decorator
def _clean_share_name(func, *args, **kwargs):
    """
    Decorator to remove dashes and underscores from share names.
    """
    temp_kwargs = dict(kwargs)
    for key, arg in temp_kwargs.items():
        if key == 'share_name':
            kwargs[key] = arg.replace('_', '').replace('-', '')
            break
    return func(*args, **kwargs)


class RestError(Exception):
    def __init__(self, message='REST Error', response=None):
        Exception.__init__(self, message)
        self.response = response

    def __str__(self):
        return '<{0} {1}>'.format(self.message, repr(self.response))

    def __repr__(self):
        return '<{0} {1}>'.format(self.message, repr(self.response))


def _convert_booleans(params):
    """
    Convert 'enable', 'disable', 'on', 'off' to 'true' or 'false'.
    """
    for key, value in params.items():
        if str(value).strip().lower() == 'enable' or str(value).strip().lower() == 'on':
            params[key] = 'true'
        elif str(value).strip().lower() == 'disable' or str(value).strip().lower() == 'off':
            params[key] = 'false'


def _convert_booleans_to_strings(params):
    """
    Convert True or False to 'true' or 'false'.
    """
    for key, value in params.items():
        if value is True or str(value).strip().lower() == 'true':
            params[key] = 'true'
        elif value is False or str(value).strip().lower() == 'false':
            params[key] = 'false'


class RestClient(object):
    """
    Library with convenience methods for accessing the WD REST API.


    = Getting data out of XML =

    You can either parse the XML yourself (xml.etree.ElementTree is the
    easiest way to do this) or use the `Get XML Tag` helper method.

    For example:

    'Get Storage Usage' returns the XML below:
    | <storage_usage>
    |   <size>152162</size>
    |   <usage>145031554</usage>
    |   <video>0</video>
    |   <photos>81082671</photos>
    |   <music>62489606</music>
    |   <other>1459277</other>
    |   <shares>
    |     <share>
    |       <sharename>USB_DRIVE</sharename>
    |       <usage>51033037</usage>
    |       <video>0</video>
    |       <photos>49732482</photos>
    |       <music>0</music>
    |       <other>1300555</other>
    |     </share>
    |     <share>
    |       <sharename>WD_USB</sharename>
    |       <usage>93998517</usage>
    |       <video>0</video>
    |       <photos>31350189</photos>
    |       <music>62489606</music>
    |       <other>158722</other>
    |     </share>
    |   </shares>
    | </storage_usage>

    To get the first value of _usage_, use `Get XML Tag` as seen below:
    == Example ==
    | def get_disk_usage(self):
    |     """

    def __init__(self, uut):
        self.uut = uut

        '''
        By default,
        self.api_url = 'http://{0}/api/{1}/rest/'.format(self.uut[Fields.internal_ip_address], self.api_version)
        We are currently using api_version 2.1 (2.6 as of 2/11/15)

        You can override this if you need to.
        '''

        # Setup the logger
        self.currentDir = os.path.dirname(os.path.realpath(__file__))
        self.logger = ctl.getLogger('AAT.restclient')
        self.requests_log = ctl.getLogger('AAT.restclient.requests')
        self.debug = self.logger.debug

        self.common = None

        # Dictionary for storing successful authentication methods
        self.successful_authentication = {}
        self.successful_authentication_history = {}

        self.debug('REST Client constructed:')
        self.debug('IP address: {0}, username: {1}, password: {2}'.
                   format(self.uut[Fields.internal_ip_address],
                          self.uut[Fields.rest_username],
                          self.uut[Fields.rest_password]))


    # def _rest_url(self, function_name, api_version='2.1'):
    def _rest_url(self, function_name, api_version='2.6'):
        """
        Return the REST API's URL for a given function

        """

        return 'http://{0}/api/{1}/rest/{2}'.format(self.uut[Fields.internal_ip_address],
                                                    api_version,
                                                    function_name)

    def _rest_put(self, function_name, **kwargs):
        """Do a REST PUT call"""
        return self._rest_call(requests.put, function_name, **kwargs)

    def _rest_get(self, function_name, **kwargs):
        """Do a REST GET call"""
        return self._rest_call(requests.get, function_name, **kwargs)

    def _rest_post(self, function_name, **kwargs):
        """Do a REST POST call"""
        return self._rest_call(requests.post, function_name, **kwargs)

    def _rest_delete(self, function_name, **kwargs):
        """Do a REST DELETE call"""
        return self._rest_call(requests.delete, function_name, **kwargs)

    def _rest_head(self, function_name, **kwargs):
        """Do a REST HEAD call"""
        return self._rest_call(requests.head, function_name, **kwargs)

    def _rest_options(self, function_name, **kwargs):
        """Do a REST OPTIONS call"""
        return self._rest_call(requests.options, function_name, **kwargs)

    def _is_successful(self, result, start_time=None):
        """Return True if the request is successful (2xx)."""
        self.debug('Request Status: ' + str(result.status_code))
        if start_time:
            self.debug('Request took {0} seconds'.format(int(time.time() - start_time)))
        return ((200 <= result.status_code < 300)
                and not 'stack trace' in result.content.lower()
                and not 'bad request' in result.content.lower())

    #
    # Method for authenticating/sending data to REST functions
    #

    # noinspection PyPep8Naming
    def _URL_authentication(self, request_function, args, temp_kwargs):
        """URL-based authentication"""
        temp_kwargs['params']['auth_username'] = self.uut[Fields.rest_username]
        temp_kwargs['params']['auth_password'] = self.uut[Fields.rest_password]
        return request_function(*args, **temp_kwargs)

    def _reopen_file(self, temp_kwargs, kwargs):
        """Workaround to allow for reopening file after deepcopy."""
        if 'files' in kwargs:
            self.debug('Reopening {0}'.format(temp_kwargs['files']['file']))
            filename = kwargs['files']['file'].name
            if not temp_kwargs['files']['file'].closed:
                self.debug('Closing {0}'.format(temp_kwargs['files']['file']))
                temp_kwargs['files']['file'].close()
            temp_kwargs['files']['file'] = open(filename, 'rb')
            self.debug('Reopened {0}'.format(temp_kwargs['files']['file']))
        return temp_kwargs

    def _convert_params_to_ascii(self, params):
        """Converts Unicode params to ASCII."""
        for key, value in params.items():
            if isinstance(type(value), types.UnicodeType):
                self.debug(str(value) + ' is unicode. Converting to ASCII.')
                params[key] = value.encode('ascii', 'ignore')

    def create_rest_report(self):
        """Creates a report as a text file"""
        self.debug(self.successful_authentication_history)
        with open('rest_report.txt', 'w') as reportfile:
            for rest_function, authentication_set in self.successful_authentication_history.items():
                self.debug('{0:<50}: {1}\n'.format(rest_function, authentication_set))
                reportfile.write('{0:<50}: {1}\n'.format(rest_function, authentication_set))


    def _rest_call(self, request_function, *args, **kwargs):
        """

        To use this method, pass any parameters/data you have in as a dict
        assigned to the keyword 'params'.

        'request_function' must be a requests call, like requests.post or requests.get
        """

        if not self.uut[Fields.internal_ip_address]:
            raise RestError('Test unit IP address not found!')
        self.debug('request_function.__name__: {0}'.format(request_function.__name__))
        self.debug('args: {0}'.format(args))
        self.debug('kwargs: {0}'.format(kwargs))
        if not 'params' in kwargs:
            kwargs['params'] = {}

        rest_function_name = args[0]

        # filter None values out of params
        for key, value in dict(kwargs['params']).items():
            if value is None:
                del kwargs['params'][key]

        # set default request timeout to ninety seconds
        if not 'timeout' in kwargs:
            kwargs['timeout'] = REST_CALL_TIMEOUT

        self._convert_params_to_ascii(kwargs['params'])
        _convert_booleans_to_strings(kwargs['params'])


        # authentication method
        funcs = [self._URL_authentication]

        # api version
        # versions = ['2.1']
        versions = ["2.6"]
        urls = [self._rest_url(args[0], ver) for ver in versions]
        result = None
        last_status = None

        for url in urls:
            args = (url,)
            skip = False
            # try each authentication method
            for func in funcs:
                if skip:
                    break
                try:
                    # if this request function and URL have used before, skip
                    # directly to what worked before
                    if (request_function, args) in self.successful_authentication:
                        func = self.successful_authentication[(request_function, args)]
                        self.debug('Using {0} for {1}'.format(func.__name__, args))

                    temp_kwargs = copy.deepcopy(kwargs)
                    _convert_booleans(temp_kwargs['params'])
                    temp_kwargs = self._reopen_file(temp_kwargs, kwargs)
                    self.debug('Firmware build name: {0}'.format(os.environ.get('FIRMWARE_BUILD_NAME')))
                    self.debug(
                        '{0} args={1}, kwargs={2} auth={3}'.format(request_function.__name__.upper(), args, temp_kwargs,
                                                                   func.__name__))
                    start_time = time.time()
                    result = func(request_function, args, temp_kwargs)
                    self.debug('{0} {1}'.format(request_function.__name__.upper(), result.url))
                     
                    last_status = result.status_code
                    if self._is_successful(result, start_time):
                        self.successful_authentication[(request_function, args)] = func
                        if not (rest_function_name,
                                request_function.__name__.upper()) in self.successful_authentication_history:
                            self.successful_authentication_history[
                                (rest_function_name, request_function.__name__.upper())] = [set(), 0, 0, 0]
                        self.successful_authentication_history[(rest_function_name, request_function.__name__.upper())][
                            0].add(func.__name__)
                        self.successful_authentication_history[(rest_function_name, request_function.__name__.upper())][
                            1] += 1
                        count = \
                            self.successful_authentication_history[
                                (rest_function_name, request_function.__name__.upper())][1]
                        duration = time.time() - start_time
                        self.successful_authentication_history[(rest_function_name, request_function.__name__.upper())][
                            2] += duration
                        total_duration = \
                            self.successful_authentication_history[
                                (rest_function_name, request_function.__name__.upper())][2]
                        self.successful_authentication_history[(rest_function_name, request_function.__name__.upper())][
                            3] = total_duration / float(count)

                        if len(str(result.content)) < 10000:
                            self.debug(format(result.content))
                        else:
                            self.debug(
                                'Result content is over 10000 characters long. First 10000 characters: \n{0}'.format(
                                    str(result.content[0:10000])))
                        return result
                    else:
                        if result.content:
                            self.debug(format(result.content))
                            if '<error_message>' in result.content:
                                error_message = self.get_xml_tag(result.content, 'error_message', verbose=False)
                                self.debug('ERROR MESSAGE: "{0}"'.format(error_message))
                        if result.status_code:
                            if result.status_code == 404:
                                self.logger.debug('404 Request not found ' + str(result))
                            elif result.status_code == 403:
                                self.logger.debug('403 Request is forbidden' + str(result))
                            elif result.status_code == 500:
                                raise RestError('500 Internal server error' + str(result))
                            return result
                except (RestError, requests.exceptions.Timeout) as e:
                    self.logger.debug(ctl.exception('Failed to issue REST call'))
                    skip = True
                    if '500 Internal server error' in e.message:
                        raise
                if (request_function, args) in self.successful_authentication:
                    del self.successful_authentication[(request_function, args)]

                if result is not None:
                    self.debug('{0} {1}'.format(request_function.__name__.upper(), result.url))
                    self.debug('Failed REST kwargs: {0}'.format(kwargs))
                    self.debug('{0}'.format(result.content))
                    self.debug('REST call failed: {0}\n\n\n'.format(result))

                # rate limit
                time.sleep(1)
        self.debug('*ERROR* \n{0}'.format(datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p")))
        self.debug('*ERROR* request_function.__name__: {0}'.format(request_function.__name__))
        self.debug('*ERROR* args: {0}'.format(args))
        self.debug('*ERROR* kwargs: {0}'.format(kwargs))
        if result:
            result.raise_for_status()
        else:
            if not last_status:
                last_status = 'timeout'
            raise RestError('REST call failed. Status was {0}'.format(last_status))

    @utilities.decode_unicode_args
    def is_true(self, boolean):
        """Returns a python usable boolean no matter which string is used"""
        boolean = boolean.strip().lower()
        return boolean in ['true', 'on', 'enable']

    @utilities.decode_unicode_args
    def is_false(self, boolean):
        """Returns a python usable boolean no matter which string is used"""
        boolean = boolean.strip().lower()
        return boolean in ['false', 'off', 'disable']

    @utilities.decode_unicode_args
    @_convert_response_to_content
    def get_xml_tags(self, xml_content, tag):
        """Returns the first instance of the given tag in the given XML tree."""
        return ctl.get_xml_tags(xml_content=xml_content, tag=tag)

    @utilities.decode_unicode_args
    @_convert_response_to_content
    def get_xml_tag(self, xml_content, tag, verbose=True):
        """Returns the first instance of the given tag in the given XML tree."""
        self.debug('Searching for {0} in {1}'.format(tag, xml_content))
        if xml_content:
            xml_content = utilities.clean_xml(xml_content)
            tree = xml.etree.ElementTree.fromstring(xml_content)
            if tag:
                tag = str(tag).strip().strip('"\'')
            try:
                element = tree.iter(tag).next()
                text = None
                if element.text:
                    if verbose:
                        self.debug('element.text: {0}'.format(element.text))
                    text = element.text.strip()
                # if not text:
                # # we have an element that has children
                # if verbose:
                # self.logger.debug('element tostring: {0}'.format(xml.etree.ElementTree.tostring(element)))
                # text = xml.etree.ElementTree.tostring(element)

                return text

            except StopIteration:
                raise RestError("XML tag not found or doesn't have a value: {0}".format(tag))

    @utilities.decode_unicode_args
    @_convert_response_to_content
    def get_xml_children_dict(self, xml_content, flat=True):
        """Returns a dict of all the tags (and their content) in an XML tree."""
        if not flat:
            result = xmltodict.parse(xml_content)
            self.debug(str(result))
            return result
        else:
            if xml_content:
                xml_content = utilities.clean_xml(xml_content)
                tree = xml.etree.ElementTree.fromstring(xml_content)
                d = {}
                for elem in tree:
                    d[elem.tag] = elem.text or ''
                    d[elem.tag] = d[elem.tag].strip()
                return d

    def _get_params(self, parameters):
        """Returns the given parameters"""
        self.debug('*DEBUG* _get_params() running')
        for key, value in parameters.items():
            self.debug('*DEBUG*     ------------------------------')
            self.debug('*DEBUG*     "{0}"="{1}"'.format(key, value))
            self.debug('*DEBUG*     "{0}" starts with "{1}" : "{2}"'.format(str(value), '{0}='.format(key),
                                                                            str(value).startswith('{0}='.format(key))))
            if str(value).startswith('{0}='.format(key)):
                parameters[key] = str(value).split('{0}='.format(key))[1]
                self.debug('*DEBUG*     "{0}"="{1}"'.format(key, value))
        self.debug('*DEBUG* _get_params() exiting')
        return {key: value for (key, value) in parameters.items() if key != 'self'}

    #
    # REST API function wrappers
    #

    def reboot(self, timeout=REST_CALL_TIMEOUT):
        """
        Reboot using a REST API put.

        XML Response Example:
        | <shutdown>
        |   <status>success</status>
        | </shutdown>
        """
        result = self._rest_put('shutdown', timeout=timeout, params={'state': 'reboot'})
        return result

    def shutdown(self, timeout=REST_CALL_TIMEOUT):
        """
        Shutdown using a REST API put.

        XML Response Example:
        | <shutdown>
        |   <status>success</status>
        | </shutdown>
        """

        result = self._rest_put('shutdown', timeout=timeout, params={'state': 'halt'})
        return result

    @utilities.decode_unicode_args
    def local_login(self, username='admin', password=''):
        """
        Local authentication using a REST API get.

        XML Response Example:
        | <local_login>
        |   <status>success</status>
            <wd2go_server/>
        | </local_login>
        """
        params = self._get_params(locals())
        self.uut[Fields.rest_username] = username
        self.uut[Fields.rest_password] = password
        result = self._rest_get('local_login',
                                params=params)
        return result

    def logout(self):
        """
        Log out using a REST API get.

        XML Response Example:
        | <logout>
        |   <status>success</status>
        | </logout>
        """
        result = self._rest_get('logout')
        return result

    @utilities.decode_unicode_args
    def login(self, device_user_id, device_user_auth_code):
        """
        Login using a REST API get.

        XML Response Example:
        | <login>
        |   <status>success</status>
            <username>myUser</username>
            <userid>myUserId</userid>
        | </login>
        """
        # Note: Both userid and authcode can be adquired using 'device_user' rest call
        params = self._get_params(locals())
        result = self._rest_get('login',
                                params=params)
        return result

    def port_test(self, timeout=REST_CALL_TIMEOUT):
        """
        Test communications by retrieving the device id.

        XML Response Example:
        | <port_test>
        |   <deviceid>27258</deviceid>
        | </port_test>
        """
        result = self._rest_get('port_test', timeout=timeout)
        return result

    def status(self):
        """
        Get status of the processes.

        XML Response Example:
        | <status>
        |   <communicationmanager>running</communicationmanager>
        |   <mediacrawler>running</mediacrawler>
        |   <miocrawler>running</miocrawler>
        | </status>
        """
        result = self._rest_get('status')
        return result

    def system_state(self):
        """
        Get system state.

        XML Response Example:
        | <system_state>
        |   <status>ready</status>
        |   <temperature>good</temperature>
        |   <smart>good</smart>
        |   <volume>good</volume>
        |   <free_space>good</free_space>
        | </system_state>
        """
        result = self._rest_get('system_state')
        return result

    def is_ready(self, timeout=REST_CALL_TIMEOUT):
        """
        Returns True if the system state is 'ready', False otherwise.
        """
        # noinspection PyBroadException
        try:
            port_test_result = self.port_test(timeout=timeout)
        except Exception:
            self.logger.debug(ctl.exception('Failed to pass port test'))
            return False
        else:
            if self._is_successful(port_test_result):
                # noinspection PyBroadException
                try:
                    result = (self.get_xml_tag(self.system_state().content, 'status') == 'ready')
                except Exception:
                    return False
                else:
                    return result

    @utilities.decode_unicode_args
    def start_smart_test(self, test='start_short'):
        """
        test: {start_short/start_long}
        """
        params = self._get_params(locals())
        result = self._rest_put('smart_test', params=params)
        return result

    def get_smart_results(self):
        """
        percent_complete: {Number between 0 and 100}
        status: {good/bad/in_progress/aborted}
        """
        result = self._rest_get('smart_test')
        return result

    def get_all_users(self):
        """
        Get all users.

        XML Response Example:
        | <users>
        |   <user_id>2</user_id>
        |   <username>guest</username>
        |   <fullname>Guest User</fullname>
        |   <is_admin>false</is_admin>
        |   <is_password>false</is_password>
        | </users>
        """
        # return self.get_user()
        result = self._rest_get('users')
        return result

    @utilities.decode_unicode_args
    def get_user(self, username):
        """
        Get user account information.

        XML Response Example:
        | <users>
        |   <user_id>2</user_id>
        |   <username>guest</username>
        |   <fullname>Guest User</fullname>
        |   <is_admin>false</is_admin>
        |   <is_password>false</is_password>
        | </users>
        """
        # params = {}
        # url = 'users'
        '''
        if user_id:
            params = {'user_id': user_id}
        elif username:
            # params = {'username': username}
            url += '/{0}'.format(username)
        '''

        result = self._rest_get('users?username={0}'.format(username))
        return result

    @utilities.decode_unicode_args
    def set_ssh(self, enable=True, enablessh=None):
        """
        Modify SSH configuration.

        XML Response Example:
        | <ssh_configuration>
        |   <status>Success</status>
        | </ssh_configuration>

        """
        if enablessh is not None:
            params = {'enablessh': enablessh}
        else:
            # params = self._get_params(locals())
            params = {'enablessh': enable}
        result = self._rest_put('ssh_configuration', params=params)
        return result

    @utilities.decode_unicode_args
    def get_ssh_configuration(self):
        """
        XML Response Example:
        | <ssh_configuration>
        |   <enablessh>true</enablessh>
        | </ssh_configuration>
        """
        result = self._rest_get('ssh_configuration')
        return result

    def get_firmware_info(self):
        """
        XML Response Example:
        | <firmware_info>
        |  <current_firmware>
        |    <package>
        |      <name>MyBookLiveDuo </name>
        |      <version>00.00.00-000</version>
        |      <description>Core F/W</description>
        |      <package_build_time>1364429632</package_build_time>
        |      <last_upgrade_time>1364429632</last_upgrade_time>
        |    </package>
        |  </current_firmware>
        |  <firmware_update_available>
        |    <available>false</available>
        |  </firmware_update_available>
        |  <upgrades>
        |    <available>false</available>
        |    <message></message>
        |  </upgrades>
        | </firmware_info>
        """
        result = self._rest_get('firmware_info')
        return result

    # # Immediately check for firmware update available
    def firmware_info(self):
        """
        XML Response Example:
        <firmware_info>
        |    <status>success</status>
        </firmware_info>
        """
        result = self._rest_put('firmware_info')
        return result

    # @ Brief, this function is fetching the build image automatically
    def firmware_update(self, image):
        """
        Description:
            Causes NAS to fetch and update FW automatically.

        Security:
            Requires user authentication (LAN/WAN)

        HTTP Method: PUT
            http://localhost/api/2.1/rest/firmware_update

        Parameters:
            image    String - required
            format    String - optional (default is xml)

        Parameter Details:
            image=http://download.wdc.com/nas/apnc-020100-20110807.deb
            Note that this URI string must be encoded (e.g. php urlencode()) for passing as a parameter to the REST API.

        Return values:
            status    String - success

        XML Response Example:
        <firmware_update>
        |    <status>success</status>
        </firmware_update>
        """
        result = self._rest_put('firmware_update?image={0}'.format(image))
        return result

    # @ Brief this function take a build name which is already copied and resided in /CacheVolume/ path of your unit
    def firmware_update_by_file(self, fw_filename):
        #result = self._rest_post('firmware_update?filepath=/CacheVolume/{0}'.format(fw_filename))
        result = self._rest_post('firmware_update?filepath=/shares/Public/{0}'.format(fw_filename)) 
        return result

    def delete_all_alerts(self):
        """
        Delete all alerts.
        """
        alerts = self.get_alerts()
        alert_ids = set()
        for line in alerts.text.split('\n'):
            if 'alert_id' in line:
                line_list = line.split('&amp;')
                for piece in line_list:
                    if 'alert_id' in piece:
                        alert_ids.add(piece.strip('alert_id='))
        for alert_id in alert_ids:
            self.debug('Deleting alert ID: {0}'.format(alert_id))
            
            result = self.delete_alert(alert_id)
            if not self._is_successful(result):
                raise wd_exceptions.RestCallUnsuccessful()         
        self.debug('Deleted {0} alerts'.format(len(alert_ids)))
    
    @utilities.decode_unicode_args
    def delete_alert(self, alert_id, ack=False):
        """
        XML Response Example:
        | <alerts>
        |   <status>{return status}</status>
        | </alerts>
        """
        result = self._rest_put('alerts', params={'alert_id': alert_id, 'ack': ack})
        return result

    @utilities.decode_unicode_args
    def acknowledge_alert(self, alert_id, ack=True):
        """
        Used for acknowledging an alert.

        XML Response Example:
        | <alerts>
        |     <status>{return status}</status>
        | </alerts>

        """
        params = self._get_params(locals())
        result = self._rest_put('alerts', params=params)
        return result

    @utilities.decode_unicode_args
    def get_alerts(self, simple=True, include_admin=True, admin=False,
                   specific=False, all=True, hide_ack=True, min_level=10,
                   limit=20, offset=0, order='desc'):
        """
        XML Response Example:
        | <?xml version="1.0" encoding="utf-8"?>
        | <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
        |   <channel>
        |     <title>alerts</title>
        |     <link>{alert link}</link>
        |     <atom:link href="{IP Address}/api/1.0/rest/alerts?format=rss" rel="self" type="application/rss+xml" />
        |     <description>System Alerts</description>
        |     <lastBuildDate>{alert date}</lastBuildDate>
        |     <language>{language}</language>
        |     <ttl>1</ttl>
        |     <item>
        |         <title>{alert title}</title>
        |         <link>{IP Address}/api/1.0/rest/display_alert?code={alert code}&amp;timestamp={alert time stamp}&amp;alert_id={alert id}&amp;acknowledged={alert acknowledgement}</link>
        |         <description>{alert description}</description>
        |         <pubDate>{xml publish date}</pubDate>
        |         <guid isPermaLink="true">{IP Address}/api/2.1/rest/display_alert?code={alert code}&amp;timestamp={alert time stamp}&amp;alert_id={alert id}&amp;acknowledged={alert acknowledgement}&amp;guid=60d409a4</guid>
        |     </item>
        |   </channel>
        | </rss>
        """
        params = self._get_params(locals())
        result = self._rest_get('alerts', params=params)
        return result

    def get_alert_catalogue(self):
        """
        XML Response Example:
        | <alert_events>
        |   <alerts>
        |     <alert>
        |       <code>0201</code>
        |       <severity>critical</severity>
        |       <scope>all</scope>
        |       <admin_ack_only>1</admin_ack_only>
        |       <description>Drive failed</description>
        |     </alert>
        |     <alert>
        |       <code>1202</code>
        |       <severity>warning</severity>
        |       <scope>all</scope>
        |       <admin_ack_only>0</admin_ack_only>
        |       <description>Drive too small</description>
        |     </alert>
        |     <alert>
        |       <code>2203</code>
        |       <severity>Info</severity>
        |       <scope>admin</scope>
        |       <admin_ack_only>1</admin_ack_only>
        |       <description>Drive is being initialized</description>
        |     </alert>
        |   </alerts>
        | </alert_events>
        """
        result = self._rest_get('alert_events')
        return result

    def get_alert_configuration(self):
        """
        XML Response Example:
        | <alert_configuration>
        |     <email_enabled>false</email_enabled>
        |     <email_returnpath>nas.alerts@wdc.com</email_returnpath>
        |     <min_level_email>10</min_level_email>
        |     <min_level_rss>10</min_level_rss>
        |     <email_recipient_0></email_recipient_0>
        |     <email_recipient_1></email_recipient_1>
        |     <email_recipient_2></email_recipient_2>
        |     <email_recipient_3></email_recipient_3>
        |     <email_recipient_4></email_recipient_4>
        | </alert_configuration>

        """
        result = self._rest_get('alert_configuration')

        return result

    @utilities.decode_unicode_args
    def set_alert_email(self, recipient=None, email_address=None):
        """
        Set an alert configuration for a single email/recipient pair.
        """
        recipient_dict = {}

        try:
            recipient = int(recipient)
            if 0 <= recipient < 5:
                recipient = 'email_recipient_{0}'.format(recipient)
            else:
                raise RestError('invalid recipient: {0}'.format(recipient))
        except ValueError:
            pass

        recipient_dict[recipient] = email_address
        result = self.set_alert_configuration(**recipient_dict)
        return result

    @utilities.decode_unicode_args
    def set_alert_configuration(self,
                                email_enabled='${CURRENT}',
                                min_level_email='${CURRENT}',
                                min_level_rss='${CURRENT}',
                                email_recipient_0='${CURRENT}',
                                email_recipient_1='${CURRENT}',
                                email_recipient_2='${CURRENT}',
                                email_recipient_3='${CURRENT}',
                                email_recipient_4='${CURRENT}'):
        """
        XML Response Example:
        | <alert_configuration>
        |     <status>SUCCESS</status>
        | </alert_configuration>

        """

        args = self._get_params(locals())
        device_settings = self.get_xml_children_dict(self.get_alert_configuration().content)
        params = {arg: (value if value != '${CURRENT}' else device_settings[arg]) for (arg, value) in args.items()}
        params = {arg: ('' if value is None else value) for (arg, value) in params.items()}

        result = self._rest_put('alert_configuration', params=params)
        return result

    def test_alert_configuration(self):
        """
        Sends an email to all configured recipients
        XML Response Example:
        | <alert_configuration_test>
        |     <email_recipient_0 Status="Success">john.doe@wdc.com</email_recipient_0>
        | </alert_configuration_test>

        """
        result = self._rest_post('alert_test_email')

        return result

    def send_email_alerts(self):
        """
        """
        result = self._rest_post('alert_notify')
        return result

    class Alerts(object):
        temperatureOver = "0001"
        temperatureUnder = "0002"
        temperatureNormal = "2003"
        driveSmartFail = "0003"
        volumeFailure = "0004"
        diskNearCapacity = "1001"
        newFirmwareAvailable = "2002"
        networkLinkDown = "1002"
        restart = "2001"
        firmwareUpdateSucceeded = "2004"
        firmwareUpdateFailed = "1003"
        factoryRestoreSucceeded = "2005"

    @utilities.decode_unicode_args
    def generate_alert(self, alert_code, alert_description_string, user_id):
        """
        XML Response Example:
        | <alerts>
        |   <status>{return status}</status>
        | </alerts>
        """
        if alert_code in self.Alerts.__dict__:
            alert_code = self.Alerts.__dict__[alert_code]
        result = self._rest_post('alerts', params={'code': alert_code,
                                                   'parameter': alert_description_string,
                                                   'user': user_id})
        return result

    @utilities.decode_unicode_args
    def delete_user(self, user):
        """
        A single user can be identified either by the user_id or by the
        username. Only after it is found that there is no user with a user_id
        that matches, then the user will attempt to be found based on a
        matching username. Such a request to delete a single user requires
        one of the two parameters to be specified. There is no facility to
        delete all users in one API call. Either the user_id or username
        must be used to identify the user to be deleted.

        XML Response Example:
        | <users>
        |   <status>success</status>
        | </users>
        """
        result = self._rest_delete('users/{0}'.format(user))

        return result

    @utilities.decode_unicode_args
    def delete_all_users(self, exclude='admin'):
        """Delete all users except those specified in exclude. Exclude
        defaults to 'admin'."""
        # Convert to a list if necessary
        if isinstance(exclude, basestring):
            exclude = [exclude]

        users = self.get_all_users()
        if not users:
            raise RestError('Failed to get list of users.')
        content = xml.etree.ElementTree.fromstring(users.content)
        users = {user.text for user in content.iter('username')}
        for user in users:
            if user in exclude:
                continue
            self.debug('Deleting user: {0}'.format(user))
            self.delete_user(user)


    @utilities.decode_unicode_args
    def create_user(self, username, password=None, fullname=None, is_admin=False,group_names='cloudholders'):
        """
        A username must be an alphanumeric value between 1 and 32 characters
        and cannot contain spaces; but is allowed to start with a letter or a
        number, and contain the underscore character.
        The length of a password can be between 0 and 16 characters without spaces

        XML Response Example:
        | <users>
        |   <status>success</status>
        |   <username>myUserName<\username>
        |   <user_id>2</user_id>
        | </users>
        """
        if password:
            self.debug('base64 encoding password: {0}'.format(password))
            password = base64.standard_b64encode(password)
        params = self._get_params(locals())
        result = self._rest_post('users', params=params)
        return result

    def update_user(self, user, old_password=None, new_password=None, fullname=None, is_admin=None):
        """
        A single user can be identified either by the user_id or by the
        username. Only after it is found that there is no user with a user_id
        that matches, then the user will attempt to be found based on a
        matching username. Such a request to update a single user requires one
        of the two parameters to be specified. To change a user's password both
        old_password and new_password are required; except for an Admin user,
        who only needs to supply the new_password parameter.
        When testing the API, the password must be base 64 encoded.
        Refer to the Create User section for restrictions.
        Either the user_id or the username must be used to identify the user to
        be updated. Neither of these values can be modified.

        XML Response Example:
        | <users>
        |   <status>success</status>
        | </users>
        """
        if old_password is not None:
            self.debug('base64 encoding old_password: {0}'.format(old_password))
            old_password = base64.standard_b64encode(old_password)
        if new_password is not None:
            self.debug('base64 encoding new_password: {0}'.format(new_password))
            new_password = base64.standard_b64encode(new_password)

        params = self._get_params(locals())
        user = params['user']
        del params['user']
        returned_call = self._rest_put('users/{0}'.format(user), params=params)

        '''
        if user == 'admin':
            self.uut[Fields.rest_password] = base64.standard_b64decode(new_password)
            #local login to the unit with new password
            self.local_login(user, self.uut[Fields.rest_password])
        else:
            self.local_login(user, new_password)
        return returned_call
        '''
        if new_password is not None and user == self.uut[Fields.rest_username]:
            self.uut[Fields.rest_password] = base64.standard_b64decode(new_password)
            # local login to the unit with new password
            self.local_login(user, self.uut[Fields.rest_password])

        result = returned_call
        return result

    def get_disk_size(self):
        """
        Returns total disk size in MB.
        """
        result = self.get_xml_tag(self.get_storage_usage().content, 'size')
        return result

    def get_disk_usage(self):
        """
        Returns total disk usage in MB.
        """
        result = self.get_xml_tag(self.get_storage_usage().content, 'usage')
        return result

    def get_storage_usage(self):
        """
        XML Response Example:
        | <storage_usage>
        |   <size>152162</size>
        |   <usage>145031554</usage>
        |   <video>0</video>
        |   <photos>81082671</photos>
        |   <music>62489606</music>
        |   <other>1459277</other>
        |   <shares>
        |     <share>
        |       <sharename>USB_DRIVE</sharename>
        |       <usage>51033037</usage>
        |       <video>0</video>
        |       <photos>49732482</photos>
        |       <music>0</music>
        |       <other>1300555</other>
        |     </share>
        |     <share>
        |       <sharename>WD_USB</sharename>
        |       <usage>93998517</usage>
        |       <video>0</video>
        |       <photos>31350189</photos>
        |       <music>62489606</music>
        |       <other>158722</other>
        |     </share>
        |   </shares>
        | </storage_usage>
        """
        result = self._rest_get('storage_usage')
        return result

    def get_system_information(self):
        """
        XML Response Example:
        |<system_information>
        |  <manufacturer>Western Digital Corporation</manufacturer>
        |  <manufacturer_url>http://www.wdc.com</manufacturer_url>
        |  <model_description>WD My Cloud 4-Bay Network Storage</model_description>
        |  <model_name>WDMyCloudEX4</model_name>
        |  <model_url>http://products.wd.com/wdmycloudex4</model_url>
        |  <model_number>LT4A</model_number>
        |  <host_name>WDMyCloudEX4</host_name>
        |  <capacity>1</capacity>
        |  <serial_number>string</serial_number>
        |  <master_drive_serial_number>6078486b:1a4455a5:bc7c7109:4227785e</master_drive_serial_number>
        |  <mac_address>00:90:A9:66:56:C6</mac_address>
        |  <uuid>73656761-7465-7375-636b-0090a96656c6</uuid>
        |  <wd2go_server/><dlna_server/>
        |</system_information>

        """
        result = self._rest_get('system_information')
        return result

    def get_system_configuration(self, attach_file=False):
        """
        XML Response Example:
        | <system_configuration>
        |   <path_to_config>/CacheVolume/name-20120530-1823.conf</path_to_config>
        | </system_configuration>
        """
        if attach_file:
            params = {'attach_file': 'enable'}
        else:
            params = {'attach_file': 'disable'}
        result = self._rest_get('system_configuration', params=params)
        return result

    def restore_system_configuration(self, filepath=None):
        """
        XML Response Example:
        | <restore_status>
        |   <status_code></status_code>
        |   <status_description></status_description>
        | </restore_status>
        """
        params = {}
        if filepath:
            params = {'filepath': filepath}
        result = self._rest_post('system_configuration', params=params)
        return result

    def start_factory_restore(self, erase='format'):
        """
        erase should be 'format' or 'zero' or 'systemOnly'

        XML Response Example:
        | <system_factory_restore>
        |   <status>success</status>
        | </system_factory_restore>
        """

        try:
            result = self._rest_post('system_factory_restore', params={'erase': erase})
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
            # Expected if the unit rebooted
            self.logger.debug('Unit rebooted')
            result = (0, '<system_factory_restore>\n  <status>success</status>\n</system_factory_restore>')

        return result

    def get_factory_restore_progress(self):
        """
        XML Response Example:
        | <system_factory_restore>
        |   <percent></percent>
        |   <status>idle</status>
        | </system_factory_restore>
        """
        result = self._rest_get('system_factory_restore')

        return result

    def wait_for_factory_restore_to_start(self):
        """Poll the factory restore progress until it's at 100%."""
        while True:
            sys.stdout.flush()
            try:
                xml_content = self.get_factory_restore_progress().content
                status = self.get_xml_tag(xml_content, 'status')
                self.debug('Factory Restore Status: {0}'.format(status))
                if status == 'inprogress' or status == 'idle':
                    break
                time.sleep(5)
            except requests.exceptions.ConnectionError:
                # Expected if the unit rebooted
                self.logger.debug('Unit rebooted')

    def wait_for_factory_restore_to_finish(self, timeout=300, wait_after_finish=60):
        print 'Waiting for factory restore / firmware update...'
        stp = Stopwatch(timer=timeout)
        stp.start()
        not_ready = True
        last_status = None
        while not_ready:
            try:
                xml_content = self.get_system_state().content
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, RestError):
                # Not ready, see if the timeout has been reached
                if stp.is_timer_reached():
                    raise exceptions.Timeout('Factory restore never finished')
            else:
                status = self.get_xml_tag(xml_content, 'status')
                self.logger.info('System Status: {0}'.format(status))

                if status != last_status:
                    last_status = status

                if status == 'ready':
                    self.logger.info('System ready, exit while loop')
                    not_ready = False

            self.logger.debug('Unit not ready. Waiting 5 seconds.')
            time.sleep(5)

        # TODO: Do we really need this fixed delay, or can we check if it is still loading?
        if wait_after_finish:
            self.logger.debug('Additional {0} second delay just in case unit is loading'.format(wait_after_finish))
            time.sleep(wait_after_finish)
        return 0

    def get_firmware_update_configuration(self):
        """
        XML Response Example:
        | <firmware_update_configuration>
        |   <auto_install>disable</auto_install>
        |   <auto_install_day>0</auto_install_day>
        |   <auto_install_hour>3</auto_install_hour>
        | </firmware_update_configuration>
        """
        result = self._rest_get('firmware_update_configuration')
        return result

    def set_firmware_update_configuration(self, auto_install="disable", auto_install_day="0", auto_install_hour="3"):
        """
        Parameter details:
        | -auto_install: enable/disable
        | -auto_install_day: {0-everyday 1-7 representing Monday-Sunday }
        | -auto_install_hour: {0-23 hour of day }

        XML Response Example:
        | <firmware_update_configuration>
        |   <status>success</status>
        | </firmware_update_configuration>
        """
        params = self._get_params(locals())
        result = self._rest_put('firmware_update_configuration', params=params)
        return result

    def get_firmware_update_status(self):
        """
        XML Response Example:
        | <firmware_update>
        |   <status>idle</status>
        |   <completion_percent></completion_percent>
        |   <error_code></error_code>
        |   <error_description></error_description>
        | </firmware_update>
        """
        result = self._rest_get('firmware_update')
        return result

    # # Cause NAS to fetch and update FW automatically
    def set_firmware_update(self):
        """
        XML Response Example:
        | <firmware_update>
        |   <status>idle</status>
        |   <completion_percent></completion_percent>
        |   <error_code></error_code>
        |   <error_description></error_description>
        | </firmware_update>
        """
        result = self._rest_put('firmware_update')
        return result

    @utilities.decode_unicode_args
    def wait_for_firmware_update_to_start(self):
        """
        Waits for the firmware update to start
        """
        start = time.time()
        # wait for update to start
        while True:
            sys.stdout.flush()
            self.debug('Waiting for firmware update to start.')
            try:
                status = self.get_xml_tag(self.get_firmware_update_status().content, 'status')
                if status == 'upgrading':
                    self.debug('Firmware update started.')
                    break
            except requests.Timeout:
                self.debug('Firmware update started.')
                break
            except Exception as e:
                if 'Request timed out' in str(e):
                    self.debug('Firmware update started.')
                    break
            if time.time() - start > 300:
                raise RestError('Update never started')
            time.sleep(10)

    def wait_for_firmware_update_to_complete(self):
        """
        Waits for the firmware update to start
        """
        start = time.time()
        # wait for update to start
        while True:
            sys.stdout.flush()
            self.debug('Waiting for firmware update to complete.')
            try:
                status = self.get_xml_tag(self.get_firmware_update_status().content, 'status')
                if status == 'upgrading':
                    self.debug('Firmware updating...')
                    percentage = self.get_xml_tag(self.get_firmware_update_status().content, 'completion_percent')
                    self.debug('completion_percent: {}'.format(percentage))
                else:
                    break
            except requests.Timeout:
                self.debug('Firmware updating')
                break
            except Exception as e:
                if 'Request timed out' in str(e):
                    self.debug('Firmware update not completed.')
                    break
            if time.time() - start > 600:
                raise RestError('Firmware update not complete after 10 minutes')
            time.sleep(30)

    def start_firmware_update_from_uri(self, firmware_uri):
        """
        Cause NAS to fetch and update FW automatically

        XML Response Example:
        | <firmware_update
        |   <status>success</status>
        | </firmware_update>
        """
        firmware_path = quote_plus(firmware_uri)  # Input string must be url encoded

        result = self._rest_put('firmware_update', params={'firmware_update': firmware_path})
        return result


    def start_firmware_update_from_file(self, firmware_path):
        """
        Causes NAS to update to firmware in file that is copied to the device.

        If the specified path is a file on the test PC, it will be uploaded as part of the POST data (multipart file).
        http://docs.python-requests.org/en/v1.0.0/user/quickstart/#post-a-multipart-encoded-file

        Otherwise, it will be assumed that the path refers to a file on the NAS (sent as a string)
        | Example: '/DataVolume/shares/public/ap2nc-024006-048-20121206.deb'
        **Note**: This type of update will fail if the .deb is not extracted.

        XML Response Example:
        | <firmware_update
        |   <status>success</status>
        | </firmware_update>
        """

        if os.path.isfile(firmware_path):
            self.debug('{0} is a file, streaming file to test unit'.format(firmware_path))
            with open(firmware_path, 'rb') as f:
                multipart_files = {'file': f}
                return self._rest_post('firmware_update', files=multipart_files)
        else:
            self.debug('{0} is not file, specifying filepath to test unit'.format(firmware_path))
            result = self._rest_post('firmware_update', params={'filepath': firmware_path})
            return result

    def get_firmware_version(self):
        """
        XML Response Example:
        | <version>
        |   <firmware>01.00.08-432</firmware>
        |   <modules>
        |     <module>
        |       <module_name>albummetadata</module_name>
        |       <module_version>1.0</module_version>
        |       <module_description>Module has albummetadata related components.</module_description>
        |     </module>
        |     -
        |     -
        |   </modules>
        | </version>
        """
        result = self._rest_get('version')
        return result

    def get_firmware_version_number(self):
        """
        Returns the version number from the version data
        """
        result = self.get_xml_tag(self._rest_get('version').content, 'firmware')
        return result

    def get_network_configuration(self):
        """
        XML Response Example:
        | <?xml version="1.0" encoding="utf-8"?>
        | <network_configuration>
        |   <proto>dhcp_client</proto>
        |   <ip></ip>
        |   <netmask></netmask>
        |   <gateway></gateway>
        |   <dns0></dns0>
        |   <dns1></dns1>
        |   <dns2></dns2>
        | </network_configuration>
        """
        result = self._rest_get('network_configuration')
        return result

    def set_network_configuration(self,
                                  proto=None,
                                  ip=None,
                                  netmask=None,
                                  gateway=None,
                                  dns0=None,
                                  dns1=None,
                                  dns2=None):
        """
        Parameter Details:
        | -In DHCP case, ip, netmask, gateway, dns0, dns1 and dns2 values are ignored.
        | -proto: The network mode must be set to 'dhcp_client' or 'static'.
        | -ip: The IP address can be any Class A, B or C address except 127.X.X.X and those with all zeros or ones in the host portion.
        | -netmask: Is the number of leading binary 1s to make up mask. Valid values are 1 to 31.
        | -gateway: If gateway is set, IP and gateway must be on the same network.
        | -dns0: If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
        | -dns1: If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
        | -dns2: If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx

        XML Response Example:
        | <network_configuration>
        |   <status>success</status>
        | </network_configuration>
        """
        args = self._get_params(locals())
        device_settings = self.get_xml_children_dict(self.get_network_configuration().content)
        params = {arg: (value or device_settings[arg] or '') for (arg, value) in args.items()}

        result = self._rest_put('network_configuration', params=params, timeout=60)
        return result

    def get_network_configurations(self, ifname=None):
        """
        XML Response Example:
        | <?xml version="1.0" encoding="utf-8"?>
        | <network_configurations>
        |   <network_configuration>
        |     <ifname>eth0</ifname>
        |     <iftype>wired</iftype>
        |     <proto>dhcp_client</proto>
        |     <ip>192.168.0.165</ip>
        |     <netmask></netmask>
        |     <gateway></gateway>
        |     <dns0></dns0>
        |     <dns1></dns1>
        |     <dns2></dns2>
        |   </network_configuration>
        |   <network_configuration>
        |     <ifname>ath0</ifname>
        |     <iftype>wireless</iftype>
        |     <proto>dhcp_client</proto>
        |     <ip></ip>
        |     <netmask></netmask>
        |     <gateway></gateway>
        |     <dns0></dns0>
        |     <dns1></dns1>
        |     <dns2></dns2>
        |   </network_configuration>
        | </network_configurations>
        """
        function = 'network_configuration'
        if ifname:
            function += '/{0}'.format(ifname)
        result = self._rest_get(function)
        return result

    def set_network_configurations(self,
                                   ifname=None,
                                   iftype=None,
                                   proto=None,
                                   ip=None,
                                   netmask=None,
                                   gateway=None,
                                   dns0=None,
                                   dns1=None,
                                   dns2=None):
        """
        Parameter Details:
        | -In DHCP case, ip, netmask, gateway, dns0, dns1 and dns2 values are ignored.
        | -ifname: The network interface name (eth0, ath0,...).
        | -iftype: The network interface type (wired or wireless).
        | -proto: The network mode must be set to 'dhcp_client' or 'static'.
        | -ip: The IP address can be any Class A, B or C address except 127.X.X.X and those with all zeros or ones in the host portion.
        | -netmask: Is the number of leading binary 1s to make up mask. Valid values are 1 to 31.
        | -gateway: If gateway is set, IP and gateway must be on the same network.
        | -dns0: If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
        | -dns1: If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx
        | -dns2: If set, the IP address of the domain name server must be entered in the form xxx.xxx.xxx.xxx

        XML Response Example:
        | <network_configuration>
        |   <status>success</status>
        | </network_configuration>
        """
        assert ifname is not None
        args = self._get_params(locals())
        device_settings = self.get_xml_children_dict(self.get_network_configurations(ifname).content)
        params = {arg: (value or device_settings[arg] or '') for (arg, value) in args.items()}
        ifname = params['ifname']
        del params['ifname']

        result = self._rest_put('network_configurations/{0}'.format(ifname), params=params, timeout=60)
        return result

    def delete_volume(self, volume_id='2'):
        """
        XML Response Example:
        | <volumes>
        |   <status>success</status>
        | </volumes>
        """
        result = self._rest_delete('volumes/{0}'.format(volume_id))
        return result

    def get_volumes(self, volume_id=None):
        """
        XML Response Example:
        | <volumes>
        |   <volume>
        |     <volume_id>1</volume_id>
        |     <label>shares</label>
        |     <base_path>/shares</base_path>
        |     <drive_path>/dev/sda4</drive_path>
        |     <is_connected>true</is_connected>
        |   </volume>
        | </volumes>
        """
        func = 'volumes'
        if volume_id:
            func += '/{0}'.format(volume_id)
        result = self._rest_get(func)
        return result

    def create_volume(self, volume_id='2', label='test', base_path='', drive_path='/', is_connected=True):
        """
        Adds an existing volume into the Volume Database

        XML Response Example:
        | <volumes>
        |   <status>success</status>
        | </volumes>
        """
        params = self._get_params(locals())
        result = self._rest_post('volumes/{0}'.format(volume_id), params=params)
        return result

    # # Get info for all USB drive attached to test unit
    def get_usb_info(self):
        """
        XML Response Example:
        |<usb_drives>
        |    <usb_drive>
        |    <name>SanDisk Cruzer</name>
        |    <handle>1</handle>
        |    <serial_number>1000150906110937598D</serial_number>
        |    <vendor>SanDisk</vendor>
        |    <model>Cruzer</model>
        |    <revision>1100</revision>
        |    <ptp>false</ptp>
        |    <smart_status>unsupported</smart_status>
        |    <standby_timer>unsupported</standby_timer>
        |    <lock_state>unsupported</lock_state>
        |    <usage>875.061248</usage>
        |    <capacity>7988.678656</capacity>
        |    <vendor_id>0781</vendor_id>
        |    <product_id>5530</product_id>
        |    <usb_port>2</usb_port>
        |    <usb_version>2.0</usb_version>
        |    <usb_speed>480</usb_speed>
        |    <is_connected>true</is_connected>
        |    <volumes>
        |        <volume>
        |            <volume_id>1</volume_id>
        |            <base_path>/shares/Cruzer</base_path>
        |            <label></label>
        |            <mounted_date>1338572532</mounted_date>
        |            <usage>875.061248</usage>
        |            <capacity>7988.678656</capacity>
        |            <read_only>false</read_only>
        |            <shares>
        |                <share>Cruzer</share>
        |            </shares>
        |        </volume>
        |    </volumes>
        |    </usb_drive>
        |</usb_drives>
        """
        result = self._rest_get('usb_drives')
        return result

    # Retrieve info for a USB drive, providing handler
    def get_usb_info_handler(self, handler='temp'):
        """
        XML Response Example:
        <usb_drive>
        |    <name>Western_Digital My_Passport_0778</name>
        |    <handle>WX81E32UHY88</handle>
        |    <serial_number>WX81E32UHY88</serial_number>
        |    <vendor>Western_Digital</vendor>
        |    <model>My_Passport_0778</model>
        |    <revision>1.042</revision>
        |    <ptp/>
        |    <smart_status/>
        |    <standby_timer/>
        |    <lock_state>unlocked</lock_state>
        |    <password_hint/>
        |    <usage>201.562500</usage>
        |    <capacity>476908.000000</capacity>
        |    <vendor_id>1058</vendor_id>
        |    <product_id>0778</product_id>
        |    <usb_port>1</usb_port>
        |    <usb_version>3.00</usb_version>
        |    <usb_speed>5000</usb_speed>
        |    <is_connected>true</is_connected>
        |    <file_system_type>ntfs</file_system_type>
        |    <volumes>
        |        <volume>
        |            <volume_id>WX81E32UHY88</volume_id>
        |            <base_path>/mnt/USB/USB2_e1</base_path>
        |            <label>New Volume</label>
        |            <mounted_date>1402945717</mounted_date>
        |            <usage>201.562500</usage>
        |            <capacity>476904.996094</capacity>
        |            <read_only/>
        |            <shares>
        |                <share>My_Passport_0778-1</share>
        |            </shares>
        |        </volume>
        |    </volumes>
        </usb_drive>
        """
        result = self._rest_get('usb_drive/{0}'.format(handler))
        return result

    # # Unlock a USB locked drive given drive's handler
    # Note: Drive is connected to test unit (Lightning) but is not unlocked yet
    def unlock_usb_drive(self, handler='temp', drive_password='temp'):
        """
        Example: http://192.168.1.125/api/2.1/rest/usb_drive/WX61C22C7470?rest_method=PUT&drive_password=fituser

        XML Response Example:
        <usb_drive>
        |    <status>success</status>
        </usb_drive>
        """
        result = self._rest_put('usb_drive/{0}?drive_password={1}'.format(handler, drive_password))
        return result

    # # The DELETE request is used to eject a specified Usb drive, given drive's handler
    # It causes all the shares associated with the drive to be deleted and all of its partitions unmounted.
    def eject_usb_drive(self, handler='temp'):
        """
        Example: http://192.168.1.125/api/2.1/rest/usb_drive/WX61C22C7470?rest_method=DELETE

        XML Response Example:
        <usb_drive>
        |    <status>success</status>
        </usb_drive>
        """
        result = self._rest_delete('usb_drive/{0}'.format(handler))
        return result

    def get_usb_drive_tags(self):
        """
        Return USB drive info as a list of strings that can be set as test tags.

        """
        tags = set()

        # get tag info from REST XML
        drives = self.get_usb_info()
        share_names = self.get_xml_tags(drives, 'share')
        # return empty if no drives are attached
        if not share_names:
            return tags
        drive_dicts = xmltodict.parse(drives.content)
        try:
            for drive_dict in drive_dicts['usb_drives']['usb_drive']:
                self.debug(drive_dict)
                name = drive_dict['name']
                serial_number = drive_dict['serial_number']
                capacity = drive_dict['capacity']
                tag = 'name={0}_cap={1}_sn={2}'.format(name, capacity, serial_number).replace(' ', '-')
                tags.add(tag)
        # if there's only one usb drive attached, the code above will raise a TypeError
        except TypeError:
            self.debug(drive_dicts)
            name = drive_dicts['usb_drives']['usb_drive']['name']
            serial_number = drive_dicts['usb_drives']['usb_drive']['serial_number']
            capacity = drive_dicts['usb_drives']['usb_drive']['capacity']
            tag = 'name={0}_cap={1}_sn={2}_format='.format(name, capacity, serial_number).replace(' ', '-')
            tags.add(tag)

        # get format info from mount
        mount_info = utilities.run_on_device('mount', self.uut[Fields.internal_ip_address])
        for line in mount_info:
            self.debug(line)
            share_name = line.split()[2].split('/')[-1]
            if share_name in share_names:
                drive_format = line.split()[4]
                tags.add('USB-{0}'.format(drive_format))
                tags_copy = tags.copy()
                for tag in tags_copy:
                    if (share_name in tag or share_name.replace('_', '-') in tag) and 'name' in tag:
                        tags.remove(tag)
                        tags.add(tag + drive_format + '_')
        return tags

    @_clean_share_name
    def get_share(self, share_name=TEST_SHARE_NAME):
        """
        Returns the specified share

        XML Response Example:
        <shares>
            <share>
                <share_name>Public</share_name>
                <description/>
                <size>0</size>
                <remote_access>true</remote_access>
                <public_access>true</public_access>
                <media_serving>true</media_serving>
                <volume_id>WD-WMC4M1170610_2</volume_id>
                <dynamic_volume>false</dynamic_volume>
            </share>
        </shares>
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_get('shares/{0}'.format(share_name))
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def create_shares(self, share_name='share', description=TEST_SHARE_DESC, media_serving='any', public_access='true', volume_id=''):
        """
        Creates a new share with the given name, description,
        public access setting, and media serving
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_post('shares/{0}'.format(share_name), params=params)
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def update_share(self, share_name, new_share_name=None, description=TEST_SHARE_DESC,
                     public_access=True, media_serving='any'):
        """
        Updates the given share with the given information
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        new_share_name = params['new_share_name']
        del params['share_name']
        if share_name == new_share_name:
            del params['new_share_name']
        result = self._rest_put('shares/{0}'.format(share_name), params=params)
        return result

    def get_all_shares(self):
        """
        XML Response Example:
        <shares>
            <share>
                <share_name>Public</share_name>
                <description/>
                <size>0</size>
                <remote_access>true</remote_access>
                <public_access>true</public_access>
                <media_serving>true</media_serving>
                <volume_id>WD-WMC4M1170610_2</volume_id>
                <dynamic_volume>false</dynamic_volume>
            </share>
        </shares>
        """
        result = self._rest_get('shares')
        return result
    
    # This rest call is not working anymore an ITR has been submitted against this issue
    @utilities.decode_unicode_args
    @_clean_share_name
    def delete_share_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        """
        Deletes share access
        """
        self.logger.warning('delete_share_access() called, but is known to fail')
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_delete('share_access/{0}'.format(share_name), params=params)
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def get_share_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        """
        Returns all groups that have access to the share

        XML Response Example:
        <share_access_list>
            <share_name>test</share_name>
            <share_access>
                <username>est</username>
                <user_id>est</user_id>
                <access>RO</access>
            </share_access>
            <share_access>
                <username>@group0</username>
                <user_id>@group0</user_id>
                <access>RW</access>
            </share_access>
        </share_access_list>
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_get('share_access/{0}'.format(share_name), params=params)
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def create_share_access(self, share_name=TEST_SHARE_NAME, username='admin', access='RW'):
        """
        Creates the given share for the given user and given access level
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_post('share_access/{0}'.format(share_name), params=params)
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def update_share_access(self, share_name=TEST_SHARE_NAME, username='admin', access='RW'):
        """
        Adjusts the given share for the given user and given access level
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_put('share_access/{0}'.format(share_name), params=params)
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def update_share_network_access(self, share_name=TEST_SHARE_NAME, username='admin', public_access=True):
        """
        Change network share access to Public
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_put('shares/{0}'.format(share_name), params=params)
        return result

    @utilities.decode_unicode_args
    @_clean_share_name
    def get_share_network_access(self, share_name=TEST_SHARE_NAME, username='admin'):
        """
        Retrieve network share access
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_get('shares/{0}'.format(share_name), params=params)
        return result

    def get_system_state(self):
        """

        XML Response Example:
        | <system_state>
        |   <status>ready</status>
        |   <temperature>good</temperature>
        |   <smart>good</smart>
        |   <volume>good</volume>
        |   <free_space>good</free_space>
        | </system_state>
        """
        result = self._rest_get('system_state')
        return result

    def get_date_time_configuration(self):
        """
        Service: date_time_configuration
        Operation: GET

        XML Response Example:
        | <date_time_configuration>
        |     <datetime>{timestamp}</datetime>
        |     <ntpservice>{on/off}</ntpservice>
        |     <ntpsrv0>{Fixed address of time server}</ntpsrv0>
        |     <ntpsrv1>{Fixed address of time server}</ntpsrv1>
        |     <ntpsrv_user>{User enterable time server}</ntpsrv_user>
        |     <time_zone_name>{Name from timeZones}</time_zone_name>
        | </date_time_configuration>

        """
        result = self._rest_get('date_time_configuration')
        return result

    # noinspection PyShadowingNames
    def set_date_time_configuration(self,
                                    datetime=None,
                                    ntpservice=None,
                                    ntpsrv0=None,
                                    ntpsrv1=None,
                                    ntpsrv_user=None,
                                    time_zone_name=None):
        """
        Service: date_time_configuration
        Operation: PUT

        Key: Possible Values:
        | datetime:  {timestamp}
        | ntpservice:  boolean
        | ntpsrv0:  {Fixed address of time server0}
        | ntpsrv1:  {Fixed address of time server1}
        | ntpsrv_user:  {User selectable time server}
        | time_zone_name:  {Name from timeZones}


        """
        args = self._get_params(locals())
        device_settings = self.get_xml_children_dict(self.get_date_time_configuration().content)
        params = {arg: (value or device_settings[arg] or '') for (arg, value) in args.items()}

        result = self._rest_put('date_time_configuration', params=params)
        return result

    def enable_remote_access(self):
        """
        Enables remote access using a put operation
        """
        result = self._rest_put('device', params={'remote_access': True})
        return result

    def disable_remote_access(self):
        """
        Disables remote access using a put operation
        """
        result = self._rest_put('device', params={'remote_access': False})
        return result

    def get_device_info(self):
        """

        <device>
            <device_id></device_id>
            <device_type>8</device_type>
            <communication_status>disabled</communication_status>
            <remote_access>true</remote_access>
            <local_ip></local_ip>
            <default_ports_only>true</default_ports_only>
            <manual_port_forward>FALSE</manual_port_forward>
            <manual_external_http_port></manual_external_http_port>
            <manual_external_https_port></manual_external_https_port>
            <internal_port>80</internal_port>
            <internal_ssl_port>443</internal_ssl_port>
        </device>

        """
        result = self._rest_get('device')
        return result

    def set_cloud_access_connection_options(self, connectivity, http_port=80, https_port=443):
        """ Change the cloud access connection options (settings/general/cloud access -> configure)
                :param connectivity: The connectivity mode to use. Can be one of the following from global_libraries.constants:
                                                   CLOUD_ACCESS_AUTO, CLOUD_ACCESS_MANUAL, or CLOUD_ACCESS_WINXP
                 :param http_port: For CLOUD_ACCESS_MANUAL, the HTTP port to use
                :param https_port: For CLOUD_ACCESS_MANUAL, the HTTPSport to use
                """
        params = {}
        manual = 'manual_port_forward'
        default_ports_only = 'default_ports_only'
        external_http_port = 'manual_external_http_port'
        external_https_port = 'manual_external_https_port'

        # Verify that the port parameters are valid for the selected connectivity mode
        if http_port != 80 or https_port != 443:
            if connectivity != Modes.CLOUD_ACCESS_MANUAL:
                raise wd_exceptions.InvalidParameter('Connectivity must be constants.CLOUD_ACCESS_MANUAL ' +\
                                                     'if http_port or https_port is set.')

        # Ports can only be in the range 1 to 65535, so raise an exception if the value is outside this range
        if http_port < 1 or http_port > 65535:
            raise wd_exceptions.InvalidParameter('HTTP port value of {} is invalid'.format(http_port))

        if https_port < 1 or https_port > 65535:
            raise wd_exceptions.InvalidParameter('HTTPS port value of {} is invalid'.format(https_port))

        # Set the parameters for the REST call based off the connectivity mode
        if connectivity == Modes.CLOUD_ACCESS_AUTO:
            params[manual] = 'false'
            params[default_ports_only] = 'false'
        elif connectivity == Modes.CLOUD_ACCESS_MANUAL:
            params[manual] = 'true'
            params[external_http_port] = str(http_port)
            params[external_https_port] = str(https_port)
            params[default_ports_only] = 'false'
        elif connectivity == Modes.CLOUD_ACCESS_WINXP:
            params[manual] = 'false'
            params[external_http_port] = '80'
            params[external_https_port] = '443'
            params[default_ports_only] = 'true'
        else:
            raise wd_exceptions.InvalidParameter('Connectivity value of "{}" is invalid.'.format(connectivity))

        return self._rest_put('device', params=params)

    # TODO: Determine what format is supposed to do. It is currently unused.
    @utilities.decode_unicode_args
    def get_device_user(self, device_user_id=None, username=None, format=None):
        """

        Description:
            Retrieve one device user or a list to which user has access.
            An Admin User can get a list of all Device Users if a username is not provided in the parameter list

        Security:
        Only the user or admin user can get a device user account.
        HTTP Method: GET

        http://localhost/api/2.1/rest/device_user/{device_user_id}
        http://localhost/api/2.1/rest/device_user?username={username}

        Parameters:
            device_user_id - integer(optional)
            username - string(optional)
            format - string(optional)

        Parameter Details:
            If device_user_id is set, then return only that device user is returned
            If username is set, then all associated device users are returned
        Return values:
            device_users    - Array of device user accounts

        XML Response Example:
        |<device_users>
        |   <device_user>
        |      <device_user_id>62580</device_user_id>
        |      <device_user_auth_code>6b8ebf52e99c0f1437d4bd52c84a75dd</device_user_auth_code>
        |      <username>guest</username>
        |      <device_reg_date>1297211862</device_reg_date>
        |      <type></type>
        |      <name></name>
        |      <active></active>
        |      <email>guest@mywebmail.net</email>
        |      <dac>951914401933</dac>
        |      <dac_expiration>1297298315</dac_expiration>
        |      <type_name></type_name>
        |      <application></application>
        |   </device_user>
        |</device_users>

        """

        params = {}
        url = 'device_user'
        if device_user_id:
            params = {'device_user_id': device_user_id}
        elif username:
            # params = {'username': username}
            url += '?username={0}'.format(username)
        result = self._rest_get(url, params=params)
        return result

    @utilities.decode_unicode_args
    def create_device_user(self, email=None,
                           user_id=None,
                           sender=None,
                           send_email=None,
                           alias=None,
                           format=None):

        """
        Description:
            Creates a new device user.

        Security:
            Only the user or admin user can create a device user account.

        HTTP Method: POST
            ttp://localhost/api/2.1/rest/device_user

        Web User:
        Parameters:
            email - string(optional) (if a web user needs to be created this becomes a required field)
            user_id - (or) username string(optional)
            sender - string(optional)
            send_email - string(optional)
            alias - string(optional)
            format - string(optional)

                email - email address to which user got to be created
                username - to which the device user need to be associated
                sender - name of the sender
                send_email - {true/false} describes whether to send email or not
                alias - display name for this device on the wd2go Portal
                dac - set to 1 to return a DAC, defaults to 1.

        XML Response Example:
        <device_user>
            <status>success</status>
            <device_user_id>62580</device_user_id>
            <device_user_auth>6b8ebf52e99c0f1437d4bd52c84a75dd</device_user_auth>
        </device_user>
        """
        params = self._get_params(locals())
        result = self._rest_post('device_user', params=params)
        return result


    @utilities.decode_unicode_args
    def update_device_user(self, device_user_id,
                           type,
                           name,
                           email,
                           device_user_auth_code=None,
                           type_name=None,
                           application=None,
                           resend_email=None,
                           is_active=None,
                           format=None):
        """
        Description:
            Update an existing device user.

        Security:
            Only the user or admin user can update a device user account.

        HTTP Method: PUT
            http://localhost/api/2.1/rest/device_user/{device_user_id}
            http://localhost/api/2.1/rest/device_user/{device_user_id}?resend_email=true

        Parameters:
            device_user_id    Integer - required
            type    Integer - required
            name    String - required only for email device user
            email    String - required only for email device user
            device_user_auth_code    String - optional
            type_name    String - optional
            application    String - optional
            resend_email    Boolean - optional (if this parameter is passed all other parameters are ignored except sender)
            sender    String - optional
            is_active    Boolean - optional
            format    String - optional
            On non-remote access systems parameters: email, sender, resend_email are invalid. Valid parameters are:
                device_user_id
                type
                name
                device_user_auth_code
                type_name
                application
                is_active
                format
        Parameter Details:
            The device_user_id and device_user_auth_code are required.

        Type Parameter Details:
            Type    Enumerated Value
                1    iPhone
                2    iPad Touch
                3    iPad
                4    Android Phone
                5    Android Tablet
                6    Sync Application

        Return values:
            status    String - success

        XML Response Example:
          <users>
              <status>success</status>
          </users>
        """
        params = self._get_params(locals())
        result = self._rest_put('device_user/{0}'.format(device_user_id), params=params)
        return result

    @utilities.decode_unicode_args
    def delete_device_user(self, device_user_id, device_user_auth_code):
        """
        Description:
            Delete an existing device user.

        Security:
        Only the user or admin user can delete a device user account.
        HTTP Method: DELETE
        http://localhost/api/2.1/rest/device_user/{deviceUserId}?device_user_auth_code={auth_code}

        Parameters:
            device_user_id    Integer - required
            device_user_auth_code    String - required
            format    String - optional

        Parameter Details:
            The device_user_id and device_user_auth_code are required.

        Return values:
            status    String - success

        XML Response Example:
             <users>
                 <status>success</status>
             </users>
        """
        result = self._rest_delete(
            'device_user/{0}?device_user_auth_code={1}'.format(device_user_id, device_user_auth_code))
        return result


    @utilities.decode_unicode_args
    def register_remote_device(self, name='myNAS'):
        """
        Used for register a remote device

        XML Response Example:
        | <device>
        |   <status>success</status>
        | </device>

        Parameters:
        name - string (required)
        email - string (optional)
        format - string (optional)

        """
        result = self._rest_post('device?name={0}'.format(name))
        return result

    @utilities.decode_unicode_args
    def set_remote_device_attributes(self, name=None,
                                     remote_access=None,
                                     default_ports_only=None,
                                     manual_port_forward=None,
                                     manual_external_http_port=None,
                                     manual_external_https_port=None,
                                     format=None):
        """
        Used for updating the device attributes

        XML Response Example:
        | <device>
        |   <status>success</status>
        | </device>

        Parameters:
        name - string (optional)
        remote_access - boolean (optional)
        default_ports_only - boolean (optional)
        manual_port_forward - boolean - optional
        manual_external_http_port - integer (optional)
        manual_external_https_port - integer (optional)
        format - string (optional)

        Note: At least one of the seven parameters should be provided.
        If manual_port_forward is false, both manual_external_http_port and
        manual_external_https_port will be unset. If manual_port_forward is true,
        must provide both manual_external_http_port and manual_external_https_port.
        """

        params = self._get_params(locals())
        result = self._rest_put('device', params=params)
        return result

    def get_media_server_database_status(self):
        """

        XML Response Example:
        | <media_server_database>
        |   <version>2.2.2.6492C</version>
        |   <time_db_update>1338486474</time_db_update>
        |   <music_tracks>0</music_tracks>
        |   <pictures>0</pictures>
        |   <videos>0</videos>
        |   <scan_in_progress></scan_in_progress>
        | </media_server_database>
        """
        result = self._rest_get('media_server_database')
        return result

    def configure_media_server_database(self, database='rebuild'):
        """

        database: rebuild/reset_defaults/rescan

        XML Response Example:
        | <media_server_database>
        |   <status>success</status>
        | </media_server_database>
        """
        params = self._get_params(locals())
        result = self._rest_put('media_server_database', params=params)
        return result

    def get_media_server_configuration(self):
        """
        XML Response Example:
        | <media_server_configuration>
        |     <enable_media_server>true</enable_media_server>
        | </media_server_configuration>

        """
        result = self._rest_get('media_server_configuration')
        return result

    def set_media_server_configuration(self, enable_media_server):
        """
        XML Response Example:
        | <media_server_configuration>
        |     <status>success</status>
        | </media_server_configuration>

        """
        params = self._get_params(locals())
        result = self._rest_put('media_server_configuration', params=params)
        return result

    # # Get media crawler status
    def get_media_crawler_status(self):
        """
        XML Response Example:
        <mediacrawler_status>
        |    <volumes>
        |        <volume>
        |            <volume_id>05348c95:efb3f44f:03916b6c:573be1d0</volume_id>
        |            <volume_state>idle</volume_state>
        |            <categories>
        |                <category>
        |                   <category_type>videos</category_type>
        |                   <mdate>1402679637</mdate>
        |                   <extracted_count>8</extracted_count>
        |                   <transcoded_count>8</transcoded_count>
        |                   <total>7</total>
        |                </category>
        |                <category>
        |                <category_type>music</category_type>

        """
        result = self._rest_get('mediacrawler_status')
        return result

    # # Update media crawler for a share
    def set_mediacrawler(self, share_name=TEST_SHARE_NAME):
        """
        XML Response Example:
        <mediacrawler>
        |    <status>success</status>
        </mediacrawler>

        """
        result = self._rest_put('mediacrawler/{0}'.format(share_name))
        return result


    # # Get meta data of a share
    def get_meta_data(self, share_name=TEST_SHARE_NAME):
        """
        XML Response Example:
        <metadb_info>
        <generated_time>1402699451</generated_time>
        <last_purge_time>1400084753</last_purge_time>
        <last_updated_db_time>1402679637</last_updated_db_time>
        <file>
        |    <path>/Public/FTPTest</path>
        |    <name>FourMethodsOfFlushRiveting.mp4</name>
        |    <size>39266531</size>
        |    <media_type>videos</media_type>
        |    <modified>1397157362</modified>
        |    <deleted>false</deleted>
        |    <status>2</status>
        |    <title>FourMethodsOfFlushRiveting.mp4</title>
        |    <date>-1</date>
        |    <duration>572</duration>
        |    <stars/>
        |    <director/>
        |    <writer/>
        |    <genre/>
        |    <cover_art>true</cover_art>
        |    </file>

        """
        result = self._rest_get('metadb_info/{0}'.format(share_name))
        return result

    # # Get meta data summary of a share
    def get_meta_data_summary(self, share_name=TEST_SHARE_NAME):
        """
        XML Response Example:
        <metadb_summary>
        |    <file_count>10</file_count>
        |    <size>96189380</size>
        |    <path>/Public</path>
        </metadb_summary>
        """
        result = self._rest_get('metadb_summary/{0}'.format(share_name))
        return result

    # # Get miocrawler status
    def get_miocrawler_status(self):
        """
        XML Response Example:
        <miocrawler_status>
        |    <mdate>1402697708</mdate>
        |    <desc/><etype>0</etype>
        |    <mc_version>2.0</mc_version>
        </miocrawler_status>
        """
        result = self._rest_get('miocrawler_status')
        return result

    # # Get miodb
    def get_mio_data(self, compress=True):
        """

        """
        params = self._get_params(locals())
        result = self._rest_get('miodb', params=params)
        return result


    # # Get RAID drive status
    # noinspection PyPep8Naming
    def get_RAID_drives_status(self):
        """
        XML Response Example:
        <raid_drives_status>
        |    <status>drive_raid_already_formatted</status>
        |    <status_desc>RAID is already formatted</status_desc>
        |    <busy_drive_location></busy_drive_location>
        |    <drives>
        |        <partition_uuid>05348c95:efb3f44f:03916b6c:573be1d0</partition_uuid>
        |        <drive>
        |            <location>1</location>
        |            <model>WDC WD30EFRX-68AX9N0</model>
        |            <serial_number>WD-WCC1T1604614</serial_number>
        |            <drive_size>3000592982016</drive_size>
        |            <valid_drive_list>allowed</valid_drive_list>
        |            <smart_status/><raid_mode>5</raid_mode>
        |            <removable>1</removable>
        |        </drive>
        """
        result = self._rest_get('raid_drives_status')
        return result

    # # Get user RAID status
    # noinspection PyPep8Naming
    def get_user_RAID_status(self):
        """
        XML Response Example:
        <user_raid_status>
        |    <raid_status>GOOD</raid_status>
        </user_raid_status>
        """
        result = self._rest_get('user_raid_status')
        return result

    # # Get RAID configuration status
    def get_raid_configuration_status(self):
        """
        XML Response Example:
        """
        result = self._rest_get('raid_configuration')
        return result

    # # Post RAID configuration status
    def raid_configuration_status(self):
        """
        XML Response Example:
        <user_raid_status>
        |    <raid_status>GOOD</raid_status>
        </user_raid_status>
        """
        result = self._rest_post('raid_configuration')
        return result


    @utilities.decode_unicode_args
    def create_directory(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        """
        Creates a new directory with the given name, default = test_dir, in the
        given share
        """
        result = self._rest_post('dir/{0}/{1}'.format(share_name, dir_path))
        return result

    # # Get directories of a share given share name
    @utilities.decode_unicode_args
    def get_directory(self, share_name=TEST_SHARE_NAME):
        """
        Gets all the directories of a given share

        XML Response Example:
        <dir>
            <entry>
                <is_dir>true</is_dir>
                <path>/Public</path>
                <name>Shared Pictures</name>
                <mtime>1403204736</mtime>
            </entry>
            <entry>
                <is_dir>true</is_dir>
                <path>/Public</path>
                <name>Shared Music</name>
                <mtime>1403204736</mtime>
            </entry>
            <entry>
                <is_dir>true</is_dir>
                <path>/Public</path>
                <name>Shared Videos</name>
                <mtime>1403204736</mtime>
            </entry>
        </dir>
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_get('dir/{0}'.format(share_name))
        return result

    # # Get directories contents of a share given a share name
    @utilities.decode_unicode_args
    def get_directory_contents(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        """
        Gets the archived contents of a given directory of a given share
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_get('dir_contents/{0}/{1}'.format(share_name, dir_path))
        return result

    @utilities.decode_unicode_args
    def get_dir_contents(self):
        url = 'http://' + self.uut[
            Fields.internal_ip_address] + '/api/2.1/rest/dir_contents/Public/Temp?owner=admin&pw='

        file_name = url.split('/')[-1]
        u = urllib2.urlopen(url)
        f = open(file_name, 'wb')
        meta = u.info()
        file_size = int(meta.getheaders("Content-Length")[0])
        self.logger.info("Downloading: %s Bytes: %s" % (file_name, file_size))

        file_size_dl = 0
        block_sz = 8192
        while True:
            read_buffer = u.read(block_sz)
            if not read_buffer:
                break

            file_size_dl += len(read_buffer)
            f.write(read_buffer)
            status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
            status += chr(8) * (len(status) + 1)
            self.logger.debug(status)

        f.close()

        response = urllib2.urlopen(url)
        self.logger.debug('RESPONSE: ' + str(response))
        self.logger.debug('URL     : ' + str(response.geturl()))

        headers = response.info()
        self.logger.debug('DATE    : ' + str(headers['date']))
        self.logger.debug('HEADERS : ')
        self.logger.debug('--------- ')
        self.logger.debug(str(headers))

        data = response.read()
        self.logger.debug('LENGTH  : ' + str(len(data)))
        self.logger.debug('DATA    : ')
        self.logger.debug('--------- ')
        self.logger.debug(str(data))


    # # Delete a directory within a share given share and directory name
    @utilities.decode_unicode_args
    def delete_directory(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        """
        Delete a directory within a share given share and directory name
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_delete('dir/{0}/{1}'.format(share_name, dir_path))
        return result

    # # Copy a directory from one share to another
    # Given source and destination shares
    # Given source and destination directories
    @utilities.decode_unicode_args
    def copy_directory(self, source_share_name=TEST_SHARE_NAME, source_dir_path='test_dir',
                       dest_share_name=TEST_SHARE_NAME, dest_dir_path='test_dir2'):
        """
        Copy a directory from one share to another given source and
        destination shares and directories
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_put(
            'dir/{0}/{1}?dest_path=/{2}/{3}&copy=true&recursive=true'.format(source_share_name, source_dir_path,
                                                                             dest_share_name, dest_dir_path))
        return result

    # # Retrieve directory info inside a share, given directory and share name
    @utilities.decode_unicode_args
    def get_directory_info(self, share_name=TEST_SHARE_NAME, dir_path='test_dir'):
        """
        Retrieve directory info inside a share, given directory and share name
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_get('dir_info/{0}/{1}'.format(share_name, dir_path))
        return result

    # # Retrieve file info inside a share, given share and file name
    @utilities.decode_unicode_args
    def get_file(self, share_name=TEST_SHARE_NAME, file_name='test_file'):
        """
        Retrieve file info inside a share, given share and file name
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_get('file/{0}/{1}'.format(share_name, file_name))
        return result

    # # Retrieve file info inside a share, given directory and file name
    @utilities.decode_unicode_args
    def delete_file(self, share_name=TEST_SHARE_NAME, file_name='test_file'):
        """
        Deletes a given file within a given share
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_delete('file/{0}/{1}'.format(share_name, file_name))
        return result

    # # Copy a file from one share to another
    # Given source and destination shares
    # Given source and destination file
    @utilities.decode_unicode_args
    def copy_file(self, source_share_name=TEST_SHARE_NAME, source_file='source_file', dest_share_name=TEST_SHARE_NAME,
                  dest_file='dest_file'):
        """
        Copies a given file from a given share to another
        given file in a given share
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_put(
            'file/{0}/{1}?dest_path=/{2}/{3}&copy=true&overwrite=true'.format(source_share_name, source_file,
                                                                              dest_share_name, dest_file))
        return result

    @utilities.decode_unicode_args
    def get_file_contents(self, share_name=TEST_SHARE_NAME, file_name='test_file'):
        """

        share_name - required
        file_name - required
        http_range  - optional
        tn_type - optional [tn96s1, i160s1, i1024s1]
        is_download - optional
        format - optional (default is xml)
        is_blocking - optional (default to true)

        """

        result = self._rest_get('file_contents/{0}/{1}'.format(share_name, file_name))
        return result

    def download_file(self, share_name=TEST_SHARE_NAME, file_name='test_file', saved_file='save_file'):
        with open(saved_file, 'wb') as handle:
            file_response = self.get_file_contents(share_name, file_name)

            if not file_response.ok:
                self.debug('*DEBUG* failed to download file')
            for block in file_response.iter_content(1024):
                if not block:
                    break
                handle.write(block)

    def upload_file_contents(self, share_name=TEST_SHARE_NAME, dir_name='test_dir', file_name='test_file', format='xml',
                             create_path=False):
        """
        This POST request is used to upload a specified file.
        Note: File uploads are limited to 1GB. Any larger files must use chunked PUT data.

        share_name - required
        dir_name - required
        file_name - required
        format - optional (default is xml)
        create_path - optional (default is false)

        """

        result = self._rest_post(
            'file_contents/{0}/{1}/{2}?format={3}&create_path={4}'.format(share_name, dir_name, file_name, format,
                                                                          create_path))
        return result

    def set_file_contents(self, share_name=TEST_SHARE_NAME, dir_name='test_dir', file_name='test_file', format='xml',
                          create_path=False):
        """
        This PUT request is used to modify or replace an existing file.
        Note: File uploads are limited to 1GB. Any larger files must use chunked PUT data.

        share_name - required
        dir_name - required
        file_name - required
        http_range - optional
        mtime - optional
        format - optional (default is xml)
        create_path - optional (default is false)

        """

        result = self._rest_put(
            'file_contents/{0}/{1}/{2}?format={3}&create_path={4}'.format(share_name, dir_name, file_name, format,
                                                                          create_path))
        return result

    # # Retrieve file info inside a directory of a share, given share, directory and file name
    @utilities.decode_unicode_args
    def get_file_info(self, share_name=TEST_SHARE_NAME, dir_path='test_dir', file_name='test_file'):
        """
        Sample response:
        <files>
            <file>
                <path/>
                <name>AFP-74BD.mp4</name>
                <size>52347989</size>
                <modified>2011-07-20 11:40:49</modified>
                <last_updated>2014-06-13 08:48:48</last_updated>
                <deleted>false</deleted>
            </file>
        </files>
        """
        # params = self._get_params(locals())
        # share_name = params['share_name']
        # del params['share_name']
        result = self._rest_get('file_info/{0}/{1}/{2}'.format(share_name, dir_path, file_name))

        return result

    # # Get language configuration
    @utilities.decode_unicode_args
    def get_language_configuration(self):
        """
        XML Response Example:
        <language_configuration>
            <language>en_US</language>
        </language_configuration>
        """
        result = self._rest_get('language_configuration')
        return result

    # # Set language configuration (system restore before this can be executed)
    @utilities.decode_unicode_args
    def set_language_configuration(self, language='en_US'):
        """
        XML Response Example:
        <language_configuration>
            <language>en_US</language>
        </language_configuration>
        """
        result = self._rest_post('language_configuration?language={0}'.format(language))
        return result

    # # Update language configuration
    @utilities.decode_unicode_args
    def update_language_configuration(self, language='en_US'):
        """
        XML Response Example:
        <language_configuration>
            <status>Success</status>
        </language_configuration>
        """
        result = self._rest_put('language_configuration?language={0}'.format(language))
        return result

    def get_eula_status(self):
        """
        XML Response Example:
        | <eula_acceptance>
        |    <accepted>yes</accepted>
        |</eula_acceptance>
        """
        result = self._rest_get('eula_acceptance')
        return result

    def accept_eula(self):
        """
        XML Response Example:
        | <eula_acceptance>
        |    <accepted>success</accepted>
        |</eula_acceptance>
        """
        params = {'accepted': 'yes'}
        result = self._rest_post('eula_acceptance', params=params)
        return result

    def get_device_registration(self):

        """
        XML Response Example:

        |<device_registration>
        |  <status>Success</status>
        |</device_registration>
        """
        result = self._rest_get('device_registration')
        return result

    @utilities.decode_unicode_args
    def set_device_registration(self, registered):
        """
        XML Response Example:
        | <device_registration>
        |     <status>success</status>
        | </device_registration>

        """
        params = self._get_params(locals())
        result = self._rest_put('device_registration', params=params)
        return result

    @utilities.decode_unicode_args
    def device_registration(self, country='US', lang='ENG', first='test', last='test', email='test@test.com',
                            option='yes'):
        """
        XML Response Example:
        | <device_registration>
        |   <status>success</status>
        | </device_registration>
        """
        params = self._get_params(locals())
        result = self._rest_post('device_registration', params=params)
        return result

    def get_device_description(self):
        """
        XML Response Example:
        |  <?xml version="1.0" encoding="utf-8"?>
        |  <device_description>
        |  <machine_name>wdvenue</machine_name>
        |  <machine_desc>My Book Live Network Storage</machine_desc>
        |  </device_description>
        """
        result = self._rest_get('device_description')
        return result

    @utilities.decode_unicode_args
    def set_device_description(self, machine_name, machine_desc):
        params = self._get_params(locals())
        result = self._rest_put('device_description', params=params)
        return result

    # noinspection PyPep8Naming
    def get_FTP_server_configuration(self):
        """
        XML Response Example:
        | <network_services_configuration>
        |     <enable_ftp>true</enable_ftp>
        | </network_services_configuration>

        """
        result = self._rest_get('network_services_configuration')
        return result

    # noinspection PyPep8Naming
    @utilities.decode_unicode_args
    def set_FTP_server_configuration(self, enable_ftp):
        """
        XML Response Example:
        | <network_services_configuration>
        |     <enable_ftp>true</enable_ftp>
        | </network_services_configuration>

        """
        params = self._get_params(locals())
        result = self._rest_put('network_services_configuration', params=params)
        return result

    # TODO: Remove **kwargs since it is not used, but need to check all other APIs to confirm it doesn't break anything
    def random_rest_calls(self, *args, **kwargs):
        """
        Runs random rest keyword
        """
        args = list(args)
        # Disabled calls:
        # self.get_itunes_server_configuration,
        # self.get_fw_info,
        rest_calls = [self.get_firmware_update_configuration, self.get_firmware_version,
                      self.get_network_configuration, self.get_date_time_configuration,
                      self.get_device_info, self.get_media_server_configuration,
                      self.get_FTP_server_configuration,
                      self.system_state, self.is_ready, self.create_user(*args),
                      self.get_all_users, self.get_system_information,
                      self.set_ssh, self.get_ssh_configuration,
                      self.get_alerts, self.get_alert_configuration,
                      self.test_alert_configuration, self.delete_all_users,
                      self.get_disk_size, self.set_media_server_configuration]

        rest_call = random.choice(rest_calls)
        result = rest_call()
        return result

    def delete_all_shares(self, exclude=None):
        """
        Delete all shares.
        """
        if not exclude:
            exclude = ['Public', 'TimeMachineBackup', 'SmartWare']
        shares = self.get_all_shares()
        if not shares:
            raise RestError('Failed to get list of shares.')
        content = xml.etree.ElementTree.fromstring(shares.content)
        shares_list = {share.text for share in content.iter('share_name')}
        for share in shares_list:
            if share in exclude or self.is_usb_share(share):
                continue
            self.debug('Deleting share: {0}'.format(share))
            self.delete_share(share)

    @_clean_share_name
    def is_usb_share(self, share_name=TEST_SHARE_NAME):
        """
        Determines if the given share is a USB share
        """
        return 'usb_handle' in self.get_share(share_name).content


    @_clean_share_name
    def delete_share(self, share_name=TEST_SHARE_NAME):
        """
        Deletes the share with the given name
        """
        params = self._get_params(locals())
        share_name = params['share_name']
        del params['share_name']
        result = self._rest_delete('shares/{0}'.format(share_name))
        return result

    def reset_admin_password(self):
        """
        Resets admin password to default value
        """
        result = self.update_user(user='admin', new_password='', fullname='administrator', is_admin=True)
        return result

    def set_authentication(self,
                           username,
                           password):
        """
        Changes the username and password used to login. Useful if a test
        changes the authentication for the UUT.
        """
        self.debug('*DEBUG* REST Client authentication changed:')
        self.debug('*DEBUG* Old: username={0}, password={1}'.format(self.uut[Fields.rest_username],
                                                                    self.uut[Fields.rest_password]))
        self.debug('*DEBUG* New: username={0}, password={1}'.format(username, password))
        self.uut[Fields.rest_username] = username
        self.uut[Fields.rest_password] = password

    def get_raid_mode(self, ):
        """
        Gets raid mode - 0-10 for RAID mode 0-1, 11 for JBOD, 12 for spanning.
        """
        result = 'Unknown'
        xml_content = self.get_RAID_drives_status()
        raid_mode = self.get_xml_tag(xml_content, 'raid_mode')

        raid = ['0', '1', '2', '3', '4', '5', '6', '8', '9', '10']
        if raid_mode in raid:
            result = 'RAID{0}'.format(raid_mode)

        if raid_mode == '11':
            result = 'JBOD'
        elif raid_mode == '12':
            result = 'spanning'
        return result

    def firmware_update_nexus(self, device_ip, current_firmware, product, group):
        """
        update UUT firmware via REST call, get firmware from Nexus
        :param device_ip: UUT IP
        :param current_firmware: firmware that will be updated onto UUT
        :param product: product name, e.g: Aurora, Lightning
        :param group: e.g: My_Cloud_LT4A
        :return:
        """
        self.debug('current_firmware: {0}'.format(current_firmware))
        self.debug('device_ip: {0}'.format(device_ip))
        self.debug('product: {0}'.format(product))
        self.debug('group: {0}'.format(group))

        a, b, c = current_firmware.split(".")
        firmware_version = a + '_' + b
        self.debug('firmware_version: {0}'.format(firmware_version))

        product = product.replace(' ', '')
        self.debug('remove space for KC and Grand Teton: {0}'.format(product))

        firmware_url = 'http://repo.wdc.com/service/local/repositories/projects/content/' + product + '-' + \
                       firmware_version + '/' + group + '/' + current_firmware + '/' + group + '_' \
                       + current_firmware + '.bin'
        self.debug('firmware_url: {0}'.format(firmware_url))

        self.debug('Updating firmware {0}'.format(current_firmware))
        self.firmware_update(firmware_url)

        self.debug('5 minutes waiting for firmware update...')
        time.sleep(300)
        self.wait_for_factory_restore_to_finish()

        self.debug('Verify correct firmware version is installed')
        firmware_number = self.get_firmware_version_number()
        if firmware_number == current_firmware:
            self.debug('Correct firmware: {0}'.format(firmware_number))
        else:
            self.debug('Incorrect firmware: {0}'.format(firmware_number))


    # ************************************************************************************************************#
    # The following methods have been isolated as they are not presently used within the API, but they may be #
    # in the future if support becomes available. If there is not plan for support they should be deleted.    #
    # ************************************************************************************************************#
    '''

    def should_update(self):
        """
        Returns True if a firmware update should be run during test setup.
        """
        if not 'PACKAGE_URL' in os.environ:
            return False
        if os.environ.get('TYPE') == 'production':
            return True
        current_version = self.get_xml_tag(self.get_firmware_version().content, 'firmware').replace('.', '').strip()
        self.debug('Current Version: {0}'.format(current_version))
        new_version = os.environ['PACKAGE_URL'].split('/')[-2].strip()
        self.debug('New Version: {0}'.format(new_version))
        return current_version != new_version


    def get_system_log(self):
        """
        XML Response Example:
        | <system_log>
        |   <path_to_log>/CacheVolume/systemLog_WCAZA0470171_1338507122.zip</path_to_log>
        | </system_log>
        """
        result = self._rest_get('system_log', timeout=60)
        return result

    def generate_system_log_and_send_to_support(self):
        """
        XML Response Example:
        | <system_log>
        |   <transfer_success>succeeded</transfer_success>
        |   <logfilename>systemLog_WCAZA0470171_1338507712.zip</logfilename>
        | </system_log>
        """
        result = self._rest_post('system_log', timeout=300)
                if  200 <= result.status_code < 300:
        return result
        '''

    '''
    def get_hdd_standby_time(self):
    return self._rest_get('hdd_standby_time')

    def enable_hdd_standby(self, enable_hdd_standby='enable', hdd_standby_time_minutes=None):
        args = self._get_params(locals())
        device_settings = self.get_xml_children_dict(self.get_hdd_standby_time().content)
        params = {arg: (value or device_settings[arg] or '') for (arg, value) in args.items()}
        return self._rest_put('hdd_standby_time', params=params)

    def get_itunes_server_configuration(self):
        """

        XML Response Example:
        | <itunes_configuration>
        |     <enable_itunes_server>true</enable_itunes_server>
        | </itunes_configuration>

        """
        return self._rest_get('itunes_configuration')

    def set_itunes_server_configuration(self, enable_itunes_server):
        """
        XML Response Example:
        | <itunes_configuration>
        |     <enable_itunes_server>true</enable_itunes_server>
        | </itunes_configuration>

        """
        params = self._get_params(locals())
        return self._rest_put('itunes_configuration', params=params)

    def downloadChunks(url=None):
        """
        Helper to download large files
        the only arg is a url
        this file will go to a temp directory
        the file will also be downloaded
        in chunks and print out how much remains
        """

        if url is None:
            url = 'http://' + self.uut[Fields.internal_ip_address] + '/api/2.1/rest/dir_contents/Public/Temp?owner=admin&pw='

        baseFile = '' # os.path.basename(url)

        # move the file to a more unique path
        os.umask(0002)
        temp_path = "/tmp/"
        try:
            file = os.path.join(temp_path,baseFile)

            req = urllib2.urlopen(url)
            total_size = int(req.info().getheader('Content-Length').strip())
            downloaded = 0
            CHUNK = 256 * 10240
            with open(file, 'wb') as fp:
                while True:
                    chunk = req.read(CHUNK)
                    downloaded += len(chunk)
                    self.logger.debug(math.floor( (downloaded / total_size) * 100 ))
                    if not chunk: break
                    fp.write(chunk)
        except urllib2.HTTPError, e:
            self.logger.warning("HTTP Error: " + str(e.code) + ' - ' + str(url))
            return False
        except urllib2.URLError, e:
            self.logger.warning("URL Error: " + str(e.reason) + ' - ' + str(url))
            return False

        return file

    def get_time_machine_configuration(self):
        """
        XML Response Example:
        | <time_machine>
        |     <backup_enabled>true</backup_enabled>
        |     <backup_share>Cruzer</backup_share>
        |     <backup_size_limit>0</backup_size_limit>
        | </time_machine>


        """
        return self._rest_get('time_machine')

    @utilities.decode_unicode_args
    def set_time_machine_configuration(self,
                                       backup_enabled=None,
                                       backup_share=None,
                                       backup_size_limit=None):
        """
        XML Response Example:
        | <time_machine>
        |     <status>success</status>
        | </time_machine>

        """
        params = self._get_params(locals())
        return self._rest_put('time_machine', params=params)

    def disable_hdd_standby(self, enable_hdd_standby='disable', hdd_standby_time_minutes=None):
        return self.enable_hdd_standby(enable_hdd_standby, hdd_standby_time_minutes)
    '''
