'''
Created on Jun 2, 2014

@author: vasquez_c
'''
#from src.RESTClient import *
import time
import os
import sys

#import logger from parent directory
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
from src import RESTClient


if __name__ == '__main__':
    
    # instantiate object
    rest = RESTClient.RESTClient('192.168.1.120')
    
    # retrieve SSH configuration
    #rest.ssh_configuration()
    
    # enable SSH
    rest.set_ssh()
    
    # Create a new user
    rest.create_user('peter' , 'password', 'Peter Root', True)
    
    # Get new user with id
    content = rest.get_user('peter')
    
    #
    if (rest.get_xml_tag(content, 'user_id', True) == 'peter'):
        print '*************************************'
        print '*            PASS                   *'
        print '*************************************'

    #create a share
    rest.create_share('testshare', 'a test share')
    
    
    # delete user
    #rest.delete_user('peter')
    
    # delete share
    #rest.delete_share('testshare')