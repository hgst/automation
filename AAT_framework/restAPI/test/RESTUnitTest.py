'''
Created on Jun 3, 2014
@author: Jason Tran

This module contains the class and methods to run REST API test cases.

Since test cases are executed alphabetically, add prefixes (1,2,3,4) as follow:
test_1_ POST API call
test_2_ PUT API call
test_3_ GET API call
test_4_ DELETE API call
'''

import os
import sys
import unittest
import logging



'''
## To run unit test from commamnd line
#import RESTClient from sibling directory
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir0 = os.path.dirname(currentdir)
sys.path.insert(0,parentdir0)
from src import RESTClient

#import logger from grandparent directory
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir2 = os.path.dirname(parentdir)
sys.path.insert(0,parentdir2)
from UnitTestFramework import mylogger
'''

from UnitTestFramework import mylogger
from restAPI.src.restclient import RestClient

from global_libraries.testdevice import Fields
from testCaseAPI.src.testclient import TestClient



l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('RESTapiUnitTest')

# # Variables to execute test cases on local test unit
#  Need to update to your unit's values
hostname = '192.168.1.125'
sharename = 'privateshare'
sharedesc = 'a test share'
new_sharename = 'updatedshare'
username = 'admin'
shareaccess = 'RW'
shareaccess2 = 'RO'
dirname = 'tmp'
dest_share = 'Public'
dest_dir = 'tmp2'
source_share = 'ftpshare'
source_file = 'AFP-74BD.mp4'
dest_file = 'Video4.mp4'
test_dir = 'FTPTest'
test_file = 'Kobra3.jpg'
language = 'en_US'
language2 = 'ru_RU'
alert_id = '1'  # how to get id? using 1 as example from Alpha's doc
ack = '1'
handler = 'WXB1A20D5871'
auto_install = False
auto_install_day = '0'
auto_install_hour = '0'
auto_install2 = True
auto_install_day2 = '7'
auto_install_hour2 = '12'

trueBoolean = True

firmware_uri = 'http://repo.wdc.com/service/local/repositories/projects/content/Lightning-1_03/My_Cloud_LT4A/1.03.41/My_Cloud_LT4A-1.03.41.bin'
firmware_path = '/shares/Public/My_Cloud_LT4A_1.03.41.bin'

drive_password = ''
password_new = 'newpassword'

username = 'jasont'
fullname = 'Jason Tran'

device_name = 'MyEX4'
device_desc = 'Personal clould'

device_user_id = '7631406'
type = ''
name = ''
email = ''
device_user_auth_code = '01604c25f21b8b07e9551c736dbfbb62'

NASname = 'WDMyCloudEX4'
remote_access = True
default_ports_only = True
manual_port_forward = False
manual_external_http_port = None
manual_external_https_port = None
format = None

video_dl = 'myfile.mp4'

test = TestClient(ip_address=hostname)
test.uut[Fields.web_username] = username
test.uut[Fields.web_password] = drive_password

class RESTUnitTest(unittest.TestCase):

    # # Method is called at the beginning of test to instatiate RESTClient then do local login
    #  IP must be provided, using defaul username 'admin' and blank password
    def setUp(self):
        logger1.info('Start executing %s', self._testMethodName)

        # instantiate RestApiLibrary class
        try:
            self.myRest = RestClient(test.uut)
        except Exception, ex:
            logger1.info('Failed to instantiate\n' + str(ex))
            raise Exception, 'Failed to instantiate\n' + str(ex)

        # login as admin befor executing other REST call
        try:
            self.myRest.local_login()
        except Exception, ex:
            logger1.info('Failed to login\n' + str(ex))

    def tearDown(self):
        try:
            self.myRest.logout()
        except Exception, ex:
            raise Exception, 'Failed to logout\n' + str(ex)
        logger1.info('Finished executing %s', self._testMethodName)


    # # Note: Max retries error when executing this method, unit still reboot
    #  Run this method at the end of the suite
    def test_5_reboot(self):
        try:
            self.myRest.reboot()
        except Exception, ex:
            logger1.info('Failed to reboot unit\n' + str(ex))
            raise Exception, 'Failed to reboot unit\n' + str(ex)

    # # Shut down test unit
    def test_5_shutdown(self):
        try:
            self.myRest.shutdown()
        except Exception, ex:
            logger1.info('Failed to reboot unit\n' + str(ex))
            raise Exception, 'Failed to reboot unit\n' + str(ex)

    # # Retrieve device id
    #  Throw exception if failed
    def test_3_port_test(self):
        try:
            self.myRest.port_test()
        except Exception, ex:
            logger1.info('Failed to retrieve device id\n' + str(ex))
            raise Exception, "Failed to retrieve device id\n" + str(ex)

    # # Get status of the processes
    #  Throw exception if failed
    def test_3_status(self):
        try:
            self.myRest.status()
        except Exception, ex:
            logger1.info('Failed to retrieve process statuses\n' + str(ex))
            raise Exception, "Failed to retrieve process statuses\n" + str(ex)

    # # Get system state
    #  Throw exception if failed
    def test_3_system_state(self):
        try:
            self.myRest.system_state()
        except Exception, ex:
            logger1.info('Failed to retrieve system state\n' + str(ex))
            raise Exception, "Failed to retrieve system state\n" + str(ex)

    # # Retrieve 'ready' status from system state
    #  True should be returned, assertionError otherwise
    def test_3_is_ready(self):
        try:
            mystatus = self.myRest.is_ready()
            logger1.info('Ready status value: %s', str(mystatus))
            expected = True
            self.assertEqual(mystatus, expected, 'Unit is not in ready state')
        except AssertionError:
            logger1.info('Unit is not in ready state')

    # # Perform smart test
    #  Throw exception if failed
    # def test_3_start_smart_test(self):
    #    try:
    #        self.myRest.start_smart_test()
    #    except Exception,ex:
    #        logger1.info('Failed to run smart test\n' + str(ex))
    #        raise Exception, "Failed to run smart test\n" + str(ex)

    # # Retrieve user(s) in test unit
    # Throw exception if failed
    def test_3_get_all_users(self):
        try:
            self.myRest.get_all_users()
        except Exception, ex:
            logger1.info('Failed to retrieve user(s)\n' + str(ex))
            raise Exception, "Failed to retrieve user(s)\n" + str(ex)

    # # Retrieve user in test unit
    # Throw exception if failed
    def test_3_get_user(self):
        try:
            self.myRest.get_user()
        except Exception, ex:
            logger1.info('Failed to retrieve user\n' + str(ex))
            raise Exception, "Failed to retrieve user\n" + str(ex)

    # # Create user
    # Throw exception if failed
    #  Add 01 to make sure it's executed first
    def test_1_create01_user(self):
        try:
            self.myRest.create_user(username, drive_password, fullname, trueBoolean)
        except Exception, ex:
            logger1.info('Failed to create user\n' + str(ex))
            raise Exception, "Failed to create user\n" + str(ex)


    # # update password for admin user (only need to provide new pw as it's admin)
    # Throw exception if failed
    def test_2_update_user(self):
        try:
            self.myRest.update_user(username, drive_password, password_new)
            test.uut[Fields.web_password] = password_new
        except Exception, ex:
            logger1.info('Failed to update password\n' + str(ex))
            raise Exception, "Failed to update password\n" + str(ex)

    # # Delete user
    # Throw exception if failed
    def test_4_delete_user(self):
        try:
            self.myRest.delete_user(username)
        except Exception, ex:
            logger1.info('Failed to delete user\n' + str(ex))
            raise Exception, "Failed to delete user\n" + str(ex)

    # # Enable SSH on test unit
    #  Throw exception if failed
    def test_1_set_ssh(self):
        try:
            self.myRest.set_ssh()
        except Exception, ex:
            logger1.info('Failed to enable ssh\n' + str(ex))
            raise Exception, "Failed to enable ssh\n" + str(ex)


    # # Retrieve SSH configuration on test unit
    #  Throw exception if failed
    def test_3_get_ssh_configuration(self):
        try:
            self.myRest.get_ssh_configuration()
        except Exception, ex:
            logger1.info('Failed to retrieve SSH configuration\n' + str(ex))
            raise Exception, "Failed to retrieve SSH configuration\n" + str(ex)

    # # Retrieve fw info on test unit
    #  Throw exception if failed
    def test_3_get_firmware_info(self):
        try:
            self.myRest.get_firmware_info()
        except Exception, ex:
            logger1.info('Failed to retrieve firmware info\n' + str(ex))
            raise Exception, "Failed to retrieve firmware info\n" + str(ex)

    # # Retrieve device info on test unit
    #  Throw exception if failed
    def test_3_get_device_info(self):
        try:
            self.myRest.get_device_info()
        except Exception, ex:
            logger1.info('Failed to retrieve device info\n' + str(ex))
            raise Exception, "Failed to retrieve device info\n" + str(ex)

    # # Retrieve device description on test unit
    #  Throw exception if failed
    def test_3_get_device_description(self):
        try:
            self.myRest.get_device_description()
        except Exception, ex:
            logger1.info('Failed to retrieve device description\n' + str(ex))
            raise Exception, "Failed to retrieve device description\n" + str(ex)

    # # Set device description on test unit
    #  Throw exception if failed
    def test_2_set_device_description(self):
        try:
            self.myRest.set_device_description(device_name, device_desc)
        except Exception, ex:
            logger1.info('Failed to set device description\n' + str(ex))
            raise Exception, "Failed to set device description\n" + str(ex)

    # # Retrieve all shares on test unit
    #  Throw exception if failed
    def test_3_get_all_shares(self):
        try:
            self.myRest.get_all_shares()
        except Exception, ex:
            logger1.info('Failed to restart AFP service\n' + str(ex))
            raise Exception, "Failed to restart AFP service\n" + str(ex)

    # # Retrieve share access info for an user
    # # 'sharename' - a private share which user (admin) does/doesn't has access to
    #  Throw exception if failed
    def test_3_get_share_access(self):
        try:
            self.myRest.get_share_access(sharename, username)
        except Exception, ex:
            logger1.info('Failed to retrieve share access info for user %s\n', str(username) + str(ex))
            raise Exception, 'Failed to retrieve share access info for user %s\n', str(username) + str(ex)

    # # Add share access for an user to RW (Read/Write), RO (Read Only)
    # # 'sharename' - a private share which user (admin) doesn't has access to
    #  Throw exception if failed
    def test_1_create_share_access(self):
        try:
            self.myRest.create_share_access(sharename, username, shareaccess)
        except Exception, ex:
            logger1.info('Failed to add share access for user %s\n', str(username) + str(ex))
            raise Exception, 'Failed to add share access for user %s\n', str(username) + str(ex)

    # # Update share access for an user to RW (Read/Write), RO (Read Only)
    # # 'sharename' - a private share which user (admin) has access to
    #  Throw exception if failed
    def test_2_update_share_access(self):
        try:
            self.myRest.update_share_access(sharename, username, shareaccess2)
        except Exception, ex:
            logger1.info('Failed to update share access for user %s\n', str(username) + str(ex))
            raise Exception, 'Failed to update share access for user %s\n', str(username) + str(ex)

    # # Delete share access for an user
    # # 'sharename' - a private share which user (admin) has access to
    #  Throw exception if failed
    def test_4_delete_share_access(self):
        try:
            self.myRest.delete_share_access(sharename, username)
        except Exception, ex:
            logger1.info('Failed to delete share access for user %s\n', str(username) + str(ex))
            raise Exception, 'Failed to delete share access info for user %s\n', str(username) + str(ex)

    # # Create a share with given name and description
    #  Throw exception if failed
    #  Change name to "ashare" to ensure it's executed ahead of other share calls
    def test_1_create_ashare(self):
        try:
            self.myRest.create_share(sharename, sharedesc, False)
        except Exception, ex:
            logger1.info('Failed to create share: %s\n', str(sharename) + str(ex))
            raise Exception, 'Failed to create share: %s\n', str(sharename) + str(ex)

    # # Get info on a share
    #  Throw exception if failed
    def test_3_get_share(self):
        try:
            self.myRest.get_share(sharename)
        except Exception, ex:
            logger1.info('Failed to get share\n' + str(ex))
            raise Exception, "Failed to get share\n" + str(ex)

    # # Update share
    #  Throw exception if failed
    def test_2_update_share(self):
        try:
            self.myRest.update_share(sharename, new_sharename)
        except Exception, ex:
            logger1.info('Failed to update share\n' + str(ex))
            raise Exception, "Failed to update share\n" + str(ex)

    # # Create a directory in a share with given name
    #  Throw exception if failed
    def test_1_create_directory(self):
        try:
            self.myRest.create_directory(dest_share, dirname)
        except Exception, ex:
            logger1.info('Failed to create %s inside %s\n', str(dirname), str(dest_share) + str(ex))
            raise Exception, ('Failed to create directory in a share\n', +str(ex))

    # # Get directories of a share
    #  Throw exception if failed
    def test_3_get_directory(self):
        try:
            self.myRest.get_directory(sharename)
        except Exception, ex:
            logger1.info('Failed to retrieve directory of share %s\n', str(sharename) + str(ex))
            raise Exception, ('Failed to retrieve directory in a share\n', +str(ex))

    # # Delete a directory in a share with given name
    #  Throw exception if failed
    def test_4_delete_directory(self):
        try:
            self.myRest.delete_directory(sharename, dirname)
        except Exception, ex:
            logger1.info('Failed to delete directory inside a share\n', str(ex))
            raise Exception, ('Failed to retrieve directory in a share\n', str(ex))

    # # Copy a directory from one share to another
    #  Throw exception if failed
    def test_2_copy_directory(self):
        try:
            self.myRest.copy_directory(sharename, dirname, dest_share, dest_dir)
        except Exception, ex:
            logger1.info('Failed to copy directory\n', str(ex))
            raise Exception, ('Failed to copy directory\n', str(ex))

    # # Retrieve a directory info
    #  Throw exception if failed
    def test_3_get_directory_info(self):
        try:
            self.myRest.get_directory_info(sharename, dirname)
        except Exception, ex:
            logger1.info('Failed to retrieve directory info\n', str(ex))
            raise Exception, ('Failed to retrieve directory info\n', str(ex))

    # # Copy a file from one share to another
    #  Throw exception if failed
    def test_2_copy_file(self):
        try:
            self.myRest.copy_file(source_share, source_file, dest_share, dest_file)
        except Exception, ex:
            logger1.info('Failed to copy file\n', str(ex))
            raise Exception, ('Failed to copy file\n', str(ex))

    # # Get file info inside a share
    #  Throw exception if failed
    def test_3_get_file(self):
        try:
            self.myRest.get_file(dest_share, dest_file)
        except Exception, ex:
            logger1.info('Failed to retrieve file\n', str(ex))
            raise Exception, ('Failed to retrieve file\n', str(ex))

    # # Get file info inside a directory of a share
    #  Throw exception if failed
    def test_3_get_file_info(self):
        try:
            self.myRest.get_file_info(dest_share, test_dir, test_file)
        except Exception, ex:
            logger1.info('Failed to retrieve file info\n', str(ex))
            raise Exception, ('Failed to retrieve file info\n', str(ex))

    # # Delete file
    #  Throw exception if failed
    def test_4_delete_file(self):
        try:
            self.myRest.delete_file(dest_share, dest_file)
        except Exception, ex:
            logger1.info('Failed to retrieve file info\n', str(ex))
            raise Exception, ('Failed to retrieve file info\n', str(ex))

    # # Retrive volumes info of test device
    #  Throw exception if failed
    def test_3_get_volumes(self):
        try:
            self.myRest.get_volumes()
        except Exception, ex:
            logger1.info('Failed to retrieve volume info\n', str(ex))
            raise Exception, ('Failed to retrieve volume info\n', str(ex))

    # # Get language configuration
    def test_3_get_language_configuration(self):
        try:
            self.myRest.get_language_configuration()
        except Exception, ex:
            logger1.info('Failed to retrieve language configuration\n' + str(ex))
            raise Exception, ('Failed to retrieve language configuration\n', str(ex))

    # # Set language configuration, throw exception otherwise
    def test_1_set_language_configuration(self):
        try:
            self.myRest.set_language_configuration(language)
        except Exception, ex:
            logger1.info('Failed to set language configuration\n' + str(ex))
            raise Exception, ('Failed to set language configuration\n', str(ex))

    # # Update language configuration
    def test_2_update_language_configuration(self):
        try:
            self.myRest.update_language_configuration(language2)
        except Exception, ex:
            logger1.info('Failed to update language configuration\n' + str(ex))
            raise Exception, ('Failed to update language configuration\n', str(ex))

    # # Update language configuration
    #  Reset to en_US language
    def test_2_update_language_configuration2(self):
        try:
            self.myRest.update_language_configuration(language)
        except Exception, ex:
            logger1.info('Failed to update language configuration\n' + str(ex))
            raise Exception, ('Failed to update language configuration\n', str(ex))

    # # Get firmware info
    def test_3_get_firmware_info(self):
        try:
            self.myRest.get_firmware_info()
        except Exception, ex:
            logger1.info('Failed to retrieve firmware info\n' + str(ex))
            raise Exception, ('Failed to retrieve firmware info\n', str(ex))

    # # Immediately check for firmware update available
    def test_2_firmware_info(self):
        try:
            self.myRest.firmware_info()
        except Exception, ex:
            logger1.info('Failed to check for available firmware update\n' + str(ex))
            raise Exception, ('Failed to check for available firmware update\n', str(ex))

    # # Get firmware version
    def test_3_get_firmware_version(self):
        try:
            self.myRest.get_firmware_version()
        except Exception, ex:
            logger1.info('Failed to retrieve firmware version\n' + str(ex))
            raise Exception, ('Failed to retrieve firmware version\n', str(ex))

    # # Get firmware update configuratiopn
    def test_3_get_firmware_update_configuration(self):
        try:
            self.myRest.get_firmware_update_configuration()
        except Exception, ex:
            logger1.info('Failed to retrieve firmware update configuration\n' + str(ex))
            raise Exception, ('Failed to retrieve firmware update configuration\n', str(ex))

    # # Set firmware update configuration
    #  Seem like disable/enable not work, but true/false are working
    def test_2_set_firmware_update_configuration(self):
        try:
            self.myRest.set_firmware_update_configuration(auto_install, auto_install_day, auto_install_hour)
        except Exception, ex:
            logger1.info('Failed to set firmware update configuration\n' + str(ex))
            raise Exception, ('Failed to set firmware update configuration\n', str(ex))

    # # Get firmware update status
    def test_3_get_firmware_update_status(self):
        try:
            self.myRest.get_firmware_update_status()
        except Exception, ex:
            logger1.info('Failed to retrieve firmware update status\n' + str(ex))
            raise Exception, ('Failed to retrieve firmware update status\n', str(ex))

    # # Cause NAS to fetch and update FW automatically
    def test_2_set_firmware_update(self):
        try:
            self.myRest.set_firmware_update()
        except Exception, ex:
            logger1.info('Failed to fetch and update firmware automatically\n' + str(ex))
            raise Exception, ('Failed to fetch and update firmware automatically\n', str(ex))

    # # Cause NAS to fetch and update FW automatically from uri
    #  Currently return success message, but firmware doesn't actually update
    def test_2_start_firmware_update_from_uri(self):
        try:
            self.myRest.start_firmware_update_from_uri(firmware_uri)
        except Exception, ex:
            logger1.info('Failed to fetch and update firmware automatically from uri\n' + str(ex))
            raise Exception, ('Failed to fetch and update firmware automatically from uri\n', str(ex))

    # # Cause NAS to fetch and update FW from file
    def test_1_start_firmware_update_from_file(self):
        try:
            self.myRest.start_firmware_update_from_file(firmware_path)
        except Exception, ex:
            logger1.info('Failed to fetch and update firmware from file\n' + str(ex))
            raise Exception, ('Failed to fetch and update firmware from file\n', str(ex))

    # # Get media crawler status
    def test_3_get_media_crawler_status(self):
        try:
            self.myRest.get_media_crawler_status()
        except Exception, ex:
            logger1.info('Failed to retrieve media crawler status\n' + str(ex))
            raise Exception, ('Failed to retrieve media crawler status\n', str(ex))


    # # Get meta data of a share
    def test_3_get_meta_data(self):
        try:
            self.myRest.get_meta_data(dest_share)
        except Exception, ex:
            logger1.info('Failed to retrieve meta data of share\n' + str(ex))
            raise Exception, ('Failed to retrieve meta data of share\n', str(ex))


    # # Get meta data summary of a share
    def test_3_get_meta_data_summary(self):
        try:
            self.myRest.get_meta_data_summary(dest_share)
        except Exception, ex:
            logger1.info('Failed to retrieve meta data summary of share\n' + str(ex))
            raise Exception, ('Failed to retrieve meta data summary of share\n', str(ex))

    # # Get RAID drive status
    def test_3_get_RAID_drives_status(self):
        try:
            self.myRest.get_RAID_drives_status()
        except Exception, ex:
            logger1.info('Failed to retrieve RAID drives status\n' + str(ex))
            raise Exception, ('Failed to retrieve RAID drives status\n', str(ex))

    # # Get user RAID status
    def test_3_get_user_RAID_status(self):
        try:
            self.myRest.get_user_RAID_status()
        except Exception, ex:
            logger1.info('Failed to retrieve user RAID status\n' + str(ex))
            raise Exception, ('Failed to retrieve user RAID status\n', str(ex))

    # # Get miocrawler status
    def test_3_get_miocrawler_status(self):
        try:
            self.myRest.get_miocrawler_status()
        except Exception, ex:
            logger1.info('Failed to retrieve miocrawler status\n' + str(ex))
            raise Exception, ('Failed to retrieve miocrawler status\n', str(ex))

    # # Get miocrawler status
    def test_2_set_mediarawler(self):
        try:
            self.myRest.set_mediacrawler(dest_share)
        except Exception, ex:
            logger1.info('Failed to retrieve miocrawler status\n' + str(ex))
            raise Exception, ('Failed to retrieve miocrawler status\n', str(ex))

    # # Get alerts, throw exception otherwise
    def test_3_get_alerts(self):
        try:
            self.myRest.get_alerts()
        except Exception, ex:
            logger1.info('Failed to retrieve alerts\n' + str(ex))
            raise Exception, ('Failed to retrieve alerts\n', str(ex))

    # # Acknowledge alert, providing alert id; throw exception otherwise
    def test_2_acknowledge_alert(self):
        try:
            self.myRest.acknowledge_alert(alert_id, trueBoolean)
        except Exception, ex:
            logger1.info('Failed to acknowledge alerts\n' + str(ex))
            raise Exception, ('Failed to acknowledge alerts\n', str(ex))

    # # Get alerts, throw exception otherwise
    def test_4_delete_alert(self):
        try:
            self.myRest.delete_alert(alert_id, ack)
        except Exception, ex:
            logger1.info('Failed to delete alert\n' + str(ex))
            raise Exception, ('Failed to delete alert\n', str(ex))

    # # Get storage usage, throw exception otherwise
    def test_3_get_storage_usage(self):
        try:
            self.myRest.get_storage_usage()
        except Exception, ex:
            logger1.info('Failed to retrieve storage info\n' + str(ex))
            raise Exception, ('Failed to retrieve storage info\n', str(ex))

    # # Get all USB drives info, throw exception otherwise
    def test_3_get_usb_info(self):
        try:
            self.myRest.get_usb_info()
        except Exception, ex:
            logger1.info('Failed to retrieve USB drive info\n' + str(ex))
            raise Exception, ('Failed to retrieve USB drive info\n', str(ex))

    # # Get one USB drive info providing handler, throw exception otherwise
    def test_3_get_usb_info_handler(self):
        try:
            self.myRest.get_usb_info_handler(handler)
        except Exception, ex:
            logger1.info('Failed to retrieve USB drive info\n' + str(ex))
            raise Exception, ('Failed to retrieve USB drive info\n', str(ex))

    # # Get one USB drive info providing handler, throw exception otherwise
    def test_3_get_usb_info_handler(self):
        try:
            self.myRest.get_usb_info_handler(handler)
        except Exception, ex:
            logger1.info('Failed to retrieve USB drive info\n' + str(ex))
            raise Exception, ('Failed to retrieve USB drive info\n', str(ex))

    # # The DELETE request is used to eject a specified Usb drive, given drive's handler
    def test_4_eject_usb_drive(self):
        try:
            self.myRest.eject_usb_drive(handler)
        except Exception, ex:
            logger1.info('Failed to eject USB drive\n' + str(ex))
            raise Exception, ('Failed to eject USB drive\n', str(ex))

    # # Unlock a USB locked drive given drive's handler
    def test_2_unlock_usb_drive(self):
        try:
            self.myRest.unlock_usb_drive(handler, drive_password)
        except Exception, ex:
            logger1.info('Failed to unlock usb drive\n' + str(ex))
            raise Exception, ('Failed to unlock usb drive\n', str(ex))


    # # Get EULA status of test device, throw exception otherwise
    def test_3_get_eula_status(self):
        try:
            self.myRest.get_eula_status()
        except Exception, ex:
            logger1.info('Failed to retrieve EULA status\n' + str(ex))
            raise Exception, ('Failed to retrieve EULA\n', str(ex))


    # # Set EULA status, throw exception otherwise
    def test_1_accept_eula(self):
        try:
            self.myRest.accept_eula()
        except Exception, ex:
            logger1.info('Failed to set EULA status to yes\n' + str(ex))
            raise Exception, ('Failed to set EULA status to yes\n', str(ex))

    # # Get registration status of test device, throw exception otherwise
    def test_3_get_device_registration(self):
        try:
            self.myRest.get_device_registration()
        except Exception, ex:
            logger1.info('Failed to retrieve device registration status\n' + str(ex))
            raise Exception, ('Failed to retrieve device registration status\n', str(ex))

    # # Set registration status of test device, throw exception otherwise
    def test_2_set_device_registration(self):
        try:
            self.myRest.set_device_registration(trueBoolean)
        except Exception, ex:
            logger1.info('Failed to register device\n' + str(ex))
            raise Exception, ('Failed to register device\n', str(ex))

    # # Get system info, throw exception otherwise
    def test_3_get_system_information(self):
        try:
            self.myRest.get_system_information()
        except Exception, ex:
            logger1.info('Failed to retrieve system info\n' + str(ex))
            raise Exception, ('Failed to retrieve system info\n', str(ex))

    # # Get media server configuration, throw exception otherwise
    def test_3_get_media_server_configuration(self):
        try:
            self.myRest.get_media_server_configuration()
        except Exception, ex:
            logger1.info('Failed to retrieve media server configuration\n' + str(ex))
            raise Exception, ('Failed to retrieve media server configuration\n', str(ex))

    # # Set media server configuration of test device, throw exception otherwise
    def test_2_set_media_server_configuration(self):
        try:
            self.myRest.set_media_server_configuration(trueBoolean)
        except Exception, ex:
            logger1.info('Failed to enable media server\n' + str(ex))
            raise Exception, ('Failed to enable media server\n', str(ex))

    # # Get RAID configuration status, throw exception otherwise
    def test_3_raid_configuration_status(self):
        try:
            self.myRest.raid_configuration_status()
        except Exception, ex:
            logger1.info('Failed to retrieve RAID configuration status\n' + str(ex))
            raise Exception, ('Failed to retrieve RAID configuration status\n', str(ex))

    # # Get RAID configuration status, throw exception otherwise
    def test_3_get_raid_configuration_status(self):
        try:
            self.myRest.get_raid_configuration_status()
        except Exception, ex:
            logger1.info('Failed to retrieve RAID configuration status\n' + str(ex))
            raise Exception, ('Failed to retrieve RAID configuration status\n', str(ex))

    # # Set RAID configuration, throw exception otherwise
    #  This will kick off RAID configuration: 1 HDD -> JBOD    2HDD -> RAID1    3HDD->RAID5    4HDD->RAID5
    def test_2_raid_configuration_status(self):
        try:
            self.myRest.raid_configuration_status()
        except Exception, ex:
            logger1.info('Failed to set RAID configuration\n' + str(ex))
            raise Exception, ('Failed to set RAID configuration\n', str(ex))

    # # Retrieve device user, throw exception otherwise
    def test_3_get_device_user(self):
        try:
            self.myRest.get_device_user()
        except Exception, ex:
            logger1.info('Failed to retrieve device user\n' + str(ex))
            raise Exception, ('Failed to retrieve device user\n', str(ex))

    # # Create device user, throw exception otherwise
    def test_1_create_device_user(self):
        try:
            self.myRest.create_device_user()
        except Exception, ex:
            logger1.info('Failed to create device user\n' + str(ex))
            raise Exception, ('Failed to create device user\n', str(ex))

    # # Update device user, throw exception otherwise
    def test_2_update_device_user(self):
        try:
            self.myRest.update_device_user(device_user_id, type, name, email)
        except Exception, ex:
            logger1.info('Failed to update device user\n' + str(ex))
            raise Exception, ('Failed to update device user\n', str(ex))

    # # Delete device user, throw exception otherwise
    def test_4_delete_device_user(self):
        try:
            self.myRest.delete_device_user(device_user_id, device_user_auth_code)
        except Exception, ex:
            logger1.info('Failed to delete device user\n' + str(ex))
            raise Exception, ('Failed to delete device user\n', str(ex))

    # # Login with device_user_id and device_user_auth_code, throw exception otherwise
    def test_3_login(self):
        try:
            self.myRest.login(device_user_id, device_user_auth_code)
        except Exception, ex:
            logger1.info('Failed to login\n' + str(ex))
            raise Exception, ('Failed to login\n', str(ex))

    # # Register remote device, throw exception otherwise
    def test_1_register_remote_device(self):
        try:
            self.myRest.register_remote_device(NASname)
        except Exception, ex:
            logger1.info('Failed to login\n' + str(ex))
            raise Exception, ('Failed to login\n', str(ex))

    # # Set remote device attributes, throw exception otherwise
    def test_2_set_remote_device_attributes(self):
        try:
            self.myRest.set_remote_device_attributes(NASname, remote_access, default_ports_only, manual_port_forward, manual_external_https_port, format)
        except Exception, ex:
            logger1.info('Failed to change attribute\n' + str(ex))
            raise Exception, ('Failed to change attribute\n', str(ex))


    # # Get file content inside a share
    #  Throw exception if failed
    def test_3_get_file_contents(self):
        try:

            with open(video_dl, 'wb') as handle:
                file_response = self.myRest.get_file_contents(source_share, source_file)

                if not file_response.ok:
                    logger1.info('failed to download file')
                for block in file_response.iter_content(1024):
                    if not block:
                        break
                    handle.write(block)


        except Exception, ex:
            logger1.info('Failed to retrieve file content\n', str(ex))
            raise Exception, ('Failed to retrieve file content\n', str(ex))


    def test_3_download_file(self):
        try:
            self.myRest.download_file(source_share, source_file, video_dl)
        except Exception, ex:
            logger1.info('Failed to download file\n' + str(ex))
            raise Exception, ('Failed to download file\n', str(ex))

'''
def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(RESTUnitTest))
    return suite
'''

if __name__ == '__main__':
    # unittest.main()

    # #specify test(s) to execute if needed
    suite = unittest.TestSuite()
    suite.addTest(RESTUnitTest("test_3_status"))
    runner = unittest.TextTestRunner()
    runner.run(suite)
    # unittest.TextTestRunner(verbosity=2).run(suite)
