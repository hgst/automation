""" Seq class. Test cases should subclass the Seq class.

"""
__author__ = 'tran_jas'

from testCaseAPI.src.testclient import TestClient
from seleniumAPI.src.seq_seleniumclient import SeqSeleniumClient


class Sequoia(TestClient):
    """ This class extends the TestClient API to support Sequoia
    """

    def _seq_cleanup(self):
        self.log.info('clean up..')

    # Methods that override the TestClient class
    # noinspection PyPep8Naming
    def __init__(self,
                 checkDevice=True,
                 ip_address=None,
                 selenium_timeout=30,
                 device_file=None,
                 health_monitor_interval=10,
                 health_monitor_thresholds=None):
        TestClient.__init__(self,
                            checkDevice=checkDevice,
                            ip_address=ip_address,
                            selenium_timeout=selenium_timeout,
                            device_file=device_file,
                            health_monitor_interval=health_monitor_interval,
                            health_monitor_thresholds=health_monitor_thresholds,
                            execute_test=False)

        self._ssel = SeqSeleniumClient(self.uut, wait_time=selenium_timeout)

        # Switch the Selenium client to use Sequoia, but first save the UUT's client
        self._uut_sel = self._sel
        self._sel = self._ssel

        self._execute_test()

    def end(self):
        """ Close all browser windows. Needed in case cleanup was skipped.
        """
        self._seq_cleanup()
        exit(self._result)

    # # @brief Accept user if it's displayed
    #
    #  @param accept_eula:  True if want to test EULA acceptance
    def seq_accept_eula(self, accept_eula=None):
        self._sel.seq_accept_eula(accept_eula)


    def is_eula_accepted(self):
        status = self._rest.get_eula_status()
        if status.status_code == 200:
            return True
        else:
            return False

