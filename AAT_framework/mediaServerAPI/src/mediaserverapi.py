""" API to interact with the UUT's media server

    Functions to implement:

    1) SET function for any configuration

    2) GET function for any configuration
        get_option -> Server.get_option(option)
        get_all_options -> Server.get_all_options()

    3) ENABLE/DIABLE Logging
        Enable -> Server.set_logging(True)
        Disable -> Server.set_logging(False)

    4) CHANGE Logging Mode
       Server.set_logging (0 to 5)

    5) ENABLE/DISABLE Restart on NIC changes
       set_restart_on_nic_change

    6) SET Rescan Interval time
        set_rescan_interval

    7) SET Server Name
        done

    8) SET USERNAME and PASSWORD
        done

"""

from global_libraries.testdevice import Fields
from global_libraries.product_info import MediaServers
from global_libraries import wd_exceptions

class MediaServer(object):
    def __init__(self, uut):
        # Load the appropriate media server interface file
        media_server = uut[Fields.media_server]

        if media_server == MediaServers.TWONKY:
            from interface_twonky import Server

        self._uut = uut
        self._server = Server(uut)


    def get_option(self, option):
        return self._server.get_option(option)

    def get_all_options(self):
        return self._server.get_all_options()

    def set_logging(self, state):
        """ Sets the logging to True or False
        :param state: True = Enabled, False = Disabled, 0 - 5 enabled at a specific level (debug = 0, 5 = critical)

        :return: The result of the API call
        """
        return self._server.set_logging(state)

    def set_restart_on_nic_change(self, state):
        """ Sets the option to restart on a NIC change

        :param state: True = Enabled, False = Disabled
        :return: The result of the API call
        """
        return self._server.set_restart_on_nic_change(state)

    def set_rescan_interval(self, interval):
        """ Sets the rescan interval time

        :param interval: An integer value. Use -1 to use inotify. 0 to disable, > 0 for the scan interval
        :return: The result of the API call
        """
        return self._server.set_recan_interval(interval)

    def set_server_name(self, name=None):
        """ Sets the friendly name of the server

        :param name:  The name to use. If None, it will use the UUT's hostname
        :return: The result of the API call
        """

        return self._server.set_server_name(name)

    def set_authentication(self, user, password):
        """ Sets the username and password for the media server configuration

        :param user: The username to use
        :param password: The password to use
        """

        # Set the username and password
        if self._server.set_authentication(user, password):
            self._uut[Fields.media_server_user] = user
            self._uut[Fields.media_server_password] = password
        else:
            raise wd_exceptions.FailedToSetAuthentication('Unable to set authentication')

    def reset_authentication(self):
        """ Restores the authentication back to default
        """

        current_user = self._uut[Fields.media_server_user]
        default_user = self._uut[Fields.default_media_server_user]
        current_password = self._uut[Fields.media_server_password]
        default_password = self._uut[Fields.default_media_server_password]

        if current_user != default_user or current_password != default_password:
            self._server.set_authentication(default_user,
                                            default_password)
