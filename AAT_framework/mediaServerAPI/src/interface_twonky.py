""" Abstraction layer to translate mediaserverapi commands into Twonky commands

"""
import requests

from global_libraries.testdevice import Fields
from global_libraries import wd_exceptions
from global_libraries.wd_logging import create_logger

class Commands(object):
    get_option_setting = 'get_option'
    set_option_setting = 'set_option'
    get_all_settings = 'get_all'
    set_all_settings = 'set_all'
    restart_server = 'restart'
    stop_server = 'stop'
    get_version = 'version'
    get_version_full = 'long_version'
    rescan_database = 'rescan'
    rebuild_database = 'rebuild'
    factory_reset = 'reset'
    list_files = 'dir'
    get_status = 'info_status'
    get_clients = 'info_clients'
    get_clients_connect = 'info_connected_clients'
    enable_client = 'client_enable'
    disable_client = 'client_disable'
    add_client = 'client_add'
    delete_client = 'client_delete'
    reset_clients = 'resetclients'
    get_strings = 'get_language_file'
    translate_string = 'translate_option'
    translate_help = 'translate_help'
    get_log = 'log_getfile'
    clear_log = 'log_clear'
    disable_logging = 'log_disable?1'
    enable_logging = 'log_disable?0'
    get_view_names = 'view_names'
    reload_config = 'reload'
    wakeup_server = 'wakeup'
    sleep_server = 'byebye'
    send_announcement = 'announce'
    change_nic = 'nicchange'
    get_filename_from_oid = 'translate'
    get_license_info = 'license_info'
    get_tls_port = 'get_tls_port'
    get_cache_stats = 'cache_hashtables'
    clear_cache = 'clear_cache'
    get_scanner_stats = 'stat'
    get_friendly_name = 'get_friendly_name'
    is_remote_access_allowed = 'checK-remote_access'




    use_post_list = [set_all_settings]


class Server(object):
    log = create_logger('AAT.twonky')

    def __init__(self, uut):
        self._uut = uut
        
    def get_option(self, option_name):
        return self._send_api_command(Commands.get_option_setting)

    def get_all_options(self):
        """ Gets all options and values from the server

        :return: A dictionary of options and values
        """
        result_list = self._send_api_command(Commands.get_all_settings).split()
        options = {}
        for result_line in result_list:
            result_pair = result_line.split('=')
            option = result_line.split('=')[0]
            if len(result_pair) > 1:
                value = result_line.split('=')[1]
            else:
                value = None

            options[option] = value

        return options

    def set_option(self, option_name, value):
        return self._send_api_command(Commands.set_option_setting,
                                      '{}={}'.format(option_name, value))

    def set_logging(self, state):
        """ Sets the logging state

        :param state: True = Enabled, False = Disabled, 0 - 5 enabled at a specific level (debug = 0, 5 = critical)
        """
        if state is True:
            result = self._send_api_command(Commands.enable_logging)
            if 'enabled' not in result:
                raise wd_exceptions.MediaServerError('Failed to enable logging. Result was {}'.format(result))
        elif state is False:
            result = self._send_api_command(Commands.disable_logging)
            if 'disabled' not in result:
                raise wd_exceptions.MediaServerError('Failed to disable logging. Result was {}'.format(result))
        else:
            # Enable logging
            self.set_logging(True)
            # And set the level to the specified level
            result = self.set_option('v', state)

        self.reload_config_file()
        return result

    def restart(self):
        """ Restarts the media server

        """
        result = self._send_api_command(Commands.restart_server)

        if 'restarting' in result:
            return result
        else:
            raise wd_exceptions.MediaServerError('Failed to restart media server. Result was {}'.format(result))


    def _send_api_command(self, command, parameter_string=None, post_body=None):
        # Command can be one of the following
        # parameter_string is a string appended to the URL (such as 'option=Value')
        # post_body is a list that is parsed to be the body of a POST request (one element per line of the body)

        if self._uut[Fields.media_server_user] is not None or self._uut[Fields.media_server_password] is not None:
            if self._uut[Fields.media_server_user] is not None:
                user = self._uut[Fields.media_server_user]
            else:
                user = ''

            if self._uut[Fields.media_server_password] is not None:
                password = self._uut[Fields.media_server_password]
            else:
                password = ''

            auth = (user, password)
        else:
            auth = None

        base_url = 'http://{}:9000/rpc/{}'.format(self._uut[Fields.internal_ip_address],
                                                  command)

        if parameter_string:
            base_url += '?{}'.format(parameter_string)

        if command in Commands.use_post_list:
            # This command requires a POST command, so build the body
            if post_body:
                post_string = ''
                for line in post_body:
                    post_string += line + '\n'

            return requests.post(base_url,
                                 data=post_body,
                                 auth=auth).text
        else:
            return requests.get(base_url,
                                auth=auth).text

    def set_restart_on_nic_change(self, state):
        """ Sets the option to restart on a NIC change

        :param state: True = Enabled, False = Disabled
        :return:
        """

        if state is True:
            result = self.set_option('nicrestart', '1')
        elif state is False:
            result = self.set_option('nicrestart', '0')
        else:
            raise wd_exceptions.InvalidParameter('set_restart_on_nic_change state must be True or False')

        self.reload_config_file()
        return result

    def set_rescan_interval(self, interval):
        """ Sets the rescan interval time

        :param interval: An integer value. Use -1 to use inotify. 0 to disable, > 0 for the scan interval

        :return: The result of the API call
        """
        result = self.set_option('scantime', interval)
        self.reload_config_file()
        return result

    def reload_config_file(self):
        """ Forces Twonky to re-read its config file. Used after making a change to an option to have the change
            take effect without a full media server restart

        :return: The result of the API call
        """

        return self._send_api_command(Commands.reload_config)

    def set_server_name(self, name=None):
        """ Sets the friendly name of the server

        :param name:  The name to use. If None, it will use the UUT's hostname
        :return: The result of the API call
        """

        if name is None:
            name = '%HOSTNAME%'
        result = self.set_option('friendlyname', name)
        self.reload_config_file()
        return result

    def set_authentication(self, user, password):
        """ Sets the username and password
        :param user: The user to set
        :param password: The password to set
        :return: True on success, False on failure
        """

        user_option = 'accessuser'
        password_option = 'accesspwd'

        # Store the current user in case setting the password fails
        old_user = self.get_option(user_option)
        if user is None:
            user = ''

        result = self.set_option(user_option, user)
        if result != user:
            # Failed
            self.log.warning('Unable to set user to {}. Result was {}'.format(user,
                                                                              result))
            return False

        if password is None:
            password = ''

        result = self.set_option(password_option, password)
        if result != password:
            # Failed
            self.log.warning('Unable to set password to {}. Result was {}'.format(password,
                                                                                  result))

            # Revert the user
            self.set_option(user_option, old_user)
            return False

        return True
