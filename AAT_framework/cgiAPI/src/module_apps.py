availableApps = {
    'aMule': 'css=.LightningCheckbox,f_apps_install_0',
    'IceCast': 'css=.LightningCheckbox,f_apps_install_1',
    'Joomla': 'css=.LightningCheckbox,f_apps_install_2',
    'phpBB': 'css=.LightningCheckbox,f_apps_install_3',
    'phpMyAdmin':'css=.LightningCheckbox,f_apps_install_4',
    'SqueezeCenter':'css=.LightningCheckbox,f_apps_install_5',
    'WordPress':'css=.LightningCheckbox,f_apps_install_6',
    'nzbget':'css=.LightningCheckbox,f_apps_install_7',
    'git':'css=.LightningCheckbox,f_apps_install_8',
    'Transmission':'css=.LightningCheckbox,f_apps_install_9',
    'DVBLink':'css=.LightningCheckbox,f_apps_install_10',
    'Dropbox':'css=.LightningCheckbox,f_apps_install_11',
    'plexmediaserver':'css=.LightningCheckbox,f_apps_install_12',
    'AcronisTrueImage':'css=.LightningCheckbox,f_apps_install_13'
}

