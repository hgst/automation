"""
Created on May 29, 2015

@author: vasquez_c, lin_ri, hoffman_t
"""


from urllib import quote_plus
import base64
import copy
import datetime
import decorator
import os
import random
import requests
import sys
import time
import types
import xml.etree.ElementTree
import xmltodict
import urllib2
import collections

from global_libraries import wd_exceptions
from requests import exceptions
from requests import models

from global_libraries.testdevice import Fields
# noinspection PyPep8Naming
import global_libraries.CommonTestLibrary as ctl
from global_libraries import utilities
from module_apps import availableApps

REST_CALL_TIMEOUT = 90  # 90 second timeout


class CgiClient(object):

    def __init__(self, uut):
        self.uut = uut
        self.log = ctl.getLogger('AAT.cgiclient')

    def _send_cgi_cmd(self, command, header, uutIP=None):
        destip = uutIP or self.uut[Fields.internal_ip_address]
        loginurl = 'http://{}/cgi-bin/login_mgr.cgi?'.format(destip)
        logindata = {
            'cmd': 'wd_login',
            'port': '',
            'username': 'admin',
            'pwd': ''
        }
        s = requests.session()
        r = s.post(url=loginurl, data=logindata)
        if r.status_code != 200:
            raise Exception("CGI Login authentication failed!!!, status_code = {}".format(r.status_code))
        cmdUrl = 'http://{}/cgi-bin/{}cmd={}'.format(destip, header, command)
        r = s.get(cmdUrl)
        if r.status_code != 200:
            raise Exception("CGI command execution failed!!!, status_code = {}".format(r.status_code))
        self.log.debug("Successful CGI CMD: {}".format(cmdUrl))
        return r

    #
    # CGI API function wrappers
    #

    def add_app(self, appName='IceCast', uutIP=None):
        if appName not in availableApps.keys():
            self.log.error("{} is NOT an available app!!".format(appName))
            raise Exception("Invalid App Name: {}".format(appName))
        header = 'apkg_mgr.cgi?'
        cmd = 'cgi_apps_auto_install&f_module_name={}'.format(appName)
        self._send_cgi_cmd(cmd,header,uutIP)

    def delete_app(self, appName='IceCast', uutIP=None):
        if appName not in availableApps.keys():
            self.log.error("{} is NOT an available app!!".format(appName))
            raise Exception("Invalid App Name: {}".format(appName))
        header = 'apkg_mgr.cgi?'
        cmd = 'cgi_apps_del&f_module_name={}'.format(appName)
        self._send_cgi_cmd(cmd, header, uutIP)

    def save_config_file(self):
        header = 'system_mgr.cgi?'
        cmd = 'cgi_backup_conf'
        self._send_cgi_cmd(cmd, header)

    def reset_network_config_dhcp(self):
        self._send_cgi_cmd(command='cgi_setip_lock&lan=0&dhcp=1', header='network_mgr.cgi?')
        self._send_cgi_cmd(command='cgi_ip&f_dhcp_enable=1& &vlan_enable=0&vlan_id=&f_dns_manual=1&lan=0', header='network_mgr.cgi?')
        self._send_cgi_cmd(command='cgi_setip_lock&lan=0&dhcp=1', header='network_mgr.cgi?')

    def get_alerts(self):
        header = 'system_mgr.cgi?'
        cmd = 'cgi_get_alert'
        result = self._send_cgi_cmd(cmd, header)
        return result

    def get_file_info(self, file_name, parent_share_path):
        if not parent_share_path.endswith('/'):
            parent_share_path += '/'

        header = 'webfile_mgr.cgi?'
        cmd = 'cgi_get_properties&path={0}{1}'.format(parent_share_path, file_name)
        result = self._send_cgi_cmd(cmd, header)
        return result

    def delete_remote_backup_job(self, job_name=None):
        header = 'remote_backup.cgi?'
        cmd = 'cgi_del_schedule&name={0}'.format(job_name)
        self._send_cgi_cmd(cmd, header)

    def enable_ftp_service(self, state='ON'):
        """
        :param state: Turn ON or OFF the ftp service
        :return: 1 if success
        """
        access = {
            'ON': 1,
            'OFF': 0
        }
        if state not in access.keys():
            self.log.error("ERROR: Unable to recognize the state information({}), please use ON or OFF.".format(state))
        header = 'app_mgr.cgi?'
        cmd = 'FTP_Server_Enable&f_state={}'.format(access[state])
        result = self._send_cgi_cmd(cmd, header)
        return result

    def get_usb_mapping_info(self):
        header = 'folder_tree.cgi?'
        cmd = 'cgi_get_USB_Mapping_Info'
        result = self._send_cgi_cmd(cmd, header)
        return result