# This test client will demo the typical workflow from Jenkins
# 1. Get list of products
# 2. Get devices for a specific product
# 3. Get an available device
# 4. Checkout the device
# 5. Run the test (assumed)
# 6. Check in the device
from InventoryServerApi import *

class logger(object):
    def debug(self, owner, message):
        print message

theLogger = logger()
restApi = RestAPI('http://sevautoweb.labspan.wdc.com/InventoryServer',theLogger)

deviceApi = restApi.deviceApi
productApi = restApi.productApi
powerSwitchApi = restApi.powerSwitchApi
serialServerApi = restApi.serialServerApi
sshGatewayApi = restApi.sshGatewayApi

try:
    # 1. Get the list of products
    productList = productApi.list()['list']
    print 'product list ' + str(productList)
    # Get the Lightning product
    product = None
    for p in productList:
        if p['name'] == 'Lightning':
            product = p
            break

    if product is None:
        print 'Product not found'
        exit()

    # 2. Get devices for a product
    devices = productApi.getDevices(product['id'])['devices']
    if len(devices) == 0:
        print 'No devices for product'
        exit()

    # 3. Get an available device
    device = None
    for d in devices:
        if d['isOperational'] == True and d['isBusy'] == False:
            device = d
            break

    if device is None:
        print 'No available devices'
        exit()


    #4. CheckOut the device
    if not deviceApi.checkOut(device['id'],'MyJenkinsJob'):
        print 'Failed to checkout the device'
        exit()

    #5. Run the test

    #6. Check in the device
    if not deviceApi.checkIn(device['id']):
        print 'Failed to checkin the device'
        exit()


except InventoryException, ex:
    print "Status = {0},Status code = {1}, message = {2}".format(ex.status, ex.statusCode, ex.message)
