from requests import Request, Session

import global_libraries.CommonTestLibrary as ctl


class RestAPI(object):
    def __init__(self, server_url=None, logger=None, location=None):
        """ @brief Inventory server REST client
        @ param url: Optional URL for the inventory server. Overrides everything else
        @ param logger: Optional _logger to use for debug and error messages
        @ param location: Physical location of the inventory server (Irvine, Taiwan, etc.).
                          Details are in global_libraries.testenvironment.py
        """
        if logger is None:
            self.log = ctl.getLogger('AAT.inventoryserverapi-RestAPI')
        else:
            self.log = logger

        self._location = location
        self.url = None

        self._session = Session()

        if server_url is None:
            self.log.debug('Inventory server URL not specified. Test aborted.')
            raise InventoryException()
        else:
            self.url = ctl.clean_url(server_url)

        if self.url is not None:
            self.log.debug('Inventory server is ' + self.url)
        else:
            self.log.debug('Invalid Inventory server URL of ' + server_url + '. Test aborted.')
            raise InventoryException()

        # Default headers
        self.headers = {'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
        # References to encapsulated APIs
        self.deviceApi = RestAPI.DeviceAPI(self)
        self.powerSwitchApi = RestAPI.PowerSwitchAPI(self)
        self.serialServerApi = RestAPI.SerialServerAPI(self)
        self.sshGatewayApi = RestAPI.SSHGatewayAPI(self)
        self.productApi = RestAPI.ProductAPI(self)

    def restCall(self, request):
        preparedRequest = self._session.prepare_request(request)
        preparedRequest.headers = self.headers
        response = self._session.send(preparedRequest)
        self.log.debug("REST Call: {0}".format(response.url))
        jsonResponse = response.json()
        self.log.debug("JSON Response: {0}".format(jsonResponse))

        if jsonResponse is {} and jsonResponse['statusCode'] != 1000:
            raise InventoryException(jsonResponse['status'], jsonResponse['statusCode'], jsonResponse['message'])

        return response.json()

    class DeviceAPI(object):
        def __init__(self, restApi):
            self.restApi = restApi
            self.log = ctl.getLogger('AAT.inventoryserverapi-deviceapi')

        def list(self):
            url = str.format("{0}/devices", self.restApi.url)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def get(self, deviceId):
            url = str.format("{0}/devices/{1}", self.restApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def create(self, macAddress, productId, internalIPAddress=None, location=None, firmware=None,
                   powerSwitchId=None,
                   sshGatewayId=None, serialServerId=None):
            url = str.format("{0}/devices", self.restApi.url)
            data = {'macAddress': macAddress, 'product': productId, 'internalIPAddress': internalIPAddress,
                    'location': location, 'firmware': firmware,
                    'powerSwitch': powerSwitchId, 'sshGateway': sshGatewayId, 'serialServer': serialServerId}
            request = Request('POST', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def update(self, deviceId, macAddress=None, productId=None, internalIPAddress=None, location=None,
                   firmware=None, powerSwitchId=None,
                   sshGatewayId=None, serialServerId=None, isBusy=None, isOperational=None, jenkinsJob=None):
            url = str.format("{0}/devices/{1}", self.restApi.url, deviceId)
            data = {'macAddress': macAddress, 'product': productId, 'internalIPAddress': internalIPAddress,
                    'location': location, 'firmware': firmware,
                    'powerSwitch': powerSwitchId, 'sshGateway': sshGatewayId, 'serialServer': serialServerId,
                    'isBusy': isBusy, 'isOperational': isOperational, 'jenkinsJob': jenkinsJob}
            request = Request('PUT', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def delete(self, deviceId):
            url = str.format("{0}/devices/{1}", self.restApi.url, deviceId)
            request = Request('DELETE', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def checkIn(self, deviceId):
            url = str.format("{0}/api/device/checkIn/{1}", self.restApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse['checkedIn']

        def checkOut(self, deviceId, jenkinsJob, force=False):
            url = str.format("{0}/api/device/checkOut/{1}", self.restApi.url, deviceId)
            params = {'jenkinsJob': jenkinsJob, 'force': force}
            request = Request('GET', url, params=params)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse['checkedOut']

        def isAvailable(self, deviceId):
            url = str.format("{0}/api/device/isAvailable/{1}", self.restApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse['isAvailable']

        def getSSHGateway(self, deviceId):
            url = str.format("{0}/api/device/getSSHGateway/{1}", self.restApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse['sshGateway']

        def getSerialServer(self, deviceId):
            url = str.format("{0}/api/device/getSerialServer/{1}", self.restApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse['serialServer']

        def getPowerSwitch(self, deviceId):
            url = str.format("{0}/api/device/getPowerSwitch/{1}", self.restApi.url, deviceId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse['powerSwitch']

        def getDeviceByJob(self, jenkinsJob):
            self.log.info('Getting device information for job ' + str(jenkinsJob))
            url = str.format("{0}/api/device/getDeviceByJob", self.restApi.url)
            params = {'jenkinsJob': jenkinsJob}
            request = Request('GET', url, params=params)
            jsonResponse = self.restApi.restCall(request)
            self.log.debug('Response = ' + str(jsonResponse))
            return jsonResponse['device']

    class PowerSwitchAPI(object):
        def __init__(self, restApi):
            self.restApi = restApi
            self.log = ctl.getLogger('AAT.inventoryserverapi-powerswitchapi')

        def list(self):
            url = str.format("{0}/powerSwitches", self.restApi.url)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def get(self, powerSwitchId):
            url = str.format("{0}/powerSwitches/{1}", self.restApi.url, powerSwitchId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def create(self, ipAddress, port):
            url = str.format("{0}/powerSwitches", self.restApi.url)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('POST', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def update(self, powerSwitchId, ipAddress=None, port=None):
            url = str.format("{0}/powerSwitches/{1}", self.restApi.url, powerSwitchId)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('PUT', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def delete(self, powerSwitchId):
            url = str.format("{0}/powerSwitches/{1}", self.restApi.url, powerSwitchId)
            request = Request('DELETE', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

    class SerialServerAPI(object):
        def __init__(self, restApi):
            self.restApi = restApi
            self.log = ctl.getLogger('AAT.inventoryserverapi-serialserverapi')

        def list(self):
            url = str.format("{0}/serialServers", self.restApi.url)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def get(self, serialServerId):
            url = str.format("{0}/serialServers/{1}", self.restApi.url, serialServerId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def create(self, ipAddress, port):
            url = str.format("{0}/serialServers", self.restApi.url)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('POST', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def update(self, serialServerId, ipAddress=None, port=None):
            url = str.format("{0}/serialServers/{1}", self.restApi.url, serialServerId)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('PUT', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def delete(self, serialServerId):
            url = str.format("{0}/serialServers/{1}", self.restApi.url, serialServerId)
            request = Request('DELETE', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

    class SSHGatewayAPI(object):
        def __init__(self, restApi):
            self.restApi = restApi
            self.log = ctl.getLogger('AAT.inventoryserverapi-sshgatewayapi')

        def list(self):
            url = str.format("{0}/SSHGateways", self.restApi.url)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def get(self, sshGatewayId):
            url = str.format("{0}/SSHGateways/{1}", self.restApi.url, sshGatewayId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def create(self, ipAddress, port):
            url = str.format("{0}/SSHGateways", self.restApi.url)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('POST', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def update(self, sshGatewayId, ipAddress=None, port=None):
            url = str.format("{0}/SSHGateways/{1}", self.restApi.url, sshGatewayId)
            data = {'ipAddress': ipAddress, 'port': port}
            request = Request('PUT', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def delete(self, sshGatewayId):
            url = str.format("{0}/SSHGateways/{1}", self.restApi.url, sshGatewayId)
            request = Request('DELETE', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

    class ProductAPI(object):
        def __init__(self, restApi):
            self.restApi = restApi
            self.log = ctl.getLogger('AAT.inventoryserverapi-productapi')

        def list(self):
            url = str.format("{0}/products?max=50", self.restApi.url)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def get(self, productId):
            url = str.format("{0}/products/{1}", self.restApi.url, productId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def create(self, name):
            url = str.format("{0}/products", self.restApi.url)
            data = {'name': name}
            request = Request('POST', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def update(self, productId, name):
            url = str.format("{0}/products/{1}", self.restApi.url, productId)
            data = {'name': name}
            request = Request('PUT', url, params=data)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def delete(self, productId):
            url = str.format("{0}/products/{1}", self.restApi.url, productId)
            request = Request('DELETE', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse

        def getDevices(self, productId):
            url = str.format("{0}/api/product/getDevices/{1}", self.restApi.url, productId)
            request = Request('GET', url)
            jsonResponse = self.restApi.restCall(request)
            return jsonResponse


class InventoryException(Exception):
    def __init__(self, status=None, statusCode=None, message=None):
        self.status = status
        self.statusCode = statusCode
        self.message = message

