'''
# Created on Jul 8, 2014
# @author: Jason Tran
# 
# This module contains the class and methods to run Selenium API test cases.
#
'''

import unittest
import logging
import time


'''
## to run tests from command line

#import SeleniumClient from sibling directory
import os 
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir0 = os.path.dirname(currentdir)
sys.path.insert(0,parentdir0) 
from src import SELENIUMClient

#import logger from grandparent directory
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
parentdir2 = os.path.dirname(parentdir)
sys.path.insert(0,parentdir2) 
from UnitTestFramework import mylogger
'''

from seleniumAPI.src.seleniumclient import SeleniumClient
from UnitTestFramework import mylogger

l = mylogger.Logger()
logger1 = l.myLogger()
logger1 = logging.getLogger('SeleniumAPI')

#variable to be used for unit testing
ip_address = '192.168.1.120'            # IP of test unit, to be used in Setup method   
username = 'admin'                      # default username of test unit
password = ''                           # default password of test unit
page_name = 'Apps'                      # to be used in test_get_to_page
numberOfgroups = 2                      # to be used in test_create_groups
start_prefix = 1                        # to be used in test_create_multiple_users
numberOfusers = 3                       # number of users to be created, to be used in test_create_multiple_users
numberOfshares = 2                      # number of shares to be created, to be used in test_create_shares
element_id = 'home_firmwareInfo_value'  # to be used in element_text_should_be
expected_text = '1.05.08'               # to be used in element_text_should_be

locator = 'logout_toolbar'              # to be used in test_is_element_visible
group_name = 'group1'                   # to be used in  test_assign_group_to_share and test_assign_user_to_group
share_number = 1                        # to be used in test_assign_group_to_share
user_name = 'user1'                     # to be used in test_assign_user_to_group
size_of_quota = '10'                    # to be used in test_create_group_quota
unit_size = 'GB'                        # to be used in test_create_group_quota
name = user_name                        # to be used in test_find_element_in_sublist
group_user_or_share = group_name        # to be used in test_find_element_in_sublist 
locator_2 = 'vol_capacity'              # to be used in test_wait_until_element_is_not_visible
group_number = 1                        # to be used in test_find_group_in_user_sublist

class SeleniumUnitTest(unittest.TestCase):
    
    ## Method is called at the beginning of test to instatiate RESTClient then do local login
    #  IP must be provided, using defaul username 'admin' and blank password
    def setUp(self):                
        logger1.info('Start executing %s', self._testMethodName)
         
        #instantiate Selenium client class
        try:
            self.mySelenium = SeleniumClient(ip_address)
        except Exception,ex:
            logger1.info('Failed to instantiate Selenium client\n' + str(ex))
            raise Exception, 'Failed to instantiate Selenium client\n' + str(ex)
        
        #open browser
        try:
            self.mySelenium.access_webUI()
        except Exception,ex:
            logger1.info('Failed to access device web UI\n' + str(ex))          
            raise Exception, 'Failed to access device web UI\n' + str(ex)
    
    def tearDown(self):  
        '''
        ##log out
        try:
            self.mySelenium.click_on_home_logout_icon()
        except Exception,ex:
            logger1.info('Failed to click home logout icon\n' + str(ex))          
            raise Exception, 'Failed to click home logout icon\n' + str(ex) 
        '''
        
        ## close browser
        try:
            self.mySelenium.close_webUI()
        except Exception,ex:
            logger1.info('Failed to close device web UI\n' + str(ex)) 
            raise Exception, 'Failed to close device web UI\n' + str(ex)
        
        logger1.info('Finished executing %s', self._testMethodName)
         
    ## Go to a page of the device web UI, provide page name    
    def test_get_to_page(self):
        try:
            self.mySelenium.get_to_page(page_name)
        except Exception,ex:
            logger1.info('Failed to go to %s \n', str(page_name) + str(ex))          
            raise Exception, ('Failed to go to %s ', str(page_name) + str(ex))   

    ## Create group is not working due to invalid html tag 
    def test_00_create_groups(self):
        try:
            self.mySelenium.create_groups(numberOfgroups)
        except Exception,ex:
            logger1.info('Failed to create group\n' + str(ex))          
            raise Exception, 'Failed to create group\n' + str(ex) 

    ## Creates a given number of users using the create multiple users button on the users page of the webUI. 
    #  Users are named as follows: userX where X starts at the given start prefix
    #    and is continued sequentially until the proper number of 
    #    users are created. The password for the created user is the default 'welc0me'
    def test_00_create_multiple_users(self):
        try:
            self.mySelenium.create_multiple_users(start_prefix, numberOfusers)
        except Exception,ex:
            logger1.info('Failed to create user\n' + str(ex))          
            raise Exception, 'Failed to create user\n' + str(ex) 
    
    ## Creates number of shares by passing in # of shares   
    def test_00_create_shares(self):
        try:
            self.mySelenium.create_shares(numberOfshares)
        except Exception,ex:
            logger1.info('Failed to create share\n' + str(ex))          
            raise Exception, 'Failed to create share\n' + str(ex) 

    ## Verify text of an element by giving element's ID and expected text 
    #  Prerequisite: element_id, expected_text are available in test unit  
    def test_element_text_should_be(self):
        try:
            self.mySelenium.element_text_should_be(element_id, expected_text)
        except Exception,ex:
            logger1.info('Failed to verify element text\n' + str(ex))          
            raise Exception, 'Failed to verify element text\n' + str(ex) 

    ## Go to group tab   
    def test_get_to_group_tab(self):
        try:
            self.mySelenium.get_to_group_tab()
        except Exception,ex:
            logger1.info('Failed to go to group tab\n' + str(ex))          
            raise Exception, 'Failed to go to group tab\n' + str(ex) 

    ## Delete all groups  
    def test_z_delete_groups(self):
        try:
            self.mySelenium.delete_groups()
        except Exception,ex:
            logger1.info('Failed to delete groups\n' + str(ex))          
            raise Exception, 'Failed to delete groups\n' + str(ex) 
    
    '''
    ## Configure unit drives as JBOD 
    def test_configure_jbod(self):
        try:
            self.mySelenium.configure_jbod()
        except Exception,ex:
            logger1.info('Failed to configure unit as JBOD\n' + str(ex))          
            raise Exception, 'Failed to configure unit as JBOD\n' + str(ex) 
    '''
        
    ## Delete all shares, excluding Public    
    # Note: see this warning - [ WARN ] Keyword 'Capture Page Screenshot' could not be run on failure: 'NoneType' object has no attribute 'get_handler'
    def test_z_delete_shares(self):
        try:
            self.mySelenium.delete_shares()
        except Exception,ex:
            logger1.info('Failed to delete shares\n' + str(ex))          
            raise Exception, 'Failed to delete shares\n' + str(ex) 
       
    ## Assigns the given group to the give share number with the given access level
    #  Default access_type='rw', optional to pass in different access level
    #  share_number is the number append to the word 'group'
    #  Prerequisite: group_name and share_number are available in test unit    
    def test_1_assign_group_to_share(self):
        try:
            self.mySelenium.assign_group_to_share(group_name, share_number)
        except Exception,ex:
            logger1.info('Failed to assign group to share\n' + str(ex))          
            raise Exception, 'Failed to assign group to share\n' + str(ex)                 
        
    ## Assigns the given user to the given group
    #  Prerequisite: user_name, group_name are available in test unit
    #  Error: Element locator '//div[@id='m_group_div']/ul/li[2]/div/label/span' did not match any elements.
    def test_1_assign_user_to_group(self):
        try:
            self.mySelenium.assign_user_to_group(user_name, group_name)
        except Exception,ex:
            logger1.info('Failed to assign user to group\n' + str(ex))          
            raise Exception, 'Failed to assign user to group\n' + str(ex) 
      
    ## Adjusts the quota for the given group to the given size
    #  Acceptable Unit Size:
    #    MB - Megabyte
    #    GB - Gigabyte
    #    TB - Terabyte
    #  Prerequisite: group_name is available in test unit
    #  [ WARN ] Keyword 'Capture Page Screenshot' could not be run on failure: 'NoneType' object has no attribute 'get_handler'
    def test_01_create_group_quota(self):
        try:
            self.mySelenium.create_group_quota(group_name, size_of_quota, unit_size)
        except Exception,ex:
            logger1.info('Failed to adjust quota for given group\n' + str(ex))          
            raise Exception, 'Failed to adjust quota for given group\n' + str(ex)
        
    ## Finds an element in the given group, user or share sublists given the name of that element  
    #  Prerequisite: name, group_user_or_share are available in test unit  
    def test_find_element_in_sublist(self):
        try:
            self.mySelenium.find_element_in_sublist(name, group_user_or_share)
        except Exception,ex:
            logger1.info('Failed to find an element in a given list\n' + str(ex))          
            raise Exception, 'Failed to find an element in a given list\n' + str(ex)
    
    '''        
    ## Finds the group name with the given group number in the group sublist
    #    Mainly used in the assign_user_to_group function 
    #  Error: Element locator 'css=div.jspDrag' did not match any elements.     
    def test_find_group_in_user_sublist(self):
        try:
            self.mySelenium.find_group_in_user_sublist(group_number)
        except Exception,ex:
            logger1.info('Failed to find group %s, with error %s ', str(group_number), str(ex))          
            raise Exception, 'Failed to find group name\n' + str(ex)
    '''
            
    ## Locate given element  
    #  Prerequisite: locator (element id) is available on browser      
    def test_is_element_visible(self):
        return_value = self.mySelenium.is_element_visible(locator)
        expected = True
        self.assertEqual(return_value, expected, 'Failed to locate element %s.' % locator)

    ## Verify element is not visible in browser, time_out value is 60 and can be changed  
    #  Prerequisite: locator (element id) is not available on browser 
    #  Require user interaction - Once browser is open, click a different tab (not Home) 
    #    as it tries to verify 'vol_capacity' on home page is not there anymore    
    def test_wait_until_element_is_not_visible(self):
        return_value = self.mySelenium.wait_until_element_is_not_visible(locator_2, time_out = 10)
        expected = True
        self.assertEqual(return_value, expected, 'Element still visible %s.' % locator_2)
        
    ## Verify button is clickable  
    #  Prerequisite: locator (button) is available on browser 
    #  Usually use when a popup is displayed. For this test, negative testing - verify button is not there to be clicked                      
    def test_wait_until_button_is_clickable(self):
        return_value = self.mySelenium.wait_until_button_is_clickable(locator)
        expected = True
        self.assertEqual(return_value, expected, 'Button is not clickable %s.' % locator)

if __name__ == '__main__':
    ## To run whole test suite
    #unittest.main()
    
    
    ##specify test(s) to execute individual
    suite = unittest.TestSuite()
    
    #suite.addTest(SeleniumUnitTest("test_00_create_groups"))
    #suite.addTest(SeleniumUnitTest("test_00_create_multiple_users"))
    #suite.addTest(SeleniumUnitTest("test_1_assign_user_to_group"))
    #suite.addTest(SeleniumUnitTest("test_00_create_shares"))
    
    suite.addTest(SeleniumUnitTest("test_1_assign_group_to_share"))
    
    runner = unittest.TextTestRunner()
    #runner.run(suite)
    unittest.TextTestRunner(verbosity=2).run(suite)