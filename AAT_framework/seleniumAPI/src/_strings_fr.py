# -*- coding: UTF-8 -*-
__author__ = 'Troy Hoffman'
from ui_map import Elements as E

class StringList(object):
    default_values = {
        E.LOGIN_USERNAME_TEXT: u'Utilisateur',
        E.LOGIN_USERNAME: u'admin',
        E.LOGIN_PASSWORD_TEXT: u'Mot de passe',
        E.LOGIN_PASSWORD: u'Aucun mot de passe créé',
        E.LOGIN_REMEMBER_ME_TEXT: u'Mémoriser mes informations',
        E.LOGIN_REMEMBER_ME: 0,
        E.LOGIN_BUTTON: 'Connexion'
    }


