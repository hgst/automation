""" Selenium methods for Webfiles

"""
__author__ = 'hoffman_t'

from seleniumclient import SeleniumClient
from seleniumclient import ElementNotReady
from webfilesAPI.src.wf_keys import Keys

from wf_ui_map import Pages
from wf_ui_map import Elements

# Override the classes in ui_map
import ui_map
import wf_ui_map

ui_map.Page_Info = wf_ui_map.Page_Info
ui_map.Pages = wf_ui_map.Pages


class WfSeleniumClient(SeleniumClient):
    """ Webfiles Selenium methods

    :param wf_settings: The wf dictionary of settings
    :param uut: The UUT dictionary of settings
    :param wait_time: Amount of time to wait before a method times out
    """

    def __init__(self, wf_settings, uut, wait_time=30):
        SeleniumClient.__init__(self, uut=uut, wait_time=wait_time)

        self._wf = wf_settings
        self._browser = self._wf[Keys.wf_browser]

    def wf_launch(self, use_ssl=True):
        """ Launches a web browser and connects to the Webfiles main page

        :param use_ssl: If True, connect with HTTPS
        """
        wf_url = self._wf[Keys.wf_server_url]
        if '//' not in wf_url:
            # No protocol defined, so prepend HTTP or HTTPS
            if use_ssl:
                url = 'https'
            else:
                url = 'http'

            url += '://{}'.format(wf_url)
        else:
            # Use as-is
            url = wf_url

        self.go_to_url(url=url)

    def wf_login(self, user, password, use_ssl=True):
        """ Launches the WF UI, clicks the login button, enters the user and password, and logs in

        :param user: The user to login with
        :param password: The password to use
        :param use_ssl: If True, use HTTPS
        """
        # Get to the login page. First, close any open browsers
        self.close_all_browsers()
        # Now, launch
        self.wf_launch(use_ssl=use_ssl)

        if self.is_already_on_page(Pages.SIGN_IN_PAGE):
            self.click_button(Elements.SIGN_IN_BUTTON)
        else:
            self.log.error('Not on the correct page')

        # Now we're on the login page, so enter the username and password
        self.input_text(Elements.LOG_IN_USERNAME, user)
        self.input_password(Elements.LOG_IN_PASSWORD, password)
        # And login
        self.click_button(Elements.LOG_IN_BUTTON)

        # Now, wait up to 2 seconds for the "connecting" text to appear
        try:
            self.wait_until_element_is_visible(element=Elements.HOME_CONNECTING_TEXT, timeout=2)
        except ElementNotReady:
            # If it doesn't appear, that's okay. It just means it was too fast to catch
            self.log.debug('{} did not appear'.format(Elements.HOME_CONNECTING_TEXT.name))
        else:
            # Now, wait up to 60 seconds for it to disappear
            self.wait_until_element_is_not_visible(locator=Elements.HOME_CONNECTING_TEXT, time_out=60)

            # def get_to_page(self, page_name, retry=True, check_browser=True):
            #     if self.is_already_on_page(page_name):
            #         # Already on the page. Nothing to do.
            #         return
            #     else:
            #         # Click the navigation_element
            #         self.click_element()
            #
            #
            #
            #
            # def is_ready(self,
            #              element,
            #              timeout=None,
            #              check_page=True,
            #              check_is_visible=True,
            #              check_is_enabled=True,
            #              do_login=True,
            #              check_browser=True):
            #     """ Checks to see if the element is visible and enabled, switching
            #         to the appropriate page if necessary. Returns True if it is,
            #         False if it is not. If raise_exception is true (default),
            #         it raises an exception instead of returning false.
            #     """
            #     element = self._build_element_info(element)
            #     message = 'Element ' + element.name
            #     if element.page is not None:
            #         message += ' on ' + str(Page_Info.info[element.page].name) + ' page '
            #
            #
            #     self.log.debug('is_ready: ')
            #     self.log.debug('element = ' + str(element))
            #     self.log.debug('page, name, type = ' +
            #                    str(element.page) +
            #                    ', ' +
            #                    str(element.name) +
            #                    ', ' +
            #                    str(element.control_type))
            #
            #     if check_browser:
            #         if element.page:
            #             self.check_browser_state(do_login=do_login)
            #         else:
            #             self.check_browser_state(do_login=False)
            #
            #     if element.page and check_page:
            #         # Switch to the page (if necessary)
            #         self.get_to_page(element.page, check_browser=False)
            #
            #     retval = True
            #
            #     # Confirm the element is on the page
            #     if element.wait_before_check:
            #         # Wait for the element to get ready
            #         time.sleep(element.wait_before_check)
            #
            #     if not element.is_hidden:
            #         if not self.is_present(element, timeout):
            #             retval = False
            #             message += 'is not present'
            #         # Confirm it's visible
            #         elif check_is_visible:
            #             if not self.is_element_visible(element, wait_time=timeout):
            #                 retval = False
            #                 message += ' is not visible'
            #         elif check_is_enabled:
            #                 # Next make sure it's enabled
            #                 # Special handler for this since not every control supports is_enabled
            #                 if not self.is_enabled(element, timeout):
            #                     retval = False
            #                     message += ' is not enabled'
            #
            #     if retval is False:
            #         self.log.debug(message)
            #
            #     return retval
            #
            #
            #
            #
            #
