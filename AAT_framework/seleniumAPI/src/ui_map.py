""" Used to identify elements in the web UI using a standard name

    Created on November 4, 2014

    @author: hoffman_t
"""
from collections import namedtuple


class Pages(object):
    """ This class contains a list of the pages.
        They must also be added to the Page_Info class to define them.
    """
    HOME = "Home"
    NON_ADMIN_HOME = 'Non Admin Home'
    NON_ADMIN_WEB_FILE_VIEWER = 'Non Admin Web File Viewer'
    NON_ADMIN_DOWNLOAD = 'Non Admin Download'
    USERS = 'Users'
    USERS_TAB = 'UsersTab'
    GROUPS_TAB = 'GroupsTab'
    SHARES = 'Shares'
    CLOUD = 'Cloud Access'
    # Tabs under Cloud Access
    ADMIN_TAB = 'Admin Tab'
    BACKUPS = 'Backups'
    # Tabs under Backups
    USB_BACKUPS_TAB = 'Usb Backups Tab'
    REMOTE_BACKUPS_TAB = 'Remote Backups Tab'
    INTERNAL_BACKUPS_TAB = 'Internal Backups Tab'
    CLOUD_BACKUPS_TAB = 'Cloud Backups Tab'
    CAMERA_BACKUPS_TAB = 'Camera Backups Tab'
    STORAGE = 'Storage'
    # Tabs under Storage
    RAID_TAB = 'RAID Tab'
    DISK_STATUS_TAB = 'Disk Status Tab'
    ISCSI_TAB = 'iSCSI Tab'
    VOLUME_VIRTUALIZATION_TAB = 'Volume Virtualization Tab'
    APPS = 'Apps'
    # Tabs under Apps
    HTTP_DOWNLOADS_TAB = 'HTTP Download Tab'
    FTP_DOWNLOADS_TAB = 'FTP Download Tab'
    P2P_DONWNLOAD_TAB = 'P2P Download Tab'
    WEB_FILE_VIEWER_TAB = 'Web File Viewer Tab'
    SETTINGS = 'Settings'
    GENERAL_TAB = 'General Tab'
    DATE_TIME_SETTINGS = 'Date & Time Settings'
    NETWORK_TAB = 'Network Tab'
    MEDIA_TAB = 'Media Tab'
    UTILITIES_TAB = 'Utilities Tab'
    NOTIFICATIONS_TAB = 'Notifications Tab'
    FIRMWARE_UPDATE_TAB = 'Firmware Update Tab'


class ElementInfo(namedtuple('ElementInfo', ['name',
                                             'locator',
                                             'page',
                                             'verification_element',
                                             'control_type',
                                             'up_arrow',
                                             'down_arrow',
                                             'is_hidden',
                                             'label',
                                             'string_tag',
                                             'is_disabled',
                                             'wait_before_check'])):
    """ Named tuple for element information:

        The first two parameters are required for every element
            name: A friendly name for the element that is used in logs and screen output
            locator: The locator string used to find the element
        The remaining are optional:
            page: The page or tab that contains the element. This must come from the Pages class
            verification_element: Once the element is clicked, if the verification_element can be found, the click is
                                  considered to be successful. If the verification_element cannot be found, the click
                                  is considered a failure.
            control_type: One of the element types (TYPE_*) from the Elements class
            up_arrow: For an element with a scroll bar, the locator for the up arrow
            down_arrow: For an element with a scroll bar, the locator for the down arrow
            is_hidden: If True, the element is normally hidden. Used to prevent failures when calling is_ready()
            label: The label that contains the string that appears in the UI for the element
            string_tag: Similar to label, but it's a tag that contains the string for the element
            is_disabled: If True, the element is normally disabled. Used to prevent failures when calling is_ready()
            wait_before_check: If set to a positive integer, wait this number of seconds after getting to the page and
                               before checking if the element is ready.
                               Needed for elements that may have a delay before they appear.

    """
    # noinspection PyInitNewSignature
    def __new__(cls,
                name,
                locator,
                page=None,
                verification_element=None,
                control_type=None,
                up_arrow=None,
                down_arrow=None,
                is_hidden=False,
                label=None,
                string_tag=None,
                is_disabled=False,
                wait_before_check=None):
        return super(ElementInfo, cls).__new__(cls,
                                               name,
                                               locator,
                                               page,
                                               verification_element,
                                               control_type,
                                               up_arrow,
                                               down_arrow,
                                               is_hidden,
                                               label,
                                               string_tag,
                                               is_disabled,
                                               wait_before_check)


class Elements(object):
    """ This class contains UI elements and information on each element
    """

    # Element types
    TYPE_OTHER = 'Other'
    TYPE_BUTTON = 'Button'
    TYPE_LINK = 'link'
    TYPE_TEXTBOX = 'textbox'
    TYPE_PASSWORD = 'password'
    TYPE_CHECKBOX = 'checkbox'
    TYPE_DROPDOWN = 'dropdown'
    TYPE_LIST = 'list'
    TYPE_CONTAINER = 'container'
    # Attribute types
    TYPE_HREF = 'href'
    TYPE_DATAFLD = 'datafld'
    TYPE_PLACEHOLDER = 'placeholder'
    TYPE_TITLE = 'title'

    # Strings in the ID string that identify what type the element is
    # Using a list to allow having multiple identifiers per type
    id_strings = {TYPE_OTHER: [],
                  TYPE_BUTTON: ['_button'],
                  TYPE_LINK: ['_link'],
                  TYPE_TEXTBOX: ['_text'],
                  TYPE_CHECKBOX: ['_chkbox'],
                  TYPE_PASSWORD: ['_password'],
                  TYPE_DROPDOWN: ['_select_m'],
                  TYPE_LIST: ['List', '_list'],
                  TYPE_CONTAINER: ['_container'],
                  TYPE_HREF: ['_href'],
                  TYPE_DATAFLD: ['_datafld'],
                  TYPE_PLACEHOLDER: ['_placeholder'],
                  TYPE_TITLE: ['_title']}

    # Element information
    #
    # name                  A friendly name for the element (used in logs)
    #
    # locator               The locator used to find the element
    #
    # page                  The page where the element can be found
    #
    # verification_element  An element that, if present, confirms the action was successful. For example, clicking on
    #                       a button might make a field appear. Optional.
    #
    # control_type          The type of element. If not specified, the element ID is parsed, and the ending word
    #                       is used to specify the type. Optional.
    #
    #                       _button = TYPE_BUTTON
    #                       _link = TYPE_LINK
    #                       _text = TYPE_TEXTBOX
    #                       _password = TYPE_PASSWORD
    #                       _chkbox = TYPE_CHECKBOX
    #                       _select_m = TYPE_DROPDOWN
    #                       _list or List = TYPE_LIST
    #                       _container = TYPE_CONTAINER
    #                       _href = TYPE_HREF
    #                       _datafld = TYPE_DATAFLD
    #                       _placeholder = TYPE_PLACEHOLDER
    #                       _title = TYPE_TITLE
    #
    #
    # up_arrow              For a list, the element used to scroll up
    #
    #
    # down_arrow            For a list, the element used to scroll down
    #
    # is_hidden             If True, the element is hidden
    #
    # label                 An element that contains the static label associated with the element. Optional.
    #                       NOTE: It is probably best to put the label for an element first in the list. Otherwise,
    #                             it may be invalid since Python hasn't seen it yet.
    #
    # string_tag            Maps the element to the tag in the string file that contains this element's expected string
    #
    # is_disabled           If True, the element is disabled by default

    # Non-page items
    MAIN_CONTENT = ElementInfo(name='Main Content',
                               locator='main_content')
    LOGIN_DIAG = ElementInfo(name='Login Diag',
                             locator='css=div._b4')
    LOGIN_WD_LOGO = ElementInfo(name='Login WD Logo',
                                locator='css=div.wd_logo')
    LOGIN_USERNAME_TEXT = ElementInfo(name='Login Username Text',
                                      locator='//table[@id="login_tb"]/tbody/tr[1]/td/span')
    LOGIN_USERNAME = ElementInfo(name='Login Username',
                                 locator='login_userName_text',
                                 label=LOGIN_USERNAME_TEXT,
                                 control_type=TYPE_TEXTBOX)
    LOGIN_USERNAME_CANCEL = ElementInfo(name='Login Username Cancel Button',
                                        locator='css=span.deletebutton')
    LOGIN_PASSWORD_TEXT = ElementInfo(name='Login Password Text',
                                      locator='//table[@id="login_tb"]/tbody/tr[2]/td/span')
    LOGIN_PASSWORD = ElementInfo(name='Login Password',
                                 locator='login_pw_password',
                                 label=LOGIN_PASSWORD_TEXT,
                                 is_disabled=True,
                                 control_type=TYPE_PASSWORD)
    LOGIN_BUTTON = ElementInfo(name='Login Button',
                               locator='login_login_button',
                               verification_element=MAIN_CONTENT)
    LOGIN_BUTTON_WO_VER = ElementInfo(name='Login Button',
                                      locator=LOGIN_BUTTON.locator)  # login_login_button
    LOGIN_REMEMBER_ME_TEXT = ElementInfo(name='Remember Me Text',
                                         locator='css=label > span._text')
    LOGIN_REMEMBER_ME = ElementInfo(name='Remember Me',
                                    locator='css=.LightningCheckbox,login_rememberMe_chkbox',
                                    label=LOGIN_REMEMBER_ME_TEXT,
                                    control_type=TYPE_CHECKBOX)

    WEB_UI_TIMEOUT_ACCEPT = ElementInfo(name='UI Timed out',
                                        locator='unknown')

    UPDATING_STRING = ElementInfo(name='"Updating" string',
                                  locator='css=div.updateStr')

    EULA_CHECKBOX = ElementInfo(name='EULA Agreement Checkbox',
                                locator="css=.LightningCheckbox,eula_agreement_chkbox")
    EULA_CONTINUE = ElementInfo(name='EULA Continue Button',
                                locator='eula_continue_button')
    EULA_GETTING_STARTED_NEXT0 = ElementInfo(name='EULA Next',
                                             locator='gettingstarted_chgPWSave_button')
    # NEXT1 is supported on BOTH 1.05.XX and 1.06.XX
    EULA_GETTING_STARTED_NEXT1 = ElementInfo(name='EULA Next',
                                             locator='gettingstarted_next1_button')
    EULA_GETTING_STARTED_NEXT2 = ElementInfo(name='EULA Next',
                                             locator='gettingstarted_save_button')
    # CANCEL0 is the one that opens OVER EULA checkbox page and misleads the EULA CONTINUE button for visibility for 1.06.XX
    EULA_GETTING_STARTED_CANCEL0 = ElementInfo(name='EULA Cancel',
                                               locator='gettingstarted_chgPWCancel_button')
    # 1.05.XX and 1.06.XX support
    EULA_GETTING_STARTED_CANCEL1 = ElementInfo(name='EULA Cancel',
                                               locator='gettingstarted_cancel1_button')
    # CANCEL2 only Support for 1.05.XX
    EULA_GETTING_STARTED_CANCEL2 = ElementInfo(name='EULA Cancel',
                                               locator='gettingstarted_cancel2_button')
    EULA_STEP_2 = ElementInfo(name='EULA Step 2',
                              locator='css=#w_step2 > button.ButtonMarginLeft_40px.close')
    PAGE_BAR = ElementInfo(name='Page Bar',
                           locator='main_nav_container')
    PAGE_BAR_RIGHT = ElementInfo(name='Page Bar Right',
                                 locator='nav_right_link')
    PAGE_BAR_LEFT = ElementInfo(name='Page Bar Left',
                                locator='nav_left_link')
    TEST_ELEMENT = ElementInfo(name='Test element',
                               locator='test_element_button', control_type=TYPE_CONTAINER)
    # Privacy statement dialog when first time login
    PRIVACY_STATEMENT_DIALOG_TEXT = ElementInfo(name='Privacy Statement Dialog Text',
                                                locator='//div[@class="LightningStatusPanelIcon"]')
    PRIVACY_STATEMENT_DIALOG_OK = ElementInfo(name='Privacy Statement Dialog OK Button',
                                              locator='popup_ok_button')

    # Tool bar, there are 4 buttons: Device/Notification/Help/Logout
    # Device button
    TOOLBAR_DEVICE = ElementInfo(name='Device Toolbar',
                                 locator='css=#id_usb > img',
                                 page=Pages.HOME)
    TOOLBAR_DEVICE_EMPTY_DIAG = ElementInfo(name='Device Diag Without Device',
                                            locator='css=ul.ul_obj_usb > li > div')
    TOOLBAR_DEVICE_DETAIL_DIAG = ElementInfo(name='Device Detail Diag',
                                             locator='USBDetailDiag')
    TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_ARROW = ElementInfo(name='Device Diag with Device First Arrow',
                                                           locator='home_USBAlertStorageInfo1_button',
                                                           verification_element=TOOLBAR_DEVICE_DETAIL_DIAG)
    TOOLBAR_DEVICE_EJECT_DIAG = ElementInfo(name='Device Eject Diag',
                                            locator='USBUnmountDiag')
    TOOLBAR_DEVICE_NONEMPTY_DIAG_FIRST_EJECT = ElementInfo(name='Device Diag with Device First Eject',
                                                           locator='home_USBAlertStorageEject1_button',
                                                           verification_element=TOOLBAR_DEVICE_EJECT_DIAG)
    TOOLBAR_DEVICE_EJECT_DIAG_TITLE = ElementInfo(name='Device Eject Diag Title',
                                                  locator='css=div.WDLabelHeaderDialogue.WDLabelHeaderDialogueUSB_WarningIcon > span._text')
    TOOLBAR_DEVICE_EJECT_DIAG_OK = ElementInfo(name='Device Eject Diag OK Button',
                                               locator='home_USBAlertOK1_button')
    TOOLBAR_DEVICE_EJECT_DIAG_CANCEL = ElementInfo(name='Device Eject Diag Cancel Button',
                                                   locator='home_USBAlertCancel1_button')
    TOOLBAR_DEVICE_EJECT_DIAG_TEXT1 = ElementInfo(name='Device Eject Diag Text1',
                                                  locator='css=div._text > b')
    TOOLBAR_DEVICE_EJECT_DIAG_TEXT2 = ElementInfo(name='Device Eject Diag Text2',
                                                  locator='//div[2]/div[2]/div[3]/div[9]/div[3]/div/ul/li[1]')
    TOOLBAR_DEVICE_EJECT_DIAG_TEXT3 = ElementInfo(name='Device Eject Diag Text3',
                                                  locator='//div[2]/div[2]/div[3]/div[9]/div[3]/div/ul/li[2]')
    TOOLBAR_DEVICE_EJECT_DIAG_TEXT4 = ElementInfo(name='Device Eject Diag Text4',
                                                  locator='//div[2]/div[2]/div[3]/div[9]/div[3]/div/ul/li[3]')
    TOOLBAR_DEVICE_DETAIL_DIAG_TITLE = ElementInfo(name='Device Detail Diag Title',
                                                   locator='css=div.WDLabelHeaderDialogue.WDLabelHeaderDialogueUSBIcon > span._text')
    TOOLBAR_DEVICE_DETAIL_DIAG_TEXT1 = ElementInfo(name='Device Detail Diag Text1',
                                                   locator='//div[@id="USBDetailDiag"]/div[3]/table/tbody/tr[1]/td/span')
    TOOLBAR_DEVICE_DETAIL_DIAG_INFO1 = ElementInfo(name='Device Detail Diag Info1',
                                                   locator='usb_device_name')
    TOOLBAR_DEVICE_DETAIL_DIAG_TEXT2 = ElementInfo(name='Device Detail Diag Text2',
                                                   locator='//div[@id="USBDetailDiag"]/div[3]/table/tbody/tr[2]/td/span')
    TOOLBAR_DEVICE_DETAIL_DIAG_INFO2 = ElementInfo(name='Device Detail Diag Info2',
                                                   locator='usb_vendor')
    TOOLBAR_DEVICE_DETAIL_DIAG_TEXT3 = ElementInfo(name='Device Detail Diag Text3',
                                                   locator='//div[@id="USBDetailDiag"]/div[3]/table/tbody/tr[3]/td/span')
    TOOLBAR_DEVICE_DETAIL_DIAG_INFO3 = ElementInfo(name='Device Detail Diag Info3',
                                                   locator='usb_model')
    TOOLBAR_DEVICE_DETAIL_DIAG_TEXT4 = ElementInfo(name='Device Detail Diag Text4',
                                                   locator='//div[@id="USBDetailDiag"]/div[3]/table/tbody/tr[4]/td/span')
    TOOLBAR_DEVICE_DETAIL_DIAG_INFO4 = ElementInfo(name='Device Detail Diag Info4',
                                                   locator='usb_sn')
    TOOLBAR_DEVICE_DETAIL_DIAG_TEXT5 = ElementInfo(name='Device Detail Diag Text5',
                                                   locator='//div[@id="USBDetailDiag"]/div[3]/table/tbody/tr[5]/td/span')
    TOOLBAR_DEVICE_DETAIL_DIAG_INFO5 = ElementInfo(name='Device Detail Diag Info5',
                                                   locator='usb_version')
    TOOLBAR_DEVICE_DETAIL_DIAG_TEXT6 = ElementInfo(name='Device Detail Diag Text6',
                                                   locator='//div[@id="USBDetailDiag"]/div[3]/table/tbody/tr[6]/td/span')
    TOOLBAR_DEVICE_DETAIL_DIAG_INFO6 = ElementInfo(name='Device Detail Diag Info6',
                                                   locator='usb_size')
    TOOLBAR_DEVICE_DETAIL_DIAG_CLOSE = ElementInfo(name='Device Detail Diag Close Button',
                                                   locator='home_USBAlertClose1_button')
    # Notification button
    TOOLBAR_NOTIFICATION = ElementInfo(name='Notification Toolbar',
                                       locator='id_alertIcon',
                                       page=Pages.HOME)
    TOOLBAR_NOTIFICATION_EMPTY_DIAG = ElementInfo(name='Notification Empty Diag',
                                                  locator='css=ul.ul_obj_alert_no > li > div')
    TOOLBAR_NOTIFICATION_ALERT_DIAG = ElementInfo(name='Notification Alert Diag',
                                                  locator='alertDiag')
    TOOLBAR_NOTIFICATION_NONEMPTY_DIAG = ElementInfo(name='Notification Nonempty Diag',
                                                     locator='ul_alert')
    TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_TITLE = ElementInfo(name='Notification Nonempty Diag First Title',
                                                                 locator='css=span#obj_alert_content li div.a_main div.a_title')
    TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_PUBTIME = ElementInfo(name='Notification Nonempty Diag First Publish Time',
                                                                   locator='css=span#obj_alert_content li div.a_main div.a_time')
    TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_ARROW = ElementInfo(name='Notification Nonempty Diag First Arrow',
                                                                 locator='home_alertDetail1_link',
                                                                 verification_element=TOOLBAR_NOTIFICATION_ALERT_DIAG)
    TOOLBAR_NOTIFICATION_NONEMPTY_DIAG_FIRST_DELETE = ElementInfo(name='Notification Nonempty Diag First Delete Link',
                                                                  locator='home_alertDel1_link')
    TOOLBAR_NOTIFICATION_NONEMPTY_INFO = ElementInfo(name='Notification Nonempty Diag Info Text',
                                                     locator='css=span.a_info')
    TOOLBAR_NOTIFICATION_NONEMPTY_WARNING = ElementInfo(name='Notification Nonempty Diag Warning Text',
                                                        locator='css=span.a_warning')
    TOOLBAR_NOTIFICATION_NONEMPTY_CRITICAL = ElementInfo(name='Notification Nonempty Diag Critical Text',
                                                         locator='css=span.a_critical')
    TOOLBAR_NOTIFICATION_VIEW_ALL_DIAG = ElementInfo(name='Notification View All Diag',
                                                     locator='alertAllDiag')
    TOOLBAR_NOTIFICATION_VIEW_ALL = ElementInfo(name='Notification Nonempty Diag View All Button',
                                                locator='home_alertView_button',
                                                verification_element=TOOLBAR_NOTIFICATION_VIEW_ALL_DIAG)
    TOOLBAR_NOTIFICATION_VIEW_ALL_TITLE = ElementInfo(name='Notification View All Title',
                                                      locator='css=#alertAllDiag > div.WDLabelHeaderDialogue.WDLabelHeaderDialogueMailIcon > span._text')
    TOOLBAR_NOTIFICATION_VIEW_ALL_DISMISS = ElementInfo(name='Notification View All Diag Dismiss Button',
                                                        locator='home_alertDelAll_button')
    TOOLBAR_NOTIFICATION_VIEW_ALL_CLOSE = ElementInfo(name='Notification View All Diag Close Button',
                                                      locator='home_alertViewClose_button')
    TOOLBAR_NOTIFICATION_ALERT_DIAG_TITLE = ElementInfo(name='Notification Alert Diag Title',
                                                        locator='css=div.WDLabelHeaderDialogue.WDLabelHeaderDialogueMailIcon > span._text')
    TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_MSG = ElementInfo(name='Notification Alert Diag Alert Message',
                                                            locator='id_alert_msg')
    TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_DESCRIPTION = ElementInfo(name='Notification Alert Diag Alert Description',
                                                                    locator='id_alert_desc')
    TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_TIME = ElementInfo(name='Notification Alert Diag Alert Publish Time',
                                                             locator='id_alert_time')
    TOOLBAR_NOTIFICATION_ALERT_DIAG_ALERT_CODE = ElementInfo(name='Notification Alert Diag Alert Code',
                                                             locator='id_alert_error_code')
    TOOLBAR_NOTIFICATION_ALERT_DIAG_CLOSE = ElementInfo(name='Notification Alert Diag Close Button',
                                                        locator='home_alertClose_button')

    TOOLBAR_HELP_WIZARD = ElementInfo(name='Help Wizard',
                                      locator='ul_wizard')
    # Help button, there are 4 links under it
    TOOLBAR_HELP = ElementInfo(name='Help Toolbar',
                               locator='css=#id_help > img',
                               page=Pages.HOME,
                               verification_element=TOOLBAR_HELP_WIZARD)
    # Help button -> Getting Started link
    TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG = ElementInfo(name='Getting Started Step1 Diag',
                                                          locator='w_step1')
    TOOLBAR_HELP_GETTING_STARTED = ElementInfo(name='Getting Started Button',
                                               locator='home_gettingStarted_link',
                                               page=Pages.HOME,
                                               verification_element=TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
    # Elements in the step1 page
    TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG_TITLE = ElementInfo(name='Getting Started Step1 Diag Title',
                                                                locator='wDiag_title')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_CLOUD_TITLE = ElementInfo(name='Getting Started Setup Cloud Access Title',
                                                                 locator='wd2go_header')
    TOOLBAR_HELP_GETTING_STARTED_STEP1_TEXT1 = ElementInfo(name='Getting Started Step1 Text1',
                                                           locator='css=#w_step1 > div.WDLabelBodyDialogue > div._text')
    TOOLBAR_HELP_GETTING_STARTED_STEP1_TEXT2 = ElementInfo(name='Getting Started Step1 Text2',
                                                           locator='//div[@id="w_step1"]/div/div[2]/div[2]/div')
    TOOLBAR_HELP_GETTING_STARTED_STEP1_TEXT3 = ElementInfo(name='Getting Started Step1 Text3',
                                                           locator='//div[@id="w_step1"]/div/div[2]/div[2]/div[2]')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_ADMIN = ElementInfo(name='Getting Started Cloud Access Admin Account',
                                                           locator='css=div.fullName')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_ADMIN_EMAIL = ElementInfo(name='Getting Started Cloud Access Admin Email',
                                                                 locator='css=div.emailInfo')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME = ElementInfo(name='Getting Started Setup Cloud Access Username',
                                                              locator='gettingstarted_userName_text',
                                                              control_type=TYPE_TEXTBOX)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME = ElementInfo(name='Getting Started Setup Cloud Access Firstname',
                                                               locator='gettingstarted_firstName_text',
                                                               control_type=TYPE_TEXTBOX)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME = ElementInfo(name='Getting Started Setup Cloud Access Lastname',
                                                              locator='gettingstarted_lastName_text',
                                                              control_type=TYPE_TEXTBOX)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL = ElementInfo(name='Getting Started Setup Cloud Access Email',
                                                           locator='gettingstarted_email_text',
                                                           control_type=TYPE_TEXTBOX)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME_DELETE = ElementInfo(name='Getting Started Setup Cloud Access Username Delete Button',
                                                                     locator='css=div.accountName > span.deleteicon > span.deletebutton')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME_DELETE = ElementInfo(name='Getting Started Setup Cloud Access Firstname Delete Button',
                                                                      locator='css=div.firstName > span.deleteicon > span.deletebutton')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME_DELETE = ElementInfo(name='Getting Started Setup Cloud Access Lastname Delete Button',
                                                                     locator='css=div.lastName > span.deleteicon > span.deletebutton')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL_DELETE = ElementInfo(name='Getting Started Setup Cloud Access Email Delete Pic Button',
                                                                  locator='css=div.email > span.deleteicon > span.deletebutton')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_USER1_FULLNAME = ElementInfo(name='Getting Started Setup User1 Full Name',
                                                                    locator='//div[@id=\'create_device_user_list\']/ul/li[2]/div')
    TOOLBAR_HELP_GETTING_STARTED_SETUP_USER1_EMAIL = ElementInfo(name='Getting Started Setup User1 Email',
                                                                 locator='//div[@id=\'create_device_user_list\']/ul/li[2]/div[2]')
    # Default texts are placeholder attribute, but the locators are the same
    TOOLBAR_HELP_GETTING_STARTED_SETUP_USERNAME_TEXT = ElementInfo(name='Getting Started Setup Cloud Access Username Text',
                                                                   locator='gettingstarted_userName_text',
                                                                   control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_FIRSTNAME_TEXT = ElementInfo(name='Getting Started Setup Cloud Access Firstname Text',
                                                                    locator='gettingstarted_firstName_text',
                                                                    control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_LASTNAME_TEXT = ElementInfo(name='Getting Started Setup Cloud Access Lastname Text',
                                                                   locator='gettingstarted_lastName_text',
                                                                   control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_EMAIL_TEXT = ElementInfo(name='Getting Started Setup Cloud Access Email Text',
                                                                locator='gettingstarted_email_text',
                                                                control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_SETUP_USER_SAVE = ElementInfo(name='Getting Started Cloud Access Create User Save Button',
                                                               locator='gettingstarted_addUserSave_button')
    # Put step 2 diag here for below verification element
    TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG = ElementInfo(name='Getting Started Step2 Diag',
                                                          locator='w_step2')
    TOOLBAR_HELP_GETTING_STARTED_NEXT1 = ElementInfo(name='Getting Started Next Button1',
                                                     locator='gettingstarted_next1_button',
                                                     verification_element=TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
    TOOLBAR_HELP_GETTING_STARTED_CANCEL1 = ElementInfo(name='Getting Started Cancel Button1',
                                                       locator='gettingstarted_cancel1_button')
    # Elements in the step2 page
    TOOLBAR_HELP_GETTING_STARTED_STEP2_TITLE = ElementInfo(name='Getting Started Step2 Title',
                                                           locator='wDiag_title2')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TITLE = ElementInfo(name='Getting Started Step2 Param1 Title',
                                                                  locator='css=div._text.WDLabelHeader2')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1 = ElementInfo(name='Getting Started Step2 Param1 Text1',
                                                                  locator='css=td.tdfield_250 > span._text')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT1_TIP = ElementInfo(name='Getting Started Step2 Param1 Text1 Tip',
                                                                      locator='tip_autofw',
                                                                      control_type=TYPE_TITLE)
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2 = ElementInfo(name='Getting Started Step2 Param1 TEXT2',
                                                                  locator='//div[@id="w_step2"]/div/table/tbody/tr[2]/td/span')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM1_TEXT2_TIP = ElementInfo(name='Getting Started Step2 Param1 Text2 Tip',
                                                                      locator='tip_imp_info',
                                                                      control_type=TYPE_TITLE)
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM2_TITLE = ElementInfo(name='Getting Started Step2 Param2 Title',
                                                                  locator='//div[@id="w_step2"]/div/div[3]')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_PARAM2_TEXT1 = ElementInfo(name='Getting Started Step2 Param2 Text',
                                                                  locator='css=#w_step2 > div.WDLabelBodyDialogue > div._text.tdfield_padding_top_14')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME = ElementInfo(name='Getting Started Step2 Param2 Register FirstName',
                                                                        locator='gettingstarted_rFirstName_text')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME = ElementInfo(name='Getting Started Step2 Param2 Register LastName',
                                                                       locator='gettingstarted_rLastName_text')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL = ElementInfo(name='Getting Started Step2 Param2 Register EMail',
                                                                    locator='gettingstarted_rMail_text')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_TEXT = ElementInfo(name='Getting Started Step2 Param2 Register FirstName Text',
                                                                             locator='gettingstarted_rFirstName_text',
                                                                             control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_TEXT = ElementInfo(name='Getting Started Step2 Param2 Register LastName Text',
                                                                            locator='gettingstarted_rLastName_text',
                                                                            control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_TEXT = ElementInfo(name='Getting Started Step2 Param2 Register EMail Text',
                                                                         locator='gettingstarted_rMail_text',
                                                                         control_type=TYPE_PLACEHOLDER)
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_FIRSTNAME_DELETE = ElementInfo(name='Getting Started Step2 Param2 Register Firstname Delete Button',
                                                                               locator='css=div.WDLabelBodyDialogue > span.deleteicon > span.deletebutton')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_LASTNAME_DELETE = ElementInfo(name='Getting Started Step2 Param2 Register Lastname Delete Button',
                                                                              locator='//div[@id="w_step2"]/div/span[2]/span[2]')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER_EMAIL_DELETE = ElementInfo(name='Getting Started Step2 Param2 Register Email Delete Pic Button',
                                                                           locator='//div[@id="w_step2"]/div/span[3]/span[2]')
    TOOLBAR_HELP_GETTING_STARTED_STEP2_REGISTER = ElementInfo(name='Getting Started Step2 Param2 Register Button',
                                                              locator='gettingstarted_Register_button')
    TOOLBAR_HELP_GETTING_STARTED_BACK2 = ElementInfo(name='Getting Started Back Button2',
                                                     locator='gettingstarted_back2_button',
                                                     verification_element=TOOLBAR_HELP_GETTING_STARTED_STEP1_DIAG)
    TOOLBAR_HELP_GETTING_STARTED_CANCEL2 = ElementInfo(name='Getting Started Cancel Button2',
                                                       locator='gettingstarted_cancel2_button')
    # Put step 3 diag here for below verification element
    TOOLBAR_HELP_GETTING_STARTED_STEP3_DIAG = ElementInfo(name='Getting Started Step3 Diag',
                                                          locator='w_step3')
    TOOLBAR_HELP_GETTING_STARTED_NEXT2 = ElementInfo(name='Getting Started Next Button2',
                                                     locator='gettingstarted_next2_button',
                                                     verification_element=TOOLBAR_HELP_GETTING_STARTED_STEP3_DIAG)
    # Step 3
    TOOLBAR_HELP_GETTING_STARTED_STEP3_TEXT1 = ElementInfo(name='Getting Started Step3 Text1',
                                                           locator='w3Diag_desc')
    TOOLBAR_HELP_GETTING_STARTED_HELP_UPDATE_LABEL = ElementInfo(name='Getting Started Help Update Link Label',
                                                                 locator='css=#gettingstarted_update_link > span._text')
    TOOLBAR_HELP_GETTING_STARTED_HELP_UPDATE = ElementInfo(name='Getting Started Help Update Link',
                                                           locator='gettingstarted_update_link',
                                                           control_type=TYPE_HREF,
                                                           label=TOOLBAR_HELP_GETTING_STARTED_HELP_UPDATE_LABEL)
    TOOLBAR_HELP_GETTING_STARTED_HELP_BACKUP_LABEL = ElementInfo(name='Getting Started Help Backup Link Label',
                                                                 locator='css=#gettingstarted_backup_link > span._text')
    TOOLBAR_HELP_GETTING_STARTED_HELP_BACKUP = ElementInfo(name='Getting Started Help Backup Link',
                                                           locator='gettingstarted_backup_link',
                                                           control_type=TYPE_HREF,
                                                           label=TOOLBAR_HELP_GETTING_STARTED_HELP_BACKUP_LABEL)
    TOOLBAR_HELP_GETTING_STARTED_HELP_MOBILE_LABEL = ElementInfo(name='Getting Started Help Mobile Link Label',
                                                                 locator='css=#gettingstarted_mobile_link > span._text')
    TOOLBAR_HELP_GETTING_STARTED_HELP_MOBILE = ElementInfo(name='Getting Started Help Mobile Link',
                                                           locator='gettingstarted_mobile_link',
                                                           control_type=TYPE_HREF,
                                                           label=TOOLBAR_HELP_GETTING_STARTED_HELP_MOBILE_LABEL)
    TOOLBAR_HELP_GETTING_STARTED_BACK3 = ElementInfo(name='Getting Started Back Button3',
                                                     locator='gettingstarted_back3_button',
                                                     verification_element=TOOLBAR_HELP_GETTING_STARTED_STEP2_DIAG)
    TOOLBAR_HELP_GETTING_STARTED_CANCEL3 = ElementInfo(name='Getting Started Cancel Button3',
                                                       locator='gettingstarted_cancel3_button')
    TOOLBAR_HELP_GETTING_STARTED_FINISH = ElementInfo(name='Getting Started Finish Button',
                                                      locator='gettingstarted_save_button')
    # Help button -> Help link
    TOOLBAR_HELP_HELP_DIAG = ElementInfo(name='Help DiaG',
                                         locator='HelpDiag')
    TOOLBAR_HELP_HELP = ElementInfo(name='Help Link',
                                    locator='home_help_link',
                                    page=Pages.HOME,
                                    verification_element=TOOLBAR_HELP_HELP_DIAG)
    TOOLBAR_HELP_HELP_MAIN_FRAME = ElementInfo(name='Help Main Frame',
                                               locator='help_iframe')
    TOOLBAR_HELP_HELP_TOOLBAR_FRAME = ElementInfo(name='Help Toolbar Frame',
                                                  locator='toolbar')
    TOOLBAR_HELP_HELP_CONTENT = ElementInfo(name='Help Content Tab',
                                            locator='css=img[alt="Contents"]')
    TOOLBAR_HELP_HELP_SEARCH = ElementInfo(name='Help Search Tab',
                                           locator='css=img[alt="Search"]')
    TOOLBAR_HELP_HELP_CONTENT_FRAME = ElementInfo(name='Help Left Content Frame',
                                                  locator='//frame[contains(@src, "whskin_frmset01.htm")]')
    TOOLBAR_HELP_HELP_LEFT_FRAME_L1 = ElementInfo(name='Help Left Frame Layer1',
                                                  locator='minibar_navpane')
    TOOLBAR_HELP_HELP_LEFT_FRAME_L2 = ElementInfo(name='Help Left Frame Layer2',
                                                  locator='navpane')
    TOOLBAR_HELP_HELP_LEFT_FRAME_L3 = ElementInfo(name='Help Left Frame Layer3',
                                                  locator='tocIFrame')
    TOOLBAR_HELP_HELP_LEFT_CONTENT = ElementInfo(name='Help Left Frame Content',
                                                 locator='tocDiv')
    TOOLBAR_HELP_HELP_LEFT_SEARCH = ElementInfo(name='Help Left Frame Search',
                                                locator='ftsDiv')
    TOOLBAR_HELP_HELP_RIGHT_FRAME_L1 = ElementInfo(name='Help Right Frame Layer1',
                                                   locator='topic')
    TOOLBAR_HELP_HELP_DIAG_OK = ElementInfo(name='Help Diag OK button',
                                            locator='help_ok_button')
    TOOLBAR_HELP_HELP_PAGE_TITLE = ElementInfo(name='Help Page Title',
                                               locator='css=h1')
    # Help button -> Support link
    TOOLBAR_HELP_SUPPORT_DIAG = ElementInfo(name='Help Support Diag',
                                            locator='topDiag')
    TOOLBAR_HELP_SUPPORT = ElementInfo(name='Help Support Button',
                                       locator='home_support_link',
                                       page=Pages.HOME,
                                       verification_element=TOOLBAR_HELP_SUPPORT_DIAG)
    TOOLBAR_HELP_SUPPORT_DIAG_TITLE = ElementInfo(name='Help Support Diag Title',
                                                  locator='topDiag_title')
    TOOLBAR_HELP_SUPPORT_PARAM1_TITLE = ElementInfo(name='Help Support Diag Param1 Title',
                                                    locator='css=h2 > span._text')
    TOOLBAR_HELP_SUPPORT_PARAM1_TEXT1 = ElementInfo(name='Help Support Diag Param1 Text1',
                                                    locator='css=div#supportDiag div.WDLabelBodyDialogue.sbody div._text.field_top')
    TOOLBAR_HELP_SUPPORT_PARAM1_CHECKBOX1_LABEL = ElementInfo(name='Help Support Diag Param1 Checkbox1 Label',
                                                              locator='css=#request_automated > table > tbody > tr > td.tdfield_padding.tdfield_padding_left_10 > span._text')
    TOOLBAR_HELP_SUPPORT_PARAM1_CHECKBOX1 = ElementInfo(name='Help Support Diag Param1 Checkbox1',
                                                        locator='css=.LightningCheckbox,support_requestSupport_chkbox',
                                                        label=TOOLBAR_HELP_SUPPORT_PARAM1_CHECKBOX1_LABEL,
                                                        control_type=TYPE_CHECKBOX,
                                                        is_hidden=True)
    TOOLBAR_HELP_SUPPORT_REQUEST_SUPPORT = ElementInfo(name='Help Support Diag Request Support Button',
                                                       locator='support_requestSupport_button')
    TOOLBAR_HELP_SUPPORT_PRIVACY_POLICY_LABEL = ElementInfo(name='Help Support Privacy Policy Link Label',
                                                            locator='css=#support_privacyPpolicy_link > span._text')
    TOOLBAR_HELP_SUPPORT_PRIVACY_POLICY = ElementInfo(name='Help Support Privacy Policy Link',
                                                      locator='support_privacyPpolicy_link',
                                                      label=TOOLBAR_HELP_SUPPORT_PRIVACY_POLICY_LABEL,
                                                      control_type=TYPE_HREF)
    TOOLBAR_HELP_SUPPORT_PARAM2_TITLE = ElementInfo(name='Help Support Diag Param2 Title',
                                                    locator='css=div.WDLabelBodyDialogue.sbody > h2 > span._text')
    TOOLBAR_HELP_SUPPORT_PARAM2_TEXT1 = ElementInfo(name='Help Support Diag Param2 Text1',
                                                    locator='css=div.WDLabelBodyDialogue.sbody > div._text.field_top')
    TOOLBAR_HELP_SUPPORT_CREATE_AND_SAVE = ElementInfo(name='Help Support Diag Create and Save Button',
                                                       locator='support_create_button')
    TOOLBAR_HELP_SUPPORT_PARAM3_TITLE = ElementInfo(name='Help Support Diag Param3 Title',
                                                    locator='css=#support_productImprovement > h2 > span._text')
    TOOLBAR_HELP_SUPPORT_PARAM3_TEXT1 = ElementInfo(name='Help Support Diag Param3 Text1',
                                                    locator='css=div.field_top > div._text')
    TOOLBAR_HELP_SUPPORT_PARAM3_TEXT2 = ElementInfo(name='Help Support Diag Param3 Text2',
                                                    locator='//ul[@id="UL14px"]/li[1]')
    TOOLBAR_HELP_SUPPORT_PARAM3_TEXT3 = ElementInfo(name='Help Support Diag Param3 Text3',
                                                    locator='//ul[@id="UL14px"]/li[2]')
    TOOLBAR_HELP_SUPPORT_PARAM3_TEXT4 = ElementInfo(name='Help Support Diag Param3 Text4',
                                                    locator='//ul[@id="UL14px"]/li[3]')
    TOOLBAR_HELP_SUPPORT_PARAM3_TEXT5 = ElementInfo(name='Help Support Diag Param3 Text5',
                                                    locator='css=#support_productImprovement > table > tbody > tr > td > span._text')
    TOOLBAR_HELP_SUPPORT_PARAM4_TITLE = ElementInfo(name='Help Support Diag Param4 Title',
                                                    locator='//div[@id="supportDiag"]/div/h2[2]/span')
    TOOLBAR_HELP_SUPPORT_DOC_LABEL = ElementInfo(name='Help Support Doc Link Label',
                                                 locator='css=#support_doc_link > span._text')
    TOOLBAR_HELP_SUPPORT_DOC = ElementInfo(name='Help Support Doc Link',
                                           locator='support_doc_link',
                                           label=TOOLBAR_HELP_SUPPORT_DOC_LABEL,
                                           control_type=TYPE_HREF)
    TOOLBAR_HELP_SUPPORT_FAQ_LABEL = ElementInfo(name='Help Support FAQ Link Label',
                                                 locator='css=#support_faq_link > span._text')
    TOOLBAR_HELP_SUPPORT_FAQ = ElementInfo(name='Help Support FAQ Link',
                                           locator='support_faq_link',
                                           label=TOOLBAR_HELP_SUPPORT_FAQ_LABEL,
                                           control_type=TYPE_HREF)
    TOOLBAR_HELP_SUPPORT_FORUM_LABEL = ElementInfo(name='Help Support Forum Link Label',
                                                   locator='css=#support_forum_link > span._text')
    TOOLBAR_HELP_SUPPORT_FORUM = ElementInfo(name='Help Support Forum Link',
                                             locator='support_forum_link',
                                             label=TOOLBAR_HELP_SUPPORT_FORUM_LABEL,
                                             control_type=TYPE_HREF)
    TOOLBAR_HELP_SUPPORT_CONTACTS_LABEL = ElementInfo(name='Help Support Contacts Link Label',
                                                      locator='css=#support_contacts_link > span._text')
    TOOLBAR_HELP_SUPPORT_CONTACTS = ElementInfo(name='Help Support Contacts Link',
                                                locator='support_contacts_link',
                                                label=TOOLBAR_HELP_SUPPORT_CONTACTS_LABEL,
                                                control_type=TYPE_HREF)
    TOOLBAR_HELP_SUPPORT_DIAG_OK = ElementInfo(name='Help Support Diag OK Button',
                                               locator='support_ok_button')
    # Help button -> About link
    TOOLBAR_HELP_ABOUT_DIAG = ElementInfo(name='About Diag',
                                          locator='topDiag2')
    TOOLBAR_HELP_ABOUT = ElementInfo(name='Help About Button',
                                     locator='home_about_link',
                                     page=Pages.HOME,
                                     verification_element=TOOLBAR_HELP_ABOUT_DIAG)
    TOOLBAR_HELP_ABOUT_DIAG_LOGO = ElementInfo(name='About Diag Logo',
                                               locator='css=#topDiag2_title > div.wd_logo')
    TOOLBAR_HELP_ABOUT_DIAG_DEV = ElementInfo(name='About Diag Dev Pic',
                                              locator='css=p > img')
    TOOLBAR_HELP_ABOUT_DIAG_FIRMWARE_TEXT = ElementInfo(name='About Diag Firmware Text',
                                                        locator='fw_version')
    TOOLBAR_HELP_ABOUT_DIAG_COPYRIGHT_TEXT = ElementInfo(name='About Diag Copyright Text',
                                                         locator='css=#aboutDiag > div.WDLabelBodyDialogue > div._text')
    TOOLBAR_HELP_ABOUT_DIAG_COPYRIGHT_LABEL = ElementInfo(name='About Copyright Link',
                                                          locator='about_copyright_link')
    TOOLBAR_HELP_ABOUT_DIAG_COPYRIGHT = ElementInfo(name='About Copyright Link',
                                                    locator='about_copyright_link',
                                                    control_type=TYPE_HREF,
                                                    label=TOOLBAR_HELP_ABOUT_DIAG_COPYRIGHT_LABEL)
    TOOLBAR_HELP_ABOUT_DIAG_GPL_LABEL = ElementInfo(name='About GPL Link Label',
                                                    locator='about_gpl_link')
    TOOLBAR_HELP_ABOUT_DIAG_GPL = ElementInfo(name='About GPL Link',
                                              locator='about_gpl_link',
                                              control_type=TYPE_HREF,
                                              label=TOOLBAR_HELP_ABOUT_DIAG_GPL_LABEL)
    TOOLBAR_HELP_ABOUT_DIAG_OK = ElementInfo(name='About OK Button',
                                             locator='about_ok_button')
    # Logout Button
    TOOLBAR_LOGOUT_TABLE = ElementInfo(name='Logout Table',
                                       locator='//div[@id="pages"]/div/div/table/tbody/tr/td[4]/div/ul/li/ul/li/div')
    TOOLBAR_LOGOUT = ElementInfo(name='Logout Toolbar',
                                 locator='css=#id_logout > img',
                                 page=Pages.HOME,
                                 verification_element=TOOLBAR_LOGOUT_TABLE)
    TOOLBAR_LOGOUT_TITLE = ElementInfo(name='Logout Table Title',
                                       locator='css=li > div > table > tbody > tr > td')
    TOOLBAR_LOGOUT_SHUTDOWN = ElementInfo(name='Logout shutdown link',
                                          locator='home_shutdown_link')
    TOOLBAR_LOGOUT_SHUTDOWN_WARNING_DIAG = ElementInfo(name='Logout shutdown warning diag',
                                                       locator='error_dialog_message_list')
    TOOLBAR_LOGOUT_SHUTDOWN_DIAG = ElementInfo(name='Device is shutting down',
                                               locator='css=div#shutdownDiag.WDLabelDiag > div.WDLabelBodyDialogue')
    TOOLBAR_LOGOUT_SHUTDOWN_CANCEL = ElementInfo(name='Logout shutdown cancel',
                                                 locator='popup_close_button')
    TOOLBAR_LOGOUT_SHUTDOWN_OK = ElementInfo(name='Logout shutdown ok',
                                             locator='popup_apply_button')
    TOOLBAR_LOGOUT_REBOOT = ElementInfo(name='Logout reboot link',
                                        locator='home_reboot_link')
    TOOLBAR_LOGOUT_REBOOT_WARNING_DIAG = ElementInfo(name='Logout reboot warning diag',
                                                     locator='error_dialog_message_list')
    TOOLBAR_LOGOUT_REBOOT_DIAG = ElementInfo(name='Device is rebooting',
                                             locator='css=div#rebootDiag.WDLabelDiag > div.WDLabelBodyDialogue')
    TOOLBAR_LOGOUT_REBOOT_CANCEL = ElementInfo(name='Logout reboot cancel',
                                               locator='popup_close_button')
    TOOLBAR_LOGOUT_REBOOT_OK = ElementInfo(name='Logout reboot ok',
                                           locator='popup_apply_button')
    TOOLBAR_LOGOUT_LOGOUT = ElementInfo(name='Logout logout link',
                                        locator='home_logout_link',
                                        verification_element=LOGIN_BUTTON)
    # Home page
    HOME_CLOUD_ACCESS_DIAG = ElementInfo(name='Cloud Access Diag',
                                         locator='cloudAddAccessDiag')
    HOME_CLOUD_ACCESS = ElementInfo(name='Cloud Access',
                                    locator='home_cloud_link',
                                    page=Pages.HOME,
                                    verification_element=HOME_CLOUD_ACCESS_DIAG)
    HOME_CLOUD_ACCESS_DIAG_SELECT_ALLOPTIONS = ElementInfo(name='Cloud Access Diag Select List All Options',
                                                           locator='css=div#cloud_user_select.select_menu ul li.option_list ul.ul_obj')
    HOME_CLOUD_ACCESS_DIAG_SELECT = ElementInfo(name='Cloud Access Diag Select List',
                                                locator='home_cloudUser_select',
                                                verification_element=HOME_CLOUD_ACCESS_DIAG_SELECT_ALLOPTIONS)
    HOME_CLOUD_ACCESS_DIAG_SELECT_OPTION1 = ElementInfo(name='Cloud Access Diag Select List Option 1',
                                                        locator='home_cloudUserUl0_select')
    HOME_CLOUD_ACCESS_DIAG_SELECT_OPTION2 = ElementInfo(name='Cloud Access Diag Select List Option 2',
                                                        locator='home_cloudUserUl1_select')
    HOME_CLOUD_ACCESS_DIAG_GETCODE_DIAG = ElementInfo(name='Cloud Access Diag Get Code Diag',
                                                      locator='cloudGetCodeDiag')
    HOME_CLOUD_ACCESS_DIAG_GETCODE = ElementInfo(name='Cloud Access Diag Get Code Button',
                                                 locator='home_cloudGetCode_button',
                                                 verification_element=HOME_CLOUD_ACCESS_DIAG_GETCODE_DIAG)
    HOME_CLOUD_ACCESS_DIAG_GETCODE_DIAG_OK = ElementInfo(name='Cloud Access Diag Get Code Diag OK Button',
                                                         locator='cloud_getCodeOK_button')
    HOME_DEVICE_DIAGNOSTICS_DIAG = ElementInfo(name='Device Diagnostics Diag',
                                               locator='smartDiag')
    HOME_DEVICE_DIAGNOSTICS_ARROW = ElementInfo(name='Device Diagnostics Arrow',
                                                locator='smart_info',
                                                page=Pages.HOME,
                                                verification_element=HOME_DEVICE_DIAGNOSTICS_DIAG)
    HOME_DEVICE_DIAGNOSTICS_DIAG_OVERLAY = ElementInfo(name='Device Diagnostics Diag Overlay (Health Status)',
                                                       locator='home_diagnosticsFlexigrid')
    HOME_DEVICE_DIAGNOSTICS_DIAG_CLOSE = ElementInfo(name='Device Diagnostics Diag Close Button',
                                                     locator='home_diagnosticsClose1_button')
    HOME_DEVICE_FIRMWARE_DIAG = ElementInfo(name='Device Firmware Diag',
                                            locator='FWDiag')
    HOME_DEVICE_FIRMWARE_ARROW = ElementInfo(name='Device Firmware Arrow',
                                             locator='css=#home_fw_link > #home_fw_link',
                                             page=Pages.HOME,
                                             verification_element=HOME_DEVICE_FIRMWARE_DIAG)
    HOME_DEVICE_FIRMWARE_DIAG_OK = ElementInfo(name='Device Firmware Diag OK Button',
                                               locator='settings_fwChkUpdateOk_button')
    HOME_DEVICE_CAPACITY_FREE_SPACE = ElementInfo(name='Free Space Capacity',
                                                  locator='home_b4_free',
                                                  page=Pages.HOME)
    # Device Activity main diag
    HOME_DEVICE_DIAG = ElementInfo(name='Device Diag',
                                   locator='active_set')
    # CPU Pic in Network Column
    HOME_NETWORK_CPU_DIAG = ElementInfo(name='CPU Diag',
                                        locator='id_cpu')
    HOME_NETWORK_CPU_PIC = ElementInfo(name='CPU Pic',
                                       locator='home_cpuDetail_link',
                                       page=Pages.HOME,
                                       verification_element=HOME_NETWORK_CPU_DIAG)
    HOME_NETWORK_CPU_DIAG_OVERLAY = ElementInfo(name='CPU Diag Overlay',
                                                locator='css=#placeholder > canvas.overlay')
    HOME_NETWORK_CPU_DIAG_BACK = ElementInfo(name='CPU Diag Back Button',
                                             locator='home_deviceActivityCpuBack_button',
                                             verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_CPU_DIAG_BACK_TEXT = ElementInfo(name='CPU Diag Back Text',
                                                  locator='css=div.edit_detail > span._text',
                                                  verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_CPU_DIAG_CLOSE = ElementInfo(name='CPU Diag Close Button',
                                              locator='home_deviceActivityCpuClose_button')
    # RAM Pic in Network Column
    HOME_NETWORK_RAM_DIAG = ElementInfo(name='Ram Diag',
                                        locator='id_memory')
    HOME_NETWORK_RAM_PIC = ElementInfo(name='Ram Pic',
                                       locator='home_ramDetail_link',
                                       page=Pages.HOME,
                                       verification_element=HOME_NETWORK_RAM_DIAG)
    HOME_NETWORK_RAM_DIAG_OVERLAY = ElementInfo(name='Ram Diag Overlay',
                                                locator='css=#placeholder_memory > canvas.overlay')
    HOME_NETWORK_RAM_DIAG_BACK = ElementInfo(name='Ram Diag Back Button',
                                             locator='home_deviceActivityMemoryBack_button',
                                             verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_RAM_DIAG_BACK_TEXT = ElementInfo(name='Ram Diag Back Text',
                                                  locator='css=#id_memory > div.WDLabelBodyDialogue > div.edit_detail > span._text',
                                                  verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_RAM_DIAG_CLOSE = ElementInfo(name='Ram Diag Close Button',
                                              locator='home_deviceActivityMemoryClose_button')
    # Network Pic in Network Column
    HOME_NETWORK_NETWORK_DIAG = ElementInfo(name='Network Diag',
                                            locator='id_network')
    HOME_NETWORK_NETWORK_PIC = ElementInfo(name='Network Pic',
                                           locator='css=#home_networkActivity_link > canvas.overlay',
                                           page=Pages.HOME,
                                           verification_element=HOME_NETWORK_NETWORK_DIAG)
    HOME_NETWORK_NETWORK_DIAG_OVERLAY = ElementInfo(name='Network Diag Overlay',
                                                    locator='css=#placeholder_network > canvas.overlay')
    HOME_NETWORK_NETWORK_DIAG_BACK = ElementInfo(name='Network Diag Back Button',
                                                 locator='home_deviceActivityNetworkBack_button',
                                                 verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_NETWORK_DIAG_BACK_TEXT = ElementInfo(name='Network Diag Back Text',
                                                      locator='css=#id_network > div.WDLabelBodyDialogue > div.edit_detail > span._text',
                                                      verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_NETWORK_DIAG_CLOSE = ElementInfo(name='Network Diag Close Button',
                                                  locator='home_deviceActivityNetworkClose_button')
    HOME_NETWORK_PROCESS_DIAG = ElementInfo(name='Process Diag',
                                            locator='id_process')
    HOME_NETWORK_PROCESS_DIAG_BACK = ElementInfo(name='Process Diag Back',
                                                 locator='home_deviceActivityProcessBack_button',
                                                 verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_PROCESS_DIAG_BACK_TEXT = ElementInfo(name='Process Diag Back Text',
                                                      locator='css=span > span._text',
                                                      verification_element=HOME_DEVICE_DIAG)
    HOME_NETWORK_PROCESS_DIAG_CLOSE = ElementInfo(name='Process Diag Close',
                                                  locator='home_deviceActivityProcessClose_button')
    HOME_NETWORK_ARROW = ElementInfo(name='Home Network Arrow',
                                     locator='home_networkActivityDetail_link',
                                     page=Pages.HOME,
                                     verification_element=HOME_DEVICE_DIAG)
    HOME_DEVICE_DIAG_CPU_ARROW = ElementInfo(name='Device Diag CPU Arrow',
                                             locator='home_cpu_link',
                                             verification_element=HOME_NETWORK_CPU_DIAG)
    HOME_DEVICE_DIAG_RAM_ARROW = ElementInfo(name='Device Diag RAM Arrow',
                                             locator='home_memory_link',
                                             verification_element=HOME_NETWORK_RAM_DIAG)
    HOME_DEVICE_DIAG_NETWORK_ARROW = ElementInfo(name='Device Diag Network Arrow',
                                                 locator='home_network_link',
                                                 verification_element=HOME_NETWORK_NETWORK_DIAG)
    HOME_DEVICE_DIAG_PROCESS_ARROW = ElementInfo(name='Device Diag Process Arrow',
                                                 locator='home_process_link',
                                                 verification_element=HOME_NETWORK_PROCESS_DIAG)
    HOME_CLOUD_DIAG = ElementInfo(name='Cloud Diag',
                                  locator='CloudDiag')
    HOME_CLOUD_ARROW = ElementInfo(name='Cloud Arrow',
                                   locator='home_cloud_link',
                                   page=Pages.HOME,
                                   verification_element=HOME_CLOUD_DIAG)
    HOME_CLOUD_DIAG_CANCEL = ElementInfo(name='Cloud Diag Cancel Button',
                                         locator='home_cloudCancel_button')
    HOME_USER_DIAG = ElementInfo(name='User Diag',
                                 locator='createUserDiag')
    HOME_USER_ARROW = ElementInfo(name='User Arrow',
                                  locator='home_user_link',
                                  page=Pages.HOME,
                                  verification_element=HOME_USER_DIAG)
    HOME_USER_DIAG_CANCEL = ElementInfo(name='User Diag Cancel Button',
                                        locator='users_addUserCancel1_button')
    HOME_APPS_DIAG = ElementInfo(name='Home Apps Diag',
                                 locator='AppsDiag')
    HOME_APPS_ARROW = ElementInfo(name='Home Apps Arrow',
                                  locator='home_apps_link',
                                  page=Pages.HOME,
                                  verification_element=HOME_APPS_DIAG)
    HOME_APPS_DIAG_APP_NAME1 = ElementInfo(name='Home Apps Diag App Name 1',
                                           locator='home_appsName0_value')
    HOME_APPS_DIAG_CLOSE = ElementInfo(name='Home Apps Diag Close Button',
                                       locator='home_appsClose1_button')
    HOME_APPS_DIAG_DETAIL_CLOSE = ElementInfo(name='Home Apps Diag App Detail Link Close Button',
                                              locator='home_appsClose2_button')
    HOME_APPS_DIAG_APP_DETAIL1 = ElementInfo(name='Home Apps Diag App Detail Link 1',
                                             locator='home_appsDetail0_link',
                                             verification_element=HOME_APPS_DIAG_DETAIL_CLOSE)
    HOME_APPS_DIAG_APP_DETAIL_APP_NAME = ElementInfo(name='Home Apps Diag App Detail Link APP Name',
                                                     locator='apps_details_name')
    # Users page
    USERS_BUTTON = ElementInfo(name='Users Button',
                               locator='users_user_button',
                               page=Pages.USERS)
    USERS_HELP_MANAGING = ElementInfo(name='User Help Managing Users',
                                      locator='css=a.bumper_help_link.about_users > span._text',
                                      page=Pages.USERS)
    USERS_HELP_ADDING = ElementInfo(name='User Help Adding a User',
                                    locator='css=a.bumper_help_link.adding_users > span._text',
                                    page=Pages.USERS)
    USERS_HELP_CHANGING = ElementInfo(name='User Help Changing Access to a Share',
                                      locator='css=a.bumper_help_link.user_change_access > span._text',
                                      page=Pages.USERS)
    USERS_ADD_DIAG = ElementInfo(name='User Add Diag',
                                 locator='createUserDiag')
    USERS_ADD_LINK = ElementInfo(name='User Add Link',
                                 locator='users_createUser_link',
                                 page=Pages.USERS,
                                 verification_element=USERS_ADD_DIAG)
    USERS_ADD_DIAG_CANCEL = ElementInfo(name='User Add Diag Cancel Button',
                                        locator='users_addUserCancel1_button')
    USERS_ADD_MULTIPLE_USERS_DIAG = ElementInfo(name='User Add Multiple User Diag',
                                                locator='importDiag')
    USERS_ADD_MULTIPLE_USERS_LINK = ElementInfo(name='User Add Multiple User Link',
                                                locator='users_addMultiUser_link',
                                                page=Pages.USERS,
                                                verification_element=USERS_ADD_MULTIPLE_USERS_DIAG)
    USERS_ADD_MULTIPLE_USERS_DIAG_BACK = ElementInfo(name='User Add Multiple User Diag Back Button',
                                                     locator='users_batchBack2_button')
    USERS_ADD_MULTIPLE_USERS_DIAG_NEXT1 = ElementInfo(name='User Add Multiple User Diag Next Button 1',
                                                      locator='users_multipleNext1_button',
                                                      verification_element=USERS_ADD_MULTIPLE_USERS_DIAG_BACK)
    USERS_ADD_MULTIPLE_USERS_DIAG_CANCEL1 = ElementInfo(name='User Add Multiple User Diag Cancel Button 1',
                                                        locator='users_multipleCancel_button')
    USERS_ADD_MULTIPLE_USERS_DIAG_CANCEL2 = ElementInfo(name='User Add Multiple User Diag Cancel Button 2',
                                                        locator='users_batchCancel2_button')
    GROUPS_BUTTON = ElementInfo(name='Groups Button',
                                locator='users_group_button',
                                page=Pages.GROUPS_TAB)
    GROUPS_HELP_MANAGING = ElementInfo(name='Group Help Managing Groups',
                                       locator='css=a.bumper_help_link.about_groups > span._text',
                                       page=Pages.GROUPS_TAB)
    GROUPS_HELP_ADDING = ElementInfo(name='Group Help Adding a Group',
                                     locator='css=a.bumper_help_link.adding_groups > span._text',
                                     page=Pages.GROUPS_TAB)
    GROUPS_HELP_CHANGING = ElementInfo(name='Group Help Changing Access to a Share',
                                       locator='css=a.bumper_help_link.group_change_access > span._text',
                                       page=Pages.GROUPS_TAB)
    GROUPS_ADD_DIAG = ElementInfo(name='Group Add Diag',
                                  locator='createGroupDiag')
    GROUPS_ADD_LINK = ElementInfo(name='Group Add Link',
                                  locator='users_createGroup_button',
                                  page=Pages.GROUPS_TAB,
                                  verification_element=GROUPS_ADD_DIAG)
    GROUPS_ADD_DIAG_CANCEL = ElementInfo(name='Group Add Diag Cancel Button',
                                         locator='uesrs_addGroupCancel1_button')
    GROUPS_IMPORT_DIAG = ElementInfo(name='Group Import Diag',
                                     locator='iGroupDiag')
    GROUPS_IMPORT_LINK = ElementInfo(name='Group Import Link',
                                     locator='users_impGroups_link',
                                     page=Pages.GROUPS_TAB,
                                     verification_element=GROUPS_IMPORT_DIAG)
    GROUPS_IMPORT_DIAG_CANCEL = ElementInfo(name='Group Import Diag Cancel Button',
                                            locator='users_impGroupCancel1_butto')
    # Add Users Tab
    USERS_CREATE_USER = ElementInfo(name='Create User',
                                    locator='users_createUser_link',
                                    page=Pages.USERS_TAB,
                                    control_type=TYPE_BUTTON)
    USERS_REMOVE_USER = ElementInfo(name='Remove User',
                                    locator='users_removeUser_link',
                                    page=Pages.USERS_TAB,
                                    control_type=TYPE_BUTTON)
    USERS_REMOVE_USER_OK = ElementInfo(name='Remove User Ok',
                                       locator='popup_apply_button',
                                       control_type=TYPE_BUTTON)
    USERS_ADD_MULTIPLE = ElementInfo(name='Add Multiple Users',
                                     locator='users_addMultiUser_link',
                                     page=Pages.USERS_TAB,
                                     control_type=TYPE_LINK)
    USERS_CREATE_TABLE = ElementInfo(name='Users Create Table',
                                     locator="//table[@id='create_tb']/tbody/tr/td[2]/div",
                                     page=Pages.USERS_TAB)
    # Add Single user
    USERS_CREATE_NAME = ElementInfo(name='User Name',
                                    locator='users_userName_text',
                                    control_type=TYPE_TEXTBOX)
    USERS_CREATE_SAVE = ElementInfo(name='Apply',
                                    locator='users_addUserSave_button',
                                    control_type=TYPE_BUTTON)

    # Multiple users pop-up
    USERS_CREATE_MULTIPLE_USERS = ElementInfo(name='Create Multiple Users',
                                              locator='users_createMultiUsers_button',
                                              control_type=TYPE_BUTTON,
                                              page=Pages.USERS_TAB)
    USERS_CREATE_IMPORT_USERS = ElementInfo(name='Import Users',
                                            locator='users_importUsers_button',
                                            control_type=TYPE_BUTTON,
                                            page=Pages.USERS_TAB)
    USERS_CREATE_MULTIPLE_NEXT1 = ElementInfo(name='Next',
                                              locator='users_multipleNext1_button',
                                              control_type=TYPE_BUTTON)
    USERS_CREATE_MULTIPLE_NAME_PREFIX = ElementInfo(name='User Name Prefix',
                                                    locator='users_userNamePrefix_text',
                                                    control_type=TYPE_TEXTBOX)
    USERS_CREATE_MULTIPLE_ACCOUNT_PREFIX = ElementInfo(name='Account Prefix',
                                                       locator='users_accountPrefix_text',
                                                       control_type=TYPE_TEXTBOX)
    USERS_CREATE_MULTIPLE_NUMBER = ElementInfo(name='Number of Users',
                                               locator='users_numberOfUsers_text',
                                               control_type=TYPE_TEXTBOX)
    USERS_CREATE_MULTIPLE_PASSWORD = ElementInfo(name='Password',
                                                 locator='users_batchPW_password',
                                                 control_type=TYPE_PASSWORD)
    # "Comfirm" is how it is listed in the UI. It is not a typo
    USERS_CREATE_MULTIPLE_PASSWORD_CONFIRM = ElementInfo(name='Confirm New Password',
                                                         locator='users_batchComfirmPW_password',
                                                         control_type=TYPE_PASSWORD)
    USERS_CREATE_MULTIPLE_OVERWRITE_CHECKBOX = ElementInfo(name='Overwrite Users Check',
                                                           locator='users_batchOverwrite_chkbox',
                                                           control_type=TYPE_CHECKBOX)
    USERS_CREATE_MULTIPLE_QUOTAS = ElementInfo(name='Users Multiple Quotas',
                                               locator='users_batchQuotaSize1_text',
                                               control_type=TYPE_TEXTBOX)
    USERS_CREATE_MULTIPLE_NEXT2 = ElementInfo(name='Next',
                                              locator='users_batchNext2_button',
                                              control_type=TYPE_BUTTON)
    USERS_CREATE_MULTIPLE_NEXT3 = ElementInfo(name='Next',
                                              locator='users_batchNext3_button',
                                              control_type=TYPE_BUTTON)
    USERS_CREATE_MULTIPLE_NEXT4 = ElementInfo(name='Next',
                                              locator='users_batchNext4_button',
                                              control_type=TYPE_BUTTON)
    USERS_CREATE_MULTIPLE_SAVE = ElementInfo(name='Next',
                                             locator='users_batchSave_button',
                                             control_type=TYPE_BUTTON)
    # Groups Tab
    # Group List
    GROUPS_UP_ARROW = ElementInfo(name='Groups Arrow Up',
                                  locator='users_groupUp_link',
                                  page=Pages.GROUPS_TAB,
                                  control_type=TYPE_LINK)
    GROUPS_DOWN_ARROW = ElementInfo(name='Groups Arrow Down',
                                    locator='users_groupDown_link',
                                    page=Pages.GROUPS_TAB,
                                    control_type=TYPE_LINK)
    GROUPS_LIST = ElementInfo(name='Group List',
                              locator='css=.LightningSubMenu.userMenu.groupMenuList',
                              page=Pages.GROUPS_TAB,
                              up_arrow=GROUPS_UP_ARROW,
                              down_arrow=GROUPS_DOWN_ARROW,
                              control_type=TYPE_LIST)
    GROUPS_CREATE_GROUP = ElementInfo(name='Create Group Button',
                                      locator='users_createGroup_button',
                                      page=Pages.GROUPS_TAB,
                                      control_type=TYPE_BUTTON)
    GROUPS_REMOVE_GROUP = ElementInfo(name='Remove Group Button',
                                      locator='users_removeGroup_button',
                                      page=Pages.GROUPS_TAB,
                                      control_type=TYPE_BUTTON)
    GROUPS_REMOVE_OK = ElementInfo(name='OK',
                                   locator='popup_apply_button',
                                   control_type=TYPE_BUTTON)
    # Add Group popup
    GROUPS_GROUP_NAME = ElementInfo(name='Group Name',
                                    locator='users_groupName_text',
                                    page=Pages.GROUPS_TAB)
    GROUPS_ADD_GROUP_SAVE = ElementInfo(name='Add Group Save Button',
                                        locator='users_addGroupSave_button',
                                        page=Pages.GROUPS_TAB)
    GROUPS_GROUP_MEMBERSHIP = ElementInfo(name='Group membership',
                                          locator='users_editGroupMember2_link')
    GROUPS_EDIT_GROUP_DIAG = ElementInfo(name='Edit Group Diag',
                                         locator='editGroupDiag')
    GROUPS_EDIT_GROUP_SAVE = ElementInfo(name='Edit Group Save',
                                         locator='users_editGroupMemberSave_button')
    # Shares page
    SHARES_ADD_DIAG = ElementInfo(name='Shares Add Diag',
                                  locator='createShareDiag')
    SHARES_CREATE_SHARE = ElementInfo(name='Create Share',
                                      locator='shares_createShare_button',
                                      page=Pages.SHARES,
                                      verification_element=SHARES_ADD_DIAG)
    SHARES_DELETE_SHARE = ElementInfo(name='Delete Share',
                                      locator='shares_removeShare_button',
                                      page=Pages.SHARES)
    SHARES_HELP_ABOUT_SHARES = ElementInfo(name='Shares Help About Shares',
                                           locator='css=a.bumper_help_link.about_shares > span._text',
                                           page=Pages.SHARES)
    SHARES_HELP_SHARE_ADD = ElementInfo(name='Shares Help Share Add',
                                        locator='css=a.bumper_help_link.share_add > span._text',
                                        page=Pages.SHARES)
    SHARES_HELP_USER_CHANGE_ACCESS = ElementInfo(name='Shares Help User Change Access',
                                                 locator='css=a.bumper_help_link.user_change_access > span._text',
                                                 page=Pages.SHARES)
    # Add Share popup
    SHARES_ADD_NAME = ElementInfo(name='Share Name',
                                  locator='shares_shareName_text',
                                  control_type=TYPE_TEXTBOX)
    SHARES_ADD_DESCRIPTION = ElementInfo(name='Share Description',
                                         locator='shares_shareDesc_text',
                                         control_type=TYPE_TEXTBOX)
    SHARES_ADD_CANCEL = ElementInfo(name='Cancel',
                                    locator='shares_createCancel_button',
                                    control_type=TYPE_BUTTON)
    SHARES_ADD_SAVE = ElementInfo(name='Save',
                                  locator='shares_createSave_button',
                                  control_type=TYPE_BUTTON)
    SHARES_ADD_DIAG_CANCEL = ElementInfo(name='Shares Add Diag Cancel Button',
                                         locator='shares_createCancel_button')
    # Cloud page
    CLOUD_HELP_ABOUT_ACCESS = ElementInfo(name='Cloud Help About Cloud Access',
                                          locator='css=a.bumper_help_link.about_cloud_access > span._text',
                                          page=Pages.CLOUD)
    CLOUD_HELP_BASIC = ElementInfo(name='Cloud Help Cloud Access Basics',
                                   locator='css=a.bumper_help_link.cloud_access_basics > span._text',
                                   page=Pages.CLOUD)
    CLOUD_HELP_SETTING_UP_ACCESS = ElementInfo(name='Cloud Help Setting Up Cloud Access',
                                               locator='css=a.bumper_help_link.setting_up_cloud_access > span._text',
                                               page=Pages.CLOUD)
    CLOUD_ADMIN_SIGN_UP = ElementInfo(name='Cloud Admin Sign Up Button',
                                      locator='cloud_signUp_button',
                                      page=Pages.ADMIN_TAB)
    CLOUD_ADMIN_ICON = ElementInfo(name='Admin Cloud',
                                   locator='cloud_account_admin',
                                   page=Pages.CLOUD,
                                   verification_element=CLOUD_ADMIN_SIGN_UP)
    CLOUD_ADMIN_SIGN_UP_DIAG = ElementInfo(name='Cloud Admin Sign Up Diag',
                                           locator='CloudDiag')
    CLOUD_ADMIN_SIGN_UP_DIAG_CLOSE = ElementInfo(name='Cloud Admin Sign Up Diag Close Button',
                                                 locator='cloud_editMailClose_button')
    CLOUD_ADMIN_GET_CODE = ElementInfo(name='Cloud Admin Get Code',
                                       locator='cloud_getCode_button',
                                       page=Pages.ADMIN_TAB)
    CLOUD_ADMIN_GET_CODE_DIAG = ElementInfo(name='Cloud Admin Get Code Diag',
                                            locator='CloudGetDiag')
    CLOUD_ADMIN_GET_CODE_DIAG_TITLE = ElementInfo(name='Cloud Admin Get Diag Title',
                                                  locator='CloudGetDiag_title')
    CLOUD_ADMIN_GET_CODE_OK = ElementInfo(name='Cloud Admin Get Code Diag OK Button',
                                          locator='cloud_getCodeOK_button')
    # Backups page
    # Backup -> USB
    BACKUPS_CREATE_USB_DIAG = ElementInfo(name='Create USB Job Diag',
                                          locator='BackupsDiag')
    BACKUPS_CREATE_USB_JOB = ElementInfo(name='Create USB Job',
                                         locator='backups_USBBackupsCreate_button',
                                         page=Pages.USB_BACKUPS_TAB,
                                         verification_element=BACKUPS_CREATE_USB_DIAG)
    BACKUPS_CREATE_USB_DIAG_JOB_NAME = ElementInfo(name='Create USB Job Diag Job Name',
                                                   locator='backups_USBBackupsTaskName_text')
    BACKUPS_CREATE_USB_DIAG_DIRECTION_LINK = ElementInfo(name='Create USB Job Diag Direction Link',
                                                         locator='f_category')
    BACKUPS_CREATE_USB_DIAG_SOURCE_FOLDER_BUTTON = ElementInfo(name='Create USB Job Diag Source Folder Button',
                                                               locator='backups_USBBackupsSourcepath_button')
    BACKUPS_CREATE_USB_DIAG_DESTINATION_FOLDER_BUTTON = ElementInfo(name='Create USB Job Diag Destination Folder Button',
                                                                    locator='backups_USBBackupsDestpath_button')
    BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG = ElementInfo(name='Create USB Job Diag Select Folder Diag',
                                                             locator='SelectPathDiag')
    BACKUPS_CREATE_USB_DIAG_SELECT_FOLDER_DIAG_OK = ElementInfo(name='Create USB Job Diag Select Folder Diag OK Button',
                                                                locator='home_treeOk_button')
    BACKUPS_CREATE_USB_DIAG_BACKUP_TYPE_LINK = ElementInfo(name='Create USB Job Diag Backup Type Link',
                                                           locator='f_type')
    BACKUPS_CREATE_USB_DIAG_CREATE = ElementInfo(name='Create USB Job Diag Create Button',
                                                 locator='backups_USBBackupsCreate2_button')
    BACKUPS_CREATE_USB_DIAG_CANCEL = ElementInfo(name='Create USB Job Diag Cancel Button',
                                                 locator='backups_USBBackupsCancel1_button')
    BACKUPS_USB = ElementInfo(name='Backups USB Button',
                              locator='backups_usb_link',
                              page=Pages.BACKUPS,
                              verification_element=BACKUPS_CREATE_USB_JOB)
    BACKUPS_USB_JOB_LIST = ElementInfo(name='Backups USB Job List',
                                       locator='jobs_list_div',
                                       page=Pages.USB_BACKUPS_TAB)
    # Backup -> Remote
    BACKUPS_REMOTE_CREATE_JOB_DIAG = ElementInfo(name='Create Remote Job Diag',
                                                 locator='remoteDiag')
    BACKUPS_REMOTE_CREATE_JOB = ElementInfo(name='Create Remote Job Button',
                                            locator='backups_rCreateJob_button',
                                            page=Pages.REMOTE_BACKUPS_TAB,
                                            verification_element=BACKUPS_REMOTE_CREATE_JOB_DIAG)
    BACKUPS_REMOTE_CREATE_JOB_DIAG_CANCEL = ElementInfo(name='Create Remote Job Diag Cancel Button',
                                                        locator='backups_rCancel_button')
    BACKUPS_REMOTE_BACKUPS_JOB_NAME = ElementInfo(name='Job Name',
                                                  locator='backups_rJobName_text')
    BACKUPS_REMOTE_BACKUPS_REMOTE_IP = ElementInfo(name='Remote IP Address',
                                                   locator='backups_rIP_text')
    BACKUPS_REMOTE_BACKUPS_REMOTE_PASSWORD = ElementInfo(name='Password',
                                                         locator='backups_rpw_password')
    BACKUPS_REMOTE_BACKUPS_SSH_PASSWORD = ElementInfo(name='SSH Password',
                                                      locator='backups_rSSHPW_password')
    BACKUPS_REMOTE_BACKUPS_CREATE_JOB_SAVE = ElementInfo(name='Create',
                                                         locator='backups_rCreateJobSave_button')
    BACKUPS_REMOTE_BACKUPS_SOURCE_FOLDER = ElementInfo(name='Browse Source Folder',
                                                       locator='backups_rBrowseSource_button')
    BACKUPS_REMOTE_BACKUPS_SOURCE_FOLDER_OK = ElementInfo(name='Source Folder OK Button',
                                                          locator='backups_rNext2_button')
    BACKUPS_REMOTE_BACKUPS_DESTINATION_FOLDER = ElementInfo(name='Browse Destination Folder',
                                                            locator='backups_rBrowseDest_button')
    BACKUPS_REMOTE_BACKUPS_DESTINATION_FOLDER_OK = ElementInfo(name='Destination Folder OK Button',
                                                               locator='backups_rNext5_button')
    BACKUPS_REMOTE_BACKUPS_JOB_DELETE = ElementInfo(name='Delete Remote Backup Jobs',
                                                    locator='backups_rDel0_link')
    BACKUPS_REMOTE_BACKUPS_JOB_DELETE_OK = ElementInfo(name='Delete Remote Backup Jobs OK',
                                                       locator='popup_apply_button')
    BACKUPS_REMOTE = ElementInfo(name='Backup Remote Button',
                                 locator='backups_remote_link',
                                 page=Pages.BACKUPS,
                                 verification_element=BACKUPS_REMOTE_CREATE_JOB)
    # Backup -> Internal
    BACKUPS_INTERNAL_CREATE_JOB_DIAG = ElementInfo(name='Internal Create Job Diag',
                                                   locator='Internal_Backup_Create')
    BACKUPS_INTERNAL_CREATE_JOB = ElementInfo(name='Internal Create Job Button',
                                              locator='backups_InternalBackupsCreate_button',
                                              page=Pages.INTERNAL_BACKUPS_TAB,
                                              verification_element=BACKUPS_INTERNAL_CREATE_JOB_DIAG)
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_NAME = ElementInfo(name='Internal Create Job Diag Job Name',
                                                        locator='backups_InternalBackupsTaskName_text')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_SOURCE_FOLDER_BUTTON = ElementInfo(name='Internal Create Job Diag Source Folder Button',
                                                                        locator='backups_InternalBackupsSourcepath_button')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_DESTINATION_FOLDER_BUTTON = ElementInfo(name='Internal Create Job Diag Destination Folder Button',
                                                                             locator='backups_InternalBackupsDestpath_button')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_SELECT_FOLDER_DIAG = ElementInfo(name='Internal Create Job Diag Select Folder Diag',
                                                                      locator='SelectPathDiag')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_SELECT_FOLDER_DIAG_OK = ElementInfo(name='Internal Create job Diag Select Folder Diag OK Button',
                                                                         locator='home_treeOk_button')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_TYPE_LINK = ElementInfo(name='Internal Create Job Diag Type Link',
                                                             locator='f_type')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_CREATE = ElementInfo(name='Internal Create Job Diag Create Button',
                                                          locator='backups_InternalBackupsCreate2_button')
    BACKUPS_INTERNAL_CREATE_JOB_DIAG_CANCEL = ElementInfo(name='Internal Create Job Diag Cancel Button',
                                                          locator='backups_InternalBackupsCancel1_button')
    BACKUPS_INTERNAL = ElementInfo(name='Backup Internal Button',
                                   locator='backups_internal_link',
                                   page=Pages.BACKUPS,
                                   verification_element=BACKUPS_INTERNAL_CREATE_JOB)
    BACKUPS_INTERNAL_JOB_LIST = ElementInfo(name='Backups Internal Job List',
                                            locator='jobs_list_div',
                                            page=Pages.INTERNAL_BACKUPS_TAB)
    # Backup -> Cloud
    BACKUPS_CLOUD_GRAPHIC = ElementInfo(name='Cloud Graphic',
                                        locator='cloud_graphic',
                                        page=Pages.CLOUD_BACKUPS_TAB)
    BACKUPS_CLOUD = ElementInfo(name='Backup Cloud Button',
                                locator='css=#backups_cloud_link > span._text',
                                page=Pages.BACKUPS,
                                verification_element=BACKUPS_CLOUD_GRAPHIC)
    BACKUPS_CLOUD_ELEPHANTDRIVE_TITLE = ElementInfo(name='Elephant Drive Title',
                                                    locator='css=div.h1_content.header_2 > span._text')
    BACKUPS_CLOUD_ELEPHANTDRIVE = ElementInfo(name='Cloud Elephant Drive Button',
                                              locator='css=button.b_elephantdrive.left_button',
                                              verification_element=BACKUPS_CLOUD_ELEPHANTDRIVE_TITLE)
    BACKUPS_CLOUD_ELEPHANTDRIVE_ENABLE = ElementInfo(name='Elephant Drive Enable Button',
                                                     locator='css=div.checkbox_container')
    BACKUPS_CLOUD_AMAZON_CREATE_DIAG = ElementInfo(name='Amazon Create Diag',
                                                   locator='s3Diag')
    BACKUPS_CLOUD_AMAZON_CREATE = ElementInfo(name='Amazon Create Button',
                                              locator='s3_create',
                                              verification_element=BACKUPS_CLOUD_AMAZON_CREATE_DIAG)
    BACKUPS_CLOUD_AMAZON_CREATE_DIAG_CANCEL = ElementInfo(name='Amazon Create Diag Cancel Button',
                                                          locator='backups_s3Cancel1_button')
    BACKUPS_CLOUD_AMAZON = ElementInfo(name='Cloud Amazon Button',
                                       locator='css=button.b_s3.right_button',
                                       verification_element=BACKUPS_CLOUD_AMAZON_CREATE)
    # Backup -> Camera
    BACKUPS_CAMERA_TRANSFER_DIAG = ElementInfo(name='Camera Transfer Diag',
                                               locator='CameraDiag')
    BACKUPS_CAMERA_TRANSFER = ElementInfo(name='Camera Transfer Link',
                                          locator='css=#backups_mediaCameraTransferMode_link > span._text',
                                          page=Pages.CAMERA_BACKUPS_TAB,
                                          verification_element=BACKUPS_CAMERA_TRANSFER_DIAG)
    BACKUPS_CAMERA_TRANSFER_DIAG_CANCEL = ElementInfo(name='Camera Transfer Diag Cencel Button',
                                                      locator='backups_mediaCameraCancel_button')
    BACKUPS_CAMERA = ElementInfo(name='Backups Camera Button',
                                 locator='backups_camera_link',
                                 page=Pages.BACKUPS,
                                 verification_element=BACKUPS_CAMERA_TRANSFER)
    # Storage page
    # RAID
    STORAGE_CHANGE_RAID_MODE_DIAG = ElementInfo(name='Change Raid Mode Diag',
                                                locator='jConfirm_apply')
    STORAGE_CHANGE_RAID_MODE = ElementInfo(name='Change RAID Mode',
                                           locator='storage_raidChangeRAIDMode_button',
                                           page=Pages.RAID_TAB,
                                           verification_element=STORAGE_CHANGE_RAID_MODE_DIAG)
    STORAGE_SETUP_RAID_MODE = ElementInfo(name='Setup RAID Mode',
                                          locator='storage_raidSetupRAIDMode_button',
                                          page=Pages.RAID_TAB)
    STORAGE_CHANGE_RAID_MODE_CLOSE = ElementInfo(name='Change Raid Mode Close Button',
                                                 locator='popup_close_button')
    STORAGE_RAID_PROFILE_TITLE = ElementInfo(name='Raid Profile Title',
                                             locator='css=div.h1_content.header_2 > span._text')
    STORAGE_RAID = ElementInfo(name='RAID Button',
                               locator='storage_raid_link',
                               page=Pages.STORAGE,
                               verification_element=STORAGE_RAID_PROFILE_TITLE)
    # Disk Status
    STORAGE_DISK_STATUS_LIST = ElementInfo(name='Disk Status List',
                                           locator='system_disks_list',
                                           page=Pages.DISK_STATUS_TAB)
    STORAGE_DISK_STATUS = ElementInfo(name='Disk Status Button',
                                      locator='storage_diskstatus_link',
                                      page=Pages.STORAGE,
                                      verification_element=STORAGE_DISK_STATUS_LIST)
    STORAGE_DISK_STATUS_FIRST_DISK = ElementInfo(name='Disk Status First Disk',
                                                 locator='css=div.edit_detail',
                                                 page=Pages.DISK_STATUS_TAB)
    STORAGE_DISK_STATUS_DIAG = ElementInfo(name='Disk Status Diag',
                                           locator='DiskMgmt_Diag_title')
    STORAGE_DISK_STATUS_DIAG_CLOSE = ElementInfo(name='Disk Status Diag Close Button',
                                                 locator='storage_diskstatusDriveClose1_button')
    STORAGE_DISK_STATUS_FIRST_SMART = ElementInfo(name='Disk Status First SMART Link',
                                                  locator='//div[@onclick=\'system_smart_data_diag("1")\']',
                                                  page=Pages.DISK_STATUS_TAB)
    STORAGE_DISK_STATUS_FIRST_SMART_DIAG_CLOSE = ElementInfo(name='Disk Status First SMART Diag Close Button',
                                                             locator='storage_diskstatusSMARTClose2_button')
    # iSCSI
    # TODO: STORAGE_ISCSI_TITLE is not unique. This same locator appears on other pages.
    # Need to find something unique or this can't be used as a verification element.
    STORAGE_ISCSI_TITLE = ElementInfo(name='Disk Status iSCSI Title',
                                      locator='//div[@id=\'mainbody\']/div/span',
                                      page=Pages.ISCSI_TAB)
    STORAGE_ISCSI_ENABLE = ElementInfo(name='Disk Status iSCSI Enable Button',
                                       locator='css=div.checkbox_container',
                                       page=Pages.ISCSI_TAB)
    STORAGE_ISCSI_SWITCH = ElementInfo(name='iSCSI Switch',
                                       locator='css=#storage_iscsi_switch+span .checkbox_container')
    STORAGE_ISCSI = ElementInfo(name='Disk Status iSCSI Button',
                                locator='storage_iscsi_link',
                                page=Pages.STORAGE,
                                verification_element=STORAGE_ISCSI_SWITCH)
    STORAGE_ISCSI_TARGET_ALIAS_TEXT = ElementInfo(name='iSCSI Target Alias',
                                                  locator='storage_iscsiTargetAlias_text')
    STORAGE_ISCSI_TARGET_CREATE_BUTTON = ElementInfo(name='iSCSI Target Create Button',
                                                     locator='storage_iscsiCreateTarget_button',
                                                     page=Pages.ISCSI_TAB,
                                                     verification_element=STORAGE_ISCSI_TARGET_ALIAS_TEXT)
    STORAGE_ISCSI_TARGET_SIZE_TEXT = ElementInfo(name='iSCSI Target Size',
                                                 locator='storage_iscsiTargetSize_text')
    STORAGE_ISCSI_TARGET_CAPACITY_LIST = ElementInfo(name='iSCSI Target Capacity List',
                                                     locator='id_unit')
    STORAGE_ISCSI_TARGET_NEXT1 = ElementInfo(name='iSCSI Target NEXT 1',
                                             locator='storage_iscsiTargetNext1_button')
    STORAGE_ISCSI_TARGET_NEXT2 = ElementInfo(name='iSCSI Target Apply',
                                             locator='storage_iscsiTargetSave2_button')
    STORAGE_VOLUME_VIRTUALIZATION_DIAG = ElementInfo(name='Volume Virtualization Diag',
                                                     locator='VV_Create_Info')
    STORAGE_VOLUME_VIRTUALIZATION_DIAG_CANCEL = ElementInfo(name='Volume Virtualization Diag Cancel Button',
                                                            locator='storage_volumevirtualCreateCancel1_button')
    STORAGE_VOLUME_VIRTUALIZATION_CREATE = ElementInfo(name='Volume Virtualization Create Button',
                                                       locator='storage_volumevirtualCreate_button',
                                                       page=Pages.VOLUME_VIRTUALIZATION_TAB,
                                                       verification_element=STORAGE_VOLUME_VIRTUALIZATION_DIAG)
    STORAGE_VOLUME_VIRTUALIZATION = ElementInfo(name='Volume Virtualization Button',
                                                locator='storage_virtualization_link',
                                                page=Pages.STORAGE,
                                                verification_element=STORAGE_VOLUME_VIRTUALIZATION_CREATE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_IP = ElementInfo(name='Volume Virtualization Create IP',
                                                          locator='storage_volumevirtualCreateIP_text',
                                                          page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT1 = ElementInfo(name='Volume Virtualization Create Next1 Button',
                                                             locator='storage_volumevirtualCreateNext1_button',
                                                             page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT2 = ElementInfo(name='Volume Virtualization Create Next2 Button',
                                                             locator='storage_volumevirtualCreateNext2_button',
                                                             page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT3 = ElementInfo(name='Volume Virtualization Create Next3 Button',
                                                             locator='storage_volumevirtualCreateNext3_button',
                                                             page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT4 = ElementInfo(name='Volume Virtualization Create Next4 Button',
                                                             locator='storage_volumevirtualCreateNext4_button',
                                                             page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_SHARE_FOLDER = ElementInfo(name='Volume Virtualization Create Share Folder',
                                                                    locator='storage_volumevirtualCreateShareFolder_text',
                                                                    page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_NEXT5 = ElementInfo(name='Volume Virtualization Create Next5 Button',
                                                             locator='storage_volumevirtualCreateNext5_button',
                                                             page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_CREATE_SAVE = ElementInfo(name='Volume Virtualization Create Save Button',
                                                             locator='storage_volumevirtualCreateNext6_button',
                                                             page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_ISCSI_TARGET_CONNECT_BUTTON = ElementInfo(name='Volume Virtualization iSCSI Connect Button',
                                                                            locator='storage_volumevirtualDetailsConnect_button',
                                                                            page=Pages.STORAGE)
    STORAGE_VOLUME_VIRTUALIZATION_ISCSI_TARGET_DIAG_CLOSE_BUTTON = ElementInfo(name='Volume Virtualization iSCSI Target Diag Close Button',
                                                                               locator='storage_volumevirtualDetailsCancel7_button',
                                                                               page=Pages.STORAGE)
    # RAID page
    RAID_POPUP_APPLY = ElementInfo(name='RAID Popup Apply',
                                   locator='popup_apply_button')
    RAID_CHANGE_RAID_MODE = ElementInfo(name='Change RAID Mode',
                                        locator='storage_raidChangeRAIDMode_button',
                                        page=Pages.RAID_TAB,
                                        verification_element=RAID_POPUP_APPLY)
    RAID_SWITCH_MODE = ElementInfo(name='Switch RAID Mode Checkbox',
                                   locator='css=.LightningCheckbox,storage_raidFormatType1_chkbox')
    RAID_CREATE_NEW_VOLUMES = ElementInfo(name='Create New Volume(s) Checkbox',
                                          locator='css=.LightningCheckbox,storage_raidFormatType2_chkbox')
    RAID_CHANGE_RAID_MODE_CLOSE = ElementInfo(name='Change RAID Mode Close Button',
                                              locator='storage_raidFormatFinish9_button',
                                              control_type=TYPE_BUTTON)
    RAID_ROAMING_CLOSE = ElementInfo(name='RAID Roaming Close Button',
                                     locator='home_RAIDRoamingNext1_button',
                                     control_type=TYPE_BUTTON)

    # Insert Hard Drive message
    INSERT_HARD_DRIVE = ElementInfo(name='Insert Hard Drive Close Button',
                                    locator='eula_chkHDD_button',
                                    control_type=TYPE_BUTTON)

    # Apps page
    # Add APP
    APPS_ADD_APP_DIAG = ElementInfo(name='Add APP Diag',
                                    locator='AppsDiag_Browse_List')
    APPS_ADD_APP = ElementInfo(name='Add App',
                               locator='apps_installed_button',
                               page=Pages.APPS,
                               verification_element=APPS_ADD_APP_DIAG)
    APPS_ADD_APP_CANCEL = ElementInfo(name='Add APP Diag Cancel Button',
                                      locator='Apps_Cancel1_button')
    # Apps EULA
    APPS_EULA_DIAG = ElementInfo(name='APPs EULA Diag',
                                 locator='AppsDiag_Eula')
    APPS_EULA_DIAG_ACCEPT = ElementInfo(name='APPs EULA Diag Accept Button',
                                        locator='apps_next_button_7',
                                        verification_element=APPS_ADD_APP_DIAG)
    # Apps -> HTTP Download
    APPS_HTTP_DOWNLOAD_CREATE = ElementInfo(name='Http Download Create Button',
                                            locator='apps_httpdownloadsCreate_button',
                                            page=Pages.HTTP_DOWNLOADS_TAB)
    APPS_HTTP_DOWNLOAD_CREATE_DIAG = ElementInfo(name='Http Download Create Diag',
                                                 locator='popup_ok')
    APPS_HTTP_DOWNLOAD_CREATE_DIAG_OK = ElementInfo(name='Http Download Create Diag OK Button',
                                                    locator='popup_ok_button')
    APPS_HTTP_DOWNLOAD = ElementInfo(name='Http Download Button',
                                     locator='apps_httpdownloads_link',
                                     page=Pages.APPS,
                                     verification_element=APPS_HTTP_DOWNLOAD_CREATE)
    APPS_HTTP_DOWNLOAD_BROWSE = ElementInfo(name='Http Download Browse Button',
                                            locator='apps_httpdownloadsBrowse_button',
                                            page=Pages.HTTP_DOWNLOADS_TAB) 
    APPS_HTTP_DOWNLOAD_URL = ElementInfo(name='Http Download URL',
                                         locator='apps_httpdownloadsURL_text',
                                         control_type=TYPE_TEXTBOX)
    APPS_HTTP_DOWNLOAD_SAVE_TO = ElementInfo(name='Http Download Save To',
                                             locator='apps_httpdownloadsSaveTo_text',
                                             control_type=TYPE_TEXTBOX)
    APPS_HTTP_DOWNLOAD_RENAME = ElementInfo(name='Http Download Rename',
                                            locator='apps_httpdownloadsRename_text',
                                            control_type=TYPE_TEXTBOX)
    # Apps -> FTP Download
    APPS_FTP_DOWNLOAD_CREATE = ElementInfo(name='FTP Download Create Button',
                                           locator='apps_ftpdownloadsCreate_button',
                                           page=Pages.FTP_DOWNLOADS_TAB)
    APPS_FTP_DOWNLOAD_CREATE_ACCOUNT = ElementInfo(name='FTP Download Create Account Button',
                                                   locator='apps_ftpdownlaodsAccount_button',
                                                   page=Pages.FTP_DOWNLOADS_TAB)
    APPS_FTP_DOWNLOAD_CREATE_ACCOUNT_USERNAME = ElementInfo(name='FTP Download Create Account USERNAME',
                                                            locator='apps_ftpdownloadsUsername_text')
    APPS_FTP_DOWNLOAD_CREATE_ACCOUNT_PASSWORD = ElementInfo(name='FTP Download Create Account USERNAME',
                                                            locator='apps_ftpdownloadsPassword_text')
    APPS_FTP_DOWNLOAD_CREATE_SOURCE_FOLDER_PATH = ElementInfo(name='FTP Download Create Source Folder Path',
                                                              locator='apps_ftpdownloadsSourcepath_button')
    APPS_FTP_DOWNLOAD_CREATE_SOURCE_FOLDER_OK = ElementInfo(name='FTP Download Create Source Folder OK',
                                                            locator='home_treeOk_button')
    APPS_FTP_DOWNLOAD_CREATE_DESTINATION_FOLDER_PATH = ElementInfo(name='FTP Download Create Destination Folder Path',
                                                                   locator='apps_ftpdownloadsDestpath_button')
    APPS_FTP_DOWNLOAD_CREATE_DESTINATION_FOLDER_OK = ElementInfo(name='FTP Download Create Destination Folder OK',
                                                                 locator='home_treeOk_button')
    APPS_FTP_DOWNLOAD_CREATE_JOB = ElementInfo(name='FTP Download Create JOB',
                                               locator='apps_ftpdownloadsTaskName_text')
    APPS_FTP_DOWNLOAD_CREATE_HOST = ElementInfo(name='FTP Download Create Host',
                                                locator='apps_ftpdownloadsHost_text')
    APPS_FTP_DOWNLOAD_CREATE_HOST_PORT = ElementInfo(name='FTP Download Create Host Port',
                                                     locator='apps_ftpdownloadsHostPort_text')
    APPS_FTP_DOWNLOAD_CREATE_JOB_OK = ElementInfo(name='FTP Download Create JOB OK',
                                                  locator='apps_ftpdownloadsCreate3_button')
    APPS_FTP_DOWNLOAD_CREATE_JOB_RECURRENCE = ElementInfo(name='FTP Download Recurrence Switch',
                                                          locator='css = #apps_ftpdownloadsRecurring_switch+span .checkbox_container')
    APPS_FTP_DOWNLOAD_CREATE_JOB_RECURRENCE_WEEKLY = ElementInfo(name='FTP Download Recurrence Weekly',
                                                                 locator='backups_InternalBackupWeekly_button')
    APPS_FTP_DOWNLOAD_CREATE_DIAG = ElementInfo(name='FTP Download Create Diag',
                                                locator='Ftp_Download_Create')
    APPS_FTP_DOWNLOAD_CREATE_CANCEL = ElementInfo(name='FTP Download Create Diag Cancel Button',
                                                  locator='apps_ftpdownloadsCancel3_button')
    APPS_FTP_DOWNLOAD = ElementInfo(name='FTP Download Button',
                                    locator='apps_ftpdownloads_link',
                                    page=Pages.APPS,
                                    verification_element=APPS_FTP_DOWNLOAD_CREATE)
    # Apps -> P2P Download
    APPS_P2P_DOWNLOAD_TITLE = ElementInfo(name='P2P Download Title',
                                          locator='css=div.h1_content.header_2 > span._text',
                                          page=Pages.P2P_DONWNLOAD_TAB)
    APPS_P2P_DOWNLOAD_ENABLE = ElementInfo(name='P2P Download Enable Button',
                                           locator='css=span.checkbox_off_text',
                                           page=Pages.P2P_DONWNLOAD_TAB)
    APPS_P2P_DOWNLOAD = ElementInfo(name='P2P Download Button',
                                    locator='apps_p2pdownloads_link',
                                    page=Pages.APPS,
                                    verification_element=APPS_P2P_DOWNLOAD_TITLE)
    # Apps -> Web File Viewer
    APPS_WEB_FILE_FRAME = ElementInfo(name='Web File Viewer Frame',
                                      locator='mainFrame',
                                      page=Pages.WEB_FILE_VIEWER_TAB)
    APPS_WEB_FILE = ElementInfo(name='Web File Viewer Button',
                                locator='apps_webfileviewer_link',
                                page=Pages.APPS,
                                verification_element=APPS_WEB_FILE_FRAME)
    APPS_WEB_FILE_PATH = ElementInfo(name='Web File Viewer Path',
                                     locator='id_path')
    APPS_WEB_FILE_COPY = ElementInfo(name='Web File Viewer Copy Button',
                                     locator='Apps_webfileviewerCopy_link')
    APPS_WEB_FILE_MOVE = ElementInfo(name='Web File Viewer Move Button',
                                     locator='Apps_webfileviewerMove_link')
    APPS_WEB_FILE_UPLOAD = ElementInfo(name='Web File Viewer Upload Button',
                                       locator='Apps_webfileviewerUpload_link')
    # For copy and move buttons
    APPS_WEB_FILE_SELECT_PATH_DIAG = ElementInfo(name='Web File Viewer Select Path Diag',
                                                 locator='SelectPathDiag')
    APPS_WEB_FILE_SELECT_PATH_DIAG_OK = ElementInfo(name='Web File Viewer Select Path Diag OK Button',
                                                    locator='home_treeOk_button')
    APPS_WEB_FILE_METHOD_OVERWRITE = ElementInfo(name='Web File Viewer Copy/Move Method - OverWrite',
                                                 locator='Apps_webfileviewerOverwrite_button')
    APPS_WEB_FILE_METHOD_SKIP = ElementInfo(name='Web File Viewer Copy/Move Method - Skip',
                                            locator='Apps_webfileviewerSkip_button')
    APPS_WEB_FILE_METHOD_KEEPBOTH = ElementInfo(name='Web File Viewer Copy/Move Method - Keep Both',
                                                locator='Apps_webfileviewerKeep_button')
    APPS_WEB_FILE_RENAME = ElementInfo(name='Web File Viewer Rename Button',
                                       locator='Apps_webfileviewerRename_link')
    APPS_WEB_FILE_RENAME_DIAG = ElementInfo(name='Web File Viewer Rename Diag',
                                            locator='shareDiag')
    APPS_WEB_FILE_RENAME_DIAG_NAME_TEXT = ElementInfo(name='Web File Viewer Rename Diag Name Text',
                                                      locator='Apps_webfileviewerRename_text')
    APPS_WEB_FILE_RENAME_DIAG_SAVE = ElementInfo(name='Web File Viewer Rename Diag Save Button',
                                                 locator='Apps_webfileviewerRenameSave_button')
    APPS_WEB_FILE_DELETE = ElementInfo(name='Web File Viewer Delete Button',
                                       locator='Apps_webfileviewerDelete_link')
    APPS_WEB_FILE_DELETE_DIAG = ElementInfo(name='Web File Viewer Delete Diag',
                                            locator='jConfirm_apply')
    APPS_WEB_FILE_DELETE_DIAG_OK = ElementInfo(name='Web File Viewer Delete Diag OK Button',
                                               locator='popup_apply_button')
    APPS_WEB_FILE_REFRESH = ElementInfo(name='Web File Viewer Refresh Button',
                                        locator='Apps_webfileviewerRefresh_link')
    APPS_WEB_FILE_CREATE_SUB_FOLDER = ElementInfo(name='Web File Viewer Create Sub Folder Button',
                                                  locator='Apps_webfileviewerCreate_link')
    APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG = ElementInfo(name='Web File Viewer Create Sub Folder Button Diag',
                                                       locator='shareDiag')
    APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_NAME_TEXT = ElementInfo(name='Web File Viewer Create Sub Folder Diag Name Text',
                                                                 locator='Apps_webfileviewerCreate_text')
    APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG = ElementInfo(name='Web File Viewer Create Sub Folder Button Diag',
                                                       locator='shareDiag')
    APPS_WEB_FILE_CREATE_SUB_FOLDER_DIAG_SAVE = ElementInfo(name='Web File Viewer Create Sub Folder Diag Save Button',
                                                            locator='Apps_webfileviewerCreateSave_button')
    APPS_WEB_FILE_PROPERTIES_DIAG = ElementInfo(name='Web File Viwer Properties Diag',
                                                locator='sDiag_properties')
    APPS_WEB_FILE_PROPERTIES_DIAG_CLOSE = ElementInfo(name='Web File Viwer Properties Diag Close Button',
                                                      locator='a_properties_ok')
    APPS_WEB_FILE_ERROR_DIAG = ElementInfo(name='Web File Viwer Error Diag',
                                           locator='popup_ok')
    APPS_WEB_FILE_ERROR_DIAG_OK = ElementInfo(name='Web File Viwer Error Diag OK Button',
                                              locator='popup_ok_button')
    # Links in the right-click context menu
    APPS_WEB_FILE_MENU_DELETE = ElementInfo(name='Web File Viewer Context Menu Delete Link',
                                            locator='link=Delete')
    APPS_WEB_FILE_MENU_PROPERTIES = ElementInfo(name='Web File Viewer Context Menu Properties Link',
                                                locator='link=Properties')
    APPS_WEB_FILE_MENU_CREATE_ZIP = ElementInfo(name='Web File Viewer Context Menu Create Zip Link',
                                                locator='id_add_zip')
    APPS_WEB_FILE_MENU_ADD_ZIP = ElementInfo(name='Web File Viewer Context Menu Add To Zip Link',
                                             locator='link=Add To Zip')
    APPS_WEB_FILE_MENU_ADD_ZIP_DIAG = ElementInfo(name='Web File Viewer Context Menu Add To Zip Link Diag',
                                                  locator='sDiag_zip_tree')
    APPS_WEB_FILE_MENU_ADD_ZIP_DIAG_OK = ElementInfo(name='Web File Viewer Context Menu Add To Zip Link Diag OK Button',
                                                     locator='a_zip_ok')
    APPS_WEB_FILE_MENU_UNZIP = ElementInfo(name='Web File Viewer Context Menu Unzip Link',
                                           locator='link=Unzip')
    APPS_WEB_FILE_MENU_UNTAR = ElementInfo(name='Web File Viewer Context Menu Untar Link',
                                           locator='link=Untar')
    APPS_WEB_FILE_MENU_OPEN = ElementInfo(name='Web File Viewer Context Menu Open Link',
                                          locator='link=Open')
    MEDIA_BUTTON = ElementInfo(name='Media Button',
                                 locator='settings_media_link',
                                 page=Pages.SETTINGS)
    # Settings -> General
    GENERAL_DEVICE_NAME = ElementInfo(name='Device Name',
                                      locator='settings_generalDeviceName_text',
                                      page=Pages.GENERAL_TAB)
    SETTINGS_DEVICE_NAME = GENERAL_DEVICE_NAME  # Left for backwards compatibility
    GENERAL_BUTTON = ElementInfo(name='General Button',
                                 locator='settings_general_link',
                                 page=Pages.SETTINGS,
                                 verification_element=GENERAL_DEVICE_NAME)
    SETTINGS_DEVICE_NAME_APPLY = ElementInfo(name='Device Name Apply',
                                             locator='settings_generalDeviceName_button',
                                             page=Pages.GENERAL_TAB,
                                             control_type=TYPE_BUTTON)
    SETTINGS_DEVICE_NAME_SAVE = ElementInfo(name='SAVE',
                                            locator='popup_apply_button',
                                            control_type=TYPE_BUTTON)
    SETTINGS_DEVICE_DESCRIPTION = ElementInfo(name='Device Description',
                                              locator='settings_generalDesc_text',
                                              control_type=TYPE_TEXTBOX)
    SETTINGS_DEVICE_DESCRIPTION_APPLY = ElementInfo(name='Device Description Apply',
                                                    locator='settings_generalDesc_button',
                                                    control_type=TYPE_BUTTON)
    SETTINGS_GENERAL_PRIMARY_SERVER_LINK = ElementInfo(name='General Primary Server Link',
                                                       locator='css=#settings_generalPrimaryServer_link > span._text',
                                                       page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_PRIMARY_SERVER_DIAG = ElementInfo(name='General Primary Server Diag',
                                                       locator='hd_ntp_Diag')
    SETTINGS_GENERAL_PRIMARY_SERVER_DIAG_CANCEL = ElementInfo(name='General Primary Server Cancel Button',
                                                              locator='settings_generalNTPServerCancel_button_')
    # use 'get_text' to get current ON/OFF status from this switch element
    SETTINGS_GENERAL_CLOUD_SWITCH = ElementInfo(name='Settings General Cloud Switch Button',
                                                locator='css=#settings_generalCloud_switch+span .checkbox_container')
    SETTINGS_GENERAL_CLOUD_SWITCH_DIAG_OK = ElementInfo(name='Settings General Cloud Switch Diag OK Button',
                                                        locator='popup_apply_button')
    SETTINGS_GENERAL_CLOUD_SERVICE_LINK = ElementInfo(name='General Cloud Service Link',
                                                      locator='css=#settings_generalCloud_link > a.edit_detail > span._text',
                                                      page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_CLOUD_SERVICE_DIAG = ElementInfo(name='General Cloud Service Diag',
                                                      locator='CloudOptionDiag')
    SETTINGS_GENERAL_CLOUD_SERVICE_DIAG_CANCEL = ElementInfo(name='General Cloud Service Link',
                                                             locator='settings_generalCloudCancel_button')
    SETTINGS_GENERAL_CLOUD_SERVICE_WIN_XP = ElementInfo(name='General Cloud Access Win XP',
                                                        locator='settings_generalCloudWinXP_button',
                                                        page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_CLOUD_SERVICE_REBUILD = ElementInfo(name='General Cloud Access Rebuild',
                                                         locator='settings_generalCloudRebuild_button',
                                                         page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_CLOUD_SERVICE_APPLY = ElementInfo(name='Apply',
                                                       locator='settings_generalCloudSave_button',
                                                       page=Pages.GENERAL_TAB)
    # TODO: Determine which of the two SETTINGS_GENERAL_CLOUD_SWITCH_DIALOG is correct
    SETTINGS_GENERAL_CLOUD_SWITCH_DIAG = ElementInfo(name='Settings General Cloud Switch Diag',
                                                     locator='jConfirm_apply')
    SETTINGS_GENERAL_CLOUD_ACCESS_CONNECTION_DIAG = ElementInfo(name='Settings General Cloud Access Connection Diag',
                                                                locator='css=div#CloudOptionDiag.WDLabelDiag')
    SETTINGS_GENERAL_TIME_MACHINE_SWITCH = ElementInfo(name='Settings General Time Machine Switch Button',
                                                       locator='css=#settings_generalTM_switch+span .checkbox_container',
                                                       page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_TIME_MACHINE_LINK = ElementInfo(name='General Time Machine Link',
                                                     locator='css=#settings_generalTM_link > span._text',
                                                     page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_TIME_MACHINE_DIAG = ElementInfo(name='General Primary Server Link',
                                                     locator='tmDiag')
    SETTINGS_GENERAL_TIME_MACHINE_SHARE_SELECT = ElementInfo(name='General Time Machine Share list',
                                                             locator='settings_generalTMShare_select',
                                                             page=Pages.GENERAL_TAB,
                                                             control_type=TYPE_LIST)
    SETTINGS_GENERAL_TIME_MACHINE_DIAG_CANCEL = ElementInfo(name='General Primary Server Link',
                                                            locator='settings_generalTMCancel_button')
    SETTINGS_GENERAL_TIME_MACHINE_SAVE = ElementInfo(name='General Time Machine Configure Save Button',
                                                     locator='settings_generalTMSave_button',
                                                     control_type=TYPE_BUTTON)
    SETTINGS_GENERAL_RECYCLEBIN_LINK = ElementInfo(name='General Recycle Bin Link',
                                                   locator='css=#settings_recycleBinConfig_link > span._text',
                                                   page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_RECYCLEBIN_DIAG = ElementInfo(name='General Primary Server Link',
                                                   locator='recycleBinDiag')
    SETTINGS_GENERAL_RECYCLEBIN_DIAG_CANCEL = ElementInfo(name='General Primary Server Link',
                                                          locator='settings_recycleBinCancel_button')
    SETTINGS_GENERAL_RECYCLEBIN_OK = ElementInfo(name='General Recycle Bin Ok Button',
                                                 locator='settings_recycleBinClear_button')
    SETTINGS_GENERAL_RECYCLEBIN_APPLY = ElementInfo(name='General Recycle Bin Apply Button',
                                                    locator='popup_apply_button')
    GENERAL_DATE_TIME_CONFIGURE = ElementInfo(name='Configure',
                                              locator="css=#time_detail > span._text",
                                              page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_DRIVE_SLEEP_SWITCH = ElementInfo(name='Settings General Drive Sleep Switch Button',
                                                      locator='css=#settings_generalDriveSleep_switch+span .checkbox_container',
                                                      page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_LED_SWITCH = ElementInfo(name='Settings General LED Switch Button',
                                              locator='css=#settings_generalLed_switch+span .checkbox_container',
                                              page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_POWER_SCHEDULE_SWITCH = ElementInfo(name='Settings General Power Schedule Switch Button',
                                                         locator='css=#settings_generalPowerSch_switch+span .checkbox_container',
                                                         page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_POWER_SCHEDULE = ElementInfo(name='Power Schedule',
                                                  locator='css=#power_on_off_switch_detail > span._text',
                                                  page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_POWER_SCHEDULE_DIAG = ElementInfo(name='Power Schedule Diag',
                                                       locator='power_Diag')
    SETTINGS_GENERAL_POWER_SCHEDULE_SAVE = ElementInfo(name='Power Schedule Configuration Save Button',
                                                       locator='settings_generalPowerSave_button',
                                                       control_type=TYPE_BUTTON)
    SETTINGS_GENERAL_WEB_ACCESS_TIMEOUT_LIST = ElementInfo(name='Settings General Web Access Timeout List',
                                                           locator='settings_generalTimeout_select',
                                                           page=Pages.GENERAL_TAB,
                                                           control_type=TYPE_LIST)
    SETTINGS_GENERAL_LANGUAGE = ElementInfo(name='Settings General Language List',
                                            locator='id_language',
                                            page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_LANGUAGE_SAVE = ElementInfo(name='Settings General Language Save',
                                                 locator='settings_generalLanguageSave_button',
                                                 page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_TIME_ZONE = ElementInfo(name='Settings General Time Zone',
                                             locator='id_timezone',
                                             page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_TIMEZONE_LIST = ElementInfo(name='Settings Time Zone List',
                                                 locator='id_timezone_li',
                                                 page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_TIMEZONE_SAVE = ElementInfo(name='Settings Time Zone Save',
                                                 locator='settings_generalTimeZoneSave_button',
                                                 page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_NTP_SERVICE = ElementInfo(name='Settings General NTP Service',
                                               locator='css=#settings_generalNTP_switch+span .checkbox_container',
                                               page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_NTP_SERVER_SAVE_BUTTON = ElementInfo(name='NTP Server Save',
                                                          locator='settings_generalNTPServerSave_button',
                                                          page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_NTP_SERVER_ADD_BUTTON = ElementInfo(name='Add NTP Server Button',
                                                         locator='settings_generalNTPServerAdd_button',
                                                         page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_NTP_SERVER_VALUE = ElementInfo(name='NTP Server Value',
                                                    locator='Settings_generallPrimaryServer_value',
                                                    page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_NTP_SERVER_TEXT = ElementInfo(name='NTP Server Text',
                                                    locator='settings_generalNTPServer_text',
                                                    page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_PRIMARY_SERVER_CONFIGURE = ElementInfo(name='Settings General Primary Server ',
                                                            locator='settings_generalPrimaryServer_link',
                                                            page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_DATE_TIME = ElementInfo(name='Settings General Date Time',
                                             locator='settings_generalDateTime_value',
                                             page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_TIME_FORMAT = ElementInfo(name='Settings General Time Format',
                                               locator='f_time_format',
                                               page=Pages.GENERAL_TAB)
    SETTINGS_GENERAL_DATE_FORMAT = ElementInfo(name='Settings Date Format',
                                               locator='f_date_format',
                                               page=Pages.GENERAL_TAB)
    # Date & Time Settings
    DATE_TIME_HOUR = ElementInfo(name='Hour',
                                 locator='settings_generalDTHour_select',
                                 page=Pages.DATE_TIME_SETTINGS)

    DATE_TIME_MINUTE = ElementInfo(name='Hour',
                                   locator='settings_generalDTMin_select',
                                   page=Pages.DATE_TIME_SETTINGS)

    DATE_TIME_SAVE = ElementInfo(name='Save',
                                 locator='settings_generalDTSave_button',
                                 page=Pages.DATE_TIME_SETTINGS)

    DATE_TIME_SAVE_OK = ElementInfo(name='OK',
                                    locator='popup_ok_button',
                                    control_type=TYPE_BUTTON)
    # Settings -> Network
    NETWORK_MAC_ADDRESS = ElementInfo(name='MAC Address',
                                      locator='mac_div',
                                      page=Pages.NETWORK_TAB)
    NETWORK_BUTTON = ElementInfo(name='Network Button',
                                 locator='settings_network_link',
                                 page=Pages.SETTINGS,
                                 verification_element=NETWORK_MAC_ADDRESS)
    SETTINGS_NETWORK_BONDING = ElementInfo(name='Network Bonding Select',
                                           locator='settings_networkBonding_select',
                                           page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_FTP_SWITCH = ElementInfo(name='Network FTP Switch',
                                              locator='css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                              page=Pages.NETWORK_TAB,
                                              control_type=TYPE_BUTTON)
    SETTINGS_NETWORK_FTP_SAVE = ElementInfo(name='Network FTP \'ON\' Save',
                                            locator='popup_ok_button',
                                            page=Pages.NETWORK_TAB,
                                            control_type=TYPE_BUTTON)
    SETTINGS_NETWORK_FTP_LINK = ElementInfo(name='Network FTP Link',
                                            locator='css=#settings_networkFTPAccessConfig_link > span._text',
                                            page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_AFP_SWITCH = ElementInfo(name='Network AFP Switch',
                                              locator='css=#settings_networkAFP_switch+span .checkbox_container',
                                              page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_NFS_SWITCH = ElementInfo(name='Network NFS Switch',
                                              locator='css=#settings_networkNFS_switch+span .checkbox_container',
                                              page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_DFS_SWITCH = ElementInfo(name='Network DFS Switch',
                                              locator='css=#settings_networkDFS_switch+span .checkbox_container',
                                              page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_DFS_CONFIGURE = ElementInfo(name='Network DFS Configure',
                                                 locator='css=#settings_networkDFS_link > span._text')
    SETTINGS_NETWORK_DFS_ROOT_FOLDER_APPLY_BUTTON = ElementInfo(name='DFS Root Folder Name Apply',
                                                                locator='settings_networkDSFRootFolderSave_button')
    SETTINGS_NETWORK_DFS_ROOT_FOLDER_NAME = ElementInfo(name='Root Folder Name',
                                                        locator='settings_networkDFSRootFolder_text')
    SETTINGS_NETWORK_DFS_ADD_LINK = ElementInfo(name='DFS Add Link',
                                                locator='settings_networkDFSConfigSave_button')
    SETTINGS_NETWORK_DFS_LOCAL_FOLDER_NAME = ElementInfo(name='DFS Local Folder Name',
                                                         locator='settings_networkDFSShareName_text')
    SETTINGS_NETWORK_DFS_REMOTE_HOST = ElementInfo(name='DFS Remote Host',
                                                   locator='settings_networkDFSHost_text')
    SETTINGS_NETWORK_DFS_REMOTE_SHARE = ElementInfo(name='DFS Remote Share',
                                                    locator='settings_networkDFSRemoteFolder_text')
    SETTINGS_NETWORK_DFS_ADD_LINK_APPLY = ElementInfo(name='DFS Add Link Apply',
                                                      locator='settings_networkDFSSave_button')
    SETTINGS_NETWORK_DFS_ADD_LINK_CANCEL = ElementInfo(name='DFS Add Link Cancel',
                                                       locator='settings_networkDFSCancel_button')
    SETTINGS_NETWORK_AD_SWITCH = ElementInfo(name='Active Directory',
                                             locator='css=#settings_networkADS_switch+span .checkbox_container')
    SETTINGS_NETWORK_AD_APPLY = ElementInfo(name='Active Directory',
                                            locator='settings_networkADSSave_button')
    SETTINGS_NETWORK_AD_USERNAME = ElementInfo(name='AD User Name',
                                               locator='settings_networkADSName_text')
    SETTINGS_NETWORK_AD_PASSWORD = ElementInfo(name='AD Password',
                                               locator='settings_networkASDPW_password')
    SETTINGS_NETWORK_AD_DOMAIN_NAME = ElementInfo(name='AD Domain Name',
                                                  locator='settings_networkADSRealmName_text')
    SETTINGS_NETWORK_AD_DNS_SERVER = ElementInfo(name='AD DNS Server',
                                                 locator='settings_networkADSDNS_text')
    SETTINGS_NETWORK_AD_OK = ElementInfo(name='AD OK',
                                         locator='popup_ok_button')
    SETTINGS_NETWORK_AD_CONFIGURE = ElementInfo(name='AD configure',
                                                locator='css=#settings_networkADS_link > span._text')
    SETTINGS_NETWORK_AD_OK = ElementInfo(name='AD OK Button',
                                         locator='popup_ok_button')
    SETTINGS_NETWORK_AD_RESULT_DIAG = ElementInfo(name='AD Result Dialog',
                                                  locator='error_dialog_message')
    SETTINGS_NETWORK_WEBDAV_SWITCH = ElementInfo(name='Network WebDAV Switch',
                                                 locator='css=#settings_networkWebdav_switch+span .checkbox_container',
                                                 page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_SSH_LINK = ElementInfo(name='Network SSH Link',
                                            locator='css=#ssh_conf_div > span._text',
                                            page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_SSH_DIAG = ElementInfo(name='Network SSH Diag',
                                            locator='sshDiag')
    SETTINGS_NETWORK_SSH_PASSWORD = ElementInfo(name='Network SSH new password',
                                                locator='settings_SSHPW_password')
    SETTINGS_NETWORK_SSH_CONFIRM_PASSWORD = ElementInfo(name='Network SSH new password',
                                                        locator='settings_SSHConfirmPW_password')
    SETTINGS_NETWORK_SSH_SAVE = ElementInfo(name='Network SSH password save',
                                            locator='settings_SSHSave_button')
    SETTINGS_NETWORK_SSH_TIP2_PASSWORD_ERROR = ElementInfo(name='SSH TIP2 PASSWORD Error',
                                                           locator='css=div.TooltipIconError.tip_pw2_error')
    SETTINGS_NETWORK_NETWORK_STATIC = ElementInfo(name='Network Mode Static',
                                                  locator='settings_networkLAN0IPv4Static_button',
                                                  page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_NETWORK_DHCP = ElementInfo(name='Network Mode DHCP',
                                                locator='settings_networkLAN0IPv4DHCP_button',
                                                page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_SSH_CLOSE = ElementInfo(name='Network SSH Close Button',
                                             locator='css=#pwDiag > button.ButtonMarginLeft_20px.close')
    SETTINGS_NETWORK_PORT_FORWARDING_LINK = ElementInfo(name='Network Port Forwarding Link',
                                                        locator='settings_networkPortForLearn_link',
                                                        page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_PORT_FORWARDING_INFO = ElementInfo(name='Network Port Forwarding Info',
                                                        locator='portforwarding_list_info',
                                                        page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_PORT_FORWARDING_NEXT = ElementInfo(name='Network Port Forwarding Next Button',
                                                        locator='settings_networkPortForNext1_button')
    SETTINGS_NETWORK_PORT_FORWARDING_ADD = ElementInfo(name='Network Port Forwarding Add Button',
                                                       locator='settings_networkPortForAdd_button',
                                                       page=Pages.NETWORK_TAB,
                                                       verification_element=SETTINGS_NETWORK_PORT_FORWARDING_NEXT)
    SETTINGS_NETWORK_WORKGROUP_TEXT = ElementInfo(name='Network Workgroup Text',
                                                  locator='settings_networkWorkgroup_text',
                                                  page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_WORKGROUP_SAVE = ElementInfo(name='Network Workgroup Save',
                                                  locator='settings_networkWorkgroupSave_button',
                                                  page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_UPS_SWITCH = ElementInfo(name='Network UPS Switch',
                                              locator='css=#settings_networkUPS_switch+span .checkbox_container',
                                              page=Pages.NETWORK_TAB,
                                              control_type=TYPE_BUTTON)
    SETTINGS_NETWORK_LINK_AGGREGATION_SAVE = ElementInfo(name='Network Link Aggregation Save',
                                                         locator='settings_networkBondingSave_button',
                                                         page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_LINK_AGGREGATION_SELECT = ElementInfo(name='Network Link Aggregation Select',
                                                           locator='settings_networkBonding_select',
                                                           page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_LINK_SPEED_SAVE = ElementInfo(name='Network Link Speed Save',
                                                   locator='settings_networkSpeedSave0_button',
                                                   page=Pages.NETWORK_TAB)
    SETTINGS_NETWORK_LINK_SPEED_SELECT = ElementInfo(name='Network Link Speed Select',
                                                     locator='settings_networkSpeed0_select',
                                                     page=Pages.NETWORK_TAB)
    # Settings -> Media
    # TODO: SETTINGS_MEDIA_DLNA_TITLE is not unique. This same locator appears on other pages.
    # Need to find something unique or this can't be used as a verification element.
    SETTINGS_MEDIA_DLNA_TITLE = ElementInfo(name='Media DLNA Title',
                                            locator='//div[@id=\'mainbody\']/div',
                                            page=Pages.MEDIA_TAB)
    MEDIA_BUTTON = ElementInfo(name='Media Button',
                               locator='settings_media_link',
                               page=Pages.MEDIA_TAB)
    RESCAN_MEDIA_BUTTON = ElementInfo(name='Rescan Media Button',
                                      locator='settings_mediaDLNARescan_button',
                                      page=Pages.MEDIA_TAB)
    REBUILD_MEDIA_BUTTON = ElementInfo(name='Rebuild Media Button',
                                       locator='settings_mediaDLNARebuild_button',
                                       page=Pages.MEDIA_TAB)
        
    SETTINGS_MEDIA_STREAMING = ElementInfo(name='Media Streaming Switch',
                                           locator='css = #settings_mediaDLNA_switch+span .checkbox_container',
                                           page=Pages.MEDIA_TAB)
    SETTINGS_MEDIA = ElementInfo(name='Media Button',
                                 locator='settings_media_link',
                                 page=Pages.SETTINGS,
                                 verification_element=SETTINGS_MEDIA_STREAMING)

    SETTINGS_MEDIA_ITUNES_SWITCH = ElementInfo(name='iTunes switch',
                                               locator='css=#settings_mediaiTunes_switch+span .checkbox_container',
                                               page=Pages.MEDIA_TAB)
    SETTINGS_MEDIA_ITUNES_REFRESH = ElementInfo(name='iTunes Database Refresh Button',
                                                locator='settings_mediaiTunesRefresh_button',
                                                page=Pages.MEDIA_TAB)
    SETTINGS_MEDIA_ITUNES_ADVANCED_LINK = ElementInfo(name='iTunes Advanced Link',
                                                      locator='css=#settings_mediaiTunesAdvOpts_link > span._text',
                                                      page=Pages.MEDIA_TAB)
    SETTINGS_MEDIA_ITUNES_ADVANCED_DIAG = ElementInfo(name='iTunes Advanced Diag',
                                                      locator='MediaDiag_iTuens_Advanced_Options')
    SETTINGS_MEDIA_ITUNES_CANCEL = ElementInfo(name='iTunes Advanced Diag Cancel Button',
                                               locator='settings_mediaiTunesCancel_button')
    SETTINGS_MEDIA_ITUNES_ADVANCED_PWD_SWITCH = ElementInfo(name='Password switch',
                                                            locator='css=#settings_mediaiTunesPWD_switch+span .checkbox_container')

    # Settings -> Utilities
    SETTINGS_UTILITY_QUICKTEST = ElementInfo(name='Utility Quicktest Button',
                                             locator='settings_utilitiesQuickTest_button',
                                             page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY = ElementInfo(name='Utility Button',
                                   locator='css=#settings_utilities_link > span._text',
                                   page=Pages.SETTINGS,
                                   verification_element=SETTINGS_UTILITY_QUICKTEST)
    SETTINGS_UTILITY_FULLTEST = ElementInfo(name='Utility Fulltest Button',
                                            locator='settings_utilitiesFullTest_button',
                                            page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_RESTORE = ElementInfo(name='Utility Restore Button',
                                           locator='settings_utilitiesReset_button',
                                           page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_RESTORE_TO_DEFAULT = ElementInfo(name='Utility Restore to Default',
                                                      locator='css=#id_utilitiesReset > span._text',
                                                      page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_VIEWLOGS = ElementInfo(name='Utility View Logs Button',
                                            locator='css=#settings_utilitiesLogs_button',
                                            page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_SCANDISK = ElementInfo(name='Scan Disk Button',
                                            locator='settings_utilitiesScanDisk_button',
                                            page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_SCANDISK_VOL_SELECT = ElementInfo(name='Scan Disk Volume Select',
                                                       locator='settings_utilitiesScanDisk_volume',
                                                       page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_FORMATDISK = ElementInfo(name='Format Disk Button',
                                              locator='settings_utilitiesFormatDisk_button',
                                              page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_FORMATDISK_VOL_SELECT = ElementInfo(name='Format Disk Volume select',
                                                         locator='settings_utilitiesFormatDisk_volume',
                                                         page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_SHUTDOWN = ElementInfo(name='Device Maintenance Shutdown',
                                            locator='settings_utilitiesShutdown_button',
                                            page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_REBOOT = ElementInfo(name='Device Maintenance Reboot',
                                          locator='settings_utilitiesReboot_button',
                                          page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_SAVE_CONFIG_FILE = ElementInfo(name='Save Configuration file ',
                                                    locator='settings_utilitiesConfig_button',
                                                    page=Pages.UTILITIES_TAB)
    SETTINGS_UTILITY_IMPORT_CONFIG_FILE = ElementInfo(name='Import Configuration file ',
                                                      locator='id_file',
                                                      page=Pages.UTILITIES_TAB)
    # Settings -> Notifications
    SETTINGS_NOTIFICATION_EMAIL_TITLE = ElementInfo(name='Notification Email Title',
                                                    locator='css=div.h1_content.header_2 > span._text',
                                                    page=Pages.NOTIFICATIONS_TAB)
    SETTINGS_NOTIFICATION_EMAIL_ENABLE = ElementInfo(name='Notification Email Button',
                                                     locator='css=label.checkbox_off',
                                                     page=Pages.NOTIFICATIONS_TAB)
    SETTINGS_NOTIFICATION = ElementInfo(name='Notification Link',
                                        locator='css=#settings_notifications_link > span._text',
                                        page=Pages.SETTINGS,
                                        verification_element=SETTINGS_NOTIFICATION_EMAIL_TITLE)
    # Settings -> Firmware Update
    SETTINGS_FIRMWARE_CHECKUPDATE = ElementInfo(name='Firmware Check Update Button',
                                                locator='settings_fwCheckUpdate_button',
                                                page=Pages.FIRMWARE_UPDATE_TAB)
    SETTINGS_FIRMWARE_AUTOUPDATE = ElementInfo(name='Firmware Auto Update Button',
                                               locator='css=span.checkbox_off_text',
                                               page=Pages.FIRMWARE_UPDATE_TAB)
    SETTINGS_FIRMWARE_AUTOUPDATE_TEXT = ElementInfo(name='Firmware Auto Update Button Text',
                                                    locator='css=#settings_fwAutoupdate_switch+span .checkbox_container',
                                                    page=Pages.FIRMWARE_UPDATE_TAB)
    SETTINGS_FIRMWARE_UPDATE_CHECK = ElementInfo(name='Firmware Update Check Button',
                                                 locator='settings_fwCheckUpdate_button',
                                                 page=Pages.FIRMWARE_UPDATE_TAB)
    SETTINGS_FIRMWARE_UPDATE_FILE = ElementInfo(name='Update from File',
                                                locator='css=#id_file.upload_button',
                                                page=Pages.FIRMWARE_UPDATE_TAB)
    SETTINGS_FIRMWARE = ElementInfo(name='Firmware Update Link',
                                    locator='css=#settings_firmware_link > span._text',
                                    page=Pages.SETTINGS,
                                    verification_element=SETTINGS_FIRMWARE_CHECKUPDATE)

    SETTINGS_WEB_UI_TIMEOUT_OPTION = ElementInfo(name='Web UI Timeout LisT Option',
                                                 locator='settings_generalTimeoutLi*',
                                                 page=Pages.SETTINGS)
    SETTINGS_WEB_UI_TIMEOUT = ElementInfo(name='Web UI Timeout',
                                          locator='id_timeout',
                                          page=Pages.SETTINGS)
    SETTINGS_WEB_UI_TIMEOUT_SCROLLBAR = ElementInfo(name='Web UI Timeout Scrollbar',
                                                    locator='css=div.jspDrag',
                                                    page=Pages.SETTINGS)

    NETWORK_LINK_SPEED = ElementInfo(name='Link Speed',
                                     locator='#settings_networkSpeedSave0_button',
                                     page=Pages.NETWORK_TAB,
                                     control_type=TYPE_LIST,
                                     is_hidden=True)
    # Non Admin Home page:
    NON_ADMIN_HOME_FTP_DOWNLOAD_ICON = ElementInfo(name='User Home FTP Download Icon',
                                                   locator='ftp_download_arrow',
                                                   page=Pages.NON_ADMIN_HOME)
    NON_ADMIN_HOME_HTTP_DOWNLOAD_ICON = ElementInfo(name='User Home HTTP Download Icon',
                                                    locator='http_download_arrow',
                                                    page=Pages.NON_ADMIN_HOME)
    NON_ADMIN_HOME_P2P_DOWNLOAD_ICON = ElementInfo(name='User Home P2P Download Icon',
                                                   locator='p2p_download_arrow',
                                                   page=Pages.NON_ADMIN_HOME)
    NON_ADMIN_HOME_APPS_ARROW = ElementInfo(name='User Home Apps Arrow',
                                            locator='user_home_apps_link',
                                            page=Pages.NON_ADMIN_HOME)
    NON_ADMIN_HOME_WD_DESKTOP_APP_LINK = ElementInfo(name='WD Desktop App Link',
                                                     locator="//a[@id='wd_cloud_text_url']/span",
                                                     page=Pages.NON_ADMIN_HOME)
    # Non Admin ToolBar:
    NON_ADMIN_TOOLBAR_LOGOUT = ElementInfo(name='Logout Toolbar',
                                           locator='logout_toolbar')
    NON_ADMIN_TOOLBAR_LOGOUT_LOGOUT = ElementInfo(name='Logout logout link',
                                                  locator='home_logout_link')
    # Non Admin- Download page:
    NON_ADMIN_DOWNLOAD_HTTP_DOWNLOAD = ElementInfo(name='User Download HTTP Download',
                                                   locator='http_downloads',
                                                   page=Pages.NON_ADMIN_DOWNLOAD)
    NON_ADMIN_DOWNLOAD_FTP_DOWNLOAD = ElementInfo(name='User Download FTP Download',
                                                  locator='ftp_downloads',
                                                  page=Pages.NON_ADMIN_DOWNLOAD)
    # Non Admin- Web File Viewer page:
    NON_ADMIN_WEB_FILE_VIEWER = ElementInfo(name='Web File Viewer',
                                            locator='nav_wfs_link',
                                            page=Pages.NON_ADMIN_WEB_FILE_VIEWER)
    # Non Admin Apps Page
    NON_ADMIN_APPS_HEADER_TEXT = ElementInfo(name='User Apps Header Text',
                                             locator='css=div.header_1 > span._text')
    NON_ADMIN_APPS_TITLE_TEXT = ElementInfo(name='User Apps Title Text',
                                            locator='app_show_name')
    NON_ADMIN_APPS_CONFIGURE_BUTTON = ElementInfo(name='User Apps Configure Button',
                                                  locator='apps_config_button')


# noinspection PyPep8Naming
class _page_info(namedtuple('ElementInfo', ['name',
                                            'link',
                                            'locator',
                                            'verification_element',
                                            'parent_page',
                                            'string_tag'])):
    # noinspection PyInitNewSignature
    def __new__(cls, name, link, locator, verification_element, parent_page=None, string_tag=None):
        return super(_page_info, cls).__new__(cls, name, link, locator, verification_element, parent_page, string_tag)


# noinspection PyPep8Naming
class Page_Info(object):
    """ This class contains a list of all pages and information on each page

    """
    # Page information
    #
    # name - A friendly name for the page
    # link - The navigation link that is clicked to switch to the page
    # locator - The locator string used to find the button for the page. Can be the same as link.
    # verification_element - An element unique to the page that is used
    # to confirm that the page was switched to correctly
    # parent_page - If present, this is a tab and parent_page refers to the page (or tab) containing the tab.
    # string_tag - Maps to the tag in the string file that contains the expected string for the page name

    # A dictionary containing the _page_info tuple for each page
    info = {Pages.HOME: _page_info(Pages.HOME,
                                   'nav_dashboard_link',
                                   'nav_dashboard',
                                   Elements.HOME_USER_ARROW,
                                   string_tag=('_menu_title', 'home')),
            Pages.NON_ADMIN_HOME: _page_info(Pages.NON_ADMIN_HOME,
                                             'nav_dashboard_link',
                                             'nav_dashboard',
                                             Elements.NON_ADMIN_WEB_FILE_VIEWER,
                                             string_tag=('_menu_title', 'home')),
            Pages.NON_ADMIN_WEB_FILE_VIEWER: _page_info(Pages.NON_ADMIN_WEB_FILE_VIEWER,
                                                        'nav_wfs_link',
                                                        'nav_wfs_link',
                                                        'mainFrame'),
            Pages.NON_ADMIN_DOWNLOAD: _page_info(Pages.NON_ADMIN_DOWNLOAD,
                                                 'nav_downloads_link',
                                                 'nav_downloads',
                                                 Elements.NON_ADMIN_DOWNLOAD_HTTP_DOWNLOAD),
            Pages.USERS: _page_info(Pages.USERS,
                                    'nav_users_link',
                                    'nav_users',
                                    Elements.USERS_CREATE_USER),
            Pages.USERS_TAB: _page_info(Pages.USERS_TAB,
                                        Elements.USERS_BUTTON,
                                        Elements.USERS_BUTTON,
                                        Elements.USERS_CREATE_USER,
                                        Pages.USERS),
            Pages.GROUPS_TAB: _page_info(Pages.GROUPS_TAB,
                                         Elements.GROUPS_BUTTON,
                                         Elements.GROUPS_BUTTON,
                                         Elements.GROUPS_CREATE_GROUP,
                                         Pages.USERS_TAB),
            Pages.SHARES: _page_info(Pages.SHARES,
                                     'nav_shares_link',
                                     'nav_shares',
                                     Elements.SHARES_CREATE_SHARE),
            Pages.CLOUD: _page_info(Pages.CLOUD,
                                    'nav_cloudaccess_link',
                                    'nav_remoteaccess',
                                    Elements.CLOUD_ADMIN_ICON),
            Pages.ADMIN_TAB: _page_info(Pages.ADMIN_TAB,
                                        Elements.CLOUD_ADMIN_ICON,
                                        Elements.CLOUD_ADMIN_ICON,
                                        Elements.CLOUD_ADMIN_SIGN_UP,
                                        Pages.CLOUD),
            Pages.BACKUPS: _page_info(Pages.BACKUPS,
                                      'nav_backups_link',
                                      'nav_safepoints',
                                      Elements.BACKUPS_USB),
            Pages.USB_BACKUPS_TAB: _page_info(Pages.USB_BACKUPS_TAB,
                                              Elements.BACKUPS_USB,
                                              Elements.BACKUPS_USB,
                                              Elements.BACKUPS_CREATE_USB_JOB,
                                              Pages.BACKUPS),
            Pages.REMOTE_BACKUPS_TAB: _page_info(Pages.REMOTE_BACKUPS_TAB,
                                                 Elements.BACKUPS_REMOTE,
                                                 Elements.BACKUPS_REMOTE,
                                                 Elements.BACKUPS_REMOTE_CREATE_JOB,
                                                 Pages.BACKUPS),
            Pages.INTERNAL_BACKUPS_TAB: _page_info(Pages.INTERNAL_BACKUPS_TAB,
                                                   Elements.BACKUPS_INTERNAL,
                                                   Elements.BACKUPS_INTERNAL,
                                                   Elements.BACKUPS_INTERNAL_CREATE_JOB,
                                                   Pages.BACKUPS),
            Pages.CLOUD_BACKUPS_TAB: _page_info(Pages.CLOUD_BACKUPS_TAB,
                                                Elements.BACKUPS_CLOUD,
                                                Elements.BACKUPS_CLOUD,
                                                Elements.BACKUPS_CLOUD_GRAPHIC,
                                                Pages.BACKUPS),
            Pages.CAMERA_BACKUPS_TAB: _page_info(Pages.CAMERA_BACKUPS_TAB,
                                                 Elements.BACKUPS_CAMERA,
                                                 Elements.BACKUPS_CAMERA,
                                                 Elements.BACKUPS_CAMERA_TRANSFER,
                                                 Pages.BACKUPS),
            Pages.STORAGE: _page_info(Pages.STORAGE,
                                      'nav_storage_link',
                                      'nav_storage',
                                      Elements.STORAGE_RAID),
            Pages.RAID_TAB: _page_info(Pages.RAID_TAB,
                                       Elements.STORAGE_RAID,
                                       Elements.STORAGE_RAID,
                                       Elements.RAID_CHANGE_RAID_MODE,
                                       Pages.STORAGE),
            Pages.DISK_STATUS_TAB: _page_info(Pages.DISK_STATUS_TAB,
                                              Elements.STORAGE_DISK_STATUS,
                                              Elements.STORAGE_DISK_STATUS,
                                              Elements.STORAGE_DISK_STATUS_LIST,
                                              Pages.STORAGE),
            Pages.ISCSI_TAB: _page_info(Pages.ISCSI_TAB,
                                        Elements.STORAGE_ISCSI,
                                        Elements.STORAGE_ISCSI,
                                        Elements.STORAGE_ISCSI_TITLE,
                                        Pages.STORAGE),
            Pages.VOLUME_VIRTUALIZATION_TAB: _page_info(Pages.VOLUME_VIRTUALIZATION_TAB,
                                                        Elements.STORAGE_VOLUME_VIRTUALIZATION,
                                                        Elements.STORAGE_VOLUME_VIRTUALIZATION,
                                                        Elements.STORAGE_VOLUME_VIRTUALIZATION_CREATE,
                                                        Pages.STORAGE),
            Pages.APPS: _page_info(Pages.APPS,
                                   'nav_apps_link',
                                   'nav_addons',
                                   Elements.APPS_HTTP_DOWNLOAD),
            Pages.HTTP_DOWNLOADS_TAB: _page_info(Pages.HTTP_DOWNLOADS_TAB,
                                                 Elements.APPS_HTTP_DOWNLOAD,
                                                 Elements.APPS_HTTP_DOWNLOAD,
                                                 Elements.APPS_HTTP_DOWNLOAD_CREATE,
                                                 Pages.APPS),
            Pages.FTP_DOWNLOADS_TAB: _page_info(Pages.FTP_DOWNLOADS_TAB,
                                                Elements.APPS_FTP_DOWNLOAD,
                                                Elements.APPS_FTP_DOWNLOAD,
                                                Elements.APPS_FTP_DOWNLOAD_CREATE,
                                                Pages.APPS),
            Pages.P2P_DONWNLOAD_TAB: _page_info(Pages.P2P_DONWNLOAD_TAB,
                                                Elements.APPS_P2P_DOWNLOAD,
                                                Elements.APPS_P2P_DOWNLOAD,
                                                Elements.APPS_P2P_DOWNLOAD_TITLE,
                                                Pages.APPS),
            Pages.WEB_FILE_VIEWER_TAB: _page_info(Pages.WEB_FILE_VIEWER_TAB,
                                                  Elements.APPS_WEB_FILE,
                                                  Elements.APPS_WEB_FILE,
                                                  Elements.APPS_WEB_FILE_FRAME,
                                                  Pages.APPS),
            Pages.SETTINGS: _page_info(Pages.SETTINGS,
                                       'nav_settings_link',
                                       'nav_settings',
                                       Elements.GENERAL_BUTTON),
            Pages.GENERAL_TAB: _page_info(Pages.GENERAL_TAB,
                                          Elements.GENERAL_BUTTON,
                                          Elements.GENERAL_BUTTON,
                                          Elements.GENERAL_DEVICE_NAME,
                                          Pages.SETTINGS),
            Pages.DATE_TIME_SETTINGS: _page_info(Pages.DATE_TIME_SETTINGS,
                                                 Elements.GENERAL_DATE_TIME_CONFIGURE,
                                                 Elements.GENERAL_DATE_TIME_CONFIGURE,
                                                 Elements.DATE_TIME_SAVE,
                                                 Pages.GENERAL_TAB),
            Pages.NETWORK_TAB: _page_info(Pages.NETWORK_TAB,
                                          Elements.NETWORK_BUTTON,
                                          Elements.NETWORK_BUTTON,
                                          Elements.NETWORK_MAC_ADDRESS,
                                          Pages.SETTINGS),
            Pages.MEDIA_TAB: _page_info(Pages.MEDIA_TAB,
                                        Elements.SETTINGS_MEDIA,
                                        Elements.SETTINGS_MEDIA,
                                        Elements.SETTINGS_MEDIA_STREAMING,
                                        Pages.SETTINGS),
            Pages.UTILITIES_TAB: _page_info(Pages.UTILITIES_TAB,
                                            Elements.SETTINGS_UTILITY,
                                            Elements.SETTINGS_UTILITY,
                                            Elements.SETTINGS_UTILITY_QUICKTEST,
                                            Pages.SETTINGS),
            Pages.NOTIFICATIONS_TAB: _page_info(Pages.NOTIFICATIONS_TAB,
                                                Elements.SETTINGS_NOTIFICATION,
                                                Elements.SETTINGS_NOTIFICATION,
                                                Elements.SETTINGS_NOTIFICATION_EMAIL_TITLE,
                                                Pages.SETTINGS),
            Pages.FIRMWARE_UPDATE_TAB: _page_info(Pages.FIRMWARE_UPDATE_TAB,
                                                  Elements.SETTINGS_FIRMWARE,
                                                  Elements.SETTINGS_FIRMWARE,
                                                  Elements.SETTINGS_FIRMWARE_CHECKUPDATE,
                                                  Pages.SETTINGS),
            }