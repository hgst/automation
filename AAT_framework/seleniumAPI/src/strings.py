# -*- coding: UTF-8 -*-
__author__ = 'Troy Hoffman'

import os
import inspect

from global_libraries import wd_exceptions as exception
from global_libraries import CommonTestLibrary
from ui_map import Elements
from ui_map import Page_Info
from _strings_en_us import StringList

class Strings(object):
    def __init__(self, language='en_US'):
        self.default_values = StringList.default_values
        # self.default_values = None
        #
        # this_file = inspect.getfile(inspect.currentframe())
        # this_path = os.path.split(this_file)[0]
        # language_string = language.replace('_', '-')
        # language_file = os.path.join(this_path, 'languages', language_string, 'english_{}.xml'.format(language_string.lower()))
        #
        # if os.path.exists(language_file):
        #     # Go through the UI map and map each supported element to the string from the XML file
        #     all_pages = CommonTestLibrary.get_class_attributes(Page_Info)
        #     for page in all_pages:
        #         print page.name
        #
        # else:
        #     raise exception.UnsupportedLanguage('Web UI is set to {}, which is not supported'.format(language))
        #
        # print 'done'
        # #self.default_values = StringList.default_values