""" This file is used by the WfSeleniumClient class to navigate the UI

"""
from collections import namedtuple

from ui_map import ElementInfo

class Pages(object):
    """ This class contains a list of the pages.
        They must also be added to the Page_Info class to define them.
    """
    SIGN_IN_PAGE = 'Sign in page'
    LOG_IN_PAGE = 'Log in page'


class Elements(object):
    """ This class contains UI elements and information on each element
    """
    HOME_DEVICE_LIST = ElementInfo(name='Device list - Home page',
                                   locator='left_nav_device')
    HOME_CONNECTING_TEXT = ElementInfo(name='Connecting text - Home',
                                       locator='css=inital_loag_icns h1')

    LOG_IN_USERNAME = ElementInfo(name='Username text field',
                                  locator='visible.username')
    LOG_IN_PASSWORD = ElementInfo(name='Password text field',
                                  locator='visible.password')
    LOG_IN_BUTTON = ElementInfo(name='Sign in button',
                                locator='submit',
                                verification_element=HOME_DEVICE_LIST)

    SIGN_IN_BUTTON = ElementInfo(name='Sign In Button',
                                 locator='login_form_submit_button',
                                 verification_element=LOG_IN_BUTTON)


# Named tuple for page information. Overrides base class since some attributes do not apply. See the Page_Info class
# for details on each attribute.
class _PageInfo(namedtuple('ElementInfo', ['verification_element',
                                           'navigation_element',
                                           'parent_page'])):
    # noinspection PyInitNewSignature
    def __new__(cls, verification_element, navigation_element=None, parent_page=None):
        return super(_PageInfo, cls).__new__(cls,
                                             verification_element,
                                             navigation_element,
                                             parent_page)

# noinspection PyPep8Naming
class Page_Info(object):
    """ This class contains a list of all pages and information on each page. The verification_element and
        navigation_element values will be references to an attribute in the Elements class.

        verification_element - The element used to verify that the UI is on the page. It must be unique to the page
                               and must always be visible when on the page.
        navigation_element - Optional. The element to click to switch to the page. If no parent page is specified,
                             this element must always be visible from anywhere on the web UI. If a parent page is
                             specified, this element must always be visible on the parent page.
        parent_page - Optional. The page that contains the navigation element. If specified, the framework will
                      switch to the parent_page before clicking the navigation element.

    """

    # The pages are stored in a dictionary to make lookups easier
    info = {
        Pages.SIGN_IN_PAGE:         _PageInfo(verification_element=Elements.SIGN_IN_BUTTON),
        Pages.LOG_IN_PAGE:          _PageInfo(verification_element=Elements.LOG_IN_BUTTON)}


