# # @package wd_wizards
# Created on March 14, 2015
# @brief This module contains classes used to navigate through wizard-based functions (such as RAID)
# @author: hoffman_t
#
# Each wizard inherits the _Wizard base class and must define the following instance variables:
# self._pages - A dictionary that defines the controls on each page
# self._open_element - The element (as defined in the ui_map) that launches the wizard
# self._wizard_name - A friendly name for the wizard
# self._raid_level - The desired RAID level
# self._first_page - The first page the wizard goes to on launch
""" WD Wizard Class

"""
from collections import namedtuple
import time
from selenium.webdriver.common.keys import Keys

from ui_map import Elements
from seleniumAPI.src.seleniumclient import ElementNotReady
from global_libraries import wd_exceptions as exception
# noinspection PyPep8Naming
from global_libraries import CommonTestLibrary as ctl
from global_libraries.performance import Stopwatch
from global_libraries.testdevice import Fields


# noinspection PyPep8Naming
class _wizard_page_info(namedtuple('WizardPageInfo', ['verification_element',
                                                      'next_button',
                                                      'on_next',
                                                      'next_page',
                                                      'back_button',
                                                      'on_back',
                                                      'previous_page',
                                                      'cancel_button',
                                                      'close_button'])):
    # noinspection PyInitNewSignature
    def __new__(cls,
                verification_element,
                next_button=None,
                on_next=None,
                next_page=None,
                back_button=None,
                on_back=None,
                previous_page=None,
                cancel_button=None,
                close_button=None):
        return super(_wizard_page_info, cls).__new__(cls,
                                                     verification_element,
                                                     next_button,
                                                     on_next,
                                                     next_page,
                                                     back_button,
                                                     on_back,
                                                     previous_page,
                                                     cancel_button,
                                                     close_button)


class _Wizard(object):
    def __init__(self, test_client):
        self.log = ctl.configure_logger('AAT.wd_wizards')
        # If True, output messages showing what would be clicked, but don't actually open the UI
        self._debug_flow = False

        # Page 0 means the wizard isn't opened
        self._current_page = None

        # The element in the UI that raises the wizard
        self._open_element = None

        # The first page
        self._first_page = None

        # Used in messages and exceptions to report which Wizard is running
        self._wizard_name = ''

        # The test client
        self._test = test_client

        self._pages = {}

        # If a page is added to this list, it will be skipped when moving forward or backward through the wizard.
        # This is useful if a device option or setting changes the flow of the wizard.
        self._skip_pages = []

    def open(self):
        """ Open the wizard


        :raise exception.WizardElementUndefined: If there is no open element defined for the wizard
            exception.WizardElementUndefined: If the wizard is already opened
        """
        self.log.info('Opening {} wizard'.format(self._wizard_name))
        if self._current_page:
            raise exception.WizardAlreadyOpened(
                'Attempt to open {} wizard when it is already opened'.format(self._wizard_name))
        else:
            if self._open_element:
                if self._debug_flow:
                    self.log.info('Clicking {}'.format(self._open_element))
                else:
                    self._test.click_element(self._open_element)

                self._current_page = self._first_page
            else:
                raise exception.WizardElementUndefined(
                    'No _open_element defined for {} wizard'.format(self._wizard_name))

    def next(self, time_to_wait=5, retries=1):
        """ Goes to the next page

        :param time_to_wait: Maximum amount of time to wait for the next page's verification element to appear
        :param retries: The number of retries that will be attempted at validating the page switch before failing
        :raise exception.WizardElementUndefined: If there is no next button defined for the current page
        """
        self._switch_page(action='next', time_to_wait=time_to_wait, retries=retries)

    def back(self, time_to_wait=5, retries=1):
        """ Goes to the previous page

        :param time_to_wait: Maximum amount of time to wait for the next page's verification element to appear
        :param retries: The number of retries that will be attempted at validating the page switch before failing
        :raise exception.WizardElementUndefined: If there is no next button defined for the current page
        """
        self._switch_page(action='back', time_to_wait=time_to_wait, retries=retries)

    def _switch_page(self, action, time_to_wait, retries):
        self.log.info('Clicking {}'.format(action))

        if action == 'next':
            button = self._pages[self._current_page].next_button
            on_click = self._pages[self._current_page].on_next
            # Determine the destination page
            skip_page = True
            this_page = self._current_page
            while skip_page:
                destination_page = self._pages[this_page].next_page
                if destination_page in self._skip_pages:
                    this_page = destination_page
                else:
                    skip_page = False

        elif action == 'back':
            button = self._pages[self._current_page].back_button
            on_click = self._pages[self._current_page].on_back

            skip_page = True
            this_page = self._current_page
            while skip_page:
                destination_page = self._pages[this_page].back_page
                if destination_page in self._skip_pages:
                    this_page = destination_page
                else:
                    skip_page = False
        else:
            raise exception.InvalidParameter('Action of {} is not valid. Must be either "next" or "back"')

        current_verification_element = self._pages[self._current_page].verification_element
        destination_verification_element = self._pages[destination_page].verification_element

        if not button:
            raise exception.WizardElementUndefined('No {} button on page "{}" of {} wizard'.format(action,
                                                                                                   self._current_page,
                                                                                                   self._wizard_name))

        for remaining_attempts in range(retries + 1, 0, -1):
            if self._debug_flow:
                self.log.info('Clicking {} to get to {} page'.format(button, destination_page))
            else:
                if self._test.is_element_visible(current_verification_element, wait_time=1):
                    self._test.click_element(button)

            if on_click:
                if self._debug_flow:
                    self.log.info('Running function {}'.format(on_click))
                else:
                    self._run_functions(on_click)

            if time_to_wait > 0:
                # See if the page switched
                time.sleep(0.25)
                if self._debug_flow:
                    self.log.info('Verifying element {} is visible'.format(destination_verification_element))
                    self._current_page = destination_page
                    return
                else:
                    if self._test.is_element_visible(destination_verification_element, time_to_wait):
                        # Page switched, so update and run the on_click function
                        self._current_page = destination_page

                        return
                    else:
                        if remaining_attempts > 1:
                            self.log.info('Click did not work. Retrying after 2 seconds.')
                            time.sleep(2)
                        else:
                            raise exception.WizardUnknownPage('Could not switch page')
            else:
                # Just blindly switch to the page
                self._current_page = destination_page

    def cancel(self):
        """ Clicks the cancel button to close the wizard


        :raise exception.WizardElementUndefined: If no cancel button is defined for the current page
        """
        self.log.info('Clicking cancel')
        cancel_button = self._pages[self._current_page].cancel_button
        if cancel_button:
            if self._debug_flow:
                self.log.info('Clicking {}'.format(cancel_button))
            else:
                self._test.click_element(cancel_button)

            self._current_page = None
        else:
            raise exception.WizardElementUndefined(
                'No Cancel button on page "{}" of {} wizard'.format(self._current_page,
                                                                    self._wizard_name))

    def close(self):
        """ Clicks the Close button to close the wizard

        :raise exception.WizardElementUndefined: If no close button is defined for the current page
        """
        if self._current_page:
            self.log.info('Clicking close')
            close_button = self._pages[self._current_page].close_button
            if close_button:
                if self._debug_flow:
                    self.log.info('Clicking {}'.format(close_button))
                else:
                    self._test.click_element(close_button, check_is_ready=False)

                self._current_page = None
            else:
                raise exception.WizardElementUndefined(
                    'No Close button on page "{}" of {} wizard'.format(self._current_page,
                                                                       self._wizard_name))

    def _is_page_loaded(self, page_number, wait_time=5):
        # Verify that the page loaded
        page_info = self._pages[page_number]
        if page_info.verification_element:
            # Confirm the element is present
            if self._test.is_element_visible(page_info.verification_element, wait_time=wait_time):
                return True
            else:
                return False
        else:
            raise exception.WizardElementUndefined(
                'No verification element for page "{}" of {} wizard'.format(page_number,
                                                                            self._wizard_name))

    def go_to_page(self, desired_page):
        """ Go to the desired page in the wizard, opening the wizard if necessary

        :param desired_page: The name of the page to go to
        :return: :raise exception.WizardUnknownPage: If the page is undefined
        """
        if not self._current_page:
            self.open()

        if self._current_page != desired_page:
            # Determine if we have to go forward or backward
            next_page = self._pages[self._current_page].next_page
            while next_page:
                if next_page == desired_page:
                    # Found it, so start clicking next until we get to it
                    while True:
                        self.next()
                        if self._current_page == desired_page:
                            return

                next_page = self._pages[next_page].next_page

            # Didn't find, so move backwards
            previous_page = self._pages[self._current_page].previous_page
            while previous_page:
                if previous_page == desired_page:
                    # Found it, so start clicking back until we get to it
                    while True:
                        self.back()
                        if self._current_page == desired_page:
                            return
                previous_page = self._pages[previous_page].previous_page
            raise exception.WizardUnknownPage('Could not get to {}'.format(desired_page))

    @staticmethod
    def _run_functions(function_list):
        try:
            # noinspection PyUnusedLocal
            dummy = function_list[0]
        except TypeError:
            # It's not a list or tuple, so make it one
            function_list = [function_list]

        for function in function_list:
            function()
            time.sleep(0.5)


class RAIDWizard(_Wizard):
    """ The Configure RAID wizard

    :param test_client: The test client object for the current test (needed to access Selenium functions)
    :param raid_level: The desired RAID level. 0 - 10 are RAID levels 0 through 10. 11 is JBOD and 12 is Spanning
    """

    def __init__(self, test_client, raid_level):
        super(RAIDWizard, self).__init__(test_client)
        # Page names:
        self.WARNING = 'Warning'
        self.CHANGE_RAID_MODE = 'Change RAID Mode'
        self.SELF_TEST = 'Self Test'
        self.SIZE = 'Size'
        self.SIZE_VOLUME2 = 'Size for Volume 2'
        self.ENCRYPT_VOLUMES = 'Encrypt Volumes'
        self.AUTO_REBUILD = 'Auto Rebuild'
        self.SUMMARY = 'Summary'
        self.CONFIRMATION = 'Confirmation'
        self.PARTITIONING = 'Partitioning'
        self.FORMATTING = 'Formatting'
        self.RESULTS = 'RAID Results'

        self._open_element = Elements.RAID_CHANGE_RAID_MODE
        self._wizard_name = 'RAID'
        self._raid_level = raid_level
        self._first_page = self.WARNING

        self._pages = {self.WARNING: _wizard_page_info(verification_element='error_dialog_message_list',
                                                       next_button='popup_apply_button',
                                                       next_page=self.CHANGE_RAID_MODE,
                                                       back_button=None,
                                                       cancel_button='popup_close_button',
                                                       close_button=None),
                       self.CHANGE_RAID_MODE: _wizard_page_info(verification_element='raid_main_menu_std',
                                                                next_button='storage_raidFormatNext1_button',
                                                                next_page=self.SELF_TEST,
                                                                on_next=[self._wait_for_update,
                                                                         self._wait_for_selftest],
                                                                back_button=None,
                                                                cancel_button='storage_raidFormatCancel1_button',
                                                                close_button=None),
                       self.SELF_TEST: _wizard_page_info(verification_element='dskDiag_physical_info',
                                                         next_button='storage_raidFormatNext2_button',
                                                         next_page=self.SIZE,
                                                         back_button='storage_raidFormatBack2_button',
                                                         previous_page=self.CHANGE_RAID_MODE,
                                                         cancel_button='storage_raidFormatCancel2_button',
                                                         close_button=None),
                       self.SIZE: _wizard_page_info(verification_element='dskDiag_raidsize_1st_desc1',
                                                    next_button='storage_raidFormatNext4_button',
                                                    next_page=self.ENCRYPT_VOLUMES,
                                                    back_button='storage_raidFormatBack4_button',
                                                    previous_page=self.SELF_TEST,
                                                    cancel_button='storage_raidFormatCancel4_button',
                                                    close_button=None),
                       self.SIZE_VOLUME2: _wizard_page_info(verification_element='dskDiag_raidsize_2nd_desc2',
                                                            next_button='storage_raidFormatNext5_button',
                                                            next_page=self.ENCRYPT_VOLUMES,
                                                            back_button='storage_raidFormatBack5_button',
                                                            previous_page=self.SIZE,
                                                            cancel_button='storage_raidFormatCancel5_button',),
                       self.AUTO_REBUILD: _wizard_page_info(verification_element='dskDiag_rebuild_set',
                                                            next_button='storage_raidFormatNext3_button',
                                                            next_page=self.ENCRYPT_VOLUMES,
                                                            back_button='storage_raidFormatBack3_button',
                                                            previous_page=self.SIZE,
                                                            cancel_button='storage_raidFormatCancel3_button',
                                                            close_button=None),
                       self.ENCRYPT_VOLUMES: _wizard_page_info(verification_element='dskDiag_volume_encrpty_list',
                                                               next_button='storage_raidFormatNext11_button',
                                                               next_page=self.SUMMARY,
                                                               back_button='storage_raidFormatBack11_button',
                                                               previous_page=self.SIZE,
                                                               cancel_button='storage_raidFormatCancel11_button',
                                                               close_button=None),
                       self.SUMMARY: _wizard_page_info(verification_element='dskDiag_summary',
                                                       next_button='storage_raidFormatNext6_button',
                                                       next_page=self.CONFIRMATION,
                                                       back_button='storage_raidFormatBack6_button',
                                                       previous_page=self.ENCRYPT_VOLUMES,
                                                       cancel_button='storage_raidFormatCancel6_button',
                                                       close_button=None),
                       self.CONFIRMATION: _wizard_page_info(verification_element='dskDiag_create_confirm',
                                                            next_button='storage_raidFormatNext16_button',
                                                            next_page=self.PARTITIONING,
                                                            back_button='storage_raidFormatBack16_button',
                                                            previous_page=self.SUMMARY,
                                                            cancel_button='storage_raidFormatCancel16_button',
                                                            close_button=None),
                       self.PARTITIONING: _wizard_page_info(verification_element='dskDiag_partitioning_wait',
                                                            next_button=None,
                                                            back_button=None,
                                                            cancel_button=None,
                                                            close_button=None),
                       self.FORMATTING: _wizard_page_info(verification_element='dskDiag_bar',
                                                          next_button=None,
                                                          back_button=None,
                                                          cancel_button=None,
                                                          close_button=None),
                       self.RESULTS: _wizard_page_info(verification_element='dskDiag_res',
                                                       next_button=None,
                                                       back_button=None,
                                                       cancel_button=None,
                                                       close_button='storage_raidFormatFinish9_button')}

        if not self._test.uut[Fields.supports_volume_encryption]:
            # Skip the volume encryption
            self._skip_pages.append(self.ENCRYPT_VOLUMES)

        if self._raid_level > 10:
            self._skip_pages.append(self.SIZE)

        if self._raid_level not in (1, 5, 10):
            self._skip_pages.append(self.AUTO_REBUILD)

    def _wait_for_update(self):
        self.log.info('Updating...')
        timer = Stopwatch(timer=60)
        timer.start()
        update_dialog = 'css=.LightningUpdating,unselect,updateStr'
        self._test.wait_until_element_is_visible(update_dialog)

        while self._test.is_element_visible(update_dialog):
            time.sleep(1)
            if timer.is_timer_reached():
                raise exception.ActionTimeout('"Updating" did not finish in time')

    # Closes the wizard, if open, and changes the raid leve to the specified level
    def set_level(self, raid_level):
        """ Closes the Wizard and changes the RAID level

        :param raid_level: The new RAID level. 0 - 10 are RAID levels 0 through 10. 11 is JBOD and 12 is spanning
        """
        self.close()
        self._raid_level = raid_level

    def _select_raid_button(self):
        # Elements to select each RAID level. 11 is JBOD, 12 is spanning
        # Note that the element locator for spanning is called raid_main_menu_jbod.
        # This is how it is in the UI and not a typo in this code.
        raid_level_buttons = {11: 'raid_main_menu_std',
                              12: 'raid_main_menu_jbod',
                              0: 'raid_main_menu_r0',
                              1: 'raid_main_menu_r1',
                              5: 'raid_main_menu_r5',
                              10: 'raid_main_menu_r10'}

        self.go_to_page(self.CHANGE_RAID_MODE)

        self.log.info('Selecting RAID level {}'.format(self._raid_level))
        raid_button = raid_level_buttons[self._raid_level]
        if self._debug_flow:
            self.log.info('Clicking {}'.format(raid_button))
        else:
            self._test.click_element(raid_button)

        time.sleep(0.5)
        # Check the box to switch to the new RAID mode
        checkbox = 'css=.LightningCheckbox,storage_raidFormatType1_chkbox'
        if self._debug_flow:
            self.log.info('Checking checkbox {}'.format(checkbox))
        else:
            self._test.select_checkbox(checkbox)

    def _wait_for_selftest(self):
        # Wait for the drive tests to reach 'Good' status
        self.log.info('Waiting for self test to complete')

        self._test.wait_until_element_is_clickable('storage_raidFormatNext2_button', timeout=360)

    # encryption_dictionary:
    # <volume>: (password, True/False for automount)
    # noinspection PyPep8,PyProtectedMember
    def format(self,
               downgrade_to_jbod=False,
               expand_capacity=False,
               limit_capacity=False,
               remainder_as_spanning=False,
               raid5_spare=False,
               migrate_to_raid_1=False,
               create_new_volumes=False):
        # Check the options
        """ Walk through the wizard to configure RAID and format the drive

        :param downgrade_to_jbod: Only valid with JBOD (level 11) when coming from RAID 1.
                                  Switches to JBOD without data loss.
        :param expand_capacity: Only valid with RAID 1. Allows resizing the volume without data loss.
        :param limit_capacity: Limits the capacity of the RAID volume Not valid for spanning or JBOD.
        :param remainder_as_spanning: Configures the remaining space as spanning. Also enables limit_capacity.
        :param raid5_spare: If True, configure RAID 5 with a hot spare
        :param migrate_to_raid_1: If True, migrate to RAID 1 from JBOD
        :param create_new_volumes: If True, creates new volumes
        :raise exception.InvalidRAIDLevel: If one of the options is not valid for the current RAID level
        """

        current_raid = self._test.uut.get('forceCurrentRAID')  # Set this dictionary value when debugging

        if current_raid is None:
            current_raid = self._test.get_raid_level()

        number_of_drives = self._test.uut.get('forceNumberOfDrives')  # Set this dictionary value when debugging

        if number_of_drives is None:
            number_of_drives = self._test.get_drive_count()

        ctl.validate_raid_path(current_raid=current_raid,
                               target_raid=self._raid_level,
                               number_of_drives=number_of_drives,
                               downgrade_to_jbod=downgrade_to_jbod,
                               expand_capacity=expand_capacity,
                               limit_capacity=limit_capacity,
                               remainder_as_spanning=remainder_as_spanning,
                               raid5_spare=raid5_spare,
                               migrate_to_raid_1=migrate_to_raid_1,
                               create_new_volumes=create_new_volumes)

        if current_raid == -1:
            # No RAID configured, so it will use Set up RAID Mode, not Change RAID Mode
            self._open_element = Elements.STORAGE_SETUP_RAID_MODE
        else:
            self._open_element = Elements.STORAGE_CHANGE_RAID_MODE

        self.log.info('Formatting as RAID level {}. Current level is {}'.format(self._raid_level, current_raid))

        if limit_capacity or remainder_as_spanning:
            if not limit_capacity:
                # Only remainder_as_spanning was set, so default to 50 GiB
                limit_capacity = True

        if self._raid_level == 1 and self._test.get_drive_count() > 3:
            # Add the size page for the second volume on 4+ drive systems
            self._pages[self.SIZE] = self._pages[self.SIZE]._replace(next_page=self.SIZE_VOLUME2)

        # First, get to the RAID mode selector page
        self.go_to_page(self.CHANGE_RAID_MODE)

        # Click the appropriate control
        self._select_raid_button()

        if downgrade_to_jbod:
            self.log.info('Downgrading to JBOD')
            self._test.select_checkbox('tr_r5_r12std')

        if expand_capacity:
            self.log.info('Extending capacity')
            self._test.select_checkbox('tr_r1_expan')  # Locator is correct and does not have a 'd' at the end

        if limit_capacity or expand_capacity:
            # Get to the capacity page
            self.go_to_page(self.SIZE)

            volume_size_element = 'amount_right'
            slider_element = '//div[@id=\'slider_volume1\']/a'

            starting_volume_size = self._test.get_text(volume_size_element)

            self._test.wait_until_element_is_clickable(slider_element)
            if expand_capacity:
                # Press the "End" key
                self._test.send_keys(element=slider_element, keystrokes=Keys.END, check_page=False, do_login=False)
            else:
                # Press Home, and then page up
                self._test.send_keys(element=slider_element, keystrokes=Keys.HOME, check_page=False, do_login=False)
                self._test.send_keys(element=slider_element, keystrokes=Keys.PAGE_UP, check_page=False, do_login=False)

            new_size = self._test.get_text(volume_size_element)

            self.log.info('Resized from {} to {}'.format(starting_volume_size, new_size))

        if remainder_as_spanning:
            self._test.select_checkbox('storage_raidFormat1stSpanning4_chkbox')

        # Get to the partitioning page
        self.go_to_page(self.PARTITIONING)
        self.log.info('Unit is being partitioned')

        # Wait for the formatting page
        formatting_element = self._pages[self.FORMATTING].verification_element
        if self._debug_flow:
            self.log.info('Waiting for {}'.format(formatting_element))
        else:
            self._test.wait_until_element_is_visible(formatting_element, timeout=120)

        self._current_page = self.FORMATTING

        # Wait for the close button to appear with up to a 5 minute timeout without the progress changing
        timer = Stopwatch(timer=300)
        old_progress = ''
        old_state = ''
        while True:
            if self._debug_flow:
                self.log.info('Formatting complete')
                self._current_page = self.RESULTS
                break

            if self._test.is_element_visible('formatdsk_state', wait_time=1):
                try:
                    state = self._test.get_text('formatdsk_state')
                except ElementNotReady:
                    state = ''

            if self._test.is_element_visible('formatdsk_desc', wait_time=1):
                try:
                    progress = self._test.get_text('formatdsk_desc')
                except ElementNotReady:
                    progress = ''

            if self._test.is_element_visible(self._pages[self.RESULTS].close_button, wait_time=1):
                self._current_page = self.RESULTS
                self.log.info('Format complete')
                break
            else:
                if progress == old_progress and state == old_state:
                    if timer.is_timer_reached():
                        raise exception.ActionTimeout('Time out waiting for formatting.')
                else:
                    # It changed
                    if progress != old_progress:
                        old_progress = progress

                    if state != old_state:
                        old_state = state

                    self.log.info('{} - {}'.format(state, progress))
                    timer.reset(keep_running=True)

        # Close the wizard
        self.close()

        # TODO: Finish Encrypt volume(s)
        # if encryption_dictionary:
        # for volume in encryption_dictionary:
        # self.log.info('Encrypting volume {}'.volume)
        # self._test.click_element('RAIDEncrytionVol_{}_l'.format(volume))  # The typo in the locator is in the UI
        #
        # password, automount = encryption_dictionary[volume]
        # self._test.set_password('storage_raidFormatVE1stPWD12_password', password, do_login=False, check_browser=False, check_page=False)
        # self._test.set_password('storage_raidFormatVE1stConfirmPWD12_password', password, do_login=False, check_browser=False, check_page=False)
        #         # Automount = storage_raidFormatVE1stAutoMount12_switch
        #         # Next = storage_raidFormatNext12_button
        #         # Back = storage_raidFormatBack12_button
        #         # Cancel = storage_raidFormatCancel12_button
        #         # All locators increment for each volume

        # RAID Size
        # slider_volume1 -> percentage
        # storage_raidFormat1stSpanning4_chkbox -> Configure remaining as spanning

    def _get_volume_size(self):
        volume_size = self._test.get_text('amount_right')
        return int(volume_size.split()[0])






