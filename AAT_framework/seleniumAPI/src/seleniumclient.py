"""
Created on June 29, 2014

@author: vasquez_c


"""
import time
import string

import Selenium2Library

from global_libraries.testdevice import Fields

# noinspection PyPep8Naming
from global_libraries import CommonTestLibrary as ctl
from global_libraries.performance import Stopwatch
from global_libraries import wd_exceptions
from powerSwitchAPI.src.powerswitchclient import PowerSwitchClient as PowerSwitch
from selenium.common.exceptions import StaleElementReferenceException, InvalidSelectorException
from selenium.webdriver.common.action_chains import ActionChains
from BeautifulSoup import BeautifulSoup
import requests

from restAPI.src.restclient import RestClient


class PageLoadException(Exception):
    pass


class ElementNotReady(Exception):
    pass

# ===================================================================================
# Class SELENIUMClient
# ===================================================================================

DEFAULT_TIMEOUT = 60

# noinspection PyBroadException
class SeleniumClient(object):

    # noinspection PyProtectedMember
    def __init__(self, uut, wait_time=DEFAULT_TIMEOUT):
        self.uut = uut
        self.driver = Selenium2Library.Selenium2Library()
        self.implicit_wait_time = wait_time
        self.driver.set_selenium_implicit_wait(self.implicit_wait_time)
        self.log = ctl.configure_logger('AAT.seleniumclient')
        self.browser_cache = self.driver._cache
        self.no_browser = self.driver._cache._no_current
        self._rest = RestClient(self.uut)  # Needed for access_webUI to accept the EULA
        self.requests_cookies = None
        self._browser = self.uut[Fields.uut_browser]

        # Set register keyword to run on failure to Nothing to avoid warning message
        self.driver.register_keyword_to_run_on_failure('Nothing')

        # Imports were moved here so the classes can be overriden by a custom ui_map
        global Elements
        from ui_map import Elements

        global Page_Info
        from ui_map import Page_Info

        global ElementInfo
        from ui_map import ElementInfo

        global Pages
        from ui_map import Pages

    def _call_selenium_function(self, func, locator):
        element = self._build_element_info(locator)
        if self.is_ready(element):
            return func(element.locator)
        else:
            raise ElementNotReady(element.name)

    # noinspection PyProtectedMember,PyPep8Naming
    def access_webUI(self, use_ssl=False, do_login=True):
        """
        Opens a new browser and if a password is set for the
        admin, inputs it. If not, then it skips immediately to
        the main page of the webUI

        :param use_ssl: If True, connect with SSL
        :param do_login: If True, login if necessary
        """
        self.check_browser_state(use_ssl=use_ssl, do_login=do_login)

    # # @brief Logs into the web UI
    # noinspection PyPep8Naming
    def login_webUI(self, check_browser=True):
        # TODO: Since firmware 1.05.02, admin is auto login, add a check whether a pw is set before login process
        username = self.uut[Fields.web_username]
        password = self.uut[Fields.web_password]

        if check_browser:
            # See if the browser is ready. Do NOT login, since that will cause recursion
            self.check_browser_state(do_login=False)

        if not self._is_browser():
            # Open a new browser instance, but use go_to_url since access_webUI will call login and create a loop
            self.go_to_url()

        # Check if the login is even needed
        if self.is_element_visible(Elements.LOGIN_USERNAME, wait_time=1):
            self.input_text(Elements.LOGIN_USERNAME, username, do_login=False, check_browser=False)

            if password != ('' and None):
                self.input_password(Elements.LOGIN_PASSWORD, password, do_login=False, check_browser=False)

            self.click_button(Elements.LOGIN_BUTTON, do_login=False, check_browser=False, timeout=5)

        # try to clear all popups before running test
        self.check_for_popups()

    # @ Brief This function only supports Fimrware branches 1.06.XX , NOT 1.05.xx
    #  The returned values can be: None, 0, 1 and 2
    #  "0" means : logged in to the UI, EULA page found and accepted and Main Content on the main page appeared
    #  "1" means : Logged in to the UI, but no EULA could be found
    #  "None" means: Failed to log in or validate the presence of one of the elements (such as login button, EUla checkbox, ....)
    #  "2" means : Logged in to the UI, EULA page found and accepted and now Mandatory Firmware update menu is shown up
    def accept_eula(self):
        results = None
        username = self.uut[Fields.web_username]
        password = self.uut[Fields.web_password]
        hostname = 'http'
        hostname += '://{0}'.format(self.uut[Fields.internal_ip_address])
        # Launch browser and Navigate to webUI
        try:
            self.driver.open_browser(hostname, browser=self._browser)
            self.driver.reload_page()
        except Exception:
            # Try one more time
            self.log.info('Failed to open browser. Trying again.')
            self._wait(5)
            self.driver.open_browser(hostname, browser=self._browser)
            self.driver.reload_page()
        time.sleep(4)
        # at this point the is visible hits 1.06.xx Branches and higher first and check if EULA CONTINUE BUTTON is there
        if self.is_visible(element=Elements.EULA_CONTINUE, timeout=5):
            if self.is_element_visible(element=Elements.EULA_GETTING_STARTED_CANCEL0, wait_time=5):
                self.click_button(locator=Elements.EULA_GETTING_STARTED_CANCEL0, timeout=5, check_is_ready=False, check_page=False, do_login=False)
            if self.is_element_visible(element=Elements.EULA_GETTING_STARTED_CANCEL1, wait_time=5):
                self.click_button(locator=Elements.EULA_GETTING_STARTED_CANCEL1, timeout=5, check_is_ready=False, check_page=False, do_login=False)
            status = False
            count = 0
            while (not status) and (count <= 3):
                try:
                    self.select_checkbox(locator=Elements.EULA_CHECKBOX, timeout=5)
                    #self.click_element(Elements.EULA_CHECKBOX)
                    time.sleep(1)
                    # self.wait_until_element_is_visible(Elements.EULA_CHECKBOX)
                    self.click_element(locator=Elements.EULA_CONTINUE, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    time.sleep(2)

                    if self.is_visible(element='id_eula_current', timeout=5):
                        self.log.info('*******Mandatory Update Menu is open now******* ')
                        results = 2
                        break
                    else:
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT0, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT1, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT2, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        time.sleep(5)
                        if self.is_visible(element=Elements.MAIN_CONTENT, timeout=5):
                            results = 0
                            break
                    count += 1
                except Exception:
                    self.log.warning(ctl.exception('Failed to accept EULA'))
                    status = False
                    count += 1
        else:
            try:
                if self.is_visible(element=Elements.LOGIN_USERNAME, timeout=5):
                    time.sleep(1)
                    self.input_text(Elements.LOGIN_USERNAME, username)
                    time.sleep(1)
                    if password != '':
                        self.input_password(Elements.LOGIN_PASSWORD, password)
                    time.sleep(1)
                    self.click_element(locator=Elements.LOGIN_BUTTON_WO_VER, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    if self.is_visible(element=Elements.MAIN_CONTENT, timeout=5):
                        results = 1

                if self.is_visible(element=Elements.EULA_GETTING_STARTED_NEXT1, timeout=5):
                    #self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT0, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    self.click_element(locator=Elements.EULA_GETTING_STARTED_CANCEL1, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    #self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT2, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    if self.is_visible(element=Elements.MAIN_CONTENT, timeout=7):
                        results = 1
                elif self.is_visible(element=Elements.EULA_CONTINUE, timeout=5):
                    self.select_checkbox(locator=Elements.EULA_CHECKBOX, timeout=5)
                    #self.click_element(Elements.EULA_CHECKBOX)
                    time.sleep(1)
                    self.click_element(locator=Elements.EULA_CONTINUE, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    time.sleep(1)
                    results = 0
                    if self.is_visible(element='id_eula_current', timeout=5):
                        self.log.info('*******Mandatory Update Menu is open now******* ')
                        results = 2
                    elif self.is_visible(element=Elements.EULA_GETTING_STARTED_NEXT1, timeout=5):
                        #self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT0, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_CANCEL1, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        #self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT2, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                    elif self.is_visible(element=Elements.EULA_GETTING_STARTED_NEXT0, timeout=5):
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT0, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT1, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)
                        self.click_element(locator=Elements.EULA_GETTING_STARTED_NEXT2, timeout=5, check_is_ready=False, check_page=False, attempts=2, do_login=False)

            except Exception, ex:
                self.log.info('Failed to log in first time ... Trying again.....' + str(ex))
                self.click_button(locator=Elements.LOGIN_BUTTON, timeout=5, check_is_ready=False, check_page=False, do_login=False)
                if self.is_visible(element=Elements.MAIN_CONTENT, timeout=5):
                    results = 1
                else:
                    results = None
                pass
        return results

    def set_timeout(self, timeout):
        if timeout:
            self.driver.set_selenium_implicit_wait(timeout)

    def reset_timeout(self):
        self.driver.set_selenium_implicit_wait(self.implicit_wait_time)

    def get_element(self, element, timeout=None):
        locator = self._get_locator(element)
        self.set_timeout(timeout)

        try:
            # noinspection PyProtectedMember
            element = self.driver._element_find(locator, True, True)
        except Exception:
            self.log.debug(ctl.exception('Error getting element: ' + element))
            raise
        finally:
            self.reset_timeout()

        return element

    # noinspection PyProtectedMember
    def is_present(self, element, timeout=None):
        self.set_timeout(timeout)
        present = None

        try:
            if self.get_element(element):
                present = True
        except Exception:
            self.log.debug(ctl.exception('Error when trying to see if element is present. Assuming not enabled: ' + element))
            raise
        finally:
            self.reset_timeout()
            return present

    # noinspection PyProtectedMember
    def is_enabled(self, element, timeout=None):
        element = self._build_element_info(element)
        self.set_timeout(timeout)

        start_time = time.time()

        try:
            enabled = self.driver._is_enabled(element.locator)
        except AttributeError:
            self.log.debug('Element has no enabled attribute. Assuming enabled')
            return True
        except AssertionError:
            self.log.debug('Assertion error. Assuming enabled')
            enabled = True
        except ValueError:
            self.log.debug('Element not found')
            return False
        finally:
            self.reset_timeout()

        if enabled:
            # Confirm it's not grayed out
            while True:
                attribute = self.driver.get_element_attribute(str(element.locator) + '@class')
                if 'gray_out' not in attribute:
                    return True
                else:
                    time.sleep(0.5)
                    if time.time() - start_time >= timeout:
                        self.log.debug('Element is greyed out')
                        return False
        else:
            return False

    # noinspection PyProtectedMember
    def is_visible(self, element, timeout=None):
        locator = self._get_locator(element)
        self.set_timeout(timeout)

        visible = None
        self.driver.set_selenium_implicit_wait(timeout)

        try:
            visible = self.driver._is_visible(locator)

        except Exception:
            self.log.debug(ctl.exception('Error when trying to see if element is visible. Assuming not visible: ' + locator))
            raise
        finally:
            self.reset_timeout()
            return visible

    # noinspection PyPep8Naming
    def access_webUI_https(self):
        """ Wrapper for access_webUI with use_ssl set to True """
        self.access_webUI(use_ssl=True)

    # #@ Brief Go to the specified URL
    #
    # @param url: The URL to go to. If it does not contain //, it assumes the URL is relative to the UUT. If not passed,
    #             it opens the root of the UUT. This makes it similar to access_webUI, except it does not attempt to
    #             clear the EULA or login.
    # @param use_ssl: If true, use HTTPS. If the the URL contains //, this is ignored
    # @param port: The port to use. If the URL contains //, this is ignored
    def go_to_url(self, url=None, use_ssl=False, port=None):
        # First, determine what the actual URL will be
        if url:
            if '//' not in url:
                # No protocol defined, so use the UUT as the host
                hostname = 'http'
                if use_ssl:
                    hostname = 'https'

                hostname += '://{0}'.format(self.uut[Fields.internal_ip_address])

                if port:
                    # Add the port
                    hostname += ':' + str(port)

                # Add a slash and the URL that was passed
                hostname += '/' + url
            else:
                # Use as-is
                hostname = url
        else:
            url = 'http://{}'.format(self.uut[Fields.internal_ip_address])

        if not self._is_browser():
            # No browser, so create one
            self.driver.open_browser(url, browser=self._browser)
        else:
            # Go to the URL
            self.driver.go_to(hostname)

        status_code = self.get_page_status_code()

        if status_code != 200:
            raise PageLoadException('Error {} when loading {}. URL was {}'.format(status_code,
                                                                                  hostname,
                                                                                  url))

    def _is_browser(self):
        """ Check if a browser instance exists
        :return: True if a browser instance exists. False if no browser exists.
        """
        try:
            if self.browser_cache.current != self.no_browser:
                return True
            else:
                return False
        except Exception:
            return False

    # #@ Brief Attmempts to determine the status_code from the server based on the page contents
    #
    #    This is needed because the Selenium webdriver does not return the status_code. It does not support all status
    #    codes, nor will it work on all web pages.
    def get_page_status_code(self):
        title = self.driver.get_title().lower()

        if ('403' and 'forbidden') in title:
            code = 403
        elif ('404' and 'not found') in title:
            code = 404
        elif ('405' and 'method not allowed') in title:
            code = 405
        elif ('501' and 'not implemented') in title:
            code = 501
        elif ('503' and 'service unavailable') in title:
            code = 503
        else:
            code = 200

        if code != 200:
            self.log.debug('Page error of ' + str(code) + ' - ' + title)

        return code

    # noinspection PyPep8Naming
    def close_webUI(self):
        """
        Closes the web browser accessing the webUI
        """
        self.driver.close_browser()

    def close_all_browsers(self):
        self.driver.close_all_browsers()

    # noinspection PyProtectedMember
    def element_find(self, element, first_only=True, required=True, tag=None):
        locator = self._get_locator(element)
        return self.driver._element_find(locator, first_only, required, tag)

    @staticmethod
    def _wait(seconds):
        # Waits the specified number of seconds, displaying a count-down timer
        for i in range(seconds, 0, -1):
            print 'Waiting ' + str(i) + ' seconds'
            time.sleep(i)

    def _move_pagebar_button_on_screen(self, page_name):
        # Move the specified page_name's button to the visible screen

        page_info = Page_Info.info[page_name]

        is_button_offscreen = True
        old_x = None
        old_min_loc = None
        old_max_loc = None
        attempts = 0

        while is_button_offscreen:
            # First, determine the minimum and maximum values for the
            # placement of a visible button in the page bar
            pagebar_rect = self.element_find(Elements.PAGE_BAR).rect
            pagebar_x = pagebar_rect['x']

            rect = self.element_find(page_info.link).rect
            x = rect['x']
            width = rect['width']

            min_loc = pagebar_x + width / 2
            max_loc = pagebar_x + pagebar_rect['width'] - width / 2

            self.log.debug('Scrolling to page ' + str(page_name))
            self.log.debug('Control information:')
            self.log.debug('pagebar_rect: ' + str(pagebar_rect))
            self.log.debug('control rect: ' + str(rect))
            self.log.debug('min_loc: ' + str(min_loc))
            self.log.debug('max_loc: ' + str(max_loc))
            self.log.debug('x + width = ' + str(x + width))

            if old_x is not None:
                # Verify that something changed
                if x == old_x and min_loc == old_min_loc and max_loc == old_max_loc:
                    # Failed to move, so throw an exception
                    if attempts > 3:
                        raise wd_exceptions.CannotReachPage('Cannot scroll to ' + page_name + ' button.')
                    else:
                        attempts += 1
                        self.log.warning('Button click did not work. Retrying.' )
                else:
                    attempts = 0

            if x + width > min_loc and x < max_loc:
                is_button_offscreen = False
            else:
                self.log.debug('Button off page. Trying to get to it.')
                # Store the old values to avoid getting stuck in a loop
                # if clicking the arrow fails
                old_x = x
                old_min_loc = min_loc
                old_max_loc = max_loc

                # Click to move it on screen. Use _click_element_blind to avoid trying to switch to the page, which
                # will call this function, which then clicks and switches to the page, which calls this function,
                # etc. and creates an infinite loop and really bad things.
                if x < min_loc:
                    self.log.debug('Clicking left arrow')
                    self._click_element_blind(Elements.PAGE_BAR_LEFT.locator)
                else:
                    self.log.debug('clicking right arrow')
                    self._click_element_blind(Elements.PAGE_BAR_RIGHT.locator)

        return 0

    def is_already_on_page(self, page_name):
        verification_element = Page_Info.info[page_name].verification_element

        if verification_element is not None:
            return self.is_element_visible(verification_element,
                                           wait_time=1)
        else:
            self.log.debug('No verification element for the page. Assuming UI is not on the page')
            return False

    def _open_browser(self, use_ssl=False, accept_eula=True):
        if not self._is_browser():
            # No browser, so first accept the EULA
            if accept_eula:
                self.log.debug('Accepting the EULA')
                self._rest.accept_eula()
            else:
                self.log.debug('Not accepting the EULA')

            # Now, open the browser
            hostname = 'http'
            if use_ssl:
                hostname = 'https'

            hostname += '://{0}'.format(self.uut[Fields.internal_ip_address])
            self.log.info('Opening new browser instance to ' + hostname)
            self.driver.open_browser(hostname, browser=self._browser)

    def check_browser_state(self, do_login=True, accept_eula=True, attempts=2, use_ssl=False):
        """ Checks the browser state. If necessary it opens it, accepts the EULA, and logs in

        :param do_login: If True, login if needed
        :param accept_eula: If True, accept the EULA if needed
        :param attempts: The number of times to attempt to open the browser and login
        :param use_ssl: If True, use HTTPS
        """
        # This intentionally accesses the private _cache attribute of the driver.
        self._open_browser(use_ssl=use_ssl, accept_eula=accept_eula)
        # In #2.30.184, after reset to default, the login page will be skipped and enter EULA page directly
        # Check if the privacy dialog is popped up
        try:
            if self.get_text(Elements.PRIVACY_STATEMENT_DIALOG_TEXT, check_is_ready=False) == 'Privacy Statement':
                self.log.warning("Found Privacy Statement Dialog, close it")
                self.click_button(Elements.PRIVACY_STATEMENT_DIALOG_OK, check_browser=False)
        except:
            pass

        # See if we're on the login page and login if necessary
        if do_login:
            if self.is_element_visible(Elements.LOGIN_USERNAME, wait_time=0):
                for i in range(0, attempts):
                    try:
                        self.login_webUI(check_browser=False)
                        break
                    except ElementNotReady:
                        if i >= attempts - 1:
                            # Too many retries, raise the exception
                            raise

                        self.log.info('Could not login to web UI. ' +
                                      str(attempts) + ' attempts remaining.')

    def check_for_popups(self):
        # Give the dialog time to pop-up
        time.sleep(3)

        # Check if the RAID dialog is popped up
        if self.is_element_visible(Elements.RAID_CHANGE_RAID_MODE_CLOSE, wait_time=0):
            self.click_button(Elements.RAID_CHANGE_RAID_MODE_CLOSE, check_browser=False)

        # Check if the RAID ROAMING dialog is popped up
        if self.is_element_visible(Elements.RAID_ROAMING_CLOSE, wait_time=0):
            self.click_button(Elements.RAID_ROAMING_CLOSE, check_browser=False)

        # Check if Insert Hard Drive dialog is popped up, need reboot
        if self.is_element_visible(Elements.INSERT_HARD_DRIVE, wait_time=0):
            PowerSwitch.power_cycle()

            # wait 3 minutes then login again
            time.sleep(180)
            self.login_webUI()

    def get_to_page(self, page_name, retry=True, check_browser=True):
        """
        Gets to the specified page

        Acceptable Pages:
        -Home
        -Users
        -Shares
        -Cloud Access
        -Backups
        -Storage
        -Apps
        -Settings
        """

        if check_browser:
            self.check_browser_state()

        if page_name is None:
            # Element is not on a specific page, so return without doing anything
            return

        # First, get the page information
        # Get the page information
        page_info = Page_Info.info[page_name]

        # See if we're already on the page
        if self.is_already_on_page(page_name):
            self.log.debug('Already on the page, nothing to do')
            return

        self.log.debug('Page info: ' + str(page_info))

        if page_info.parent_page:
            # Get to the parent page first
            self.get_to_page(page_info.parent_page, check_browser=False)
            # Recheck to see if we're on the page (in case it's a sub page that's already active)
            # See if we're already on the page
            if self.is_already_on_page(page_name):
                self.log.debug('Subpage already active. Nothing more to do.')
                return

            # Now, click the correct element
            self.log.info('Going to page ' + str(page_name))
            self.click_element(page_info.link, check_is_ready=False)
        else:
            self.log.info('Going to page ' + str(page_name))
            # Get the link on screen
            if self._move_pagebar_button_on_screen(page_name) == 0:
                # Now, click the link
                self.click_element(page_info, check_is_ready=False, check_page=False)
            else:
                return 1
        # And verify that the verification element is visible
        try:
            self.wait_until_element_is_visible(page_info.verification_element, timeout=5)
        except Exception:
            self.log.debug(ctl.exception('Failed to get to page.'))
            if retry:
                self.log.info('Failed to switch to page. Waiting five seconds and retrying.')
                time.sleep(5)
                self.get_to_page(page_name, retry=False)
            else:
                self.log.warning('Could not confirm that page switch was successful. ' +
                                 str(page_info.verification_element.locator) + ' element not found')
                return 1

        return 0


    # create group is not working due to invalid html tag
    # noinspection PyPep8Naming
    def create_groups(self, numberOfgroups=1, group_name='group'):
        """
        Creates a given number of groups starting at
        group1 and being named sequentially
        """
        self.get_to_group_tab()
        for x in range(1, (numberOfgroups + 1)):
            self.click_element(Elements.GROUPS_CREATE_GROUP, check_page=False)
            # self.driver.wait_until_element_is_visible(Elements.USERS_ADD_GROUP_SAVE)
            group = '{0}{1}'.format(group_name, x)
            self.log.info('Creating group ' + group)
            # self.click_element(Elements.USERS_GROUP_NAME)
            self.input_text(Elements.GROUPS_GROUP_NAME, group, check_page=False)
            # time.sleep(2)
            self.click_element(Elements.GROUPS_ADD_GROUP_SAVE, check_page=False)
            self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)

    # noinspection PyPep8Naming
    def create_multiple_users(self, start_prefix=1, numberOfusers=1):
        """
        Creates a given number of users using the create multiple
        users button on the users page of the webUI. Users are named
        as follows: userX where X starts at the given start prefix
        and is continued sequentially until the proper number of
        users are created. The password for the created user is the
        default 'welc0me'
        """
        self.click_element(Elements.USERS_BUTTON)
        self.click_element(Elements.USERS_ADD_MULTIPLE)

        self.click_element(Elements.USERS_CREATE_MULTIPLE_USERS)
        time.sleep(1)

        self.click_element(Elements.USERS_CREATE_MULTIPLE_NEXT1)
        time.sleep(1)

        self.input_text(Elements.USERS_CREATE_MULTIPLE_NAME_PREFIX, 'user')
        self.input_text(Elements.USERS_CREATE_MULTIPLE_ACCOUNT_PREFIX, start_prefix)
        self.input_text(Elements.USERS_CREATE_MULTIPLE_NUMBER, numberOfusers)
        self.input_text(Elements.USERS_CREATE_MULTIPLE_PASSWORD, 'welc0me')
        self.input_text(Elements.USERS_CREATE_MULTIPLE_PASSWORD_CONFIRM, 'welc0me')

        self.click_element(Elements.USERS_CREATE_MULTIPLE_NEXT2)
        time.sleep(1)
        self.click_element(Elements.USERS_CREATE_MULTIPLE_NEXT3)
        time.sleep(1)
        self.click_element(Elements.USERS_CREATE_MULTIPLE_NEXT4)
        time.sleep(1)
        self.click_element(Elements.USERS_CREATE_MULTIPLE_SAVE)

        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING, time_out=300)

    # noinspection PyPep8Naming
    def create_shares(self, numberOfshares=1, share_name='share'):
        """
        Creates a given number of shares with the
        following names:
                        if number of shares to create is 1 and a new
                        share_name(other than default) is passed,
                        it will create the passed share without suffix.
                        if number of shares > 1 it will append an incremental suffix
                        to the share.
        """
        if numberOfshares == 1:
            self.click_button(Elements.SHARES_CREATE_SHARE)
            self.input_text(Elements.SHARES_ADD_NAME, share_name)
            self.click_button(Elements.SHARES_ADD_SAVE)
            time.sleep(3)
        else:    
            for x in range(1, (numberOfshares + 1)):
                self.click_button(Elements.SHARES_CREATE_SHARE)
                share = '{0}{1}'.format(share_name,x)
                self.input_text(Elements.SHARES_ADD_NAME, share)
            self.click_element(Elements.SHARES_ADD_SAVE)
            self.wait_until_element_is_clickable(locator=Elements.SHARES_CREATE_SHARE)


    def get_to_group_tab(self):
        """
        Gets to the group tab of the users page
        """
        self.click_element(Elements.GROUPS_BUTTON)

    def _get_visible_list_contents(self, element, prefix=''):
        # noinspection PyProtectedMember
        visible_list = self.driver._element_find(element.locator, first_only=True, required=True)
        time.sleep(0.5)
        contents = visible_list.text
        content_list = []
        for line in contents.split('\n'):
            if line.startswith(prefix):
                content_list.append(line.strip())

        return content_list

    def _get_to_end_of_list(self, arrow, timer=None):
        while 'gray_out' not in self.driver.get_element_attribute(arrow.locator + '@class'):
            self.click_element(arrow)
            time.sleep(0.5)
            if timer.is_timer_reached():
                raise wd_exceptions.ActionTimeout('Timeout getting to end of list')

    def select_item_in_list(self, list, item_name, item_locator, timeout=60, start_at_top=True):
        """ Selects an item in the list

        :param list: The list. Must be an element, not a locator string
        :param item_name: The visible name of the item to select
        :param item_locator: The locator string of the item
        :param timeout: The maximum amount of time to spend trying to select the item
        :param start_at_top: If True (default), start at the top. If False, start at the bottom
        :return:
        """
        if list.control_type != Elements.TYPE_LIST:
            raise wd_exceptions.InvalidElementType(list.name + ' must be a list type.')

        # Get to the top or bottom of the list
        if start_at_top:
            arrow = list.up_arrow
            next_arrow = list.down_arrow
        else:
            arrow = list.down_arrow
            next_arrow = list.up_arrow

        timer = Stopwatch(timer=timeout)
        timer.start()
        self._get_to_end_of_list(arrow, timer)

        while item_name not in self._get_visible_list_contents(list):
            if timer.is_timer_reached():
                    raise wd_exceptions.ActionTimeout('Timeout getting the rest of the list contents')

            if 'gray_out' not in self.driver.get_element_attribute(next_arrow.locator + '@class'):
                self.click_element(next_arrow)
            else:
                raise wd_exceptions.ItemNotInList(item_name + ' not found in ' + list.name)

        self.click_element(item_locator)


    def get_list_contents(self, element, prefix='', timeout=60):
        """ Get the content of a list
        :param element - The list. This must be an element object, not just a locator, because it requires the
                         up_arrow and down_arrow attributes.
        :param prefix - If specified, only return items that start with prefix
        :param timeout - The maximum amount of total time spent getting the contents, not counting the time getting
                         to the element.

        :return: A list containing the items in the UI list
        """
        # If None is passed for prefix, convert to an empty string
        if not prefix:
            prefix = ''

        if element.control_type != Elements.TYPE_LIST:
            raise wd_exceptions.InvalidElementType(element.name + ' must be a list type.')

        if not self.is_ready(element):
            raise ElementNotReady(element.name + ' is not ready.')

        up_arrow = element.up_arrow
        down_arrow = element.down_arrow

        # Start the stopwatch
        sw = Stopwatch(timer=timeout)
        sw.start()

        # Get to the top of the list
        self._get_to_end_of_list(up_arrow, sw)

        # At the top, so gather the visible elements
        contents = self._get_visible_list_contents(element, prefix)

        # Now, move down
        while 'gray_out' not in self.driver.get_element_attribute(down_arrow.locator + '@class'):
            self.click_element(down_arrow)
            if sw.is_timer_reached():
                raise wd_exceptions.ActionTimeout('Timeout getting the rest of the list contents')

            next_contents = self._get_visible_list_contents(element, prefix)
            for item in next_contents:
                if item not in contents:
                    contents.append(item)

        return contents

    def delete_groups(self, group_name='', timeout=60):
        """ Deletes all groups

        :param group_name: If specified, only delete groups that start with group_name
        :param timeout: The maximum amount of time to wait for each group to be deleted
        """
        if not group_name:
            group_name = ''

        groups = self.get_list_contents(Elements.GROUPS_LIST, group_name)

        if len(groups) > 0:
            self.log.info('Deleting ' + str(len(groups)) + ' groups.')

            for group_name in groups:
                self.log.info('Deleting group ' + group_name)
                self.select_item_in_list(Elements.GROUPS_LIST, group_name, 'users_group_' + group_name, timeout=timeout)
                self.click_element(Elements.GROUPS_REMOVE_GROUP, check_page=False)
                self.click_element(Elements.GROUPS_REMOVE_OK, check_page=False)

                # Waiting for Elements.UPDATING_STRING to disappear was not reliable, especially when the drive
                # had been in standby and "Updating" took too long to disappear. Instead, wait for the group to
                # disappear from the list.
                sw = Stopwatch(timer=timeout)
                sw.start()

                while True:
                    new_group_list = self._get_visible_list_contents(Elements.GROUPS_LIST, group_name)

                    if group_name in new_group_list:
                        # Not deleted yet. Check timeout, then wait 1 second
                        if sw.is_timer_reached():
                            # Timeout reached
                            raise wd_exceptions.ActionTimeout('Timeout waiting for group to be deleted')
                        else:
                            time.sleep(0.5)
                    else:
                        time.sleep(1)
                        break
        else:
            self.log.debug('No groups to delete.')

    ##@ Brief Change web access timeout value
    #
    # @param timeOut: timeout value, optional, default is 30 minutes
    #
    def set_web_access_timeout(self, newTO='30'):
        timeout = 'link={} minutes'.format(newTO)
        if str(newTO) in self.get_text(Elements.SETTINGS_WEB_UI_TIMEOUT):
            self.log.info('Timeout value is already set to {0} minutes.'.format(newTO))
            return

        self.log.debug('Setting web access timeout value to: {0} minutes'.format(newTO))
        self.click_element(Elements.SETTINGS_WEB_UI_TIMEOUT)

        # Scroll to the top of the list
        at_top = False
        scroll_bar = Elements.SETTINGS_WEB_UI_TIMEOUT_SCROLLBAR
        last_position = None
        while not at_top:
            if not self.is_element_visible(scroll_bar):
                # Re-open the list
                self.click_element(Elements.SETTINGS_WEB_UI_TIMEOUT)

            position = self.get_element_attribute(scroll_bar, 'style')
            if position != last_position:
                last_position = position
                self.drag_and_drop_by_offset(scroll_bar.locator, 0, -30)
            else:
                at_top = True

        # Now, scroll down if element is not displayed on drop down menu
        timeout_found = False
        last_position = None

        while not timeout_found:
            # if drop down menu closed, re-open it
            if not self.is_element_visible(scroll_bar):
                self.click_element(Elements.SETTINGS_WEB_UI_TIMEOUT)

            # if element is visible, exit loop
            if self.is_element_visible(timeout):
                timeout_found = True
                self.click_element(timeout)
                self.log.info('Set web access timeout value to: {0} minutes successfully'.format(newTO))
            else:
                self.log.debug('Timeout value: {0} minutes not visible on dropdown menu'.format(newTO))

                # scroll down
                self.drag_and_drop_by_offset(scroll_bar.locator, 0, 30)

                position = self.get_element_attribute(scroll_bar, 'style')
                if position != last_position:
                    last_position = position
                else:
                    # Didn't move, so at bottom
                    raise wd_exceptions.ItemNotInList('Requested value of {0} minutes is not in the list'.format(newTO))

    def get_dropdown_list_options(self, locator, attribute='textContent'):
        """ Get a dictionary of all options in a drop down list

        :param locator: The locator to use. Add an asterisk to use a wildcard
        :param attribute: The attribute to use for the text. Default is textContent.

        :return: A dictionary of options, with the key being the 'rel' attribute (position in list)
        """
        element = self._build_element_info(locator)
        self.get_to_page(element.page)

        search_text = element.locator
        if search_text.endswith('*'):
            search_text = "//li[starts-with(@id, '{}')]".format(search_text[:-1])

        option_dictionary = {}
        options = self.call_selenium_driver_function(self.driver._element_find, search_text, False, False)
        for option in options:
            rel = option.get_attribute('rel')
            children = option.find_elements_by_css_selector('li a')
            for child in children:
                option_dictionary[rel] = child.get_attribute(attribute)

        return option_dictionary


    # @Brief Select an item from drop down menu
    #
    # @param dropdown_menu: menu where item will be selected
    # @param dropdown_item: item which will be selected from menu
    #
    def select_item_drop_down_menu(self, dropdown_menu, dropdown_item, scroll_value=20):
        count = 0

        # open drop down menu
        self.click_element(dropdown_menu)
        time.sleep(3)

        # if item is visible, select it
        if self.is_element_visible(dropdown_item):
            self.click_element(dropdown_item)
            self.log.info('Set value to: {0} successfully'.format(dropdown_item))
        else:
            # scroll down if element is not displayed on drop down menu
            while self.is_element_visible(dropdown_item) is False and count < 12:
                self.log.info('Item {0} not visible on drop down menu'.format(dropdown_item))

                # if drop down menu closed, re-open it
                if not self.is_element_visible('css=div.jspDrag'):
                    self.click_element(dropdown_menu)
                    time.sleep(3)

                # scroll down
                self.drag_and_drop_by_offset('css=div.jspDrag', 0, scroll_value)
                time.sleep(3)
                count += 1

                # if element is visible, exit loop
                if self.is_element_visible(dropdown_item):
                    break

                # re-open drop down menu
                self.click_element(dropdown_menu)
                time.sleep(3)

            if self.is_element_visible(dropdown_item):
                self.click_element(dropdown_item)
                self.log.info('Set value to: {0} successfully'.format(dropdown_item))
            else:
                self.log.info('Failed to set value to: {0}'.format(dropdown_item))

    def configure_jbod(self):
        """
        Sets the drive into JBOD mode
        """
        # Check if drive is already into JBOD

        self.get_to_page('Storage')
        time.sleep(3)
        # self.click_element('storage_raid_link')
        # self.wait_until_element_is_clickable(Elements.RAID_CHANGE_RAID_MODE)
        visible = self.is_element_visible(Elements.RAID_CHANGE_RAID_MODE)
        if visible == True:
            self.click_element(Elements.RAID_CHANGE_RAID_MODE)
            self.click_element(Elements.RAID_POPUP_APPLY)
        else:
            self.click_element('storage_raidSetupRAIDMode_button')
        # self.wait_until_element_is_clickable('popup_apply_button')

        #self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        status = False
        time.sleep(5)
        while not status:
            self.select_checkbox(Elements.RAID_SWITCH_MODE)

            try:
                #self.click_element('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
                self.click_button('storage_raidFormatNext1_button')
                self.click_button('storage_raidFormatNext2_button')
                status = True
            except Exception:
                status = False
        time.sleep(180)
        #self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        #self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        if self.uut[Fields.product] != 'Zion':
            self.click_button('storage_raidFormatNext11_button')
        #self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        #self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.driver.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.driver.element_should_contain(locator='id=0', expected='JBOD')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode JBOD was successful")

    def configure_spanning(self):
        """
        Sets the RAID to Spanning
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_element('storage_raid_link')
        time.sleep(3)
        self.wait_until_element_is_clickable(Elements.RAID_CHANGE_RAID_MODE)
        self.click_button(Elements.RAID_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        time.sleep(5)
        self.wait_until_element_is_clickable('css = #raid_main_menu_jbod > div.tlt')
        self.click_element('css = #raid_main_menu_jbod > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(210)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        if self.uut[Fields.product] != 'Zion':
            self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='Spanning')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode Spanning was successful")

    def configure_raid0(self):
        """
        Sets the RAID to RAID 0
        """
        self.get_to_page('Storage')
        time.sleep(5)
        self.click_element('storage_raid_link')
        time.sleep(5)
        self.wait_until_element_is_clickable('storage_raidChangeRAIDMode_button')
        self.click_button('storage_raidChangeRAIDMode_button')
        time.sleep(3)
        if self.is_element_visible('storage_raidChangeRAIDMode_button'):
            self.click_button('storage_raidChangeRAIDMode_button')
            time.sleep(3)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r0 > div.tlt')
        self.click_element('css = #raid_main_menu_r0 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_element('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        time.sleep(3)
        if self.uut[Fields.product] != 'Zion':
            self.click_button('storage_raidFormatNext11_button')
            time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='RAID 0')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 0 was successful")

    def configure_raid1(self):
        """
        Sets the RAID to RAID 1
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_element('storage_raid_link')
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidChangeRAIDMode_button')
        self.click_button('storage_raidChangeRAIDMode_button')
        time.sleep(3)
        if self.is_element_visible('storage_raidChangeRAIDMode_button'):
            self.click_button('storage_raidChangeRAIDMode_button')
            time.sleep(3)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r1 > div.tlt')
        self.click_element('css = #raid_main_menu_r1 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        time.sleep(5)
        try:
            self.wait_until_element_is_clickable('storage_raidFormatNext5_button')
            self.click_button('storage_raidFormatNext5_button')
        except Exception:
            pass
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_button('storage_raidFormatNext3_button')
        time.sleep(3)
        if self.uut[Fields.product] != 'Zion':
            self.click_button('storage_raidFormatNext11_button')
            time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.driver.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        # Confirm that the RAID mode to display
        self.driver.element_should_contain(locator='id=0', expected='RAID 1')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 1 was successful")

    def configure_raid5(self):
        """
        Sets the RAID to RAID 5
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_button('storage_raid_link')
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        self.click_button(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r5 > div.tlt')
        self.click_element('css = #raid_main_menu_r5 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_button('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=7200)
        self.element_should_contain(locator='id=0', expected='RAID 5')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        print 'Waiting for RAID parity check'
        time.sleep(43200)
        self.get_to_page('Storage')
        visible = self.is_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        while visible == False:
            time.sleep(3600)
            visible = self.is_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        self.log.info("Configure to RAID mode RAID 5 was successful")


    def configure_raid10(self):
        """
        Sets the RAID to RAID 10
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_element('storage_raid_link')
        time.sleep(5)
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        time.sleep(5)
        self.click_element(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        time.sleep(5)
        self.click_element('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r10 > div.tlt')
        time.sleep(5)
        self.click_element('css = #raid_main_menu_r10 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_element('storage_raidFormatNext4_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_element('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_element('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_element('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_element('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='RAID 10')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 10 was successful")


    def configure_raid0spanning(self):
        """
        Sets the RAID to RAID 0 with spanning
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_button('storage_raid_link')
        time.sleep(5)
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        time.sleep(5)
        self.click_button(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        time.sleep(5)
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r0 > div.tlt')
        time.sleep(5)
        self.click_element('css = #raid_main_menu_r0 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume1\']/a')
        self.drag_and_drop_by_offset('//div[@id=\'slider_volume1\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        self.click_button('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        if self.uut[Fields.product] != 'Zion':
            self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='RAID 0')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 0 w/ Spanning was successful")


    def configure_raid1spanning1(self):
        """
        Sets the RAID to RAID 1 with spanning
        """
        self.get_to_page('Storage')
        self.click_button('storage_raid_link')
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        self.click_button(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r1 > div.tlt')
        self.click_button('css = #raid_main_menu_r1 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_button('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume1\']/a')
        self.driver.drag_and_drop_by_offset('//div[@id=\'slider_volume1\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        self.click_button('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        time.sleep(3)
        self.click_button('storage_raidFormatNext4_button')
        time.sleep(3)
        visible = self.is_element_visible('storage_raidFormatNext5_button')
        if visible == True:
            self.click_button('storage_raidFormatNext5_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_button('storage_raidFormatNext3_button')
        if self.uut[Fields.product] != 'Zion':
            self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='RAID 1')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 1 w/ Spanning on first volume was successful")

    def configure_raid1spanning2(self):
        """
        Sets the RAID to RAID 1 with spanning
        """
        self.get_to_page('Storage')
        self.click_element('storage_raid_link')
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        self.click_element(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_element('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r1 > div.tlt')
        self.click_element('css = #raid_main_menu_r1 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        time.sleep(3)
        self.click_element('storage_raidFormatNext4_button')
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume2\']/a')
        self.driver.drag_and_drop_by_offset('//div[@id=\'slider_volume2\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat2ndSpanning5_chkbox+span')
        self.click_button('css = .LightningCheckbox #storage_raidFormat2ndSpanning5_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext5_button')
        self.click_element('storage_raidFormatNext5_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_element('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_element('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_element('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_element('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='RAID 1')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 1 w/ Spanning on first volume was successful")

    def configure_raid1spanningboth(self):
        """
        Sets the RAID to RAID 1 with spanning
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_element('storage_raid_link')
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidChangeRAIDMode_button')
        self.click_button('storage_raidChangeRAIDMode_button')
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r1 > div.tlt')
        self.click_element('css = #raid_main_menu_r1 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_element('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume1\']/a')
        self.driver.drag_and_drop_by_offset('//div[@id=\'slider_volume1\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        self.click_element('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        time.sleep(3)
        self.click_element('storage_raidFormatNext4_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume2\']/a')
        self.driver.drag_and_drop_by_offset('//div[@id=\'slider_volume2\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat2ndSpanning5_chkbox+span')
        self.click_element('css = .LightningCheckbox #storage_raidFormat2ndSpanning5_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext5_button')
        self.click_element('storage_raidFormatNext5_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_element('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_element('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_element('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_element('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.driver.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.driver.element_should_contain(locator='id=0', expected='RAID 1')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 1 w/ Spanning on both volumes was successful")

    def configure_raid5spanning(self):
        """
        Sets the RAID to RAID 1 with spanning
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_button('storage_raid_link')
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        self.click_button(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r5 > div.tlt')
        self.click_element('css = #raid_main_menu_r5 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume1\']/a')
        self.drag_and_drop_by_offset('//div[@id=\'slider_volume1\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        self.click_button('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_button('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=7200)
        self.element_should_contain(locator='id=0', expected='RAID 5')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        print 'Waiting for RAID parity check'
        time.sleep(43200)
        self.get_to_page('Storage')
        visible = self.is_element_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        while visible == False:
            time.sleep(3600)
            visible = self.is_element_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        self.log.info("Configure to RAID mode RAID 5 w/ Spanning  was successful")


    def configure_raid10spanning(self):
        """
        Sets the RAID to RAID 10 w/ Spanning
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_element('storage_raid_link')
        time.sleep(5)
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        time.sleep(5)
        self.click_element(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        time.sleep(5)
        self.click_element('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r10 > div.tlt')
        time.sleep(5)
        self.click_element('css = #raid_main_menu_r10 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(180)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        self.wait_until_element_is_clickable('//div[@id=\'slider_volume1\']/a')
        self.drag_and_drop_by_offset('//div[@id=\'slider_volume1\']/a', -250, 0)
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        self.click_button('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_element('storage_raidFormatNext4_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        self.click_element('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_element('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_element('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_element('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=5400)
        self.element_should_contain(locator='id=0', expected='RAID 10')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        self.log.info("Configure to RAID mode RAID 10 w/ Spanning was successful")

    def configure_raid5hotspare(self):
        """
        Sets the RAID to RAID 5 with Spare
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_button('storage_raid_link')
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        self.click_button(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r5 > div.tlt')
        self.click_element('css = #raid_main_menu_r5 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(210)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatSpareDsk4_chkbox+span')
        time.sleep(3)
        self.focus('css = .LightningCheckbox #storage_raidFormatSpareDsk4_chkbox+span')
        self.click_element('css = .LightningCheckbox #storage_raidFormatSpareDsk4_chkbox+span')
        time.sleep(10)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        #self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        #self.click_button('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=7200)
        self.element_should_contain(locator='id=0', expected='RAID 5')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        print 'Waiting for RAID parity check'
        time.sleep(43200)
        self.get_to_page('Storage')
        visible = self.is_element_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        while visible == False:
            time.sleep(3600)
            visible = self.is_element_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        self.log.info("Configure to RAID mode RAID 5 w/ Hot Spare  was successful")

    def configure_raid5hotsparespanning(self):
        """
        Sets the RAID to RAID 5 with Spare
        """
        self.get_to_page('Storage')
        time.sleep(3)
        self.click_button('storage_raid_link')
        self.wait_until_element_is_clickable(Elements.STORAGE_CHANGE_RAID_MODE)
        self.click_button(Elements.STORAGE_CHANGE_RAID_MODE)
        self.wait_until_element_is_clickable('popup_apply_button')
        self.click_button('popup_apply_button')
        self.wait_until_element_is_clickable('css = #raid_main_menu_r5 > div.tlt')
        self.click_element('css = #raid_main_menu_r5 > div.tlt')
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatType1_chkbox+span')
        time.sleep(5)
        self.select_checkbox(Elements.RAID_SWITCH_MODE)
        self.click_button('storage_raidFormatNext1_button')
        time.sleep(210)
        self.wait_until_element_is_clickable('storage_raidFormatNext2_button')
        self.click_element('storage_raidFormatNext2_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('css = .LightningCheckbox #storage_raidFormatSpareDsk4_chkbox+span')
        time.sleep(3)
        self.focus('css = .LightningCheckbox #storage_raidFormatSpareDsk4_chkbox+span')
        self.click_element('css = .LightningCheckbox #storage_raidFormatSpareDsk4_chkbox+span')
        time.sleep(10)
        self.driver.drag_and_drop_by_offset('//div[@id=\'slider_volume1\']/a', -250, 0)
        time.sleep(5)
        self.click_button('css = .LightningCheckbox #storage_raidFormat1stSpanning4_chkbox+span')
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        time.sleep(3)
        self.wait_until_element_is_clickable('storage_raidFormatNext4_button')
        self.click_button('storage_raidFormatNext4_button')
        #self.wait_until_element_is_clickable('storage_raidFormatNext3_button')
        #self.click_button('storage_raidFormatNext3_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext11_button')
        self.click_button('storage_raidFormatNext11_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext6_button')
        self.click_button('storage_raidFormatNext6_button')
        self.wait_until_element_is_clickable('storage_raidFormatNext16_button')
        self.click_button('storage_raidFormatNext16_button')
        self.wait_until_element_is_visible('storage_raidFormatFinish9_button', timeout=7200)
        self.element_should_contain(locator='id=0', expected='RAID 5')
        time.sleep(5)
        visible = self.is_element_visible('storage_raidFormatFinish9_button')
        while visible==False:
            time.sleep(5)
            visible = self.is_element_visible('storage_raidFormatFinish9_button')
        time.sleep(30)
        try:
            self.click_element('storage_raidFormatFinish9_button')
        except:
            time.sleep(3)
        print 'Waiting for RAID parity check'
        time.sleep(43200)
        self.get_to_page('Storage')
        visible = self.is_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        while visible == False:
            time.sleep(3600)
            visible = self.is_visible(Elements.STORAGE_CHANGE_RAID_MODE)
        self.log.info("Configure to RAID mode RAID 5 w/ Hot Spare  was successful")

    def delete_shares(self):
        """
        Deletes all shares sequentially, excluding the Public share
        starting with share1. This function assumes that all shares
        are created using the create_shares function
        """
        self.get_to_page('Shares')
        time.sleep(2)
        list_empty = False
        i = 1

        while not list_empty:
            try:
                share_name = "shares_share_{0}{1}".format('share', i)
                self.driver.element_should_be_visible(share_name)
                i += 1
            except Exception:
                list_empty = True

            if not list_empty:
                self.driver.click_element(share_name)
                self.driver.click_element(share_name)
                time.sleep(2)
                self.driver.click_element('shares_removeShare_button')
                self.driver.wait_until_element_is_visible('popup_apply_button', 5)
                self.driver.click_element('popup_apply_button')
                time.sleep(1)
                self.wait_until_element_is_not_visible(Elements.UPDATING_STRING, 360)

    def assign_group_to_share(self, group_name, share_number, access_type='rw'):
        """
        Creates group share access.

        The share number corresponds to the share name, e.g. share1 is share number 1,
        share34 is share number 34.

        Note: This function assumes that the share(s) was/were created using the create_shares function
              share1, share2 ... share99
              Also, share must be set to private instead of public. Toggle the Public switch off

        Acceptable access_level:
        rw - Read/Write access
        r  - Read access only
        d  - No access
        """
        self.get_to_group_tab()
        access_text = access_type.lower()

        # Add 2 to the index since there already 3 (0,1,2) defined shares
        share_link = 'users_{0}{1}_link'.format(access_text, (share_number + 2))
        group_link = 'users_group_{0}'.format(group_name)
        time.sleep(2)
        self.find_element_in_sublist(group_name, 'group')
        self.driver.click_element(group_link)
        time.sleep(2)
        self.driver.click_element(share_link)
        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING, time_out=180)
        self.get_to_page('Home')

    def assign_user_to_share(self, user_name, share_name, access_type='rw'):
        """
        Assigns the given user to the given share name with the given access level

        The user number corresponds to the user name, e.g. user1 is share number 1,
        user34 is 34.

        Note: We assume that users were created using this framework (user0...user511)
        share must be set to private instead of public. Toggle the Public switch off

        Acceptable access_level:
        rw - Read/Write access
        r  - Read access only
        d  - No access
        """
        self.get_to_page('Shares')
        access_text = access_type.lower()
        user_number = user_name.replace("user", "")

        user_link = 'shares_{0}{1}_link'.format(access_text, user_number)
        share_link = 'shares_share_{0}'.format(share_name)
        time.sleep(2)
        self.find_element_in_sublist(share_name, 'share')
        self.driver.click_element(share_link)
        time.sleep(2)
        self.driver.click_element(user_link)
        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING, time_out=180)

    def assign_user_to_group(self, user_name, group_name):
        """
        Assigns the given user to the given group
        """
        self.get_to_page('Home')  # there is a user's tab bug where the users are not displayed. So, refresh page must
        # be done.
        self.get_to_page('Users')
        user_link = self.find_element_in_sublist(user_name, 'user')

        # user_link = 'users_user_{0}'.format(user_name)

        self.driver.click_element(user_link)
        time.sleep(1)

        self.driver.click_element('users_editGroupMember_link')
        self.driver.wait_until_element_is_visible('users_modGroupSave_button')

        group_prefix = group_name.rstrip('0123456789')
        group_number = group_name[len(group_prefix):]

        group_link = self._find_group_in_user_sublist(group_number)

        # group_link = "//div[@id='m_group_div']/ul/li[{0}]/div/label/span".format(int(group_number))

        self.driver.click_element(group_link)
        self.driver.click_element('users_modGroupSave_button')
        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING, time_out=180)

    def create_group_quota(self, group_name, size_of_quota, unit_size):
        """
        Adjusts the quota for the given group to the given size

        Acceptable Unit Size:
        MB - Megabyte
        GB - Gigabyte
        TB - Terabyte
        """
        self.get_to_group_tab()
        time.sleep(1)

        group_link = 'users_group_{0}'.format(group_name)
        self.find_element_in_sublist(group_name, 'group')

        self.driver.click_element(group_link)
        time.sleep(5)

        self.driver.click_element('users_editGroupQuota_link')
        self.driver.wait_until_element_is_visible('quota_unit_1')

        self.driver.input_text('users_v1Size_text', size_of_quota)

        self.driver.click_element('quota_unit_1')
        self.driver.wait_until_element_is_visible('link=MB')

        unit_size_link = 'link={0}'.format(unit_size)
        self.driver.click_element(unit_size_link)

        self.driver.click_element('users_editQuotaSave_button')
        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)

    def create_user_quota(self, user_name, size_of_quota, unit_size):
        """
        Adjusts the quota for the given user to the given size

        Acceptable Unit Size:
        MB - Megabyte
        GB - Gigabyte
        TB - Terabyte
        """
        self.get_to_page('Users')
        time.sleep(1)

        # Finds the correct user in the sublist
        user_link = 'users_user_{0}'.format(user_name)
        self.find_element_in_sublist(user_name, 'user')

        # Click the user in the list
        self.driver.click_element(user_link)
        time.sleep(5)

        # Opens the quota configure window
        self.driver.click_element('users_editUserQuota_link')
        self.driver.wait_until_element_is_visible('quota_unit_1')

        # Inputs the text into the size text box
        self.driver.input_text('users_v1Size_text', size_of_quota)

        # Opens the drop down menu with the quota size options
        self.driver.click_element('quota_unit_1')
        self.driver.wait_until_element_is_visible('link=MB')

        # Clicks the correct quota size
        unit_size_link = 'link={0}'.format(unit_size)
        self.driver.click_element(unit_size_link)

        # The element has a typo on the webui
        # Saves the new quota
        self.driver.click_element('uesrs_modQuotaSave_button')
        self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)

    def find_element_in_sublist(self, name, group_user_or_share):
        """
        Finds an element in the group, user and share sub-lists given the name
        of that element.

        group user or share is necessary to determine which list the element is in
        """
        element_type = group_user_or_share.lower()

        if element_type == 'group':
            self.get_to_group_tab()
            element_link = 'users_group_{0}'.format(name)
            while self.is_enabled(element='users_groupUp_link', timeout=2):
                self.driver.click_element('users_groupUp_link')
            element_found = self.is_element_visible(element_link)
            while not element_found:
                self.driver.click_element('users_groupDown_link')
                element_found = self.is_element_visible(element_link)
            return element_link

        elif element_type == 'user':
            self.get_to_page('Users')
            element_link = 'users_user_{0}'.format(name)
            while self.is_enabled(element='users_userUp_link', timeout=2):
                self.driver.click_element('users_userUp_link')
            element_found = self.is_element_visible(element_link)
            while not element_found:
                self.driver.click_element('users_userDown_link')
                element_found = self.is_element_visible(element_link)
            return element_link

        elif element_type == 'share':
            self.get_to_page('Shares')
            element_link = 'shares_share_{0}'.format(name)
            while self.is_enabled(element='shares_shareUp_link', timeout=2):
                self.driver.click_element('shares_shareUp_link')
            element_found = self.is_element_visible(element_link)
            while not element_found:
                self.driver.click_element('shares_shareDown_link')
                element_found = self.is_element_visible(element_link)
            return element_link

    def _find_group_in_user_sublist(self, group_number):
        """
        Mainly used in the assign_user_to_group function

        Finds the group with the group number in the group sub-list,
        e.g. group2 is group number 2, group10 is group number 10
        """

        number = int(group_number)
        group_link = "//div[@id='m_group_div']/ul/li[{0}]/div/label/span".format(number)
        top_group = "//div[@id='m_group_div']/ul/li[1]/div/label/span"

        top_visible = self.is_element_visible(top_group)
        group_visible = self.is_element_visible(group_link)

        # Get to the top of the sub menu, looking on the way for the group
        while not group_visible and not top_visible:
            self.driver.drag_and_drop_by_offset('css=div.jspDrag', 0, -20)
            group_visible = self.is_element_visible(group_link)
            top_visible = self.is_element_visible(top_group)

            self.log.debug(top_visible)
            self.log.debug(group_visible)

        iterations = 0

        # Scroll down the sub menu until found, iterations is to prevent an infinite loop
        # if the group is not actually in the list
        while not group_visible and iterations < 100:
            self.driver.drag_and_drop_by_offset('css=div.jspDrag', 0, 20)
            group_visible = self.is_element_visible(group_link)
            iterations += 1
        return group_link

    @staticmethod
    def _get_locator(element):
        try:
            # See if it's a named tuple
            locator = element.locator
        except Exception:
            locator = element

        return locator


    def is_element_visible(self, element, wait_time=1):
        """
        Determines whether or not the given element is logically visible
        """
        element = self._build_element_info(element)

        visible = True

        try:
            self.wait_until_element_is_visible(element.locator, timeout=wait_time)
        except ElementNotReady:
            visible = False

        return visible

    def call_selenium_driver_function(self,
                                      function,
                                      *args,
                                      **kwargs):
        # Wrapper for any direct self.driver.* calls. This allows StaleElementReferenceException handling
        for attempt in xrange(5, 0, -1):
            try:
                retval = function(*args, **kwargs)
            except StaleElementReferenceException:
                if attempt > 1:
                    self.log.debug('Stale element. Trying again.')
                else:
                    exception_message = 'Could not execute {}() - element remained stale.'.format(function.__name__)
                    raise wd_exceptions.ActionTimeout(exception_message)
            else:
                # Success
                return retval

            # Wait 1 second and try again, but only if the timeout was set
            time.sleep(1)


    # Calls the specified function in a loop with a 1 second wait_time until it either
    # succeeds or the timeout period is exceeded. Note that the function being called must take a "timeout" argument.
    #
    # On timeout, if attempts_remaining > 0, it waits attempt_delay seconds and tries again. If not, it raises an
    # ActionTimeout exception

    def _call_wait_function(self, function, timeout=None, attempts=1, attempt_delay=5, *args, **kwargs):
        if timeout is None:
            timeout = self.implicit_wait_time

        # 1 second timeout
        kwargs['timeout'] = 1
        self.driver.set_browser_implicit_wait(1)

        stale_retries = attempts * 5

        for attempt in xrange(attempts, 0, -1):
            for i in xrange(timeout + 1, 0, -1):
                # Use timeout + 1 so it will enter this at least once, even if the timeout is set to 0
                try:
                    function(*args, **kwargs)
                except StaleElementReferenceException:
                    stale_retries -= 1
                    if stale_retries > 0:
                        self.log.debug('Stale element. Trying again.')
                    else:
                        exception_message = 'Could not execute {}() - element remained stale.'.format(function.__name__)
                        raise wd_exceptions.ActionTimeout(exception_message)
                except (AssertionError, InvalidSelectorException):
                    pass
                else:
                    # Success
                    self.driver.set_browser_implicit_wait(self.implicit_wait_time)
                    return

                if timeout > 1:
                    # Wait 1 second and try again, but only if the timeout was set
                    time.sleep(1)

            if attempt > 1:
                self.log.info('{}() failed. Waiting {} seconds and trying again.'.format(function.__name__,
                                                                                         attempt_delay))
                time.sleep(attempt_delay)

        self.log.debug("Set browser implicit wait: {}".format(self.implicit_wait_time))
        self.driver.set_browser_implicit_wait(self.implicit_wait_time)
        raise wd_exceptions.ActionTimeout('Could not execute function {}()'.format(function.__name__))

    def wait_until_element_is_visible(self,
                                      element,
                                      timeout=None,
                                      attempts=1,
                                      attempt_delay=5,
                                      error=None):
        """
        Waits until element specified with 'element' is visible
        Fails if 'timeout' expires before the element is visible.
        Will retry up to the number specified by attempts
        """
        element_object = self._build_element_info(element)
        self.log.debug('Waiting until element: "{}" is visible.'.format(element_object.name))

        try:
            self._call_wait_function(function=self.driver.wait_until_element_is_visible,
                                     attempts=attempts,
                                     attempt_delay=attempt_delay,
                                     locator=element_object.locator,
                                     timeout=timeout,
                                     error=error)
            self.log.debug('Element: "{}" is visible'.format(element_object.name))
        except wd_exceptions.ActionTimeout:
            raise ElementNotReady('Could not find element: ' + element_object.name)

    def wait_until_element_is_not_visible(self, locator, time_out=None, attempts=1, attempt_delay=5):
        """
        Waits until the given element is not logically visible
        """
        element_object = self._build_element_info(locator)
        self.log.debug('Waiting until element: "{}" is not visible.'.format(element_object.name))

        if time_out is None:
            time_out = self.implicit_wait_time

        timer = Stopwatch(timer=time_out)
        timer.start()

        attempts_remaining = attempts
        while True:
            if self.is_element_visible(element_object, wait_time=1):
                if timer.is_timer_reached():
                    attempts_remaining -= 1
                    if attempts_remaining > 0:
                        time.sleep(attempt_delay)
                        timer.reset(keep_running=True)
                    else:
                        raise wd_exceptions.ActionTimeout('Element {} remained visible.'.format(element_object.name))

                # Wait 1/2 second and try again
                time.sleep(0.5)
            else:
                self.log.debug('Element: "{}" is not visible.'.format(element_object.name))
                return


    def wait_until_element_is_clickable(self, locator, timeout=None, attempts=1, attempt_delay=5):
        """
        Waits until the given locator is clickable
        """
        element_object = self._build_element_info(locator)
        self.log.debug('Waiting until element "{}" is clickable.'.format(element_object.name))
        
        if timeout is None:
            timeout = self.implicit_wait_time        

        attempts_remaining = attempts
        timer = Stopwatch(timer=timeout)
        timer.start()

        # First, wait until the element is visible
        try:
            self.wait_until_element_is_visible(element=locator, timeout=timeout)
        except ElementNotReady:
            attempts_remaining -= 1
            if attempts_remaining > 0:
                self.log.info('Element is not visible. Waiting {} seconds and trying again.'.format(attempt_delay))
                time.sleep(attempt_delay)
                timer.reset(keep_running=True)
            else:
                raise

        attribute_locator = element_object.locator + '@class'

        # Element is visible, but make sure it's not grayed out
        while True:
            attribute = self.driver.get_element_attribute(attribute_locator)
            if ('gray_out' and 'grayout') not in attribute:
                # Not grayed out
                return

            if timer.is_timer_reached():
                attempts_remaining -= 1
                if attempts_remaining > 0:
                    self.log.info('Element is grayed out. Waiting {} seconds and trying again.'.format(attempt_delay))
                    time.sleep(attempt_delay)
                    timer.reset(keep_running=True)
                else:
                    self.log.info('Element remained grayed out. Attributes are: {}'.format(attribute))
                    raise wd_exceptions.ActionTimeout('Timed out waiting for {}'.format(element_object.locator))

    def is_element_clickable(self, locator, timeout=None):
        """
        Waits until the given locator is clickable, return True
        """
        if timeout is None:
            timeout = self.implicit_wait_time

        try:
            name = locator.name
        except Exception:
            name = str(locator)

        selenium_locator = self._get_locator(locator)

        self.wait_until_element_is_visible(element=locator, timeout=timeout)

        attribute = self.driver.get_element_attribute(str(selenium_locator) + '@class')

        start_time = time.time()
        while 'gray_out' in attribute:
            attribute = self.driver.get_element_attribute(str(selenium_locator) + '@class')
            time.sleep(0.5)
            if time.time() - start_time >= timeout:
                raise Exception('Timed out waiting for + ' + name + ' to not be greyed out.')

        return True

    def view_critical_info(self):
        """
        Navigates through the UI and views critical information
        """
        self.get_to_page('Home')
        self.driver.wait_until_element_is_visible('id=smart_info', 60)
        self.driver.click_element('id=smart_info')
        self.driver.wait_until_element_is_visible('css=div.WDLabelHeaderDialogue.WDLabelHeaderDialogueDiagnosticsIcon')
        self.driver.wait_until_element_is_visible('home_diagnosticsSysTemper_value')
        temperature = self.driver.get_text('home_diagnosticsSysTemper_value')
        time.sleep(2)
        if temperature == 'Normal':
            self.log.info('Temperature is normal')
        self.driver.click_element('css=div.jspTrack')
        fan = self.driver.get_text('home_diagnosticsFanTemper_value')
        if len(fan) > 0:
            self.log.info('Fan speed is greater than zero')
        time.sleep(2)
        self.driver.click_element('home_diagnosticsClose1_button')
        time.sleep(3)
        self.get_to_page('Settings')
        time.sleep(5)
        self.driver.wait_until_element_is_visible('settings_utilities_link', 60)
        self.driver.click_element('settings_utilities_link')
        self.driver.wait_until_element_is_visible('settings_utilitiesUptime_value', 60)
        uptime = self.driver.get_text('settings_utilitiesUptime_value')
        if len(uptime) > 0:
            self.log.info('Uptime of device is greater than zero')
        time.sleep(5)
        self.get_to_page('Storage')
        self.driver.wait_until_element_is_visible('storage_diskstatus_link', 60)
        self.driver.click_element('storage_diskstatus_link')
        time.sleep(5)
        self.driver.element_should_be_visible('id=row1')
        drive1 = self.driver.get_text('id=row1')
        if len(drive1) > 0:
            self.driver.element_should_contain('id=row1', 'Good')
            self.log.info('Drive 1 is good')

        drive2 = self.driver.get_text('id=row2')
        if len(drive2) > 0:
            self.driver.element_should_contain('id=row2', 'Good')
            self.log.info('Drive 2 is good')
        drive3 = self.driver.get_text('id=row3')
        if len(drive3) > 0:
            self.driver.element_should_contain('id=row3', 'Good')
            self.log.info('Drive 3 is good')
        drive4 = self.driver.get_text('id=row4')
        if len(drive4) > 0:
            self.driver.element_should_contain('id=row4', 'Good')
            self.log.info('Drive 4 is good')

    def invalid_vlan_id(self):
        """
        Inputs invalid vlan IDs and verifies they are actually invalid
        """
        self.get_to_page('Settings')
        self.driver.wait_until_element_is_visible('settings_network_link', 10)
        self.driver.click_element('settings_network_link')
        time.sleep(5)
        self.driver.wait_until_element_is_visible('settings_networkLAN0IPv4Static_button', 10)
        self.driver.click_element('settings_networkLAN0IPv4Static_button')
        self.driver.wait_until_element_is_visible('settings_networkIPv4Next1_button', 10)
        self.driver.click_element('settings_networkIPv4Next1_button')
        # TODO: Confirm this typo is expected. If so, document it here.
        self.driver.wait_until_element_is_visible('settings_networkIPv4Next2_butotn', 10)
        # TODO: Confirm this typo is expected. If so, document it here.
        self.driver.click_element('settings_networkIPv4Next2_butotn')
        self.driver.wait_until_element_is_visible('css = #settings_networkVLAN_switch+span .checkbox_container', 10)
        self.driver.click_element('css = #settings_networkVLAN_switch+span .checkbox_container')
        time.sleep(3)
        self.driver.input_text('settings_networkVLANID_text', '5000')
        self.driver.click_element('settings_networkIPv4Next3_button')
        self.driver.wait_until_element_is_visible('id=popup_ok_button', 15)
        self.driver.click_element('id=popup_ok_button')
        time.sleep(3)
        self.driver.input_text('settings_networkVLANID_text', 'abc')
        self.driver.click_element('settings_networkIPv4Next3_button')
        self.driver.wait_until_element_is_visible('id=popup_ok_button', 15)
        self.driver.click_element('id=popup_ok_button')
        self.driver.click_element('settings_networkIPv4Cancel3_button')
        self.log.info('PASS')

    def select_frame(self, locator):
        """
        Select a frame on the page identify by 'locator'
        """
        locator = self._get_locator(locator)
        self.log.debug('Selecting frame: %s' % locator)
        self.driver.select_frame(locator)

    def unselect_frame(self):
        """
        Unselect a frame on the page
        """
        self.log.debug('Unselecting all frame')
        self.driver.unselect_frame()

    # Get the type of the element based off the string
    @staticmethod
    def _get_element_type(element):
        # Not specified, so find it based off the locator string
        for element_type, identifiers in Elements.id_strings.iteritems():
            if identifiers:
                for identifier in identifiers:
                    if element.locator.endswith(identifier) and element.locator != identifier:
                        return element_type

        # Unknown
        return Elements.TYPE_OTHER

    def is_ready(self,
                 element,
                 timeout=None,
                 check_page=True,
                 check_is_visible=True,
                 check_is_enabled=True,
                 do_login=True,
                 check_browser=True):
        """ Checks to see if the element is visible and enabled, switching
            to the appropriate page if necessary. Returns True if it is,
            False if it is not. If raise_exception is true (default),
            it raises an exception instead of returning false.
        """
        element = self._build_element_info(element)
        message = 'Element ' + element.name
        if element.page is not None:
            message += ' on ' + str(Page_Info.info[element.page].name) + ' page '


        self.log.debug('is_ready: ')
        self.log.debug('element = ' + str(element))
        self.log.debug('page, name, type = ' +
                       str(element.page) +
                       ', ' +
                       str(element.name) +
                       ', ' +
                       str(element.control_type))

        if check_browser:
            if element.page:
                self.check_browser_state(do_login=do_login)
            else:
                self.check_browser_state(do_login=False)

        if element.page and check_page:
            # Switch to the page (if necessary)
            self.get_to_page(element.page, check_browser=False)

        retval = True

        # Confirm the element is on the page
        if element.wait_before_check:
            # Wait for the element to get ready
            time.sleep(element.wait_before_check)

        if not element.is_hidden:
            if not self.is_present(element, timeout):
                retval = False
                message += 'is not present'
            # Confirm it's visible
            elif check_is_visible:
                if not self.is_element_visible(element, wait_time=timeout):
                    retval = False
                    message += ' is not visible'
                elif check_is_enabled:
                        # Next make sure it's enabled
                        # Special handler for this since not every control supports is_enabled
                        if not self.is_enabled(element, timeout):
                            retval = False
                            message += ' is not enabled'

        if retval is False:
            self.log.debug(message)

        return retval

    # Public, form

    def submit_form(self, locator=None):
        """
        Submits a form identified by `locator`.
        """
        element = self._build_element_info(locator)
        if self.is_ready(element.locator):
            self.driver.submit_form(element.locator)
        else:
            raise ElementNotReady(str(element))

    # Public, checkboxes

    def checkbox_should_be_selected(self, locator):
        """
        Verifies checkbox identified by `locator` is selected/checked.
        """
        # Split the locator into a parent
        element = self._build_element_info(locator)
        parent, chkbox = self._split_element_path(element.locator)
        self.log.debug('Verifying checkbox: "%s" should be selected.' % chkbox)
        if self.is_ready(element, check_is_visible=False):
            self.driver.checkbox_should_be_selected(chkbox)
        else:
            raise ElementNotReady()

    def checkbox_should_not_be_selected(self, locator):
        """
        Verifies checkbox identified by `locator` is not selected/checked.
        """
        # Split the locator into a parent
        element = self._build_element_info(locator)
        parent, chkbox = self._split_element_path(locator)
        self.log.debug('Verifying checkbox: "%s" should not be selected.' % chkbox)
        if self.is_ready(element, check_is_visible=False):
            self.driver.checkbox_should_not_be_selected(chkbox)
        else:
            raise ElementNotReady

    def checkbox_should_be_disabled(self, locator):
        """
        Verifies checkbox identified with `locator` is disabled.
        """
        parent, chkbox = self._split_element_path(locator)
        self.driver.element_should_be_disabled(chkbox)
    def page_should_contain_checkbox(self, locator, message='', loglevel='INFO'):
        """
        Verifies checkbox identified by `locator` is found from current page.
        """
        # Split the locator into a parent
        parent, chkbox = self._split_element_path(locator)
        self.log.debug('Verifying page should contain checkbox: "%s".' % chkbox)
        self.driver.page_should_contain_checkbox(chkbox, message, loglevel)

    def page_should_not_contain_checkbox(self, locator, message='', loglevel='INFO'):
        """
        Verifies checkbox identified by `locator` is not found from current page.
        """
        # Split the locator into a parent
        parent, chkbox = self._split_element_path(locator)
        self.log.debug('Verifying page should not contain checkbox: %s' % chkbox)
        self.driver.page_should_not_contain_checkbox(chkbox, message, loglevel)

    def select_checkbox(self, locator, timeout=None):
        """
        Selects checkbox identified by `locator`.
        """
        locator = self._get_locator(locator)
        # Split the locator into a parent
        if ',' not in locator:
            locator = 'css=.LightningCheckbox,' + locator
        parent, chkbox = self._split_element_path(locator)
        self.log.debug('Selecting checkbox: "%s"' % chkbox)
        if self.is_ready(chkbox, check_is_visible=False, check_is_enabled=False, timeout=timeout):
            if not self.driver.get_element_attribute(chkbox + '@checked'):
                parent.click()
                if not self.driver.get_element_attribute(chkbox + '@checked'):
                    self.log.info('Unable to verify if checkbox is checked. Retrying.')
                    time.sleep(1)
                    if not self.driver.get_element_attribute(chkbox + '@checked'):
                        self.log.info('Trying to click a second time')
                        parent.click()
                        time.sleep(0.5)
                        if not self.driver.get_element_attribute(chkbox + '@checked'):
                            raise Exception('Failed to select checkbox: ' + str(locator))
            else:
                self.log.debug('Element is already selected: ' + str(locator))
        else:
            raise Exception('Checkbox ' + str(locator) + ' is not ready.')

    def unselect_checkbox(self, locator, timeout=None):
        """
        Removes selection of checkbox identified by `locator`.
        """
        # Split the locator into a parent
        parent, chkbox = self._split_element_path(locator)

        self.log.debug('Unselecting checkbox: "%s"' % chkbox)
        if self.is_ready(chkbox, check_is_visible=False, check_is_enabled=False, timeout=timeout):
            if self.driver.get_element_attribute(chkbox + '@checked'):
                parent.click()
                if self.driver.get_element_attribute(chkbox + '@checked'):
                    time.sleep(1)
                    if self.driver.get_element_attribute(chkbox + '@checked'):
                        raise Exception('Failed to unselect checkbox: ' + str(locator))
            else:
                self.log.debug('Element is already unselected: ' + str(locator))
        else:
            raise Exception('Checkbox ' + str(locator) + ' is not ready.')

    def _split_element_path(self, locator):
        locator_text = self._get_locator(locator)
        parts = string.rsplit(locator_text, ',', 1)
        if len(parts) > 1:
            # Need to split it
            # TODO: Support multiple levels of the path instead of just a parent/child
            parent_locator = parts[0]
            child_locator = parts[1]

            if self.is_ready(child_locator, check_is_visible=False, check_is_enabled=False):
                time.sleep(0.5)
                # noinspection PyProtectedMember
                element_list = self.driver._element_find(parent_locator, False, True)
                for element in element_list:
                    children = element.find_elements(by="xpath", value=".//*")
                    if children:
                        for child_element in children:
                            if child_element.get_attribute('id') == child_locator:
                                parent_element = element
                                return parent_element, child_locator

                # Did not find it
                raise Exception('Unable to find element identified with: ' + str(locator))
            else:
                raise Exception('Unable to split element because child is not ready: ' + locator)
        else:
            # No need to split, so return locator_text as child and the element as parent
            child_locator = locator_text
            # noinspection PyProtectedMember
            parent_element = self.driver._element_find(locator_text, True, True)

        return parent_element, child_locator


    # Public, text fields

    def choose_file(self, locator, file_path):
        """
        Inputs the `file_path` into file input field found by `identifier`.

        This keyword is most often used to input files into upload forms.
        The file specified with `file_path` must be available on the same host
        where the Selenium Server is running.

        """
        locator_text = self._get_locator(locator)
        if self.is_ready(locator):
            self.driver.choose_file(locator_text, file_path)

    def input_password(self, locator, text, do_login=True, check_browser=True, check_page=True):
        """Types the given password into text field identified by `locator`.

        Difference between this keyword and `Input Text` is that this keyword
        does not log the given password. See `introduction` for details about
        locating elements.
        """
        element = self._build_element_info(locator)
        if self.is_ready(element=element, do_login=do_login, check_page=check_page, check_browser=check_browser):
            self.driver.input_password(element.locator, text)
        else:
            message = 'Element ' + locator.name + ' is not ready.'
            raise ElementNotReady(message)

    def input_text(self, locator, text, do_login=True, check_page=True, check_browser=True, validate_entry=True):
        """ Types the given `text` into text field identified by `locator`.

        :param locator: The element object or locator string of the text box
        :param text: The text to send to the text box
        :param do_login: If True, login to the web UI if necessary
        :param check_page: If True, switch to the element's page if necessary
        :param check_browser: If True, open the web browser if necessary
        :param validate_entry: If True, verify the contents of the text field after changing
        """

        element = self._build_element_info(locator)
        if self.is_ready(element=element, do_login=do_login, check_page=check_page, check_browser=check_browser):
            self.driver.input_text(element.locator, text)
            time.sleep(0.5)  # Wait 1/2 a second to allow the field to update
            actual_text = self.get_value(locator)
            if validate_entry:
                if actual_text != str(text):
                    raise wd_exceptions.TextMismatch('Field {} has a value of {}. Expected {}'.format(locator.name,
                                                                                                      actual_text,
                                                                                                      text))
        else:
            message = 'Element ' + element.name + ' is not ready.'
            raise ElementNotReady(message)

    def page_should_contain_textfield(self, locator, message='', loglevel='INFO'):
        """
        Verifies text field identified by `locator` is found from current page.
        """
        locator_text = self._get_locator(locator)
        self.driver.page_should_contain_textfield(locator_text, message, loglevel)

    def page_should_not_contain_textfield(self, locator, message='', loglevel='INFO'):
        """
        Verifies text field identified by `locator` is not found from current page.
        """
        locator_text = self._get_locator(locator)
        self.driver.page_should_not_contain_textfield(locator_text, message, loglevel)

    def textfield_should_contain(self, locator, expected, message=''):
        """
        Verifies text field identified by `locator` contains text `expected`.
        `message` can be used to override default error message.
        """
        locator_text = self._get_locator(locator)
        self.driver.textfield_should_contain(locator_text, expected, message)

    def textfield_value_should_be(self, locator, expected, message=''):
        """
        Verifies the value in text field identified by `locator` is exactly `expected`.
        `message` can be used to override default error message.
        """
        element = self._build_element_info(locator)
        if self.is_ready(element, check_is_enabled=(not element.is_disabled)):
            self.driver.textfield_value_should_be(element.locator, expected, message)
        else:
            raise ElementNotReady(element.name)

    def textarea_should_contain(self, locator, expected, message=''):
        """
        Verifies text area identified by `locator` contains text `expected`.
        `message` can be used to override default error message.
        """
        locator_text = self._get_locator(locator)
        self.driver.textarea_should_contain(locator_text, expected, message)

    def textarea_value_should_be(self, locator, expected, message=''):
        """
        Verifies the value in text area identified by `locator` is exactly `expected`.
        `message` can be used to override default error message.
        """
        locator_text = self._get_locator(locator)
        self.driver.textarea_value_should_be(locator_text, expected, message)

    # Public, buttons and elements

    def click_button(self,
                     locator,
                     timeout=None,
                     check_is_ready=True,
                     check_page=True,
                     do_login=True,
                     check_browser=True,
                     skip_click=False):
        """ Clicks the specified button

        :param locator: The element or locator ID to click
        :param timeout: The amount of time to wait
        :param check_is_ready: If True, confirms the element is ready before trying to click
        :param check_page: If True, confirms the UI is on the correct page before clicking
        :param do_login: If True, logs in to the web UI if necessary
        :param check_browser: If True, confirms the browser is running before clicking
        :param skip_click: If True, gets to the element and makes sure it's ready, but doesn't actually click
        """
        if timeout is None:
            timeout = self.implicit_wait_time

        element = self._build_element_info(locator, force_type=Elements.TYPE_BUTTON)

        self.click_element(locator=element,
                           timeout=timeout,
                           check_is_ready=check_is_ready,
                           check_page=check_page,
                           do_login=do_login,
                           check_browser=check_browser,
                           skip_click=skip_click)

    def _build_element_info(self, element, force_type=None):
        try:
            # If it's an ElementInfo tuple, get the info out of it
            name = element.name
        except AttributeError:
            # Just a string
            name = str(element)

        try:
            locator = element.locator
        except AttributeError:
            # Just a string
            locator = str(element)

        try:
            page = element.page
        except AttributeError:
            # Just a string
            page = None

        try:
            verification_element = element.verification_element
        except AttributeError:
            # Just a string
            verification_element = None

        try:
            control_type = element.control_type
        except AttributeError:
            # Just a string
            control_type = None

        try:
            is_hidden = element.is_hidden
        except AttributeError:
            # Just a string
            is_hidden=False

        try:
            is_disabled = element.is_disabled
        except AttributeError:
            # Just a string
            is_disabled=False

        try:
            wait_before_check = element.wait_before_check
        except AttributeError:
            # Just a string
            wait_before_check = None

        if type is None:
            self._get_element_type(locator)

        if force_type:
            control_type = force_type

        element_info = ElementInfo(name=name,
                                   locator=locator,
                                   page=page,
                                   verification_element=verification_element,
                                   control_type=control_type,
                                   is_hidden=is_hidden,
                                   is_disabled=is_disabled,
                                   wait_before_check=wait_before_check)
        return element_info

    def _click_element(self,
                       element,
                       check_is_ready,
                       check_page,
                       timeout,
                       verify_timeout,
                       do_login=True,
                       check_browser=True,
                       click_delay=0.25,
                       skip_click=False):
        # Does all the work for click_element. First, see convert to a named tuple, if necessary
        element = self._build_element_info(element)
        self.log.debug('_click: element: {0}'.format(element))

        if check_is_ready:
            is_element_ready = self.is_ready(element,
                                             timeout=timeout,
                                             check_page=check_page,
                                             do_login=do_login,
                                             check_browser=check_browser)
        else:
            is_element_ready = True

        if is_element_ready:
            if skip_click is False:
                self.log.debug('skip_click element :element.locator :{0}'.format(element.locator))
                self.driver.mouse_down(element.locator)
                time.sleep(click_delay)
                #self.driver.mouse_down(element.locator)
                self.driver.mouse_up(element.locator)
                #self.driver.click_element(element.locator)

                # If the element has a verification_element, check to see if that element has appeared
                self.log.debug('verify element: {0}'.format(element.verification_element))
                if element.verification_element:
                    self.wait_until_element_is_visible(element.verification_element, verify_timeout)
        else:
            raise ElementNotReady('Element ' + str(element.name) + ' is not ready.')

    def click_download_button(self,
                              element,
                              dest_file='downloaded_file',
                              timeout=None,
                              check_is_ready=True,
                              check_page=True,
                              do_login=True,
                              check_browser=True,
                              proxies=None):
        """ Clicks the specified button and downloads the file it gets from the UUT. Used for buttons that present
            a system Save As dialog when clicked

        :param element: The element to click
        :param dest_file: The path and filename to save the data as
        :param timeout: How long to wait before giving up
        :param check_is_ready: Confirm that the element is ready before clicking
        :param check_page: Confirm that the UI is on the correct page
        :param do_login: Login if necessary
        :param check_browser: Checks the browser state
        """

        self.click_button(locator=element,
                          check_is_ready=check_is_ready,
                          check_page=check_page,
                          do_login=do_login,
                          check_browser=check_browser,
                          skip_click=True)

        button_locator = element.locator
        page_contents = BeautifulSoup(self.driver.get_source())
        button = page_contents.find(id=button_locator)
        parent = button.parent
        action = parent.get('action')
        method = parent.get('method')

        data = {}

        for field in parent.findAll('input'):
            value = field.get('value')
            cmd = field.get('name')
            data[cmd] = value

        device_ip = self.uut[Fields.internal_ip_address]
        url = 'http://{}{}'.format(device_ip, action)
        self.log.debug('Clicking download button. url = {}, action = {}, method = {}'.format(url, action, method))

        self._requests_login(proxies)


        r = self._submit_request(url, method=method, data=data, proxies=proxies)

        with open(dest_file, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
                    f.flush()

    def click_upload_button(self,
                            element,
                            source_file,
                            timeout=None,
                            check_is_ready=True,
                            check_page=True,
                            do_login=True,
                            check_browser=True,
                            proxies=None):
        """ Clicks the specified button and uploads the file it gets to the UUT. Used for buttons that present
            a system Open dialog when clicked

        :param element: The element to click
        :param source_file: The path and filename to upload
        :param timeout: How long to wait before giving up
        :param check_is_ready: Confirm that the element is ready before clicking
        :param check_page: Confirm that the UI is on the correct page
        :param do_login: Login if necessary
        :param check_browser: Checks the browser state
        """

        self.click_button(locator=element,
                          check_is_ready=check_is_ready,
                          check_page=check_page,
                          do_login=do_login,
                          check_browser=check_browser,
                          skip_click=True)

        button_locator = element.locator
        page_contents = BeautifulSoup(self.driver.get_source())
        button = page_contents.find(id=button_locator)
        grandparent = button.parent.parent
        action = grandparent.get('action')
        method = grandparent.get('method')

        data = self._get_form_fields(grandparent)

        input_id = button.parent.find('input').get('id')
        files = {input_id: (source_file, open(source_file, 'rb'), 'application/octet-stream')}

        url = 'http://{}{}'.format(self.uut[Fields.internal_ip_address], action)
        self.log.debug('Clicking upload button. url = {}, action = {}, method = {}'.format(url, action, method))
        self._submit_request(url, method=method, data=data, files=files, proxies=proxies)

    def _submit_request(self, url, method, data, files=None, proxies=None):
        self._requests_login(proxies)

        self.log.debug('Submitting request. url={}, method={}, data={}'.format(url,
                                                                               method,
                                                                               data))


        if method == 'post':
            r = requests.post(url,
                              data=data,
                              files=files,
                              cookies=self.requests_cookies,
                              proxies=proxies)
        elif method == 'put':
            r = requests.put(url,
                             data=data,
                             files=files,
                             cookies=self.requests_cookies,
                             proxies=proxies)
        elif method == 'get':
            r = requests.get(url,
                             data=data,
                             files=files,
                             cookies=self.requests_cookies,
                             proxies=proxies)
        else:
            raise wd_exceptions.UnsupportedProtocol('Request method of {} not supported'.format(method))

        return r

    def _get_form_fields(self, element):
        data = {}

        for field in element.findAll('input'):
            cmd = field.get('name')
            if cmd == 'f_ip':
                data[cmd] = self.uut[Fields.internal_ip_address]
            else:
                value = field.get('value')
                data[cmd] = value

        return data

    def _requests_login(self, proxies=None):
        if self.requests_cookies is None:
            self.log.debug('Logging in for requests')
            login = requests.post('http://{}/cgi-bin/login_mgr.cgi'.format(self.uut[Fields.internal_ip_address]),
                                  data={'cmd': 'wd_login',
                                        'username': self.uut[Fields.web_username],
                                        'pwd': self.uut[Fields.web_password],
                                        'port': None},
                                  proxies=proxies)

            self.requests_cookies = login.cookies

    def download_config_file(self, config_file, proxies=None):
        """ Get the config file from the unit

        :param config_file: The destination file
        :param proxies: If present, which proxies to use for Requests. Used to debug using something like Charles.
        """
        self.click_download_button(Elements.SETTINGS_UTILITY_SAVE_CONFIG_FILE,
                                   dest_file=config_file,
                                   proxies=proxies)

    # # TODO: Get to the page, confirm the form is ready
    # def submit_upload_form(self, form_id, source_file, proxies=None, page_contents=None):
    #     if page_contents is None:
    #         page_contents = BeautifulSoup(self.driver.get_source())
    #     form = page_contents.find(id=form_id)
    #     action = form.get('action')
    #     method = form.get('method')
    #
    #     data = self._get_form_fields(form)
    #
    #     input_id = form.find('input').get('id')
    #     files = {input_id: (source_file, open(source_file, 'rb'), 'application/octet-stream')}
    #
    #     url = 'http://{}{}'.format(self.uut[Fields.internal_ip_address], action)
    #     self._submit_request(url, method=method, data=data, files=files, proxies=proxies)

    def upload_config_file(self, config_file, proxies=None):
        """ Uploads the specified config file and reboots the unit

        :param config_file: The config file to upload
        :param proxies: If present, which proxies to use for Requests. Used to debug using something like Charles.
        """
        # self.submit_upload_form('form_restore',
        #                         source_file=config_file,
        #                         proxies=proxies)

        self.click_upload_button(Elements.SETTINGS_UTILITY_IMPORT_CONFIG_FILE,
                                 source_file=config_file,
                                 proxies=proxies)

        # Restart the unit
        url = 'http://{}/cgi-bin/system_mgr.cgi'.format(self.uut[Fields.internal_ip_address])
        try:
            requests.post(url, data={'cmd': 'cgi_restart'}, cookies=self.requests_cookies, proxies=proxies)
        except requests.ConnectionError:
            pass
        else:
            raise wd_exceptions.InvalidDeviceState('Device failed to reboot')


    def upload_firmware_file(self, firmware_file, proxies=None):
        self.click_upload_button(Elements.SETTINGS_FIRMWARE_UPDATE_FILE,
                                 source_file=firmware_file,
                                 proxies=proxies)

        # Restart the unit
        url = 'http://{}/cgi-bin/system_mgr.cgi'.format(self.uut[Fields.internal_ip_address])
        try:
            requests.post(url, data={'cmd': 'cgi_restart'}, cookies=self.requests_cookies, proxies=proxies)
        except requests.ConnectionError:
            pass

    def click_element(self,
                      locator,
                      timeout=None,
                      check_is_ready=True,
                      check_page=True,
                      attempts=3,
                      do_login=True,
                      check_browser=True,
                      skip_click=False):
        """ Clicks the specified element
        :param locator: The element or locator ID to click
        :param timeout: The amount of time to wait
        :param check_is_ready: If True, confirms the element is ready before trying to click
        :param check_page: If True, confirms the UI is on the correct page before clicking
        :param do_login: If True, logs in to the web UI if necessary
        :param check_browser: If True, confirms the browser is running before clicking
        :param skip_click: If True, gets to the element and makes sure it's ready, but doesn't actually click
        """

        self.set_timeout(timeout)
        click_delay = 0.25

        if attempts <= 0:
            raise Exception('click_element sent an attempt value of ' + str(attempts) + ', which is invalid.')
        else:
            if attempts > 5:
                self.log.warning(ctl.exception('click_elements sent an attempt value of ' +
                                               str(attempts) +
                                               ', which may be too high.'))
        if timeout is None:
            timeout = self.implicit_wait_time

        for i in range(0, attempts):
            if i < attempts - 1:
                # Use a short timeout for the first
                if timeout > 5:
                    verify_timeout = 5
                else:
                    verify_timeout = timeout
            else:
                verify_timeout = timeout

            # Send the click
            element = self._build_element_info(locator)
            self.log.debug('Clicking element: "{}"'.format(element.name))
            try:
                self._click_element(element=element,
                                    check_is_ready=check_is_ready,
                                    check_page=check_page,
                                    timeout=timeout,
                                    verify_timeout=verify_timeout,
                                    do_login=do_login,
                                    check_browser=check_browser,
                                    click_delay=click_delay,
                                    skip_click=skip_click)
                break
            except ElementNotReady:
                self.log.debug(ctl.exception('Exception:'))
                if i >= attempts - 1:
                    # Too many retries, raise the exception
                    raise

                self.log.info('Element ' +
                              element.name +
                              ' did not click. Waiting 2 seconds and retrying. ' +
                              str(attempts - i - 1) +
                              ' attempts remaining.')
                time.sleep(2)
                # Increase the delay between mouse down and mouse up
                click_delay *= 2



    def _click_element_blind(self, locator, timeout=None):
        if timeout is None:
            timeout = self.implicit_wait_time
        self.click_element(locator, timeout, check_is_ready=False, check_page=False)

    def double_click_element(self, locator):
        """
        Double clicks a button identified by `locator`.
        Key attributes for buttons are `id`, `name` and `value`.
        """
        locator_text = self._get_locator(locator)
        if self.is_ready(locator):
            self.driver.double_click_element(locator_text)

    def send_keys(self,
                  element,
                  keystrokes,
                  timeout=None,
                  check_page=True,
                  do_login=True):

        element_info = self._build_element_info(element)

        self.click_element(element, timeout=timeout, check_page=check_page, do_login=do_login)
        # noinspection PyProtectedMember
        ui_element = self.driver._element_find(element_info.locator, True, True)
        ui_element.send_keys(keystrokes)
        self.click_element(element, timeout=timeout, check_page=check_page, do_login=do_login)

    def page_should_contain_button(self, locator, message='', loglevel='INFO'):
        """
        Verifies button identified by `locator` is found from current page.
        This keyword searches for buttons created with either `input` or `button` tag.
        Key attributes for buttons are `id`, `name` and `value`.
        """
        locator_text = self._get_locator(locator)
        self.driver.page_should_contain_button(locator_text, message, loglevel)

    def page_should_not_contain_button(self, locator, message='', loglevel='INFO'):
        """
        Verifies button identified by `locator` is not found from current page.
        This keyword searches for buttons created with either `input` or `button` tag.
        """
        locator_text = self._get_locator(locator)
        self.driver.page_should_not_contain_button(locator_text, message, loglevel)

    # _runonfailure.py
    def register_keyword_to_run_on_failure(self, keyword):
        """
        Sets the keyword to execute when a Selenium2Library keyword fails.

        `keyword_name` is the name of a keyword (from any available
        libraries) that  will be executed if a Selenium2Library keyword fails.
        It is not possible to use a keyword that requires arguments.
        Using the value "Nothing" will disable this feature altogether.

        The initial keyword to use is set in `importing`, and the
        keyword that is used by default is `Capture Page Screenshot`.
        Taking a screenshot when something failed is a very useful
        feature, but notice that it can slow down the execution.

        This keyword returns the name of the previously registered
        failure keyword. It can be used to restore the original
        value later.
        """
        self.driver.register_keyword_to_run_on_failure(keyword)

        # _screenshot.py - Public

    def capture_browser_screenshot(self, filename):
        """ Takes a screenshot and saves to filename

        :param filename: File to save the screenshot
        """
        if self._is_browser():
            # noinspection PyProtectedMember
            if hasattr(self.driver._current_browser(), 'get_screenshot_as_file'):
                try:
                    # noinspection PyProtectedMember
                    self.driver._current_browser().get_screenshot_as_file(filename)
                    self.log.debug('Screenshot captured as ' + filename)
                except RuntimeError:
                    self.log.debug(ctl.exception('Could not capture screenshot.'))
            else:
                try:
                    # noinspection PyProtectedMember
                    self.driver._current_browser().save_screenshot(filename)
                    self.log.debug('Screenshot captured as ' + filename)
                except RuntimeError:
                    self.log.debug(ctl.exception('Could not capture screenshot.'))

    def capture_page_screenshot(self, filename=None):
        """
        Takes a screenshot of the current page and embeds it into the log.

        `filename` argument specifies the name of the file to write the
        screenshot into. If no `filename` is given, the screenshot is saved into file
        `selenium-screenshot-<counter>.png` under the directory where
        the Robot Framework log file is written into. The `filename` is
        also considered relative to the same directory, if it is not
        given in absolute format.
        """
        self.driver.capture_page_screenshot(filename)

    # _waiting.py
    def wait_for_condition(self, condition, timeout=None, error=None):
        """
        Waits until the given `condition` is true or `timeout` expires.

        `code` may contain multiple lines of code but must contain a
        return statement (with the value to be returned) at the end

        The `condition` can be arbitrary JavaScript expression but must contain a
        return statement (with the value to be returned) at the end.
        See `Execute JavaScript` for information about accessing the
        actual contents of the window through JavaScript.

        `error` can be used to override the default error message.
        """
        self.driver.wait_for_condition(condition, timeout, error)

    def wait_until_page_contains(self, text, timeout=None, error=None, attempts=1, attempt_delay=5):
        """
        Waits until `text` appears on current page.

        Fails if `timeout` expires before the text appears. See
        `introduction` for more information about `timeout` and its
        default value.

        `error` can be used to override the default error message.

        See also `Wait Until Page Contains Element`, `Wait For Condition` and
        BuiltIn keyword `Wait Until Keyword Succeeds`.
        """
        self.log.debug('Waiting for page to contain {}'.format(text))
        self._call_wait_function(function=self.driver.wait_until_page_contains,
                                 timeout=timeout,
                                 attempts=attempts,
                                 attempt_delay=attempt_delay,
                                 text=text,
                                 error=error)

    def wait_until_page_contains_element(self, locator, timeout=None, error=None, attempts=1, attempt_delay=5):
        """
        Waits until element specified with `locator` appears on current page.

        Fails if `timeout` expires before the element appears. See
        `introduction` for more information about `timeout` and its
        default value.

        `error` can be used to override the default error message.

        See also `Wait Until Page Contains`, `Wait For Condition` and
        BuiltIn keyword `Wait Until Keyword Succeeds`.
        """
        element = self._build_element_info(locator)
        self.log.debug('Waiting for page to contain element "{}"'.format(element.name))
        self._call_wait_function(function=self.driver.wait_until_page_contains_element,
                                 timeout=timeout,
                                 attempts=attempts,
                                 attempt_delay=attempt_delay,
                                 locator=element.locator,
                                 error=error)

    # Public, element lookups

    def current_frame_contains(self, text, loglevel='INFO'):
        """
        Verifies that current frame contains `text`.
        """
        self.log.debug('Current frame should contains text: "{}"'.format(text))
        self.driver.current_frame_contains(text, loglevel)

    def current_frame_should_not_contain(self, text, loglevel='INFO'):
        """
        Verifies that current frame should not contains `text`.
        """
        self.driver.current_frame_should_not_contain(text, loglevel)

    def element_should_contain(self, locator, expected, message=''):
        """
        Verifies element identified by `locator` contains text `expected`.
        If you wish to assert an exact (not a substring) match on the text
        of the element, use `Element Text Should Be`.
        `message` can be used to override the default error message.
        """
        locator_text = self._get_locator(locator)
        self.driver.element_should_contain(locator_text, expected, message)


    def frame_should_contain(self, locator, text, loglevel='INFO'):
        """
        Verifies frame identified by `locator` contains `text`.
        """
        locator_text = self._get_locator(locator)
        self.driver.frame_should_contain(locator_text, text, loglevel)

    def page_should_contain(self, text, loglevel='INFO'):
        """Verifies that current page contains `text`.

        If this keyword fails, it automatically logs the page source
        using the log level specified with the optional `loglevel` argument.
        Giving `NONE` as level disables logging.
        """
        self.driver.page_should_contain(text, loglevel)


    def page_should_contain_element(self, locator, message='', loglevel='INFO'):
        """
        Verifies element identified by `locator` is found on the current page.
        `message` can be used to override default error message.
        """
        locator_text = self._get_locator(locator)
        self.driver.page_should_contain_element(locator_text, message, loglevel)

    def page_should_not_contain(self, text, loglevel='INFO'):
        """
        Verifies the current page does not contain `text`.
        """
        self.driver.page_should_not_contain(text, loglevel)

    def page_should_not_contain_element(self, locator, message='', loglevel='INFO'):
        """
        Verifies element identified by `locator` is not found on the current page.
        `message` can be used to override the default error message.
        """
        locator_text = self._get_locator(locator)
        self.driver.page_should_not_contain_element(locator_text, message, loglevel)

    # Public, attributes

    def assign_id_to_element(self, locator, identifier):
        """
        Assigns a temporary identifier to element specified by `locator`.
        This is mainly useful if the locator is complicated/slow XPath expression.
        Identifier expires when the page is reloaded.
        """
        locator_text = self._get_locator(locator)
        self.driver.assign_id_to_element(locator_text, identifier)

    def element_should_be_disabled(self, locator):
        """
        Verifies that element identified with `locator` is disabled.
        """
        locator_text = self._get_locator(locator)
        self.driver.element_should_be_disabled(locator_text)

    def element_should_be_enabled(self, locator):
        """
        Verifies that element identified with `locator` is enabled.
        """
        locator_text = self._get_locator(locator)
        self.driver.element_should_be_enabled(locator_text)


    def element_should_be_visible(self, element, message=''):
        """
        Verifies that the element identified by `element` is visible.
        Herein, visible means that the element is logically visible, not optically
        visible in the current browser view port. For example, an element that carries
        display:none is not logically visible, so using this keyword on that element
        would fail.

        `message` can be used to override the default error message.

        NOTE: This method is designed to immediately fail if the element is not visible. If this is not the desired
        behavior, use wait_until_element_is_visible() instead.
        """
        element = self._build_element_info(element)

        if self.is_ready(element, check_is_enabled=(not element.is_disabled)):
            self.log.debug('Verifying element: "%s" should be visible.' % element.name)
            self.driver.element_should_be_visible(element.locator, message)
        else:
            raise ElementNotReady(element.name)

    def element_should_not_be_visible(self, locator, message=''):
        """
        Verifies that the element identified by `locator` is NOT visible.
        This is the opposite of `Element Should Be Visible`.
        `message` can be used to override the default error message.
        """
        locator = self._get_locator(locator)

        self.log.debug('Verifying element: %s should not be visible.' % locator)
        self.driver.element_should_not_be_visible(locator, message)


    def element_text_should_be(self, element, expected_text, message=''):
        """
        Verifies element identified by `locator` exactly contains text `expected`.
        In contrast to `Element Should Contain`, this keyword does not try
        a substring match but an exact match on the element identified by `locator`.
        `message` can be used to override the default error message.
        """

        locator = self._get_locator(element)
        self.driver.element_text_should_be(locator, expected_text, message)

    def get_element_attribute(self, attribute_locator, attribute=None):
        """ Return value of element attribute.

        :param attribute_locator: If attribute is not passed, consists of element locator followed by an @ sign and
                                  attribute name, for example "element_id@class".

                                  If attribute is passed, then this should be an element or locator string, without
                                  the @ sign and attribute name.
        :param attribute: The attribute to read
        :return: The value of the attribute
        """
        element = self._build_element_info(attribute_locator)

        locator_string = element.locator
        if attribute is not None:
            locator_string += '@{}'.format(attribute)

        return self.call_selenium_driver_function(self.driver.get_element_attribute, locator_string)

    def get_horizontal_position(self, locator):
        """
        Returns horizontal position of element identified by `locator`.
        The position is returned in pixels off the left side of the page,
        as an integer. Fails if a matching element is not found.
        """
        self.driver.get_horizontal_position(locator)

    def get_value(self, locator):
        """
        Returns the value attribute of element identified by `locator`.
        """
        element = self._build_element_info(locator)
        if self.is_ready(element):
            return self.driver.get_value(element.locator)
        else:
            raise ElementNotReady(str(element.locator))

    def get_text(self, locator, check_is_ready=True):
        """
        Returns the text value of element identified by `locator`.
        """

        element = self._build_element_info(locator)
        if check_is_ready:
            is_ready = self.is_ready(element)
        else:
            is_ready = True

        if is_ready:
            return self.driver.get_text(element.locator)
        else:
            raise ElementNotReady(str(locator))

    def get_vertical_position(self, locator):
        """
        Returns vertical position of element identified by `locator`.
        The position is returned in pixels off the top of the page,
        as an integer. Fails if a matching element is not found.
        """
        self.driver.get_vertical_position(locator)

    # Public, mouse input/events
    def focus(self, locator):
        """
        Sets focus to element identified by `locator`.
        """
        self.driver.focus(locator)

    def drag_and_drop(self, source, target):
        """
        Drags element identified with `source` which is a locator.
        Element can be moved on top of another element with `target`
        argument.
        `target` is a locator of the element where the dragged object is
        dropped.
        """
        self.driver.drag_and_drop(source, target)

    def drag_and_drop_by_offset(self, source, xoffset, yoffset):
        """
        Drags element identified with `source` which is a locator.
        Element will be moved by xoffset and yoffset.  each of which is a
        negative or positive number specify the offset.
        """
        self.driver.drag_and_drop_by_offset(source, xoffset, yoffset)

    def mouse_down(self, locator):
        """
        Simulates pressing the left mouse button on the element specified by `locator`.
        The element is pressed without releasing the mouse button.
        """
        locator = self._get_locator(locator)
        self.driver.mouse_down(locator)

    def mouse_out(self, locator):
        """
        Simulates moving mouse away from the element specified by `locator`.
        """
        locator = self._get_locator(locator)
        self.driver.mouse_out(locator)

    def mouse_over(self, locator):
        """
        Simulates hovering mouse over the element specified by `locator`.
        """
        locator = self._get_locator(locator)
        self.driver.mouse_over(locator)

    def mouse_up(self, locator):
        """
        Simulates releasing the left mouse button on the element specified by `locator`.
        """
        locator = self._get_locator(locator)
        self.driver.mouse_up(locator)

    def open_context_menu(self, locator):
        """
        Opens context menu on element identified by `locator`.
        """
        element = self._build_element_info(locator)
        self.log.debug('Opens context menu on element identified by locator:{0}'.format(element.name))
        self.driver.open_context_menu(element.locator)

    def simulate(self, locator, event):
        """
        Simulates `event` on element identified by `locator`.
        This keyword is useful if element has OnEvent handler that needs to be
        explicitly invoked.
        """
        self.driver.simulate(locator, event)

    def press_key(self, locator, key):
        """
        Simulates user pressing key on element identified by `locator`.
        `key` is either a single character, or a numerical ASCII code of the key
        lead by '\\'.
        """
        self.driver.press_key(locator, key)

    # Public, links

    def click_link(self, locator):
        """
        Clicks a link identified by locator.
        """
        self.driver.click_link(locator)


    def get_all_links(self):
        """
        Returns a list containing ids of all links found in current page.
        If a link has no id, an empty string will be in the list instead.
        """
        self.driver.get_all_links()


    def mouse_down_on_link(self, locator):
        """
        Simulates a mouse down event on a link.
        """
        self.driver.mouse_down_on_link(locator)


    def page_should_contain_link(self, locator, message='', loglevel='INFO'):
        """
        Verifies link identified by `locator` is found from current page.
        """
        self.driver.page_should_contain_link(locator, message, loglevel)


    def page_should_not_contain_link(self, locator, message='', loglevel='INFO'):
        """
        Verifies image identified by `locator` is not found from current page.
        """
        self.driver.page_should_not_contain_link(locator, message, loglevel)


    # Public, images

    def click_image(self, locator):
        """
        Clicks an image found by `locator`.
        """
        self.driver.click_image(locator)


    def mouse_down_on_image(self, locator):
        """
        Simulates a mouse down event on an image.
        """
        self.driver.mouse_down_on_image(locator)


    def page_should_contain_image(self, locator, message='', loglevel='INFO'):
        """
        Verifies image identified by `locator` is found from current page.
        """
        self.driver.page_should_contain_image(locator, message, loglevel)


    def page_should_not_contain_image(self, locator, message='', loglevel='INFO'):
        """
        Verifies image identified by `locator` is found from current page.
        See `Page Should Contain Element` for explanation about `message` and
        `loglevel` arguments.
        """
        self.driver.page_should_not_contain_image(locator, message, loglevel)


    # Public, xpath

    def get_matching_xpath_count(self, xpath):
        """
        Returns number of elements matching `xpath`
        If you wish to assert the number of matching elements, use
        `Xpath Should Match X Times`.
        """
        self.driver.get_matching_xpath_count(xpath)

    def xpath_should_match_x_times(self, xpath, expected_xpath_count, message='', loglevel='INFO'):
        """
        Verifies that the page contains the given number of elements located by the given `xpath`.
        See `Page Should Contain Element` for explanation about `message` and
        `loglevel` arguments.
        """
        self.driver.xpath_should_match_x_times(xpath, expected_xpath_count, message, loglevel)


    # _selectelement.py

    def get_list_items(self, locator):
        """
        Returns the values in the select list identified by `locator`.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.get_list_items(locator)


    def get_selected_list_label(self, locator):
        """
        Returns the visible label of the selected element from the select list identified by `locator`.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.get_selected_list_label(locator)


    def get_selected_list_labels(self, locator):
        """
        Returns the visible labels of selected elements (as a list) from the select list identified by `locator`.
        Fails if there is no selection.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.get_selected_list_labels(locator)


    def get_selected_list_value(self, locator):
        """
        Returns the value of the selected element from the select list identified by `locator`.
        Return value is read from `value` attribute of the selected element.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        return self._call_selenium_function(self.driver.get_selected_list_value, locator)
        # element = self._build_element_info(locator)
        # if self.is_ready(element):
        #     return self.driver.get_selected_list_value(element.locator)


    def get_selected_list_values(self, locator):
        """
        Returns the values of selected elements (as a list) from the select list identified by `locator`.
        Fails if there is no selection.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.get_selected_list_values(locator)


    def list_selection_should_be(self, locator, *items):
        """
        Verifies the selection of select list identified by `locator` is exactly `*items`.
        If you want to test that no option is selected, simply give no `items`.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.list_selection_should_be(locator, *items)


    def list_should_have_no_selections(self, locator):
        """
        Verifies select list identified by `locator` has no selections.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.list_should_have_no_selections(locator)


    def page_should_contain_list(self, locator, message='', loglevel='INFO'):
        """
        Verifies select list identified by `locator` is found from current page.
        See `Page Should Contain Element` for explanation about `message` and
        `loglevel` arguments.
        Key attributes for lists are `id` and `name`.
        """
        self.driver.page_should_contain_list(locator, message, loglevel)


    def page_should_not_contain_list(self, locator, message='', loglevel='INFO'):
        """
        Verifies select list identified by `locator` is not found from current page.
        See `Page Should Contain Element` for explanation about `message` and
        `loglevel` arguments.
        Key attributes for lists are `id` and `name`.
        """
        self.driver.page_should_not_contain_list(locator, message, loglevel)


    def select_all_from_list(self, locator):
        """
        Selects all values from multi-select list identified by `id`.

        Key attributes for lists are `id` and `name`.
        """
        self.driver.select_all_from_list(locator)


    def select_from_list(self, locator, *items):
        """
        Selects `*items` from list identified by `locator`
        If more than one value is given for a single-selection list, the last
        value will be selected. If the target list is a multi-selection list,
        and `*items` is an empty list, all values of the list will be selected.
        *items try to select by value then by label.
        It's faster to use 'by index/value/label' functions.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.select_from_list(locator, *items)


    def select_from_list_by_index(self, locator, *indexes):
        """
        Selects `*indexes` from list identified by `locator`
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.select_from_list_by_index(locator, *indexes)


    def select_from_list_by_value(self, locator, *values):
        """
        Selects `*values` from list identified by `locator`
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        element = self._build_element_info(locator)
        if self.is_ready(element):
            return self.driver.select_from_list_by_value(element.locator, *values)
        else:
            raise ElementNotReady(str(locator))


    def select_from_list_by_label(self, locator, *labels):
        """
        Selects `*labels` from list identified by `locator`
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.select_from_list_by_label(locator, *labels)


    def unselect_from_list(self, locator, *items):
        """
        Unselects given values from select list identified by locator.
        As a special case, giving empty list as `*items` will remove all
        selections.
        *items try to unselect by value AND by label.
        It's faster to use 'by index/value/label' functions.
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.unselect_from_list(locator, *items)


    def unselect_from_list_by_index(self, locator, *indexes):
        """
        Unselects `*indexes` from list identified by `locator`
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.unselect_from_list_by_index(locator, *indexes)


    def unselect_from_list_by_value(self, locator, *values):
        """
        Unselects `*values` from list identified by `locator`
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.unselect_from_list_by_value(locator, *values)

    def unselect_from_list_by_label(self, locator, *labels):
        """
        Unselects `*labels` from list identified by `locator`
        Select list keywords work on both lists and combo boxes. Key attributes for
        select lists are `id` and `name`.
        """
        self.driver.unselect_from_list_by_label(locator, *labels)

        # _tableelement.py

    def get_table_cell(self, table_locator, row, column, loglevel='INFO'):
        """
        Returns the content from a table cell.

        Row and column number start from 1. Header and footer rows are
        included in the count. This means that also cell content from
        header or footer rows can be obtained with this keyword.
        """
        self.driver.get_table_cell(table_locator, row, column, loglevel)


    def table_cell_should_contain(self, table_locator, row, column, expected, loglevel='INFO'):
        """
        Verifies that a certain cell in a table contains `expected`.

        Row and column number start from 1. This keyword passes if the
        specified cell contains the given content. If you want to test
        that the cell content matches exactly, or that it e.g. starts
        with some text, use `Get Table Cell` keyword in combination
        with built-in keywords such as `Should Be Equal` or `Should
        Start With`.
        """
        self.driver.table_cell_should_contain(table_locator, row, column, expected, loglevel)


    def table_column_should_contain(self, table_locator, col, expected, loglevel='INFO'):
        """
        Verifies that a specific column contains `expected`.

        The first leftmost column is column number 1. If the table
        contains cells that span multiple columns, those merged cells
        count as a single column. For example both tests below work,
        if in one row columns A and B are merged with colspan="2", and
        the logical third column contains "C".
        """
        self.driver.table_column_should_contain(table_locator, col, expected, loglevel)


    def table_footer_should_contain(self, table_locator, expected, loglevel='INFO'):
        """
        Verifies that the table footer contains `expected`.
        With table footer can be described as any <td>-element that is
        child of a <tfoot>-element.
        """
        self.driver.table_footer_should_contain(table_locator, expected, loglevel)


    def table_header_should_contain(self, table_locator, expected, loglevel='INFO'):
        """
        Verifies that the table header, i.e. any <th>...</th> element, contains `expected`.
        """
        self.driver.table_header_should_contain(table_locator, expected, loglevel)


    def table_row_should_contain(self, table_locator, row, expected, loglevel='INFO'):
        """
        Verifies that a specific table row contains `expected`.
        The uppermost row is row number 1. For tables that are
        structured with thead, tbody and tfoot, only the tbody section
        is searched. Please use `Table Header Should Contain` or
        `Table Footer Should Contain` for tests against the header or
        footer content.
        If the table contains cells that span multiple rows, a match
        only occurs for the uppermost row of those merged cells.
        """
        self.driver.table_row_should_contain(table_locator, row, expected, loglevel)


    def table_should_contain(self, table_locator, expected, loglevel='INFO'):
        """
        Verifies that `expected` can be found somewhere in the table.
        """
        self.driver.table_should_contain(table_locator, expected, loglevel)

    # noinspection PyPep8Naming
    def NAS_Scan_Disk(self):
        """
        Scans disks for any errors
        """
        self.configure_jbod()
        self.get_to_page('Settings')
        self.driver.wait_until_element_is_visible('settings_utilities_link', 60)
        self.driver.click_element('settings_utilities_link')
        self.driver.wait_until_element_is_visible('settings_utilities_ScanDisk_button', 60)
        self.driver.double_click_element('settings_utilities_ScanDisk_button')
        self.driver.wait_until_element_is_visible('id=popup_apply_button', 60, )
        self.driver.click_element('id=popup_apply_button')

    def reload_page(self):
        """
        Simulates user reloading page
        """
        self.driver.reload_page()

    def wait_and_click(self, locator, timeout=None):
        """
        Waits until button is clickable and then click
        """
        self.wait_until_element_is_clickable(locator, timeout)
        self.click_button(locator)

    # noinspection PyProtectedMember
    def find_group_in_list(self, group):
        """
        Go to the end of the group list
        """
        self.get_to_group_tab()
        while not (self.driver._is_visible(group)):
            self.driver.click_element('users_groupDown_link')

    def update_ssh_password(self, newpassword, confirm_password=None, time_out=60, retry=3):
        # Todo: add code to update the password through Selenium or rest
        # Return 0 on success, 1 on failure
        result = 0
        if confirm_password is None:
            confirm_password = newpassword
        while retry >= 0:
            try:
                self.get_to_page('Settings')
            except Exception as e:
                if retry == 0:
                    raise Exception("exception: {}".format(repr(e)))
                self.log.info("Retry {0} times for get to page".format(3-retry))
                retry -= 1
                self.close_webUI()
                continue
            else:
                break

        self.wait_until_element_is_clickable(Elements.NETWORK_BUTTON, timeout=time_out)
        self.click_wait_and_check(Elements.NETWORK_BUTTON, Elements.SETTINGS_NETWORK_SSH_LINK, timeout=time_out)
        self.click_wait_and_check(Elements.SETTINGS_NETWORK_SSH_LINK, Elements.SETTINGS_NETWORK_SSH_DIAG, timeout=time_out)
        self.input_text_check(Elements.SETTINGS_NETWORK_SSH_PASSWORD, newpassword)
        self.input_text_check(Elements.SETTINGS_NETWORK_SSH_CONFIRM_PASSWORD, confirm_password)
        time.sleep(2)

        if confirm_password != newpassword:
            self.click_wait_and_check(Elements.SETTINGS_NETWORK_SSH_SAVE, Elements.SETTINGS_NETWORK_SSH_TIP2_PASSWORD_ERROR, timeout=time_out)
            time.sleep(5)
            self.check_attribute(Elements.SETTINGS_NETWORK_SSH_TIP2_PASSWORD_ERROR, expected_value='Passwords do not match.', attribute_type='title')
        else:
            self.click_wait_and_check(Elements.SETTINGS_NETWORK_SSH_SAVE, visible=False, timeout=time_out)
            time.sleep(5)

        return result

    def execute_javascript(self, *code):
        """Executes the given JavaScript code.

        `code` may contain multiple lines of code and may be divided into
        multiple cells in the test data. In that case, the parts are
        catenated together without adding spaces.

        If `code` is an absolute path to an existing file, the JavaScript
        to execute will be read from that file. Forward slashes work as
        a path separator on all operating systems.

        The JavaScript executes in the context of the currently selected
        frame or window as the body of an anonymous function. Use _window_ to
        refer to the window of your application and _document_ to refer to the
        document object of the current frame or window, e.g.
        _document.getElementById('foo')_.

        This keyword returns None unless there is a return statement in the
        JavaScript. Return values are converted to the appropriate type in
        Python, including WebElements.

        """
        self.driver.execute_javascript(*code)

    def check_attribute(self, locator, expected_value, attribute_type):
        # #@ Brief: Verify if the expected value of specified attribute is correct
        #
        # @param locator: The locator of element
        # @param expected_value: The expected value of specified attribute
        # @param attribute_type: Can be "href", "datafld", "style"..etc.
        locator = self._get_locator(locator)
        self.log.debug('Verifying attribute: "%s" of element: "%s" should be "%s".' \
                       % (attribute_type, locator, expected_value))
        element = self.element_find(locator)
        value = element.get_attribute(attribute_type)

        if not value:
            raise Exception('Cannot find the attribute value in element: "%s".' % locator)

        if attribute_type == 'href' and not expected_value.startswith('http'):
            # Need internal ip address in the begining of expected_value
            internal_ipaddress = self.uut.get('internalIPAddress')
            expected_value = "http://" + internal_ipaddress + expected_value

        if value != expected_value:
            raise Exception('The attritube: "%s" of element: "%s" should be "%s" but it is %s.' \
                            % (attribute_type, locator, expected_value, value))

    def get_window_size(self):
        """
            Returns current window size as `width` then `height`.
        """
        return self.driver.get_window_size()

    def set_window_size(self, width, height):
        """
            Sets the `width` and `height` of the current window to the specified values.
        """
        self.driver.set_window_size(width, height)

    def enable_nfs_service(self):
        """
            Enable NFS
        """
        try:
            self.click_element(Elements.NETWORK_BUTTON)
            time.sleep(3)
            self.log.info('Enable NFS Service')
            nfs_status_text = self.get_text('css = #settings_networkNFS_switch+span .checkbox_container')
            if nfs_status_text == 'ON':
                self.log.info('NFS is enabled, status = {0}'.format(nfs_status_text))
            else:
                self.log.info('Turning ON NFS service')
                self.click_wait_and_check('css=#settings_networkNFS_switch+span .checkbox_container',
                                          'css=#settings_networkNFS_switch+span .checkbox_container .checkbox_on', visible=True)
        except Exception as e:
            self.log.exception('FAILED: Unable to enable nfs service!! Exception: {}'.format(repr(e)))
            raise
            
    def enable_ftp_service(self):
        """
            Enable FTP service
        """
        try:
            self.get_to_page('Settings')
            time.sleep(2)
            self.click_wait_and_check('nav_settings_link', 'settings_network_link', visible=True)
            self.click_wait_and_check('settings_network_link',
                                      'css=#settings_networkFTPAccess_switch+span .checkbox_container', visible=True)
            self.log.info('==> Configure ftp')
            self.wait_until_element_is_visible('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                               timeout=15)
            ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
            if ftp_status_text == 'ON':
                self.log.info('FTP is enabled, status = {0}'.format(ftp_status_text))
            else:
                self.log.info('Turn ON FTP service')
                self.click_wait_and_check('css=#settings_networkFTPAccess_switch+span .checkbox_container',
                                          'popup_ok_button', visible=True)
                self.click_wait_and_check('popup_ok_button', visible=False)
                ftp_status_text = self.get_text('css=#settings_networkFTPAccess_switch+span .checkbox_container')
        except Exception as e:
            self.log.exception('Failed: Unable to configure ftp!! Exception: {}'.format(e))
            raise
        else:
            if ftp_status_text == 'ON':
                self.log.info("FTP is enabled successfully. Status = {}".format(ftp_status_text))
            else:
                self.log.error("Unable to turn on FTP service, now is set to {}".format(ftp_status_text))

    def enable_ftp_share_access(self, share_name, access):
        """
            Set access permission to the ftpShare for anonymous user
            
            @access: Public share access permission for anonymous
        """
        self.enable_ftp_service()
        try:
            self.log.info('Configure FTP share with {}'.format(access))
            self.get_to_page('Shares')
            time.sleep(2)
            self.click_wait_and_check('nav_shares_link',
                                      'shares_createShare_button', visible=True)
            self.wait_until_element_is_visible('shares_share_{}'.format(share_name), timeout=30)
            self.log.info('Click {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_public_switch+span .checkbox_container', visible=True)
            self.log.info('Switch the ftp access to ON')
            self.wait_until_element_is_visible('css=#shares_ftp_switch+span .checkbox_container', timeout=15)
            ftp_status = self.get_text('css=#shares_ftp_switch+span .checkbox_container')
            if ftp_status == 'ON':
                self.log.info("ftp has been turned to ON, status = {}".format(ftp_status))
            else:
                self.click_wait_and_check('css=#shares_ftp_switch+span .checkbox_container', 'ftp_dev', visible=True)
            self.log.info('ftp service is set to ON')
            time.sleep(2)
            self.click_link_element('ftp_dev', access)
            if self.is_element_visible('shares_ftpSave_button', 5):
                self.click_wait_and_check('shares_ftpSave_button', visible=False)
            # Waiting for "Updating" to go away
            self.wait_until_element_is_clickable(locator='shares_share_{}'.format(share_name),timeout=90)
        except Exception as e:
            self.log.error('FAILED: Unable to config {}, exception: {}'.format(share_name, repr(e)))
            raise
        else:
            self.log.info("Save {} permission to Anonymous on {} share.".format(access, share_name))
            self.close_webUI()
            
    def enable_nfs_share_access(self, share_name, access):
        """
            Configure nfsShare to the access value passed by the argument
            
            @share_name: name of the share to enable nfs access to
            @access: The access permission of nfs Share
        """
        productName = self.uut[Fields.product]
     
        if productName == 'Glacier' or productName == 'Mirrorman':
            self.log.info('{} supports NFS service, but does not need to be set in the settings'.format(productName))
        else:
            self.enable_nfs_service()
        try:              
            self.log.info('Configure NFS {0} with {1}'.format(share_name, access))          
            self.get_to_page('Shares')
            time.sleep(2)
            self.log.info('Click on {}'.format(share_name))
            self.click_wait_and_check('shares_share_{}'.format(share_name),
                                      'css=#shares_nfs_switch+span .checkbox_container', visible=True)
            self.log.info('Switch the nfs to ON')
            nfs_status = self.get_text('css=#shares_nfs_switch+span .checkbox_container')
            if nfs_status == 'ON':
                self.log.info("nfs has been turned to ON, status = {}".format(nfs_status))
            else:
                self.click_wait_and_check('css=#shares_nfs_switch+span .checkbox_container',
                                          "css=#shares_nfs_switch+span .checkbox_container .checkbox_on", visible=True)
            time.sleep(2)
            self.click_wait_and_check('shares_editNFS_link',
                                      'css=#shares_write_switch+span .checkbox_container', visible=True)
            time.sleep(2)
            nfsWrite_status = self.get_text('css=#shares_write_switch+span .checkbox_container')
            if nfsWrite_status == 'ON':
                self.log.info("nfs Write access has been turned to ON, status = {}".format(nfsWrite_status))
            else:
                self.click_wait_and_check('css=#shares_write_switch+span .checkbox_container',
                                          'css=#shares_write_switch+span .checkbox_container .checkbox_on', visible=True)
            self.click_wait_and_check('shares_editNFSSave_button', visible=False, timeout=20)
            time.sleep(10)   # waiting for the applying done
        except Exception as e:
            self.log.error("FAILED: Unable to config public share. exception: {}".format(repr(e)))
            raise
    
    def toggle_media_streaming(self, enable_streaming=True): 
        """
            Enable/Disable Media Streaming
        """

        try:                    
            self.click_element(Elements.SETTINGS_MEDIA)
            media_status_text = self.get_text('css = #settings_mediaDLNA_switch+span .checkbox_container')
            if enable_streaming:
                if media_status_text == 'ON':
                    self.log.info('Media streaming is enabled, status = {0}'.format(media_status_text))
                else:
                    self.log.info('Turning ON Media streaming')
                    self.click_element('css = #settings_mediaDLNA_switch+span .checkbox_container')
                    time.sleep(1)
                    self.click_button(locator='popup_ok_button')
                    self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)
                    self.log.info('Media streaming was turned ON')
            else:
                if media_status_text == 'OFF':
                    self.log.info('Media streaming is disabled, status = {0}'.format(media_status_text))
                else:
                    self.log.info('Turning OFF Media streaming')
                    self.click_element('css = #settings_mediaDLNA_switch+span .checkbox_container')
                    self.wait_until_element_is_not_visible(Elements.UPDATING_STRING)
                    self.log.info('Media streaming was turned OFF')
        except Exception as e:
            self.log.error('FAILED: Could not enable Media Streaming. exception: {}'.format(repr(e)))
    
    def enable_media_serving(self, share_name): 
        """
            Enable Media Serving
            
            @share_name: name of the share to enable media serving to
        """  
        try:              
            self.log.info('Configure Media Serving: {0}'.format(share_name))          
            self.get_to_page('Shares')
            time.sleep(2)
            self.log.info('Click on {}'.format(share_name))
            self.click_element('shares_share_{}'.format(share_name))
            time.sleep(2)
            self.log.info('Switching media serving ON')
            media_serving_status = self.get_text('css=#shares_media_switch+span .checkbox_container')
            if media_serving_status == 'ON':
                self.log.info("media serving was ON, status = {}".format(media_serving_status))
            else:
                self.click_element('css=#shares_media_switch+span .checkbox_container')
            self.log.info('media serving was turned ON')
            time.sleep(2)
        
        except Exception as e:
            self.log.error("FAILED: Media Serving was not enabled. exception: {}".format(repr(e)))
                                          
    # #@ Brief: Selects the window found with `locator` as the context of actions.
    #
    # @param locator: Can be in 3 formats:
    # 1. window title -> title=My Document
    # 2. window javascript name -> name=${name}
    # 3. window's current url -> url=http://google.com
    # If locator is 'None', it will select the main window
    def select_window(self, locator=None):
        self.driver.select_window(locator)

    # #@ Brief: Closes currently opened pop-up window.
    #
    def close_window(self):
        self.driver.close_window()

    # #@ Brief: Re-try the target locator until check_locator is verified
    #           If check_locator is None, set check_locator to target locator and verify if it is invisible after click
    #
    def click_wait_and_check(self, locator, check_locator=None, visible=True, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to click
        :param check_locator: new locator you want to verify if target locator is being clicked
        :param visible: condition of the new locator to confirm if target locator is being clicked
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait for the element visible/invisible
        """
        locator = self._build_element_info(locator)
        if not check_locator:
            check_locator = locator
            visible = False
        else:
            check_locator = self._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Click [{}] locator, check [{}] locator if [{}]".
                           format(locator.name, check_locator.name, 'visible' if visible else 'invisible'))
            try:
                self.wait_and_click(locator, timeout=timeout)
                if visible:
                    self.wait_until_element_is_visible(check_locator, timeout=timeout)
                else:
                    self.wait_until_element_is_not_visible(check_locator, time_out=timeout)
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break

    # #@ Brief: Retry the link element until it is set to the correct value
    #
    def click_link_element(self, locator, linkvalue, retry=3, timeout=5):
        """
        :param locator: the link locator
        :param linkvalue: the value of link locator you want to set
        :param timeout: how long to wait for timeout
        :param retry: how many attempts to retry
        """
        while retry >= 0:
            try:
                self.wait_and_click(locator, timeout=timeout)
                self.wait_and_click("link="+linkvalue, timeout=timeout)
                link_text = self.get_text(locator)
                self.log.info("Set link={}".format(link_text))
            except Exception as e:
                if retry == 0:
                    raise Exception("Failed to click link={} element, exception: {}".format(linkvalue, repr(e)))
                self.log.info("WARN: {} was not set, remaining {} retries".format(linkvalue, retry))
                retry -= 1
                continue
            else:
                if link_text != linkvalue:
                    self.log.info("Link is set to [{}] not [{}], remaining {} retries".format(link_text, linkvalue, retry))
                    retry -= 1
                    continue
                else:
                    break

    def click_element_at_coordinates(self, locator, xoffset, yoffset, click_delay):
        ''' Click element identified by locator at x/y coordinates of the element.
            Cursor is moved from the center of the element and x/y coordinates are calculted from that point.
        @param locator: the reference locator
        @param xoffset: x coordinate from the center of locator
        @param yoffset: y coordinate from the center of locator
        @param click_delay: the delay time between mouse_down and mouse_up
        '''
        element = self._build_element_info(locator)
        self.log.debug('Clicking at x:{0}, y:{1} coordinates of element:{2}.'.format(xoffset, yoffset, element.name))
        element = self.driver._element_find(element.locator, True, True)
        ActionChains(self.driver._current_browser()).move_to_element(element).move_by_offset(xoffset, yoffset).click_and_hold().perform()
        time.sleep(click_delay)
        ActionChains(self.driver._current_browser()).move_to_element(element).move_by_offset(xoffset, yoffset).release().perform()

    # #@ Brief: Retry the text element until it is set to the correct value
    #
    def input_text_check(self,
                         locator,
                         text,
                         do_login=True,
                         check_browser=True,
                         check_page=True,
                         retry=3,
                         retry_delay=1):
        """ Fill in a text field and confirm the data is correct. Wrapper for input_text to allow retries.

        :param locator: The locator string or element object of the text field
        :param text: The text to input
        :param do_login: If True, login to the web UI if necessary
        :param check_browser: If True, open the web UI if necessary
        :param check_page: If True, switch to the element's page if necessary
        :param retry: The number of times to retry in the event the element is not ready or the text doesn't change
                      as expected
        :param retry_delay: The number of seconds to wait after a failure before retrying
        """
        element = self._build_element_info(locator)
        for i in xrange(retry, 0, -1):
            try:
                self.input_text(locator=element,
                                text=text,
                                do_login=do_login,
                                check_page=check_page,
                                check_browser=check_browser,
                                validate_entry=True)
            except (ElementNotReady, wd_exceptions.TextMismatch) as e:
                if i > 1:
                    # There are still retries left
                    self.log.info("{}. {} retries left.".format(e.message,
                                                                i - 1))
                    time.sleep(retry_delay)
                else:
                    # Out of retries, so raise the exception
                    raise
            else:
                # Success, so exit the function
                return

    def open_context_menu_and_check(self, locator, check_locator, retry=3, timeout=5):
        """
        :param locator: the target locator you're going to right-click and open menu
        :param check_locator: new locator you want to verify if target menu is opened
        :param retry: how many retries to verify the target locator being clicked
        :param timeout: how long you want to wait for the element visible/invisible
        """
        locator = self._build_element_info(locator)
        check_locator = self._build_element_info(check_locator)
        while retry >= 0:
            self.log.debug("Open context menu by clicking element: [{}], check if element: [{}] is visible".
                           format(locator.name, check_locator.name))
            try:
                self.open_context_menu(locator)
                self.wait_until_element_is_visible(check_locator, timeout=timeout)
            except Exception as e:
                if retry == 0:
                    raise wd_exceptions.ClickElementError("Failed to open menu by clicking [{}] element, exception: {}".format(locator.name, repr(e)))
                self.log.info("Element [{}] did not click, remaining {} retries...".format(locator.name, retry))
                retry -= 1
                continue
            else:
                break
