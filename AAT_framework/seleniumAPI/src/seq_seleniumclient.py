""" Selenium methods for Sequoia

"""
__author__ = 'tran_jas'

from seleniumclient import SeleniumClient
from seleniumclient import ElementNotReady
# from webfilesAPI.src.wf_keys import Keys
from global_libraries.testdevice import Fields



from seq_ui_map import Pages
from seq_ui_map import Elements

import time
# Override the classes in ui_map
import ui_map
import seq_ui_map

ui_map.Page_Info = seq_ui_map.Page_Info
ui_map.Pages = seq_ui_map.Pages


class SeqSeleniumClient(SeleniumClient):
    """ Seq Selenium methods

    :param wf_settings: The wf dictionary of settings
    :param uut: The UUT dictionary of settings
    :param wait_time: Amount of time to wait before a method times out
    """

    def __init__(self, uut, wait_time=30):
        SeleniumClient.__init__(self, uut=uut, wait_time=wait_time)

    #
    def seq_accept_eula(self, accept_eula=None):
        hostname = 'http'
        hostname += '://{0}'.format(self.uut[Fields.internal_ip_address])
        try:
            self.driver.open_browser(hostname)

            self.log.info('accept_eula: {}'.format(accept_eula))

            if self.is_element_visible(Elements.EULA_LINK):
                self.log.info("EULA is displayed")
                self.click_element_at_coordinates(Elements.EULA_LINK, -200, 0, 0)

                if self.is_element_visible(Elements.EULA_ACCEPT_BUTTON):
                    self.log.info("EULA accept button is displayed")
                    self.click_element(Elements.EULA_ACCEPT_BUTTON)

                    if self.is_element_visible(Elements.USERNAME):
                        self.input_text(Elements.USERNAME, 'admin')
                        self.input_text(Elements.LASTNAME, 'test')
                        time.sleep(3)
                        self.click_element('css=#getting_started_wizard_add_user_edit_admin_form > span.form_controls > input[type="submit"]')
                        time.sleep(5)
                        self.click_element(Elements.NEXT_BUTTON)
                        time.sleep(3)
                        self.click_element(Elements.FINISH_BUTTON)
                else:
                    if accept_eula:
                        self.log.error("EULA accept button is not displayed")
                    else:
                        self.log.info("EULA accept button is not displayed")
            else:
                self.log.info("EULA already accepted")

        except Exception, ex:
            self.log.exception("Fail to login - {}".format(ex))
