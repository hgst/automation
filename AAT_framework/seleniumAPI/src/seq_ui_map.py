""" This file is used by the SeqSeleniumClient class to navigate the UI

"""
from collections import namedtuple

from ui_map import ElementInfo

class Pages(object):
    """ This class contains a list of the pages.
        They must also be added to the Page_Info class to define them.
    """
    EULA_PAGE = 'EULA page'
    GETTING_STARTED_PAGE = 'Getting Started page'
    USERS_PAGE = 'User page'
    HOME_PAGE = 'Home page'
    SHARES_PAGE = 'Share page'
    CLOUD_ACCESS_PAGE = 'Cloud access page'
    SAFEPOINT_PAGE = 'Safepoint page'
    SETTINGS_PAGE = 'Settings page'

class Elements(object):
    """ This class contains UI elements and information on each element
    """

    # EULA page
    EULA_ACCEPT_BUTTON = ElementInfo(name='Accept Button - EULA page', locator='eula_accept_btn')
    EULA_LINK = ElementInfo(name='EULA Link - EULA page', locator='eulaShowLink')

    # Getting Started page
    USERNAME = ElementInfo(name='Username - Getting Started page',
                           locator='getting_started_wizard_add_user_edit_admin_username')
    LASTNAME = ElementInfo(name='Lastname - Getting Started page',
                           locator='getting_started_wizard_add_user_edit_admin_lastname')
    SAVE_BUTTON = ElementInfo(name='Save button - Getting Started page',
            locator='css=#getting_started_wizard_add_user_edit_admin_form > span.form_controls > input[type="submit"]')

    NEXT_BUTTON = ElementInfo(name='Next Button - Getting Started page',
        locator='css=#getting_started_wizard > div.navigation.dialog_form_controls > button.forward.mochi_dialog_save_button')

    FINISH_BUTTON = ElementInfo(name='Finish Button - Getting Started page',
        locator='css=#getting_started_wizard > div.navigation.dialog_form_controls > button.finish.mochi_dialog_save_button')

    # Home page
    HOME_LINK = ElementInfo(name='Home page link - Home page', locator='nav_dashboard_link')

    # Users page
    USERS_LINK = ElementInfo(name='Users page link - Users page', locator='nav_users_link')
    CREATE_USER_BUTTON = ElementInfo(name='Creater user button - Users page', locator='create_user_btn')
    FIRSTNAME_TEXTBOX = ElementInfo(name='First name - Users page', locator='create_user_first_name')
    SAVE_USER_BUTTON = ElementInfo(name='Save user button - Users page', locator='css=input.mochi_dialog_save_button')
    TEST_USER = ElementInfo(name='Test user - Users page', locator='user_testuser')
    DELETE_USER_BUTTON = ElementInfo(name='Delete user button - Users page', locator='user_delete')
    DELETE_USER_OK_BUTTON = ElementInfo(name='Delete user ok button - Users page', locator='delete_user_ok_button')

    # Shares page
    SHARES_LINK = ElementInfo(name='Shares page link - Shares page', locator='nav_shares_link')
    CREATE_SHARE_BUTTON = ElementInfo(name='Creater share button - Shares page', locator='create_share_btn')
    SHARE_NAME_TEXTBOX = ElementInfo(name='Share name - Shares page', locator='create_share_name')
    SAVE_SHARE_BUTTON = ElementInfo(name='Save share button - Shares page',
                locator='css=#create_share_form > div.dialog_form_controls > input.mochi_dialog_save_button')
    TEST_SHARE = ElementInfo(name='Test share - Users page', locator='share_testshare')

    # Cloud Access page
    CLOUD_ACCESS_LINK = ElementInfo(name='Cloud access page link - Cloud access page', locator='nav_remoteaccess_link')
    REMOTE_ACCESS_ADMIN = ElementInfo(name='Remote access admin - Cloud access page', locator='remoteaccess_user_admin')
    CREATE_DEVICE_USER_ACCESS_BUTTON = ElementInfo(name='Creater device user access button - Cloud access page',
                                                   locator='create_device_user_access_btn')

    # Safepoint page
    SAFEPOINT_LINK = ElementInfo(name='Safepoint page link - Safepoint access page', locator='nav_safepoints_link')
    ADD_SAFEPOINT_BUTTON = ElementInfo(name='Add safepoint button - Safepoint page', locator='add_safepoint_btn')
    CANCEL_CREATE_SAFEPOINT_BUTTON = ElementInfo(name='Cancel create safepoint button - Safepoint page',
                                                   locator='css=div.navigation.dialog_form_controls > button.cancel')
    RESTORE_SAFEPOINT_BUTTON = ElementInfo(name='Add safepoint button - Safepoint page',
                                           locator='restoreSafepointButton')

    # Settings page
    SETTINGS_LINK = ElementInfo(name='Settings page link - Settings page', locator='nav_settings_link')
    DEVICE_LINK = ElementInfo(name='Device link - Settings page', locator='settings_nav_device_link')
    DEVICE_CONTAINER = ElementInfo(name='Device container - Settings page', locator='device_container')
    NETWORK_LINK = ElementInfo(name='Network link - Settings page', locator='settings_nav_network_settings')
    NETWORK_CONTAINER = ElementInfo(name='Network container - Settings page', locator='network_container')
    MEDIA_LINK = ElementInfo(name='Media link - Settings page', locator='settings_nav_media_link')
    MEDIA_CONTAINER = ElementInfo(name='Media container - Settings page', locator='media_container')
    DIAGNOSTICS_LINK = ElementInfo(name='Diagnostics link - Settings page', locator='settings_nav_diagnostics')
    DIAGNOSTICS_CONTAINER = ElementInfo(name='Diagnostics container - Settings page', locator='diagnostics_container')
    NOTIFICATIONS_LINK = ElementInfo(name='Notifications link - Settings page', locator='settings_nav_notifications')
    NOTIFICATIONS_CONTAINER = ElementInfo(name='Notifications container - Settings page',
                                          locator='notifications_container')
    UPDATE_LINK = ElementInfo(name='Update link - Settings page', locator='settings_nav_updates')
    UPDATE_CONTAINER = ElementInfo(name='Update container - Settings page', locator='update_container')


# Named tuple for page information. Overrides base class since some attributes do not apply. See the Page_Info class
# for details on each attribute.
class _PageInfo(namedtuple('ElementInfo', ['verification_element',
                                           'navigation_element',
                                           'parent_page'])):
    # noinspection PyInitNewSignature
    def __new__(cls, verification_element, navigation_element=None, parent_page=None):
        return super(_PageInfo, cls).__new__(cls,
                                             verification_element,
                                             navigation_element,
                                             parent_page)

# noinspection PyPep8Naming
class Page_Info(object):
    """ This class contains a list of all pages and information on each page. The verification_element and
        navigation_element values will be references to an attribute in the Elements class.

        verification_element - The element used to verify that the UI is on the page. It must be unique to the page
                               and must always be visible when on the page.
        navigation_element - Optional. The element to click to switch to the page. If no parent page is specified,
                             this element must always be visible from anywhere on the web UI. If a parent page is
                             specified, this element must always be visible on the parent page.
        parent_page - Optional. The page that contains the navigation element. If specified, the framework will
                      switch to the parent_page before clicking the navigation element.

    """

    # The pages are stored in a dictionary to make lookups easier
    info = {
        Pages.EULA_PAGE:         _PageInfo(verification_element=Elements.EULA_ACCEPT_BUTTON),
        Pages.GETTING_STARTED_PAGE:         _PageInfo(verification_element=Elements.USERNAME),
        Pages.HOME_PAGE:         _PageInfo(verification_element=Elements.HOME_LINK),
        Pages.USERS_PAGE:         _PageInfo(verification_element=Elements.USERS_LINK),
        Pages.SHARES_PAGE:         _PageInfo(verification_element=Elements.SHARES_LINK),
        Pages.CLOUD_ACCESS_PAGE:         _PageInfo(verification_element=Elements.CLOUD_ACCESS_LINK),
        Pages.SAFEPOINT_PAGE:         _PageInfo(verification_element=Elements.SAFEPOINT_LINK),
        Pages.SETTINGS_PAGE:         _PageInfo(verification_element=Elements.SETTINGS_LINK)}


