#!/usr/bin/python

import os
import sys
import datetime
import time
import junit_xml
import paramiko

DEVICE_IP = sys.argv[1]
SSH_PORT = sys.argv[2]
REPEAT = int(sys.argv[3])
reboot_KPI = 35
bootup_KPI = 30


def run(repeat):
    reboot_time_list = []
    boot_up_time_list = []
    reboot_time_rate = []
    boot_up_time_rate = []
    repeat_count = repeat
    while repeat != 0:
        reboot_result = get_reboot_time()
        reboot_time_list.append(reboot_result)
        if reboot_result > reboot_KPI:
            reboot_time_rate.append('FAILED')
        else:
            reboot_time_rate.append('PASSED')
        boot_up_result = get_boot_up_time()
        boot_up_time_list.append(boot_up_result)
        if boot_up_result > bootup_KPI:
            boot_up_time_rate.append('FAILED')
        else:
            boot_up_time_rate.append('PASSED')
        time.sleep(3)
        repeat -= 1

    reboot_time = round(sum(reboot_time_list) / float(len(reboot_time_list)), 1)
    boot_up_time = round(sum(boot_up_time_list) / float(len(boot_up_time_list)), 1)
    reboot_time_sr = (reboot_time_rate.count('PASSED')/ float(len(reboot_time_rate))) * 100
    boot_up_time_sr = (boot_up_time_rate.count('PASSED')/ float(len(boot_up_time_rate))) * 100

    if boot_up_time == 0 or reboot_time == 0:
        metrics_bootup = None
        metrics_reboot = None
    else:
        metrics_bootup = [["Success_Rate", int(boot_up_time_sr), "PCT"], ["BootUp_Time", boot_up_time, "secs"]]
        metrics_reboot = [["Success_Rate", int(reboot_time_sr), 'PCT'], ["Reboot_Time", reboot_time, "secs"]]

    print 'Boot up time is {} secs'.format(boot_up_time)
    print 'Reboot time is {} secs'.format(reboot_time)

    tc = []
    tc.append(junit_xml.TestCase('BootUp Time test',
                                 stdout='KPI: {} secs, Repeat {} times: '.format(bootup_KPI, repeat_count)+str(boot_up_time_list),
                                 metrics=metrics_bootup))
    tc.append(junit_xml.TestCase('Reboot Time test',
                                 stdout='KPI: {} secs, Repeat {} times: '.format(reboot_KPI, repeat_count)+str(reboot_time_list),
                                 metrics=metrics_reboot))

    return tc


def get_boot_up_time():
    result = ssh_execute('cat /proc/boot_time')
    loop = 1
    while int(result) == 0 or result is None and loop <= 100:
        print 'Boot_time not exist, try again.. loop {}'.format(loop)
        result = ssh_execute('cat /proc/boot_time')
        loop += 1
        if loop == 100:
            raise Exception('Failed to get boot_time after retry {} times'.format(loop))

    return int(result)


def get_reboot_time():
    max_boot_time = 600
    start = time.time()
    print 'Start Rebooting..'
    ssh_execute('reboot')

    while is_device_pingable():
        print 'Waiting device power off ...'
        time.sleep(1)
        if time.time() - start >= max_boot_time:
            raise Exception('Device failed to power off within {} seconds'.format(max_boot_time))

    while not is_device_pingable():
        print 'Waiting device boot up ...'
        time.sleep(1)
        if time.time() - start > max_boot_time:
            raise Exception('Timed out waiting to boot within {} seconds'.format(max_boot_time))

    ssh_connected = False
    boot_finished = ''
    i = 1
    loop = 1
    print 'Waiting device boot up and boot_finished ...'
    while not ssh_connected or 'boot_finished' not in boot_finished:
        try:
            print 'boot_finished not exist, try again.. loop {}'.format(loop)
            boot_finished = ssh_execute('ls /tmp')
            ssh_connected = True
        except Exception as ex:
            print 'ssh failed, try again {0}, {1}'.format(i, ex)
            if time.time() - start > max_boot_time:
                raise Exception('Timed out waiting ssh connect.')
            i += 1
        loop += 1
        if 'boot_finished' not in boot_finished and time.time() - start > max_boot_time:
            raise Exception('Time out waiting boot_finished file')
    reboot_time = time.time() - start

    return int(reboot_time)


def ssh_execute(command):
    try:
        ssh_info = [DEVICE_IP, 'root', "", SSH_PORT]
        ssh = SSHClient(*ssh_info)
        ssh.connect()

        print "Command : {0}\n".format(command)
        status, result = ssh.execute(command)
        ssh.close()
        return result
    except Exception as e:
        raise Exception('SSH connection error, exception: {0}'.format(repr(e)))


def is_device_pingable():
    command = 'nc -zv -w 1 {0} {1} > /dev/null 2>&1'.format(DEVICE_IP, SSH_PORT)
    response = os.system(command)
    if response == 0:
        return True
    else:
        return False


def get_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def get_version():
    command = 'cat /etc/version'
    fw_ver = ssh_execute(command).rstrip('\n')
    return fw_ver


class SSHClient():
    def __init__(self, hostname, username, password, port):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.port = int(port)
        self.connection = None

    def connect(self):
        self.connection = paramiko.SSHClient()
        self.connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.connection.connect(self.hostname, username=self.username, password=self.password, port=self.port)

    def execute(self, command):
        status = 0
        stdin, stdout, stderr = self.connection.exec_command(command)
        stdin.close()
        error = str(stderr.read())
        if error:
            status = stdout.channel.recv_exit_status()
            output = "Execute command failed, error message:\n{}".format(error)
        else:
            output = str(stdout.read())
        return status, output

    def close(self):
        self.connection.close()


if __name__ == "__main__":
    testCases = run(repeat=REPEAT)
    testsuite = junit_xml.TestSuite('KAMINO BootUP_Reboot', test_cases=testCases,
                                    timestamp=get_timestamp(),
                                    properties={'DEVICE_FW': get_version(),
                                                'DEVICE_IP': DEVICE_IP,
                                                'TCTYPE': "Perf"},
                                    package='Performance Test')
    print junit_xml.TestSuite.to_xml_string([testsuite])
    with open('output.xml', 'w') as f:
        junit_xml.TestSuite.to_file(f, [testsuite], prettyprint=True)
