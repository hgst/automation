#!/bin/bash -xe

# Container to run a python app

set +x
APPNAME=${APPNAME:-pythonapp}

# Use '-i' to run in interactive mode
RUN_CMD=
RUN_OPTS=
if [ "$1" = "-i" ]; then
    RUN_CMD=
    RUN_OPTS='-it --entrypoint /bin/bash'
fi

# Error checks
if [ $# -eq 0 ] && [ "$RUN_CMD" == "" ]; then
    echo "Usage: $0 pythonapp.py [OPTIONS] PARAMETERS"
    exit 1
fi

if [ ! -e app ]; then
    echo "Directory named \"app\" (with your code inside) not found"
    exit 1
fi

# Leave this section alone
if [ "$XPATH" = "" ]; then
    XPATH=$(pwd)
else
    echo "Adjusting path for the underlying host filesystem"
    XPATH="${XPATH}"/$(pwd | sed 's/.*workspace\///')
fi

# Pass the variables in this list to docker if they
# are defined and have any value except blank
VARS_OF_INTEREST="\
    JOB_NAME \
    HOST_IP \
    DOCKER_IP \
"
VARS_TO_PASS=
for var in $VARS_OF_INTEREST; do
    NEW_ADD="$(eval echo \$$var)"
    if [ "$NEW_ADD" ]; then
        VARS_TO_PASS="$VARS_TO_PASS -e $var=$NEW_ADD"
    fi
done
set -x

docker BUILD --tag ${APPNAME} .

rm -f container.cid

docker RUN \
    ${RUN_OPTS} \
    --tty \
    --cidfile container.cid \
    ${VARS_TO_PASS} \
    ${APPNAME} \
    ${RUN_CMD} "$@"

EXIT_CODE=$?

# Copy output.xml from inside the container
# Using || true in case there isn't an output.xml file
docker cp $(cat container.cid):/root/app/output.xml ./ || true

exit $EXIT_CODE