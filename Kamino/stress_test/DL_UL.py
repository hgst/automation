__author__ = 'Erica Lee <erica.lee@wdc.com>: DL_UL.py'
__author__ = 'Nick Yang <nick.yang@wdc.com>: DL'

from restAPI import RestAPI
from ToolAPI import Tool
import os
import logging
import time




class DL_UL(object):
    def __init__(self, uut_ip=None, email=None, password=None, port=None, env=None):
        self.uut_ip = uut_ip        
        self.email = email
        self.password = password
        self.port = port
        self._rest = RestAPI(uut_ip, email, password, env)
        self._tool = Tool(uut_ip, email, password, env)
        self.dl_throughput = []
        self.ul_throughput = []
        self.log = logging.getLogger('DL_UL')
        self.correct_userID = self._rest.get_user_id()
        logging.basicConfig(filename='result.txt', format='%(asctime)s %(name)-6s %(threadName)-10s %(levelname)-4s %(message)s', level=logging.INFO, filemode='a', datefmt='%Y-%m-%d %H:%M:%S') 

    def upload(self, src=None, dest=None, userno=0, item_no=0):
        # dest must be a folder
        # check if path of dest existed on nas or create it
        #default_path_on_nas = '/home/root/restsdk/userRoots'
        result = False
        message = None
        default_path_on_nas = '/Data/restsdk/userRoots'
        dest_path = dest.replace(default_path_on_nas, '')
        dest_path_list = dest_path.split('/')
        if dest_path_list[len(dest_path_list)-1]:
            raise Exception('destination must be a folder')
            
        dest_path_list.pop(0)
        userID = dest_path_list.pop(0)
        self.log_info('input userID: {0} is incorrect and changed to Correct User ID: {1}'.format(userID, self.correct_userID))
        if self.correct_userID != userID:
            dest = dest.replace(userID, self.correct_userID)
            userID = self.correct_userID

        dest_path_list.pop(len(dest_path_list)-1)

        if dest_path_list:
            # means need to create sub folder under userID's folder
            current_path = '/'.join((default_path_on_nas, userID)) + '/'
            for path_list in dest_path_list:
                current_path += path_list
                current_path += '/'
                print 'create folder: %s' % current_path
                self._tool.check_create_data_on_nas(destFile=current_path, userno=userno, item_no=item_no)

        if not os.path.isdir(src):
            # start to upload file to dest
            filenames = src.split('/')[-1]
            destFile = dest + filenames

            start = time.time()
            self._tool.check_create_data_on_nas(srcFile=src, destFile=destFile, userno=userno, item_no=item_no)
            end = time.time()
            
            status, items_data, user_folder_id = self._tool.search_id(destFile)
            file_id = items_data[0]['id']
            
            #file_size = self._tool.get_file_size(file_id)
            
            if 'size' not in items_data[0].keys():
                message = "No key: 'size' in data list:{0} for upload {1} to NAS: {2}".format(items_data[0], filenames, destFile)
                raise Exception(message)
            
            elif int(items_data[0]['size']):
                file_size = int(items_data[0]['size'])

            elif file_id:
                file_size = self._tool.get_file_size(file_id)
                if file_size == 0:
                    message = 'Still no size after retry twice with file ID: {0} and fileInfo: {1}'.format(file_id, items_data[0])
                    raise Exception(message)

            else:
                message = "no size and id in 'size' & 'id' field: {0} for upload {1} to NAS: {2}".format(items_data[0], sfile, destFile)
                raise Exception(message)
                
            # Perform file comparison
            #result, message = self._tool.file_comparison(local_filepath=src, NAS_file_id=file_id)
            result, message = self._tool.file_comparison(local_filepath=src,  nas_file_size=file_size, NAS_file_id=file_id)
            
            if result:
                #self.log_info("File {0} is upload successfully onto NAS: {1}".format(filenames, self.correct_userID))
                self.log_info("File {0} is upload successfully onto NAS: {1}".format(filenames, destFile))
            else:
                #self.log_error("File {0} fails to be upload onto NAS: {1}".format(filenames, self.correct_userID))
                self.log_error("File {0} fails to be upload onto NAS: {1} with {2}".format(filenames, destFile, message))
                
            # Calculate single file throughput
            duration = end - start
            throughput = round((file_size * 0.001)/(duration), 3)
            self.ul_throughput.append(throughput)

        else:
            # start to upload folder to dest
            self.log.info('Upload Folder')
            for path, dirs, filenames in os.walk(src):
                print 'path: %s' % path
                for directory in dirs:
                    destDir = path.replace(src, dest)
                    self._tool.check_create_data_on_nas(destFile=os.path.join(destDir, directory)+'/', userno=userno, item_no=item_no)
                    self.log_info("sub-Folder {0} is created successfully onto NAS: {1}".format(destDir, self.correct_userID))

                for sfile in filenames:
                    srcFile = os.path.join(path, sfile)
                    destFile = os.path.join(path.replace(src, dest), sfile)

                    #status, items_data, user_folder_id = self._tool.search_id(destFile)
                    start = time.time()
                    self._tool.check_create_data_on_nas(srcFile=srcFile, destFile=destFile, userno=userno, item_no=item_no)
                    status, items_data, user_folder_id = self._tool.search_id(destFile)
                    end = time.time()
                    file_id = items_data[0]['id']
                    #file_size = self._tool.get_file_size(file_id)
                    if 'size' not in items_data[0].keys():
                        message = "No key: 'size' in data list:{0} for upload {1} to NAS: {2}".format(items_data[0], sfile, destFile)
                        raise Exception(message)
            
                    elif items_data[0]['size']:
                        file_size = int(items_data[0]['size'])
                    elif file_id:
                        file_size = self._tool.get_file_size(file_id)  
                        if file_size == 0:
                            message = 'Still no size after retry twice with file ID: {0} and fileInfo: {1}'.format(file_id, items_data[0])
                            raise Exception(message)                  
                    else:
                        message = "no size and id in 'size' & 'id' field: {0} for upload {1} to NAS: {2}".format(items_data[0], sfile, destFile)
                        raise Exception(message)
                    
                    # Perform file comparison

                    #result, message = self._tool.file_comparison(local_filepath=srcFile, NAS_file_id=file_id)
                    result, message = self._tool.file_comparison(local_filepath=srcFile,  nas_file_size=file_size, NAS_file_id=file_id)

                    if result:
                        #self.log_info("Folder {0} is upload successfully onto NAS: {1}".format(sfile, self.correct_userID,))
                        self.log_info("Folder {0} is upload successfully onto NAS: {1}".format(sfile, destFile))
                    else:
                        #self.log_error("Folder {0} fails to be upload onto NAS: {1}".format(sfile, self.correct_userID,))
                        self.log_error("Folder {0} fails to upload to NAS: {1} with {2}".format(sfile, destFile, message))
                        
                    # Calculate single file throughput
                    duration = end - start
                    throughput = round((file_size * 0.001)/(duration), 3)
                    self.ul_throughput.append(throughput)

        return result
                       
    def download_dir_files(self, download_dest=None, download_from=None,upload_dest=None,upload_from=None):
        '''
        Download a single file or directory from NAS to local

        INPUT:
        download_dest (String) : Download destination on local environment
        download_from (String) : Download path on NAS storage
        upload_dest (String) : Upload destination on NAS
        upload_from (String) : Upload absolute path on local envrionment
        '''

        # Start Download Job
        # get all file's id & name & mimetypes under specific folder
        # nas_info_dic : (True, [{'parentID': 'NfuK-7nUCUDf_cKzjmk6rwycJIQ7y-lutUHXAorq', 'name': 'text.txt', 'id': 'OxpDGols6JcjKCEZxM8HCUIUMMc6tratz4rt5qix'}])
        file_exist, nas_info_dic, pa = self._tool.search_id(download_from)
        file_compare_result = False
        message = None
        
        # Check if the folder is exists on NAS, if exists, then continue, else return

        print "download from is: ", download_from
        folder_or_file, name = self._tool.file_or_folder(download_from) 

        # Create a folder on local if downloaded destination folder not exists
        if not os.path.exists(download_dest):
            createdir_command = 'mkdir -p {0}'.format(download_dest)
            os.system(createdir_command)

        # means path is directory (ex: /root/)
        if folder_or_file:                
                  
            # Create a folder on local if downloaded folder not exists
            dl_path = os.path.join(download_dest, name)
            if not os.path.exists(dl_path):
                createdir_command = 'mkdir -p {0}'.format(dl_path)
                os.system(createdir_command)
            
            # Download all the contents of each file/directory under the directory
            #nas_info_dic
            #[{'mimeType': 'application/x.wd.dir', 'parentID': 'Y92zf9a-d9O78Lja74Fykcs93QuSgcxW-zIx8G3I', 'name': 'aaa', 'id': 'yMtZbS6RUmVHpJ8OoIm7353XcdTfKT7EtOENxGTt'}, 
            # {'mimeType': 'application/octet-stream', 'parentID': 'Y92zf9a-d9O78Lja74Fykcs93QuSgcxW-zIx8G3I', 'name': 'bbb.txt', 'id': 'tH9KNHt-DMfhCUvZkmA4sqvwNuqHu-O3qxnTUJjj'}
            #]

            for j in range(len(nas_info_dic)):
                fileinfo = nas_info_dic[j]
                #file_mime = fileinfo['mimeType']  
                file_id = fileinfo['id']
                file_name = fileinfo['name']
                # ['childCount'] Only applicable to folder
                try:
                    file_num = int(fileinfo['childCount'])
                except KeyError:
                    pass
                
                # If current element is not a folder (ex: /root/abc.txt)
                #if file_mime != "application/x.wd.dir":
                start = time.time()
                # Write the binary content into local file
                local_path = os.path.join(dl_path, file_name)
                #file_size = self._tool.get_file_size(file_id)
                
                if 'size' not in fileinfo.keys():
                    message = "No key: 'size' in data list:{0} for download {1} from NAS".format(fileinfo, download_from+file_name)
                    raise Exception(message)
                    

                elif fileinfo['size']:
                    file_size = int(fileinfo['size'])
                elif file_id:
                    file_size = self._tool.get_file_size(file_id)
                    if file_size == 0:
                        message = 'Still no size after retry twice with file ID: {0} and fileInfo: {1}'.format(file_id, fileinfo)
                        raise Exception(message)                  
                else:
                    message = "no size and id in 'size' & 'id' field: {0} for download {1} from NAS".format(fileinfo, download_from+file_name)
                    raise Exception(message)
                
                        
                file_content = self._tool.get_file_content(file_id)
                with open(local_path, 'w') as f:
                    f.write(file_content)
                end = time.time()
            
                #Do file comparison to compare local file and NAS file
                #file_compare_result, message = self._tool.file_comparison(local_path, file_id)
                file_compare_result, message = self._tool.file_comparison(local_filepath=local_path,  nas_file_size=file_size, NAS_file_id=file_id)
                if file_compare_result:
                    self.log_info("File {0} is downloaded successfully onto local".format(download_from+file_name))
                else:
                    self.log_error("File {0} fails to be downloaded with {1}".format(download_from+file_name, message))
                                
                # Calculate single file throughput

                duration = end - start
            
                # Change unit from Byte/s to kB/s
                throughput = round((file_size * 0.001)/(duration), 3)
                self.dl_throughput.append(throughput)
                '''
                # If current element is a non-empty folder (ex: /root/hello/), recursively call download_dir_files() again
                elif (file_mime == "application/x.wd.dir") and (file_num != 0):
                    sub_download_from = download_from + file_name + '/'
                    sub_download_dest = dl_path + '/'                
                    self.download_dir_files(sub_download_dest, sub_download_from, dnum)
                '''

        # means path is file (ex: /root/abc.txt)
        else:
            start = time.time()

            # Only 1 file in nas_id_dic, so get first element is OK
            fileinfo = nas_info_dic[0]
            file_id = fileinfo['id']
            file_name = fileinfo['name']
            
            # Get file binary content by file_id
            #file_size = self._tool.get_file_size(file_id)
            if 'size' not in fileinfo.keys():
                message = "No key: 'size' in data list:{0} for download {1} from NAS".format(fileinfo, download_from)
                raise Exception(message)
            
            elif fileinfo['size']:
                file_size = int(fileinfo['size'])
            elif file_id:
                file_size = self._tool.get_file_size(file_id)
                if file_size == 0:
                    message = 'Still no size after retry twice with file ID: {0} and fileInfo: {1}'.format(file_id, fileinfo)
                    raise Exception(message)
            else:
                message = "no size and id in 'size' & 'id' field: {0} for download {1} from NAS".format(fileinfo, download_from)
                raise Exception(message)

            file_content = self._tool.get_file_content(file_id)
            local_path = os.path.join(download_dest, file_name)
            # Write the binary content into local file
            with open(local_path, 'w') as f:
                f.write(file_content)
            end = time.time()
           
            #Do file comparison to compare local file and NAS file
            #file_compare_result, message = self._tool.file_comparison(local_path, file_id)
            file_compare_result, message = self._tool.file_comparison(local_filepath=local_path,  nas_file_size=file_size, NAS_file_id=file_id)
            if file_compare_result:
                self.log_info("File {0} is downloaded successfully onto local".format(download_from))
            else:
                self.log_error("File {0} fails to be downloaded with {1}".format(download_from, message))
                
            # Calculate single file throughput
            duration = end - start
            # Change unit from Byte/s to kB/s
            throughput = round((file_size * 0.001)/(duration), 3)
            self.dl_throughput.append(throughput)

        return file_compare_result

    def delete_dir_files_on_nas(self, src=None,dest=None):
        filenames = src.split('/')[-1]
        destFile = dest + filenames
        
        status, items_data, user_folder_id = self._tool.search_id(destFile) 

        # Cannot deleate a file/folder which is not on NAS
        if not status:
            print 'no %s existed' % destFile
            return
        try:
            url = 'http://{0}/sdk/v2/files/{1}'.format(self.uut_ip, items_data[0]['id'])
            res = self._rest.delete(url)
            if res.status_code == 204:
                print "%s is deleted" % destFile
            else:
                raise Exception('Delete {0} error with: {1}'.format(destFile, res.content))
        except Exception as e:
            self.log.info('Exception: {0} when delete'.format(repr(e)))
    
    def log_info(self, msg):
        '''
        Save info in log and print on the screen at the same time
        '''
        self.log.info(msg)
        print msg
            
    def log_error(self, msg):
        '''
        Save error in log and raise exception at the same time
        '''
        self.log.error(msg)
        raise Exception(msg)