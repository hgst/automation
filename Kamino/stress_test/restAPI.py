# @ Arthor: Erica Lee <erica.lee@wdc.com>

import requests
import json
import logging




class Environment:
    env = {
        "qa1": {
                "authService": "https://qa1-auth.remotewd1.com",
                "deviceService": "https://qa1-device.remotewd1.com",
                "shareService": "https://qa1.remotewd1.com",
                "configService": "https://qa1-config.remotewd1.com"
                },
        "dev1": {
                "authService": "https://dev1-auth.remotewd1.com",
                "deviceService": "https://dev1-device.remotewd1.com",
                "shareService": "https://dev1.remotewd1.com",
                "configService": "https://qa1-config.remotewd1.com"
                }
        }

    def __init__(self):
        self.currentEnv = None
        self.envName = None

    def set_env(self, e):
        self.currentEnv = Environment.env[e]
        self.envName = e

    def get_env_name(self):
        return self.envName

    def get_auth_service(self):
        return self.currentEnv['authService']

    def get_device_service(self):
        return self.currentEnv['deviceService']

    def get_share_service(self):
        return self.currentEnv['shareService']

    def get_config_service(self):
        return self.currentEnv['configService']

class RestAPI(object):
    def __init__(self, uut_ip=None, username=None, password=None, env=None):
        self.environment = Environment()
        self.environment.set_env(env)
        self.uut_ip = uut_ip        
        self.username = username
        self.password = password
        self.create_user(self.username, self.password)
        self.get_access_token()
        self.attach_user_to_device()

    def create_user(self, username=None, password=None):
        data = {"email": self.username, "password": self.password}
        #url: 'http://qa1-auth.remotewd1.com/authservice/v1/user?'
        url = self.environment.get_auth_service()+'/authservice/v1/user?'
        
        # BasicAuth is a header that takes in the  Base64 encoding of the 'username:password' combination. 
        # For example, if the username is kamino_mobile and password  is t3st!ng, the base64 encoding of kamino_mobile:test!ng is a2FtaW5vX21vYmlsZTp0M3N0IW5n
        headers = {'Content-Type': 'application/json','Authorization': 'Basic a2FtaW5vX21vYmlsZTp0M3N0IW5n'}
        try:
            result = requests.post(url, data=json.dumps(data), headers=headers)
            r = result.json()
        except Exception as e:
            if result.status_code == 503:
                raise Exception('Create user fail: {0} Service status code {1}, error message: {2}'.format(self.environment.get_env_name(),result.status_code,e.message))
            else:
                raise Exception('Create user error with error message: {0}'.format(e.message))
        else:
            if result.status_code != 200:
                error = r.get('error')
                if 'User already exsist with this email' in error.get('message'):
                    logging.info('User already created')
                    pass
                else:
                    raise Exception('Create user error with error message: {}'.format(e.message))
        return r

    def get_access_token(self):
        #data = {'client_id': 'testclient', 'username': self.username, 'password': self.password, 'client_secret': 't3st!ng', 'grant_type': 'password'}        
        data = {'client_id': 'kamino_mobile', 'username': self.username, 'password': self.password, 'client_secret': 't3st!ng', 'grant_type': 'password'}
        # url :'http://qa1-auth.remotewd1.com/authservice/oauth/token?'
        url = self.environment.get_auth_service() + '/authservice/oauth/token?'
        result = self.post(url, data)
        access_token = result['access_token']
        token_type = result['token_type']
        #expires_in = result['expires_in']
        refresh_token = result['refresh_token']
        scope = result['scope']
        return access_token, token_type, refresh_token, scope

    def get_user_id(self):
        access_token, token_type, refresh_token, scope = self.get_access_token()
        #url: "http://qa1-auth.remotewd1.com/authservice/oauth/tokeninfo?token={0}".format(access_token)
        url = self.environment.get_auth_service() + '/authservice/oauth/tokeninfo?token={0}'.format(access_token)
        response = requests.get(url)
        userID = response.json()['data']['user_id']
        return str(userID)

    def get(self, url):
        access_token, token_type, refresh_token, scope = self.get_access_token()
        url = url
        headers = {'authorization': 'Bearer %s' % access_token}
        response = requests.get(url, headers=headers)
        return response

    def post(self, url, data):
        retry = 2
        while(retry > 0):
            try:
                response = requests.post(url, params=data)
                result = response.json()
            except Exception as e:
                retry = retry - 1
                continue
            else:
                break

        return result

    def delete(self, url):
        access_token, token_type, refresh_token, scope = self.get_access_token()
        url = url
        headers = {'Authorization': 'Bearer %s' % access_token}
        response = requests.delete(url, headers=headers)
        return response
    
    def getLocalCodeAndSecurityCode(self):
        access_token, token_type, refresh_token, scope = self.get_access_token()
        url = "http://{0}/sdk/v1/device".format(self.uut_ip)
        headers = {"content-type": "application/json", "authorization": "Bearer {0}".format(access_token)}
        try:
            res = requests.get(url, headers=headers)
            r = res.json()
        except Exception as e:
            raise Exception('getLocalAttachAndSecurityCode with error message: {0} and status code: {1}'.format(e.message, res.status_code))
        else:
            if res.status_code != 200:
                raise Exception('getLocalAttachAndSecurityCode with error: {0} and status code: {1}'.format(r, res.status_code))
            else:
                print "response:::: {}".format(r)
                deviceId = str(r.get('id'))
                securityCode = str(r.get('securityCode'))
                localCode = str(r.get('localCode'))

        return deviceId, securityCode, localCode
          
          
    def attach_user_to_device(self):
        ## Attach user with SecurityCode
        access_token, token_type, refresh_token, scope = self.get_access_token()
        userID = self.get_user_id()
        deviceId, securityCode, localCode = self.getLocalCodeAndSecurityCode()
        url = self.environment.get_device_service() + '/device/v1/user/{0}/device'.format(userID)
        data = {"securityCode": securityCode}
        headers = {"content-type": "application/json", "authorization": "Bearer {0}".format(access_token)}
        
        try:
            res = requests.post(url, data=json.dumps(data), headers=headers)
            r = res.json()
        except Exception as e:
            raise Exception('attach_user_to_device with error: {0} and status code: {1}'.format(e.message, res.status_code))
        else:
            if res.status_code == 409 and "User already attached to device" in str(r.get('error').get('message')):
                print 'User already attached to device'
            elif res.status_code == 200 and str(r.get('data').get('status')) == "APPROVED":
                print 'User is attached to device succesfully\n'
            else:
                raise Exception('attach_user_to_device with error: {0} and status code: {1}'.format(r, res.status_code))
