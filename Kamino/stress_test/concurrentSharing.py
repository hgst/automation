__author__ = 'Nick Yang <nick.yang@wdc.com>'
__author__ = 'Erica Lee <erica.lee@wdc.com>'

from ToolAPI import Tool
from junit_xml import TestCase, TestSuite
from datetime import datetime
from glob import glob
import sshAPI as ctl

import argparse
import commands
import json
import os
import requests
import sys
import time
import threading

class Test():
    def __init__(self):        

        example1 = '\n  python.exe stressPerformance.py -uut_ip 192.168.1.2:21062 -port 22 -t 10'
        
        # Create usages
        parser = argparse.ArgumentParser(description='*** Stress Test for sharing on Kamino ***\n\nExamples:{0}'.format(example1),formatter_class=argparse.RawTextHelpFormatter)
        parser.add_argument('-uut_ip', help='Destination NAS IP address, ex. 192.168.1.2:21062')        
        parser.add_argument('-port', help='Destination IP port, ex. 22')        
        parser.add_argument('-t', help='Total testing time (Minutes)', metavar='timeout')           
        args = parser.parse_args()

        if not args.uut_ip:
            print "DEVICE IP: ", os.environ['DEVICE_IP']
            self.uut_ip = os.environ['DEVICE_IP'] 
            print "SSH PORT: ", os.environ['SSH_PORT']
            self.port = int(os.environ['SSH_PORT']) 
        else:
            self.uut_ip = args.uut_ip
            self.port = int(args.port)
        
        self.ip = self.uut_ip.split(':')[0]
        self.dev_id = self.get_devid(uut_ip=self.ip, port=self.port)

        self.env = os.environ.get('CLOUD_SERVER')        
        DEVICE_FW = os.environ.get('FW_VER')
        if not self.env:
            self.env = 'qa1'
        
        self.timeout = args.t        
        self.start_time = datetime.now().replace(microsecond=0)

        self.test_cases = None        
        self.properties = {'Device IP': self.uut_ip, 'CLOUD_ENV': self.env, 'DEVICE_FW': DEVICE_FW, 'Time Out (In minute)': self.timeout}
        
        # Found counting error numbers and log error message
        #self.fail_count = 0
        self.err_message = []

        if not os.path.exists(os.getcwd()+'/output'):
            os.mkdir(os.getcwd()+'/output')
        self.result_file = os.path.join(os.getcwd()+'/output', 'output.xml')
        
    def run(self): 
        tname = 'Stress Performance Sharing Test (Concurrent access by 5 users on public/private shares)'
        init = time.time()

        try:
            print "Step 1: Create 5 users on server and get access tokens for each user, finally attach them to device\n"
            tokens_list = []
            usersID_list = []   
            worker = []

            for i in range(5):
                user_name = 'nick{0}@abc.com'.format(i)
                self.create_user(username=user_name)
                
                token = self.get_access_token(username=user_name)                
                tokens_list.append(token)
                print 'Access token for nick{0} is : {1}'.format(i,tokens_list[i])
                token_time = time.time()
                
                userID = self.get_user_id(access_token=tokens_list[i])
                usersID_list.append(userID)
                print 'User id of nick{0} is {1}'.format(i,usersID_list[i])            
                
                self.attach_user_to_device(access_token=tokens_list[i])  

            print 'Step 2: User upload a file onto NAS by user0'
            self.generate_upload_list()
            upload_result = self.upload_command(access_token=tokens_list[0])
            if 'alreadyExisting' in upload_result:
                print 'Warning: The file is already exists in the NAS folder\n'
            elif 'HTTP/1.1 201 Created' in upload_result:
                print 'The file is succesfully uploaded onto the NAS folder\n'      

            print 'Step 3: Create Readfile permission for everyone'
            fileID = self.get_file_id(access_token=tokens_list[0])
            print 'File id of the uploaded file is {0}'.format(fileID)
            self.set_permission(access_token=tokens_list[0],fileID=fileID)
            
            print 'Step 4a: User0 creates a new share record for other 4 users (Create a private share)'
            shareID = self.create_share_api(accessToken=tokens_list[0],ownerID=usersID_list[0],userID=usersID_list,fileID=fileID)
            print 'The private share ID is {0}\n'.format(shareID)

            print 'Step 4b: User1 creates a new share record for anybody (Create a public share)'
            shareID2 = self.create_share_api(accessToken=tokens_list[0],ownerID=usersID_list[0],userID='anybody',fileID=fileID)
            print 'The public share ID is {0}\n'.format(shareID2)

            print '=== Step 5: Keep getting public/private share API in {0} minutes ===\n'.format(self.timeout)
            start = time.time()
            test_count = 0
            if self.timeout:
                timeout = int(self.timeout) * 60
                while ((time.time() - start) < timeout):
                    # Refresh a new token for every users for every 20 minutes
                    if (time.time() - token_time > 1200):
                        tokens_list = []
                        for i in range(5):
                            user_name = 'nick{0}@abc.com'.format(i)
                            token = self.get_access_token(username=user_name)
                            tokens_list.append(token)
                            token_time = time.time()
                    
                    for i in range(0,5):
                        print '*** Getting the share API from the created share by user1 (Get private share API testing) ***'
                        t = threading.Thread(name='GetPrivateShare%s' % i, target=self.get_share_api,args=(tokens_list[i],shareID)) 
                        #self.get_share_api(accessToken=tokens_list[i],shareID=shareID)                    
                        print '*** Getting the share API from the created share by user1 (Get public share API testing) ***'
                        w = threading.Thread(name='GetPublicShare%s' % i, target=self.get_share_api,args=(tokens_list[i],shareID2))

                        worker.append(w)    
                        worker.append(t)    
                        w.start()    
                        t.start()
                        #time.sleep(1)
                    
                    # Let the thread jobs all done
                    for wp in worker:
                        wp.join() 

                    test_count = test_count + 1
            
            metric = [['Number.of.fails',len(self.err_message),'times'],['Number.of.access',test_count*10,'times']]

        except Exception as e:
            print "Stress Performance sharing test fails, error message is %s" % e.message            
            self.test_cases = TestCase(name=tname, classname='Stress test on sharing', elapsed_sec=time.time()-init)
            self.test_cases.add_failure_info(e.message)
        
        else:
            self.test_cases = TestCase(name=tname, classname='Stress test on sharing', elapsed_sec=time.time()-init,metrics=metric)
            if len(self.err_message) > 0:
                for i in self.err_message:
                    print i
                    self.test_cases.add_failure_info(i)
        finally:
            # Clean existed report file
            report_file = glob(self.result_file)
            if report_file:
                for f in report_file:
                    os.remove(f)
            
            # Generate report file
            with open(self.result_file, 'a') as f:
                TestSuite.to_file(f, [TestSuite(name='Stress Test on sharing', package='Concurrent access on public/private shares', 
                                                test_cases=[self.test_cases], timestamp=self.start_time.strftime('%Y%m%d%H%M%S'),
                                                properties=self.properties)], prettyprint=True)
                f.close()
                print "Report file {} is generated\n".format(self.result_file)

    def create_share_api(self,accessToken=None,ownerID=None,userID=None,fileID=None):

        headers = {'Content-Type': 'application/json'}
        url = 'https://qa1.remotewd1.com/v1/shares'
        if type(userID) == str:
            data = {"accessToken": accessToken,"ownerId":ownerID,"deviceId":self.dev_id,"fileIds": [fileID],"userIds":[userID]}
        else:
            data = {"accessToken": accessToken,"ownerId":ownerID,"deviceId":self.dev_id,"fileIds": [fileID],"userIds":userID}

        result = requests.post(url, data=json.dumps(data), headers=headers)
        r = result.json()

        if result.status_code == 201:
            print "Share API is succesfully created\n"
            shareID = r['data']['shareId']
            return str(shareID)
        else:
            print "Create share API fail, FAIL code : {0}\n".format(result.status_code)


    def get_share_api(self,accessToken=None,shareID=None):
        try:
            url = "https://qa1.remotewd1.com/v1/shares/{0}?accessToken={1}".format(shareID,accessToken)
            response = requests.get(url)

            if response.status_code == 200:
                print "Get share API success, user can succesfully access the share with file\n"
            else:
                err_message = "Get share API fail, FAIL code : {0}, Return message: {1}\n".format(response.status_code,response.content)
                raise Exception(err_message)
        except:
            exc_type, exc_value = sys.exc_info()[:2]               
            message = 'Handling %s exception with message "%s" in thread %s' % (exc_type.__name__, exc_value, threading.current_thread().name)   
            if exc_type.__name__ == 'ConnectionError':
                print 'Get share API responded incorrectly, error message:{}'.format(exc_value)
            else:
                print 'Get share API responded incorrectly, error message:{}'.format(exc_value)
                self.err_message.append(message)

    def get_file_content(self,accessToken=None,fileID=None):
        '''
        Get file binary content of a single file
        '''

        # Get file content by file_id
        url = "http://{0}/sdk/v2/files/{1}/content".format(self.uut_ip, fileID)
        headers = {'Authorization': 'Bearer {0}'.format(accessToken)}
        response = requests.get(url,headers=headers)
        if response.status_code == 200:
            print 'The user can access the file succesfully\n'
        else:
            print 'Fail to access the file, FAIL code : {0}\n'.format(response.status_code)        

    def upload_command(self,access_token=None):
        command = 'curl -v -X POST -H "Authorization: Bearer %s" ' % access_token + \
                  '-H "Content-Type: multipart/related;boundary=foo" ' + \
                  '--data-binary @uploadFile "http://%s/sdk/v2/files"' % self.uut_ip
        status, result = commands.getstatusoutput(command)
        #self.delete_upload_list()
        return result

    def generate_upload_list(self):
        fo = open(os.path.join(os.getcwd(),'uploadFile'), 'w')
        fo.write('--foo \n')
        fo.write('\n')
        command = '{"parentID":"root","name":"hello.txt"}\n'

        fo.write(command)
        fo.write('\n')
        fo.write('--foo \n')
        fo.write('\n')        

        #fo.write(open(os.path.abspath(source_path), 'rb').read())
        fo.write('Test Stress sharing API')
        fo.write('\n')
        fo.write('--foo--\n')
        fo.close()

    def set_permission(self,access_token=None,fileID=None):
        data = {"fileID":fileID,"userID":"anybody","value":"ReadFile"}
        headers = {'Content-Type': 'application/json','Authorization': 'Bearer {0}'.format(access_token)}
        url = 'http://%s/sdk/v1/filePerms' % self.uut_ip
        result = requests.post(url, data=json.dumps(data), headers=headers)
        #r = result.json()
        if result.status_code == 201 or result.status_code == 409:
            print 'Uploaded file read permission is already set to everyone\n'
        else:
            print 'Fail to set file read permission to everyone, error message:{0}\n'.format(result)
    
    def get_file_id(self,access_token=None):
        url = 'http://%s/sdk/v2/filesSearch/parents' % self.uut_ip
        headers = {'Content-Type': 'application/json','Authorization': 'Bearer {0}'.format(access_token)}
        response = requests.get(url,headers=headers)
        result = response.json()

        fileID = ''
        for tempDic in result['files']:
            for k,v in tempDic.items():
                if k == 'name' and v=='hello.txt':
                    fileID = tempDic['id']

        return str(fileID)

    def get_user_id(self,access_token=None):
        url =  "http://qa1-auth.remotewd1.com/authservice/oauth/tokeninfo?token={0}".format(access_token)
        response = requests.get(url)
        userID =  response.json()['data']['user_id']
        return str(userID)

    def create_user(self,username=None):
        data = {"email": username, "password":"Test1234"}
        url = 'http://qa1-auth.remotewd1.com/authservice/v1/user?'
        
        # BasicAuth is a header that takes in the  Base64 encoding of the 'username:password' combination. 
        # For example, if the username is kamino_mobile and password  is t3st!ng, the base64 encoding of kamino_mobile:test!ng is a2FtaW5vX21vYmlsZTp0M3N0IW5n
        headers = {'Content-Type': 'application/json','Authorization': 'Basic a2FtaW5vX21vYmlsZTp0M3N0IW5n'}
        result = requests.post(url, data=json.dumps(data), headers=headers)
        r = result.json()

        if result.status_code != 200:
            error = r['error']
            if 'User already exsist with this email' in error['message']:
                print '{0} is already created'.format(username)
                pass
            else:
                raise Exception('Create User Error with: {}'.format(r))
        else:        
            print "{0} is already created succesfully\n".format(username)
        
        return r

    def get_access_token(self,username=None):
        data = {'client_id': 'kamino_mobile', 'username': username, 'password': "Test1234", 'client_secret': 't3st!ng', 'grant_type': 'password'}
        url = 'http://qa1-auth.remotewd1.com/authservice/oauth/token?'
        result = requests.post(url, data=data)
        r =  result.json()
        
        if result.status_code == 200:
            print "Access token is succesfully created"
            access_token = r['access_token']
            return str(access_token)
        else:
            print "Get access token fail, FAIL code : {0}\n".format(result.status_code)

    def attach_user_to_device(self,access_token):
        url = "http://{0}/sdk/v1/users/useMyToken".format(self.uut_ip)
        headers = {'Content-Type': 'application/json','Authorization': 'Bearer {0}'.format(access_token)}
        response = requests.put(url,headers=headers)
        
        if response.status_code == 204:
            print 'User is attached to device succesfully\n'
        else:
            print 'Fail to attach user to device, error code = {0}\n'.format(response.status_code)
    
    def get_devid(self,uut_ip=None,port=None):
        ssh_info = [uut_ip, 'root', "", port]
        ssh = ctl.SSHClient(*ssh_info)
        ssh.connect()

        dev_result = ssh.execute('cat /Data/devmgr/db/wddevice.ini')
        dev_id = dev_result[1].splitlines()[1].split('deviceid = ')[1]
        ssh.close()
        return dev_id

test = Test()
test.run()
