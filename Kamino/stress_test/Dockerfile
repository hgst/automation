From ubuntu:14.04.3
MAINTAINER Erica Lee "erica.lee@wdc.com"

USER root

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        curl           \
        git            \
        inetutils-ping \
        net-tools      \
        tar            \
        vim            \
        wget           \
        ssh            \
        unzip          \
        make           \
        gcc            \
        g++            \
        zip            \
        libffi-dev     \
       # dependencies for python
        openssl        \
        build-essential \
        libssl-dev \
        zlib1g-dev \
        libbz2-dev \
        libreadline-dev \
        libsqlite3-dev \
        llvm \
        libncurses5-dev \
        --no-install-recommends

#================#
# Install Python #
#================#

ENV PYTHON_VER=2.7.9

# download & install

RUN wget https://www.python.org/ftp/python/${PYTHON_VER}/Python-${PYTHON_VER}.tar.xz --no-check-certificate && \
    xz -d Python-${PYTHON_VER}.tar.xz && \
    tar -xvf Python-${PYTHON_VER}.tar && \
    cd Python-${PYTHON_VER} && \
    ./configure --prefix=/usr/local && \
    make && \
    make install

RUN wget --no-check-certificate https://pypi.python.org/packages/source/s/setuptools/setuptools-16.0.tar.gz && \
    tar -xvf setuptools-16.0.tar.gz && \
    cd setuptools-16.0 && \
    python2.7 setup.py install

RUN wget https://bootstrap.pypa.io/get-pip.py --no-check-certificate && \
    python2.7 get-pip.py

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
        python-imaging \
        libfreetype6-dev \
        libjpeg8-dev

#========================#
# Install Python package #
#========================#
COPY requirement.txt .
RUN pip install --upgrade pip
RUN pip install -r requirement.txt

WORKDIR /root

ENV PYTHONPATH=/root/stress_test

COPY run.sh /root/

