__author__ = 'Nick Yang <nick.yang@wdc.com>'

from junit_xml import TestCase, TestSuite
import sshAPI as ctl
import argparse
import os
import time
from glob import glob

class Test():
    def __init__(self):        

        example1 = '\n  python stressapp.py -uut_ip 192.168.1.1 -port 22 "-s 20 -M 256 -m 8 -W"'
        
        # Create usages
        parser = argparse.ArgumentParser(description='*** stressapptest testing on Kamino ***\n\nExamples:{0}'.format(example1),formatter_class=argparse.RawTextHelpFormatter)
        parser.add_argument('-uut_ip', help='Destination NAS IP address, ex. 192.168.1.45')        
        parser.add_argument('-port', help='Destination ssh port, ex. 22')
        parser.add_argument('command', help='stressapptest commands with parameters and values, ex. "-s 20 -M 256 -W"', metavar='command')           
        args = parser.parse_args()

        #If user didn't enter ip,port, use os.environ['DEVICE_IP']
        if not args.uut_ip:
            print "DEVICE IP: ",os.environ['DEVICE_IP']
            self.uut_ip = os.environ['DEVICE_IP'] 
            print "SSH PORT: ",os.environ['SSH_PORT']
            self.port = int(os.environ['SSH_PORT']) 
        else:
            self.uut_ip = args.uut_ip
            self.port = int(args.port)
        
        self.uut_ip = self.uut_ip.split(':')[0]
        print "UUT_IP: ",self.uut_ip
        
        env = os.environ.get('CLOUD_SERVER')
        DEVICE_FW = os.environ.get('FW_VER')
        if not env:
            env = 'qa1'
        
        self.command = 'stressapptest ' + args.command
        
        self.properties = {'Device IP': self.uut_ip, 'Device ssh port': self.port, 'CLOUD_ENV': env, 'DEVICE_FW': DEVICE_FW,'stressapptest command': self.command}

        if '--listen' in self.command:
            self.testcaseName = 'stressapptest (Memory, CPU, network)'
        elif '-f' in self.command and '--listen' not in self.command:
            self.testcaseName = 'stressapptest (Memory, CPU, HDD)'
        else:
            self.testcaseName = 'stressapptest (Memory, CPU)'

    def run(self):
        try:
            start = time.time()
            stress_result = self.get_result()
            metric = self.get_metrics(stress_result)

        except Exception as e:
            print "FAIL: stressapptest test, error message: %s" % e.message
            test_cases = TestCase(name=self.testcaseName, classname='stressapptest', elapsed_sec=time.time()-start)        
            test_cases.add_failure_info(e.message)

        else:
            test_cases = TestCase(name=self.testcaseName, classname='stressapptest', elapsed_sec=time.time()-start,stdout=stress_result,metrics=metric)        

        finally:
            print "*** stressapptest completed ***\n"
            self.output_report(test_cases=test_cases)

    def get_result(self):

        try:
            ssh_info = [self.uut_ip, 'root', "", self.port]

            ssh = ctl.SSHClient(*ssh_info)
            ssh.connect()
            
            print "stressapptest command : {0}\n".format(self.command)
            status,stress_result = ssh.execute(self.command)
            ssh.close()

        except Exception as e:
            raise Exception("SSH connection error")

        if not status:
            print stress_result
            if "Status: PASS" in stress_result:     
                print "*** PASS: stressapptest test ***"
                return stress_result
            else: 
                raise Exception(stress_result)
        else:
            raise Exception("Fail executing the stressapptest command, FAIL result : {0}\n".format(stress_result))

    def get_metrics(self,result=None):
        try:
            test = result.splitlines()
            test.pop()
            test.pop()
            test.pop()
            temp_list =  test[len(test)-6:]           
            
            # Memory Copy , File Copy, Net Copy, Data Check, Invert Data and Disk totally 6 metrics          
            metric_list = []
            usage_list = []
            for ele in temp_list:
                metric =  ele.split('at ')[1].split('MB/s')[0]
                metric_list.append(metric) 

            for ele in temp_list[0:3]:
                usage =  ele.split('M at')[0].split('Copy: ')[1]
                usage_list.append(usage) 

            if self.testcaseName == 'stressapptest (Memory, CPU, network)':
                stressapp_metric = [['Memory.Copy.Speed', metric_list[0], 'MB/s'],
                                    ['Memory.Copy.Usage', usage_list[0], 'MB'],
                                    ['File.Copy.Speed', metric_list[1], 'MB/s'],
                                    ['File.Copy.Usage', usage_list[1], 'MB'],
                                    ['Net.Copy.Speed', metric_list[2], 'MB/s'],
                                    ['Net.Copy.Usage', usage_list[2], 'MB']
                                    ]
            elif self.testcaseName == 'stressapptest (Memory, CPU, HDD)': 
                stressapp_metric = [['Memory.Copy.Speed', metric_list[0], 'MB/s'],
                                    ['Memory.Copy.Usage', usage_list[0], 'MB'],
                                    ['File.Copy.Speed', metric_list[1], 'MB/s'],
                                    ['File.Copy.Usage', usage_list[1], 'MB']
                                    ]
            else:           
                stressapp_metric = [['Memory.Copy.Speed', metric_list[0], 'MB/s'],
                                    ['Memory.Copy.Usage', usage_list[0], 'MB']
                                    ]            
            
            return stressapp_metric

        except Exception as e:
            print "Error occurs when handling metrics output, error message is %s" % e.message

    def output_report(self,start=None,test_cases=None):
                
        report_path = os.getcwd()+'/output'
        if not os.path.exists(report_path):
            os.mkdir(report_path)
        result_file = os.path.join(report_path, 'output.xml')  

        # Clean existed report file
        report_file = glob(result_file)
        if report_file:
            for f in report_file:
                os.remove(f)

        # Generate report file
        with open(result_file, 'a') as f:
            TestSuite.to_file(f, [TestSuite(name='stressapptest', package='stressapptest', 
                                            test_cases=[test_cases],properties=self.properties)], prettyprint=True)
            f.close()
        
        print "Generated {} report file".format(result_file)

test = Test()
test.run()