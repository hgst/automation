""" @brief A collection of utilities used by all cases
"""
import paramiko
import scp
import os
import sys
import time


class SSHClient():
    '''
        @author: Nick Yang
        @brief: Create a SCP transfer mechanism
        @Usage:
                from scp import SCPClient
                ssh = SSHClient()
                ssh.load_system_host_keys()
                ssh.connect('example.com')

                # SCPCLient takes a paramiko transport as its only argument
                scp = SCPClient(ssh.get_transport())

                scp.put('test.txt', 'test2.txt')
                scp.get('test2.txt')
    '''

    def __init__(self, hostname, username, password, port):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.port = port
        self.connection = None
        self.sftp = None
        self.scp = None

    def connect(self):
        self.connection = paramiko.SSHClient()
        self.connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.connection.connect(self.hostname, username=self.username, password=self.password, port=self.port)

    def execute(self, command):
        status = 0
        output = ''
        stdin, stdout, stderr = self.connection.exec_command(command)
        stdin.close()
        error = str(stderr.read())
        if error:
            status = stdout.channel.recv_exit_status()
            output = "Execute command failed, error message:\n{}".format(error)
        else:
            output = str(stdout.read())
        return status, output

    def close(self):
        self.connection.close()

    # ======================================================================================#
    #                                         SCP                                           #
    # ======================================================================================#
    '''
        You need to connect ssh first, then run sftp_connection to create a new SCPClient session object.
        After that, upload/download/ command can be executed.
    '''

    def scp_connection(self):
        self.scp = scp.SCPClient(self.connection.get_transport())

    def scp_upload(self, localpath, remotepath,recursive=False):
        if recursive:
            self.scp.put(localpath, remotepath,recursive=True)
        else:
            self.scp.put(localpath, remotepath, recursive=False)

    def scp_download(self, remotepath, localpath):
        self.scp.get(remotepath, localpath)
        
