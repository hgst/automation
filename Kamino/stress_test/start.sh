#!/bin/bash -xe

# Container to run stress test

# Use '-i' to run in interactive mode
RUN_CMD=/root/run.sh
RUN_OPTS=
if [ "$1" = "-i" ]; then
    RUN_CMD=/bin/bash
    RUN_OPTS=-it
fi

if [ "$XPATH" = "" ]; then
    XPATH=$(pwd)
else
    echo "Adjusting path for the underlying host filesystem"
    XPATH="${XPATH}"/$(pwd | sed 's/.*workspace\///')
fi

docker build --tag stress-tool .

docker run \
    ${RUN_OPTS} \
    --net=host \
    -v "${XPATH}":/root/stress_test \
    -v "${XPATH}/output":/root/stress_test/output \
    -e DEVICE_IP=$UUT_IP \
    -e SSH_PORT=$SSH_PORT \
    -e CLOUD_SERVER=$CLOUD_SERVER \
    -e FW_VER=$FW_VER \
    stress-tool \
    ${RUN_CMD} "$@"

