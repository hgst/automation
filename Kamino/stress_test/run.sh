#!/bin/bash -e

Download_From="/Data/restsdk/userRoots/0000000052385efd015243e74eee0083/"
Download_Destination="/root/stress_test/output/"
Upload_Destination="$Download_From"
Upload_From="/root/stress_test/"

generate_files(){
    a=0
    ARRAY=()
    DF=()
    file_number=$1
    type=`ls ./${DATASET}/ | grep "${file}" | sed 's/.*\././g'`
    uf="${Upload_From}data_${file_number}"
    mkdir ${uf}
    while [ $a -lt $file_number ]
    do
       files=${uf}/${file_size}_$a${type}
       cp ./${DATASET}/"${file}" ${files}
       ARRAY+=(${files})
       DF+=("${Download_From}data_${file_number}/${file_size}_$a${type}")
       a=`expr $a + 1`
    done
}


cd /root/stress_test

TC_NAME=$1

if [ "$TC_NAME" == "" ]; then
    echo "Require Test case name without .py"
    exit 1
fi

if [ "$TC_NAME" == "stressPerformance" ]; then
    shift
    TESTING_MODE=$1
    shift
    if [ "$TESTING_MODE" == "DEBUG" ]; then
        python stressPerformance.py "$@"
    else
        if [ "$1" == "sequential" ]; then
            shift
            parameter="-t 180"
            python stressPerformance.py ${parameter}

        elif [ "$1" == "concurrent" ]; then
            shift
            parameter="-t 180"
            python concurrentSharing.py ${parameter}
        fi
    fi

elif [ "$TC_NAME" == "stressFolderCreation" ]; then
    shift
    TESTING_MODE=$1
    
    if [ "$TESTING_MODE" == "DEBUG" ]; then
        python stressFolderCreation.py "$@"
    else
        python stressFolderCreation.py         
    fi

elif [ "$TC_NAME" == "stressapp" ]; then
    shift
    TESTING_MODE=$1
    shift
    if [ "$TESTING_MODE" == "DEBUG" ]; then
        parameter="$@"
    else
        if [ "$1" == "MemoryCPU" ]; then
            shift
            parameter="-s 3600 -M 512 -m 4 -C 4 -W"

        elif [ "$1" == "MemoryCPUHD" ]; then
            shift
            parameter="-f /media/file1 -f /media/file2 -s 3600 -M 512 -m 4 -C 4 -W"
        elif [ "$1" == "MemoryCPUNetwork" ]; then
            shift
            parameter="-f /media/file1 -f /media/file2 -s 3600 -M 512 -m 4 -C 4  -n 127.0.0.1 --listen -W"
        fi
    fi
    python stressapp.py "${parameter}"
#stressULDLtest AUTO scenario user_number file_number file_size file_name DATASET DURATION
elif [ "$TC_NAME" == "stressULDLtest" ]; then
    shift
    TESTING_MODE=$1
    echo "$@"
    shift
    scenario=$1
    echo "scenario: $scenario - $TESTING_MODE"
    user_number=$2
    echo "user_number: $user_number"
    file_number=$3
    echo "file_number: $file_number"
    file_size=$4
    echo "file_size: $file_size"
    file="${5}"
    echo "file_name: ${file}"
    DATASET=$6
    DURATION=$7
    ITERATION=$8
    echo "DURATION:${DURATION}"
    echo "ITERATION:${ITERATION}"
    generate_files $file_number

    if [ "$scenario" == "HTTP.TP.UL.Perf" ]; then
        condition="Upload $file_number file-$file in size $file_size in $DATASET"
        metrics_name="HTTP.TP.UL.Perf_${user_number}-user-${file_number}x${file_size}"
        parameter="-uf ${ARRAY[@]} -ud ${Upload_Destination}"

    elif [ "$scenario" == "HTTP.TP.DL.Perf" ]; then
        condition="Download $file_number file-$file in size $file_size in $DATASET"
        metrics_name="HTTP.TP.DL.Perf_${user_number}-user-${file_number}x${file_size}"
        parameter="-uf ${ARRAY[@]} -df ${DF[@]} -dd ${Download_Destination}"

    elif [ "$scenario" == "HTTP.TP.Bi-Dir.Perf" ]; then
        condition="Concurrent Upload&Download $file_number file-$file in size $file_size in $DATASET"
        metrics_name="HTTP.TP.Bi-Dir.Perf_${user_number}-user-${file_number}x${file_size}"
        parameter="-uf ${ARRAY[@]} -ud ${Upload_Destination} -df ${DF[@]} -dd ${Download_Destination}"

    elif [ "$scenario" == "HTTP.Con.Bi-Dir.Stress" ]; then
        DAY=0
        if [ ! "$DURATION" == "N/A" ]; then
            DAY=$((${DURATION} / 1440))
        fi
        echo $DAY
        condition="Stress test (upload & download)-${user_number}-user-concurrent-${file_number}-files-${file_size}"
        metrics_name="HTTP.Con.Bi-Dir.Stress_${DAY}DAY-${user_number}-user-${file_number}x${file_size}"
        parameter="-uf ${ARRAY[@]} -ud ${Upload_Destination} -df ${DF[@]} -dd ${Download_Destination}"

    elif [ "$scenario" == "HTTP.Seq.Bi-Dir.Stress" ]; then
        DAY=0
        if [ ! "$DURATION" == "N/A" ]; then
            DAY=$((${DURATION} / 1440))
        fi
        echo $DAY
        condition="Stress test (upload & download) with concurrent-${user_number}-users & concurrent upload & download a folder with ${file_number} files for each user"
        metrics_name="HTTP.Seq.Bi-Dir.Stress_${DAY}DAY-${user_number}-user-${file_number}x${file_size}"
        parameter="-uf ${uf}/ -ud ${Upload_Destination}data_${file_number}/ -df ${Download_From}data_test/ -dd ${Download_Destination}"
    fi

    if [ ! "$DURATION" == "N/A" ]; then
        parameter="$parameter -t ${DURATION}"
    else
        parameter="$parameter -times ${ITERATION}"
    fi

    if [ "$TESTING_MODE" == "AUTO" ]; then
        echo $condition
        parameter="$parameter -m_n ${metrics_name}"
    fi
    
    if [ $user_number -gt 1 ]; then
        echo "user number: $user_number"
        parameter="$parameter -u_n ${user_number}"
    fi

    echo "$parameter"
    python stressULDLtest.py ${parameter}
fi
