"""
title           :Stress Folder creation
description     :User stressful creates 2800 folders by Rest API commands, upload datas to each of them, and check if data is uploaded succesfully, finally remove all of the folders created (include uploaded files)
author          :Nick yang <nick.yang@wdc.com>
date            :2016/07/03
notes           :
"""

from ToolAPI import Tool
from junit_xml import TestCase, TestSuite
from datetime import datetime
from glob import glob
import sshAPI as ctl

import argparse
import commands
import json
import os
import requests
import sys
import time
import threading

class Test():
    def __init__(self):        

        example1 = '\n  python.exe stressFolderCreation.py -uut_ip 192.168.1.2:21062 -port 22'
        
        # Create usages
        parser = argparse.ArgumentParser(description='*** Stress Test for folder creation ***\n\nExamples:{0}'.format(example1),formatter_class=argparse.RawTextHelpFormatter)
        parser.add_argument('-uut_ip', help='Destination NAS IP address, ex. 192.168.1.2')        
        parser.add_argument('-port', help='Destination IP port, ex. 22')        
        args = parser.parse_args()

        if not args.uut_ip:
            print "DEVICE IP: ", os.environ['DEVICE_IP']
            self.uut_ip = os.environ['DEVICE_IP'] 
            print "SSH PORT: ", os.environ['SSH_PORT']
            self.port = int(os.environ['SSH_PORT']) 
        else:
            self.uut_ip = args.uut_ip
            self.port = int(args.port)
        
        self.env = os.environ.get('CLOUD_SERVER')        
        DEVICE_FW = os.environ.get('FW_VER')
        if not self.env:
            self.env = 'qa1'
        
        self.start_time = datetime.now().replace(microsecond=0)

        self.test_cases = None        
        self.properties = {'Device IP': self.uut_ip, 'CLOUD_ENV': self.env, 'DEVICE_FW': DEVICE_FW, 'Total folders created': 2800}
        
        # Log error message
        self.err_message = []

        if not os.path.exists(os.getcwd()+'/output'):
            os.mkdir(os.getcwd()+'/output')
        self.result_file = os.path.join(os.getcwd()+'/output', 'output.xml')
        
    def run(self): 
        tname = 'Stress Folder creation test (2800 folders)'
        init = time.time()

        try:
            print "Step 1: Create a user1 on server"      
            self.create_user(username='nick@abc.com')

            print 'Step 2: Get access token for the user1'
            token = self.get_access_token(username='nick@abc.com')
            token_time = time.time()
            print 'Access token for this user is : {0}\n'.format(token)

            print 'Step 3: Attach user1 to the device'
            self.attach_user_to_device(access_token=token)

            print 'Step 4: Delete everything under user folder before test begins'
            self.delete_user_folder(token=token)

            print '=== Step 5: User1 creates 2800 folders on NAS directory ===\n'
            test_count = 0

            for i in range(2800):
                # Refresh a new token every 20 minutes
                if (time.time() - token_time > 1200):
                    token = self.get_access_token(username='nick@abc.com')
                    token_time = time.time()
                
                foldername = 'test'+str(test_count)
                self.generate_upload_list(folderName=foldername)
                self.upload_folder_command(access_token=token,foldername=foldername)
                
                test_count = test_count + 1
            
            folder_IDlist = self.get_folder_id(access_token=token)

            print '=== Step 6: User1 upload files to each of the 2800 folders created ===\n'
            for name,folderid in enumerate(folder_IDlist):
                # Refresh a new token every 20 minutes
                if (time.time() - token_time > 1200):
                    token = self.get_access_token(username='nick@abc.com')
                    token_time = time.time()
                
                foldername = 'test'+str(name)
                self.generate_upload_file_list(folderID=folderid)
                self.upload_file_command(access_token=token,foldername=foldername)

            metric = [['Number.of.failed.folder.creation',len(self.err_message),'times'],['Number.of.creation',test_count,'times']]

        except Exception as e:
            print "Stress folder creation test fails, error message is %s" % e.message            
            self.test_cases = TestCase(name=tname, classname='Stress test on folder creation', elapsed_sec=time.time()-init)
            self.test_cases.add_failure_info(e.message)
        
        else:
            self.test_cases = TestCase(name=tname, classname='Stress test on folder creation', elapsed_sec=time.time()-init,metrics=metric)
            if len(self.err_message) > 0:
                for i in self.err_message:
                    print i
                    self.test_cases.add_failure_info(i)
        finally:
            # Remove all the created folder on NAS
            self.delete_user_folder(token=token)
            
            # Clean existed report file
            report_file = glob(self.result_file)
            if report_file:
                for f in report_file:
                    os.remove(f)
            
            # Generate report file
            with open(self.result_file, 'a') as f:
                TestSuite.to_file(f, [TestSuite(name='Stress folder creation', package='Stress test creates 2800 folders', 
                                                test_cases=[self.test_cases], timestamp=self.start_time.strftime('%Y%m%d%H%M%S'),
                                                properties=self.properties)], prettyprint=True)
                f.close()
                print "Report file {} is generated\n".format(self.result_file)

    def upload_folder_command(self,access_token=None,foldername=None):
        '''
        Rest API command for creating folders on NAS
        '''

        try:
            command = 'curl -v -X POST -H "Authorization: Bearer %s" ' % access_token + \
                      '-H "Content-Type: multipart/related;boundary=foo" ' + \
                      '--data-binary @uploadFolder "http://%s/sdk/v2/files"' % self.uut_ip
            
            status, result = commands.getstatusoutput(command)
            
            if 'alreadyExisting' in result:
                print 'Warning: The folder is already exists in the NAS folder\n'
            elif 'HTTP/1.1 201 Created' in result:
                print 'Folder {0} is succesfully created on NAS directory'.format(foldername)
            else:
                raise Exception('The folder might not create successfully : {0}'.format(result))           
        except:
            exc_type, exc_value = sys.exc_info()[:2] 
            message = 'Handling %s exception with message "%s" in thread %s' % (exc_type.__name__, exc_value, threading.current_thread().name)   
            print 'Fail to create the folder, error message:{}'.format(exc_value)
            self.err_message.append(message)

    def upload_file_command(self,access_token=None,foldername=None):        
        '''
        Rest API command for creating folders on NAS
        '''        
        try:
            command = 'curl -v -X POST -H "Authorization: Bearer %s" ' % access_token + \
                      '-H "Content-Type: multipart/related;boundary=foo" ' + \
                      '--data-binary @uploadFile "http://%s/sdk/v2/files"' % self.uut_ip

            status, result = commands.getstatusoutput(command)

            if 'alreadyExisting' in result:
                print 'Warning: The file is already exists in the NAS folder\n'
            elif 'HTTP/1.1 201 Created' in result:
                print 'The file is succesfully created onto NAS {0} directory'.format(foldername)
            else:
                raise Exception('The file might not create successfully : {0}'.format(result))           
        except:
            exc_type, exc_value = sys.exc_info()[:2] 
            message = 'Handling %s exception with message "%s" in thread %s' % (exc_type.__name__, exc_value, threading.current_thread().name)   
            print 'Fail to upload the file, error message:{}'.format(exc_value)
            self.err_message.append(message)

    def generate_upload_list(self,folderName=None):
        fo = open(os.path.join(os.getcwd(),'uploadFolder'), 'w')
        fo.write('--foo \n')
        fo.write('\n')
        command = '{"parentID":"root",'
        command += '"name":"{0}"'.format(folderName)
        command += ',"mimeType":"application/x.wd.dir"}\n'

        fo.write(command)
        fo.write('\n')
        fo.write('--foo--\n')
        fo.close()

    def generate_upload_file_list(self,folderID=None):
        fo = open(os.path.join(os.getcwd(),'uploadFile'), 'w')
        fo.write('--foo \n')
        fo.write('\n')
        command = '{"parentID":"%s"' % folderID
        command += ',"name":"hello.txt"}\n'

        fo.write(command)
        fo.write('\n')
        fo.write('--foo \n')
        fo.write('\n')        

        fo.write('Test stress folder creation')
        fo.write('\n')
        fo.write('--foo--\n')
        fo.close()    

    def get_folder_id(self,access_token=None):
        '''
        Gather all the 2800 folder IDs
        '''

        pageToken = ''            
        folder_list = []
        count = 0

        while(count < 3):
            
            url = 'http://%s/sdk/v2/filesSearch/parents?limit=1000&pageToken=%s' % (self.uut_ip,pageToken)

            headers = {'Content-Type': 'application/json','Authorization': 'Bearer {0}'.format(access_token)}
            response = requests.get(url,headers=headers)
            result = response.json()

            for tempDic in result['files']:
                for k,v in tempDic.items():
                    if k == 'name' and v.startswith('test'):
                        fileID = str(tempDic['id'])
                        folder_list.append(fileID)
            
            pageToken = str(result['pageToken'])
            count  = count + 1;
        
        return folder_list

    def get_user_id(self,access_token=None):
        url =  "http://qa1-auth.remotewd1.com/authservice/oauth/tokeninfo?token={0}".format(access_token)
        response = requests.get(url)
        userID =  response.json()['data']['user_id']
        return str(userID)

    def delete_user_folder(self,token=None):
        userID = self.get_user_id(access_token=token)
        deleted_path = '/Data/restsdk/userRoots/' + userID + '/*'
        ip = self.uut_ip.split(':')[0]

        ssh_info = [ip, 'root', "", self.port]
        ssh = ctl.SSHClient(*ssh_info)
        ssh.connect()
        print 'Removing contents in %s\n' % deleted_path
        delete_result = ssh.execute('rm -rf {0}'.format(deleted_path))
        ssh.close()

    def create_user(self,username=None):
        data = {"email": username, "password":"Test1234"}
        url = 'http://qa1-auth.remotewd1.com/authservice/v1/user?'
        
        # BasicAuth is a header that takes in the Base64 encoding of the 'username:password' combination. 
        # For example, if the username is kamino_mobile and password  is t3st!ng, the base64 encoding of kamino_mobile:test!ng is a2FtaW5vX21vYmlsZTp0M3N0IW5n
        headers = {'Content-Type': 'application/json','Authorization': 'Basic a2FtaW5vX21vYmlsZTp0M3N0IW5n'}
        result = requests.post(url, data=json.dumps(data), headers=headers)
        r = result.json()

        if result.status_code != 200:
            if 'User already exsist with this email' in r['error']['message']:
                print '{0} is already created\n'.format(username)
                pass
            else:
                raise Exception('Create User Error with: {}'.format(r))
        else:        
            print "{0} is already created succesfully\n".format(username)
        
        return r

    def get_access_token(self,username=None):
        data = {'client_id': 'kamino_mobile', 'username': username, 'password': "Test1234", 'client_secret': 't3st!ng', 'grant_type': 'password'}
        url = 'http://qa1-auth.remotewd1.com/authservice/oauth/token?'
        result = requests.post(url, data=data)
        r =  result.json()
        
        if result.status_code == 200:
            print "Access token is succesfully created"
            access_token = r['access_token']
            return str(access_token)
        else:
            print "Get access token fail, FAIL code : {0}\n".format(result.status_code)

    def attach_user_to_device(self,access_token):
        url = "http://{0}/sdk/v1/users/useMyToken".format(self.uut_ip)
        headers = {'Content-Type': 'application/json','Authorization': 'Bearer {0}'.format(access_token)}
        response = requests.put(url,headers=headers)
        
        if response.status_code == 204:
            print 'User is attached to device succesfully\n'        
        elif response.status_code == 409 and 'alreadyExisting' in response.content:
            print 'User already Existing on NAS\n'
        else:
            raise Exception('Fail to attach user to device, error code = {0}\n'.format(response.status_code))
    
test = Test()
test.run()
