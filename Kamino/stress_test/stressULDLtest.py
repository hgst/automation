__author__ = 'Erica Lee <erica.lee@wdc.com>'
__author__ = 'Nick Yang <nick.yang@wdc.com>'

from DL_UL import DL_UL
from ToolAPI import Tool
from restAPI import RestAPI
from datetime import datetime
from glob import glob
import argparse
import os
import shutil
import sys
import time
import threading
import commands
import sshAPI as sshclient
import junit_xml




class Test():
    def __init__(self):

        nfilepath = '/Data/restsdk/restsdk/userRoots/0000000052385efd015243e74eee0083/erica'
        nfolderpath = '/Data/restsdk/restsdk/userRoots/0000000052385efd015243e74eee0083/erica/'
        lfilepath = '/home/erica/'
        lfolderpath = '/home/erica'

        example1 = '\n  python.exe stressULDLtest.py -uut_ip 192.168.1.2 -port 22 -u nick@wd.com -p Test1234'
        example2 = '\n  format of file Path on local: {0} \n  format of folder Path on local: {1}'.format(lfilepath,lfolderpath)
        example3 = '\n  format of file Path on NAS: {0} \n  format of folder Path on NAS : {1}'.format(nfilepath,nfolderpath)
        example4 = '\n  python.exe stressULDLtest.py 192.168.1.2 nick@wd.com Test1234 -uf lfilePath lfolderPath -ud nfolderpath'
        example5 = '\n  python.exe stressULDLtest.py 192.168.1.2 nick@wd.com Test1234 -uf lfilePath lfolderPath -ud nfolderpath -t 10'
        example6 = '\n  python.exe stressULDLtest.py 192.168.1.2 nick@wd.com Test1234 -df nfilePath nfolderPath -dd lfolderPath -t 10'
        example7 = '\n  python.exe stressULDLtest.py 192.168.1.2 nick@wd.com Test1234 -rud nfilePath nfolderPath'

        # Create usages
        parser = argparse.ArgumentParser(description='*** Stress Test for Download/Upload on Kamino ***\n\nExamples:{0}{1}{2}{3}{4}{5}'.format(example1,example2,example3,example4,example5,example6),formatter_class=argparse.RawTextHelpFormatter)

        parser.add_argument('-u_n', help='user number')
        parser.add_argument('-m_n', help='metrics name, ex: Throughput')
        parser.add_argument('-uut_ip', help='Destination NAS IP address, ex. 192.168.1.46')
        parser.add_argument('-port', help='Destination NAS SSH Port, ex. 22')
        parser.add_argument('-u', help='Email user name')
        parser.add_argument('-p', help='Account password')
        parser.add_argument('-t', help='Total testing time (Minutes)  for single user use', metavar='timeout')
        parser.add_argument('-u_t', help='Total testing time (Minutes) for concurrent user use', metavar='timeout')
        parser.add_argument('-times', help='how many iteration want to testing, default is 1', metavar='times')
        parser.add_argument('-uf', help='Upload path from local, ex. dir: /home/erica/ , file: /home/erica', nargs='+', metavar='upload_from')
        parser.add_argument('-ud', help='Upload destination path on NAS', metavar='upload_dest')
        parser.add_argument('-rud', help='Delete upload destination path on NAS', metavar='rm_upload_dest')
        parser.add_argument('-dd', help='Download destination path on local', metavar="download_dest")
        parser.add_argument('-df', help='Download path from NAS', nargs='+', metavar="download_from")
        args = parser.parse_args()
        
        #If user didn't enter ip,port, use os.environ['uutip']
        self.properties = dict()
        if not args.uut_ip:
            self.uut_ip = os.environ['DEVICE_IP']
            self.port = int(os.environ['SSH_PORT'])
        else:
            self.uut_ip = args.uut_ip
            self.port = int(args.port)

        if not args.u and not args.p:
            self.username = "nick@abc.com"
            self.password = "Test1234"
        elif args.u and args.p:
            self.username = args.u
            self.password = args.p
        else:
            raise Exception("Please enter both user email and password")

        self.env = os.environ.get('CLOUD_SERVER')
        
        if not self.env:
            self.env = 'qa1'
        
        self.userno = args.u_n
        if not self.userno:
            self.userno = 1

        self.metric_names = 'HTTP.DEBUG'
        self.tc_name = 'DEBUG'
        self.properties.update({'WAY': 'HTTP.DEBUG', 'TCTYPE': 'DEBUG', 'USER_NO': self.userno})

        if args.m_n:
            self.metric_names, self.tc_name = args.m_n.split('_')
            # HTTP.TP.Bi-Dir.Perf_${user_number}-user-${file_number}x${file_size}
            self.properties['WAY'] = '.'.join(self.metric_names.split('.')[0:3])
            self.properties.update({'FILE_SIZE': self.tc_name.split('-')[-1].split('x')[-1]})
            self.properties['TCTYPE'] = self.metric_names.split('.')[-1]
                                   
        if args.t:
            self.properties.update({'Time Out': args.t})
            self.timeout = args.t
        else:
            self.timeout = 0
        
        if args.u_t:
            self.properties.update({'Time out': args.u_t})
            self.user_timeout = args.u_t
        else:
            self.user_timeout = 0
        
        if self.timeout == 0 or self.user_timeout == 0:
            self.total_run = 1
            if args.times:
                self.total_run = int(args.times)

        self.password = "Test1234"
        self.upload_from = args.uf
        self.upload_dest = args.ud
        self.rm_upload_dest = args.rud
        self.download_dest = args.dd
        self.download_from = args.df
        self.properties.update({'Device IP': self.uut_ip, 'CLOUD_SERVER': self.env, 'DEVICE_FW': os.environ.get('FW_VER')})
        self.upload_throughput_metrics = None
        self.download_throughput_metrics = None
        self.testCases = []
        self.ts = list()
        self.multi_ts = None
        self.exit_code = 0

    def run(self, user_no=1, iter=0, correct_userID=None):
        iteration = iter
        if self.upload_from and self.upload_dest:
            upload_target = ''
            upload_file_no = 0
            upload_folder_no = 0
            for num, alist in enumerate(self.upload_from):
                folder, name = tool.file_or_folder(alist)
                print 'test name'
                print name
                if folder:
                    print 'test name split'
                    print name.split('_')
                    upload_folder_no += int(name.split('_')[1])
                else:
                    upload_file_no += 1
                upload_target += '{0} ;'.format(name)
            total_upload = upload_file_no + upload_folder_no
            self.properties.update({'Upload Target': '{0}'.format(upload_target)})       
            self.properties.update({'Upload files number': '{0}'.format(total_upload)})

        if self.download_dest and self.download_from:
            download_target = ''
            download_file_no = 0
            download_folder_no = 0
            for num, alist in enumerate(self.download_from):
                folder, name = tool.file_or_folder(alist)
                upload_folder, upload_name = tool.file_or_folder(self.upload_from[num])
                if folder and upload_folder:
                    download_folder_no += int(upload_name.split('_')[1]) 
                else:
                    download_file_no += 1
                download_target += '{0} ;'.format(name)
            total_download = download_file_no + download_folder_no
            self.properties.update({'Download files number': '{0}'.format(total_download)})
            self.properties.update({'Download Target': '{0}'.format(download_target)})

        timeout = time.time() + int(self.timeout) * 60
        if int(self.timeout) > 0:
            print "-----------duration: ", int(self.timeout)
        
        stop_event = threading.Event()
        while not stop_event.isSet():
            thread_test = list()
            print "iteration==========: ", iteration
            if self.upload_from and self.upload_dest:
                #print "uf length: ",len(self.upload_from
                for loc, task in enumerate(self.upload_from):
                    w = threading.Thread(target=self.worker, name='Upload-%s' % (loc+1), args=(task, self.upload_dest, loc, user_no,))
                    thread_test.append(w)
                    w.start()
                    time.sleep(3)

            if self.download_dest and self.download_from:
                for j, task in enumerate(self.download_from):
                    if not self.upload_dest:
                        self.upload_dest = None
                    if not self.upload_from:
                        self.upload_from = [None]*len(self.download_from)
                    t = threading.Thread(target=self.worker_dl, name='Download-%s' % (j+1), args=(self.download_dest, task, self.upload_dest, self.upload_from[j], j, user_no,))
                    thread_test.append(t)
                    t.start()
                    time.sleep(3)
                        
            if int(self.timeout) > 0:
                print "Time out be set"
                if time.time() > timeout:
                    stop_event.set()
                    print("Stopping as you wish testing %s minutes" % self.timeout)
            elif iteration + 1 >= self.total_run:
                #print "Iteration be set"
                stop_event.set()
                if int(self.user_timeout) == 0:
                    print "Iteration be set"
                    print("Stopping as you wish testing {0} times".format(self.total_run))
                else:
                    print "Concurrent Time out be set"
            
            # Let the thread jobs all done, and proceed the throughput measurement
            map(threading.Thread.join, thread_test)

            # Generate each TestSuite for DL & UL
            self.ts = self.multiTestSuite(iteration=iteration)

            # Delete the file on NAS everytime when upload job finishes
            if self.upload_from and self.upload_dest:
                upload_dest = tool.correct_userID_path(self.upload_dest, correct_userID)
                for loc, path in enumerate(self.upload_from):
                    dlul.delete_dir_files_on_nas(path, upload_dest)
            '''
            # Delete the file on local envrionment everytime when download job finishes
            if self.download_dest and self.download_from:
                for loc, path in enumerate(self.download_from):
                    folder_or_file, name = tool.file_or_folder(path)                 
                    dl_path = os.path.join(self.download_dest, name)
                    if os.path.exists(dl_path):
                        if not folder_or_file:
                            os.remove(dl_path)
                        else:
                            shutil.rmtree(dl_path)
                    else:
                        print 'Cannot delete %s because it does not exist.' % dl_path
            '''
            
            if self.rm_upload_dest:
                dlul.delete_dir_files_on_nas(self.rm_upload_dest)
        
            ''' Throughput related metric'''
            if self.upload_from and self.upload_dest:
                new_name = self.metric_names
                if self.properties.get('TCTYPE') == 'Perf':
                    new_name = self.metric_names.replace('HTTP.TP', 'TP')
                    user_number, user, total_size = self.tc_name.split('-')
                    file_number, file_size = total_size.split('x')
                    if new_name == 'TP.Bi-Dir.Perf':
                        new_name = new_name.replace('TP.Bi-Dir.Perf', 'TP.Bi-Dir.UL.Perf')
                    ul_metric = '%s.' % file_number
                    if int(file_number) > 1:
                        ul_metric += 'con.'
                    ul_metric += 'file'
                    new_name = new_name.replace('Perf', ul_metric)
                types, min_value, max_value, average_value = tool.get_throughput(dlul.ul_throughput, 'upload')
                self.upload_throughput_metrics = [['{0}.Avg'.format(new_name), '{0}'.format(average_value), 'kB/s'],
                                                  ['{0}.Min'.format(new_name), '{0}'.format(min_value), 'kB/s'],
                                                  ['{0}.Max'.format(new_name), '{0}'.format(max_value), 'kB/s']]

            if self.download_dest and self.download_from:
                new_name = self.metric_names
                if self.properties.get('TCTYPE') == 'Perf':
                    new_name = self.metric_names.replace('HTTP.TP', 'TP')
                    user_number, user, total_size = self.tc_name.split('-')
                    file_number, file_size = total_size.split('x')
                    if new_name == 'TP.Bi-Dir.Perf':
                        new_name = new_name.replace('TP.Bi-Dir.Perf', 'TP.Bi-Dir.DL.Perf')
                    dl_metric = '%s.' % file_number
                    if int(file_number) > 1:
                        dl_metric += 'con.'
                    dl_metric += 'file'
                    new_name = new_name.replace('Perf', dl_metric)

                types, min_value, max_value, average_value = tool.get_throughput(dlul.dl_throughput, 'download')
                self.download_throughput_metrics = [['{0}.Avg'.format(new_name), '{0}'.format(average_value), 'kB/s'],
                                                    ['{0}.Min'.format(new_name), '{0}'.format(min_value), 'kB/s'],
                                                    ['{0}.Max'.format(new_name), '{0}'.format(max_value), 'kB/s']]

            self.ts = self.multiTestSuite(self.tc_name, self.upload_throughput_metrics, self.download_throughput_metrics, iteration)
            iteration += 1

    def run_ul_test(self, src, dest, unum, user_no):
        error = False
        result = False
        output = commands.getoutput('du -sh %s' % src)
        size = output.split('\t')[0]
        file_type = tool.get_type(src)
        tname = 'UL_User{0}_{1}_{2}B_{3}'.format(user_no, file_type.upper(), size, unum)
        error_message = ''
        try:
            result = dlul.upload(src=src, dest=dest, userno=user_no, item_no=unum)
        except Exception as ex:
            print "***Exception: {0} with {1}".format(ex, sys.exc_info()[0])
            error_message = ex
            error = True
            self.exit_code = 1

        tc = junit_xml.TestCase(name=tname, classname='.'.join(['UL', self.properties.get('WAY')]))

        if not result or error:
            message = 'Failed to upload'
            if error:
                message += ' with Exception: {0} and sys_exe_info:{1}'.format(error_message, sys.exc_info()[0])
            tc.add_failure_info(message=message)

        return tc

    ## UL Thread worker ##
    def worker(self, uf_path, ud, i, user_no):
        ''' upload '''
        print 'start upload'
        self.testCases.append(self.run_ul_test(src=uf_path, dest=ud, unum=i, user_no=user_no))

    ## DL Thread worker ##
    def worker_dl(self, dd=None, df=None, ud=None, uf=None, dnum=0, user_no=1):
        ''' download '''
        print 'start download'
        self.testCases.append(self.run_dl_test(dd, df, ud, uf, dnum, user_no))

    def run_dl_test(self, dd=None, df=None, ud=None, uf=None, dnum=0 ,user_no=1):
        error = False
        result = False
        error_message = ''
        # If user gives incorrect userID, provide the correct one and replace download_from with the correct one
        df = tool.correct_userID_path(df,dlul.correct_userID)

        # Return True if a folder, return False is a file
        # Also return file/folder name
    
        folder_or_file, name = tool.file_or_folder(df)
        file_type = tool.get_type(df)
        file_exist, nas_info_dic, pa = tool.search_id(df)

        # Check if the folder is exists on NAS, if exists, then continue, else return
        if file_exist:
            ip = self.uut_ip.split(':')[0]
            ssh_info = [ip, 'root', "", self.port]
            ssh = sshclient.SSHClient(*ssh_info)
            ssh.connect()
            status, output = ssh.execute('du -sh {0}'.format(df))
            ssh.close()
            download_size = output.split('\t')[0]
            tname = 'DL_User{0}_{1}_{2}B_{3}'.format(user_no, file_type.upper(), download_size, dnum)
            try:
                result = dlul.download_dir_files(download_dest=dd, download_from=df, upload_dest=ud, upload_from=uf)
            except Exception as ex:
                print "***Exception: {0} with {1}".format(ex, sys.exc_info()[0])
                error_message = ex
                error = True
                self.exit_code = 1

        else:
            tname = 'DL_User{0}_{1}_{2}MB_{3}'.format(user_no, file_type.upper(), 0, dnum)
            error_message = 'There is no file on NAS'
            error = True
            self.exit_code = 1

        tc = junit_xml.TestCase(name=tname, classname='.'.join(['DL', self.properties.get('WAY')]))

        if not result or error:
            message = 'Failed to download'
            if error:
                message += ' with Exception: {0} and sys_exe_info:{1}'.format(error_message, sys.exc_info()[0])
            tc.add_failure_info(message=message)

        return tc

    def multiTestSuite(self, throughput_tc_name=None, upload_throughput_metrics=None, download_throughtput_metrics=None, iteration=0):
        TCTYPE = self.properties.get('TCTYPE')
        testcases = list()

        if upload_throughput_metrics is not None:
            testcases.append(junit_xml.TestCase(name=throughput_tc_name, classname='.'.join([self.properties.get('WAY'), 'UL']), metrics=upload_throughput_metrics))

        if download_throughtput_metrics is not None:
            testcases.append(junit_xml.TestCase(name=throughput_tc_name, classname='.'.join([self.properties.get('WAY'), 'DL']), metrics=download_throughtput_metrics))

        if testcases:
            if TCTYPE == 'Perf' or self.properties.get('WAY') == 'HTTP.DEBUG':
                self.ts.append(junit_xml.TestSuite(name='Perf-%s' % iteration, package='Stress Test', test_cases=testcases,
                                                   properties=self.properties, timestamp=tool.get_timestamp()))
        
        elif (TCTYPE == 'Stress'and not self.user_timeout) or self.properties.get('WAY') == 'HTTP.DEBUG':
            self.ts.append(junit_xml.TestSuite(name='Stress-%s' % iteration, package='Stress Test',
                           test_cases=self.testCases, properties=self.properties, timestamp=tool.get_timestamp()))
            self.testCases = []
        
        return self.ts

    def multi_user(self, user_no, origin_dd, iteration, correct_userID):
        error = False
        try:
            if origin_dd is not None:
                self.download_dest = origin_dd + 'user_%s/' % user_no
            self.run(user_no, iteration, correct_userID)
        except Exception as ex:
            print "***Exception: {0} with {1}".format(ex, sys.exc_info()[0])
            error = True
            self.exit_code = 1
        
        ts = junit_xml.TestSuite(name='iteration-%s' % iteration, package='Stress Test',
                                 test_cases=self.testCases, properties=self.properties, timestamp=get_timestamp())
        return ts
        
    def worker_multi_user(self, user_no, origin_dd, iteration, correct_userID):
        self.multi_ts = self.multi_user(user_no, origin_dd, iteration, correct_userID)

def get_timestamp():
    return datetime.now().replace(microsecond=0).strftime('%Y%m%d%H%M%S')
    
def delete_user_folder(uut_ip=None, port=None):
    deleted_path = '/Data/restsdk/userRoots/*'
    uut_ip = uut_ip.split(':')[0]
    ssh_info = [uut_ip, 'root', "", port]
    ssh = sshclient.SSHClient(*ssh_info)
    ssh.connect()
    print 'Removing contents in %s' % deleted_path
    delete_result = ssh.execute('rm -rf {0}'.format(deleted_path))
    ssh.close()


if __name__ == '__main__':
    con_user = []
    itera = 0
    origin_dd = None
    if '-dd' in sys.argv:
        dd_index = sys.argv.index('-dd')
        origin_dd = sys.argv[dd_index+1]
    if '-u_n' in sys.argv:
        index = sys.argv.index('-u_n')
        if '-t' in sys.argv and int(sys.argv[index+1]) > 1:
            sys.argv[sys.argv.index('-t')] = '-u_t'
    test = Test()
    # SCP upload the files first before downloading
    for user in xrange(int(test.userno)):
        username = "Nick_%s@abc.com" % user
        if int(test.userno) == 1:
            username = test.username
        rest = RestAPI(uut_ip=test.uut_ip, username=username, password=test.password, env=test.env)
        tool = Tool(uut_ip=test.uut_ip, email=username, password=test.password, env=test.env)
        user_id = rest.get_user_id()
        if test.download_from and test.upload_from:
            print "start to upload data_set to %s folder: %s for download use" % (username, user_id)
            if test.upload_dest:
                for uf in test.upload_from:
                    folder_exist, fname = tool.file_or_folder(uf)
                    scp_path = test.upload_dest+fname
                    if not folder_exist:
                       scp_path += '/'
                    if scp_path in test.download_from:
                        raise Exception("When doing Con.Bi-Dir, the path of the download from should not same as upload from")
            tool.upload_folder_file2NAS(test.upload_from, test.download_from, test.port, user_id)
    
    ## start test ##
    try:
        if int(test.userno) > 1:
            duration = int(test.user_timeout)*60
            timeout = time.time() + duration
            print "--------duration for testing concurrent user: ", duration
            pill2kill = threading.Event()
            while not pill2kill.isSet():
                print "Concurrent iteration: %s"%itera
                for user in xrange(int(test.userno)):
                    username = "Nick_%s@abc.com" % user
                    dlul = DL_UL(uut_ip=test.uut_ip, email=username, password=test.password, port=test.port, env=test.env)
                    tool = Tool(uut_ip=test.uut_ip, email=username, password=test.password, env=test.env)
                    u = threading.Thread(target=test.worker_multi_user, name='User-%s' % user, args=(user, origin_dd, itera, dlul.correct_userID,))
                    con_user.append(u)
                    u.start()
                    time.sleep(2)
                
                # timeout be set or run times
                if int(test.user_timeout) > 0:
                    if time.time() > timeout:
                        pill2kill.set()
                        print("Stopping as you wish testing {0} mins".format(test.user_timeout))
                elif itera >= 1:
                    pill2kill.set()
                    print("Stopping as you wish testing {0} times".format(int(itera)))
                
                # Let the thread jobs all done, and proceed the throughput measurement
                map(threading.Thread.join, con_user)

                test.testCases = []
                test.ts.append(test.multi_ts)
                itera += 1
        else:
            print "single user"
            dlul = DL_UL(uut_ip=test.uut_ip, email=test.username, password=test.password, port=test.port, env=test.env)
            tool = Tool(uut_ip=test.uut_ip, email=test.username, password=test.password, env=test.env)
            test.run(correct_userID=dlul.correct_userID)
    except Exception as e:
        print 'outside layer: {0}'.format(e.message)
        test.exit_code = 1

    finally:
        # Remove existed report files
        resultfile = os.path.join(os.getcwd()+'/output', 'output.xml')
        report_file = glob(resultfile)
        if report_file:
            for f in report_file:
                os.remove(f)
                print "Remove {} file".format(f)
        # Generate Report
        if not os.path.exists(os.getcwd()+'/output'):
            os.mkdir(os.getcwd()+'/output')
        result_file = os.path.join(os.getcwd()+'/output', 'output.xml')
        with open(result_file, 'a') as f:
            junit_xml.TestSuite.to_file(f, test.ts, prettyprint=True)
            f.close()
        # Remove user folder on NAS after test
        delete_user_folder(uut_ip=test.uut_ip, port=test.port)
        print "Exit Code: ", test.exit_code
        sys.exit(test.exit_code)
        