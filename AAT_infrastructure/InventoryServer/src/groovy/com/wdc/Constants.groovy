package com.wdc

enum Status {
	SUCCESS(1000),
	UNKNOWN(1001),
	NOT_FOUND(1002),
	DOMAIN_VALIDATION_FAILED(1003),
	INVALID_PARAMETER(1004),
	DEVICE_IN_USE(1005),
	DEVICE_NOT_OPERATIONAL(1006),
	DEVICE_BUSY(1007)
	
	Status(int value) {
		this.value = value
	}
	private final int value
	public int value() {
		return value
	}
}
