package com.wdc

class InventoryException extends Exception {
	Status errorCode = Status.UNKNOWN
		
	InventoryException(errorCode,message){
		super(message)
		this.errorCode = errorCode
	}
	
}
