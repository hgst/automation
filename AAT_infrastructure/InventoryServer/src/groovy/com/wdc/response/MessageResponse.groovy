package com.wdc.response

import com.wdc.Status;


class MessageResponse {
		
	Status status
	String message
		
	String getStatus(){
		return status.toString()
	}
	
	int getStatusCode(){
		return this.status.value
	}
	
	String toString(){		
		return "status=${this.getStatus()};statusCode=${this.getStatusCode()};message=$message"
	}

}
