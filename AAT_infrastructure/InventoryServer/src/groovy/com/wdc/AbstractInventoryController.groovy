package com.wdc


import grails.rest.*
import grails.converters.*
import static org.springframework.http.HttpStatus.*
import grails.rest.RestfulController
import com.wdc.response.*

abstract class AbstractInventoryController extends RestfulController {
	
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	static responseFormats = ['html','json','xml']
	
	protected AbstractInventoryController(Class domainResource){
		super(domainResource)
		
	}
	
	def beforeInterceptor = {
		log.debug("Entering action $actionUri")
	}
	
	def afterInterceptor = {
		log.debug("Leaving action $actionUri")
	}
		
	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		def theList = this.resource.list(params)
		withFormat {
			def messageResponse = new GetListResponse(status:Status.SUCCESS,list:theList)
			html {respond theList, model:[instanceCount: this.resource.count()]}
			json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
		}
	}
	
	protected Boolean show(domainInstance){
		log.debug("Show: ${domainInstance?.properties}")
		if(domainInstance == null){
			notFound()
			false
		}
		else
			true
	}
	
	protected Boolean create(domainInstance){
		log.debug("Create: ${domainInstance?.properties}")
		if(domainInstance == null){
			notFound()
			false
		}
		else{
			respond domainInstance
			true
		}
	}
	
	protected Boolean edit(domainInstance) {
		log.debug("Edit: ${domainInstance?.properties}")
		if(domainInstance == null){
			notFound()
			false
		}
		else{
			respond domainInstance
			true
		}
		
	}
	
	protected Boolean save(domainInstance) {
		log.debug("Save: ${domainInstance?.properties}")
		if(domainInstance == null){
			notFound()
			false
		}
		else
			true
	}
	
	protected Boolean update(domainInstance) {
		log.debug("Update: ${domainInstance?.properties}")
		if(domainInstance == null){
			notFound()
			false
		}
		else
			true
	}
	
	protected Boolean delete(domainInstance) {
		log.debug("Delete: ${domainInstance?.properties}")
		if(domainInstance == null){
			notFound()
			false
		}
		else
			true
	}
	
	
	protected Boolean renderErrors(domainInstance,String htmlView){
		boolean hasErrors = false
		if (domainInstance.hasErrors()) {
			hasErrors = true
			def errorArray = []
			eachError bean:domainInstance, {errorArray << message(error:it)}
			log.error("Domain instance $domainInstance has these errors: $errorArray")
			
			withFormat {
				html { respond domainInstance.errors, view:htmlView}
				json {render text:errorArray as JSON,status:OK,contentType:"application/json"}
				xml {render text:errorArray as XML,status:OK,contentType:"text/xml"}
				
			}
			return hasErrors
		}
		
	}

	protected void notFound() {
		def domainSimpleName = this.resourceClassName

		def labelCode = "${domainSimpleName}.label"
		def message = message(code: 'default.not.found.message', args: [message(code:labelCode, default: domainSimpleName),params.id])
		MessageResponse messageResponse = new MessageResponse(status:Status.NOT_FOUND,message:message)
		log.warn(message)
		flash.message = message
		withFormat {
			html {
				redirect action: "index", method: "GET"
			}
			json { render text:messageResponse as JSON, status:NOT_FOUND,contentType:"application/json" }
			xml { render text:messageResponse as XML, status:NOT_FOUND,contentType:"text/xml" }
		}
	}
	
	def handleUnhandledException(Exception ex){
		String errorMessage = "Caught the following unhandeled exception: ${ex.message}"
		MessageResponse messageResponse = new MessageResponse(status:Status.UNKNOWN,message:errorMessage)
		log.error(errorMessage)
		withFormat {
			html {
				flash.message = errorMessage
				redirect action: "index", method: "GET"
			}
			json { render text:messageResponse as JSON, status:INTERNAL_SERVER_ERROR,contentType:"application/json" }
			xml { render text:messageResponse as XML, status:INTERNAL_SERVER_ERROR,contentType:"text/xml" }
		}
	}

    
}
