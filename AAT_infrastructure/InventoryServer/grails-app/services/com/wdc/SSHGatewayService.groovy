package com.wdc

import grails.transaction.Transactional

@Transactional
class SSHGatewayService {

    def delete(SSHGateway SSHGatewayInstance) {
		def c = Device.createCriteria()
		def device = c.get {
			sshGateway{
				eq('id',SSHGatewayInstance.id)
			}
		}
		
		if(device != null){
			SSHGatewayInstance.errors.reject('SSHGateway.inUse',[device.macAddress] as Object[],'SSH Gateway is in use by device {0}, remove it from the device first')
			return
		}
		
		SSHGatewayInstance.delete flush:true

    }
}
