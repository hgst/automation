package com.wdc

import org.springframework.context.MessageSource;

import com.wdc.response.*

import grails.transaction.Transactional

@Transactional
class DeviceService {
	def MessageSource messageSource
	
	def MessageResponse checkIn(Device deviceInstance){
		CheckInResponse checkInResponse = new CheckInResponse()
		
		try{
			if(deviceInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Device not found")
		
			deviceInstance.jenkinsJob = null
			deviceInstance.isBusy = false
			deviceInstance.save flush:true
			if(deviceInstance.hasErrors()){
				def errorArray = []
				deviceInstance.errors.allErrors.each {errorArray << messageSource.getMessage(it,null)}
				throw new InventoryException(Status.DOMAIN_VALIDATION_FAILED,errorArray.toString())
			}
			
			checkInResponse.status = Status.SUCCESS
			checkInResponse.message = "Device successfully checked in"
			checkInResponse.checkedIn = true
			
		}
		catch(InventoryException ex){
			checkInResponse.status = ex.errorCode
			checkInResponse.message = ex.message
		}
		catch(Exception ex){
			checkInResponse.status = Status.UNKNOWN
			checkInResponse.message = ex.message
			
		}
		
		return checkInResponse
		
	}
	
	def MessageResponse checkOut(Device deviceInstance,String jenkinsJob,boolean force){
		CheckOutResponse checkOutResponse = new CheckOutResponse()
		
		try{

			if(deviceInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Device not found")
			
			if(Utils.isNullOrEmpty(jenkinsJob))
				throw new InventoryException(Status.INVALID_PARAMETER,"jenkinsJob parameter cannot be empty")
			
			if(!Utils.isNullOrEmpty(deviceInstance.jenkinsJob) && force == false)
				throw new InventoryException(Status.DEVICE_IN_USE,"Device is in use by another job")
							
			if(!deviceInstance.isOperational)
				throw new InventoryException(Status.DEVICE_NOT_OPERATIONAL,"Device not operational")
				
			deviceInstance.jenkinsJob = jenkinsJob
			deviceInstance.isBusy = true
			deviceInstance.save flush:true
			if(deviceInstance.hasErrors()){
				def errorArray = []
				deviceInstance.errors.allErrors.each {errorArray << messageSource.getMessage(it,null)}
				throw new InventoryException(Status.DOMAIN_VALIDATION_FAILED,errorArray.toString())
			}
			
			checkOutResponse.status = Status.SUCCESS
			checkOutResponse.message = "Device successfully checked out"
			checkOutResponse.checkedOut = true
			
		}
		catch(InventoryException ex){
			checkOutResponse.status = ex.errorCode
			checkOutResponse.message = ex.message
		}
		catch(Exception ex){
			checkOutResponse.status = Status.UNKNOWN
			checkOutResponse.message = ex.message
			
		}
		
		return checkOutResponse
		
	}
	
	def MessageResponse isAvailable(Device deviceInstance){
		IsAvailableResponse isAvailableResponse = new IsAvailableResponse()
		boolean isAvailable = false
		
		try{
			if(deviceInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Device not found")
				
			if(Utils.isNullOrEmpty(deviceInstance.jenkinsJob) && !deviceInstance.isBusy && deviceInstance.isOperational)
				isAvailable = true
			
			isAvailableResponse.status = Status.SUCCESS
			isAvailableResponse.isAvailable = isAvailable
			
		}
		catch(InventoryException ex){
			isAvailableResponse.status = ex.errorCode
			isAvailableResponse.message = ex.message
		}
		catch(Exception ex){
			isAvailableResponse.status = Status.UNKNOWN
			isAvailableResponse.message = ex.message
			
		}
		
		return isAvailableResponse
		
	}
	
	def getSSHGateway(Device deviceInstance){
		MessageResponse getSSHGatewayResponse = new GetSSHGatewayResponse()
		
		try{
			if(deviceInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Device not found")
				
			getSSHGatewayResponse.status = Status.SUCCESS
			getSSHGatewayResponse.sshGateway = deviceInstance.sshGateway
							
		}
		catch(InventoryException ex){
			getSSHGatewayResponse.status = ex.errorCode
			getSSHGatewayResponse.message = ex.message
		}
		catch(Exception ex){
			getSSHGatewayResponse.status = Status.UNKNOWN
			getSSHGatewayResponse.message = ex.message
			
		}
		
		return getSSHGatewayResponse
		
	}
	
	def getSerialServer(Device deviceInstance){
		MessageResponse getSerialServerResponse = new GetSerialServerResponse()
		
		try{
			if(deviceInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Device not found")
				
			getSerialServerResponse.status = Status.SUCCESS
			getSerialServerResponse.serialServer = deviceInstance.serialServer
							
		}
		catch(InventoryException ex){
			getSerialServerResponse.status = ex.errorCode
			getSerialServerResponse.message = ex.message
		}
		catch(Exception ex){
			getSerialServerResponse.status = Status.UNKNOWN
			getSerialServerResponse.message = ex.message
			
		}
		
		return getSerialServerResponse
		
	}
	
	def getPowerSwitch(Device deviceInstance){
		MessageResponse getPowerSwitchResponse = new GetPowerSwitchResponse()
		
		try{
			if(deviceInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Device not found")
				
			getPowerSwitchResponse.status = Status.SUCCESS
			getPowerSwitchResponse.powerSwitch = deviceInstance.powerSwitch
							
		}
		catch(InventoryException ex){
			getPowerSwitchResponse.status = ex.errorCode
			getPowerSwitchResponse.message = ex.message
		}
		catch(Exception ex){
			getPowerSwitchResponse.status = Status.UNKNOWN
			getPowerSwitchResponse.message = ex.message
			
		}
		
		return getPowerSwitchResponse
		
	}
	
	def getDeviceByJob(String job){
		MessageResponse theResponse = new GetDeviceResponse()
		
		try{
			if(Utils.isNullOrEmpty(job))
				throw new InventoryException(Status.INVALID_PARAMETER,"Job cannot be empty")
				
			def c = Device.createCriteria()
			def results = c.get{
				eq('jenkinsJob',job)
			}
				
			theResponse.status = Status.SUCCESS
			theResponse.device = results
										
		}
		catch(InventoryException ex){
			theResponse.status = ex.errorCode
			theResponse.message = ex.message
		}
		catch(Exception ex){
			theResponse.status = Status.UNKNOWN
			theResponse.message = ex.message
			
		}
		
		return theResponse
	}

}