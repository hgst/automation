package com.wdc

import grails.transaction.Transactional

@Transactional
class PowerSwitchService {
    
    def delete(PowerSwitch powerSwitchInstance) {		
		def c = Device.createCriteria()
		def device = c.get {
			powerSwitch{
				eq('id',powerSwitchInstance.id)
			}
		}
		
		if(device != null){
			powerSwitchInstance.errors.reject('powerSwitch.inUse',[device.macAddress] as Object[],'Power Switch is in use by device {0}, remove it from the device first')
			return
		}
		
		powerSwitchInstance.delete flush:true

    }
}
