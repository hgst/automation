package com.wdc

import grails.transaction.Transactional

@Transactional
class SerialServerService {
	
	
    def delete(SerialServer serialServerInstance) {
		def c = Device.createCriteria()
		def device = c.get {
			serialServer{
				eq('id',serialServerInstance.id)
			}
		}
		
		if(device != null){
			serialServerInstance.errors.reject('serialServer.inUse',[device.macAddress] as Object[],'Serial Server is in use by device {0}, remove it from the device first')
			return
		}
		
		serialServerInstance.delete flush:true

    }
	
	
}
