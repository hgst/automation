package com.wdc

import com.wdc.response.*
import grails.transaction.Transactional
import org.springframework.context.MessageSource

@Transactional
class ProductService {
	
	def MessageSource messageSource
    
    def delete(Product productInstance) {
		def c = Device.createCriteria()
		def results = c.count {
			product{
				eq('id',productInstance.id)
			}
		}
		
		if(results > 0){
			productInstance.errors.reject('product.inUse',[productInstance] as Object[],'Product {0} is in use by one or more devices, remove the devices first')
			return
		}
				
		try{
			productInstance.delete flush:true
		}
		catch(Exception ex){
			String productLabel = messageSource.getMessage('product.label',null,null)
			productInstance.errors.reject('default.not.deleted.message',[productLabel,productInstance] as Object[],'Error deleting product')
		}

    }
	
	def getDevices(Product productInstance){
		
		MessageResponse theResponse = new GetDevicesByProductResponse()
		
		try{
			if(productInstance == null)
				throw new InventoryException(Status.NOT_FOUND,"Product not found")
				
			def c = Device.createCriteria()
			def results = c.list{
				product{
					eq('id',productInstance.id)
				}
			}
				
			theResponse.status = Status.SUCCESS
			theResponse.product = productInstance
			theResponse.devices = results
										
		}
		catch(InventoryException ex){
			theResponse.status = ex.errorCode
			theResponse.message = ex.message
		}
		catch(Exception ex){
			theResponse.status = Status.UNKNOWN
			theResponse.message = ex.message
			
		}
		
		return theResponse
	}
	
}
