
<%@ page import="com.wdc.Device" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'device.label', default: 'Device')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-device" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-device" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${deviceInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${deviceInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
			<ol class="property-list device">
			
				<g:if test="${deviceInstance?.macAddress}">
				<li class="fieldcontain">
					<span id="macAddress-label" class="property-label"><g:message code="device.macAddress.label" default="Mac Address" /></span>
					
						<span class="property-value" aria-labelledby="macAddress-label"><g:fieldValue bean="${deviceInstance}" field="macAddress"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${deviceInstance?.product}">
				<li class="fieldcontain">
					<span id="product-label" class="property-label"><g:message code="device.product.label" default="Product" /></span>
					
						<span class="property-value" aria-labelledby="product-label"><g:link controller="product" action="show" id="${deviceInstance?.product.id}">${deviceInstance?.product?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.jenkinsJob}">
				<li class="fieldcontain">
					<span id="jenkinsJob-label" class="property-label"><g:message code="device.jenkinsJob.label" default="Jenkins Job" /></span>
					
						<span class="property-value" aria-labelledby="jenkinsJob-label"><g:fieldValue bean="${deviceInstance}" field="jenkinsJob"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${deviceInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="device.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:fieldValue bean="${deviceInstance}" field="location"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${deviceInstance?.firmware}">
				<li class="fieldcontain">
					<span id="firmware-label" class="property-label"><g:message code="device.firmware.label" default="Firmware" /></span>
					
						<span class="property-value" aria-labelledby="firmware-label"><g:fieldValue bean="${deviceInstance}" field="firmware"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.powerSwitch}">
				<li class="fieldcontain">
					<span id="powerSwitch-label" class="property-label"><g:message code="device.powerSwitch.label" default="Power Switch" /></span>
					
						<span class="property-value" aria-labelledby="powerSwitch-label"><g:link controller="powerSwitch" action="show" id="${deviceInstance?.powerSwitch?.id}">${deviceInstance?.powerSwitch?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.sshGateway}">
				<li class="fieldcontain">
					<span id="sshGateway-label" class="property-label"><g:message code="device.sshGateway.label" default="Ssh Gateway" /></span>
					
						<span class="property-value" aria-labelledby="sshGateway-label"><g:link controller="SSHGateway" action="show" id="${deviceInstance?.sshGateway?.id}">${deviceInstance?.sshGateway?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.serialServer}">
				<li class="fieldcontain">
					<span id="serialServer-label" class="property-label"><g:message code="device.serialServer.label" default="Serial Server" /></span>
					
						<span class="property-value" aria-labelledby="serialServer-label"><g:link controller="serialServer" action="show" id="${deviceInstance?.serialServer?.id}">${deviceInstance?.serialServer?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
					
							
				<g:if test="${deviceInstance?.internalIPAddress}">
				<li class="fieldcontain">
					<span id="internalIPAddress-label" class="property-label"><g:message code="device.internalIPAddress.label" default="Internal IPA ddress" /></span>
					
						<span class="property-value" aria-labelledby="internalIPAddress-label"><g:fieldValue bean="${deviceInstance}" field="internalIPAddress"/></span>
					
				</li>
				</g:if>
			

				<li class="fieldcontain">
					<span id="isBusy-label" class="property-label"><g:message code="device.isBusy.label" default="Is Busy" /></span>
					
						<span class="property-value" aria-labelledby="isBusy-label"><g:formatBoolean boolean="${deviceInstance?.isBusy}" /></span>
					
				</li>
				
	
				<li class="fieldcontain">
					<span id="isOperational-label" class="property-label"><g:message code="device.isOperational.label" default="Is Operational" /></span>
					
						<span class="property-value" aria-labelledby="isOperational-label"><g:formatBoolean boolean="${deviceInstance?.isOperational}" /></span>
					
				</li>
	
				
				<g:if test="${deviceInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="device.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${deviceInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="device.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${deviceInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				
			
			</ol>
			<g:form url="[resource:deviceInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${deviceInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
