
<%@ page import="com.wdc.Device"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'device.label', default: 'Device')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<g:javascript>
		$( document ).ready(function() {
			//Need to adjust the body size if the table is too large
			var tableWidth = $("#list-device-table").width();
			setBodyWidth(tableWidth);
		});
	</g:javascript>
</head>
<body>
	<a href="#list-device" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;" /></a>
	<div class="nav" role="navigation">
		<ul>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="list-device" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.list.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<table id="list-device-table">
			<thead>
				<tr>
					<g:sortableColumn property="macAddress" title="${message(code: 'device.macAddress.label', default: 'Mac Address')}" />
					<g:sortableColumn property="product" title="${message(code: 'device.product.label', default: 'Product')}" />
					<g:sortableColumn property="jenkinsJob" title="${message(code: 'device.jenkinsJob.label', default: 'Jenkins Job')}" />
					<g:sortableColumn property="location" title="${message(code: 'device.location.label', default: 'Location')}" />
					<g:sortableColumn property="firmware" title="${message(code: 'device.firmware.label', default: 'Firmware')}" />
					<g:sortableColumn property="isBusy" title="${message(code: 'device.isBusy.label', default: 'Is Busy')}" />
					<g:sortableColumn property="isOperational" title="${message(code: 'device.isOperational.label', default: 'Is Operational')}" />
					<g:sortableColumn property="internalIPAddress"
						title="${message(code: 'device.internalIPAddress.label', default: 'Internal IP Address')}"
					/>
					<th><g:message code="device.powerSwitch.label" default="Power Switch" /></th>
					<th><g:message code="device.sshGateway.label" default="Ssh Gateway" /></th>
					<th><g:message code="device.serialServer.label" default="Serial Server" /></th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${deviceInstanceList}" status="i" var="deviceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${deviceInstance.id}">
								${fieldValue(bean: deviceInstance, field: "macAddress")}
							</g:link></td>
						<td><g:link controller="product" action="show" id="${deviceInstance.product.id}">
								${fieldValue(bean: deviceInstance, field: "product")}
							</g:link></td>
						<td>
							${fieldValue(bean: deviceInstance, field: "jenkinsJob")}
						</td>
						<td>
							${fieldValue(bean: deviceInstance, field: "location")}
						</td>
						<td>
							${fieldValue(bean: deviceInstance, field: "firmware")}
						</td>
						<td>
							${fieldValue(bean: deviceInstance, field: "isBusy")}
						</td>
						<td>
							${fieldValue(bean: deviceInstance, field: "isOperational")}
						</td>
						<td>
							${fieldValue(bean: deviceInstance, field: "internalIPAddress")}
						</td>
						<td><g:link controller="powerSwitch" action="show" id="${deviceInstance?.powerSwitch?.id}">
								${fieldValue(bean: deviceInstance, field: "powerSwitch")}
							</g:link></td>
						<td><g:link controller="SSHGateway" action="show" id="${deviceInstance?.sshGateway?.id}">
								${fieldValue(bean: deviceInstance, field: "sshGateway")}
							</g:link></td>
						<td><g:link controller="serialServer" action="show" id="${deviceInstance?.serialServer?.id}">
								${fieldValue(bean: deviceInstance, field: "serialServer")}
							</g:link></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate action="index" total="${instanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
