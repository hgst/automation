<%@ page import="com.wdc.Device" %>



<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'macAddress', 'error')} required">
	<label for="macAddress">
		<g:message code="device.macAddress.label" default="Mac Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="macAddress" value="${deviceInstance?.macAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'product', 'error')} required">
	<label for="product">
		<g:message code="device.product.label" default="Product" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="product" name="product.id" from="${com.wdc.Product.list()}" optionKey="id" value="${deviceInstance?.product?.id}" required=""  class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'jenkinsJob', 'error')} ">
	<label for="jenkinsJob">
		<g:message code="device.jenkinsJob.label" default="Jenkins Job" />
		
	</label>
	<g:textField name="jenkinsJob" value="${deviceInstance?.jenkinsJob}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="device.location.label" default="Location" />
		
	</label>
	<g:textField name="location" value="${deviceInstance?.location}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'firmware', 'error')} ">
	<label for="firmware">
		<g:message code="device.firmware.label" default="Firmware" />
		
	</label>
	<g:textField name="firmware" value="${deviceInstance?.firmware}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'powerSwitch', 'error')} ">
	<label for="powerSwitch">
		<g:message code="device.powerSwitch.label" default="Power Switch" />
		
	</label>
	<g:select id="powerSwitch" name="powerSwitch.id" from="${com.wdc.PowerSwitch.list()}" optionKey="id" value="${deviceInstance?.powerSwitch?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'sshGateway', 'error')} ">
	<label for="sshGateway">
		<g:message code="device.sshGateway.label" default="Ssh Gateway" />
		
	</label>
	<g:select id="sshGateway" name="sshGateway.id" from="${com.wdc.SSHGateway.list()}" optionKey="id" value="${deviceInstance?.sshGateway?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'serialServer', 'error')} ">
	<label for="serialServer">
		<g:message code="device.serialServer.label" default="Serial Server" />
		
	</label>
	<g:select id="serialServer" name="serialServer.id" from="${com.wdc.SerialServer.list()}" optionKey="id" value="${deviceInstance?.serialServer?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>


<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'internalIPAddress', 'error')} ">
	<label for="internalIPAddress">
		<g:message code="device.internalIPAddress.label" default="Internal IP Address" />
		
	</label>
	<g:textField name="internalIPAddress" value="${deviceInstance?.internalIPAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'isBusy', 'error')} ">
	<label for="isBusy">
		<g:message code="device.isBusy.label" default="Is Busy" />
		
	</label>
	<g:checkBox name="isBusy" value="${deviceInstance?.isBusy}" />

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'isOperational', 'error')} ">
	<label for="isOperational">
		<g:message code="device.isOperational.label" default="Is Operational" />
		
	</label>
	<g:checkBox name="isOperational" value="${deviceInstance?.isOperational}" />

</div>

