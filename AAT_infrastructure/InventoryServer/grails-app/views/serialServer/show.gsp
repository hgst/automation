
<%@ page import="com.wdc.SerialServer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'serialServer.label', default: 'SerialServer')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<a href="#show-serialServer" class="skip" tabindex="-1"><g:message
			code="default.link.skip.label" default="Skip to content&hellip;" /></a>
	<div class="nav" role="navigation">
		<ul>
			<li><g:link class="list" action="index">
					<g:message code="default.list.label" args="[entityName]" />
				</g:link></li>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="show-serialServer" class="content scaffold-show" role="main">
		<h1>
			<g:message code="default.show.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${serialServerInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${serialServerInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<ol class="property-list serialServer">
			<g:if test="${serialServerInstance?.ipAddress}">
				<li class="fieldcontain"><span id="ipAddress-label"
					class="property-label"><g:message
							code="serialServer.ipAddress.label" default="Ip Address" /></span> <span
					class="property-value" aria-labelledby="ipAddress-label"><g:fieldValue
							bean="${serialServerInstance}" field="ipAddress" /></span></li>
			</g:if>

			<g:if test="${serialServerInstance?.port}">
				<li class="fieldcontain"><span id="port-label"
					class="property-label"><g:message
							code="serialServer.port.label" default="Port" /></span> <span
					class="property-value" aria-labelledby="port-label"><g:fieldValue
							bean="${serialServerInstance}" field="port" /></span></li>
			</g:if>

			<g:if test="${serialServerInstance?.device?.macAddress}">
				<li class="fieldcontain"><span id="device.macAddress-label"
					class="property-label"><g:message
							code="serialServer.device.label" default="Device" /></span> <span
					class="property-value" aria-labelledby="device.macAddres-label"><g:link
							controller="device" action="show"
							id="${serialServerInstance.device.id}">
							<g:fieldValue bean="${serialServerInstance}"
								field="device.macAddress" />
						</g:link></span></li>
			</g:if>

			<g:if test="${serialServerInstance?.dateCreated}">
				<li class="fieldcontain"><span id="dateCreated-label"
					class="property-label"><g:message
							code="serialServer.dateCreated.label" default="Date Created" /></span>

					<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate
							date="${serialServerInstance?.dateCreated}" /></span></li>
			</g:if>

			<g:if test="${serialServerInstance?.lastUpdated}">
				<li class="fieldcontain"><span id="lastUpdated-label"
					class="property-label"><g:message
							code="serialServer.lastUpdated.label" default="Last Updated" /></span>

					<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate
							date="${serialServerInstance?.lastUpdated}" /></span></li>
			</g:if>

		</ol>
		<g:form url="[resource:serialServerInstance, action:'delete']"
			method="DELETE">
			<fieldset class="buttons">
				<g:link class="edit" action="edit"
					resource="${serialServerInstance}">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:link>
				<g:actionSubmit class="delete" action="delete"
					value="${message(code: 'default.button.delete.label', default: 'Delete')}"
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
