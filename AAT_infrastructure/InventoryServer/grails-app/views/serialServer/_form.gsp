<%@ page import="com.wdc.SerialServer" %>


<div class="fieldcontain ${hasErrors(bean: serialServerInstance, field: 'ipAddress', 'error')} required">
	<label for="ipAddress">
		<g:message code="serialServer.ipAddress.label" default="Ip Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="ipAddress" value="${serialServerInstance?.ipAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: serialServerInstance, field: 'port', 'error')} required">
	<label for="port">
		<g:message code="serialServer.port.label" default="Port" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="port" value="${serialServerInstance?.port}"/>

</div>

