
<%@ page import="com.wdc.SerialServer"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'serialServer.label', default: 'SerialServer')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<g:javascript>
		$( document ).ready(function() {
			//Need to adjust the body size if the table is too large
			var tableWidth = $("#list-serialServer-table").width();
			setBodyWidth(tableWidth);
		});
	</g:javascript>
</head>
<body>
	<a href="#list-serialServer" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
			default="Skip to content&hellip;"
		/></a>
	<div class="nav" role="navigation">
		<ul>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="list-serialServer" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.list.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<table id="list-serialServer-table">
			<thead>
				<tr>
					<g:sortableColumn property="ipAddress" title="${message(code: 'serialServer.ipAddress.label', default: 'Ip Address')}" />
					<g:sortableColumn property="port" title="${message(code: 'serialServer.port.label', default: 'Port')}" />
					<g:sortableColumn property="deviceMacAddress" title="${message(code: 'serialServer.device.label', default: 'Device')}" />
					<g:sortableColumn property="dateCreated" title="${message(code: 'serialServer.dateCreated.label', default: 'Date Created')}" />
					<g:sortableColumn property="lastUpdated" title="${message(code: 'serialServer.lastUpdated.label', default: 'Last Updated')}" />
				</tr>
			</thead>
			<tbody>
				<g:each in="${serialServerInstanceList}" status="i" var="serialServerInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${serialServerInstance.id}">
								${fieldValue(bean: serialServerInstance, field: "ipAddress")}
							</g:link></td>
						<td>
							${fieldValue(bean: serialServerInstance, field: "port")}
						</td>
						<td><g:if test="${serialServerInstance?.device}">
								<g:link controller="device" action="show" id="${serialServerInstance.device.id}">
									${fieldValue(bean: serialServerInstance, field: "device.macAddress")}
								</g:link>
							</g:if> <g:else>&nbsp;</g:else></td>
						<td><g:formatDate date="${serialServerInstance.dateCreated}" /></td>
						<td><g:formatDate date="${serialServerInstance.lastUpdated}" /></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate action="index" total="${instanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
