
<%@ page import="com.wdc.SSHGateway"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'SSHGateway.label', default: 'SSHGateway')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<g:javascript>
		$( document ).ready(function() {
			//Need to adjust the body size if the table is too large
			var tableWidth = $("#list-SSHGateway-table").width();
			setBodyWidth(tableWidth);
		});
	</g:javascript>
</head>
<body>
	<a href="#list-SSHGateway" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
			default="Skip to content&hellip;"
		/></a>
	<div class="nav" role="navigation">
		<ul>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="list-SSHGateway" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.list.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<table id="list-SSHGateway-table">
			<thead>
				<tr>
					<g:sortableColumn property="ipAddress" title="${message(code: 'SSHGateway.ipAddress.label', default: 'Ip Address')}" />
					<g:sortableColumn property="port" title="${message(code: 'SSHGateway.port.label', default: 'Port')}" />
					<g:sortableColumn property="deviceMacAddress" title="${message(code: 'SSHGateway.device.label', default: 'Device')}" />
					<g:sortableColumn property="dateCreated" title="${message(code: 'SSHGateway.dateCreated.label', default: 'Date Created')}" />
					<g:sortableColumn property="lastUpdated" title="${message(code: 'SSHGateway.lastUpdated.label', default: 'Last Updated')}" />
				</tr>
			</thead>
			<tbody>
				<g:each in="${SSHGatewayInstanceList}" status="i" var="SSHGatewayInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${SSHGatewayInstance.id}">
								${fieldValue(bean: SSHGatewayInstance, field: "ipAddress")}
							</g:link></td>
						<td>
							${fieldValue(bean: SSHGatewayInstance, field: "port")}
						</td>
						<td><g:if test="${SSHGatewayInstance?.device}">
								<g:link controller="device" action="show" id="${SSHGatewayInstance.device.id}">
									${fieldValue(bean: SSHGatewayInstance, field: "device.macAddress")}
								</g:link>
							</g:if> <g:else>&nbsp;</g:else></td>
						<td><g:formatDate date="${SSHGatewayInstance.dateCreated}" /></td>
						<td><g:formatDate date="${SSHGatewayInstance.lastUpdated}" /></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate action="index" total="${instanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
