<%@ page import="com.wdc.SSHGateway" %>


<div class="fieldcontain ${hasErrors(bean: SSHGatewayInstance, field: 'ipAddress', 'error')} required">
	<label for="ipAddress">
		<g:message code="SSHGateway.ipAddress.label" default="Ip Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="ipAddress" value="${SSHGatewayInstance?.ipAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: SSHGatewayInstance, field: 'port', 'error')} required">
	<label for="port">
		<g:message code="SSHGateway.port.label" default="Port" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="port" value="${SSHGatewayInstance?.port}"/>

</div>

