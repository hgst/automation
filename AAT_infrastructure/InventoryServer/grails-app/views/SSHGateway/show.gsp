
<%@ page import="com.wdc.SSHGateway" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'SSHGateway.label', default: 'SSHGateway')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-SSHGateway" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-SSHGateway" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${SSHGatewayInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${SSHGatewayInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
			<ol class="property-list SSHGateway">
				<g:if test="${SSHGatewayInstance?.ipAddress}">
				<li class="fieldcontain">
					<span id="ipAddress-label" class="property-label"><g:message code="SSHGateway.ipAddress.label" default="Ip Address" /></span>
					
						<span class="property-value" aria-labelledby="ipAddress-label"><g:fieldValue bean="${SSHGatewayInstance}" field="ipAddress"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${SSHGatewayInstance?.port}">
				<li class="fieldcontain">
					<span id="port-label" class="property-label"><g:message code="SSHGateway.port.label" default="Port" /></span>
					
						<span class="property-value" aria-labelledby="port-label"><g:fieldValue bean="${SSHGatewayInstance}" field="port"/></span>
					
				</li>
				</g:if>
				
				<g:if test="${SSHGatewayInstance?.device?.macAddress}">
				<li class="fieldcontain"><span id="device.macAddress-label"
					class="property-label"><g:message
							code="SSHGateway.device.label" default="Device" /></span> <span
					class="property-value" aria-labelledby="device.macAddres-label"><g:link
							controller="device" action="show"
							id="${SSHGatewayInstance.device.id}">
							<g:fieldValue bean="${SSHGatewayInstance}"
								field="device.macAddress" />
						</g:link></span></li>
				</g:if>
			
				<g:if test="${SSHGatewayInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="SSHGateway.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${SSHGatewayInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${SSHGatewayInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="SSHGateway.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${SSHGatewayInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:SSHGatewayInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${SSHGatewayInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
