
<%@ page import="com.wdc.PowerSwitch"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'powerSwitch.label', default: 'PowerSwitch')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<g:javascript>
		$( document ).ready(function() {
			//Need to adjust the body size if the table is too large
			var tableWidth = $("#list-powerSwitch-table").width();
			setBodyWidth(tableWidth);
		});
	</g:javascript>
</head>
<body>
	<a href="#list-powerSwitch" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
			default="Skip to content&hellip;"
		/></a>
	<div class="nav" role="navigation">
		<ul>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="list-powerSwitch" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.list.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<table id="list-powerSwitch-table">
			<thead>
				<tr>
					<g:sortableColumn property="ipAddress" title="${message(code: 'powerSwitch.ipAddress.label', default: 'IP Address')}" />
					<g:sortableColumn property="port" title="${message(code: 'powerSwitch.port.label', default: 'Port')}" />
					<g:sortableColumn property="deviceMacAddress" title="${message(code: 'powerSwitch.device.label', default: 'Device')}" />
					<g:sortableColumn property="dateCreated" title="${message(code: 'default.date.created.label', default: 'Date Created')}" />
					<g:sortableColumn property="lastUpdated" title="${message(code: 'default.last.updated.label', default: 'Last Updated')}" />
				</tr>
			</thead>
			<tbody>
				<g:each in="${powerSwitchInstanceList}" status="i" var="powerSwitchInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${powerSwitchInstance.id}">
								${fieldValue(bean: powerSwitchInstance, field: "ipAddress")}
							</g:link></td>
						<td>
							${fieldValue(bean: powerSwitchInstance, field: "port")}
						</td>
						<td><g:if test="${powerSwitchInstance?.device}">
								<g:link controller="device" action="show" id="${powerSwitchInstance.device.id}">
									${fieldValue(bean: powerSwitchInstance, field: "device.macAddress")}
								</g:link>
							</g:if> <g:else>&nbsp;</g:else></td>
						<td><g:formatDate date="${powerSwitchInstance.dateCreated}" /></td>
						<td><g:formatDate date="${powerSwitchInstance.lastUpdated}" /></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate action="index" total="${instanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
