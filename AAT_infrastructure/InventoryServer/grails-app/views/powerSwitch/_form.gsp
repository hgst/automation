<%@ page import="com.wdc.PowerSwitch" %>


<div class="fieldcontain ${hasErrors(bean: powerSwitchInstance, field: 'ipAddress', 'error')} required">
	<label for="ipAddress">
		<g:message code="powerSwitch.ipAddress.label" default="IP Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="ipAddress" value="${powerSwitchInstance?.ipAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: powerSwitchInstance, field: 'port', 'error')} required">
	<label for="port">
		<g:message code="powerSwitch.port.label" default="Port" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="port" pattern="${powerSwitchInstance.constraints.port.matches}" value="${powerSwitchInstance?.port}"/>

</div>

