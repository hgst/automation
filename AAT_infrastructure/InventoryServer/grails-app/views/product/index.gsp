
<%@ page import="com.wdc.Product"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<g:javascript>
		$( document ).ready(function() {
			//Need to adjust the body size if the table is too large
			var tableWidth = $("#list-product-table").width();
			setBodyWidth(tableWidth);
		});
	</g:javascript>
</head>
<body>
	<a href="#list-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;" /></a>
	<div class="nav" role="navigation">
		<ul>
			<li><g:link class="create" action="create">
					<g:message code="default.new.label" args="[entityName]" />
				</g:link></li>
		</ul>
	</div>
	<div id="list-product" class="content scaffold-list" role="main">
		<h1>
			<g:message code="default.list.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<table id="list-product-table">
			<thead>
				<tr>
					<g:sortableColumn property="name" title="${message(code: 'product.name.label', default: 'Name')}" />
					<g:sortableColumn property="dateCreated" title="${message(code: 'default.date.created.label', default: 'Date Created')}" />
					<g:sortableColumn property="lastUpdated" title="${message(code: 'default.last.updated.label', default: 'Last Updated')}" />
				</tr>
			</thead>
			<tbody>
				<g:each in="${productInstanceList}" status="i" var="productInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
						<td><g:link action="show" id="${productInstance.id}">
								${fieldValue(bean: productInstance, field: "name")}
							</g:link></td>
						<td><g:formatDate date="${productInstance.dateCreated}" /></td>
						<td><g:formatDate date="${productInstance.lastUpdated}" /></td>
					</tr>
				</g:each>
			</tbody>
		</table>
		<div class="pagination">
			<g:paginate action="index" total="${instanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
