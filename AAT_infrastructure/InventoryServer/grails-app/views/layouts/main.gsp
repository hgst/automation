<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Inventory Server"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
	<body>
		<div id="wdLogo" role="banner">
			<table>
			<tr>
				<td><img src="${resource(dir: 'images', file: 'WD_Logo.jpg')}"/></td>
				<td><h1>Inventory Server</h1>
			</tr>
			</table>
		</div>
		<div id="primaryNav" class="nav" role="navigation">
			<ul>
				<li><g:link class="list" controller="Device" action="index">Devices</g:link></li>
				<li><g:link class="list" controller="PowerSwitch" action="index">Power Switch</g:link></li>
				<li><g:link class="list" controller="Product" action="index">Product</g:link></li>
				<li><g:link class="list" controller="SerialServer" action="index">Serial Server</g:link></li>
				<li><g:link class="list" controller="SSHGateway" action="index">SSH Gateway</g:link></li>
			</ul>
		</div>
		<g:layoutBody/>
		<div class="footer" role="contentinfo">
			Version: <g:meta name="app.version"/>
		</div>
		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
		<r:layoutResources />
	</body>
</html>
