package com.wdc

import com.wdc.response.*
import grails.rest.*
import grails.converters.*
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProductController extends AbstractInventoryController{
	
	def productService
	
	ProductController(){
		super(Product)
	}
			
    def show(Product productInstance) {
		if(super.show(productInstance)){
			def messageResponse = new GetProductResponse(status:Status.SUCCESS,product:productInstance)
			withFormat{
				html {respond productInstance}
				json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
				xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
			}
			
		}
	   
    }

    def create() {
        super.create(new Product(params))
    }

    @Transactional
    def save(Product productInstance) {
		if(!super.save(productInstance)) return
        
        if(renderErrors(productInstance,'create')) return
        productInstance.save flush:true
		if(renderErrors(productInstance,'create')) return

		def messageResponse = new GetProductResponse(status:Status.SUCCESS,product:productInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.created.message', args: [message(code: 'product.label', default: 'Product'), productInstance])
                redirect productInstance
            }
			json {render text:messageResponse as JSON,status:CREATED,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:CREATED,contentType:"text/xml"}
        }
    }

    def edit(Product productInstance) {
        super.edit(productInstance)
    }

    @Transactional
    def update(Product productInstance) {
		if(!super.update(productInstance)) return
        
        if(renderErrors(productInstance,'edit')) return
        productInstance.save flush:true
		if(renderErrors(productInstance,'edit')) return

		def messageResponse = new GetProductResponse(status:Status.SUCCESS,product:productInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'product.label', default: 'Product'), productInstance])
                redirect productInstance
            }
            json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
        }
    }

    @Transactional
    def delete(Product productInstance) {
		if(!super.delete(productInstance)) return
        		
        productService.delete(productInstance)
		if(renderErrors(productInstance,'show')) return
		
		def messageResponse = new MessageResponse(status:Status.SUCCESS,message:'Product has been successfully deleted')
        withFormat {
            html {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'product.label', default: 'Product'), productInstance])
                redirect action:"index", method:"GET"
            }
            json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
        }
    }
	
	def getDevices(Product productInstance){
		log.debug("GetDevices product ${productInstance}")
		def messageResponse = productService.getDevices(productInstance)
		log.debug(messageResponse)
			
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
		}
	}

}
