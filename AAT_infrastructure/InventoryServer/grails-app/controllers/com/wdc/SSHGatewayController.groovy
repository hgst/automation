package com.wdc

import com.wdc.response.*
import grails.rest.*
import grails.converters.*
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SSHGatewayController extends AbstractInventoryController{
	
	def SSHGatewayService
	
	SSHGatewayController(){
		super(SSHGateway)
	}

    def show(SSHGateway SSHGatewayInstance) {
        if(super.show(SSHGatewayInstance)){
			def messageResponse = new GetSSHGatewayResponse(status:Status.SUCCESS,sshGateway:SSHGatewayInstance)
			withFormat{
				html {respond SSHGatewayInstance}
				json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
				xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
			}
			
		}
    }

    def create() {
        super.create(new SSHGateway(params))
    }

    @Transactional
    def save(SSHGateway SSHGatewayInstance) {
		if(!super.save(SSHGatewayInstance)) return
        
        if(renderErrors(SSHGatewayInstance,'create')) return
        SSHGatewayInstance.save flush:true
		if(renderErrors(SSHGatewayInstance,'create')) return

		def messageResponse = new GetSSHGatewayResponse(status:Status.SUCCESS,sshGateway:SSHGatewayInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.created.message', args: [message(code: 'SSHGateway.label', default: 'SSHGateway'), SSHGatewayInstance])
                redirect SSHGatewayInstance
            }
            json {render text:messageResponse as JSON,status:CREATED,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:CREATED,contentType:"text/xml"}
        }
    }

    def edit(SSHGateway SSHGatewayInstance) {
        super.edit(SSHGatewayInstance)
    }

    @Transactional
    def update(SSHGateway SSHGatewayInstance) {
		if(!super.update(SSHGatewayInstance)) return
        
        if(renderErrors(SSHGatewayInstance,'edit')) return
        SSHGatewayInstance.save flush:true
		if(renderErrors(SSHGatewayInstance,'edit')) return

		def messageResponse = new GetSSHGatewayResponse(status:Status.SUCCESS,sshGateway:SSHGatewayInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SSHGateway.label', default: 'SSHGateway'),  SSHGatewayInstance])
                redirect SSHGatewayInstance
            }
            json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
        }
    }

    @Transactional
    def delete(SSHGateway SSHGatewayInstance) {
		if(!super.delete(SSHGatewayInstance)) return
				
        SSHGatewayService.delete(SSHGatewayInstance)
		if(renderErrors(SSHGatewayInstance,'show')) return
		
		def messageResponse = new MessageResponse(status:Status.SUCCESS,message:'SSHGateway has been successfully deleted')
        withFormat {
            html {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SSHGateway.label', default: 'SSHGateway'),  SSHGatewayInstance])
                redirect action:"index", method:"GET"
            }
            json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
        }
    }
	
}
