package com.wdc

import com.wdc.response.*
import grails.rest.*
import grails.converters.*
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PowerSwitchController extends AbstractInventoryController{
	
	def powerSwitchService
	
	PowerSwitchController(){
		super(PowerSwitch)
	}

    def show(PowerSwitch powerSwitchInstance) {
        if(super.show(powerSwitchInstance)){
			def messageResponse = new GetPowerSwitchResponse(status:Status.SUCCESS,powerSwitch:powerSwitchInstance)
			withFormat{
				html {respond powerSwitchInstance}
				json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
				xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
			}
			
		}
    }

    def create() {
        super.create(new PowerSwitch(params))
    }

    @Transactional
    def save(PowerSwitch powerSwitchInstance) {
		if(!super.save(powerSwitchInstance)) return
                
        if(renderErrors(powerSwitchInstance,'create')) return
        powerSwitchInstance.save flush:true
		if(renderErrors(powerSwitchInstance,'create')) return

		def messageResponse = new GetPowerSwitchResponse(status:Status.SUCCESS,powerSwitch:powerSwitchInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.created.message',args: [message(code: 'powerSwitch.label', default: 'PowerSwitch'), powerSwitchInstance])
                redirect powerSwitchInstance
            }
            json {render text:messageResponse as JSON,status:CREATED,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:CREATED,contentType:"text/xml"}
        }
    }

    def edit(PowerSwitch powerSwitchInstance) {
        super.edit(powerSwitchInstance)
    }

    @Transactional
    def update(PowerSwitch powerSwitchInstance) {
		if(!super.update(powerSwitchInstance)) return
        
        if(renderErrors(powerSwitchInstance,'edit')) return
        powerSwitchInstance.save flush:true
		if(renderErrors(powerSwitchInstance,'edit')) return

		def messageResponse = new GetPowerSwitchResponse(status:Status.SUCCESS,powerSwitch:powerSwitchInstance)
		withFormat {
            html {
                flash.message = message(code: 'default.updated.message',args: [message(code: 'powerSwitch.label', default: 'PowerSwitch'),powerSwitchInstance])
                redirect powerSwitchInstance
            }
            json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
        }
    }

    @Transactional
    def delete(PowerSwitch powerSwitchInstance) {
		if(!super.delete(powerSwitchInstance)) return
		
        powerSwitchService.delete(powerSwitchInstance)
		if(renderErrors(powerSwitchInstance,'show')) return
		
		def messageResponse = new MessageResponse(status:Status.SUCCESS,message:'Power Switch has been successfully deleted')
        withFormat {
            html {
                flash.message = message(code: 'default.deleted.message',args: [message(code: 'powerSwitch.label', default: 'PowerSwitch'), powerSwitchInstance])
                redirect action:"index", method:"GET"
            }
            json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
        }
    }
	
}
