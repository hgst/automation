package com.wdc

import com.wdc.response.*
import grails.rest.*
import grails.converters.*
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SerialServerController extends AbstractInventoryController{
	
	def serialServerService

	SerialServerController(){
		super(SerialServer)
	}

    def show(SerialServer serialServerInstance) {
        if(super.show(serialServerInstance)){
			def messageResponse = new GetSerialServerResponse(status:Status.SUCCESS,serialServer:serialServerInstance)
			withFormat{
				html {respond serialServerInstance}
				json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
				xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
			}
			
		}
    }

    def create() {
        super.create(new SerialServer(params))
    }

    @Transactional
    def save(SerialServer serialServerInstance) {
		if(!super.save(serialServerInstance)) return
        
		if(renderErrors(serialServerInstance,'create')) return
		serialServerInstance.save flush:true
		if(renderErrors(serialServerInstance,'create')) return

		def messageResponse = new GetSerialServerResponse(status:Status.SUCCESS,serialServer:serialServerInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.created.message', args: [message(code: 'serialServer.label', default: 'SerialServer'), serialServerInstance])
                redirect serialServerInstance
            }
            json {render text:messageResponse as JSON,status:CREATED,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:CREATED,contentType:"text/xml"}
        }
    }

    def edit(SerialServer serialServerInstance) {
        super.edit(serialServerInstance)
    }

    @Transactional
    def update(SerialServer serialServerInstance) {
		if(!super.update(serialServerInstance)) return
        
        if(renderErrors(serialServerInstance,'edit')) return
        serialServerInstance.save flush:true
		if(renderErrors(serialServerInstance,'edit')) return

		def messageResponse = new GetSerialServerResponse(status:Status.SUCCESS,serialServer:serialServerInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'serialServer.label', default: 'SerialServer'),serialServerInstance])
                redirect serialServerInstance
            }
            json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
        }
    }

    @Transactional
    def delete(SerialServer serialServerInstance) {
		if(!super.delete(serialServerInstance)) return
				
        serialServerService.delete(serialServerInstance)
		if(renderErrors(serialServerInstance,'show')) return
		
		def messageResponse = new MessageResponse(status:Status.SUCCESS,message:'Serial Server has been successfully deleted')
        withFormat {
            html {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'serialServer.label', default: 'SerialServer'), serialServerInstance])
                redirect action:"index", method:"GET"
            }
            json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
        }
    }
	
}
