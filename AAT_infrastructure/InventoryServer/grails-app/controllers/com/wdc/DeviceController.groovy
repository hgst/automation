package com.wdc

import com.wdc.response.*
import grails.rest.*
import grails.converters.*
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DeviceController extends AbstractInventoryController{
	
	def deviceService
	
	DeviceController(){
		super(Device)
	}

    def show(Device deviceInstance) {
        if(super.show(deviceInstance)){
			def messageResponse = new GetDeviceResponse(status:Status.SUCCESS,device:deviceInstance)
			withFormat{
				html {respond deviceInstance}
				json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
				xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
			}
			
		}
    }

    def create() {
        super.create(new Device(params))
    }

    @Transactional
    def save(Device deviceInstance) {
		if(!super.save(deviceInstance)) return
        
        if(renderErrors(deviceInstance,'create')) return		
		deviceInstance.save flush:true
		if(renderErrors(deviceInstance,'create')) return

		def messageResponse = new GetDeviceResponse(status:Status.SUCCESS,device:deviceInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.created.message', args: [message(code: 'device.label', default: 'Device'), deviceInstance])
                redirect deviceInstance
            }
            json {render text:messageResponse as JSON,status:CREATED,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:CREATED,contentType:"text/xml"}
        }
    }

    def edit(Device deviceInstance) {
        super.edit(deviceInstance)
    }

    @Transactional
    def update(Device deviceInstance) {
		if(!super.update(deviceInstance)) return
        
        if(renderErrors(deviceInstance,'edit')) return
		//Support removing properties via REST calls
		if(deviceInstance.jenkinsJob?.equalsIgnoreCase('null')) deviceInstance.jenkinsJob = null
		if(deviceInstance.internalIPAddress?.equalsIgnoreCase('null')) deviceInstance.internalIPAddress = null
		if(deviceInstance.location?.equalsIgnoreCase('null')) deviceInstance.location = null
		if(deviceInstance.firmware?.equalsIgnoreCase('null')) deviceInstance.firmware = null
				
        deviceInstance.save flush:true
		if(renderErrors(deviceInstance,'edit')) return

		def messageResponse = new GetDeviceResponse(status:Status.SUCCESS,device:deviceInstance)
        withFormat {
            html {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'device.label', default: 'Device'), deviceInstance])
                redirect deviceInstance
            }
            json {render text:messageResponse as JSON,status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML,status:OK,contentType:"text/xml"}
        }
    }

    @Transactional
    def delete(Device deviceInstance) {
		if(!super.delete(deviceInstance)) return
        
		deviceInstance.delete flush:true
		if(renderErrors(deviceInstance,'show')) return
		
		def messageResponse = new MessageResponse(status:Status.SUCCESS,message:'Device has been successfully deleted')
        withFormat {
            html {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'device.label', default: 'Device'), deviceInstance])
                redirect action:"index", method:"GET"
            }
            json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
        }
    }
	
	@Transactional
	def checkIn(Device deviceInstance){
		log.debug("Checkin device ${deviceInstance}")
		def messageResponse = deviceService.checkIn(deviceInstance)
		log.debug(messageResponse)
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
			
	}
	
	@Transactional
	def checkOut(Device deviceInstance){
		log.debug("Checkout device ${deviceInstance},jenkinJob=${params.jenkinsJob},force=${params.force}")
		params.force = params.force ?: "false"
		def messageResponse = deviceService.checkOut(deviceInstance,params.jenkinsJob,Boolean.parseBoolean(params.force))
		log.debug(messageResponse)
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
		
	}
	
	def isAvailable(Device deviceInstance){
		log.debug("IsAvailable device ${deviceInstance}")
		def messageResponse = deviceService.isAvailable(deviceInstance)
		log.debug(messageResponse)
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
		
				
	}
	
	def getSSHGateway(Device deviceInstance){
		log.debug("GetSSHGateway device ${deviceInstance}")
		def messageResponse = deviceService.getSSHGateway(deviceInstance)
		log.debug(messageResponse)
			
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
				
	}
	
	def getSerialServer(Device deviceInstance){
		log.debug("GetSerialServer device ${deviceInstance}")
		def messageResponse = deviceService.getSerialServer(deviceInstance)
		log.debug(messageResponse)
			
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
				
	}
	
	def getPowerSwitch(Device deviceInstance){
		log.debug("GetPowerSwitch device ${deviceInstance}")
		def messageResponse = deviceService.getPowerSwitch(deviceInstance)
		log.debug(messageResponse)
			
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
				
	}
	
	def getDeviceByJob(){
		log.debug("GetDeviceByJob jenkinJob=${params.jenkinsJob}")
		def messageResponse = deviceService.getDeviceByJob(params.jenkinsJob)
		log.debug(messageResponse)
				
		withFormat {
			json {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
			xml {render text:messageResponse as XML, status:OK,contentType:"text/xml"}
			'*' {render text:messageResponse as JSON, status:OK,contentType:"application/json"}
		}
	}
	
}
