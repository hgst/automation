package com.wdc


class SSHGateway {
	String ipAddress
	String port
	Date dateCreated
	Date lastUpdated
	
	static transients = ['device']
	
	static mapping = {
		id column:'sshGatewayId'
	}
	
	static constraints = {
		port(unique:"ipAddress",shared:"portShared")
		ipAddress(shared:"ipAddressShared")
	}
	
	Device getDevice() {
		def c = Device.createCriteria()
		def device = c.get {
			sshGateway {
				eq('id',this.id)
			}
		}	
	}
	

	String toString(){		
		"${this.ipAddress}:${this.port}"
	}
}
