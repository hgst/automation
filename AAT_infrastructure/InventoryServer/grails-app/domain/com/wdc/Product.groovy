package com.wdc

class Product {
	String name
	Date dateCreated
	Date lastUpdated
	
	static mapping = {
		id column:'productId'
	}

    static constraints = {
		name unique:true
    }
	
	String toString(){
		this.name
	}
}
