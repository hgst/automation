package com.wdc


class PowerSwitch {
	String ipAddress
	String port
	Date dateCreated
	Date lastUpdated
	
	static transients = ['device']
	
	static mapping = {
		id column:'powerSwitchId'
	}
	
    static constraints = {
		port(unique:"ipAddress",shared:"portShared")
		ipAddress(shared:"ipAddressShared")
    }
	
	Device getDevice() {
		def c = Device.createCriteria()
		def device = c.get {
			powerSwitch {
				eq('id',this.id)
			}
		}
	}
	

	String toString(){		
		"${this.ipAddress}:${this.port}"
	}
	
}
