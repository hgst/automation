package com.wdc

class Device {
	String macAddress
	String jenkinsJob
	Boolean isBusy = false
	Boolean isOperational = false
	String internalIPAddress
	String location
	String firmware
	Date dateCreated
	Date lastUpdated
	
	Product product
	PowerSwitch powerSwitch
	SSHGateway sshGateway
	SerialServer serialServer
	

	static mapping = {
		id column:'deviceId'
	}

    static constraints = {
		macAddress(unique:true,shared:"macAddressShared")
		jenkinsJob unique:true,nullable:true
		powerSwitch unique:true, nullable:true
		sshGateway unique:true, nullable:true
		serialServer unique:true, nullable:true
		internalIPAddress unique:true, nullable:true
		location nullable:true
		firmware nullable:true
				
    }
	
	String toString(){
		this.macAddress
	}
	
}
