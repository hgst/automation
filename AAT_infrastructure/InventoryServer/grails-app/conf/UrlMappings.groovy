class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"{
			controller = "device"
			action = "index"
		}
        "500"(view:'/error')
		
		//Rest URI's
		"/devices"(resources:"device")
		"/powerSwitches"(resources:"powerSwitch")
		"/products"(resources:"product")
		"/serialServers"(resources:"serialServer")
		"/SSHGateways"(resources:"SSHGateway")
		
		//Web Service REST URI's
		"/api/device/$action/$id?"(controller:"device")
		"/api/powerSwitch/$action/$id?"(controller:"powerSwitch")
		"/api/product/$action/$id?"(controller:"product")
		"/api/serialServer/$action/$id?"(controller:"serialServer")
		"/api/SSHGateway/$action/$id?"(controller:"SSHGateway")
	}
}
