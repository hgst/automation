/*
 * Note, the integration tests will not run in the grails daemon
 */

package com.wdc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

class ProductControllerTest {
	def ProductController productController = null

	def populateValidParams(params) {
        assert params != null
        params["name"] = 'Sequoia'
    }
	
	
	@Before
	public void setUp() throws Exception {
		productController = new ProductController()
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIndex() {
		populateValidParams(productController.params)
		def product = new Product(productController.params)
		product.save flush:true
		productController.index()
		assertNotNull productController.modelAndView.model.productInstanceList
		assert productController.modelAndView.model.instanceCount == 1
	}

	@Test
	public void testShowProduct() {
		populateValidParams(productController.params)
		def product = new Product(productController.params)
		product.save flush:true
		productController.show(product)
		assert productController.modelAndView.model.productInstance == product 
	}

	@Test
	public void testCreate() {
		productController.create()
		assertNotNull productController.modelAndView.model.productInstance
		
	}

	@Test
	public void testSaveProduct() {
		populateValidParams(productController.params)
		def product = new Product(productController.params)
		productController.request.contentType = 'multipart/form-data'
		productController.save(product)
		assert productController.response.redirectedUrl == "/products/$product.id"
        assertNotNull productController.flash.message
        assert Product.count() == 1
	}

	@Test
	public void testEditProduct() {
		populateValidParams(productController.params)
		def product = new Product(productController.params)
		productController.edit(product)
		assert productController.modelAndView.model.productInstance == product
	}

	@Test
	public void testUpdateProduct() {
		populateValidParams(productController.params)
		def product = new Product(productController.params)
		product.save flush:true
		productController.request.contentType = 'multipart/form-data'
		productController.update(product)
		assert productController.response.redirectedUrl == "/products/$product.id"
		assertNotNull productController.flash.message
		
	}

	@Test
	public void testDeleteProduct() {
		populateValidParams(productController.params)
		def product = new Product(productController.params)
		product.save flush:true
		productController.request.contentType = 'multipart/form-data'
		productController.delete(product)
		assert Product.count() == 0
		assert productController.response.redirectedUrl == '/products'
		assertNotNull productController.flash.message
	}

}
