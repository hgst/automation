/*
 * Note, the integration tests will not run in the grails daemon
 */

package com.wdc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

class DeviceControllerTest {
	def DeviceController deviceController = null

	def populateValidParams(params) {
        assert params != null
        params["macAddress"] = 'a8:20:66:22:9e:66'
		def product = new Product(name:'Sequoia')
		product.save flush:true
		params["product"] = product
		params["location"] = "Test Location"
		params["firmware"] = "4.02.03"
    }
	
	
	@Before
	public void setUp() throws Exception {
		deviceController = new DeviceController()
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIndex() {
		populateValidParams(deviceController.params)
		def device = new Device(deviceController.params)
		device.save flush:true
		deviceController.index()
		assertNotNull deviceController.modelAndView.model.deviceInstanceList
		assert deviceController.modelAndView.model.instanceCount == 1
	}

	@Test
	public void testShowDevice() {
		populateValidParams(deviceController.params)
		def device = new Device(deviceController.params)
		device.save flush:true
		deviceController.show(device)
		assert deviceController.modelAndView.model.deviceInstance == device 
	}

	@Test
	public void testCreate() {
		deviceController.create()
		assertNotNull deviceController.modelAndView.model.deviceInstance
		
	}

	@Test
	public void testSaveDevice() {
		populateValidParams(deviceController.params)
		def device = new Device(deviceController.params)
		deviceController.request.contentType = 'multipart/form-data'
		deviceController.save(device)
		assert deviceController.response.redirectedUrl == "/devices/$device.id"
        assertNotNull deviceController.flash.message
        assert Device.count() == 1
	}

	@Test
	public void testEditDevice() {
		populateValidParams(deviceController.params)
		def device = new Device(deviceController.params)
		deviceController.edit(device)
		assert deviceController.modelAndView.model.deviceInstance == device
	}

	@Test
	public void testUpdateDevice() {
		populateValidParams(deviceController.params)
		def device = new Device(deviceController.params)
		device.save flush:true
		deviceController.request.contentType = 'multipart/form-data'
		deviceController.update(device)
		assert deviceController.response.redirectedUrl == "/devices/$device.id"
		assertNotNull deviceController.flash.message
		
	}

	@Test
	public void testDeleteDevice() {
		populateValidParams(deviceController.params)
		def device = new Device(deviceController.params)
		device.save flush:true
		deviceController.request.contentType = 'multipart/form-data'
		deviceController.delete(device)
		assert Device.count() == 0
		assert deviceController.response.redirectedUrl == '/devices'
		assertNotNull deviceController.flash.message
	}

}
