/*
 * Note, the integration tests will not run in the grails daemon
*/
package com.wdc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

class PowerSwitchControllerTest {
	def PowerSwitchController powerSwitchController = null
	
	def populateValidParams(params) {
		assert params != null
		params["ipAddress"] = '192.168.1.1'
		params["port"] = '5800'
	}

	@Before
	public void setUp() throws Exception {
		powerSwitchController = new PowerSwitchController()
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIndex() {
		populateValidParams(powerSwitchController.params)
		def powerSwitch = new PowerSwitch(powerSwitchController.params)
		powerSwitch.save flush:true
		powerSwitchController.index()
		assertNotNull powerSwitchController.modelAndView.model.powerSwitchInstanceList
		assert powerSwitchController.modelAndView.model.instanceCount == 1
	}

	@Test
	public void testShowPowerSwitch() {
		populateValidParams(powerSwitchController.params)
		def powerSwitch = new PowerSwitch(powerSwitchController.params)
		powerSwitch.save flush:true
		powerSwitchController.show(powerSwitch)
		assert powerSwitchController.modelAndView.model.powerSwitchInstance == powerSwitch
	}

	@Test
	public void testCreate() {
		powerSwitchController.create()
		assertNotNull powerSwitchController.modelAndView.model.powerSwitchInstance
		
	}

	@Test
	public void testSavePowerSwitch() {
		populateValidParams(powerSwitchController.params)
		def powerSwitch = new PowerSwitch(powerSwitchController.params)
		powerSwitchController.request.contentType = 'multipart/form-data'
		powerSwitchController.save(powerSwitch)
		assert powerSwitchController.response.redirectedUrl == "/powerSwitches/$powerSwitch.id"
        assertNotNull powerSwitchController.flash.message
        assert PowerSwitch.count() == 1
	}

	@Test
	public void testEditPowerSwitch() {
		populateValidParams(powerSwitchController.params)
		def powerSwitch = new PowerSwitch(powerSwitchController.params)
		powerSwitchController.edit(powerSwitch)
		assert powerSwitchController.modelAndView.model.powerSwitchInstance == powerSwitch
	}

	@Test
	public void testUpdatePowerSwitch() {
		populateValidParams(powerSwitchController.params)
		def powerSwitch = new PowerSwitch(powerSwitchController.params)
		powerSwitch.save flush:true
		powerSwitchController.request.contentType = 'multipart/form-data'
		powerSwitchController.update(powerSwitch)
		assert powerSwitchController.response.redirectedUrl == "/powerSwitches/$powerSwitch.id"
		assertNotNull powerSwitchController.flash.message
	}

	@Test
	public void testDeletePowerSwitch() {
		populateValidParams(powerSwitchController.params)
		def powerSwitch = new PowerSwitch(powerSwitchController.params)
		powerSwitch.save flush:true
		powerSwitchController.request.contentType = 'multipart/form-data'
		powerSwitchController.delete(powerSwitch)
		assert powerSwitch.count() == 0
		assert powerSwitchController.response.redirectedUrl == '/powerSwitches'
		assertNotNull powerSwitchController.flash.message
	}

}
