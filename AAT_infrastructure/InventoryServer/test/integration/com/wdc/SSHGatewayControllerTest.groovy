/*
 * Note, the integration tests will not run in the grails daemon
*/
package com.wdc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

class SSHGatewayControllerTest {
	def SSHGatewayController SSHGatewayController = null
	
	def populateValidParams(params) {
		assert params != null
		params["ipAddress"] = '192.168.1.1'
		params["port"] = '5800'
	}

	@Before
	public void setUp() throws Exception {
		SSHGatewayController = new SSHGatewayController()
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIndex() {
		populateValidParams(SSHGatewayController.params)
		def sshGateway = new SSHGateway(SSHGatewayController.params)
		sshGateway.save flush:true
		SSHGatewayController.index()
		assertNotNull SSHGatewayController.modelAndView.model.SSHGatewayInstanceList
		assert SSHGatewayController.modelAndView.model.instanceCount == 1
	}

	@Test
	public void testShowSSHGateway() {
		populateValidParams(SSHGatewayController.params)
		def sshGateway = new SSHGateway(SSHGatewayController.params)
		sshGateway.save flush:true
		SSHGatewayController.show(sshGateway)
		assert SSHGatewayController.modelAndView.model.SSHGatewayInstance == sshGateway
	}

	@Test
	public void testCreate() {
		SSHGatewayController.create()
		assertNotNull SSHGatewayController.modelAndView.model.SSHGatewayInstance
		
	}

	@Test
	public void testSaveSSHGateway() {
		populateValidParams(SSHGatewayController.params)
		def sshGateway = new SSHGateway(SSHGatewayController.params)
		SSHGatewayController.request.contentType = 'multipart/form-data'
		SSHGatewayController.save(sshGateway)
		assert SSHGatewayController.response.redirectedUrl == "/SSHGateways/$sshGateway.id"
        assertNotNull SSHGatewayController.flash.message
        assert SSHGateway.count() == 1
	}

	@Test
	public void testEditSSHGateway() {
		populateValidParams(SSHGatewayController.params)
		def sshGateway = new SSHGateway(SSHGatewayController.params)
		SSHGatewayController.edit(sshGateway)
		assert SSHGatewayController.modelAndView.model.SSHGatewayInstance == sshGateway
	}

	@Test
	public void testUpdateSSHGateway() {
		populateValidParams(SSHGatewayController.params)
		def sshGateway = new SSHGateway(SSHGatewayController.params)
		sshGateway.save flush:true
		SSHGatewayController.request.contentType = 'multipart/form-data'
		SSHGatewayController.update(sshGateway)
		assert SSHGatewayController.response.redirectedUrl == "/SSHGateways/$sshGateway.id"
		assertNotNull SSHGatewayController.flash.message
	}

	@Test
	public void testDeleteSSHGateway() {
		populateValidParams(SSHGatewayController.params)
		def sshGateway = new SSHGateway(SSHGatewayController.params)
		sshGateway.save flush:true
		SSHGatewayController.request.contentType = 'multipart/form-data'
		SSHGatewayController.delete(sshGateway)
		assert sshGateway.count() == 0
		assert SSHGatewayController.response.redirectedUrl == '/SSHGateways'
		assertNotNull SSHGatewayController.flash.message
	}

}
