/*
 * Note, the integration tests will not run in the grails daemon
*/
package com.wdc;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

class SerialServerControllerTest {
	def SerialServerController serialServerController = null
	
	def populateValidParams(params) {
		assert params != null
		params["ipAddress"] = '192.168.1.1'
		params["port"] = '5800'
	}

	@Before
	public void setUp() throws Exception {
		serialServerController = new SerialServerController()
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIndex() {
		populateValidParams(serialServerController.params)
		def serialServer = new SerialServer(serialServerController.params)
		serialServer.save flush:true
		serialServerController.index()
		assertNotNull serialServerController.modelAndView.model.serialServerInstanceList
		assert serialServerController.modelAndView.model.instanceCount == 1
	}

	@Test
	public void testShowSerialServer() {
		populateValidParams(serialServerController.params)
		def serialServer = new SerialServer(serialServerController.params)
		serialServer.save flush:true
		serialServerController.show(serialServer)
		assert serialServerController.modelAndView.model.serialServerInstance == serialServer
	}

	@Test
	public void testCreate() {
		serialServerController.create()
		assertNotNull serialServerController.modelAndView.model.serialServerInstance
		
	}

	@Test
	public void testSaveSerialServer() {
		populateValidParams(serialServerController.params)
		def serialServer = new SerialServer(serialServerController.params)
		serialServerController.request.contentType = 'multipart/form-data'
		serialServerController.save(serialServer)
		assert serialServerController.response.redirectedUrl == "/serialServers/$serialServer.id"
        assertNotNull serialServerController.flash.message
        assert SerialServer.count() == 1
	}

	@Test
	public void testEditSerialServer() {
		populateValidParams(serialServerController.params)
		def serialServer = new SerialServer(serialServerController.params)
		serialServerController.edit(serialServer)
		assert serialServerController.modelAndView.model.serialServerInstance == serialServer
	}

	@Test
	public void testUpdateSerialServer() {
		populateValidParams(serialServerController.params)
		def serialServer = new SerialServer(serialServerController.params)
		serialServer.save flush:true
		serialServerController.request.contentType = 'multipart/form-data'
		serialServerController.update(serialServer)
		assert serialServerController.response.redirectedUrl == "/serialServers/$serialServer.id"
		assertNotNull serialServerController.flash.message
	}

	@Test
	public void testDeleteSerialServer() {
		populateValidParams(serialServerController.params)
		def serialServer = new SerialServer(serialServerController.params)
		serialServer.save flush:true
		serialServerController.request.contentType = 'multipart/form-data'
		serialServerController.delete(serialServer)
		assert serialServer.count() == 0
		assert serialServerController.response.redirectedUrl == '/serialServers'
		assertNotNull serialServerController.flash.message
	}

}
