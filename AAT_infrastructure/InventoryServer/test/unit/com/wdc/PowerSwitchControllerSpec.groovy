package com.wdc



import grails.test.mixin.*
import spock.lang.*

@TestFor(PowerSwitchController)
@Mock([PowerSwitch,Device,PowerSwitchService])
class PowerSwitchControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        params["ipAddress"] = '192.168.1.1'
		params["port"] = '5800'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
           !model.instanceList
            model.instanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.powerSwitchInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            def powerSwitch = new PowerSwitch()
            powerSwitch.validate()
            controller.save(powerSwitch)

        then:"The create view is rendered again with the correct model"
            model.powerSwitchInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            powerSwitch = new PowerSwitch(params)

            controller.save(powerSwitch)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/powerSwitches/1'
            controller.flash.message != null
            PowerSwitch.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def powerSwitch = new PowerSwitch(params)
            controller.show(powerSwitch)

        then:"A model is populated containing the domain instance"
            model.powerSwitchInstance == powerSwitch
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def powerSwitch = new PowerSwitch(params)
            controller.edit(powerSwitch)

        then:"A model is populated containing the domain instance"
            model.powerSwitchInstance == powerSwitch
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/powerSwitches'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def powerSwitch = new PowerSwitch()
            powerSwitch.validate()
            controller.update(powerSwitch)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.powerSwitchInstance == powerSwitch

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            powerSwitch = new PowerSwitch(params).save(flush: true)
            controller.update(powerSwitch)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/powerSwitches/$powerSwitch.id"
            flash.message != null
    }
	/*
	 * Disabling this test until I can figure out how to get the Service
	 * to instantiate in the controller.  The integration test will work for now.
	 * 
    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/powerSwitches'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def powerSwitch = new PowerSwitch(params).save(flush: true)

        then:"It exists"
            PowerSwitch.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(powerSwitch)

        then:"The instance is deleted"
            PowerSwitch.count() == 0
            response.redirectedUrl == '/powerSwitches'
            flash.message != null
    }
    */
}
