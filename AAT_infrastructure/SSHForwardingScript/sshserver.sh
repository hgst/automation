#!/bin/bash

# $1 = Number of port forwarding rules (must be between 1 and 254)
# $2 = Starting port number (must be a multiple of 100)
# $3 = First 3 octets of starting IP address (such as 192.168.11)
# Must be run as root

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit
fi

countRules=254
startPort=10000
startIP=192.168.11
destPort=22

[[ $1 ]] && countRules=$1
[[ $2 ]] && startPort=$2
[[ $3 ]] && startIP=$3
[[ $4 ]] && destPort=$4

# Validate input
if [[ $countRules -gt 254 ]] || [[ $countRules -lt 1 ]]; then
	echo Error. Cannot create $countRules rules.
	exit
fi

modulo=$((startPort % 100))
if [[ $modulo -ne 0 ]]; then
	echo "Error. Starting port must be a multiple of 100 (such as 7000), not $startPort"
	exit
fi

countDots=$(grep -o "\." <<< "$startIP" | wc -l)

if [[ $countDots -ne 2 ]]; then
	echo "Error. Invalid starting IP address of $startIP. Must be only the first three octets (such as 192.168.11)."
	exit
fi

if [[ $destPort -gt 65535 ]] || [[ $destPort -lt 1 ]]; then
	echo Error. Destination port of $destPort is invalid.
	echo Must be between 1 and 65535
fi	

for (( i=1; i <= $countRules; i++ )); do
	port=$(($2 + i))
	ipAddr=$startIP.$i
	firewall-cmd --permanent --add-forward-port=port=$port:proto=tcp:toport=$destPort:toaddr=$ipAddr
	echo firewall-cmd --permanent --add-forward-port=port=$port:proto=tcp:toport=$destPort:toaddr=$ipAddr
done

firewall-cmd --reload
echo Complete.