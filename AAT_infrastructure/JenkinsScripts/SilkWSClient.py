## @package SilkWSClient
#  This module contains the classes to integrate with the Silk TCM manager.
#
import time
from suds.client import Client as SudsClient

SYSTEM_SERVICE_PATH = "/Services1.0/services/sccsystem?wsdl"
TEST_MANAGEMENT_SERVICE_PATH = "/Services1.0/services/tmexecution?wsdl"
ENTITY_SERVICE_PATH = "/axislegacy/sccentities?wsdl"

## @class Client().
#    
# Class Client is the primary class used to interface with the Silk TCM manager.
# This class communicates to the Silk TCM manager via a SOAP web service interface
class Client(object):
    ## @brief Constructor
    #  @param[in] silkHost string:
    #  @param[in] userName string:
    #  @param[in] password string:
    def __init__(self, silkHost, userName,password):
        #Param check
        if silkHost == None or userName == None or password == None:
            raise Exception("Invalid constructor parameters")
        
        self.silkHost = silkHost
        self.userName = userName
        self.password = password
        self.serviceClient = None
        self.testManagementClient = None
        self.entityClient = None
        self.session = None
        self.execHandle = None
        self.execState = -2
                
        self.__createClients__()
        self.__connect__()
    
    def __createClients__(self):
        self.serviceClient = SudsClient(self.silkHost + SYSTEM_SERVICE_PATH)
        if self.serviceClient == None:
            raise Exception("Could not create Service Client")
        self.testManagementClient = SudsClient(self.silkHost + TEST_MANAGEMENT_SERVICE_PATH)
        if self.testManagementClient == None:
            raise Exception("Could not create Test Management Client")
        self.entityClient = SudsClient(self.silkHost + ENTITY_SERVICE_PATH)
        if self.entityClient == None:
            raise Exception("Could not create Entity Client")
    
    def __connect__(self):
        self.session = self.serviceClient.service.logonUser(self.userName,self.password)
        if self.session == None:
            raise Exception("Could not connect to Silk Web Service")
    
    ## @brief Adds a new build or sets and existing build active on Silk
    #  @details This adds a new build or sets and existing build active on Silk
    #  @param[in] product string:
    #  @param[in] version string:
    #  @param[in] build string:
    #  @param[in] nodeId string:
    def setBuild(self,product,version,build,nodeId):
        #Check for an existing build, if we have it, make sure it is active
        existingBuild = False
        builds = self.entityClient.service.getBuilds(self.session,product,version)
        for b in builds:
            if b == build:
                if not self.entityClient.service.isBuildActive(self.session,product,version,build):
                    self.entityClient.service.setBuildActive(self.session,product,version,build,True)
                existingBuild = True
                break
         
        #If made it here, need to create a new build
        if not existingBuild:
            newBuildResult = self.entityClient.service.addBuild(self.session,product,version,build,"WebService Generated",True)
            if not newBuildResult:
                raise Exception("Failed to add new build number " + str(build))
        
        #Set the build property of the exec plan or it won't work
        node = self.testManagementClient.service.getNode(self.session,nodeId)
        for prop in node.propertyValues:
            if prop["name"] == "PROP_BUILDNAME":
                prop["value"] = build
                
        self.testManagementClient.service.updateNode(self.session,node)
        
    ## @brief Returns the execution handle for an executed test definition
    #  @return execHandle ExecutionHandle:       
    def getExecHandle(self):
        return self.execHandle
    
    ## @brief Start the execution plan on Silk
    #  @details This starts an execution plan on Silk
    #  If the build exists, it will set the build active, if not it will create a new build
    #  @param[in] product string:
    #  @param[in] version string:
    #  @param[in] build string:
    #  @param[in] nodeId string:
    def startExecution(self,product,version,build,nodeId):
        #Param check
        if product == None or version == None or build == None or nodeId == None:
            raise Exception("Invalid parameters in startExecution()")
        
        self.setBuild(product, version, build, nodeId)
        execHandleList = self.testManagementClient.service.startExecution(self.session,nodeId,build,1,None)
        if execHandleList == None or len(execHandleList) == 0:
            raise Exception("Failed to start execution...Product={0} Version={1} Build={2} NodeID={3}".format(product,version,build,nodeId))
        
        self.execHandle = execHandleList[0]
        
    ## @brief Returns the execution state for an executed test definition
    #  @details Returns the execution state.  The calling program should poll for changes in the state
    #  The following states are possible
    #  DEPLOYING = 0
    #  UPLOAD_DATA = 1
    #  QUEUED = 2
    #  FETCHING_SOURCES = 3
    #  FETCH_FULL_COVERAGE = 4
    #  RUNNING = 5
    #  FETCH_CODE_COVERAGE = 6
    #  WAIT_FOR_MANUAL_PRE = 7
    #  WAIT_FOR_MANUAL = 8
    #  WAIT_FOR_MANUAL_POST = 9
    #  COLLECTING_RESULTS = 10
    #  CLOUD_EXECSERVER_UPGRADING = 11
    #  EXECSERVER_INACCESSIBLE = 12
    #  FINISHED = -1
    #  UNKNOWN = -2
    #  @return execState ExecutionState:
    def getExecutionState(self):
        self.execState = self.testManagementClient.service.getStateOfExecution(self.session,self.execHandle)
        return self.execState
    
    
    ## @brief Report the test results to Jenkins
    #  @details This method will get the results for the entire test run.
    #  If one test fails or one test is not executed, then the test run is reported as a failure
    #  Passed = 1
    #  Failed = 2
    #  Not Executed = 3
    #  @return result int:    
    def getResults(self):        
        if self.execHandle == None:
            return 3
        
        #Get the execution result object
        executionResult = self.testManagementClient.service.getExecutionResult(self.session,self.execHandle)
        if executionResult == None:
            return 3
        
        #Get list of test definition results
        #If one test fails or is not executed, then return a fail
        testDefinitionResult = executionResult.testDefResult
        for testResult in testDefinitionResult:
            if testResult.status in [2,3]:
                return 2
            
        #If we make it here, then all passed
        return 1
            
        
        
                
                
                
            
            
            
        
        
        
        
        