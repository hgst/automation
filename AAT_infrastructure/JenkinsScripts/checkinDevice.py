## @package checkinDevice
#  This module set device on Inventory Server to be available 
#

import logging
import os

from testCaseAPI.src.configure import Device
from global_libraries import CommonTestLibrary as commonTestLibrary
from inventoryServerAPI.InventoryServerApi import InventoryException

commonTestLibrary.configure_logger()
log = logging.getLogger('AAT.checkinDevice')

device = Device(do_configure=False)
checkoutOwner = os.environ.get('JOB_NAME') + "-" + os.environ.get('FIRMWARE_NUMBER')

log.info("Checkout ownwer is: " + str(checkoutOwner))

try:
    checkoutDevice = device.device_api.getDeviceByJob(checkoutOwner)
    print 'Device to be checked in ' + str(checkoutDevice)

    ## Check in the device
    if not device.device_api.checkIn(checkoutDevice['id']):
        print 'Failed to checkin the device'
        exit()

except InventoryException, ex:
    print "Status = {0},Status code = {1}, message = {2}".format(ex.status, ex.statusCode, ex.message)
