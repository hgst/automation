# Locates an artifact in the nexus repository and downloads it

import argparse
import os
import sys
import urllib
import xml.etree.ElementTree as ET
import re
import urllib2
import time
import shutil
import traceback
import cgi
from SilkDBClient import Client

# Get environment value from Jenkins
PLATFORM = os.environ.get('PLATFORM_DESCRIPTION')
VERSION = os.environ.get('FIRMWARE_BUILD_NUMBER')
query_firmware_version = None
try:
    """ ##### for local testing #### """
    PROJECT = os.environ.get('PROJECT_NAME')
    # PROJECT = "Sequoia-04_04"
    # PROJECT = "Aurora-2_10"
    if 'iRadar' in PROJECT:
        project_name, iradar, firmware_version_temp = PROJECT.split("-")
        query_firmware_version = firmware_version_temp.replace('_', '.')
    else:
        project_name, firmware_version_temp = PROJECT.split("-")
        query_firmware_version = firmware_version_temp.replace('_', '.')
except Exception, ex:
    print ex
    PROJECT = None

try:
    NODE_ID = os.environ.get('AAT_ID')
    # NODE_ID = 34877
    # NODE_ID = 34414
except Exception, ex:
    print ex

""" ##### for local testing #### """
# PLATFORM = "Sequoia"
# PLATFORM = "Aurora"

print 'PLATFORM_DESCRIPTION: {0}'.format(PLATFORM)
print 'PROJECT: {0}'.format(PROJECT)
print 'query_firmware_version: {0}'.format(query_firmware_version)
print 'VERSION: {0}'.format(VERSION)

JENKINS_SERVER = 'http://jenkins.wdc.com'
DEFAULT_DESTINATION = os.getcwd()
REPO_LOCATION = 'http://repo.wdc.com'
ARTIFACT_BASE_URL = '{0}/service/local/repositories/projects/content/'.format(REPO_LOCATION)
REPO_FILE_BASE = '{0}/content/repositories/projects/'.format(REPO_LOCATION)
SEARCH_BASE_URL = '{0}/service/local/lucene/search?repositoryID=projects'.format(REPO_LOCATION)
ARTIFACT_BASE_URL_SEQ = '{0}/service/local/artifact/maven/content?r=projects&e=deb'.format(REPO_LOCATION)

# Inventory Server section
from testCaseAPI.src.configure import Device

device = Device(do_configure=False)


def main():

    parser = argparse.ArgumentParser(description='Locate and download latest good firmware build')
    parser.add_argument('--project', dest='project', action='store', default=PROJECT,
                        help='Specify the project name (Nexus group_id) eg. "Sequoia-03_00" '
                             'Supports wildcards but cannot start with a wildcard (*)')
    parser.add_argument('--version', dest='version', action='store', default=VERSION,
                        help='Specify a specific version number eg. "0300002-405"')
    args = parser.parse_args()
    
    assert args.project, "Could not resolve project. This must be specified as a parameter or environment variable"
    assert not args.project.startswith('*'), 'Project name query cannot start with a wildcard'
    
    group = args.project
    artifact = None
    version = args.version
    classifier = None
    build_url = None

    print 'version in main: {0}'.format(version)

    if PLATFORM == 'Sequoia':
        artifact = 'sq'
        downloader_seq = Nexus_Downloader_Sequoia(group, artifact, version, classifier, build_url)
        print 'downloader: {0}'.format(downloader_seq)
        # write out the build properties
        results = downloader_seq.write_build_properties()

    else:
        downloader = NexusDownloader(group, artifact, version)
        print 'downloader: {0}'.format(downloader)
        # write out the build properties
        results = downloader.write_build_properties()
    
    if results[0] and results[1]:
        firmware_url = results[0]
        latest_version = results[1]
        print 'build url: {0}'.format(firmware_url)
        print 'firmware version: {0}'.format(latest_version)

        # Check if promoted.txt return True, exit if not
        # # ITR 105209, only .bin file is changed with _, replace the last _ with -
        if PLATFORM == 'Sequoia':
            promoted_txt = results[2]
        else:
            temp_txt = rreplace(firmware_url, '_', '-', 1)
            print 'temp_txt: {0}'.format(temp_txt)
            promoted_txt = re.sub(r'.bin', '-promoted.txt', temp_txt)
        print 'promoted_txt: {0}'.format(promoted_txt)
        prompted = urllib2.urlopen(promoted_txt).read()
        prompted_value = prompted.replace("\n", "")
        print 'prompted_value: {0}'.format(prompted_value)

        if prompted_value == 'true':
            print 'ready for testing'
        else:
            print 'not ready for testing'
            # exit()

        # check against Silk whether latest firmware has been tested
        # if latest firmware is newer then create needText.txt to trigger AAT job
        if PLATFORM == 'Sequoia':
            s_version, s_build = downloader_seq.convert_seq_firmware_format(latest_version)
        else:
            a, b, c = latest_version.split(".")
            s_version = a + '.' + b
            s_build = c

        print 's_version: {} and s_build: {}'.format(s_version, s_build)

        client = Client('sevsql.sc.wdc.com', r'sc\fitbranded', 'Sev1contentsolutions', 'silkcentral')
        result = client.hasBuildExecuted('My Cloud OS Firmware', s_version, s_build, NODE_ID)
        print 'firmware have been tested on Silk? {}'.format(result)

        exit()

        if result:
            print '{0} already been tested'.format(latest_version)
        else:
            try:
                open('needTest.txt', 'w')
            except IOError as e:
                print 'Error open file: {0}'.format(e)
            print '{0} has not been tested, set to trigger downstream job'.format(latest_version)

    else:
        print 'Error getting latest firmware'


def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)


class NexusDownloader(object):
    def __init__(self, group, artifact, version):

        """ Using the given parameters, determine the latest (and/or desired) build properties.
            Provides methods to query nexus and download artifacts."""
        self.group = group
        self.artifact = artifact
        self.version = version
        self.latest = None

        print 'group (passed in) in init: {0}'.format(group)
        print 'artifact (passed in) in init: {0}'.format(artifact)
        print 'version (passed in) in init: {0}'.format(version)
        print 'version in init: {0}'.format(self.version)

        # get unknown parameters: artifact, version (if not passed in)
        self.group, self.artifact, self.version = self.get_lucene_result(group, artifact, version)

        # put together the url that will return the actual file
        self.build_url = self.get_artifact_url()  

        print 'build_url: {0}'.format(self.build_url)

    def get_lucene_result(self, group, artifact, version):
        """ Uses Nexus api lucene search to find the newest build, or a particular build specified by version
        ***NOTE*** This assumes that classifiers in Nexus are YYYYMMDD dates
        See lucene documentation: 
        https://repository.sonatype.org/nexus-indexer-lucene-plugin/default/docs/path__lucene_search.html """
        
        print 'version in lucene: {0}'.format(version)

        repo_query = '&g={0}'.format(group)
        url = SEARCH_BASE_URL + repo_query
        
        print '*INFO* [LUCENE QUERY] {0}'.format(url)
        
        data = urllib.urlopen(url).read()  
        try:
            tree = ET.XML(data)
        except ET.ParseError as e:
            print data
            raise e
        
        results = []
        found_desired_version = False
    
        for searchHit in ET.ElementPath.findall(tree, './/data//artifact'):
            result_group = searchHit.find('groupId').text
            result_artifact = searchHit.find('artifactId').text
            result_version = searchHit.find('version').text

            # print 'searchHit: {0}'.format(searchHit)

            if result_version == version:
                found_desired_version = True

            print '*DEBUG* Found result: {0}  {1}  {2}'.format(result_group, result_artifact, result_version)
            if result_group and result_artifact and result_version:
                results.append((result_group, result_artifact, result_version))
                print 'result inside if loop: {0}'.format(results)

        # set latest build
        self.latest = results[0][2]
        print 'latest build: {0}'.format(self.latest)
        print 'found_desired_version: {0}'.format(found_desired_version)
        print 'version: {0}'.format(version)

        if results and version is None:
            return results[0]
        elif results and found_desired_version:
            # we have to get a specific version
            return [result for result in results if result[2] == version][0]
        else:
            raise Exception("Requested artifact could not be found")

    def get_artifact_url(self):
        # Use Nexus api query structure to resolve an artifact
        # See GAV search documentation
        # http://nexus.xwiki.org/nexus/nexus-core-documentation-plugin/core/docs/rest.artifact.maven.content.html

        if PLATFORM == 'Sequoia':
            s_firmware_version, s_firmware_build = self.convert_seq_firmware_format(self.version)
        else:
            repo_query = '{0}/{1}/{2}/{1}_{2}.bin'.format(self.group, self.artifact, self.version)
        repo_query = ARTIFACT_BASE_URL + repo_query
        print '*INFO* [ARTIFACT REQUEST] {0}'.format(repo_query)
        return repo_query
    
    def guess_job_url(self, group):
        """ Trying to guess the jenkins job URL based on the nexus group """
        return JENKINS_SERVER + '/view/All/job/{0}/'.format(group)
    
    def write_build_properties(self):
        """ Writes the properties.txt and build_url.txt files containing the build details """

        a, b, c = self.version.split(".")
        s_firmware_version = a + '.' + b
        s_firmware_build = c
                        
        with open('properties.txt', 'w') as properties:
            properties.write('FIRMWARE_BUILD_URL={0}\n'.format(self.build_url))
            properties.write('FIRMWARE_BUILD_NAME={0}/{1}/{2}\n'.format(self.group, self.artifact, self.version))
            properties.write('FIRMWARE_BUILD_NUMBER={0}\n'.format(self.version))
            properties.write('TYPE={0}\n'.format(os.environ.get('TYPE')))
            properties.write('JOB_URL={0}\n'.format(self.guess_job_url(self.group)))
            properties.write('PACKAGE_URL={0}\n'.format(self.build_url))
            properties.write('FIRMWARE_FILE_URL={0}\n'.format(self.build_url))
            os.environ['SILK_VERSION'] = s_firmware_version
            os.environ['SILK_BUILD'] = s_firmware_build
            properties.write('SILK_VERSION={0}\n'.format(s_firmware_version))
            properties.write('SILK_BUILD={0}\n'.format(s_firmware_build))
            
        with open('build_url.txt', 'w') as build_file:
            build_file.write('PACKAGE_URL={0}\n'.format(self.build_url))
            if self.version:
                build_file.write('VERSION={0}\n'.format(self.version))
            
            build_file.write('LATEST={0}\n'.format(self.latest))
            
        return self.build_url, self.latest

class Nexus_Downloader_Sequoia(object):
    def __init__(self, group, artifact, version, classifier, build_url=None):
        '''Using the given parameters, determine the latest (and/or desired) build properties.
            Provides methods to query nexus and download artifacts.'''
        self.group = group
        self.artifact = artifact
        self.version = version
        self.classifier = classifier
        self.latest = None
        self.build_url = build_url

        if not self.build_url or build_url == 'NOT_SET' or '$' in build_url:
            # if there are unknown params, use a lucene search to find them
            if not classifier or '*' in group:
                self.group, self.artifact, self.version, self.classifier = self.get_lucene_result(group, artifact,
                                                                                                  version)

            # put together the url that will return the actual file
            self.build_url, self.promote = self.get_artifact_url()
            print 'self.build_url: {}'.format(self.build_url)
            print 'self.promote: {}'.format(self.promote)

    def get_lucene_result(self, group, artifact=None, version='LATEST'):
        '''Uses Nexus api lucene search to find the newest build, or a particular build specified by version
        ***NOTE*** This assumes that classifiers in Nexus are YYYYMMDD dates
        See lucene documentation:
        https://repository.sonatype.org/nexus-indexer-lucene-plugin/default/docs/path__lucene_search.html'''

        repo_query = '&g={0}&a={1}'.format(group, artifact)
        url = SEARCH_BASE_URL + repo_query

        print '*INFO* [LUCENE QUERY] {0}'.format(url)

        data = urllib.urlopen(url).read()
        try:
            tree = ET.XML(data)
        except ET.ParseError as e:
            print data
            raise e

        results = []
        found_desired_version = False

        for searchHit in ET.ElementPath.findall(tree, './/data//artifact'):
            result_group = searchHit.find('groupId').text
            result_artifact = searchHit.find('artifactId').text
            result_version = searchHit.find('version').text

            if result_version == version:
                found_desired_version = True

            result_classifier = None
            # artifacts can have other files associated (like a .pom), so we need to find the .deb
            for artifact_link in ET.ElementPath.findall(searchHit, './/artifactHits//artifactLink'):
                if artifact_link.find('extension').text == 'deb':
                    # ignore non-numerical build classifiers
                    try:
                        result_classifier = int(artifact_link.find('classifier').text)
                    except:
                        continue

            print '*DEBUG* Found result: {0}  {1}  {2}  {3}'.format(result_group, result_artifact, result_version,
                                                                    result_classifier)
            if result_group and result_artifact and result_version and result_classifier:
                results.append((result_group, result_artifact, result_version, result_classifier))

        print results

        # set latest build
        self.latest = results[0][2]
        print 'latest build: {0}'.format(self.latest)
        print 'found_desired_version: {0}'.format(found_desired_version)
        print 'version: {0}'.format(version)

        if results and version is None:
            return results[0]
        elif results and found_desired_version:
            # we have to get a specific version
            return [result for result in results if result[2] == version][0]
        else:
            raise Exception("Requested artifact could not be found")

    def get_artifact_url(self):
        repo_query = "{0}/{1}/{2}/{1}-{2}-{3}.deb".format(self.group, self.artifact, self.version, self.classifier)
        repo_query = ARTIFACT_BASE_URL + repo_query
        print '*INFO* [ARTIFACT REQUEST] {0}'.format(repo_query)

        promote_query = "{0}/{1}/{2}/{1}-{2}-promoted.txt".format(self.group, self.artifact, self.version)
        promote_query = ARTIFACT_BASE_URL + promote_query
        print '*INFO* [PROMOTE REQUEST] {0}'.format(promote_query)

        return repo_query, promote_query

    def write_build_properties(self):
        '''Writes the properties.txt and build_url.txt files containing the build details'''

        s_firmware_version, s_firmware_build = self.convert_seq_firmware_format(self.version)
        firmware_version = s_firmware_version + '.' + s_firmware_build

        with open('properties.txt', 'w') as properties:
            properties.write('FIRMWARE_BUILD_URL={0}\n'.format(self.build_url))
            properties.write('FIRMWARE_BUILD_NAME={0}/{1}/{2}\n'.format(self.group, self.artifact, self.version))
            properties.write('FIRMWARE_BUILD_NUMBER={0}\n'.format(firmware_version))
            properties.write('TYPE={0}\n'.format(os.environ.get('TYPE')))
            properties.write('PACKAGE_URL={0}\n'.format(self.build_url))
            properties.write('FIRMWARE_FILE_URL={0}\n'.format(self.build_url))
            os.environ['PACKAGE_URL'] = self.build_url
            os.environ['SILK_VERSION'] = s_firmware_version
            os.environ['SILK_BUILD'] = s_firmware_build
            properties.write('SILK_VERSION={0}\n'.format(s_firmware_version))
            properties.write('SILK_BUILD={0}\n'.format(s_firmware_build))

        with open('build_url.txt', 'w') as build_file:
            build_file.write('PACKAGE_URL={0}\n'.format(self.build_url))
            if self.version:
                build_file.write('BUILD_NUMBER={0}\n'.format(self.version))

            build_file.write('LATEST={0}\n'.format(self.latest))

        return self.build_url, self.latest, self.promote

    def convert_seq_firmware_format(self, firmware):
            a, build = firmware.split('-')
            print a, build
            version = firmware[2:4]
            version1 = firmware[1:2]
            print version, version1
            s_firmware_version = version1 + '.' + version
            s_firmware_build = build
            return s_firmware_version, s_firmware_build

if __name__ == '__main__':
    print 'starting get_nexus_artifact.main()'
    main()
    print 'exited get_nexus_artifact.main()'
    sys.stdout.flush()
