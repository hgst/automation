'''
Created on Nov 5th, 2014

@author: tran_jas

## @brief Checks out a device and assigns it to the Jenkins job
#  @detail Script will loop for 60 minutes until an available unit is found before timeout
#    will perform firmware update if needed
#    update Inventory Server with new firmware version
'''

import time
import logging
import os
import requests
import xml.etree.ElementTree as ET

from testCaseAPI.src.testclient import TestClient
from testCaseAPI.src.testclient import Fields

from testCaseAPI.src.configure import Device
from global_libraries import CommonTestLibrary as commonTestLibrary
from inventoryServerAPI.InventoryServerApi import InventoryException

commonTestLibrary.configure_logger()
log = logging.getLogger('AAT.checkoutDevice')

device = Device(do_configure=False)



try:

    firmwareUrl = 'http://repo.wdc.com/service/local/repositories/projects/content/Yosemite-2_00/My_Cloud_EX2100/2.00.131/My_Cloud_EX2100-2.00.131.bin'

    # update firmware for test unit
    updateCommand = 'http://192.168.11.47/api/2.6/rest/firmware_update?rest_method=PUT&auth_username=admin&auth_password=&image=' + firmwareUrl
    print 'updateCommand: {0}'.format (updateCommand)

    print 'updating firmware...'

    try:
        r = requests.put(updateCommand)
    except requests.exceptions.ConnectionError as e:
        print e
        pass

    print 'updating return value: {0}'.format(r)
    print 'r result: {0}'.format(r.content)

    # wait 5 minutes before checking system status
    # looking for 'ready' status, looping otherwise
    time.sleep(300)
    systemStateCommand = 'http://192.168.11.47/api/2.6/rest/system_state'
    count = 0
    myWaiting = True

    while myWaiting:
        try:
            r = requests.get(systemStateCommand)
        except requests.exceptions.ConnectionError as e:
            print e
            pass

        print 'system state return value: {0}'.format(r)
        print 'r result: {0}'.format(r.content)

        myET = ET.fromstring(r.content)

        try:
            element = myET.iter('status').next()
            text = None
            if element.text:
                text = element.text.strip()
            myStatus = text
        except StopIteration:
            raise RestError('XML tag not found or doesn\'t have a value: {0}'.format(tag))

        print 'device status: {0}'.format(myStatus)

        ## exit loop when system is ready
        if myStatus == 'ready':
            myWaiting = False

        ## keep waiting unless it passes 10 minutes
        time.sleep(60)
        count += 1
        print 'Waiting... loop count: {0}'.format(count)
        if count > 10:
            print 'FAILED: Unit not ready after 10 minutes'
            break

    # Verify correct firmware is on test unit
    versionCommand = 'http://192.168.11.47/api/2.6/rest/version'
    print 'versionCommand: {0}'.format (versionCommand)

    try:
        r = requests.get(versionCommand)
    except requests.exceptions.ConnectionError as e:
        print e
        pass

    print 'versionCommand return value: {0}'.format(r)
    print 'r result: {0}'.format(r.content)

    myET = ET.fromstring(r.content)

    try:
        element = myET.iter('firmware').next()
        text = None
        if element.text:
            text = element.text.strip()
        currentFirmware = text
    except StopIteration:
        raise RestError('XML tag not found or doesn\'t have a value: {0}'.format(tag))

    print 'currentFirmware: {0}'.format(currentFirmware)



except InventoryException, ex:
    print "Status = {0},Status code = {1}, message = {2}".format(ex.status, ex.statusCode, ex.message)
