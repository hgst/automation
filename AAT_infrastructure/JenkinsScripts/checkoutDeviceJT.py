"""
Created on Nov 5th, 2014

@author: tran_jas

## @brief Checks out a device and assigns it to the Jenkins job
#  @detail Script will loop for 60 minutes until an available unit is found before timeout
#    will perform firmware update if needed
#    update Inventory Server with new firmware version
#    will mark unit 'not operation if failed to get version (REST), or failed to update firmware, automatic try to test
#        again when getLatestFirmware is kicked off (60 minutes interval)
"""

import time
import logging
import os
import requests
import xml.etree.ElementTree as ET

from testCaseAPI.src.configure import Device
from global_libraries import CommonTestLibrary as commonTestLibrary
from inventoryServerAPI.InventoryServerApi import InventoryException

commonTestLibrary.configure_logger()
log = logging.getLogger('AAT.checkoutDevice')

device = Device(do_configure=False)

my_status = ''
error_message = 'CHECKOUT_RESULT=<p class="fail">'

# Get environment values from Jenkins
if not os.environ.get('FIRMWARE_URL'):
    firmware_url = 'FIRMWARE_URL not provided'
else:
    firmware_url = os.environ.get('FIRMWARE_URL')

if not os.environ.get('FIRMWARE_NUMBER'):
    firmware_version = 'FIRMWARE_NUMBER not provided'
else:
    firmware_version = os.environ.get('FIRMWARE_NUMBER')

if not os.environ.get('ART'):
    print 'Not ART job'
    ART = False
else:
    ART = os.environ.get('ART')
    print 'ART job? {0}'.format(ART)
    if ART == 'true':
        ART = True
        print 'Set ART to Boolean: {0}'.format(ART)
    else:
        ART = False
        print 'Set AAT to Boolean: {0}'.format(ART)

if not os.environ.get('AAT'):
    print 'Not AAT job'
    AAT = False
else:
    AAT = os.environ.get('AAT')
    print 'AAT job? {0}'.format(AAT)
    if AAT == 'true':
        AAT = True
        print 'Set AAT to Boolean: {0}'.format(AAT)
    else:
        AAT = False
        print 'Set AAT to Boolean: {0}'.format(AAT)

if not os.environ.get('BUILD_URL'):
    print 'Failed to get BUILD_URL'
else:
    build_url = os.environ.get('BUILD_URL')


checkout_owner = os.environ.get('JOB_NAME') + "-" + os.environ.get('FIRMWARE_NUMBER')
platform_description = os.environ.get('PLATFORM_DESCRIPTION')
   
print 'checkout_owner: ' + str(checkout_owner)
print 'platform_description: ' + str(platform_description)
print 'firmware_url: ' + str(firmware_url)
print 'firmware_version: ' + str(firmware_version)

try:
    count = 0
    time_out = 300
    found_device = False

    # Keep looking for available device, timeout after 60 minutes
    while count < 20 and (found_device is False):

        print 'count: ' + str(count)
        print 'found_device: ' + str(found_device)

        # Get the list of products
        product_list = device.product_api.list()['list']
        print 'product list ' + str(product_list)

        # Get the product we want to checkout
        product = None
        for p in product_list:
            print 'Product looking for: ' + str(platform_description)
            if 'BVT' in str(platform_description):
                if p['name'] == str(platform_description):
                    p['name'] = p['name'].split('-')[0]
                    product = p
                    break
            elif p['name'] == str(platform_description):
                product = p
                break

        if product is None:
            print 'Product not found'
            error_message += 'Product not found</p>\n'
            with open("checkout.txt", "w") as f:
                f.write(error_message)
            exit()

        # Get devices for a product
        devices = device.product_api.getDevices(product['id'])['devices']
        if len(devices) == 0:
            print 'No devices for product'
            error_message += 'No devices for product</p>\n'
            with open("checkout.txt", "w") as f:
                f.write(error_message)
            exit()

        # Get an available device
        # Get unit with same firmware as AAT for ART jobs
        uut = None
        if ART:
            for d in devices:
                if (d['isOperational'] is True) and (d['isBusy'] is False) and (d['firmware'] == firmware_version):
                    uut = d
                    found_device = True
                    break
        else:
            for d in devices:
                if (d['isOperational'] is True) and (d['isBusy'] is False):
                    uut = d
                    found_device = True
                    break
                
        if uut is None:
            print 'No available devices'
            count += 1
            time.sleep(180)

    if uut is None:
        print 'Failed to find uut:{0} with FW:{1}'.format(platform_description, os.environ.get('FIRMWARE_NUMBER'))
        error_message += 'Failed to find uut:{0} with FW:{1}</p>\n'.format(platform_description, os.environ.get('FIRMWARE_NUMBER'))
        with open("checkout.txt", "w") as f:
            f.write(error_message)
        exit(1)

    # CheckOut the device
    if not device.device_api.checkOut(uut['id'], checkout_owner):
        print 'Failed to checkout the device'
        error_message += 'Failed to checkout the device</p>\n'
        with open("checkout.txt", "w") as f:
            f.write(error_message)
        exit(1)

    print 'internalIPAddress: ' + str(uut['internalIPAddress'])

    # Only run firmware update for AAT
    if AAT:
        current_firmware = ''
        firmware_update_count = 0

        # get current firmware on test unit
        if platform_description == 'Yocto':
            version_command = 'cat /etc/version'
        else:
            version_command = 'http://' + uut['internalIPAddress'] + '/api/2.6/rest/version'
        print 'version_command: {0}'.format(version_command)

        try:
            r = requests.get(version_command)
        except requests.exceptions.ConnectionError as e:
            print e
            if not device.device_api.update(uut['id'], jenkinsJob=e, isOperational=False):
                print 'Failed to set unit NOT operational to Inventory Server'
            exit(1)
            pass

        print 'version_command return value: {0}'.format(r)
        print 'r result: {0}'.format(r.content)

        myET = ET.fromstring(r.content)

        try:
            element = myET.iter('firmware').next()
            text = None
            if element.text:
                text = element.text.strip()
            current_firmware = text
        except StopIteration, ex:
            print ex
            pass

        if platform_description == 'Sequoia':
            current_firmware = commonTestLibrary.convert_seq_firmware_to_alpha_format(current_firmware)
            time_out = 720
        print 'current_firmware: {0}'.format(current_firmware)

        # if unit already has the latest firmware, skip install firmware (for local testing)
        if current_firmware == firmware_version:
            print 'unit already has the latest firmware, skip firmware installation'
        else:
            updateCommand = 'http://' + uut['internalIPAddress'] + \
                            '/api/2.6/rest/firmware_update?rest_method=PUT&auth_username=admin&auth_password=&image=' \
                            + firmware_url
            print 'updateCommand: {0}'.format(updateCommand)

            incorrect_firmware = True

            while incorrect_firmware and firmware_update_count < 3:
                print 'updating firmware...'

                try:
                    r = requests.put(updateCommand)
                except requests.exceptions.ConnectionError as e:
                    print e
                    pass

                print 'updating firmware REST call return value: {0}'.format(r)
                print 'r result: {0}'.format(r.content)

                # wait 5 minutes before checking system status
                # looking for 'ready' status, looping otherwise
                print 'waiting 5 minutes for firmware update...'
                time.sleep(time_out)
                systemStateCommand = 'http://' + uut['internalIPAddress'] + '/api/2.6/rest/system_state'
                count = 0

                not_ready = True

                while not_ready:
                    try:
                        r = requests.get(systemStateCommand)
                    except requests.exceptions.ConnectionError as e:
                        print e
                        pass

                    print 'system state return value: {0}'.format(r)
                    print 'r result: {0}'.format(r.content)

                    myET = ET.fromstring(r.content)

                    try:
                        element = myET.iter('status').next()
                        text = None
                        if element.text:
                            text = element.text.strip()
                        my_status = text
                    except StopIteration, ex:
                        print ex
                        pass

                    print 'device status: {0}'.format(my_status)

                    # exit loop when system is ready
                    if my_status == 'ready':
                        not_ready = False

                    # keep waiting unless it passes 10 minutes
                    time.sleep(60)
                    count += 1
                    print 'Waiting... loop count: {0}'.format(count)
                    if count > 10:
                        print 'FAILED: Unit not ready after 10 minutes'
                        break

                # Verify correct firmware is on test unit
                try:
                    r = requests.get(version_command)
                except requests.exceptions.ConnectionError as e:
                    print e
                    pass

                print 'version_command return value: {0}'.format(r)
                print 'r result: {0}'.format(r.content)

                myET = ET.fromstring(r.content)

                try:
                    element = myET.iter('firmware').next()
                    text = None
                    if element.text:
                        text = element.text.strip()
                    current_firmware = text
                except StopIteration, ex:
                    print ex
                    pass

                if platform_description == 'Sequoia':
                    current_firmware = commonTestLibrary.convert_seq_firmware_to_alpha_format(current_firmware)
                print 'current_firmware: {0}'.format(current_firmware)
                print 'firmware_version: {}'.format(firmware_version)

                if firmware_version == 'FIRMWARE_NUMBER not provided':
                    print 'FIRMWARE_NUMBER not provided, current firmware on unit: {0}'.format(current_firmware)
                    incorrect_firmware = False
                else:
                    if current_firmware == firmware_version:
                        incorrect_firmware = False
                    else:
                        print 'incorrect firmware on test unit'
                        firmware_update_count += 1

                print 'firmware_update_count: {0}'.format(firmware_update_count)

        print 'current_firmware: {0}'.format(current_firmware)
        print 'firmware_version: {0}'.format(firmware_version)
        print 'firmware_update_count: {0}'.format(firmware_update_count)

        # mark device not operational, given build URL
        if current_firmware != firmware_version and firmware_update_count >= 3:
            print 'updating Inventory Server with: {0}'.format(current_firmware)
            if not device.device_api.update(uut['id'], firmware=current_firmware, jenkinsJob=build_url,
                                            isOperational=False):
                print 'Failed to update the device firmware version to Inventory Server'

            print 'Exit after trying to update firmware 3 times'
            error_message += 'Exit after trying to update firmware 3 times</p>\n'
            with open("checkout.txt", "w") as f:
                f.write(error_message)
            exit(1)
        else:
            print 'correct firmware on test unit, updating Inventory Server with: {0}'.format(current_firmware)
            if not device.device_api.update(uut['id'], firmware=current_firmware, jenkinsJob=checkout_owner):
                print 'Failed to update the device firmware version to Inventory Server'
except InventoryException, ex:
    print "Status = {0},Status code = {1}, message = {2}".format(ex.status, ex.statusCode, ex.message)
    error_message += 'Status = {0},Status code = {1}, message = {2}</p>\n'.format(ex.status, ex.statusCode, ex.message)
    with open("checkout.txt", "w") as f:
        f.write(error_message)
