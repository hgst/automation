#!/usr/bin/env python
# Author: Rick Lin (v0.5) 
# This script is to create a result table with all test case names 
# based on the XML generated by silk.
# Expose the following environment variables:
# 1) HTML_RESULT: HTML table
# 2) TOTAL_CASES: # of total cases
# 3) TOTAL_FAILS: # of fails
# 4) TOTAL_NOTEXECUTE: # of timeout cases
# 5) TOTAL_PASS: # of passes
# If any of case fail, generate a new file called AATFailed.txt

from xml.etree.ElementTree import parse
from glob import glob
import os

env_file = 'result.txt'
skipcases = ('silkSetup', 'silkTeardown')
total_cases = 0
total_errors = 0
total_failures = 0
total_pass = 0

def main():
    global skipcases, total_cases, total_errors, total_failures, total_pass
    print "Running Python Script to parse Silk Result"
    # Remove all txt files
    all_txt = glob('*.txt')
    if all_txt:
        for f in all_txt:
            os.remove(f)
            print "Remove {} file".format(f)
        # Switch to SCTMResults folder
    if os.path.exists('SCTMResults'):
        os.chdir('SCTMResults')
    else:
        print "Unable to locate the verification result folder"
        with open(env_file, "w") as f:
            f.write('TOTAL_CASES=0\n')
            f.write('TOTAL_FAILS=0\n')
            f.write('TOTAL_NOTEXECUTE=0\n')
            f.write('TOTAL_PASS=0\n')
            f.write('HTML_RESULT={}\n'.format('<p class="fail">Unable to locate the verification result folder</p>'))
        return
    # Check available result xml file
    xml_files = glob('*.xml')
    if xml_files:
        xml_result_file = xml_files[0]
        print "Parsing: ", xml_result_file
    else:
        print "Can not find any xml to parse the verification result"
        with open(env_file, "w") as f:
            f.write('TOTAL_CASES=0\n')
            f.write('TOTAL_FAILS=0\n')
            f.write('TOTAL_NOTEXECUTE=0\n')
            f.write('TOTAL_PASS=0\n')
            f.write('HTML_RESULT={}\n'.format('<p class="fail">Can not find any xml to parse the verification result</p>'))
        return
    # Parse xml
    tree = parse(xml_result_file)
    root = tree.getroot()
    if root.tag == 'testsuites':
        root = tree.getroot()[0]
        skipcases = []
    total_cases = int(root.attrib['tests']) - len(skipcases)
    total_failures = int(root.attrib['failures'])
    total_errors = int(root.attrib['errors'])
    print "Executed Cases: {}".format(total_cases)
    print "No. of fail cases: {}".format(total_failures)
    print "No. of error cases: {}".format(total_errors)	 # Not executed
    total_pass = total_cases - total_failures - total_errors
    if total_pass < 0:
        total_pass = 0
    case_result = {}
    for case in root.findall('testcase'):
        case_name = case.get('name')
        if case_name in skipcases:
            continue
        fail_result = case.find('failure')
        error_result = case.find('error')
        if fail_result is not None:
            # r = result.get('type')
            case_result[case_name] = 'Failed'
        elif error_result is not None:
            case_result[case_name] = 'NotExecuted'  # SCTM error
        else:
            case_result[case_name] = 'Pass'

    all_results = ''
    html_results = '<table id="report"><tr class="back"><td><b>Test Case</b></td><td><b>Test Result</b></td></tr>'
    for i, case in enumerate(case_result.items(), 1):
        name, result = case
        print "{:2d} - {:30} : {:15}".format(i, name, result)
        all_results += '{0}: {1}, '.format(name, result)
        if result == 'Failed':
            html_results += '<tr><td class="fail">{0}</td><td class="fail">{1}</td></tr>'.format(name, result)
        elif result == 'NotExecuted':
            html_results += '<tr><td class="error">{0}</td><td class="error">{1}</td></tr>'.format(name, result)
        else:
            html_results += '<tr><td>{0}</td><td>{1}</td></tr>'.format(name, result)
    # print all_results
    html_results += '</table>'
    # Switch back to parent directory
    os.chdir('..')

    # Result file
    with open(env_file, "w") as f:
        f.write('TEST_RESULT={}\n'.format(all_results))
        f.write('HTML_RESULT={}\n'.format(html_results))
        f.write('TOTAL_CASES={}\n'.format(total_cases))
        f.write('TOTAL_FAILS={}\n'.format(total_failures))
        f.write('TOTAL_NOTEXECUTE={}\n'.format(total_errors))
        f.write('TOTAL_PASS={}\n'.format(total_pass))
    if total_failures == 0:
        with open('Pass.txt', "w") as f:
            f.write('Pass {} cases'.format(total_cases))
    else:
        with open("AATFailed.txt", "w") as f:
            f.write('Fail {} cases in total {}'.format(total_failures, total_cases))

if __name__ == "__main__":
    main()
