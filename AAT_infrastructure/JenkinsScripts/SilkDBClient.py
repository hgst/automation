## @package SilkDBClient
#  This module contains the classes to integrate with the Silk TCM manager database.
#
import pymssql

## @class Client().
#    
# Class Client is the primary class used to interface with the Silk TCM manager database.
# This class communicates to the Silk TCM manager database via the pymssql library
class Client(object):
    ## @brief Constructor
    #  @param[in] host string:
    #  @param[in] user string:
    #  @param[in] password string:
    #  @param[in] database string:
    def __init__(self,host,user,password,database):
        #Param check
        if host == None or user == None or password == None or database == None:
            raise Exception("Invalid constructor parameters")
        
        self.conn = None
        self.host = host
        self.user = user
        self.password = password
        self.database = database
    
    def __connect__(self):
        self.conn = pymssql.connect(self.host,self.user,self.password,self.database)
        
    def __disconnect__(self):
        if self.conn != None:
            self.conn.close()
    
    ## @brief Determines if a build for a particular product, version, and execution plan has been executed
    #  @details This adds a new build or sets and existing build active on Silk
    #  @param[in] product string:
    #  @param[in] version string:
    #  @param[in] build string:
    #  @param[in] execPlanId int:
    #  @return boolean:        
    def hasBuildExecuted(self,product,version,build,execPlanId):
        self.__connect__()
        cursor = self.conn.cursor()
        cursor.execute('select count(*) from fcGetExecutionStatusByBuild(%s,%s,%s) where executionplanid = %d',(product,version,build,execPlanId))
        results = cursor.fetchone()
        
        cursor.close()
        self.__disconnect__()
        
        if results[0] > 0:
            return True
        else:
            return False
        
        
        
        
        
        